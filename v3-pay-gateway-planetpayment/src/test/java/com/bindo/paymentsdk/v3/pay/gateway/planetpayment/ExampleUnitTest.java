package com.bindo.paymentsdk.v3.pay.gateway.planetpayment;

import com.bindo.paymentsdk.v3.pay.common.CardDetectMode;
import com.bindo.paymentsdk.v3.pay.common.TransactionType;
import com.bindo.paymentsdk.v3.pay.common.database.models.Currency;
import com.bindo.paymentsdk.v3.pay.common.database.models.LocalBIN;
import com.bindo.paymentsdk.v3.pay.common.database.models.PYCTable;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.BatchTotals;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Request;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Response;
import com.bindo.paymentsdk.v3.pay.common.util.Hex;
import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.gateway.Gateway;

import org.junit.Test;

import java.io.FileInputStream;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    //private String root = "/Users/huhai/work/golang/src/bindolabs/bindo-payment-sdk-pp";
    private String root = "/home/dino/eclipse-workspace/bindo-payment-sdk";

    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void testTrx() throws Exception {
        Log.setsUseSystemOut(true);
        // @dino
        KeyStore keyStoreTerminal = KeyStore.getInstance("PKCS12");
        InputStream keyIS = new FileInputStream(root + "/v3-examples/src/main/assets/bindo.keystore");
        keyStoreTerminal.load(keyIS, "123456".toCharArray());

        HashMap<String, Object> config = new HashMap<>();
        config.put(PlanetPaymentGateway.CONFIG_REQUEST_KEYSTORE_TERMINAL, keyStoreTerminal);
        PlanetPaymentGateway planetPaymentGateway = new PlanetPaymentGateway(config);

        Request request = new Request();

        System.out.println("********************************** FIRST: RATE LOOKUP");
        request.setCardDetectMode(CardDetectMode.MANUAL);
        request.setTransactionType(TransactionType.PRE_AUTHORIZATION);
        //request.setPrimaryAccountNumber("4069700001000012"); request.setCardExpirationDate("2512"); // union
        //request.setPrimaryAccountNumber("4761180003441111"); request.setCardExpirationDate("2512"); // HKD
        //request.setPrimaryAccountNumber("4587400005811117"); request.setCardExpirationDate("2512"); // USD
        //request.setPrimaryAccountNumber("4002820000005125"); request.setCardExpirationDate("2512"); // OMR
        //request.setPrimaryAccountNumber("4051700000003926"); request.setCardExpirationDate("2101"); // JPY
        request.setPrimaryAccountNumber("4427808001112223337"); request.setCardExpirationDate("2212"); // ADVT1
        //request.setPrimaryAccountNumber("5411803440003449"); request.setCardExpirationDate("2512"); // HKD
        //request.setPrimaryAccountNumber("5204730000008404"); request.setCardExpirationDate("2512"); // USD

        request.setCardSequenceNumber("001");
        request.setTransactionCurrencyCode("344");
        request.setTransactionSystemTraceAuditNumber(Integer.parseInt(new SimpleDateFormat("HHmmss").format(new Date())));
        request.setTransactionAmount(59);
        //request.setTipAmount(5);
        //request.setCardholderBillingTipAmount(5);

        Response rsp = planetPaymentGateway.sendRequest(request);
        // if (rsp.getActionCode().equals("00")) { return; }
        System.out.println("***********************************************");

        if (rsp.getActionCode().equals("00")) {
            System.out.println("********************************** SECOND: FINANCIAL TRANSACTION");
            request.setTransactionType(TransactionType.AUTHORIZATION);
            request.setTransactionSystemTraceAuditNumber(Integer.parseInt(new SimpleDateFormat("HHmmss").format(new Date())));
            request.setCardholderBillingAmount(rsp.getCardholderBillingAmount());
            request.setCardholderBillingCurrencyCode(rsp.getCardholderBillingCurrencyCode());
            request.setCardholderBillingTipAmount(rsp.getCardholderBillingTipAmount());
            rsp = planetPaymentGateway.sendRequest(request);
            System.out.println("TIP1: " + rsp.getTipAmount());
            System.out.println("TIP2: " + rsp.getCardholderBillingTipAmount());
            System.out.println("Amount1: " + rsp.getTransactionAmount());
            System.out.println("Amount2: " + rsp.getCardholderBillingAmount());
            System.out.println("Rate: " + rsp.getConversionRate());
            System.out.println("Markup: " + rsp.getTransactionPYCRateMarkup());
            System.out.println("Markup: " + rsp.getTransactionPYCRateMarkupText());
            System.out.println("ARPC: " + rsp.getIccAuthorizationResponseCryptogram());
            if (rsp.getActionCode().equals("00")) { return; }
            if (rsp.getActionCode().equals("00")) {
                System.out.println("********************************** THIRD: REVERSAL");
                //request.setTransactionType(TransactionType.REFUND);
                //request.setTransactionType(TransactionType.AUTHORIZATION_VOID);
                //request.setTransactionType(TransactionType.AUTHORIZATION_TIMEOUT);
                request.setTransactionType(TransactionType.AUTHORIZATION_ADVICE);
                request.setRrn(rsp.getRetrievalReferenceNumber());
                rsp = planetPaymentGateway.sendRequest(request);
            }
        }
    }

    @Test
    public void TestAuthorization() throws Exception {
        HashMap<String, Object> config = new HashMap<>();
        HashMap<String, String> gatewayConfig = new HashMap<>();
        config.put("config_request_card_acceptor_id_code", "");
        config.put("config_request_card_acceptor_terminal_id", "");
        Gateway gateway = new PlanetPaymentGateway(config);
        Request req = new Request();
        req.setTransactionType(TransactionType.AUTHORIZATION);
        req.setMessageTypeId("0001");
        req.setTransactionAmount(100.00);
        req.setTransactionSystemTraceAuditNumber(1);
        req.setBatchNO(1);
        req.setTransactionDateAndTime("180403145051");
        req.setTransactionCurrencyCode("HKD");
        req.setGatewayConfigs(gatewayConfig);
        Response resp = gateway.sendRequest(req);
        System.out.print(resp.toString());
    }

    @Test
    public void TestDownloadLocalBIN() throws Exception {

        KeyStore keyStoreTerminal = KeyStore.getInstance("PKCS12");
        InputStream keyIS = new FileInputStream(root + "/v3-examples/src/main/assets/bindo.keystore");
        keyStoreTerminal.load(keyIS, "123456".toCharArray());

        KeyStore keyStoreTrusted = KeyStore.getInstance("PKCS12");
        InputStream tkeyIS = new FileInputStream(root + "/v3-examples/src/main/assets/ca-trust.keystore");
        //keyStoreTrusted.load(tkeyIS, "123456".toCharArray());

        String lastLocalBINDate = "00000000";
        String lastLocalBINTableIndex = "00000000";
        Request request = new Request();
        request.setTransactionType(TransactionType.LOCAL_BIN_TABLE_DOWNLOAD);
        request.setTransactionSystemTraceAuditNumber(102);
        request.setPlanetPaymentField63RequestForLocalBINTableDownload(lastLocalBINDate + lastLocalBINTableIndex + "0344");
        HashMap<String, Object> config = new HashMap<>();
        config.put(PlanetPaymentGateway.CONFIG_REQUEST_KEYSTORE_TERMINAL, keyStoreTerminal);
        config.put(PlanetPaymentGateway.CONFIG_REQUEST_KEYSTORE_TRUSTED, keyStoreTrusted);

        PlanetPaymentGateway planetPaymentGateway = new PlanetPaymentGateway(config);
        Response response = planetPaymentGateway.sendRequest(request);
        System.out.print(response.toString());
        if (response.isApproved()) {

            lastLocalBINDate = response.getLastLocalBINTableDate();
            lastLocalBINTableIndex = response.getLastLocalBINTableIndex();

            List<LocalBIN> bins = response.getPlanetPaymentLocalBINs();
            for (int i = 0; i < bins.size(); i++) {
                System.out.print(bins.toString());
            }
        }
    }

    @Test
    public void TestDownloadCurrency() throws Exception {
        KeyStore keyStoreTerminal = KeyStore.getInstance("PKCS12");
        InputStream keyIS = new FileInputStream(root + "/v3-examples/src/main/assets/bindo.keystore");
        keyStoreTerminal.load(keyIS, "123456".toCharArray());

        Request request = new Request();
        request.setTransactionType(TransactionType.CURRENCY_TABLE_DOWNLOAD);
        request.setTransactionSystemTraceAuditNumber(103);

        String nextEntry = "01";
        HashMap<String, Object> config = new HashMap<>();
        config.put(PlanetPaymentGateway.CONFIG_REQUEST_KEYSTORE_TERMINAL, keyStoreTerminal);
        PlanetPaymentGateway planetPaymentGateway = new PlanetPaymentGateway(config);
        int index = 0;
        while (true) {
            request.setPlanetPaymentField63RequestForCurrencyTableDownload(nextEntry);
            Response response = planetPaymentGateway.sendRequest(request);
            nextEntry = response.getNextEntryToSend();
            List<Currency> currencies = response.getPlanetPaymentCurrencies();
            for (int i = 0; i < currencies.size(); i++) {
                System.out.print(currencies.toString());
            }

            if (response.getTransactionProcessingCode().endsWith("0")) {
                break;
            }
            System.out.printf("\n--------------%d-----%s------------\n", index++, nextEntry);
        }
    }

    @Test
    public void DownloadPYCTable() throws Exception {
        KeyStore keyStoreTerminal = KeyStore.getInstance("PKCS12");
        InputStream keyIS = new FileInputStream(root + "/v3-examples/src/main/assets/bindo.keystore");
        keyStoreTerminal.load(keyIS, "123456".toCharArray());
        String lastLocalPYCDate = "00000000";
        String lastLocalPYCTableIndex = "00000000";
        Request request = new Request();
        request.setTransactionType(TransactionType.PYC_TABLE_DOWNLOAD);
        request.setTransactionSystemTraceAuditNumber(104);
        //"L"---4C   "F"----46
        request.setPlanetPaymentField63RequestForPYCTableDownload(lastLocalPYCDate + lastLocalPYCTableIndex + "4C");

        HashMap<String, Object> config = new HashMap<>();
        config.put(PlanetPaymentGateway.CONFIG_REQUEST_KEYSTORE_TERMINAL, keyStoreTerminal);
        PlanetPaymentGateway planetPaymentGateway = new PlanetPaymentGateway(config);
        Response response = planetPaymentGateway.sendRequest(request);
        System.out.print(response.toString());
        if (response.isApproved()) {

            lastLocalPYCDate = response.getLastLocalBINTableDate();
            lastLocalPYCTableIndex = response.getLastLocalBINTableIndex();

            List<PYCTable> pycs = response.getPlanetPaymentPYCTable();
            for (int i = 0; i < pycs.size(); i++) {
                System.out.print(pycs.toString());
            }
        }
    }

    @Test
    public void testSettlement() throws Exception {
        KeyStore keyStoreTerminal = KeyStore.getInstance("PKCS12");
        InputStream keyIS = new FileInputStream(root + "/v3-examples/src/main/assets/bindo.keystore");
        keyStoreTerminal.load(keyIS, "123456".toCharArray());
        KeyStore keyStoreTrusted = KeyStore.getInstance("PKCS12");
        InputStream tkeyIS = new FileInputStream(root + "/v3-examples/src/main/assets/ca-trust.keystore");
        //keyStoreTrusted.load(tkeyIS, "123456".toCharArray());

        Log.setsUseSystemOut(true);

        Request request = new Request();
        request.setTransactionType(TransactionType.SETTLEMENT);
        request.setTransactionSystemTraceAuditNumber(102);

        request.setBatchNO(1);
        BatchTotals batchTotals = new BatchTotals();
        batchTotals.setAuthorizeSalesAmount(150);
        batchTotals.setAuthorizeSalesCount(3);
        request.setBatchTotals(batchTotals);

        HashMap<String, Object> config = new HashMap<>();
        config.put(PlanetPaymentGateway.CONFIG_REQUEST_KEYSTORE_TERMINAL, keyStoreTerminal);
        config.put(PlanetPaymentGateway.CONFIG_REQUEST_KEYSTORE_TRUSTED, keyStoreTrusted);

        PlanetPaymentGateway planetPaymentGateway = new PlanetPaymentGateway(config);
        Response response = planetPaymentGateway.sendRequest(request);
        System.out.print(response.toString());
        if (response.getIsApproved()) {

        }
    }
}
