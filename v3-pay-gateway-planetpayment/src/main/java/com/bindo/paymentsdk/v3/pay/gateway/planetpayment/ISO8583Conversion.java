package com.bindo.paymentsdk.v3.pay.gateway.planetpayment;

import com.bindo.paymentsdk.v3.pay.common.CardDetectMode;
import com.bindo.paymentsdk.v3.pay.common.database.models.CountryToCurrency;
import com.bindo.paymentsdk.v3.pay.common.database.models.Currency;
import com.bindo.paymentsdk.v3.pay.common.database.models.LocalBIN;
import com.bindo.paymentsdk.v3.pay.common.database.models.PYCTable;
import com.bindo.paymentsdk.v3.pay.common.emv.enums.EMVTag;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.BatchTotals;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Request;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Response;
import com.bindo.paymentsdk.v3.pay.common.util.CurrencyUtil;
import com.bindo.paymentsdk.v3.pay.common.util.Hex;
import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.common.util.StringUtils;
import com.bindo.paymentsdk.v3.pay.common.util.TLVUtils;
import com.iso8583.Element;
import com.iso8583.ISO8583;
import com.iso8583.ISO8583Repo;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ISO8583Conversion {

    public static final String TAG = "ISO8583Conversion";
    public static final int LOCAL_CURRENCY_EXPONENT = 100;
    private static final char DECIMAL_SEPARATOR = new DecimalFormat().getDecimalFormatSymbols().getDecimalSeparator();

    public static ISO8583Repo requestToISO8583Repo(Request request) {
        ISO8583Repo iso8583Repo = ISO8583Repo.getInstance();
        iso8583Repo.initRepo();

        iso8583Repo.put(new Element(0, request.getMessageTypeId(), request.getMessageTypeId().length()));
        if (!StringUtils.isEmpty(request.getPrimaryAccountNumber())) {
            iso8583Repo.put(new Element(2, request.getPrimaryAccountNumber(), request.getPrimaryAccountNumber().length()));
        }

        if (!StringUtils.isEmpty(request.getProcessingCode())) {
            iso8583Repo.put(new Element(3, request.getProcessingCode(), request.getProcessingCode().length()));
        }

        {
            int amount = (int) (request.getTransactionAmount() * LOCAL_CURRENCY_EXPONENT);
            String transactionAmount = String.format("%012d", amount);
            iso8583Repo.put(new Element(4, transactionAmount, transactionAmount.length()));
        }

        if (request.getCardholderBillingAmount() != 0) {
            int amount = (int) (request.getCardholderBillingAmount() * getCurrencyExponent(request.getCardholderBillingCurrencyCode()));
            String billingAmount = String.format("%012d", amount);
            iso8583Repo.put(new Element(6, billingAmount, billingAmount.length()));
        }

        String STAN = String.format("%06d", request.getTransactionSystemTraceAuditNumber());
        iso8583Repo.put(new Element(11, STAN, STAN.length()));

        if (!StringUtils.isEmpty(request.getCardExpirationDate())) {
            iso8583Repo.put(new Element(14, request.getCardExpirationDate(), request.getCardExpirationDate().length()));
        }

        if (!StringUtils.isEmpty(request.getPOSEntryMode())) {
            iso8583Repo.put(new Element(22, request.getPOSEntryMode(), request.getPOSEntryMode().length()));
        }

        if (!StringUtils.isEmpty(request.getCardSequenceNumber())) {
            iso8583Repo.put(new Element(23, request.getCardSequenceNumber(), request.getCardSequenceNumber().length()));
        }

        if (!StringUtils.isEmpty(request.getNetworkInternationalIdentifier())) {
            iso8583Repo.put(new Element(24, request.getNetworkInternationalIdentifier(), request.getNetworkInternationalIdentifier().length()));
        }

        if (!StringUtils.isEmpty(request.getPOSConditionCode())) {
            iso8583Repo.put(new Element(25, request.getPOSConditionCode(), request.getPOSConditionCode().length()));
        }

        if (!StringUtils.isEmpty(request.getTrack2EquivalentData())) {
            iso8583Repo.put(new Element(35, request.getTrack2EquivalentData(), request.getTrack2EquivalentData().length()));
        }

        if (!StringUtils.isEmpty(request.getRrn())) {
            iso8583Repo.put(new Element(37, request.getRrn(), request.getRrn().length()));
        }

        if (!StringUtils.isEmpty(request.getTerminalIdentification())) {
            iso8583Repo.put(new Element(41, request.getTerminalIdentification(), request.getTerminalIdentification().length()));
        }

        if (!StringUtils.isEmpty(request.getMerchantIdentifier())) {
            iso8583Repo.put(new Element(42, request.getMerchantIdentifier(), request.getMerchantIdentifier().length()));
        }

        if (!StringUtils.isEmpty(request.getTrack1DiscretionaryData())) {
            iso8583Repo.put(new Element(45, request.getTrack1DiscretionaryData(), request.getTrack1DiscretionaryData().length()));
        }

        if (!StringUtils.isEmpty(request.getTransactionCurrencyCode())) {
            iso8583Repo.put(new Element(49, request.getTransactionCurrencyCode(), request.getTransactionCurrencyCode().length()));
        }

        if (!StringUtils.isEmpty(request.getCardholderBillingCurrencyCode())) {
            iso8583Repo.put(new Element(51, request.getCardholderBillingCurrencyCode(), request.getCardholderBillingCurrencyCode().length()));
        }

        String tipAmount = "";
        if (request.getTipAmount() != 0 || request.getCardholderBillingTipAmount() != 0) {
            // tip in local currency
            int amount = (int) (request.getTipAmount() * LOCAL_CURRENCY_EXPONENT);
            String sAmount = String.format("%012d", amount);
            tipAmount = tipAmount + sAmount;
        }
        if (request.getCardholderBillingTipAmount() != 0) {
            // tip in chd currency
            int amount = (int) (request.getCardholderBillingTipAmount() * getCurrencyExponent(request.getCardholderBillingCurrencyCode()));
            String sAmount = String.format("%012d", amount);
            tipAmount = tipAmount + sAmount;
        }

        if (!"".equals(tipAmount)) {
            iso8583Repo.put(new Element(54, tipAmount, tipAmount.length()));
        }

        if (request.getCardDetectMode() != null) {
            switch (request.getCardDetectMode()) {
                case CardDetectMode.CONTACT:
                case CardDetectMode.CONTACTLESS:
                    HashMap<String, byte[]> tagLengthValues = request.getTagLengthValues();
                    String[] tagSet = new String[]{
                            EMVTag.TRACK_2_EQUIVALENT_DATA.toString(),
                            EMVTag.APPLICATION_PRIMARY_ACCOUNT_NUMBER.toString(),
                            EMVTag.TRANSACTION_CURRENCY_CODE.toString(),
                            EMVTag.APPLICATION_PRIMARY_ACCOUNT_NUMBER_SEQUENCE_NUMBER.toString(),
                            EMVTag.APPLICATION_INTERCHANGE_PROFILE.toString(),
                            EMVTag.DEDICATED_FILE_NAME.toString(),
                            EMVTag.TERMINAL_VERIFICATION_RESULTS.toString(),
                            EMVTag.TRANSACTION_DATE.toString(),
                            EMVTag.TRANSACTION_STATUS_INFORMATION.toString(),
                            EMVTag.TRANSACTION_TYPE.toString(),
                            EMVTag.AMOUNT_AUTHORISED_NUMERIC.toString(),
                            EMVTag.AMOUNT_OTHER_NUMERIC.toString(),
                            EMVTag.APPLICATION_VERSION_NUMBER_ICC.toString(),
                            EMVTag.APPLICATION_VERSION_NUMBER_TERMINAL.toString(),
                            EMVTag.TERMINAL_COUNTRY_CODE.toString(),
                            EMVTag.INTERFACE_DEVICE_SERIAL_NUMBER.toString(),
                            EMVTag.APPLICATION_CRYPTOGRAM.toString(),
                            EMVTag.CRYPTOGRAM_INFORMATION_DATA.toString(),
                            EMVTag.TERMINAL_CAPABILITIES.toString(),
                            EMVTag.CARDHOLDER_VERIFICATION_METHOD_RESULTS.toString(),
                            EMVTag.TERMINAL_TYPE.toString(),
                            EMVTag.APPLICATION_TRANSACTION_COUNTER.toString(),
                            EMVTag.UNPREDICTABLE_NUMBER.toString(),
                            EMVTag.TRANSACTION_SEQUENCE_COUNTER.toString(),
                            EMVTag.ISSUER_APPLICATION_DATA.toString(),
                            EMVTag.TRANSACTION_CATEGORY_CODE.toString(),
                            EMVTag.FORM_FACTOR_INDICATOR.toString(),
                            EMVTag.ISSUER_COUNTRY_CODE.toString(),
                            EMVTag.APPLICATION_CURRENCY_CODE.toString(),
                            "9F63",
                            "9F62"};

                    StringBuilder iccdataBuilder = new StringBuilder();

                    for (String tag : tagSet) {
                        byte[] value = tagLengthValues.get(tag);

                        if (value != null) {
                            iccdataBuilder.append(tag);
                            iccdataBuilder.append(String.format("%02X", value.length));
                            iccdataBuilder.append(Hex.encode(value));
                        }else{
                            Log.e(TAG, "requestToISO8583Repo: tag:"+tag +"is null" );
                        }
                    }

                    iso8583Repo.put(new Element(55, iccdataBuilder.toString(), iccdataBuilder.length()));
                    break;
            }
        }

        if (request.getBatchNO() != 0) {
            iso8583Repo.put(new Element(60, String.format("%06d", request.getBatchNO()), 6));
        }

        String DE63="";

        if (!StringUtils.isEmpty(request.getPlanetPaymentField63CurrencyConversionIndicator())){
            DE63 += "0003" + "3132" + request.getPlanetPaymentField63CurrencyConversionIndicator();
        }

        if (!StringUtils.isEmpty(request.getPlanetPaymentField63OptOutFlag()))        {
            DE63 += "0003" + "3133" + request.getPlanetPaymentField63OptOutFlag();
        }

        if (!StringUtils.isEmpty(request.getPlanetPaymentField63RateLookupIndicator()))        {
            DE63 += "0003" + "3237" + request.getPlanetPaymentField63RateLookupIndicator();
        }

        if (!StringUtils.isEmpty(request.getPlanetPaymentField63PSCIndicator())){
            DE63 += "0003" +"3238" + request.getPlanetPaymentField63PSCIndicator();
        }

        if (!StringUtils.isEmpty(request.getPlanetPaymentField63RequestForCurrencyTableDownload()))        {
            DE63 += "0003" + "3330" + request.getPlanetPaymentField63RequestForCurrencyTableDownload();
        }

        if (!StringUtils.isEmpty(request.getPlanetPaymentField63RequestForReportDownload()))        {
            DE63 += "0005" + "3034" + request.getPlanetPaymentField63RequestForReportDownload();
        }

        if (!StringUtils.isEmpty(request.getPlanetPaymentField63RequestForLocalBINTableDownload()))        {
            DE63 += "0012" + "3031" + request.getPlanetPaymentField63RequestForLocalBINTableDownload();
        }

        if (!StringUtils.isEmpty(request.getPlanetPaymentField63RequestForAdditionalResponse()))        {
            DE63 += "0003" + "3333" + request.getPlanetPaymentField63RequestForAdditionalResponse();
        }

        if (!StringUtils.isEmpty(request.getPlanetPaymentField63RequestForPYCTableDownload())){
            DE63 += "0011" +"3232" + request.getPlanetPaymentField63RequestForPYCTableDownload();
        }

        if (request.getBatchTotals() != null){
            BatchTotals batchTotals = request.getBatchTotals();
            int trxCount = batchTotals.getAuthorizeRefundCount()
                    + batchTotals.getAuthorizeSalesCount()
                    + batchTotals.getCapturedRefundCount()
                    + batchTotals.getCapturedSalesCount()
                    + batchTotals.getDebitRefundCount()
                    + batchTotals.getDebitSalesCount();
            int trxAmount = batchTotals.getAuthorizeRefundAmount()
                    + batchTotals.getAuthorizeSalesAmount()
                    + batchTotals.getCapturedRefundAmount()
                    + batchTotals.getCapturedSalesAmount()
                    + batchTotals.getDebitRefundAmount()
                    + batchTotals.getDebitSalesAmount();
            int refundCount = batchTotals.getAuthorizeRefundCount()
                    + batchTotals.getCapturedRefundCount()
                    + batchTotals.getDebitRefundCount();
            int refundAmount = batchTotals.getAuthorizeRefundAmount()
                    + batchTotals.getCapturedRefundAmount()
                    + batchTotals.getDebitRefundAmount();
            String batchTotalsString = String.format("%03d%012d%03d%012d%03d%03d%054d",
                trxCount, trxAmount, refundCount, refundAmount, 0, 0, 0);
            DE63 += "0092" +"3930" + batchTotalsString;
        }

        if (!StringUtils.isEmpty(DE63)) {
            iso8583Repo.put(new Element(63, DE63, DE63.length()));
        }

        return iso8583Repo;
    }

    private static long getCurrencyExponent(String currCode) {
        int exponent;
        try {
            exponent = CurrencyUtil.getCurrencyExponent(currCode);
        } catch (Exception e) {
            exponent = LOCAL_CURRENCY_EXPONENT;
        }
        return Math.round(Math.pow(10, exponent));
    }

    public static Response iso8583RepoToResponse(ISO8583Repo iso8583Repo) {
        return null;
    }

    public static byte[] requestToISO8583RepoBytes(Request request) {


//        int[] fieldIds = null;
//
//
//        switch (request.getTransactionType()) {
//
//            case AUTHORIZATION:
//
//                switch (request.getCardDetectMode()) {
//                    case CardDetectMode.CONTACT:
//                    case CardDetectMode.CONTACTLESS:
//                        fieldIds = new int[]{0, 3, 4, 11, 22, 24, 25, 35, 41, 42, 49, 51, 55, 63};
//                        break;
//                    case CardDetectMode.SWIPE:
//                        fieldIds = new int[]{0, 3, 4, 11, 22, 24, 25, 35, 41, 42, 49, 51, 63};
//                        break;
//                    case CardDetectMode.MANUAL:
//                        fieldIds = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 41, 42, 49, 51, 55, 63};
//                        break;
//                }
//
//                break;
//            case AUTHORIZATION_VOID:
//
//                switch (request.getCardDetectMode()) {
//                    case CardDetectMode.CONTACT:
//                    case CardDetectMode.CONTACTLESS:
//                        fieldIds = new int[]{0, 3, 4, 11, 22, 24, 25, 35, 41, 42, 49, 51, 55, 63};
//                        break;
//                    case CardDetectMode.SWIPE:
//                        fieldIds = new int[]{0, 3, 4, 11, 22, 24, 25, 35, 41, 42, 49, 51, 63};
//                        break;
//                    case CardDetectMode.MANUAL:
//                        fieldIds = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 41, 42, 48, 49, 51, 55, 63};
//                        break;
//                }
//
//                break;
//            case DCC_RATE_LOOKUP:
//                switch (request.getCardDetectMode()) {
//                    case CardDetectMode.CONTACT:
//                    case CardDetectMode.CONTACTLESS:
//                        fieldIds = new int[]{0, 3, 4, 11, 22, 24, 25, 35, 41, 42, 49, 51, 55, 63};
//                        break;
//                    case CardDetectMode.SWIPE:
//                        fieldIds = new int[]{0, 3, 4, 11, 22, 24, 25, 35, 41, 42, 49, 51, 63};
//                        break;
//                    case CardDetectMode.MANUAL:
//                        fieldIds = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 41, 42, 48, 49, 51, 55, 63};
//                        break;
//                }
//                break;
//            case REFUND:
//                switch (request.getCardDetectMode()) {
//                    case CardDetectMode.CONTACT:
//                    case CardDetectMode.CONTACTLESS:
//                        fieldIds = new int[]{0, 3, 4, 11, 22, 24, 25, 35, 41, 42, 49, 51, 55, 63};
//                        break;
//                    case CardDetectMode.SWIPE:
//                        fieldIds = new int[]{0, 3, 4, 11, 22, 24, 25, 35, 41, 42, 49, 51, 63};
//                        break;
//                    case CardDetectMode.MANUAL:
//                        fieldIds = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 41, 42, 48, 49, 51, 55, 63};
//                        break;
//                }
//                break;
//            case REFUND_VOID:
//                switch (request.getCardDetectMode()) {
//                    case CardDetectMode.CONTACT:
//                    case CardDetectMode.CONTACTLESS:
//                        fieldIds = new int[]{0, 3, 4, 11, 22, 24, 25, 35, 41, 42, 49, 51, 55, 63};
//                        break;
//                    case CardDetectMode.SWIPE:
//                        fieldIds = new int[]{0, 3, 4, 11, 22, 24, 25, 35, 41, 42, 49, 51, 63};
//                        break;
//                    case CardDetectMode.MANUAL:
//                        fieldIds = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 41, 42, 48, 49, 51, 55, 63};
//                        break;
//                }
//                break;
//            case REVERSAL:
//                fieldIds = new int[] {-2, -1, 0};
//                break;
//            case SETTLEMENT:
//                fieldIds = new int[] {0, 3, 11, 24, 41, 42, 60, 62, 63};
//                break;
//            case BATCH_UPLOAD:
//                fieldIds = new int[] {-2, -1, 0};
//                break;
//            case PRE_AUTHORIZATION:
//                fieldIds = new int[] {-2, -1, 0};
//                break;
//            case PRE_AUTHORIZATION_VOID:
//                fieldIds = new int[] {-2, -1, 0};
//                break;
//            case PRE_AUTHORIZATION_COMPLETION:
//                fieldIds = new int[] {-2, -1, 0};
//                break;
//        }

        ISO8583Repo iso8583Repo = ISO8583Conversion.requestToISO8583Repo(request);

        ISO8583 iso8583 = new ISO8583() {
            @Override
            public int onGenMac(byte[] msg, int offset, int len, byte index, byte[] mac) {
                return 0;
            }
        };

        byte[] iso8583Msg = new byte[1024];
        int ret = iso8583.generateISO8583Msg(iso8583Msg, (byte) 0x00, request.getFieldIdSet());

        if (ret <= 0)
        {
            Log.e(TAG, "generateISO8583Msg failed:"+ret);
            return null;
        }

        byte[] finalMsg = new byte[ret];
        System.arraycopy(iso8583Msg, 0, finalMsg, 0, ret);
        return finalMsg;
    }

    static List<LocalBIN> localBINs;
    static List<Currency> currencies;
    static List<CountryToCurrency> countryToCurrencies;
    static List<PYCTable> PYCTables;

    public static Response iso8583RepoBytesToResponse(byte[] iso8583RepoBytes) {

        if (iso8583RepoBytes == null)
        {
            return null;
        }

        ISO8583 iso8583 = new ISO8583() {
            @Override
            public int onGenMac(byte[] msg, int offset, int len, byte index, byte[] mac) {
                return 0;
            }
        };

        iso8583.parseISO8583Msg(iso8583RepoBytes, 5+2, 8);
        ISO8583Repo iso8583Repo = ISO8583Repo.getInstance();
        final Response response = new Response();

        Element element;

        element = iso8583Repo.getElement(3);

        if (element != null) {
            response.setTransactionProcessingCode(element.getValue());
        }

        element = iso8583Repo.getElement(4);

        if (element != null) {
            response.setTransactionAmount(Double.parseDouble(element.getValue())/LOCAL_CURRENCY_EXPONENT);
        }

        element = iso8583Repo.getElement(10);

        if (element != null) {
            int exponent = Integer.parseInt(element.getValue().substring(0, 1));
            response.setConversionRate(Double.parseDouble(element.getValue().substring(1)) / Math.pow(10, exponent));
        }

        element = iso8583Repo.getElement(11);

        if (element != null) {
            response.setTransactionSystemTraceAuditNumber(Integer.parseInt(element.getValue()));
        }

        element = iso8583Repo.getElement(12);

        if (element != null) {
            response.setTransactionTime(element.getValue());
        }

        element = iso8583Repo.getElement(13);

        if (element != null) {
            response.setTransactionDate(element.getValue());
        }

        element = iso8583Repo.getElement(37);

        if (element != null) {
            response.setRetrievalReferenceNumber(element.getValue());
        }

        element = iso8583Repo.getElement(38);

        if (element != null) {
            response.setAuthorizationCode(element.getValue());
        }

        element = iso8583Repo.getElement(39);

        if (element != null) {
            response.setActionCode(element.getValue());
            if (response.getActionCode().equals("00"))
            {
                response.setApproved(true);
            }
        }

        element = iso8583Repo.getElement(51);

        if (element != null) {
            response.setCardholderBillingCurrencyCode(element.getValue().substring(1));
        }

        element = iso8583Repo.getElement(6);

        if (element != null) {
            response.setCardholderBillingAmount(Double.parseDouble(element.getValue())/getCurrencyExponent(response.getCardholderBillingCurrencyCode()));
        }

        element = iso8583Repo.getElement(54);

        if (element != null) {
            response.setTipAmount(Double.parseDouble(element.getValue().substring(0, 12))/LOCAL_CURRENCY_EXPONENT);
            if (element.getValue().length() == 24) {
                response.setCardholderBillingTipAmount(Double.parseDouble(element.getValue().substring(12, 24))/getCurrencyExponent(response.getCardholderBillingCurrencyCode()));
            }
        }

        element = iso8583Repo.getElement(55);

        if (element != null) {
            byte[] rspDE55 = Hex.decode(element.getValue());
            response.setIccRelatedData(element.getValue());
            TLVUtils.parseTLVString(rspDE55, rspDE55.length, true, new TLVUtils.OnDecodeTlvObjectListner() {
                @Override
                public void onDecode(int tag, int len, byte[] value) {
                    switch (tag) {
                        case 0x91:
                            response.setIccRelatedDataIssuerAuthenticationData(Hex.encode(value));
                            break;
                        case 0x71:
                            response.setIccRelatedDataIssuerScriptData("71" + String.format("%02X", len) + Hex.encode(value));
                            break;
                        case 0x72:
                            response.setIccRelatedDataIssuerScriptData(response.getIccRelatedDataIssuerScriptData() + "72" + String.format("%02X", len) + Hex.encode(value));
                            break;
                    }
                }
            });

        }

        element = iso8583Repo.getElement(63);

        if (element != null) {
            localBINs = new ArrayList<>();
            currencies = new ArrayList<>();
            PYCTables = new ArrayList<>();

            LTVDecoder.ParseLTVFormatedData(element.getValue(), new LTVDecoder.OnDecodeEvent() {
                @Override
                public void onFectchTag(String tag, String value) {
                    System.out.println("tag:" + tag + " value:" + value+"-"+ new String(Hex.decode(value)));
                    switch (tag) {
                        case "3033":
                            LocalBIN dbLocalBIN = new LocalBIN();
                            dbLocalBIN.setHighPrefixNumber(value.substring(0, 10));
                            dbLocalBIN.setLowPrefixNumber(value.substring(10, 20));
                            dbLocalBIN.setLocalCurrencyCode(value.substring(20, value.length()));
                            localBINs.add(dbLocalBIN);
                            break;
                        case "3032":
                            response.setLastLocalBINTable(value.substring(0, 8));
                            response.setLastLocalBINTableIndex(value.substring(8));
                            break;
                        case "3233":
                            response.setLastLocalPYCTableDate(value.substring(0, 8));
                            response.setLastLocalPYCTableIndex(value.substring(8));
                            break;
                        case "3234":
                            PYCTable pyc = new PYCTable();
                            pyc.setHighPrefixNumber(value.substring(0, 10));
                            pyc.setLowPrefixNumber(value.substring(10, 20));
                            pyc.setStatus(value.substring(20, value.length()));
                            PYCTables.add(pyc);
                        case "3035":
                            response.setLastPageNumber(value);
                            break;
                        case "3036":
                            response.setPrintText(value);
                            break;
                        case "3331":
                            response.setNextEntryToSend(value);
                            break;
                        case "3332":
                            Currency currency = new Currency();
                            int offset = 0;
                            currency.setEntryNumber(value.substring(offset, offset + 2));
                            offset += 2;
                            currency.setCurrencyName(new String(Hex.decode(value.substring(offset, offset + 40))));
                            offset += 40;
                            currency.setNumericCode(new String(Hex.decode(value.substring(offset, offset + 6))));
                            offset += 6;
                            currency.setAlphabeticCode(new String(Hex.decode(value.substring(offset, offset + 6))));
                            offset += 6;
                            System.out.println("terminal symbol:" + value.substring(offset, offset + 2)
                                    + "sym:" + new String(Hex.decode(value.substring(offset, offset + 2))));

                            if (value.substring(offset, offset + 2).equalsIgnoreCase("A2")) {
                                currency.setTerminalSymbol("￠");
                            } else if (value.substring(offset, offset + 2).equalsIgnoreCase("A5")) {
                                currency.setTerminalSymbol("￥");
                            } else {
                                currency.setTerminalSymbol(new String(Hex.decode(value.substring(offset, offset + 2))));
                            }


                            offset += 2;
                            currency.setDecimalPlaces(value.substring(offset, offset + 2));
                            offset += 2;
                            currency.setIgnoreDigits(value.substring(offset, offset + 2));
                            offset += 2;
                            currency.setConversionRate(value.substring(offset, offset + 12));
                            offset += 12;
                            currency.setRateExponent(value.substring(offset, offset + 2));
                            offset += 2;
                            currency.setDecimalSymbol(new String(Hex.decode(value.substring(offset, offset + 2))));
                            offset += 2;
                            currency.setSeparatorSymbol(new String(Hex.decode(value.substring(offset, offset + 2))));
                            offset += 2;
                            currency.setRoundUnit(value.substring(offset, offset + 2));
                            offset += 2;
                            currency.setCurrOpt(value.substring(offset, offset + 2));
                            offset += 2;
                            currency.setMenuName(new String(Hex.decode(value.substring(offset))));
                            System.out.println("currency:" + currency.toString());
                            currencies.add(currency);
                            break;
                        case "3334":
                            response.setTransactionPYCRateMarkupSign(new String(Hex.decode(value.substring(0, 2))));
                            int sign = 1;
                            if (response.getTransactionPYCRateMarkupSign().equals("-")) {
                                sign = -1;
                            }
                            response.setTransactionPYCRateMarkup(sign * Double.parseDouble(value.substring(2, 4)
                                    + DECIMAL_SEPARATOR
                                    + value.substring(4, 6)));
                            break;
                        case "3337":
                            response.setTransactionPYCRateMarkupText(new String(Hex.decode(value)));
                            break;
                        case "3339":

                            break;
                        case "3630":
                            response.setIccAuthorizationResponseCryptogram(new String(Hex.decode(value)));
                            break;
                        case "3236":
                            Log.d("LOCAL BIN TABLE DOWNLOAD REQUIRED: " + value);
                            response.setIsLocalPYCBinTableDownloadRequired("Y".equals(value));
                            break;
                    }

                }
            });

            response.setPlanetPaymentLocalBINs(localBINs);
            response.setPlanetPaymentCurrencies(currencies);
            response.setPlanetPaymentPYCTable(PYCTables);
        }

        return response;
    }
}
