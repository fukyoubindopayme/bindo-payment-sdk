package com.bindo.paymentsdk.v3.pay.gateway.planetpayment.enums;

public enum OptOutFlag {
    YES('Y'),
    NO('N');

    private char value;

    OptOutFlag(char v) {
        value = v;
    }

    public char getValue() {
        return value;
    }
}
