package com.bindo.paymentsdk.v3.pay.gateway.planetpayment.enums;

public enum PSC {
    P_CARDHOLDER_SELECTED('P'),
    E_EMV_DERIVED('E');

    private char value;

    PSC(char v) {
        value = v;
    }

    public char getValue() {
        return value;
    }
}
