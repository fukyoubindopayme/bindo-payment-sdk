package com.bindo.paymentsdk.v3.pay.gateway.planetpayment.enums;

public enum CCI {
    D_PYC_PROCESSING('D'),
    M_MULTICURRENCY_PRICING('M'),
    X_DOMESTIC_PROCESSING('X'),
    L_LIKE_TO_LIKE_PROCESSING('L');

    private char value;

    CCI(char v) {
        value = v;
    }

    public char getValue() {
        return value;
    }
}

