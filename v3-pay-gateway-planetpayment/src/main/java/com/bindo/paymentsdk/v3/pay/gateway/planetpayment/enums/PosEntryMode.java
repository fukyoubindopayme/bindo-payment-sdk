package com.bindo.paymentsdk.v3.pay.gateway.planetpayment.enums;

public enum PosEntryMode {

    UNKNOWN("00"),
    KEYPAD("01"),
    MAGSTRIPE_READER("02"),
    BARCODE_READER("03"),
    OPTICAL_READER("04"),
    CHIP_READER("05"),
    CONTACTLESS("07"),
    KEYPAD_CHIP_CAPABLE("79"),
    MAGSTRIPE_CHIP_CAPABLE("80"),
    MAGSTRIPE_ANY("90"),
    CONTACTLESS_MAGSTRIPE("91"),
    CHIP_CVV_UNRELIABLE("95"),;

    private String code;

    PosEntryMode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return code;
    }
}
