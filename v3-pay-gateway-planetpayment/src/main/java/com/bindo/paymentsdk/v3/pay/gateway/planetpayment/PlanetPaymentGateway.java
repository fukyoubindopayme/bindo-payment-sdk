package com.bindo.paymentsdk.v3.pay.gateway.planetpayment;

import com.bindo.paymentsdk.v3.pay.common.CardDetectMode;
import com.bindo.paymentsdk.v3.pay.common.gateway.SocketService;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Request;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Response;
import com.bindo.paymentsdk.v3.pay.common.util.Hex;
import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.common.util.StringUtils;
import com.bindo.paymentsdk.v3.pay.gateway.Gateway;
import com.bindo.paymentsdk.v3.pay.gateway.planetpayment.enums.CCI;
import com.bindo.paymentsdk.v3.pay.gateway.planetpayment.enums.OptOutFlag;
import com.bindo.paymentsdk.v3.pay.gateway.planetpayment.enums.PinEntryCapability;
import com.bindo.paymentsdk.v3.pay.gateway.planetpayment.enums.PosEntryMode;
import com.iso8583.Element;
import com.iso8583.ISO8583Repo;

import java.io.IOException;
import java.net.SocketException;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.SSLException;

public class PlanetPaymentGateway implements Gateway {

    private final String TAG = "PlanetPaymentGateway";

    // Gateway 请求参数
    public static final String CONFIG_REQUEST_CARD_ACCEPTOR_TERMINAL_ID = "config_request_card_acceptor_terminal_id";
    public static final String CONFIG_REQUEST_CARD_ACCEPTOR_ID_CODE     = "config_request_card_acceptor_id_code";
    public static final String CONFIG_REQUEST_TPDU                      = "config_request_tpdu";
    public static final String CONFIG_NETWORK_INTERNATIONAL_ID          = "config_network_id";
    public static final String CONFIG_HOST_URL                          = "config_host_url";
    public static final String CONFIG_HOST_PORT                         = "config_host_port";
    public static final String CONFIG_REQUEST_KEYSTORE_TERMINAL         = SocketService.KEYSTORE_TERMINAL;
    public static final String CONFIG_REQUEST_KEYSTORE_TRUSTED          = SocketService.KEYSTORE_TRUSTED;

    //private static final String DEFAULT_REQUEST_CARD_ACCEPTOR_TERMINAL_ID   = "82831111";
    private static final String DEFAULT_REQUEST_CARD_ACCEPTOR_TERMINAL_ID   = "82832222";
    private static final String DEFAULT_REQUEST_CARD_ACCEPTOR_ID_CODE       = "001711112222   ";
    private static final String DEFAULT_CONFIG_REQUEST_TPDU                 = "60005281B6";
    private static final String DEFAULT_NETWORK_INTERNATIONAL_ID            = "052";
    private static final String DEFAULT_CONFIG_HOST_URL                     = "terminal.uat.planetpayment.com";
    private static final String DEFAULT_CONFIG_HOST_PORT                    = "30112";

    private HashMap<String, Object> mConfigMap = new HashMap<>();

    private SocketService mSocketService = new SocketService();

    public PlanetPaymentGateway(HashMap<String, Object> configMap) {
        // Gateway 参数默认值
        mConfigMap.put(CONFIG_REQUEST_CARD_ACCEPTOR_TERMINAL_ID, DEFAULT_REQUEST_CARD_ACCEPTOR_TERMINAL_ID);
        mConfigMap.put(CONFIG_REQUEST_CARD_ACCEPTOR_ID_CODE, DEFAULT_REQUEST_CARD_ACCEPTOR_ID_CODE);
        mConfigMap.put(CONFIG_REQUEST_TPDU, DEFAULT_CONFIG_REQUEST_TPDU);
        mConfigMap.put(CONFIG_NETWORK_INTERNATIONAL_ID, DEFAULT_NETWORK_INTERNATIONAL_ID);
        mConfigMap.put(CONFIG_HOST_URL, DEFAULT_CONFIG_HOST_URL);
        mConfigMap.put(CONFIG_HOST_PORT, DEFAULT_CONFIG_HOST_PORT);

        if (configMap == null) {
            Log.w(TAG, "Please set the gateway parameters");
        } else {
            for (String key : configMap.keySet()) {
                if (mConfigMap.containsKey(key)){
                    continue;
                }
                Object value = configMap.get(key);
                mConfigMap.put(key, value);
            }
        }
    }

    @Override
    public Response sendRequest(Request request) {

        int [] fieldSet = new int[] {0,2,3,4,6,11,14,22,23,24,25,35,37,41,42,49,51,54,55,63};

        if (request.getCardDetectMode() != null) {
            PosEntryMode posEntryMode;

            switch (request.getCardDetectMode()) {
                case CardDetectMode.CONTACT:
                    fieldSet = new int[] {0,2,3,4,6,11,22,23,24,25,35,37,41,42,49,51,54,55,63};
                    posEntryMode = PosEntryMode.CHIP_READER;
                    break;
                case CardDetectMode.CONTACTLESS:
                    posEntryMode = PosEntryMode.CONTACTLESS;
                    fieldSet = new int[] {0,2,3,4,6,11,22,23,24,25,35,37,41,42,49,51,54,55,63};
                    break;
                case CardDetectMode.SWIPE:
                    posEntryMode = PosEntryMode.MAGSTRIPE_READER;
                    fieldSet = new int[] {0,2,3,4,6,11,22,23,24,25,35,37,41,42,49,51,54,63};
                    break;
                case CardDetectMode.FALLBACK_SWIPE:
                    posEntryMode = PosEntryMode.MAGSTRIPE_READER;
                    fieldSet = new int[] {0,2,3,4,6,11,22,23,24,25,35,37,41,42,49,51,54,63};
                    break;
                case CardDetectMode.MANUAL:
                    fieldSet = new int[] {0,2,3,4,6,11, 14, 22,23,24,25,37,41,42,49,51,54,63};
                    posEntryMode = PosEntryMode.KEYPAD;
                    break;
                default:
                    posEntryMode = PosEntryMode.UNKNOWN;
            }

            PinEntryCapability pinEntryCapability = PinEntryCapability.NO_PIN_ENTRY_CAPABILITY;
            request.setPOSEntryMode(posEntryMode.getCode() + pinEntryCapability.getCode());
        }

        if (request.getTransactionType() != null) {
            switch (request.getTransactionType()) {
                case AUTHORIZATION:
                case SALE:
                    request.setMessageTypeId("0200");
                    request.setProcessingCode("000000");
                    break;
                case AUTHORIZATION_VOID:
                case REVERSAL:
                    request.setMessageTypeId("0200");
                    request.setProcessingCode("020000");
                    break;
                case REFUND:
                    request.setMessageTypeId("0200");
                    request.setProcessingCode("200000");
                    break;
                case AUTHORIZATION_TIMEOUT:
                    request.setMessageTypeId("0400");
                    request.setProcessingCode("000000");
                    break;
                case AUTHORIZATION_ADVICE:
                    request.setMessageTypeId("0420");
                    request.setProcessingCode("000000");
                    break;
                case REFUND_VOID:
                    request.setMessageTypeId("0200");
                    request.setProcessingCode("220000");
                    break;
                case PRE_AUTHORIZATION:
                case RATE_LOOKUP:
                    request.setMessageTypeId("0100");
                    request.setProcessingCode("000000");
                    break;
                case SETTLEMENT:
                    fieldSet = new int[]{0, 3, 11, 24, 41, 42, 60, 62, 63};
                    request.setMessageTypeId("0500");
                    request.setProcessingCode("920000");
                    request.setPlanetPaymentField63RateLookupIndicator("R");
                    break;
                case OFFLINE_SALE:
                    request.setMessageTypeId("0220");
                    request.setProcessingCode("000000");
                    break;
                case LOCAL_BIN_TABLE_DOWNLOAD:
                    request.setMessageTypeId("0900");
                    request.setProcessingCode("000000");
                    fieldSet = new int[]{0, 3, 11, 24, 41, 42, 63};
                    break;
                case CURRENCY_TABLE_DOWNLOAD:
                    request.setMessageTypeId("0900");
                    request.setProcessingCode("040000");
                    fieldSet = new int[]{0, 3, 11, 24, 41, 42, 63};
                    break;
                case PYC_TABLE_DOWNLOAD:
                    request.setMessageTypeId("0900");
                    request.setProcessingCode("030000");
                    fieldSet = new int[]{0, 3, 11, 24, 41, 42, 63};
                    break;
                default:
                    Log.e("not support transaction type !");

            }
            switch (request.getTransactionType()) {
                case AUTHORIZATION:
                case SALE:
                case REFUND:
                case AUTHORIZATION_VOID:
                case AUTHORIZATION_TIMEOUT:
                case REVERSAL:
                    request.setPlanetPaymentField63CurrencyConversionIndicator(String.format("%02X", (int) CCI.D_PYC_PROCESSING.getValue()));
                    request.setPlanetPaymentField63RateLookupIndicator("");
                    request.setPlanetPaymentField63OptOutFlag(String.format("%02X", (int) OptOutFlag.NO.getValue()));
                    request.setPlanetPaymentField63RequestForAdditionalResponse("FF");
                    break;
                case PRE_AUTHORIZATION:
                case RATE_LOOKUP:
                    request.setPlanetPaymentField63CurrencyConversionIndicator(String.format("%02X", (int) CCI.D_PYC_PROCESSING.getValue()));
                    request.setPlanetPaymentField63RateLookupIndicator(String.format("%02X", (int) 'R'));
                    request.setPlanetPaymentField63RequestForAdditionalResponse("FF");
                    break;
                case LOCAL_BIN_TABLE_DOWNLOAD:
                    break;
                case SETTLEMENT:
                    break;
                default:
                    Log.e(TAG, "TRANSACTION TYPE " + request.getTransactionType() + " NOT SUPPORTED");
            }
        }

        Response response = null;
        List<Response.Error> errors = new ArrayList<>();
        request.setPOSConditionCode("00");
        request.setNetworkInternationalIdentifier((String) mConfigMap.get(CONFIG_NETWORK_INTERNATIONAL_ID));
        request.setTerminalIdentification((String) mConfigMap.get(CONFIG_REQUEST_CARD_ACCEPTOR_TERMINAL_ID));
        request.setMerchantIdentifier((String) mConfigMap.get(CONFIG_REQUEST_CARD_ACCEPTOR_ID_CODE));

        if(request.getFieldIdSet() == null)
        {
            request.setFieldIdSet(fieldSet);
        }

        String t2 = request.getTrack2EquivalentData();
        if (t2 != null) {
            if (t2.endsWith("F")) {
                t2 = t2.substring(0, t2.length() - 1);
            }
            request.setTrack2EquivalentData(t2);
        }

        byte[] requestForISO8583RepoBytes = ISO8583Conversion.requestToISO8583RepoBytes(request);

        Log.d("org iso8583Msg:"+Hex.encode(requestForISO8583RepoBytes));

        byte[] finalISO8583Message = new byte[requestForISO8583RepoBytes.length + 5];
        int offset = 0;
        finalISO8583Message[offset] = (byte) ((finalISO8583Message.length-2) / 256);
        offset += 1;
        finalISO8583Message[offset] = (byte) ((finalISO8583Message.length-2) % 256);
        offset += 1;

        String tpdu;

        if (mConfigMap.containsKey(CONFIG_REQUEST_TPDU))
        {
            tpdu = (String) mConfigMap.get(CONFIG_REQUEST_TPDU);
            if (!StringUtils.isEmpty(tpdu))
            {
                System.arraycopy(Hex.decode(tpdu), 0, finalISO8583Message, offset, Hex.decode(tpdu).length);
                offset += Hex.decode(tpdu).length;
            }
        }

        if (requestForISO8583RepoBytes != null)
        {
            System.arraycopy(requestForISO8583RepoBytes, 2, finalISO8583Message, offset, requestForISO8583RepoBytes.length - 2);
        }

        Log.d("REQ ICC: " + request.getIccRelatedData());
        Log.d("final iso8583Msg:"+Hex.encode(finalISO8583Message));

        try {
            HashMap<String, KeyStore> keyStoreMap = new HashMap<>();
            keyStoreMap.put(SocketService.KEYSTORE_TERMINAL, (KeyStore) mConfigMap.get(CONFIG_REQUEST_KEYSTORE_TERMINAL));
            keyStoreMap.put(SocketService.KEYSTORE_TRUSTED, (KeyStore) mConfigMap.get(CONFIG_REQUEST_KEYSTORE_TRUSTED));
            mSocketService.setKeyStoreMap(keyStoreMap);
            mSocketService.setHostIp((String)mConfigMap.get(CONFIG_HOST_URL));
            mSocketService.setHostPort((String)mConfigMap.get(CONFIG_HOST_PORT));
            mSocketService.connect();

            if (mSocketService.isConnected()) {
                Log.d("SEND:");
                Log.d(request.toString());
                mSocketService.sendMessage(finalISO8583Message);
                byte[] receivedDataBytes = mSocketService.receiveMessage();
                response = ISO8583Conversion.iso8583RepoBytesToResponse(receivedDataBytes);
                Log.d("RECV:" + Hex.encode(receivedDataBytes));
                Log.d(response.toString());
                ISO8583Repo iso8583Repo = ISO8583Repo.getInstance();
                Element element = iso8583Repo.getElement(39);
                if (element != null) {
                    Log.d("RC: " + element.getValue());
                }
                element = iso8583Repo.getElement(55);
                if (element != null) {
                    Log.d("ICC: " + element.getValue());
                    Log.d("ICC: " + response.getIccRelatedData());
                    Log.d("ICC: " + response.getIccRelatedDataIssuerAuthenticationData());
                    Log.d("ICC: " + response.getIccRelatedDataIssuerScriptData());
                }
            }
        } catch (SSLException e) {
            // TODO: Handle ssl exception
            e.printStackTrace();
            errors.add(new Response.Error("-1", e.getMessage()));
        } catch (SocketException e) {
            // TODO: Handle socket exception
            e.printStackTrace();
            errors.add(new Response.Error("-1", e.getMessage()));
        } catch (Exception e) {
            // TODO: Handle socket exception
            e.printStackTrace();
            errors.add(new Response.Error("-1", e.getMessage()));
        } finally {
            try {
                mSocketService.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response == null) {
                response = new Response();
                response.setErrors(errors);
            }
        }
        return response;
    }
}
