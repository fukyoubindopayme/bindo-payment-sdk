package com.bindo.paymentsdk.v3.pay.gateway.planetpayment.enums;

public enum PinEntryCapability {

    PIN_ENTRY_CAPABILITY("1"),
    NO_PIN_ENTRY_CAPABILITY("2"),
    PIN_PAD_INOPERATIVE("8"),
    RESERVED("9"),
    UNKNOWN("0");

    private String code;

    PinEntryCapability(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return code;
    }

    public String getCode() {
        return code;
    }
}
