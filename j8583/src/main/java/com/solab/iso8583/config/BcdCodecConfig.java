package com.solab.iso8583.config;

import com.solab.iso8583.config.ConfigTypeDef.*;
/**
 * This Class BcdCodecConfig is used for bcd encode & decode.
 * The user should choose the right one config.
 */
public class BcdCodecConfig {

    private BcdFieldLenType mBcdFieldLenType;
    private FillPosition mFillPosition;
    private FillByteType mFillByteType;

    /**
     *
     * @param bcdFieldLenType BCD域的长度类型 {@link BcdFieldLenType}
     * @param contentFillPosition BCD域内容补位位置类型 {@link FillPosition}
     * @param contentFillByteType BCD域内容补位字节类型 {@link FillByteType}
     */
    public BcdCodecConfig(BcdFieldLenType bcdFieldLenType, FillPosition contentFillPosition, FillByteType contentFillByteType) {
        if(bcdFieldLenType == null) {
            mBcdFieldLenType = BcdFieldLenType.HEX_BYTE_COUNT;
        }else {
            mBcdFieldLenType = bcdFieldLenType;
        }
        if(contentFillPosition == null) {
            mFillPosition = FillPosition.RIGHT;
        }else {
            mFillPosition = contentFillPosition;
        }
        if(contentFillByteType == null) {
            mFillByteType = FillByteType.BYTE_F;
        }else {
            mFillByteType = contentFillByteType;
        }
    }

    public FillPosition getFillPosition() {
        return mFillPosition;
    }

    public FillByteType getFillByteType() {
        return mFillByteType;
    }

    public BcdFieldLenType getBcdFieldLenType() {
        return mBcdFieldLenType;
    }
}
