package com.solab.iso8583.config;

/**
 * 配置的类型定义
 */
public class ConfigTypeDef {

    // BCD相关
    /**
     * BCD 补码位置类型
     */
    public enum FillPosition{
        /**
         * 左补
         */
        LEFT,
        /**
         * 右补
         */
        RIGHT,
    }

    /**
     * 补码字节内容类型
     */
    public enum FillByteType{
        /**
         * 0x0
         */
        BYTE_0,
        /**
         * 0xF
         */
        BYTE_F,
    }

    /**
     * Bcd域的长度类型
     */
    public enum BcdFieldLenType {
        /**
         * 16进制字节数
         */
        HEX_BYTE_COUNT,
        /**
         * BCD码的个数
         */
        BCD_NUM_COUNT
    }

    // Other
}
