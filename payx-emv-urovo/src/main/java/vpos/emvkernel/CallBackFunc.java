package vpos.emvkernel;

import android.device.IccManager;
import android.device.PiccManager;
import android.util.Log;

import com.bindo.paymentsdk.payx.emv.urovo.BuildConfig;
import com.bindo.paymentsdk.v3.pay.common.util.Hex;


public class CallBackFunc {
    private static final String TAG = "CallBackFunc";
    private static final boolean DEBUG = BuildConfig.DEBUG;

    private static IccManager mICManager;
    private static PiccManager mRFManager;


    public static void setICManager(IccManager icManager) {
        CallBackFunc.mICManager = icManager;
    }

    public static void setRFManager(PiccManager rfManager) {
        CallBackFunc.mRFManager = rfManager;
    }

    public static int cEmvLib_WaitAppSel(int TryCnt, String[] Appname, int AppNum) {

        if (Appname == null) {
            return (-1);
        }
        Log.d(TAG, "cEmvLib_WaitAppSel--AppNum=" + AppNum + "Appname_len=" + Appname.length);
        int ret, i;

        for (i = 0; i < AppNum; i++) {
            Log.d(TAG, "-------Appname[" + i + "]" + Appname[i]);
            Log.d(TAG, "--EmvLib_GetApp---4--appname=%s" + Appname[i]);
        }

        int selectedIndex = 0;
//        if (mEMVCardReader != null) {
//            try {
//                selectedIndex = mEMVCardReader.transactionApplicationSelection(Appname);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            Log.d(TAG, "--CallBackFuncAppSelDialog---selectedIndex: " + selectedIndex + " AppName: " + Appname[selectedIndex]);
//        }

        return selectedIndex;   //选择第一个APP
    }

    public static int cEmvLib_GetHolderPwd(int TryCnt, int RemainCnt, byte[] pin) {
        Log.d(TAG,"cEmvLib_GetHolderPwd---pin.length"+new String(pin).length());
        Log.d(TAG,"cEmvLib_GetHolderPwd---TryCnt"+TryCnt);
        Log.d(TAG,"cEmvLib_GetHolderPwd---RemainCnt"+RemainCnt);
//        pin[0] = 0x33;
//        pin[1] = 0x34;
//        pin[2] = 0x35;
//        pin[3] = 0x36;
//        pin[4] = 0x37;
//
//        return 0;  //0 返回正常
        return EMVErrorNo.ERR_NOPIN; // 忽略密码
    }

    public static int cEmvLib_IccReset(byte slot, byte VCC_Mode, byte[] ATR) {
        Log.d("CallBackFunc", "--cEmvLib_IccReset :  slot: " + slot);
        if (slot == 0) {
            int ret = mICManager.open((byte) 0, (byte) 0x01, (byte) 0x01);
            if (DEBUG)
                Log.d("CallBackFunc", "--cEmvLib_IccReset : open " + ret);
            ret = mICManager.activate(ATR);
            if (DEBUG)
                Log.d("CallBackFunc", "--cEmvLib_IccReset : " + ret);
            //ret = Icc.getInstance().close();
            return (ret != -1 ? 0 : ret);
        }
        return 0;
    }

    public static int cEmvLib_IccCommand(byte slot, byte[] ApduSend, byte[] ApduResp) {

        //00a40400000e315041592e5359532e44444630310100000000
        //00a404000e315041592e5359532e444446303100
        //00a404000e315041592e5359532e44444630310000
        if (DEBUG) {
            Log.d("CallBackFunc", "--cEmvLib_IccCommand :  slot: " + slot);
            //Log.d("CallBackFunc","--cEmvLib_IccCommand :  ApduSend: " + bytesToHexString(ApduSend));
        }
        if (slot == 0) {
            int datalen = ((int) (ApduSend[4] & 0xff)) * 256 + ((int) (ApduSend[5] & 0xff));
            byte[] apdu;
            if (datalen != 0) {
                apdu = new byte[datalen + 6];
                System.arraycopy(ApduSend, 0, apdu, 0, 4);
                apdu[4] = (byte) datalen;
                System.arraycopy(ApduSend, 6, apdu, 5, datalen);
                int le = ((int) (ApduSend[datalen + 6] & 0xff)) * 256 + ((int) (ApduSend[datalen + 7] & 0xff));
                apdu[datalen + 5] = (byte) le;
            } else {
                apdu = new byte[datalen + 5];
                System.arraycopy(ApduSend, 0, apdu, 0, 4);
                int le = ((int) (ApduSend[datalen + 6] & 0xff)) * 256 + ((int) (ApduSend[datalen + 7] & 0xff));
                apdu[datalen + 4] = (byte) le;
            }

            byte[] apduRev = new byte[512];
            if (DEBUG)
                Log.d("CallBackFunc", "--cEmvLib_IccCommand :  ApduSend: " + Hex.encode(apdu) + "  apdu: " + apdu.length);
            int ret = mICManager.apduTransmit(apdu, apdu.length, apduRev, new byte[2]);
            if (DEBUG)
                Log.d("CallBackFunc", "--cEmvLib_IccCommand : apduTransmit  apduRev=  " + Hex.encode(apduRev));
            //0e315041592e5359532e444446303100
            //6f1e840e315041592e5359532e4444463031a50c8801015f2d027a689f1101019000

            if (ret != -1) {
                ApduResp[0] = (byte) ((ret - 2) >> 8);//ret 鍖呭惈鐘舵�佸瓧
                ApduResp[1] = (byte) ((ret - 2) & 0xff);
                System.arraycopy(apduRev, 0, ApduResp, 2, ret);
            } else {
                return ret;
            }
        } else {//picc
            int datalen = ((int) (ApduSend[4] & 0xff)) * 256 + ((int) (ApduSend[5] & 0xff));
            byte[] apdu;
            if (datalen != 0) {
                apdu = new byte[datalen + 6];
                System.arraycopy(ApduSend, 0, apdu, 0, 4);
                apdu[4] = (byte) datalen;
                System.arraycopy(ApduSend, 6, apdu, 5, datalen);
                int le = ((int) (ApduSend[datalen + 6] & 0xff)) * 256 + ((int) (ApduSend[datalen + 7] & 0xff));
                apdu[datalen + 5] = (byte) le;
            } else {
                apdu = new byte[datalen + 5];
                System.arraycopy(ApduSend, 0, apdu, 0, 4);
                int le = ((int) (ApduSend[datalen + 6] & 0xff)) * 256 + ((int) (ApduSend[datalen + 7] & 0xff));
                apdu[datalen + 4] = (byte) le;
            }

            byte[] apduRev = new byte[512];
            Log.d("CallBackFunc", "--cEmvLib_IccCommand :  ApduSend: " + Hex.encode(apdu) + "  apdu: " + apdu.length);
            int ret = mRFManager.activate();
            byte[] rspStatus = new byte[2];
            ret = mRFManager.apduTransmit(apdu, apdu.length, apduRev, rspStatus);
            Log.d("CallBackFunc", "--cEmvLib_IccCommand : apduTransmit  apduRev=  " + Hex.encode(apduRev));
            //0e315041592e5359532e444446303100
            //6f30840e325041592e5359532e4444463031a51ebf0c1b61194f08a000000333010101500a50424f432044454249548701019000
            if (ret != -1) {
                ApduResp[0] = (byte) ((ret) >> 8);
                ApduResp[1] = (byte) ((ret) & 0xff);
                System.arraycopy(apduRev, 0, ApduResp, 2, ret + 2);
            } else {
                return ret;
            }
        }
        return 0;
    }

}
