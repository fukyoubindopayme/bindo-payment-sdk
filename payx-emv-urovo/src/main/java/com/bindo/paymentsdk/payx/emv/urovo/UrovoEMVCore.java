package com.bindo.paymentsdk.payx.emv.urovo;

import com.bindo.paymentsdk.payx.emv.core.Application;
import com.bindo.paymentsdk.payx.emv.core.ApplicationData;
import com.bindo.paymentsdk.payx.emv.core.CAPK;
import com.bindo.paymentsdk.payx.emv.EMVCore;
import com.bindo.paymentsdk.payx.emv.results.CallIssuerConfirmResult;
import com.bindo.paymentsdk.payx.emv.results.OnlineProcessingResult;

import java.util.HashMap;

public class UrovoEMVCore extends EMVCore {

    @Override
    public void startProcess() {

    }

    @Override
    public void startProcess(boolean isFallback) {

    }

    @Override
    public void startProcess(boolean isFallback, int detectRetryCount) {

    }

    @Override
    public void stopProcess() {

    }

    @Override
    public void addApplication(Application application) {

    }

    @Override
    public void removeApplication(Application application) {

    }

    @Override
    public HashMap<String, byte[]> resolveSpecificTerminalParameters(ApplicationData applicationData) {
        return null;
    }

    @Override
    public HashMap<String, byte[]> resolveSpecificPreTransactionParameters(ApplicationData applicationData) {
        return null;
    }

    @Override
    public HashMap<String, byte[]> resolveSpecificTransactionParameters(ApplicationData applicationData) {
        return null;
    }

    @Override
    public HashMap<String, byte[]> resolveSpecificOnlineProcessingResult(OnlineProcessingResult onlineProcessingResult, CallIssuerConfirmResult callIssuerConfirmResult) {
        return null;
    }

    @Override
    public byte[] getTLV(com.bindo.paymentsdk.v3.pay.common.emv.enums.EMVTag emvTag) {
        return new byte[0];
    }

    @Override
    public byte[] getTLV(String emvTag) {
        return new byte[0];
    }

    @Override
    public int setTLV(com.bindo.paymentsdk.v3.pay.common.emv.enums.EMVTag emvTag, byte[] data) {
        return 0;
    }

    @Override
    public int setTLV(String emvTag, byte[] data) {
        return 0;
    }

    @Override
    public int setCertificationAuthorityPublicKey(CAPK capk) {
        return 0;
    }
}
