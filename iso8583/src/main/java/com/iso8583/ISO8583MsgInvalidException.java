package com.iso8583;

/**
 * Created by solomon on 8/18/17.
 */

public class ISO8583MsgInvalidException extends Exception{

    public  ISO8583MsgInvalidException(String description)
    {
        super(description);
    }
}
