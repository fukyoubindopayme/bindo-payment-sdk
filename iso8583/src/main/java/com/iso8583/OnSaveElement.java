package com.iso8583;

public interface OnSaveElement {
    public int onSave(int fd, String value);
}

