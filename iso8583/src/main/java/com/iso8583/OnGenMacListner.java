package com.iso8583;

public interface OnGenMacListner {
    public int OnGenMac(byte[] msg, int offset, int len, byte[] MAC);
}
