package com.bindo.paymentsdk.v3.pay.gateway.fake;

import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Request;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Response;
import com.bindo.paymentsdk.v3.pay.gateway.Gateway;

import java.util.Date;
import java.util.HashMap;

/**
 * Fake Gateway
 */
public class FakeGateway implements Gateway {
    private final String TAG = "FakeGateway";

    private HashMap<String, Object> mDefaultGatewayConfig = new HashMap<>();

    public FakeGateway(HashMap<String, Object> configMap) {
        // Gateway 参数默认值

        if (configMap == null) {
            System.out.println("Please set the gateway parameters");
        } else {
            for (String key : configMap.keySet()) {
                Object value = configMap.get(key);
                mDefaultGatewayConfig.put(key, value);
            }
        }
    }

    public HashMap<String, Object> getDefaultGatewayConfig() {
        return mDefaultGatewayConfig;
    }

    public void setDefaultGatewayConfig(HashMap<String, Object> defaultGatewayConfig) {
        this.mDefaultGatewayConfig = defaultGatewayConfig;
    }

    @Override
    public Response sendRequest(Request request) {
        Response fakeResponse = new Response(request);
        try {
            String approvalCode = String.valueOf((int) ((Math.random() * 9 + 1) * 100000));
            String retrievalReferenceNumber = String.valueOf(new Date().getTime() / 1000) + String.valueOf((int) ((Math.random() * 9 + 1) * 10));
            fakeResponse.setIsApproved(true);
            fakeResponse.setActionCode("00");
            fakeResponse.setApprovalCode(approvalCode);
            fakeResponse.setRetrievalReferenceNumber(retrievalReferenceNumber);

            Thread.sleep(800);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return fakeResponse;
    }
}
