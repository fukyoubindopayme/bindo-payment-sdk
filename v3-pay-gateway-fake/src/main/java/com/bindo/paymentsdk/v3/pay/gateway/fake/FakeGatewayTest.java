package com.bindo.paymentsdk.v3.pay.gateway.fake;

import com.bindo.paymentsdk.v3.pay.common.CardDetectMode;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Request;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Response;
import com.bindo.paymentsdk.v3.pay.common.TransactionType;
import com.bindo.paymentsdk.v3.pay.common.util.Hex;
import com.bindo.paymentsdk.v3.pay.gateway.Gateway;

import java.util.Date;
import java.util.HashMap;

public class FakeGatewayTest {
    public static void main(String[] args) {
        Gateway gateway = new FakeGateway(new HashMap<String, Object>());

        HashMap<String, byte[]> tagLengthValues = new HashMap<>();
        tagLengthValues.put("50", Hex.decode("414D45524943414E2045585052455353"));
        tagLengthValues.put("57", Hex.decode("374245002741006D200122115021234500000F"));
        tagLengthValues.put("82", Hex.decode("5C00"));
        tagLengthValues.put("84", Hex.decode("A000000025010801"));
        tagLengthValues.put("87", Hex.decode("01"));
        tagLengthValues.put("90", Hex.decode("0B1B26577A67B9A2F307E06F8A0ABD55C2CD37CB748561602B9D95AB9F9FB80FB8BEAF23A55C5C699195AF555BE3C2B924D7020BFC727D88D44317BC1899A1EDCF46D7DB66CB79295C796ABED56C45764520054E7D97D74412D724480BDDFD1F8DB90CCFA783912DB7824E4E22C3D0E6A3F1F07AAD8CBEF1E5FF224687B945B56B4BF639FA9C40DC8D6329A11B074386AC54703AC4DD9E79F86CC0D523C2B9BDA2808E05A5A2D885B60FD4BB4C5811F9"));
        tagLengthValues.put("92", Hex.decode("B48FA1DD"));
        tagLengthValues.put("93", Hex.decode("08B89762CBA45A03BA13EA5F974103F23F3D7012BD0868E55DC124771C8561F12D5CDC92A9D753DE9BB82EF136CD1F631B6D6D80A3109AD2593F7101E29B5CAFE8B3EE51BE92FC535175022A4676B33467902F710F249643E0221579F819F73EC58E20CF191FD18CA0C8AF71387C4183F27BC30174DF8A1888E0FAB5529DC954D36924181B5C423B642DDF10C45D86A0"));
        tagLengthValues.put("94", Hex.decode("080101000802020108030300080505001002030018030300"));
        tagLengthValues.put("95", Hex.decode("0200000000"));
        tagLengthValues.put("9F37", Hex.decode("0A8C889B"));
        tagLengthValues.put("9F08", Hex.decode("0001"));
        tagLengthValues.put("9F26", Hex.decode("0042D6D73E99371D"));
        tagLengthValues.put("5F34", Hex.decode("00"));
        tagLengthValues.put("8A", Hex.decode("5931"));
        tagLengthValues.put("5F2D", Hex.decode("656E"));
        tagLengthValues.put("5F25", Hex.decode("150201"));
        tagLengthValues.put("9F40", Hex.decode("6000F0A001"));
        tagLengthValues.put("9B", Hex.decode("E800"));
        tagLengthValues.put("8F", Hex.decode("C9"));
        tagLengthValues.put("5F2A", Hex.decode("0344"));
        tagLengthValues.put("9F03", Hex.decode("000000000000"));
        tagLengthValues.put("5F24", Hex.decode("200131"));
        tagLengthValues.put("9F07", Hex.decode("FF00"));
        tagLengthValues.put("9F4A", Hex.decode("82"));
        tagLengthValues.put("9F41", Hex.decode("00000001"));
        tagLengthValues.put("9F21", Hex.decode("111057"));
        tagLengthValues.put("9F1A", Hex.decode("0344"));
        tagLengthValues.put("9F04", Hex.decode("00000000"));
        tagLengthValues.put("8C", Hex.decode("9F02069F03069F1A0295055F2A029A039C019F3704"));
        tagLengthValues.put("9F0E", Hex.decode("0000000000"));
        tagLengthValues.put("8E", Hex.decode("000000000000000042015E035F0300000000000000000000"));
        tagLengthValues.put("9F0F", Hex.decode("FC78FCF800"));
        tagLengthValues.put("8D", Hex.decode("8A029F02069F03069F1A0295055F2A029A039C019F3704"));
        tagLengthValues.put("4F", Hex.decode("A000000025010801"));
        tagLengthValues.put("9F45", Hex.decode("DAC1"));
        tagLengthValues.put("9F09", Hex.decode("0001"));
        tagLengthValues.put("9F1B", Hex.decode("00003A98"));
        tagLengthValues.put("5F28", Hex.decode("0840"));
        tagLengthValues.put("9F35", Hex.decode("22"));
        tagLengthValues.put("9A", Hex.decode("170913"));
        tagLengthValues.put("9F36", Hex.decode("0006"));
        tagLengthValues.put("9C", Hex.decode("00"));
        tagLengthValues.put("9F42", Hex.decode("0840"));
        tagLengthValues.put("9F33", Hex.decode("E0B8C8"));
        tagLengthValues.put("9F10", Hex.decode("06020103A0A800"));
        tagLengthValues.put("5A", Hex.decode("374245002741006F"));
        tagLengthValues.put("9F32", Hex.decode("03"));
        tagLengthValues.put("9F0D", Hex.decode("FC50ECA800"));
        tagLengthValues.put("9F06", Hex.decode("A000000025010801"));
        tagLengthValues.put("9F27", Hex.decode("80"));
        tagLengthValues.put("9F02", Hex.decode("000000003100"));
        tagLengthValues.put("9F34", Hex.decode("5E0300"));

        Request request = new Request(tagLengthValues);
        Response response = null;
        try {

            HashMap<String, String> defaultConfig = new HashMap<>();

            request.setGatewayConfigs(defaultConfig);
            request.setTransactionType(TransactionType.AUTHORIZATION.name());
            request.setTransactionSystemTraceAuditNumber(999999);
            request.setTransactionDateAndTime(new Date());
            request.setTransactionAmount(31);
            request.setTransactionCurrency("HKD");

            request.setPrimaryAccountNumber("374245002741006");
            request.setCardDetectMode(CardDetectMode.CONTACT);
            request.setPOSEntryMode("");
            request.setCardExpirationDate("0110");
            request.setTrack1DiscretionaryData("");
            request.setTrack2DiscretionaryData("");
            request.setTrack2EquivalentData("374245002741006D200122115021234500000F");

            System.out.print("REQUEST>>>--------------------------------------------------\n");
            System.out.println(request.toString());

            response = gateway.sendRequest(request);

            System.out.print("RESPONSE>>>--------------------------------------------------\n");
            System.out.println(response.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
