package com.bindo.paymentsdk.v3.pay.gateway.amex.test;

import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Request;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Response;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gateway.AmexGateway;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AmexGatewayAutoTest {

    String jsonTests[] = {
            "{\n" +
                    "\t\"_testPlan\" : \"GCAG CAPN\",\n" +
                    "\t\"_testCase\" : \"1000\",\n" +
                    "\t\"transactionType\" : \"AUTHORIZATION\",\n" +
                    "\t\"cardDetectMode\" : \"swipe\",\n" +
                    "\t\"primaryAccountNumber\" : \"373953192351004\",\n" +
                    "\t\"track2EquivalentData\" : \"373953192351004=2001170115021\",\n" +
                    "\t\"transactionAmount\" : \"5.01\",\n" +
                    "\t\"transactionCurrencyCode\" : \"344\",\n" +
                    "\t\"_expected\" : \"000\"\n" +
                    "}\n"
    };

    @Test
    public void basicTest() throws JSONException {
        AmexGateway gateway = new AmexGateway(null);

        for (String json : jsonTests) {
            JsonParser parser = new JsonParser();
            JsonElement mJson = parser.parse(json);
            Gson gson = new Gson();
            Request req = gson.fromJson(mJson, Request.class);

            System.out.println(req.toString());
            Response response = gateway.sendRequest(req);
            System.out.println(response.toString());

            JsonObject jsonObject = new JsonParser().parse(json).getAsJsonObject();
            assertEquals(jsonObject.get("_expected").getAsString(), response.getActionCode());
        }
    }
}