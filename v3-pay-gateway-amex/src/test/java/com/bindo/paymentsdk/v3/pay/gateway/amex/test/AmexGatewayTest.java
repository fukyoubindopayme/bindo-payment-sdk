package com.bindo.paymentsdk.v3.pay.gateway.amex.test;

import com.bindo.paymentsdk.v3.pay.common.CardDetectMode;
import com.bindo.paymentsdk.v3.pay.common.ReversalType;
import com.bindo.paymentsdk.v3.pay.common.TransactionType;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Request;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Response;
import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gateway.AmexGateway;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.CurrencyCode;

import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class AmexGatewayTest {

    @Test
    public void basicTest() {
        Log.setsUseSystemOut(true);
        AmexGateway gateway = new AmexGateway(null);
        Request req = new Request();
        req.setTransactionType(TransactionType.AUTHORIZATION);
        req.setCardDetectMode(CardDetectMode.SWIPE);
        req.setPrimaryAccountNumber("373953192351004");
        req.setTransactionAmount(10);
        req.setTrack2EquivalentData(req.getPrimaryAccountNumber() + "=" + "2001" + "221" + "15021234500000");
        req.setProcessingCode("004800");
        req.setPOSEntryMode("261101200120");
        req.setMerchantIdentifier("0512860628");
        req.setTerminalIdentification("001");
        req.setMerchantCategoryCode("5139");
        req.setTransactionCurrencyCode("840");
        req.setTerminalCountryCode("344");
        req.setTransactionDateAndTime(new Date());
        req.setTransactionSystemTraceAuditNumber(new Integer((new SimpleDateFormat("HHmmss")).format(new Date())));
        Response response = gateway.sendRequest(req);
        assertEquals("000", response.getActionCode());
    }

    //@Test
    public void leoTest() {
        AmexGateway gateway = new AmexGateway(null);
        Request req = new Request();
        req.setTransactionType(TransactionType.AUTHORIZATION);
        req.setPrimaryAccountNumber("374245002741006");
        req.setCardDetectMode(CardDetectMode.MANUAL);
        req.setCardExpirationDate("0110");
        //req.setTrack2EquivalentData("374245002741006D200122115021234500000F");
        req.setTransactionDateAndTime(new Date());
        req.setTransactionCurrencyCode(CurrencyCode.Hong_Kong_Dollar.getCurrencyCode());
        req.setTransactionAmount(100);
        req.setTerminalIdentification("001");
        req.setCardSecurityCode("111");


        /*
        req.setProcessingCode("004800");
        req.setPOSEntryMode("261101200120");
        req.setMerchantIdentifier("0512860628");
        req.setTerminalIdentification("001");
        req.setMerchantCategoryCode("5139");
        req.setTerminalCountryCode("344");
        req.setTransactionSystemTraceAuditNumber(new Integer((new SimpleDateFormat("HHmmss")).format(new Date())));
        */
        Response response = gateway.sendRequest(req);
        assertEquals("000", response.getActionCode());
    }

    // basic auth. tests
    String[][] data1000 = new String[][] {
            {"1100", "373953192351004", "5.05", "000"},
            {"1100", "373953192351004", "15", "100"},
            {"1100", "373953192351004", "25.01", "107"},
            {"1100", "373953192351004", "35", "110"},
            {"1100", "373953192351004", "195", "101"},
            {"1100", "341111599241000", "4", "111"},
            {"1100", "373953195351001", "6", "109"},
            {"1100", "341111599241000", "12", "000"},
            {"1100", "341111599241000", "27", "100"},
            {"1100", "373953192351004", "107987.99", "000"},
    };

    // timeout reversal tests
    String[][] data3000 = new String[][] {
            //{"1100", "373953192351004", "65.99", "601", "false", "2", "", "400"},
            //{"1100", "373953192351004", "75.99", "000", "false", "2", "", "400"},
            {"1100", "373953192351004", "364.99", "601", "true", "2", "", "400"},
            //{"1100", "373953192351004", "375", "000", "false", "2", "", "400"},
    };

    // swipe track2 tests
    String[][] data4000 = new String[][] {
            {"1100", "373953192351004", "155.11", "000", "true"},
            {"1100", "373953192351004", "167", "000"},
    };

    // CID tests
    String[][] data5000 = new String[][] {
            // mti      pan             amount  rc      t2ansi  22.7  csc
            {"1100", "341111599241000", "265", "000", "false", "W", "9395"},
            {"1100", "341111599241000", "43", "122", "false", "W", "0000"},
            {"1100", "341111599241000", "53", "100", "false", "W", "0150"},
            {"1100", "341111599241000", "63", "000", "false", "W", "1005"},
            {"1100", "341111599241000", "72", "000", "false", "W", "9999"},
    };

    @Test
    public void testData1() {
        Log.setsUseSystemOut(true);
        AmexGateway gateway = new AmexGateway(null);
        String [][] data = data1000;
        for (int i = 0; i < data.length; i++) {
            Request req = new Request();
            req.setCardDetectMode(CardDetectMode.SWIPE);
            if (data[i][0].equals("1100")) {
                req.setTransactionType(TransactionType.AUTHORIZATION);
            }
            req.setPrimaryAccountNumber(data[i][1]);
            req.setTransactionAmount(Double.parseDouble(data[i][2]));
            req.setTrack2EquivalentData(req.getPrimaryAccountNumber() + "=" + "2001" + "221" + "15021234500000");
            try {
                if (Boolean.parseBoolean(data[i][4])) {
                    req.setTrack2EquivalentData(req.getPrimaryAccountNumber() + "=" + "2001" + "1701" + "15021");
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                //
            }
            req.setProcessingCode("004800");
            String pos1 = "2"; //msr
            String pos7 = "2"; //msr
            try {
                pos7 = data[i][5];
            } catch (ArrayIndexOutOfBoundsException e) {
                //
            }
            if (!pos7.equals("2")) {
                pos1 = "0";
            }
            req.setPOSEntryMode(pos1 + "61101" + pos7 + "00120");
            if (pos7.equals("S") || pos7.equals("9") || pos7.equals("W")) {
                req.setCardSecurityCode(data[i][6]);
                req.setCardExpirationDate("2001");
                req.setTrack2EquivalentData(null);
            } else {
                req.setTrack2EquivalentData(req.getPrimaryAccountNumber() + "=" + "2001" + "221" + "15021234500000");
                try {
                    if (Boolean.parseBoolean(data[i][4])) {
                        req.setTrack2EquivalentData(req.getPrimaryAccountNumber() + "=" + "2001" + "1701" + "15021");
                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                    //
                }
            }
            if (pos7.equals("W")) {
                req.setTrack2EquivalentData(req.getPrimaryAccountNumber() + "=" + "2001" + "221" + "15021234500000");
                try {
                    if (Boolean.parseBoolean(data[i][4])) {
                        req.setTrack2EquivalentData(req.getPrimaryAccountNumber() + "=" + "2001" + "1701" + "15021");
                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                    //
                }
            }
            req.setMerchantIdentifier("0512860628");
            req.setTerminalIdentification("001");
            req.setMerchantCategoryCode("5139");
            req.setTransactionCurrencyCode("840");
            req.setTerminalCountryCode("344");
            req.setTransactionDateAndTime(new Date());
            req.setTransactionSystemTraceAuditNumber(new Integer((new SimpleDateFormat("HHmmss")).format(new Date())));
            Response response = gateway.sendRequest(req);
            assertEquals(data[i][3], response.getActionCode());
            if (response.getActionCode().equals(Response.Error.ERROR_CODE_TIMEOUT) || data[i][2].equals("75.99")) {
                System.out.println("====================================================    REVERSAL");
                req.setOriginalTransactionDateAndTime(req.getTransactionDateAndTime());
                req.setProcessingCode("004000");
                if (data[i][2].equals("75.99")) {
                    req.setProcessingCode("024000");
                }
                req.setOriginalTransactionSystemTraceAuditNumber(req.getTransactionSystemTraceAuditNumber());
                req.setOriginalTransactionType(req.getTransactionType());
                req.setTransactionDateAndTime(new Date());
                req.setTransactionSystemTraceAuditNumber(new Integer((new SimpleDateFormat("HHmmss")).format(new Date())));
                req.setAcquirerReferenceData(response.getTransactionId());
                //req.setReversalType(ReversalType.MERCHANT_INITIATED);
                req.setReversalType(ReversalType.SYSTEM_GENERATED);
                req.setTransactionType(TransactionType.REVERSAL);
                response = gateway.sendRequest(req);
                String rcr = "000";
                try {
                    rcr = data[i][7];
                } finally {
                }
                assertEquals(rcr, response.getActionCode());
            }
            System.out.println("=======================================================================");
        }
    }
}