package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.connection;

import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.exception.IPSAPIException;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.exception.SSLConnectionException;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean.ErrorObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.header.RequestHeader;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.messageformatter.AuthorizationMessageFormatter;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.processor.PaymentSDKRequestProcessor;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.processor.TCPRequestProcessor;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.utils.ISOUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Connection {
    boolean debugflag = PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON");
    AuthorizationMessageFormatter authMesssageFormatter = new AuthorizationMessageFormatter();

    public Connection() {
    }

    public String getConnectionAndSendRequest(String payloadString, PropertyReader propertyReader) throws SSLConnectionException, IPSAPIException, Exception {
        if (this.debugflag) {
            Log.i("Entered into getConnectionAndSendRequest");
        }

        String isoFormatServerResponse = "";
        if ("DC".equalsIgnoreCase(PropertyReader.getConnectionType())) {
            if (this.debugflag) {
                Log.i("Creating HTTP Connection");
            }

            TCPRequestProcessor tcpRequestProcessor = new TCPRequestProcessor();
            isoFormatServerResponse = tcpRequestProcessor.createConnectionAndRequestSend(payloadString, propertyReader);
        } else {
            if (!"PaymentSDK".equals(PropertyReader.getConnectionType())) {
                throw new Exception("Invalid Connection Type");
            }

            if (this.debugflag) {
                Log.i("Creating PaymentSDK Connection");
            }

            isoFormatServerResponse = this.getPaymentSDKConnectionAndSendRequest(payloadString, propertyReader);
        }

        if (this.debugflag) {
            Log.i("Exiting from getConnectionAndSendRequest");
        }

        return isoFormatServerResponse;
    }

    public String getPaymentSDKConnectionAndSendRequest(String payloadString, PropertyReader propertyReader) throws SSLConnectionException, IPSAPIException, Exception {
        if (this.debugflag) {
            Log.i("Entered into getPaymentSDKConnectionAndSendRequest()");
            Log.i("\tpayloadString\t" + payloadString);
        }

        String serverResponse = null;
        HTTPSConnectionInfo httpsConnectionInfo = new HTTPSConnectionInfo();
        System.setProperty("sun.net.http.allowRestrictedHeaders", "true");
        RequestHeader requestHeader = new RequestHeader();
        requestHeader.setCountry(PropertyReader.getMERCHANT_COUNTRY());
        requestHeader.setMerchantNumber(PropertyReader.getMERCHANT_NUMBER());
        requestHeader.setMessage(PropertyReader.getMESSAGE_TYPE());
        requestHeader.setRegion(PropertyReader.getREGION());
        requestHeader.setRoutingIndicator(PropertyReader.getROUTING_INDICATOR());
        requestHeader.setOrigin(PropertyReader.getORIGIN());
        requestHeader.setAPIVersion("GCAG 5.1 Java");
        String httpURL = this.constructWebServiceURL(propertyReader.getHTTPS_URL(), propertyReader.getPort());
        httpsConnectionInfo.setUri(httpURL);
        httpsConnectionInfo.setMethodType(PropertyReader.getMETHOD_TYPE());
        httpsConnectionInfo.setUserAgent(PropertyReader.getUSER_AGENT());
        if (PropertyReader.getPROXY().equalsIgnoreCase("YES")) {
            httpsConnectionInfo.setProxy(PropertyReader.getPROXY());
            httpsConnectionInfo.setProxyAddress(propertyReader.getPROXY_ADDRESS());
            httpsConnectionInfo.setPort(propertyReader.getPROXY_PORT());
            httpsConnectionInfo.setUserId(propertyReader.getPROXY_USER_ID());
            httpsConnectionInfo.setPassword(propertyReader.getPROXY_PASSWORD());
            httpsConnectionInfo.setProxyAuthentication(PropertyReader.getProxyAuthentication());
        }

        PaymentSDKRequestProcessor httpsRequestProcessor = new PaymentSDKRequestProcessor();

        try {
            try {
                if (this.debugflag) {
                    Log.i("Creating HTTPS Connection here");
                }

                httpsRequestProcessor.createHTTPsConnection(httpsConnectionInfo);
                serverResponse = httpsRequestProcessor.sendHttpsRequest(payloadString, requestHeader);
                if (this.debugflag) {
                    Log.i("After submitting the request");
                }
            } catch (IPSAPIException var9) {
                if (this.debugflag) {
                    Log.e("Error in getPaymentSDKConnectionAndSendRequest() method..  Exception:" + var9);
                }

                if (serverResponse == null || serverResponse.equalsIgnoreCase("")) {
                    throw new IPSAPIException("No response message from PaymentSDK System", var9);
                }
            }
        } catch (IOException var10) {
            if (this.debugflag) {
                Log.e("Error in getPaymentSDKConnectionAndSendRequest() method..  Exception:" + var10);
            }

            throw new SSLConnectionException(var10.getMessage(), var10);
        } catch (Exception var11) {
            if (this.debugflag) {
                Log.e("Error in getPaymentSDKConnectionAndSendRequest() method..  Exception:" + var11);
            }

            throw new IPSAPIException(var11.getMessage(), var11);
        }

        if (this.debugflag) {
            Log.i("Exiting from getPaymentSDKConnectionAndSendRequest()");
        }

        return serverResponse;
    }

    public List<ErrorObject> validateConnectionInfo(PropertyReader propertyReader) {
        if (this.debugflag) {
            Log.i("Entered into validateConnectionInfo()");
        }

        List<ErrorObject> connectionInfoErrorList = new ArrayList();
        ErrorObject panErrorObj;
        if (!ISOUtil.IsNullOrEmpty(PropertyReader.getConnectionType()) && PropertyReader.getConnectionType().equalsIgnoreCase("PaymentSDK")) {
            if (ISOUtil.IsNullOrEmpty(propertyReader.getHTTPS_URL())) {
                panErrorObj = new ErrorObject("1", "Invalid Web Service URL");
                connectionInfoErrorList.add(panErrorObj);
                if (this.debugflag) {
                    Log.e("Invalid Web Service URL");
                }
            }

            if (!ISOUtil.IsNullOrEmpty(PropertyReader.getPROXY()) && PropertyReader.getPROXY().equalsIgnoreCase("YES")) {
                if (ISOUtil.IsNullOrEmpty(propertyReader.getPROXY_ADDRESS())) {
                    panErrorObj = new ErrorObject("2", "Invalid Connection Proxy Address");
                    connectionInfoErrorList.add(panErrorObj);
                    if (this.debugflag) {
                        Log.e("Invalid Connection Proxy Address");
                    }
                }

                if (ISOUtil.IsNullOrEmpty(propertyReader.getPROXY_PORT())) {
                    panErrorObj = new ErrorObject("3", "Invalid Connection Proxy Port");
                    connectionInfoErrorList.add(panErrorObj);
                    if (this.debugflag) {
                        Log.e("Invalid Connection Proxy Port");
                    }
                }
            } else if (ISOUtil.IsNullOrEmpty(PropertyReader.getPROXY())) {
                panErrorObj = new ErrorObject("2", "Invalid Connection Proxy");
                connectionInfoErrorList.add(panErrorObj);
                if (this.debugflag) {
                    Log.e("Invalid Connection Proxy");
                }
            }

            if (ISOUtil.IsNullOrEmpty(PropertyReader.getMERCHANT_NUMBER())) {
                panErrorObj = new ErrorObject("4", "Invalid Connection Header Merchant Number");
                connectionInfoErrorList.add(panErrorObj);
                if (this.debugflag) {
                    Log.e("Invalid Connection Header Merchant Number");
                }
            }

            if (ISOUtil.IsNullOrEmpty(PropertyReader.getORIGIN())) {
                panErrorObj = new ErrorObject("5", "Invalid Connection Header Origin Code");
                connectionInfoErrorList.add(panErrorObj);
                if (this.debugflag) {
                    Log.e("Invalid Connection Header Origin Code");
                }
            }

            if (ISOUtil.IsNullOrEmpty(PropertyReader.getMESSAGE_TYPE())) {
                panErrorObj = new ErrorObject("7", "Invalid Connection Header Message Type");
                connectionInfoErrorList.add(panErrorObj);
                if (this.debugflag) {
                    Log.e("Invalid Connection Header Message Type");
                }
            }

            if (ISOUtil.IsNullOrEmpty(PropertyReader.getREGION())) {
                panErrorObj = new ErrorObject("8", "Invalid Connection Header Region");
                connectionInfoErrorList.add(panErrorObj);
                if (this.debugflag) {
                    Log.e("Invalid Connection Header Region");
                }
            }

            if (ISOUtil.IsNullOrEmpty(PropertyReader.getROUTING_INDICATOR())) {
                panErrorObj = new ErrorObject("9", "Invalid Connection Header Routing Indicator");
                connectionInfoErrorList.add(panErrorObj);
                if (this.debugflag) {
                    Log.e("Invalid Connection Header Routing Indicator");
                }
            }

            if (ISOUtil.IsNullOrEmpty(PropertyReader.getMERCHANT_COUNTRY())) {
                panErrorObj = new ErrorObject("10", "Invalid Connection Header Merchant Country");
                connectionInfoErrorList.add(panErrorObj);
                if (this.debugflag) {
                    Log.e("Invalid Connection Header Merchant Country");
                }
            }
        } else if (!ISOUtil.IsNullOrEmpty(PropertyReader.getConnectionType()) && PropertyReader.getConnectionType().equalsIgnoreCase("DC")) {
            if (ISOUtil.IsNullOrEmpty(propertyReader.getHttpsIP())) {
                panErrorObj = new ErrorObject("1", "Invalid IP DirectConnectionIPAddress");
                connectionInfoErrorList.add(panErrorObj);
                if (this.debugflag) {
                    Log.e("Invalid IP DirectConnectionIPAddress");
                }
            }

            if (propertyReader.getPort() == 443) {
                panErrorObj = new ErrorObject("2", "Invalid DirectConnection Port");
                connectionInfoErrorList.add(panErrorObj);
                if (this.debugflag) {
                    Log.e("Invalid DirectConnection Port");
                }
            }
        } else {
            panErrorObj = new ErrorObject("1", "Invalid Connection Type");
            connectionInfoErrorList.add(panErrorObj);
            if (this.debugflag) {
                Log.e("Invalid Connection Type");
            }
        }

        if (this.debugflag) {
            Log.i("Errors found in validating the connection:" + connectionInfoErrorList.size());
            Log.i("Exiting from validateConnectionInfo()");
        }

        return connectionInfoErrorList;
    }

    private String constructWebServiceURL(String ipsURL, int httpPort) {
        StringBuffer PaymentSDKURL = new StringBuffer();
        if (!ISOUtil.IsNullOrEmpty(ipsURL) && httpPort > 0) {
            int index = ipsURL.indexOf("IPPayments");
            PaymentSDKURL.append(ipsURL.substring(0, index - 1));
            PaymentSDKURL.append(":");
            PaymentSDKURL.append(httpPort);
            PaymentSDKURL.append(ipsURL.substring(index - 1, ipsURL.length()));
        }

        return PaymentSDKURL.toString();
    }
}
