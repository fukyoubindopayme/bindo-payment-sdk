package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.header;

import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.Constants;

public class RequestHeader
{
  private String origin = "";
  private String country = "";
  private String region = "";
  private String message = "";
  private String merchantNumber = "";
  private String routingIndicator = "";
  private String apiVersion = "";
  
  public String getOrigin()
  {
    return this.origin;
  }
  
  public void setOrigin(String origin)
  {
    this.origin = origin;
  }
  
  public String getCountry()
  {
    return this.country;
  }
  
  public void setCountry(String country)
  {
    this.country = country;
  }
  
  public String getRegion()
  {
    return this.region;
  }
  
  public void setRegion(String region)
  {
    this.region = region;
  }
  
  public String getMessage()
  {
    return this.message;
  }
  
  public void setMessage(String message)
  {
    this.message = message;
  }
  
  public String getMerchantNumber()
  {
    return this.merchantNumber;
  }
  
  public void setMerchantNumber(String merchantNumber)
  {
    this.merchantNumber = merchantNumber;
  }
  
  public String getRoutingIndicator()
  {
    return this.routingIndicator;
  }
  
  public void setRoutingIndicator(String routingIndicator)
  {
    this.routingIndicator = routingIndicator;
  }
  
  public String getAPIVersion()
  {
    return this.apiVersion;
  }
  
  public void setAPIVersion(String version)
  {
    this.apiVersion = version;
  }
  
  public String toString()
  {
    StringBuffer buffer = new StringBuffer();
    buffer.append("ORIGIN").append(" :: ").append(getOrigin()).append(Constants.NEW_LINE_CHAR);
    buffer.append("MERCHANT_COUNTRY").append(" :: ").append(getCountry()).append(Constants.NEW_LINE_CHAR);
    buffer.append("REGION").append(" :: ").append(getRegion()).append(Constants.NEW_LINE_CHAR);
    buffer.append("MESSAGE_TYPE").append(" :: ").append(getMessage()).append(Constants.NEW_LINE_CHAR);
    buffer.append("MERCHANT_NUMBER").append(" :: ").append(getMerchantNumber()).append(Constants.NEW_LINE_CHAR);
    buffer.append("ROUTING_INDICATOR").append(" :: ").append(getRoutingIndicator()).append(Constants.NEW_LINE_CHAR);
    buffer.append("APIVersion").append(" :: ").append(getAPIVersion()).append(Constants.NEW_LINE_CHAR);
    return buffer.toString();
  }
}
