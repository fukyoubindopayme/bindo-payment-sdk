package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.validator;

public class Amex
  implements CreditCardType
{
  private static final String PREFIX = "34,35,37,";
  
  public boolean matches(String card)
  {
    String prefix2 = card.substring(0, 2) + ",";
    return ("34,35,37,".indexOf(prefix2) != -1) && (card.length() == 15);
  }
}
