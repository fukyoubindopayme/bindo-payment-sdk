package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations;

public enum RecordType {
    Transaction_File_Header("TFH"),
    Transaction_Advice_Basic("TAB"),
    Transaction_Advice_Detail("TAD"),
    Transaction_Advice_Addendum("TAA"),
    Transaction_Batch_Trailer("TBT"),
    Transaction_File_Summary("TFS");

    private final String recordType;

    private RecordType(String recordType) {
        this.recordType = recordType;
    }

    public String getRecordType() {
        return this.recordType;
    }
}
