package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations;

public enum AuditAdjInd
{
  Multiple_adjustments("X"),  One_adjustment_only_Cardmember_notified("Y"),  One_adjustment_only_Cardmember_not_notified("Z");
  
  private final String auditAdjInd;
  
  private AuditAdjInd(String auditAdjInd)
  {
    this.auditAdjInd = auditAdjInd;
  }
  
  public String getAuditAdjInd()
  {
    return this.auditAdjInd;
  }
}
