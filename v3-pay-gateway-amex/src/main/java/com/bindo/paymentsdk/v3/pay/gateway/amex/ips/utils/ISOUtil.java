package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.utils;

import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.Constants;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.CountryCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.CurrencyCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.FunctionCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.MCCCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.MessageReasonCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.MessageTypeID;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.ProcessingCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.validator.Validator;
import java.io.IOException;
import java.io.StringReader;
import java.util.LinkedList;

public class ISOUtil implements Constants {
    public String trace;
    public StringBuffer header;
    public StringBuffer messageType;
    public StringBuffer primaryBitmapStr;
    public StringBuffer secondaryBitmapStr;
    public long primaryBitMap;
    public long secondaryBitMap;
    public LinkedList<String> presentFieldList;
    public boolean invalidTrace = false;
    public String errorMessage;
    public int headerLength = 0;
    public int nationalUseDatalength = 0;

    public ISOUtil() {
    }

    public static byte[] decode(String ascii) throws IndexOutOfBoundsException {
        int oddLength = ascii.length() % 2;
        byte[] res = new byte[ascii.length() / 2 + oddLength];
        int i = 0;
        if (oddLength == 1) {
            i = i + 1;
            res[i] = (byte)Character.digit(ascii.charAt(0), 16);
        }

        for(i = oddLength; i < res.length; ++i) {
            int v = Character.digit(ascii.charAt(i * 2), 16) << 4;
            v |= Character.digit(ascii.charAt(i * 2 + 1), 16);
            res[i] = (byte)v;
        }

        return res;
    }

    public static String encode(byte[] data) throws IndexOutOfBoundsException {
        StringBuffer buf = new StringBuffer(data.length * 2);
        char[] characters = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

        for(int i = 0; i < data.length; ++i) {
            char c = characters[data[i] >> 4 & 15];
            buf.append(c);
            c = characters[data[i] & 15];
            buf.append(c);
        }

        return buf.toString();
    }

    public static boolean IsNullOrEmpty(String token) {
        return "".equals(token) || token == null;
    }

    public int getHeaderLength() {
        return this.headerLength;
    }

    public void setHeaderLength(int headerLength) {
        this.headerLength = headerLength;
    }

    public String getHeader() {
        return this.header.toString() + '\n' + "( " + DataConversion.convertStringToASCII(this.header.toString()) + " )";
    }

    public int createBitmaps(StringReader str, char[] bitMap, int bitMapLen) {
        long bits = 0L;

        int by;
        try {
            for(int i = 0; i < bitMapLen; ++i) {
                by = Character.digit(bitMap[i], 16);
                if (by == -1) {
                    throw new NumberFormatException("Invalid char found in the bitmap while creating long representation");
                }

                bits <<= 4;
                bits |= (long)by;
            }
        } catch (NumberFormatException var15) {
            this.errorMessage = "Invalid char found in the bitmap";
            return -1;
        }

        this.primaryBitMap = bits;
        long bit1 = -9223372036854775808L;
        if ((bits & bit1) != 0L) {
            int noOfChar = 16;
            char[] secBitMap = new char[noOfChar];

            int ret;
            try {
                ret = str.read(secBitMap, 0, noOfChar);
            } catch (IOException var13) {
                this.errorMessage = "Error occured when reading the Secondary Bitmap string";
                this.invalidTrace = true;
                return -1;
            }

            if (ret == -1) {
                this.errorMessage = "Error occured when reading the Secondary Bitmap string ( EOF reached )";
                this.invalidTrace = true;
                return -1;
            } else {
                this.secondaryBitmapStr = new StringBuffer();
                this.secondaryBitmapStr.append(secBitMap);

                try {
                    for(int i = 0; i < bitMapLen; ++i) {
                        by = Character.digit(secBitMap[i], 16);
                        if (by == -1) {
                            throw new NumberFormatException("Invalid char found in the bitmap while creating long representation");
                        }

                        bits <<= 4;
                        bits |= (long)by;
                    }
                } catch (NumberFormatException var14) {
                    this.errorMessage = "Invalid char found in the bitmap";
                    return -1;
                }

                this.secondaryBitMap = bits;
                return 2;
            }
        } else {
            return 1;
        }
    }

    public int getFieldLength(StringReader str, int maxLength, int bit, boolean isLLL) {
        int ret = 0;
        int intlen = 0;
        char[] len = new char[6];

        try {
            if (isLLL) {
                ret = str.read(len, 0, 6);
            } else {
                ret = str.read(len, 0, 4);
            }
        } catch (IOException var9) {
            return 0;
        } catch (NumberFormatException var10) {
            return 0;
        }

        if (ret == -1) {
            return 0;
        } else {
            String length = new String(len);
            length = DataConversion.convertStringToASCII(length.trim());
            if (!IsNullOrEmpty(length.trim())) {
                length = length.trim();
                intlen = Integer.parseInt(length);
                if (intlen > maxLength) {
                    return 0;
                }
            }

            return intlen;
        }
    }

    public String getFieldValue(StringReader str, boolean isEncoding, boolean calculateLen, int maxFieldLen, int bit, boolean isLLL) {
        String value = "";
        int dataLen = 0;
        StringBuffer tempValue = new StringBuffer();
        int ret = 0;
        if (calculateLen) {
            dataLen = this.getFieldLength(str, maxFieldLen, bit, isLLL);
            dataLen *= 2;
        } else {
            dataLen = maxFieldLen;
        }

        char[] cvalue = new char[dataLen];

        try {
            ret = str.read(cvalue, 0, dataLen);

            for(int k = 0; k < cvalue.length; ++k) {
                tempValue.append(cvalue[k]);
            }
        } catch (IOException var13) {
            return null;
        }

        if (ret == -1) {
            return null;
        } else {
            if (isEncoding) {
                value = tempValue.toString();
                value = DataConversion.convertStringToASCII(value);
            } else {
                value = tempValue.toString();
            }

            if (value.toUpperCase().endsWith("F")) {
                value = value.substring(0, value.length() - 1);
            }

            return value;
        }
    }

    public String getFieldValue(StringReader str, boolean calculateLen, int maxFieldLen, int bit, boolean isLLL) {
        String value = "";
        int dataLen = 0;
        StringBuffer tempValue = new StringBuffer();
        int ret = 0;
        if (calculateLen) {
            dataLen = this.getFieldLength(str, maxFieldLen, bit, isLLL);
            dataLen *= 2;
        } else {
            dataLen = maxFieldLen;
        }

        int noOfChar = dataLen;
        char[] cvalue = new char[dataLen];

        try {
            ret = str.read(cvalue, 0, noOfChar);

            for(int k = 0; k < cvalue.length; ++k) {
                tempValue.append(cvalue[k]);
            }
        } catch (IOException var13) {
            return null;
        }

        if (ret == -1) {
            return null;
        } else {
            value = tempValue.toString();
            value = DataConversion.convertHexToASCII(value);
            return value;
        }
    }

    public String convertTo(String value, String type) {
        String modifiedValue = "";
        if (type.equalsIgnoreCase("ENC_BINRY")) {
            modifiedValue = value;
        } else if (type.equalsIgnoreCase("ENC_PACKD")) {
            modifiedValue = DataConversion.convertNumericsToBCD(value);
        } else {
            if (!type.equalsIgnoreCase("ENC_HEX")) {
                return value;
            }

            modifiedValue = DataConversion.convertASCIIToEBCDICString(value);
        }

        return modifiedValue;
    }

    public String getRightJustifyString(String temp, int length) {
        int j = temp.length();

        for(int i = j; i < length; ++i) {
            temp = "0" + temp;
        }

        return temp;
    }

    public String getLeftJustifyString(String temp, int length) {
        int j = temp.length();

        for(int i = j; i < length; ++i) {
            temp = temp + " ";
        }

        return temp;
    }

    public String appendExtraF(String temp) {
        if (temp.length() % 2 != 0) {
            temp = temp + "F";
        }

        return temp;
    }

    public String getMasked(int length, String symbol) {
        String temp = "";

        for(int i = 0; i < length; ++i) {
            temp = symbol + temp;
        }

        return temp;
    }

    public static boolean isValidMonth(String date, String format) {
        if (date != null && !date.trim().equals("") && format.length() == date.length() && "YYMM".equalsIgnoreCase(format)) {
            String mm = date.substring(2);
            int month = Integer.parseInt(mm);
            return month != 0 && month <= 12;
        } else {
            return false;
        }
    }

    public static boolean isValidCountryCode(String countryCode) {
        boolean isValidCountryCode = false;
        CountryCode[] var5;
        int var4 = (var5 = CountryCode.values()).length;

        for(int var3 = 0; var3 < var4; ++var3) {
            CountryCode countryCodes = var5[var3];
            if (countryCode.equalsIgnoreCase(countryCodes.getCountryCode())) {
                isValidCountryCode = true;
                break;
            }
        }

        return isValidCountryCode;
    }

    public static boolean isValidCurrencyCode(String currencyCode) {
        boolean isValidCurrencyCode = false;
        CurrencyCode[] var5;
        int var4 = (var5 = CurrencyCode.values()).length;

        for(int var3 = 0; var3 < var4; ++var3) {
            CurrencyCode currencyCodes = var5[var3];
            if (currencyCode.equalsIgnoreCase(currencyCodes.getCurrencyCode())) {
                isValidCurrencyCode = true;
                break;
            }
        }

        return isValidCurrencyCode;
    }

    public static boolean isValidFunctionCode(String functionCode) {
        boolean isValidFunctionCode = false;
        FunctionCode[] var5;
        int var4 = (var5 = FunctionCode.values()).length;

        for(int var3 = 0; var3 < var4; ++var3) {
            FunctionCode functionCodes = var5[var3];
            if (functionCode.equalsIgnoreCase(functionCodes.getFunctionCode())) {
                isValidFunctionCode = true;
                break;
            }
        }

        return isValidFunctionCode;
    }

    public static boolean isValidMCCCode(String mccCode) {
        boolean isValidMCCCode = false;
        MCCCode[] var5;
        int var4 = (var5 = MCCCode.values()).length;

        for(int var3 = 0; var3 < var4; ++var3) {
            MCCCode mccCodes = var5[var3];
            if (mccCode.equalsIgnoreCase(mccCodes.getMccCode())) {
                isValidMCCCode = true;
                break;
            }
        }

        return isValidMCCCode;
    }

    public static String convertPaypalMCCCode(String mccCode) {
        if ("3351".equalsIgnoreCase(mccCode)) {
            mccCode = "7512";
        } else if ("6513".equalsIgnoreCase(mccCode)) {
            mccCode = "5978";
        } else if ("6530".equalsIgnoreCase(mccCode)) {
            mccCode = "5947";
        }

        return mccCode;
    }

    public static boolean isValidMessageReasonCode(String messageReasonCode) {
        boolean isValidMessageReasonCode = false;
        MessageReasonCode[] var5;
        int var4 = (var5 = MessageReasonCode.values()).length;

        for(int var3 = 0; var3 < var4; ++var3) {
            MessageReasonCode messageReasonCodes = var5[var3];
            if (messageReasonCode.equalsIgnoreCase(messageReasonCodes.getMessageReasonCode())) {
                isValidMessageReasonCode = true;
                break;
            }
        }

        return isValidMessageReasonCode;
    }

    public static boolean isValidMessageTypeIdentifier(String messageTypeIdentifier) {
        boolean isValidMessageTypeID = false;
        MessageTypeID[] var5;
        int var4 = (var5 = MessageTypeID.values()).length;

        for(int var3 = 0; var3 < var4; ++var3) {
            MessageTypeID messageTypeIds = var5[var3];
            if (messageTypeIdentifier.equalsIgnoreCase(messageTypeIds.getMessageTypeID())) {
                isValidMessageTypeID = true;
                break;
            }
        }

        return isValidMessageTypeID;
    }

    public static boolean isValidProcessingCode(String processingCode) {
        boolean isValidProcessingCode = false;
        ProcessingCode[] var5;
        int var4 = (var5 = ProcessingCode.values()).length;

        for(int var3 = 0; var3 < var4; ++var3) {
            ProcessingCode processingCodes = var5[var3];
            if (processingCode.equalsIgnoreCase(processingCodes.getProcessingCode())) {
                isValidProcessingCode = true;
                break;
            }
        }

        return isValidProcessingCode;
    }

    private static boolean isValidMonth(int month) {
        return month > 0 && month <= 12;
    }

    private static boolean isValidDay(int date, int month, int year)
    {
      boolean isValid = true;
      if ((month == 1) || (month == 3) || (month == 5) || (month == 7) || (month == 8) || 
        (month == 10) || (month == 12))
      {
        if ((date > 31) || (date <= 0)) {
          isValid = false;
        }
      }
      else if ((month == 4) || (month == 6) || (month == 9) || (month == 11))
      {
        if ((date > 30) || (date <= 0)) {
          isValid = false;
        }
      }
      else if (month == 2)
      {
        boolean isLeapYear = year % 4 == 0;
        if (isLeapYear)
        {
          if ((date <= 0) || (date > 29)) {
            isValid = false;
          } else {
            isValid = true;
          }
        }
        else if ((date <= 0) || (date > 28)) {
          isValid = false;
        }
      }
      return isValid;
    }

    private static boolean isValidTime(int hour, int minutes, int seconds) {
        return hour >= 0 && hour <= 23 && minutes >= 0 && minutes <= 59 && seconds >= 0 && seconds <= 59;
    }

    public static boolean isValidDate(String date, String format)
    {
      if ((date != null) && (!date.trim().equals("")) && 
        (format.length() == date.length())) {
        if ("YYMMDDhhmmss".equalsIgnoreCase(format))
        {
          String dd = date.substring(4, 6);
          String mm = date.substring(2, 4);
          String yy = date.substring(0, 2);
          String hr = date.substring(6, 8);
          String min = date.substring(8, 10);
          String sec = date.substring(10, 12);
          int day = Integer.parseInt(dd);
          int month = Integer.parseInt(mm);
          int year = Integer.parseInt(yy);
          int hour = Integer.parseInt(hr);
          int minutes = Integer.parseInt(min);
          int seconds = Integer.parseInt(sec);
          if ((isValidMonth(month)) && (isValidDay(day, month, year)) && 
            (isValidTime(hour, minutes, seconds))) {
            return true;
          }
        }
        else if ("MMDDhhmmss".equalsIgnoreCase(format))
        {
          String dd = date.substring(2, 4);
          String mm = date.substring(0, 2);
          String hr = date.substring(4, 6);
          String min = date.substring(6, 8);
          String sec = date.substring(8, 10);
          
          int day = Integer.parseInt(dd);
          int month = Integer.parseInt(mm);
          int hour = Integer.parseInt(hr);
          int minutes = Integer.parseInt(min);
          int seconds = Integer.parseInt(sec);
          if ((isValidMonth(month)) && (isValidDay(day, month, 0)) && 
            (isValidTime(hour, minutes, seconds))) {
            return true;
          }
        }
      }
      return false;
    }

    public static String padString(String value, int maxLength, String padWith, boolean isLeftJustify, boolean isRightJustify) {
        if (value == null) {
            value = "";
        }

        int length = value.length();

        StringBuffer newValue;
        for(newValue = new StringBuffer(); length < maxLength; ++length) {
            newValue = newValue.append(padWith);
        }

        if (isLeftJustify) {
            value = value + newValue;
        } else if (isRightJustify) {
            value = newValue + value;
        }

        return value;
    }

    public static String getFieldLength(String value) {
        String length = null;
        if (value != null && !"".equals(value)) {
            if (value.length() <= 9) {
                length = "0" + value.length();
            } else {
                length = String.valueOf(value.length());
            }
        } else {
            length = "01";
        }

        return length;
    }

    public static boolean isSENumber(String value) {
        boolean isSENumber = false;
        if (Validator.validateData(value, AuthorizationConstants.VALID_CHAR[4]) && value.length() == 10) {
            isSENumber = true;
        }

        return isSENumber;
    }

    public static String formatMessage(String inMessage, int commonLength) {
        StringBuffer message = new StringBuffer();
        int totalLen = inMessage.length();
        int requiredLength = commonLength;
        int startsFrom = 0;
        if (totalLen > commonLength) {
            while(requiredLength != totalLen) {
                try {
                    message.append(inMessage.subSequence(startsFrom, requiredLength));
                    message.append(NEW_LINE_CHAR);
                    int remainingLength = totalLen - requiredLength;
                    if (commonLength < remainingLength) {
                        startsFrom = requiredLength;
                        requiredLength += commonLength;
                    } else {
                        message.append(inMessage.subSequence(requiredLength, totalLen));
                        requiredLength = totalLen;
                    }
                } catch (Exception var7) {
                    ;
                }
            }
        } else {
            message.append(inMessage);
        }

        return message.toString();
    }

    public String convertNationalUseData(String nationalUseData) {
        StringBuffer modifiedValue = new StringBuffer();
        String primaryId = nationalUseData.substring(0, 2);
        if (primaryId.equalsIgnoreCase("AX")) {
            String convertToData1 = nationalUseData.substring(0, 7);
            this.nationalUseDatalength = convertToData1.length();
            modifiedValue.append(this.convertTo(convertToData1, "ENC_HEX"));
            if (nationalUseData.length() > 7) {
                String data = nationalUseData.substring(7);
                String xid;
                if (data.length() >= 4) {
                    xid = data.substring(0, 4);
                    if (xid.equalsIgnoreCase("AEVV")) {
                        modifiedValue.append(this.convertTo(xid, "ENC_HEX"));
                        data = data.substring(4);
                        this.nationalUseDatalength += xid.length();
                        if (data.length() >= 1) {
                            int xidIndex = data.indexOf("XID");
                            if (xidIndex == -1) {
                                modifiedValue.append(data);
                                this.nationalUseDatalength += data.length() / 2;
                            } else {
                                String firstBCDData = data.substring(0, xidIndex);
                                modifiedValue.append(firstBCDData);
                                this.nationalUseDatalength += firstBCDData.length() / 2;
                                data = data.substring(xidIndex);
                            }
                        } else {
                            modifiedValue.append(data);
                            this.nationalUseDatalength += data.length() / 2;
                            data = "";
                        }
                    }
                }

                if (data.length() >= 3) {
                    xid = data.substring(0, 3);
                    if (xid.equalsIgnoreCase("XID")) {
                        modifiedValue.append(this.convertTo(xid, "ENC_HEX"));
                        this.nationalUseDatalength += xid.length();
                        data = data.substring(3);
                        if (data.length() >= 1) {
                            String secondBCDData = data.substring(0);
                            modifiedValue.append(secondBCDData);
                            this.nationalUseDatalength += secondBCDData.length() / 2;
                        } else {
                            modifiedValue.append(data);
                            this.nationalUseDatalength += data.length() / 2;
                        }
                    }
                }
            }
        } else {
            this.nationalUseDatalength = nationalUseData.length() / 2;
            modifiedValue.append(nationalUseData);
        }

        return modifiedValue.toString();
    }

    public static void main(String[] args) {
        ISOUtil lISOUtil = new ISOUtil();
        String lHex = "C1C2C3C4";
        System.out.println("\t\tAX HEX value\t\t" + lISOUtil.convertTo("AX", "ENC_HEX"));
        System.out.println("\t\tKSN HEX value\t\t" + lISOUtil.convertTo("KSN", "ENC_HEX"));
        System.out.println("\tgetRightJustifyString\t" + lISOUtil.getRightJustifyString("ADADAADADA", 50));
        System.out.println("\tgetLeftJustifyString\t" + lISOUtil.getLeftJustifyString("ADADAADADA", 50));
    }

    public String getPositionMasked(int length) {
        String value = "";

        for(int count = 1; count <= length; ++count) {
            if (count % 2 == 1) {
                value = value + "E";
            } else {
                value = value + "7";
            }
        }

        return value;
    }
}
