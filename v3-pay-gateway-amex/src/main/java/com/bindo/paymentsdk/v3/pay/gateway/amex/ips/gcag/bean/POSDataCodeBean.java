package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean;

import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.connection.PropertyReader;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.Constants;

public class POSDataCodeBean
{
  private String cardDataInput;
  private String cardholderAuthentication;
  private String cardCapture;
  private String operatingEnvironment;
  private String cardholderPresent;
  private String cardPresent;
  private String cardDataInputMode;
  private String cardMemberAuthenticationMethod;
  private String cardMemberAuthenticationEntity;
  private String cardDataOutput;
  private String terminalOutput;
  private String pinCapture;
  
  public String getCardDataInput()
  {
    return this.cardDataInput;
  }
  
  public void setCardDataInput(String cardDataInput)
  {
    this.cardDataInput = cardDataInput;
  }
  
  public String getCardholderAuthentication()
  {
    return this.cardholderAuthentication;
  }
  
  public void setCardholderAuthentication(String cardholderAuthentication)
  {
    this.cardholderAuthentication = cardholderAuthentication;
  }
  
  public String getCardCapture()
  {
    return this.cardCapture;
  }
  
  public void setCardCapture(String cardCapture)
  {
    this.cardCapture = cardCapture;
  }
  
  public String getOperatingEnvironment()
  {
    return this.operatingEnvironment;
  }
  
  public void setOperatingEnvironment(String operatingEnvironment)
  {
    this.operatingEnvironment = operatingEnvironment;
  }
  
  public String getCardholderPresent()
  {
    return this.cardholderPresent;
  }
  
  public void setCardholderPresent(String cardholderPresent)
  {
    this.cardholderPresent = cardholderPresent;
  }
  
  public String getCardPresent()
  {
    return this.cardPresent;
  }
  
  public void setCardPresent(String cardPresent)
  {
    this.cardPresent = cardPresent;
  }
  
  public String getCardDataInputMode()
  {
    return this.cardDataInputMode;
  }
  
  public void setCardDataInputMode(String cardDataInputMode)
  {
    this.cardDataInputMode = cardDataInputMode;
  }
  
  public String getCardMemberAuthenticationMethod()
  {
    return this.cardMemberAuthenticationMethod;
  }
  
  public void setCardMemberAuthenticationMethod(String cardMemberAuthenticationMethod)
  {
    this.cardMemberAuthenticationMethod = cardMemberAuthenticationMethod;
  }
  
  public String getCardMemberAuthenticationEntity()
  {
    return this.cardMemberAuthenticationEntity;
  }
  
  public void setCardMemberAuthenticationEntity(String cardMemberAuthenticationEntity)
  {
    this.cardMemberAuthenticationEntity = cardMemberAuthenticationEntity;
  }
  
  public String getCardDataOutput()
  {
    return this.cardDataOutput;
  }
  
  public void setCardDataOutput(String cardDataOutput)
  {
    this.cardDataOutput = cardDataOutput;
  }
  
  public String getTerminalOutput()
  {
    return this.terminalOutput;
  }
  
  public void setTerminalOutput(String terminalOutput)
  {
    this.terminalOutput = terminalOutput;
  }
  
  public String getPinCapture()
  {
    return this.pinCapture;
  }
  
  public void setPinCapture(String pinCapture)
  {
    this.pinCapture = pinCapture;
  }
  
  public String populatePointOfServiceDataCode()
  {
    StringBuffer posData = new StringBuffer();
    posData.append(this.cardDataInput);
    posData.append(this.cardholderAuthentication);
    posData.append(this.cardCapture);
    posData.append(this.operatingEnvironment);
    posData.append(this.cardholderPresent);
    posData.append(this.cardPresent);
    posData.append(this.cardDataInputMode);
    posData.append(this.cardMemberAuthenticationMethod);
    posData.append(this.cardMemberAuthenticationEntity);
    posData.append(this.cardDataOutput);
    posData.append(this.terminalOutput);
    posData.append(this.pinCapture);
    
    StringBuffer displayBuffer = new StringBuffer();
    
    displayBuffer.append("CARD_DATA_INPUT          ").append(" :: ").append(this.cardDataInput).append(Constants.NEW_LINE_CHAR);
    displayBuffer.append("CARD_HOLDER_AUTH         ").append(" :: ").append(this.cardholderAuthentication).append(Constants.NEW_LINE_CHAR);
    displayBuffer.append("CARD_CAPTURE             ").append(" :: ").append(this.cardCapture).append(Constants.NEW_LINE_CHAR);
    displayBuffer.append("OPERATING_ENV            ").append(" :: ").append(this.operatingEnvironment).append(Constants.NEW_LINE_CHAR);
    displayBuffer.append("CARD_HOLDER_PRESENT      ").append(" :: ").append(this.cardholderPresent).append(Constants.NEW_LINE_CHAR);
    displayBuffer.append("CARD_PRESENT             ").append(" :: ").append(this.cardPresent).append(Constants.NEW_LINE_CHAR);
    displayBuffer.append("CARD_DATA_INPUT_MODE     ").append(" :: ").append(this.cardDataInputMode).append(Constants.NEW_LINE_CHAR);
    displayBuffer.append("CARD_MEMBR_AUTH_MTHD     ").append(" :: ").append(this.cardMemberAuthenticationMethod).append(Constants.NEW_LINE_CHAR);
    displayBuffer.append("CARD_MEMBR_AUTH_ENTITY   ").append(" :: ").append(this.cardMemberAuthenticationEntity).append(Constants.NEW_LINE_CHAR);
    displayBuffer.append("CARD_DATA_OUTPUT         ").append(" :: ").append(this.cardDataOutput).append(Constants.NEW_LINE_CHAR);
    displayBuffer.append("TRMNL_OUTPUT             ").append(" :: ").append(this.terminalOutput).append(Constants.NEW_LINE_CHAR);
    displayBuffer.append("PIN_CAPTURE              ").append(" :: ").append(this.pinCapture).append(Constants.NEW_LINE_CHAR);
    boolean debugflag = PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON");
    if (debugflag)
    {
      Log.i("POD Data Bean\r\n----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n-                                 Point Of Service Data Bean                                                                                                                                       -\r\n---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n" +
      
        displayBuffer.toString() + 
        "\r\n" + "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
      
      Log.i("Populated Point Of Service Data Code is -- " + posData);
    }
    return posData.toString();
  }
}
