package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.confirmation;

public class ConfirmationReportBodyBean
{
  private String recordType;
  private String reportID;
  private String reportName;
  private String submitterName;
  private String reportDateTime;
  private String submitterID;
  private String submitterFileRefNumber;
  private String fileStatus;
  private String fileCreationDateTime;
  private String fileTrackingNumber;
  private String fileReceiptDateTime;
  private String totalNumberOfRecords;
  private String hashTotalAmount;
  private String totalNumberOfDebits;
  private String debitHashTotalAmount;
  private String totalNumberOfCredits;
  private String creditHashTotalAmount;
  private String totalRejectedRecords;
  private String totalSuspendedBatches;
  private String totalSuspendedRecords;
  private String bdyReserved;
  
  public String getRecordType()
  {
    return this.recordType;
  }
  
  public void setRecordType(String recordType)
  {
    this.recordType = recordType;
  }
  
  public String getReportID()
  {
    return this.reportID;
  }
  
  public void setReportID(String reportID)
  {
    this.reportID = reportID;
  }
  
  public String getReportName()
  {
    return this.reportName;
  }
  
  public void setReportName(String reportName)
  {
    this.reportName = reportName;
  }
  
  public String getSubmitterName()
  {
    return this.submitterName;
  }
  
  public void setSubmitterName(String submitterName)
  {
    this.submitterName = submitterName;
  }
  
  public String getReportDateTime()
  {
    return this.reportDateTime;
  }
  
  public void setReportDateTime(String reportDateTime)
  {
    this.reportDateTime = reportDateTime;
  }
  
  public String getSubmitterID()
  {
    return this.submitterID;
  }
  
  public void setSubmitterID(String submitterID)
  {
    this.submitterID = submitterID;
  }
  
  public String getSubmitterFileRefNumber()
  {
    return this.submitterFileRefNumber;
  }
  
  public void setSubmitterFileRefNumber(String submitterFileRefNumber)
  {
    this.submitterFileRefNumber = submitterFileRefNumber;
  }
  
  public String getFileStatus()
  {
    return this.fileStatus;
  }
  
  public void setFileStatus(String fileStatus)
  {
    this.fileStatus = fileStatus;
  }
  
  public String getFileCreationDateTime()
  {
    return this.fileCreationDateTime;
  }
  
  public void setFileCreationDateTime(String fileCreationDateTime)
  {
    this.fileCreationDateTime = fileCreationDateTime;
  }
  
  public String getFileTrackingNumber()
  {
    return this.fileTrackingNumber;
  }
  
  public void setFileTrackingNumber(String fileTrackingNumber)
  {
    this.fileTrackingNumber = fileTrackingNumber;
  }
  
  public String getFileReceiptDateTime()
  {
    return this.fileReceiptDateTime;
  }
  
  public void setFileReceiptDateTime(String fileReceiptDateTime)
  {
    this.fileReceiptDateTime = fileReceiptDateTime;
  }
  
  public String getTotalNumberOfRecords()
  {
    return this.totalNumberOfRecords;
  }
  
  public void setTotalNumberOfRecords(String totalNumberOfRecords)
  {
    this.totalNumberOfRecords = totalNumberOfRecords;
  }
  
  public String getHashTotalAmount()
  {
    return this.hashTotalAmount;
  }
  
  public void setHashTotalAmount(String hashTotalAmount)
  {
    this.hashTotalAmount = hashTotalAmount;
  }
  
  public String getTotalNumberOfDebits()
  {
    return this.totalNumberOfDebits;
  }
  
  public void setTotalNumberOfDebits(String totalNumberOfDebits)
  {
    this.totalNumberOfDebits = totalNumberOfDebits;
  }
  
  public String getDebitHashTotalAmount()
  {
    return this.debitHashTotalAmount;
  }
  
  public void setDebitHashTotalAmount(String debitHashTotalAmount)
  {
    this.debitHashTotalAmount = debitHashTotalAmount;
  }
  
  public String getTotalNumberOfCredits()
  {
    return this.totalNumberOfCredits;
  }
  
  public void setTotalNumberOfCredits(String totalNumberOfCredits)
  {
    this.totalNumberOfCredits = totalNumberOfCredits;
  }
  
  public String getCreditHashTotalAmount()
  {
    return this.creditHashTotalAmount;
  }
  
  public void setCreditHashTotalAmount(String creditHashTotalAmount)
  {
    this.creditHashTotalAmount = creditHashTotalAmount;
  }
  
  public String getTotalRejectedRecords()
  {
    return this.totalRejectedRecords;
  }
  
  public void setTotalRejectedRecords(String totalRejectedRecords)
  {
    this.totalRejectedRecords = totalRejectedRecords;
  }
  
  public String getTotalSuspendedBatches()
  {
    return this.totalSuspendedBatches;
  }
  
  public void setTotalSuspendedBatches(String totalSuspendedBatches)
  {
    this.totalSuspendedBatches = totalSuspendedBatches;
  }
  
  public String getTotalSuspendedRecords()
  {
    return this.totalSuspendedRecords;
  }
  
  public void setTotalSuspendedRecords(String totalSuspendedRecords)
  {
    this.totalSuspendedRecords = totalSuspendedRecords;
  }
  
  public String getBdyReserved()
  {
    return this.bdyReserved;
  }
  
  public void setBdyReserved(String bdyReserved)
  {
    this.bdyReserved = bdyReserved;
  }
}
