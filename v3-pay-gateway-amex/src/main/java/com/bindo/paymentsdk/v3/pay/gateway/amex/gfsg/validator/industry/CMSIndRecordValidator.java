package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.industry;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.ErrorObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceAddendumBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceBasicBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.CommunicationServicesIndustryBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.SubmissionErrorCodes;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.exceptions.SettlementException;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.CommonValidator;
import java.util.List;

public class CMSIndRecordValidator
{
  public static void validateCMSIndRecord(TransactionAdviceBasicBean transactionAdviceBasicType, TransactionAdviceAddendumBean transactionAddendumType, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    CommunicationServicesIndustryBean communicationServicesIndustryBean = (CommunicationServicesIndustryBean)transactionAddendumType;
    
    TAADBRecordValidator.validateRecordType(
      communicationServicesIndustryBean.getRecordType(), errorCodes);
    
    TAADBRecordValidator.validateRecordNumber(communicationServicesIndustryBean
      .getRecordNumber(), errorCodes);
    TAADBRecordValidator.validateTransactionIdentifier(
      communicationServicesIndustryBean.getTransactionIdentifier(), 
      transactionAdviceBasicType, errorCodes);
    TAADBRecordValidator.validateAddendaTypeCode(
      communicationServicesIndustryBean.getAddendaTypeCode(), 
      errorCodes);
    TAADBRecordValidator.validateFormatCode(
      communicationServicesIndustryBean.getFormatCode(), 
      transactionAdviceBasicType, errorCodes);
    
    validateCallDate(communicationServicesIndustryBean.getCallDate(), 
      errorCodes);
    validateCallTime(communicationServicesIndustryBean.getCallTime(), 
      errorCodes);
    validateCallDurationTime(communicationServicesIndustryBean
      .getCallDurationTime(), errorCodes);
    validateCallFromLocationName(communicationServicesIndustryBean
      .getCallFromLocationName(), errorCodes);
    validateCallFromRegionCode(communicationServicesIndustryBean
      .getCallFromRegionCode(), errorCodes);
    validateCallFromCountryCOde(communicationServicesIndustryBean
      .getCallFromCountryCode(), errorCodes);
    
    validateCallFromPhoneNumber(communicationServicesIndustryBean
      .getCallFromPhoneNumber(), errorCodes);
    validateCallToLocationName(communicationServicesIndustryBean
      .getCallToLocationName(), errorCodes);
    
    validateCallToRegionCode(communicationServicesIndustryBean
      .getCallToRegionCode(), errorCodes);
    validateCallToCountryCode(communicationServicesIndustryBean
      .getCallToCountryCode(), errorCodes);
    
    validateCallToPhoneNumber(communicationServicesIndustryBean
      .getCallToPhoneNumber(), errorCodes);
    
    validatePhoneCardId(communicationServicesIndustryBean.getPhoneCardId(), 
      errorCodes);
    validateServiceDescription(communicationServicesIndustryBean
      .getServiceDescription(), errorCodes);
    validateBillingPeriod(communicationServicesIndustryBean
      .getBillingPeriod(), errorCodes);
    
    validateCommunicationsCallTypeCode(communicationServicesIndustryBean
      .getCommunicationsCallTypeCode(), errorCodes);
    
    validateCommunicationsRateClass(communicationServicesIndustryBean
      .getCommunicationsRateClass(), errorCodes);
  }
  
  private static void validateCallDate(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD262_CALLDATE);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ĕ']))
    {
      String reqLength = CommonValidator.validateLength(value, 8, 8);
      if ((!reqLength.equals("equal")) || 
      
        (!CommonValidator.isValidDate(value, "CCYYMMDD")))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD262_CALLDATE);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD262_CALLDATE);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateCallTime(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ĕ']))
      {
        String reqLength = CommonValidator.validateLength(value, 6, 6);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD263_CALLDATECALL_TIME);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD263_CALLDATECALL_TIME);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateCallDurationTime(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD264_CALL_DURATIONTIME);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ė']))
    {
      String reqLength = CommonValidator.validateLength(value, 6, 6);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD264_CALL_DURATIONTIME);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD264_CALL_DURATIONTIME);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateCallFromLocationName(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD265_CALLFROM_LOCATIONNAME);
      errorCodes.add(errorObj);
    }
    else
    {
      String reqLength = 
        CommonValidator.validateLength(value, 18, 18);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD266_CALLFROM_LOCATIONNAME);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateCallFromRegionCode(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ę']))
      {
        String reqLength = CommonValidator.validateLength(value, 3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD268_CALLFROM_REGIONCODE);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD269_CALLFROM_REGIONCODE);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateCallFromCountryCOde(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ę']))
      {
        String reqLength = CommonValidator.validateLength(value, 3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD271_CALLFROM_COUNTRYCODE);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD270_CALLFROM_COUNTRYCODE);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateCallFromPhoneNumber(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD272_CALLFROM_PHONENUMBER);
      errorCodes.add(errorObj);
    }
    else
    {
      String reqLength = 
        CommonValidator.validateLength(value, 16, 16);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD274_CALLFROM_PHONENUMBER);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateCallToLocationName(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD275_CALLTO_LOCATIONNAME);
      errorCodes.add(errorObj);
    }
    else
    {
      String reqLength = 
        CommonValidator.validateLength(value, 18, 18);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD277_CALLTO_LOCATIONNAME);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateCallToRegionCode(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ĝ']))
      {
        String reqLength = CommonValidator.validateLength(value, 3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD279_CALLTO_REGIONCODE);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD278_CALLTO_REGIONCODE);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateCallToCountryCode(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ĝ']))
      {
        String reqLength = CommonValidator.validateLength(value, 3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD280_CALLTO_COUNTRYCODE);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD279_CALLTO_COUNTRYCODE);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateCallToPhoneNumber(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD281_CALLTO_PHONENUMBER);
      errorCodes.add(errorObj);
    }
    else
    {
      String reqLength = 
        CommonValidator.validateLength(value, 16, 16);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD283_CALLTO_PHONENUMBER);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validatePhoneCardId(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = CommonValidator.validateLength(value, 8, 8);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD284_PHONECARDID);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateServiceDescription(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 20, 20);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD286_SERVICE_DESCRIPTION);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateBillingPeriod(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 18, 18);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD288_BILLING_PERIOD);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateCommunicationsCallTypeCode(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    if ((!CommonValidator.isNullOrEmpty(value)) && 
      (!CommonValidator.isValidCommunicationsCallTypeCode(value)))
    {
      ErrorObject errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD289_COMMUNICATIONS_CALLTYPECODE);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateCommunicationsRateClass(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ģ']))
      {
        String reqLength = CommonValidator.validateLength(value, 1, 1);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD292_COMMUNICATIONS_RATECLASS);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD293_COMMUNICATIONS_RATECLASS);
        errorCodes.add(errorObj);
      }
    }
  }
}
