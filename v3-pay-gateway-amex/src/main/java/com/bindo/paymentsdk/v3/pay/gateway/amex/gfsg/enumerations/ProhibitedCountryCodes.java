package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations;

public enum ProhibitedCountryCodes {
    Cuba("192"),
    Islamic_Republic_Of_Iran("364"),
    Democratic_Peoples_Republic_Of_Korea("408"),
    Myanmar("104"),
    Sudan("736");

    private final String countryCd;

    private ProhibitedCountryCodes(String ctyCd) {
        this.countryCd = ctyCd;
    }

    public String getCountryCode() {
        return this.countryCd;
    }
}
