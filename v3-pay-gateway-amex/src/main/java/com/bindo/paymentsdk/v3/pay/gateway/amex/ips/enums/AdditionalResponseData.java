package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums;

public enum AdditionalResponseData {
    CORRECTBILLINGPOSTALADDRESS("Y", "Yes, Billing Address and Postal Code are both correct."),
    INCORRECTBILLINGPOSTALADDRESS("N", "No, Billing Address and Postal Code are both incorrect."),
    CORRECTBILLINGADDRESS("A", "Billing Address only correct."),
    CORRECTBILLINGPOSTAL("Z", "Billing Postal Code only correct."),
    INFORMATIONUNAVAILABLE("U", "Information unavailable."),
    SENOTALLOWEDAAVFUNCTION("S", "SE not allowed AAV function"),
    SYSTEMUNAVAILABLE("R", "System unavailable; retry."),
    CMNAMEBILLINGPOSTALCODEMATCH("L", "CM Name and Billing Postal Code match."),
    CMNAMEBILLINGADDRESSPOSTALCODEMATCH("M", "CM Name, Billing Address and Postal Code match."),
    CMNAMEBILLINGADDRESSMATCH("O", "CM Name and Billing Address match."),
    CMNAMEMATCH("K", "CM Name matches."),
    CMNAMEINCORRECTBILLINGPOSTALCODEMATCH("D", "CM Name incorrect, Billing Postal Code matches."),
    CMNAMEINCORRECTBILLINGADDRESSPOSTALCODEMATCH("E", "CM Name incorrect, Billing Address and Postal Code match."),
    CMNAMEINCORRECTBILLINGADDRESSMATCH("F", "CM Name incorrect, Billing Address matches."),
    ALLINCORRECT("W", "No, CM Name, Billing Address and Postal Code are all incorrect"),
    CID_MATCHED(" Y", "CID/4DBC/4CSC matched"),
    CID_NOT_MATCHED(" N", "CID/4DBC/4CSC did not match"),
    CID_NOT_CHECKED(" U", "CID/4DBC/4CSC was not checked");

    private final String additionalResponseData;
    private final String additionalResponseDataDesc;

    private AdditionalResponseData(String additionalResponseData, String additionalResponseDataDesc) {
        this.additionalResponseData = additionalResponseData;
        this.additionalResponseDataDesc = additionalResponseDataDesc;
    }

    public String getAdditionalResponseData() {
        return this.additionalResponseData;
    }

    public String getAdditionalResponseDataDesc() {
        return this.additionalResponseDataDesc;
    }
}
