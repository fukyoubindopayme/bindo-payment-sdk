package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.ErrorObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceBasicBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceDetailBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.RecordType;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.SubmissionErrorCodes;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.exceptions.SettlementException;
import java.util.List;

public class TADRecValidator
{
  private static String recordNumber;
  private static String recordType;
  
  public static void validateTADRecord(TransactionAdviceBasicBean transactionAdviceBasicType, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    TransactionAdviceDetailBean transactionAdviceDetailBean = transactionAdviceBasicType
      .getTransactionAdviceDetailBean();
    recordNumber = transactionAdviceDetailBean.getRecordNumber();
    recordType = transactionAdviceDetailBean.getRecordType();
    validateRecordType(transactionAdviceDetailBean.getRecordType(), 
      errorCodes);
    validateRecordNumber(transactionAdviceDetailBean.getRecordNumber(), 
      errorCodes);
    validateTransactionIdentifier(transactionAdviceDetailBean
      .getTransactionIdentifier(), transactionAdviceBasicType, 
      errorCodes);
    validateAdditionalAmountType1(transactionAdviceDetailBean, errorCodes);
    validateAdditionalAmount1(transactionAdviceDetailBean, errorCodes);
    validateAdditionalAmountSign1(transactionAdviceDetailBean
      .getAdditionalAmountSign1(), errorCodes);
    validateAdditionalAmountType2(transactionAdviceDetailBean, errorCodes);
    validateAdditionalAmount2(transactionAdviceDetailBean, errorCodes);
    validateAdditionalAmountSign2(transactionAdviceDetailBean
      .getAdditionalAmountSign2(), errorCodes);
    validateAdditionalAmountType3(transactionAdviceDetailBean, errorCodes);
    validateAdditionalAmount3(transactionAdviceDetailBean, errorCodes);
    validateAdditionalAmountSign3(transactionAdviceDetailBean
      .getAdditionalAmountSign3(), errorCodes);
    validateAdditionalAmountType4(transactionAdviceDetailBean, errorCodes);
    validateAdditionalAmount4(transactionAdviceDetailBean, errorCodes);
    validateAdditionalAmountSign4(transactionAdviceDetailBean
      .getAdditionalAmountSign4(), errorCodes);
    validateAdditionalAmountType5(transactionAdviceDetailBean, errorCodes);
    validateAdditionalAmount5(transactionAdviceDetailBean, errorCodes);
    validateAdditionalAmountSign5(transactionAdviceDetailBean
      .getAdditionalAmountSign5(), errorCodes);
  }
  
  private static void validateRecordType(String recordType, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(recordType))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE
        .getErrorDescription() + 
        "\n" + 
        "TFH" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "RecordType:" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (!RecordType.Transaction_Advice_Detail.getRecordType().equalsIgnoreCase(recordType))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateRecordNumber(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD585_RECORDNUMBER
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD585_RECORDNUMBER
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "RecordNumber:" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[88]))
    {
      String reqLength = CommonValidator.validateLength(value, 8, 8);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD3_RECORDNUMBER
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD3_RECORDNUMBER
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "RecordNumber:" + 
          "|" + 
          "This field length Cannot be greater than 8");
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD2_RECORDNUMBER
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD2_RECORDNUMBER
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "RecordNumber:" + 
        "|" + 
        "This field can only be Numeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateTransactionIdentifier(String value, TransactionAdviceBasicBean transactionAdviceBasicType, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "TransactionIdentifier" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[89]))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 15, 15);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "TransactionIdentifier" + 
          "|" + 
          "This field length Cannot be greater than 15");
        errorCodes.add(errorObj);
      }
      else if (!value.equalsIgnoreCase(transactionAdviceBasicType.getTransactionIdentifier()))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD67_TRANSACTION_IDENTIFIER
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD67_TRANSACTION_IDENTIFIER
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "TransactionIdentifier" + 
          "|" + 
          SubmissionErrorCodes.ERROR_DATAFIELD67_TRANSACTION_IDENTIFIER
          .getErrorDescription());
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "TransactionIdentifier" + 
        "|" + 
        "This field can only be Numeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateAdditionalAmountType1(TransactionAdviceDetailBean transactionAdviceDetailBean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(transactionAdviceDetailBean.getAdditionalAmountType1())) {
      if (CommonValidator.validateData(transactionAdviceDetailBean.getAdditionalAmountType1(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[90]))
      {
        String reqLength = CommonValidator.validateLength(
          transactionAdviceDetailBean.getAdditionalAmountType1(), 
          3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD86_ADDITIONAL_AMOUNTTYPE1
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD86_ADDITIONAL_AMOUNTTYPE1
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            recordType + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "AdditionalAmountType1" + 
            "|" + 
            "This field length Cannot be greater than 3");
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD86_ADDITIONAL_AMOUNTTYPE1
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD86_ADDITIONAL_AMOUNTTYPE1
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "AdditionalAmountType1" + 
          "|" + 
          "This field can only be AlphaNumeric");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateAdditionalAmount1(TransactionAdviceDetailBean transactionAdviceDetailBean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(transactionAdviceDetailBean.getAdditionalAmount1())) {
      if (CommonValidator.validateData(transactionAdviceDetailBean.getAdditionalAmount1(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[92]))
      {
        String reqLength = CommonValidator.validateLength(
          transactionAdviceDetailBean.getAdditionalAmount1(), 12, 
          12);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD90_ADDITIONAL_AMOUNT1
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD90_ADDITIONAL_AMOUNT1
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            recordType + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "AdditionalAmount1" + 
            "|" + 
            "This field length Cannot be greater than 12");
          errorCodes.add(errorObj);
        }
        else if (CommonValidator.isNullOrEmpty(transactionAdviceDetailBean.getAdditionalAmountType1()))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD87_ADDITIONAL_AMOUNTTYPE1
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD87_ADDITIONAL_AMOUNTTYPE1
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            recordType + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "AdditionalAmount1" + 
            "|" + 
            SubmissionErrorCodes.ERROR_DATAFIELD87_ADDITIONAL_AMOUNTTYPE1
            .getErrorDescription());
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD88_ADDITIONAL_AMOUNT1
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD88_ADDITIONAL_AMOUNT1
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "AdditionalAmount1" + 
          "|" + 
          "This field can only be Numeric");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateAdditionalAmountSign1(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[93]))
      {
        String reqLength = CommonValidator.validateLength(value, 1, 1);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD91_ADDITIONAL_AMOUNT_SIGN1
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD91_ADDITIONAL_AMOUNT_SIGN1
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            recordType + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "AdditionalAmountSign1" + 
            "|" + 
            "This field length Cannot be greater than 1");
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD91_ADDITIONAL_AMOUNT_SIGN1
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD91_ADDITIONAL_AMOUNT_SIGN1
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "AdditionalAmountSign1" + 
          "|" + 
          "This field can only be AlphaNumeric");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateAdditionalAmountType2(TransactionAdviceDetailBean transactionAdviceDetailBean, List<ErrorObject> errorCodes)
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(transactionAdviceDetailBean.getAdditionalAmountType2())) {
      if (CommonValidator.validateData(transactionAdviceDetailBean.getAdditionalAmountType2(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[94]))
      {
        String reqLength = CommonValidator.validateLength(
          transactionAdviceDetailBean.getAdditionalAmountType2(), 
          3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD92_ADDITIONAL_AMOUNTTYPE2
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD92_ADDITIONAL_AMOUNTTYPE2
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            recordType + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "AdditionalAmountType2" + 
            "|" + 
            "This field length Cannot be greater than 3");
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD92_ADDITIONAL_AMOUNTTYPE2
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD92_ADDITIONAL_AMOUNTTYPE2
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "AdditionalAmountType2" + 
          "|" + 
          "This field can only be AlphaNumeric");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateAdditionalAmount2(TransactionAdviceDetailBean transactionAdviceDetailBean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(transactionAdviceDetailBean.getAdditionalAmount2())) {
      if (CommonValidator.validateData(transactionAdviceDetailBean.getAdditionalAmount2(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[96]))
      {
        String reqLength = CommonValidator.validateLength(
          transactionAdviceDetailBean.getAdditionalAmount2(), 12, 
          12);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD96_ADDITIONAL_AMOUNT2
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD96_ADDITIONAL_AMOUNT2
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            recordType + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "AdditionalAmount2" + 
            "|" + 
            "This field length Cannot be greater than 12");
          errorCodes.add(errorObj);
        }
        else if (CommonValidator.isNullOrEmpty(transactionAdviceDetailBean.getAdditionalAmountType2()))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD93_ADDITIONAL_AMOUNTTYPE2
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD93_ADDITIONAL_AMOUNTTYPE2
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            recordType + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "AdditionalAmount2" + 
            "|" + 
            SubmissionErrorCodes.ERROR_DATAFIELD93_ADDITIONAL_AMOUNTTYPE2
            .getErrorDescription());
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD94_ADDITIONAL_AMOUNT2
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD94_ADDITIONAL_AMOUNT2
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "AdditionalAmount2" + 
          "|" + 
          "This field can only be Numeric");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateAdditionalAmountSign2(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[97]))
      {
        String reqLength = CommonValidator.validateLength(value, 1, 1);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD97_ADDITIONAL_AMOUNT_SIGN2
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD97_ADDITIONAL_AMOUNT_SIGN2
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            recordType + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "AdditionalAmountSign2" + 
            "|" + 
            "This field length Cannot be greater than 1");
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD97_ADDITIONAL_AMOUNT_SIGN2
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD97_ADDITIONAL_AMOUNT_SIGN2
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "AdditionalAmountSign2" + 
          "|" + 
          "This field can only be AlphaNumeric");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateAdditionalAmountType3(TransactionAdviceDetailBean transactionAdviceDetailBean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(transactionAdviceDetailBean.getAdditionalAmountType3())) {
      if (CommonValidator.validateData(transactionAdviceDetailBean.getAdditionalAmountType3(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[98]))
      {
        String reqLength = CommonValidator.validateLength(
          transactionAdviceDetailBean.getAdditionalAmountType3(), 
          3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD98_ADDITIONAL_AMOUNTTYPE3
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD98_ADDITIONAL_AMOUNTTYPE3
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            recordType + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "AdditionalAmountType3" + 
            "|" + 
            "This field length Cannot be greater than 3");
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD98_ADDITIONAL_AMOUNTTYPE3
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD98_ADDITIONAL_AMOUNTTYPE3
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "AdditionalAmountType3" + 
          "|" + 
          "This field can only be AlphaNumeric");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateAdditionalAmount3(TransactionAdviceDetailBean transactionAdviceDetailBean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(transactionAdviceDetailBean.getAdditionalAmount3())) {
      if (CommonValidator.validateData(transactionAdviceDetailBean.getAdditionalAmount3(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[100]))
      {
        String reqLength = CommonValidator.validateLength(
          transactionAdviceDetailBean.getAdditionalAmount3(), 12, 
          12);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD102_ADDITIONAL_AMOUNT3
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD102_ADDITIONAL_AMOUNT3
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            recordType + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "AdditionalAmount3" + 
            "|" + 
            "This field length Cannot be greater than 12");
          errorCodes.add(errorObj);
        }
        else if (CommonValidator.isNullOrEmpty(transactionAdviceDetailBean.getAdditionalAmountType3()))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD99_ADDITIONAL_AMOUNTTYPE3
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD99_ADDITIONAL_AMOUNTTYPE3
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            recordType + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "AdditionalAmount3" + 
            "|" + 
            SubmissionErrorCodes.ERROR_DATAFIELD99_ADDITIONAL_AMOUNTTYPE3
            .getErrorDescription());
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD100_ADDITIONAL_AMOUNT3
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD100_ADDITIONAL_AMOUNT3
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "AdditionalAmount3" + 
          "|" + 
          "This field can only be Numeric");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateAdditionalAmountSign3(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[101]))
      {
        String reqLength = CommonValidator.validateLength(value, 1, 1);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD103_ADDITIONAL_AMOUNT_SIGN3
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD103_ADDITIONAL_AMOUNT_SIGN3
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            recordType + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "AdditionalAmountSign3" + 
            "|" + 
            "This field length Cannot be greater than 1");
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD103_ADDITIONAL_AMOUNT_SIGN3
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD103_ADDITIONAL_AMOUNT_SIGN3
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "AdditionalAmountSign3" + 
          "|" + 
          "This field can only be AlphaNumeric");
        
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateAdditionalAmountType4(TransactionAdviceDetailBean transactionAdviceDetailBean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(transactionAdviceDetailBean.getAdditionalAmountType4())) {
      if (CommonValidator.validateData(transactionAdviceDetailBean.getAdditionalAmountType4(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[102]))
      {
        String reqLength = CommonValidator.validateLength(
          transactionAdviceDetailBean.getAdditionalAmountType4(), 
          3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD104_ADDITIONAL_AMOUNTTYPE4
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD104_ADDITIONAL_AMOUNTTYPE4
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            recordType + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "AdditionalAmountType4" + 
            "|" + 
            "This field length Cannot be greater than 3");
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD104_ADDITIONAL_AMOUNTTYPE4
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD104_ADDITIONAL_AMOUNTTYPE4
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "AdditionalAmountType4" + 
          "|" + 
          "This field can only be AlphaNumeric");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateAdditionalAmount4(TransactionAdviceDetailBean transactionAdviceDetailBean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(transactionAdviceDetailBean.getAdditionalAmount4())) {
      if (CommonValidator.validateData(transactionAdviceDetailBean.getAdditionalAmount4(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[104]))
      {
        String reqLength = CommonValidator.validateLength(
          transactionAdviceDetailBean.getAdditionalAmount4(), 12, 
          12);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD108_ADDITIONAL_AMOUNT4
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD108_ADDITIONAL_AMOUNT4
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            recordType + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "AdditionalAmount4" + 
            "|" + 
            "This field length Cannot be greater than 12");
          errorCodes.add(errorObj);
        }
        else if (CommonValidator.isNullOrEmpty(transactionAdviceDetailBean.getAdditionalAmountType4()))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD105_ADDITIONAL_AMOUNTTYPE4
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD105_ADDITIONAL_AMOUNTTYPE4
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            recordType + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "AdditionalAmount4" + 
            "|" + 
            SubmissionErrorCodes.ERROR_DATAFIELD105_ADDITIONAL_AMOUNTTYPE4
            .getErrorDescription());
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD106_ADDITIONAL_AMOUNT4
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD106_ADDITIONAL_AMOUNT4
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "AdditionalAmount4" + 
          "|" + 
          "This field can only be Numeric");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateAdditionalAmountSign4(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[105]))
      {
        String reqLength = CommonValidator.validateLength(value, 1, 1);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD109_ADDITIONAL_AMOUNT_SIGN4
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD109_ADDITIONAL_AMOUNT_SIGN4
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            recordType + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "AdditionalAmountSign4" + 
            "|" + 
            "This field length Cannot be greater than 1");
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD109_ADDITIONAL_AMOUNT_SIGN4
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD109_ADDITIONAL_AMOUNT_SIGN4
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "AdditionalAmountSign4" + 
          "|" + 
          "This field can only be AlphaNumeric");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateAdditionalAmountType5(TransactionAdviceDetailBean transactionAdviceDetailBean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(transactionAdviceDetailBean.getAdditionalAmountType5())) {
      if (CommonValidator.validateData(transactionAdviceDetailBean.getAdditionalAmountType5(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[106]))
      {
        String reqLength = CommonValidator.validateLength(
          transactionAdviceDetailBean.getAdditionalAmountType5(), 
          3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD110_ADDITIONAL_AMOUNTTYPE5
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD110_ADDITIONAL_AMOUNTTYPE5
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            recordType + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "AdditionalAmountType5" + 
            "|" + 
            "This field length Cannot be greater than 3");
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD110_ADDITIONAL_AMOUNTTYPE5
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD110_ADDITIONAL_AMOUNTTYPE5
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "AdditionalAmountType5" + 
          "|" + 
          "This field can only be AlphaNumeric");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateAdditionalAmount5(TransactionAdviceDetailBean transactionAdviceDetailBean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(transactionAdviceDetailBean.getAdditionalAmount5())) {
      if (CommonValidator.validateData(transactionAdviceDetailBean.getAdditionalAmount5(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[108]))
      {
        String reqLength = CommonValidator.validateLength(
          transactionAdviceDetailBean.getAdditionalAmount5(), 12, 
          12);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD114_ADDITIONAL_AMOUNT5
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD114_ADDITIONAL_AMOUNT5
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            recordType + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "AdditionalAmount5" + 
            "|" + 
            "This field length Cannot be greater than 12");
          errorCodes.add(errorObj);
        }
        else if (CommonValidator.isNullOrEmpty(transactionAdviceDetailBean.getAdditionalAmountType5()))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD111_ADDITIONAL_AMOUNTTYPE5
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD111_ADDITIONAL_AMOUNTTYPE5
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            recordType + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "AdditionalAmount5" + 
            "|" + 
            SubmissionErrorCodes.ERROR_DATAFIELD111_ADDITIONAL_AMOUNTTYPE5
            .getErrorDescription());
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD112_ADDITIONAL_AMOUNT5
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD112_ADDITIONAL_AMOUNT5
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "AdditionalAmount5" + 
          "|" + 
          "This field can only be Numeric");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateAdditionalAmountSign5(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[109]))
      {
        String reqLength = CommonValidator.validateLength(value, 1, 1);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD115_ADDITIONAL_AMOUNT_SIGN5
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD115_ADDITIONAL_AMOUNT_SIGN5
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            recordType + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "AdditionalAmountSign5" + 
            "|" + 
            "This field length Cannot be greater than 1");
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD115_ADDITIONAL_AMOUNT_SIGN5
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD115_ADDITIONAL_AMOUNT_SIGN5
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "AdditionalAmountSign5" + 
          "|" + 
          "This field can only be AlphaNumeric");
        errorCodes.add(errorObj);
      }
    }
  }
}
