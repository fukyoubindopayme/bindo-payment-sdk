package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.LocationDetailTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.RecordType;
import java.util.ArrayList;

public class TransactionAdviceBasicBean
{
  private LocationDetailTAABean locationDetailTAABean;
  private TransactionAdviceDetailBean transactionAdviceDetailBean;
  private TransactionAdviceAddendumBean transactionAdviceAddendumBean;
  private String recordType = RecordType.Transaction_Advice_Basic.getRecordType();
  private String recordNumber;
  private String transactionIdentifier;
  private String formatCode;
  private String mediaCode;
  private String submissionMethod;
  private String approvalCode;
  private String primaryAccountNumber;
  private String cardExpiryDate;
  private String transactionDate;
  private String transactionTime;
  private String transactionAmount;
  private String processingCode;
  private String transactionCurrencyCode;
  private String extendedPaymentData;
  private String merchantId;
  private String merchantLocationId;
  private String merchantContactInfo;
  private String terminalId;
  private String posDataCode;
  private String invoiceReferenceNumber;
  private String tabImageSequenceNumber;
  private String matchingKeyType;
  private String matchingKey;
  private String electronicCommerceIndicator;
  private String tabField7reserved;
  private String tabField13reserved;
  private String tabField23reserved;
  private String tabField24reserved;
  private String tabField25reserved;
  private String tabField27reserved;
  private String tabField32reserved;
  private ArrayList<TransactionAdviceAddendumBean> arrayTAABean;
  
  public ArrayList<TransactionAdviceAddendumBean> getArrayTAABean()
  {
    return this.arrayTAABean;
  }
  
  public void setArrayTAABean(ArrayList<TransactionAdviceAddendumBean> arrayTAABean)
  {
    this.arrayTAABean = arrayTAABean;
  }
  
  public LocationDetailTAABean getLocationDetailTAABean()
  {
    return this.locationDetailTAABean;
  }
  
  public void setLocationDetailTAABean(LocationDetailTAABean locationDetailTAABean)
  {
    this.locationDetailTAABean = locationDetailTAABean;
  }
  
  public TransactionAdviceDetailBean getTransactionAdviceDetailBean()
  {
    return this.transactionAdviceDetailBean;
  }
  
  public void setTransactionAdviceDetailBean(TransactionAdviceDetailBean transactionAdviceDetailBean)
  {
    this.transactionAdviceDetailBean = transactionAdviceDetailBean;
  }
  
  public TransactionAdviceAddendumBean getTransactionAdviceAddendumBean()
  {
    return this.transactionAdviceAddendumBean;
  }
  
  public void setTransactionAdviceAddendumBean(TransactionAdviceAddendumBean transactionAdviceAddendumBean)
  {
    this.transactionAdviceAddendumBean = transactionAdviceAddendumBean;
  }
  
  public String getRecordType()
  {
    return this.recordType;
  }
  
  public void setRecordType(String recordType)
  {
    this.recordType = recordType;
  }
  
  public String getRecordNumber()
  {
    return this.recordNumber;
  }
  
  public void setRecordNumber(String recordNumber)
  {
    this.recordNumber = recordNumber;
  }
  
  public String getTransactionIdentifier()
  {
    return this.transactionIdentifier;
  }
  
  public void setTransactionIdentifier(String transactionIdentifier)
  {
    this.transactionIdentifier = transactionIdentifier;
  }
  
  public String getFormatCode()
  {
    return this.formatCode;
  }
  
  public void setFormatCode(String formatCode)
  {
    this.formatCode = formatCode;
  }
  
  public String getMediaCode()
  {
    return this.mediaCode;
  }
  
  public void setMediaCode(String mediaCode)
  {
    this.mediaCode = mediaCode;
  }
  
  public String getSubmissionMethod()
  {
    return this.submissionMethod;
  }
  
  public void setSubmissionMethod(String submissionMethod)
  {
    this.submissionMethod = submissionMethod;
  }
  
  public String getApprovalCode()
  {
    return this.approvalCode;
  }
  
  public void setApprovalCode(String approvalCode)
  {
    this.approvalCode = approvalCode;
  }
  
  public String getPrimaryAccountNumber()
  {
    return this.primaryAccountNumber;
  }
  
  public void setPrimaryAccountNumber(String primaryAccountNumber)
  {
    this.primaryAccountNumber = primaryAccountNumber;
  }
  
  public String getCardExpiryDate()
  {
    return this.cardExpiryDate;
  }
  
  public void setCardExpiryDate(String cardExpiryDate)
  {
    this.cardExpiryDate = cardExpiryDate;
  }
  
  public String getTransactionDate()
  {
    return this.transactionDate;
  }
  
  public void setTransactionDate(String transactionDate)
  {
    this.transactionDate = transactionDate;
  }
  
  public String getTransactionTime()
  {
    return this.transactionTime;
  }
  
  public void setTransactionTime(String transactionTime)
  {
    this.transactionTime = transactionTime;
  }
  
  public String getTransactionAmount()
  {
    return this.transactionAmount;
  }
  
  public void setTransactionAmount(String transactionAmount)
  {
    this.transactionAmount = transactionAmount;
  }
  
  public String getProcessingCode()
  {
    return this.processingCode;
  }
  
  public void setProcessingCode(String processingCode)
  {
    this.processingCode = processingCode;
  }
  
  public String getTransactionCurrencyCode()
  {
    return this.transactionCurrencyCode;
  }
  
  public void setTransactionCurrencyCode(String transactionCurrencyCode)
  {
    this.transactionCurrencyCode = transactionCurrencyCode;
  }
  
  public String getExtendedPaymentData()
  {
    return this.extendedPaymentData;
  }
  
  public void setExtendedPaymentData(String extendedPaymentData)
  {
    this.extendedPaymentData = extendedPaymentData;
  }
  
  public String getMerchantId()
  {
    return this.merchantId;
  }
  
  public void setMerchantId(String merchantId)
  {
    this.merchantId = merchantId;
  }
  
  public String getMerchantLocationId()
  {
    return this.merchantLocationId;
  }
  
  public void setMerchantLocationId(String merchantLocationId)
  {
    this.merchantLocationId = merchantLocationId;
  }
  
  public String getMerchantContactInfo()
  {
    return this.merchantContactInfo;
  }
  
  public void setMerchantContactInfo(String merchantContactInfo)
  {
    this.merchantContactInfo = merchantContactInfo;
  }
  
  public String getTerminalId()
  {
    return this.terminalId;
  }
  
  public void setTerminalId(String terminalId)
  {
    this.terminalId = terminalId;
  }
  
  public String getPosDataCode()
  {
    return this.posDataCode;
  }
  
  public void setPosDataCode(String posDataCode)
  {
    this.posDataCode = posDataCode;
  }
  
  public String getInvoiceReferenceNumber()
  {
    return this.invoiceReferenceNumber;
  }
  
  public void setInvoiceReferenceNumber(String invoiceReferenceNumber)
  {
    this.invoiceReferenceNumber = invoiceReferenceNumber;
  }
  
  public String getTabImageSequenceNumber()
  {
    return this.tabImageSequenceNumber;
  }
  
  public void setTabImageSequenceNumber(String tabImageSequenceNumber)
  {
    this.tabImageSequenceNumber = tabImageSequenceNumber;
  }
  
  public String getMatchingKeyType()
  {
    return this.matchingKeyType;
  }
  
  public void setMatchingKeyType(String matchingKeyType)
  {
    this.matchingKeyType = matchingKeyType;
  }
  
  public String getMatchingKey()
  {
    return this.matchingKey;
  }
  
  public void setMatchingKey(String matchingKey)
  {
    this.matchingKey = matchingKey;
  }
  
  public String getElectronicCommerceIndicator()
  {
    return this.electronicCommerceIndicator;
  }
  
  public void setElectronicCommerceIndicator(String electronicCommerceIndicator)
  {
    this.electronicCommerceIndicator = electronicCommerceIndicator;
  }
  
  public String getTabField7reserved()
  {
    return this.tabField7reserved;
  }
  
  public void setTabField7reserved(String tabField7reserved)
  {
    this.tabField7reserved = tabField7reserved;
  }
  
  public String getTabField13reserved()
  {
    return this.tabField13reserved;
  }
  
  public void setTabField13reserved(String tabField13reserved)
  {
    this.tabField13reserved = tabField13reserved;
  }
  
  public String getTabField23reserved()
  {
    return this.tabField23reserved;
  }
  
  public void setTabField23reserved(String tabField23reserved)
  {
    this.tabField23reserved = tabField23reserved;
  }
  
  public String getTabField24reserved()
  {
    return this.tabField24reserved;
  }
  
  public void setTabField24reserved(String tabField24reserved)
  {
    this.tabField24reserved = tabField24reserved;
  }
  
  public String getTabField25reserved()
  {
    return this.tabField25reserved;
  }
  
  public void setTabField25reserved(String tabField25reserved)
  {
    this.tabField25reserved = tabField25reserved;
  }
  
  public String getTabField27reserved()
  {
    return this.tabField27reserved;
  }
  
  public void setTabField27reserved(String tabField27reserved)
  {
    this.tabField27reserved = tabField27reserved;
  }
  
  public String getTabField32reserved()
  {
    return this.tabField32reserved;
  }
  
  public void setTabField32reserved(String tabField32reserved)
  {
    this.tabField32reserved = tabField32reserved;
  }
}
