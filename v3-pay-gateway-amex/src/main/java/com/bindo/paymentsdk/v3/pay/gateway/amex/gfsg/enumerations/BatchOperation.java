package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations;

public enum BatchOperation
{
  Open("01"),  Close("02"),  Purge("03"),  Status("04");
  
  private final String batchOperation;
  
  private BatchOperation(String batchOperation)
  {
    this.batchOperation = batchOperation;
  }
  
  public String getBatchOperation()
  {
    return this.batchOperation;
  }
}
