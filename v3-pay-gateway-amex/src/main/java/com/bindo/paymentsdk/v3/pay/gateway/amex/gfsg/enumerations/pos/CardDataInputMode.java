package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.pos;

public enum CardDataInputMode {
    POC_UNKONOWN("0"),
    POC_MANUAL("1"),
    POC_MAGNETIC_STRIPE_READ("2"),
    POC_BAR_CODE("3"),
    POC_OCR("4"),
    POC_INTEGRATED_CKT_CARD("5"),
    POC_KEY_ENTERED("6"),
    RESERVE_FOR_ISO_USE_7("7"),
    RESERVE_FOR_NATIONAL_USE_8("8"),
    POC_TECHNICAL_FALLBACK("9"),
    RESERVE_FOR_ISO_USE_A("A"),
    RESERVE_FOR_ISO_USE_B("B"),
    RESERVE_FOR_ISO_USE_C("C"),
    RESERVE_FOR_ISO_USE_D("D"),
    RESERVE_FOR_ISO_USE_E("E"),
    RESERVE_FOR_ISO_USE_F("F"),
    RESERVE_FOR_ISO_USE_G("G"),
    RESERVE_FOR_ISO_USE_H("H"),
    RESERVE_FOR_ISO_USE_I("I"),
    RESERVE_FOR_NATIONAL_USE_J("J"),
    RESERVE_FOR_NATIONAL_USE_K("K"),
    RESERVE_FOR_NATIONAL_USE_L("L"),
    RESERVE_FOR_NATIONAL_USE_M("M"),
    RESERVE_FOR_NATIONAL_USE_N("N"),
    RESERVE_FOR_NATIONAL_USE_O("O"),
    RESERVE_FOR_NATIONAL_USE_P("P"),
    RESERVE_FOR_NATIONAL_USE_Q("Q"),
    RESERVE_FOR_NATIONAL_USE_R("R"),
    POC_MANUALLY_ENTERED("S"),
    RESERVE_FOR_PRIVATE_USE_T("T"),
    RESERVE_FOR_PRIVATE_USE_U("U"),
    RESERVE_FOR_PRIVATE_USE_V("V"),
    POC_SWIPED("W"),
    RESERVE_FOR_PRIVATE_USE_Z("Z");

    private final String cardDataInputMode;

    private CardDataInputMode(String cardMode) {
        this.cardDataInputMode = cardMode;
    }

    public String getCardDataInputMode() {
        return this.cardDataInputMode;
    }
}
