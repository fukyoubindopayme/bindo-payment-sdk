package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean;

import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.bean.RequestISOMessage;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.Constants;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.ProcessingCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.utils.ISOUtil;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.utils.Utility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AuthorizationRequestBean
  extends RequestISOMessage
{
  private String messageTypeIdentifier = "";
  private StringBuffer primaryAccountNumber = new StringBuffer();
  private String processingCode = "";
  private String amountTransaction = "";
  private String transmissionDateAndTime = "";
  private String systemTraceAuditNumber = "";
  private String localTransactionDateAndTime = "";
  private String cardEffectiveDate = "";
  private String cardExpirationDate = "";
  private String cardSettlementDate = "";
  private String acquiringInstitutionIdCode = "";
  private String posDataCode = "";
  private String functionCode = "";
  private String messageReasonCode = "";
  private String cardAcceptorBusinessCode = "";
  private String approvalCodeLength = "";
  private String transactionId = "";
  private String merchantLocationCountryCode = "";
  private String forwardingInstitutionIdCode = "";
  private String track2Data = "";
  private String retrievalReferenceNumber = "";
  private String approvalCode = "";
  private String amountOriginal = "";
  private String actionCode = "";
  private String cardAcceptorTerminalId = "";
  private String cardAcceptorIdCode = "";
  private String additionalResponseData = "";
  private String cardAcceptorNameLocation = "";
  private String track1Data = "";
  private String additionalDataNational = "";
  private String additionalDataPrivate = "";
  private String transactionCurrencyCode = "";
  private String pinData = "";
  private String cardIdentifierCode = "";
  private String amountsAdditional = "";
  private String iccRelatedData = "";
  private String originalDataElements = "";
  private String nationalUseData1 = "";
  private String nationalUseData2 = "";
  private String validationInformation = "";
  private String verificationInformation = "";
  private String messageAuthenticationCode = "";
  private AdditionalDataNationalBean additionalDataNationalBean;
  private CardAcceptorNameLocationBean cardAcceptorNameLocationBean;
  private NationalUseData2Bean nationalUseData2Bean;
  private IntegratedCircuitCardRelatedDataBean iccRelatedDataBean;
  private POSDataCodeBean posDataCodeBean;
  private PrivateUseData2Bean privateUseData2Bean;
  private OriginalDataElementsBean originalDataElementsBean;
  private String keyManagementData;
  private KeyManagementDataBean keyManagementDataBean;
  private NationalUseDataBean nationalUseData1Bean;
  private SecurityRelatedControlInfoBean securityRelatedControlInfoBean;
  private String securityRelatedControlData;
  
  public static String getLocalDateTime(String format)
  {
    String value = now(format);
    return value;
  }
  
  public static String now(String dateFormat)
  {
    Calendar cal = Calendar.getInstance();
    SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
    return sdf.format(cal.getTime());
  }
  
  public AdditionalDataNationalBean getAdditionalDataNationalBean()
  {
    return this.additionalDataNationalBean;
  }
  
  public void setAdditionalDataNationalBean(AdditionalDataNationalBean additionalDataNationalBean)
  {
    this.additionalDataNationalBean = additionalDataNationalBean;
  }
  
  public CardAcceptorNameLocationBean getCardAcceptorNameLocationBean()
  {
    return this.cardAcceptorNameLocationBean;
  }
  
  public void setCardAcceptorNameLocationBean(CardAcceptorNameLocationBean cardAcceptorNameLocationBean)
  {
    this.cardAcceptorNameLocationBean = cardAcceptorNameLocationBean;
  }
  
  public NationalUseData2Bean getNationalUseData2Bean()
  {
    return this.nationalUseData2Bean;
  }
  
  public void setNationalUseData2Bean(NationalUseData2Bean nationalUseData2Bean)
  {
    this.nationalUseData2Bean = nationalUseData2Bean;
  }
  
  public IntegratedCircuitCardRelatedDataBean getIccRelatedDataBean()
  {
    return this.iccRelatedDataBean;
  }
  
  public void setIccRelatedDataBean(IntegratedCircuitCardRelatedDataBean iccRelatedDataBean)
  {
    this.iccRelatedDataBean = iccRelatedDataBean;
  }
  
  public POSDataCodeBean getPosDataCodeBean()
  {
    return this.posDataCodeBean;
  }
  
  public void setPosDataCodeBean(POSDataCodeBean posDataCodeBean)
  {
    this.posDataCodeBean = posDataCodeBean;
  }
  
  public PrivateUseData2Bean getPrivateUseData2Bean()
  {
    return this.privateUseData2Bean;
  }
  
  public void setPrivateUseData2Bean(PrivateUseData2Bean privateUseData2Bean)
  {
    this.privateUseData2Bean = privateUseData2Bean;
  }
  
  public OriginalDataElementsBean getOriginalDataElementsBean()
  {
    return this.originalDataElementsBean;
  }
  
  public void setOriginalDataElementsBean(OriginalDataElementsBean originalDataElementsBean)
  {
    this.originalDataElementsBean = originalDataElementsBean;
  }
  
  public String getAcquiringInstitutionIdCode()
  {
    return this.acquiringInstitutionIdCode;
  }
  
  public String getMerchantLocationCountryCode()
  {
    return this.merchantLocationCountryCode;
  }
  
  public String getTransactionId()
  {
    return this.transactionId;
  }
  
  public String getAdditionalDataNational()
  {
    return this.additionalDataNational;
  }
  
  public String getAdditionalDataPrivate()
  {
    return this.additionalDataPrivate;
  }
  
  public String getApprovalCodeLength()
  {
    return this.approvalCodeLength;
  }
  
  public String getCardAcceptorBusinessCode()
  {
    return this.cardAcceptorBusinessCode;
  }
  
  public String getCardAcceptorIdCode()
  {
    return this.cardAcceptorIdCode;
  }
  
  public String getCardAcceptorNameLocation()
  {
    return this.cardAcceptorNameLocation;
  }
  
  public String getCardAcceptorTerminalId()
  {
    return this.cardAcceptorTerminalId;
  }
  
  public String getCardEffectiveDate()
  {
    return this.cardEffectiveDate;
  }
  
  public String getCardExpirationDate()
  {
    return this.cardExpirationDate;
  }
  
  public String getForwardingInstitutionIdCode()
  {
    return this.forwardingInstitutionIdCode;
  }
  
  public String getFunctionCode()
  {
    return this.functionCode;
  }
  
  public String getIccRelatedData()
  {
    return this.iccRelatedData;
  }
  
  public String getLocalTransactionDateAndTime()
  {
    return this.localTransactionDateAndTime;
  }
  
  public String getMessageAuthenticationCode()
  {
    return this.messageAuthenticationCode;
  }
  
  public String getMessageReasonCode()
  {
    return this.messageReasonCode;
  }
  
  public String getNationalUseData1()
  {
    return this.nationalUseData1;
  }
  
  public String getNationalUseData2()
  {
    return this.nationalUseData2;
  }
  
  public String getPinData()
  {
    return this.pinData;
  }
  
  public String getPosDataCode()
  {
    return this.posDataCode;
  }
  
  public StringBuffer getPrimaryAccountNumber()
  {
    return this.primaryAccountNumber;
  }
  
  public String getValidationInformation()
  {
    return this.validationInformation;
  }
  
  public String getVerificationInformation()
  {
    return this.verificationInformation;
  }
  
  public String getProcessingCode()
  {
    return this.processingCode;
  }
  
  public String getRetrievalReferenceNumber()
  {
    return this.retrievalReferenceNumber;
  }
  
  public String getCardIdentifierCode()
  {
    return this.cardIdentifierCode;
  }
  
  public String getCardSettlementDate()
  {
    return this.cardSettlementDate;
  }
  
  public String getSystemTraceAuditNumber()
  {
    return this.systemTraceAuditNumber;
  }
  
  public String getTrack1Data()
  {
    return this.track1Data;
  }
  
  public String getTrack2Data()
  {
    return this.track2Data;
  }
  
  public String getTransmissionDateAndTime()
  {
    return this.transmissionDateAndTime;
  }
  
  public String getAmountTransaction()
  {
    return this.amountTransaction;
  }
  
  public String getTransactionCurrencyCode()
  {
    return this.transactionCurrencyCode;
  }
  
  public String getMessageTypeIdentifier()
  {
    return this.messageTypeIdentifier;
  }
  
  public void setAcquiringInstitutionIdCode(String acquiringInstitutionIdCode)
  {
    this.acquiringInstitutionIdCode = acquiringInstitutionIdCode;
  }
  
  public void setMerchantLocationCountryCode(String merchantLocationCountryCode)
  {
    this.merchantLocationCountryCode = merchantLocationCountryCode;
  }
  
  public void setTransactionId(String transactionId)
  {
    this.transactionId = transactionId;
  }
  
  public void setAdditionalDataNational(String additionalDataNational)
  {
    this.additionalDataNational = additionalDataNational;
  }
  
  public void setAdditionalDataPrivate(String additionalDataPrivate)
  {
    this.additionalDataPrivate = additionalDataPrivate;
  }
  
  public void setApprovalCodeLength(String approvalCodeLength)
  {
    this.approvalCodeLength = approvalCodeLength;
  }
  
  public void setCardAcceptorBusinessCode(String cardAcceptorBusinessCode)
  {
    this.cardAcceptorBusinessCode = cardAcceptorBusinessCode;
  }
  
  public void setCardAcceptorIdCode(String cardAcceptorIdCode)
  {
    this.cardAcceptorIdCode = cardAcceptorIdCode;
  }
  
  public void setCardAcceptorNameLocation(String cardAcceptorNameLocation)
  {
    this.cardAcceptorNameLocation = cardAcceptorNameLocation;
  }
  
  public void setCardAcceptorTerminalId(String cardAcceptorTerminalId)
  {
    this.cardAcceptorTerminalId = cardAcceptorTerminalId;
  }
  
  public void setCardEffectiveDate(String cardEffectiveDate)
  {
    this.cardEffectiveDate = cardEffectiveDate;
  }
  
  public void setCardExpirationDate(String cardExpirationDate)
  {
    this.cardExpirationDate = cardExpirationDate;
  }
  
  public void setForwardingInstitutionIdCode(String forwardingInstitutionIdCode)
  {
    this.forwardingInstitutionIdCode = forwardingInstitutionIdCode;
  }
  
  public void setFunctionCode(String functionCode)
  {
    this.functionCode = functionCode;
  }
  
  public void setIccRelatedData(String iccRelatedData)
  {
    this.iccRelatedData = iccRelatedData;
  }
  
  public void setLocalTransactionDateAndTime(String localTransactionDateAndTime)
  {
    this.localTransactionDateAndTime = localTransactionDateAndTime;
  }
  
  public void setMessageAuthenticationCode(String messageAuthenticationCode)
  {
    this.messageAuthenticationCode = messageAuthenticationCode;
  }
  
  public void setMessageReasonCode(String messageReasonCode)
  {
    this.messageReasonCode = messageReasonCode;
  }
  
  public void setNationalUseData1(String nationalUseData1)
  {
    this.nationalUseData1 = nationalUseData1;
  }
  
  public void setNationalUseData2(String nationalUseData2)
  {
    this.nationalUseData2 = nationalUseData2;
  }
  
  public void setPinData(String pinData)
  {
    this.pinData = pinData;
  }
  
  public void setPosDataCode(String posDataCode)
  {
    this.posDataCode = posDataCode;
  }
  
  public void setPrimaryAccountNumber(StringBuffer primaryAccountNumber)
  {
    this.primaryAccountNumber = primaryAccountNumber;
  }
  
  public void setValidationInformation(String validationInformation)
  {
    this.validationInformation = validationInformation;
  }
  
  public void setVerificationInformation(String verificationInformation)
  {
    this.verificationInformation = verificationInformation;
  }
  
  public void setProcessingCode(String processingCode)
  {
    this.processingCode = processingCode;
  }
  
  public void setRetrievalReferenceNumber(String retrievalReferenceNumber)
  {
    this.retrievalReferenceNumber = retrievalReferenceNumber;
  }
  
  public void setCardIdentifierCode(String cardIdentifierCode)
  {
    this.cardIdentifierCode = cardIdentifierCode;
  }
  
  public void setCardSettlementDate(String cardSettlementDate)
  {
    this.cardSettlementDate = cardSettlementDate;
  }
  
  public void setSystemTraceAuditNumber(String systemTraceAuditNumber)
  {
    this.systemTraceAuditNumber = systemTraceAuditNumber;
  }
  
  public void setTrack1Data(String track1Data)
  {
    this.track1Data = track1Data;
  }
  
  public void setTrack2Data(String track2Data)
  {
    this.track2Data = track2Data;
  }
  
  public void setTransmissionDateAndTime(String transmissionDateAndTime)
  {
    this.transmissionDateAndTime = transmissionDateAndTime;
  }
  
  public void setAmountTransaction(String amountTransaction)
  {
    if (getProcessingCode().equalsIgnoreCase(ProcessingCode.TRANSACTION_FOR_AUTOMATED_ADDRESS_VERIFICATION_ONLY.getProcessingCode())) {
      this.amountTransaction = "000000000000";
    } else {
      this.amountTransaction = amountTransaction;
    }
  }
  
  public void setTransactionCurrencyCode(String transactionCurrencyCode)
  {
    this.transactionCurrencyCode = transactionCurrencyCode;
  }
  
  public void setMessageTypeIdentifier(String messageTypeIdentifier)
  {
    this.messageTypeIdentifier = messageTypeIdentifier;
  }
  
  public String getApprovalCode()
  {
    return this.approvalCode;
  }
  
  public String getAmountOriginal()
  {
    return this.amountOriginal;
  }
  
  public String getActionCode()
  {
    return this.actionCode;
  }
  
  public void setApprovalCode(String transactionAprovalCode)
  {
    this.approvalCode = transactionAprovalCode;
  }
  
  public void setAmountOriginal(String amountOriginal)
  {
    this.amountOriginal = amountOriginal;
  }
  
  public void setActionCode(String actionCode)
  {
    this.actionCode = actionCode;
  }
  
  public String getAdditionalResponseData()
  {
    return this.additionalResponseData;
  }
  
  public void setAdditionalResponseData(String additionalResponseData)
  {
    this.additionalResponseData = additionalResponseData;
  }
  
  public String getAmountsAdditional()
  {
    return this.amountsAdditional;
  }
  
  public void setAmountsAdditional(String additionalAmountResponse)
  {
    this.amountsAdditional = additionalAmountResponse;
  }
  
  public String getOriginalDataElements()
  {
    return this.originalDataElements;
  }
  
  public void setOriginalDataElements(String originalDataElements)
  {
    this.originalDataElements = originalDataElements;
  }
  
  public String getKeyManagementData()
  {
    return this.keyManagementData;
  }
  
  public void setKeyManagementData(String keyManagementData)
  {
    this.keyManagementData = keyManagementData;
  }
  
  public KeyManagementDataBean getKeyManagementDataBean()
  {
    return this.keyManagementDataBean;
  }
  
  public void setKeyManagementDataBean(KeyManagementDataBean keyManagementDataBean)
  {
    this.keyManagementDataBean = keyManagementDataBean;
  }
  
  public NationalUseDataBean getNationalUseData1Bean()
  {
    return this.nationalUseData1Bean;
  }
  
  public void setSecurityRelatedControlInfoBean(SecurityRelatedControlInfoBean securityRelatedControlInfoBean)
  {
    this.securityRelatedControlInfoBean = securityRelatedControlInfoBean;
  }
  
  public SecurityRelatedControlInfoBean getSecurityRelatedControlInfoBean()
  {
    return this.securityRelatedControlInfoBean;
  }
  
  public void setSecurityRelatedControlData(String securityRelatedControlData)
  {
    this.securityRelatedControlData = securityRelatedControlData;
  }
  
  public String getSecurityRelatedControlData()
  {
    return this.securityRelatedControlData;
  }
  
  public void setNationalUseData1Bean(NationalUseDataBean nationalUseData1Bean)
  {
    this.nationalUseData1Bean = nationalUseData1Bean;
  }
  
  public String toString()
  {
    Utility objUtility = new Utility();
    StringBuffer buffer = new StringBuffer();
    
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[1]).append(" :: ");
    String maskedValue = objUtility.performMasking(getPrimaryAccountNumber().toString(), false, true, 5, 4);
    buffer.append(maskedValue).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[2]).append(" :: ").append(getProcessingCode()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[3]).append(" :: ").append(getAmountTransaction()).append(Constants.NEW_LINE_CHAR);
    if (!ISOUtil.IsNullOrEmpty(getTransmissionDateAndTime())) {
      buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[6]).append(" :: ").append(getTransmissionDateAndTime()).append(Constants.NEW_LINE_CHAR);
    }
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[10]).append(" :: ").append(getSystemTraceAuditNumber().toUpperCase()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[11]).append(" :: ").append(getLocalTransactionDateAndTime()).append(Constants.NEW_LINE_CHAR);
    if (!ISOUtil.IsNullOrEmpty(getCardEffectiveDate())) {
      buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[12]).append(" :: ").append(getCardEffectiveDate()).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getCardExpirationDate())) {
      buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[13]).append(" :: ").append(getCardExpirationDate()).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getCardSettlementDate())) {
      buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[14]).append(" :: ").append(getCardSettlementDate()).append(Constants.NEW_LINE_CHAR);
    }
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[18]).append(" :: ").append(getMerchantLocationCountryCode()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[21]).append(" :: ").append(getPosDataCode()).append(Constants.NEW_LINE_CHAR);
    if (!ISOUtil.IsNullOrEmpty(getFunctionCode())) {
      buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[23]).append(" :: ").append(getFunctionCode()).append(Constants.NEW_LINE_CHAR);
    }
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[24]).append(" :: ").append(getMessageReasonCode()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[25]).append(" :: ").append(getCardAcceptorBusinessCode()).append(Constants.NEW_LINE_CHAR);
    if (!ISOUtil.IsNullOrEmpty(getApprovalCodeLength())) {
      buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[26]).append(" :: ").append(getApprovalCodeLength()).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getAmountOriginal())) {
      buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[29]).append(" :: ").append(getAmountOriginal()).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getTransactionId())) {
      buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[30]).append(" :: ").append(getTransactionId()).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getAcquiringInstitutionIdCode())) {
      buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[31]).append(" :: ").append(getAcquiringInstitutionIdCode()).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getForwardingInstitutionIdCode())) {
      buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[32]).append(" :: ").append(getForwardingInstitutionIdCode()).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getTrack2Data()))
    {
      buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[34]).append(" :: ");
      maskedValue = objUtility.performMasking(getTrack2Data(), true, false, 0, 0);
      buffer.append(maskedValue).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getRetrievalReferenceNumber())) {
      buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[36]).append(" :: ").append(getRetrievalReferenceNumber()).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getApprovalCode())) {
      buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[37]).append(" :: ").append(getApprovalCode()).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getCardAcceptorTerminalId())) {
      buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[40]).append(" :: ").append(getCardAcceptorTerminalId()).append(Constants.NEW_LINE_CHAR);
    }
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[41]).append(" :: ").append(getCardAcceptorIdCode()).append(Constants.NEW_LINE_CHAR);
    if (!ISOUtil.IsNullOrEmpty(getCardAcceptorNameLocation())) {
      buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[42]).append(" :: ").append(getCardAcceptorNameLocation()).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getAdditionalResponseData())) {
      buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[43]).append(" :: ").append(getAdditionalResponseData()).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getTrack1Data()))
    {
      buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[44]).append(" :: ");
      maskedValue = objUtility.performMasking(getTrack1Data(), true, false, 0, 0);
      buffer.append(maskedValue).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getAdditionalDataNational())) {
      buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[46]).append(" :: ").append(getAdditionalDataNational()).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getAdditionalDataPrivate())) {
      buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[47]).append(" :: ").append(getAdditionalDataPrivate()).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getTransactionCurrencyCode())) {
      buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[48]).append(" :: ").append(getTransactionCurrencyCode()).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getPinData())) {
      buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[51]).append(" :: ").append(getPinData()).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getCardIdentifierCode()))
    {
      buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[52]).append(" :: ");
      maskedValue = objUtility.performMasking(getCardIdentifierCode(), true, false, 0, 0);
      buffer.append(maskedValue).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getAmountsAdditional()))
    {
      buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[53]).append(" :: ");
      maskedValue = objUtility.performMasking(getAmountsAdditional(), true, false, 0, 0);
      buffer.append(maskedValue).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getIccRelatedData())) {
      buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[54]).append(" :: ").append(getIccRelatedData()).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getOriginalDataElements())) {
      buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[55]).append(" :: ").append(getOriginalDataElements()).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getNationalUseData1())) {
      buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[59]).append(" :: ").append(getNationalUseData1()).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getNationalUseData2())) {
      buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[60]).append(" :: ").append(getNationalUseData2()).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getValidationInformation())) {
      buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[61]).append(" :: ").append(getValidationInformation()).append(Constants.NEW_LINE_CHAR);
    }
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[62]).append(" :: ").append(getVerificationInformation()).append(Constants.NEW_LINE_CHAR);
    if (!ISOUtil.IsNullOrEmpty(getMessageAuthenticationCode())) {
      buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[63]).append(" :: ").append(getMessageAuthenticationCode()).append(Constants.NEW_LINE_CHAR);
    }
    return buffer.toString();
  }
}
