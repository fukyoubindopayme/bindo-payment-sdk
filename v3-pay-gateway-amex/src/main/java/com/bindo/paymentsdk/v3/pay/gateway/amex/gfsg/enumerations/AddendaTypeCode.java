package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations;

public enum AddendaTypeCode
{
  Deferred_Payment_Plan_Addendum_Message("02"),  Industry_Specific_Detail_Addendum_Message("03"),  Corporate_Purchasing_Solutions("05"),  EMV_Addendum_Message("07"),  Location_Detail_Addendum_Message("99");
  
  private final String addendaTypeCode;
  
  private AddendaTypeCode(String AddendaType)
  {
    this.addendaTypeCode = AddendaType;
  }
  
  public String getAddendaTypeCode()
  {
    return this.addendaTypeCode;
  }
}
