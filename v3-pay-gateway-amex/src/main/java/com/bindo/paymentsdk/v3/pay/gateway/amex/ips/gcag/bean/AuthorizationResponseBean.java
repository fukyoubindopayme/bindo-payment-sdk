package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean;

import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.bean.ResponseISOMessage;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.Constants;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.utils.Utility;
import java.util.List;

public class AuthorizationResponseBean
  extends ResponseISOMessage
{
  private String messageTypeIdentifier = "";
  private StringBuffer primaryAccountNumber = new StringBuffer();
  private String primaryAccountNumberExtended = "";
  private String processingCode = "";
  private String amountTransaction = "";
  private String transmissionDateAndTime = "";
  private String systemTraceAuditNumber = "";
  private String localTransactionDateAndTime = "";
  private String cardEffectiveDate = "";
  private String cardExpirationDate = "";
  private String cardSettlementDate = "";
  private String acquiringInstitutionIdCode = "";
  private String posDataCode = "";
  private String functionCode = "";
  private String messageReasonCode = "";
  private String cardAcceptorBusinessCode = "";
  private String approvalCodeLength = "";
  private String transactionId = "";
  private String merchantLocationCountryCode = "";
  private String forwardingInstitutionIdCode = "";
  private String track2Data = "";
  private String retrievalReferenceNumber = "";
  private String approvalCode = "";
  private String amountOriginal = "";
  private String actionCode = "";
  private String cardAcceptorTerminalId = "";
  private String cardAcceptorIdCode = "";
  private String additionalResponseData = "";
  private String cardAcceptorNameLocation = "";
  private String track1Data = "";
  private String additionalDataNational = "";
  private String additionalDataPrivate = "";
  private String transactionCurrencyCode = "";
  private String pinData = "";
  private String cardIdentifierCode = "";
  private String iccRelatedData = "";
  private String amountsAdditional = "";
  private String originalDataElements = "";
  private String nationalUseData1 = "";
  private String nationalUseData2 = "";
  private String validationInformation = "";
  private String verificationInformation = "";
  private String messageAuthenticationCode = "";
  private String isoFormattedRequest = "";
  private String isoFormattedResponse = "";
  private String actionCodeDescription = "";
  private String additionalResponseDataDesc = "";
  private List<ErrorObject> authErrorList;
  private KeyManagementDataBean keyManagementDataBean;
  private String keyManagementData;
  private IntegratedCircuitCardRelatedDataBean iccRelatedDataBean;
  
  public String getMessageTypeIdentifier()
  {
    return this.messageTypeIdentifier;
  }
  
  public StringBuffer getPrimaryAccountNumber()
  {
    return this.primaryAccountNumber;
  }
  
  public String getPrimaryAccountNumberExtended()
  {
    return this.primaryAccountNumberExtended;
  }
  
  public String getProcessingCode()
  {
    return this.processingCode;
  }
  
  public String getAmountTransaction()
  {
    return this.amountTransaction;
  }
  
  public String getTransmissionDateAndTime()
  {
    return this.transmissionDateAndTime;
  }
  
  public String getSystemTraceAuditNumber()
  {
    return this.systemTraceAuditNumber;
  }
  
  public String getLocalTransactionDateAndTime()
  {
    return this.localTransactionDateAndTime;
  }
  
  public String getCardEffectiveDate()
  {
    return this.cardEffectiveDate;
  }
  
  public String getCardExpirationDate()
  {
    return this.cardExpirationDate;
  }
  
  public String getCardSettlementDate()
  {
    return this.cardSettlementDate;
  }
  
  public String getAcquiringInstitutionIdCode()
  {
    return this.acquiringInstitutionIdCode;
  }
  
  public String getPosDataCode()
  {
    return this.posDataCode;
  }
  
  public String getFunctionCode()
  {
    return this.functionCode;
  }
  
  public String getMessageReasonCode()
  {
    return this.messageReasonCode;
  }
  
  public String getCardAcceptorBusinessCode()
  {
    return this.cardAcceptorBusinessCode;
  }
  
  public String getApprovalCodeLength()
  {
    return this.approvalCodeLength;
  }
  
  public String getTransactionId()
  {
    return this.transactionId;
  }
  
  public String getMerchantLocationCountryCode()
  {
    return this.merchantLocationCountryCode;
  }
  
  public String getForwardingInstitutionIdCode()
  {
    return this.forwardingInstitutionIdCode;
  }
  
  public String getTrack2Data()
  {
    return this.track2Data;
  }
  
  public String getRetrievalReferenceNumber()
  {
    return this.retrievalReferenceNumber;
  }
  
  public String getCardAcceptorTerminalId()
  {
    return this.cardAcceptorTerminalId;
  }
  
  public String getCardAcceptorIdCode()
  {
    return this.cardAcceptorIdCode;
  }
  
  public String getCardAcceptorNameLocation()
  {
    return this.cardAcceptorNameLocation;
  }
  
  public String getTrack1Data()
  {
    return this.track1Data;
  }
  
  public String getAdditionalDataNational()
  {
    return this.additionalDataNational;
  }
  
  public String getAdditionalDataPrivate()
  {
    return this.additionalDataPrivate;
  }
  
  public String getTransactionCurrencyCode()
  {
    return this.transactionCurrencyCode;
  }
  
  public String getPinData()
  {
    return this.pinData;
  }
  
  public String getCardIdentifierCode()
  {
    return this.cardIdentifierCode;
  }
  
  public String getIccRelatedData()
  {
    return this.iccRelatedData;
  }
  
  public String getNationalUseData1()
  {
    return this.nationalUseData1;
  }
  
  public String getNationalUseData2()
  {
    return this.nationalUseData2;
  }
  
  public String getValidationInformation()
  {
    return this.validationInformation;
  }
  
  public String getVerificationInformation()
  {
    return this.verificationInformation;
  }
  
  public String getMessageAuthenticationCode()
  {
    return this.messageAuthenticationCode;
  }
  
  public List<ErrorObject> getAuthErrorList()
  {
    return this.authErrorList;
  }
  
  public String getAdditionalResponseDataDesc()
  {
    return this.additionalResponseDataDesc;
  }
  
  public void setMessageTypeIdentifier(String msgTypId)
  {
    this.messageTypeIdentifier = msgTypId;
  }
  
  public void setPrimaryAccountNumber(StringBuffer primaryAccountNumber)
  {
    this.primaryAccountNumber = primaryAccountNumber;
  }
  
  public void setPrimaryAccountNumberExtended(String primaryAccountNumberExtended)
  {
    this.primaryAccountNumberExtended = primaryAccountNumberExtended;
  }
  
  public void setProcessingCode(String processingCode)
  {
    this.processingCode = processingCode;
  }
  
  public void setAmountTransaction(String amountTransaction)
  {
    this.amountTransaction = amountTransaction;
  }
  
  public void setTransmissionDateAndTime(String transmisionDateTime)
  {
    this.transmissionDateAndTime = transmisionDateTime;
  }
  
  public void setSystemTraceAuditNumber(String systemTraceAuditNumber)
  {
    this.systemTraceAuditNumber = systemTraceAuditNumber;
  }
  
  public void setLocalTransactionDateAndTime(String transTs)
  {
    this.localTransactionDateAndTime = transTs;
  }
  
  public void setCardEffectiveDate(String cardEffectiveDate)
  {
    this.cardEffectiveDate = cardEffectiveDate;
  }
  
  public void setCardExpirationDate(String cardExpirationDate)
  {
    this.cardExpirationDate = cardExpirationDate;
  }
  
  public void setCardSettlementDate(String transSettleDt)
  {
    this.cardSettlementDate = transSettleDt;
  }
  
  public void setAcquiringInstitutionIdCode(String acqInstIdCd)
  {
    this.acquiringInstitutionIdCode = acqInstIdCd;
  }
  
  public void setPosDataCode(String pointOfServiceData)
  {
    this.posDataCode = pointOfServiceData;
  }
  
  public void setFunctionCode(String funcCd)
  {
    this.functionCode = funcCd;
  }
  
  public void setMessageReasonCode(String msgRsnCd)
  {
    this.messageReasonCode = msgRsnCd;
  }
  
  public void setCardAcceptorBusinessCode(String merCtgyCd)
  {
    this.cardAcceptorBusinessCode = merCtgyCd;
  }
  
  public void setApprovalCodeLength(String aprvCdLgth)
  {
    this.approvalCodeLength = aprvCdLgth;
  }
  
  public void setTransactionId(String transactionId)
  {
    this.transactionId = transactionId;
  }
  
  public void setMerchantLocationCountryCode(String acqInstCtryCd)
  {
    this.merchantLocationCountryCode = acqInstCtryCd;
  }
  
  public void setForwardingInstitutionIdCode(String forwardingInstituteIdentificationCode)
  {
    this.forwardingInstitutionIdCode = forwardingInstituteIdentificationCode;
  }
  
  public void setTrack2Data(String cardTrack2Data)
  {
    this.track2Data = cardTrack2Data;
  }
  
  public void setRetrievalReferenceNumber(String rtrvRefNbr)
  {
    this.retrievalReferenceNumber = rtrvRefNbr;
  }
  
  public void setCardAcceptorTerminalId(String merTrmnlId)
  {
    this.cardAcceptorTerminalId = merTrmnlId;
  }
  
  public void setCardAcceptorIdCode(String cardAcceptorIdCode)
  {
    this.cardAcceptorIdCode = cardAcceptorIdCode;
  }
  
  public void setCardAcceptorNameLocation(String cardAcceptorDetail)
  {
    this.cardAcceptorNameLocation = cardAcceptorDetail;
  }
  
  public void setTrack1Data(String cardTrack1Data)
  {
    this.track1Data = cardTrack1Data;
  }
  
  public void setAdditionalDataNational(String additionalDataNational)
  {
    this.additionalDataNational = additionalDataNational;
  }
  
  public void setAdditionalDataPrivate(String additionalDataPrivate)
  {
    this.additionalDataPrivate = additionalDataPrivate;
  }
  
  public void setTransactionCurrencyCode(String transCurrCd)
  {
    this.transactionCurrencyCode = transCurrCd;
  }
  
  public void setPinData(String dataTxt)
  {
    this.pinData = dataTxt;
  }
  
  public void setCardIdentifierCode(String cardIdentifierCode)
  {
    this.cardIdentifierCode = cardIdentifierCode;
  }
  
  public void setIccRelatedData(String dataTxt)
  {
    this.iccRelatedData = dataTxt;
  }
  
  public void setNationalUseData1(String nationalUseData1)
  {
    this.nationalUseData1 = nationalUseData1;
  }
  
  public void setNationalUseData2(String nationalUseData2)
  {
    this.nationalUseData2 = nationalUseData2;
  }
  
  public void setValidationInformation(String validationInformation)
  {
    this.validationInformation = validationInformation;
  }
  
  public void setVerificationInformation(String verificationInformation)
  {
    this.verificationInformation = verificationInformation;
  }
  
  public void setMessageAuthenticationCode(String msgAuthenticationCd)
  {
    this.messageAuthenticationCode = msgAuthenticationCd;
  }
  
  public String getApprovalCode()
  {
    return this.approvalCode;
  }
  
  public String getAmountOriginal()
  {
    return this.amountOriginal;
  }
  
  public String getActionCode()
  {
    return this.actionCode;
  }
  
  public void setApprovalCode(String transAprvCd)
  {
    this.approvalCode = transAprvCd;
  }
  
  public void setAmountOriginal(String amountOriginal)
  {
    this.amountOriginal = amountOriginal;
  }
  
  public void setActionCode(String actionCode)
  {
    this.actionCode = actionCode;
  }
  
  public String getAdditionalResponseData()
  {
    return this.additionalResponseData;
  }
  
  public void setAdditionalResponseData(String additionalResponseData)
  {
    this.additionalResponseData = additionalResponseData;
  }
  
  public String getAmountsAdditional()
  {
    return this.amountsAdditional;
  }
  
  public void setAmountsAdditional(String additionalAmountResponse)
  {
    this.amountsAdditional = additionalAmountResponse;
  }
  
  public String getOriginalDataElements()
  {
    return this.originalDataElements;
  }
  
  public void setOriginalDataElements(String originalDataElements)
  {
    this.originalDataElements = originalDataElements;
  }
  
  public String getISOFormattedRequest()
  {
    return this.isoFormattedRequest;
  }
  
  public void setISOFormattedRequest(String isoFormattedRequest)
  {
    this.isoFormattedRequest = isoFormattedRequest;
  }
  
  public String getISOFormattedResponse()
  {
    return this.isoFormattedResponse;
  }
  
  public void setISOFormattedResponse(String isoFormattedResponse)
  {
    this.isoFormattedResponse = isoFormattedResponse;
  }
  
  public String getActionCodeDescription()
  {
    return this.actionCodeDescription;
  }
  
  public void setActionCodeDescription(String actionCodeDescription)
  {
    this.actionCodeDescription = actionCodeDescription;
  }
  
  public void setAuthErrorList(List<ErrorObject> authErrorList)
  {
    this.authErrorList = authErrorList;
  }
  
  public void setAdditionalResponseDataDesc(String additionalResponseDataDesc)
  {
    this.additionalResponseDataDesc = additionalResponseDataDesc;
  }
  
  public KeyManagementDataBean getKeyManagementDataBean()
  {
    return this.keyManagementDataBean;
  }
  
  public void setKeyManagementDataBean(KeyManagementDataBean keyManagementDataBean)
  {
    this.keyManagementDataBean = keyManagementDataBean;
  }
  
  public String getKeyManagementData()
  {
    return this.keyManagementData;
  }
  
  public void setKeyManagementData(String pkeyManagementData)
  {
    this.keyManagementData = pkeyManagementData;
  }
  
  public IntegratedCircuitCardRelatedDataBean getIccRelatedDataBean()
  {
    return this.iccRelatedDataBean;
  }
  
  public void setIccRelatedDataBean(IntegratedCircuitCardRelatedDataBean pICCRelatedDataBean)
  {
    this.iccRelatedDataBean = pICCRelatedDataBean;
  }
  
  public String toString()
  {
    String maskedValue = "";
    Utility objUtility = new Utility();
    StringBuffer buffer = new StringBuffer();
    
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[1]).append(" :: ");
    if (getPrimaryAccountNumber() != null) {
      maskedValue = objUtility.performMasking(getPrimaryAccountNumber().toString(), false, true, 5, 4);
    }
    buffer.append(maskedValue).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[2]).append(" :: ").append(getProcessingCode()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[3]).append(" :: ").append(getAmountTransaction()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[6]).append(" :: ").append(getTransmissionDateAndTime()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[10]).append(" :: ").append(getSystemTraceAuditNumber().toUpperCase()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[11]).append(" :: ").append(getLocalTransactionDateAndTime()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[12]).append(" :: ").append(getCardEffectiveDate()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[13]).append(" :: ").append(getCardExpirationDate()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[14]).append(" :: ").append(getCardSettlementDate()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[18]).append(" :: ").append(getMerchantLocationCountryCode()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[21]).append(" :: ").append(getPosDataCode()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[23]).append(" :: ").append(getFunctionCode()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[24]).append(" :: ").append(getMessageReasonCode()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[25]).append(" :: ").append(getCardAcceptorBusinessCode()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[26]).append(" :: ").append(getApprovalCodeLength()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[29]).append(" :: ").append(getAmountOriginal()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[30]).append(" :: ").append(getTransactionId()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[31]).append(" :: ").append(getAcquiringInstitutionIdCode()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[32]).append(" :: ").append(getForwardingInstitutionIdCode()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[33]).append(" :: ");
    
    maskedValue = objUtility.performMasking(getPrimaryAccountNumberExtended(), false, true, 6, 4);
    
    buffer.append(maskedValue).append(Constants.NEW_LINE_CHAR);
    
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[34]).append(" :: ");
    maskedValue = objUtility.performMasking(getTrack2Data(), true, false, 0, 0);
    
    buffer.append(maskedValue).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[36]).append(" :: ").append(getRetrievalReferenceNumber()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[37]).append(" :: ").append(getApprovalCode()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[38]).append(" :: ").append(getActionCode()).append(" :: ").append(getActionCodeDescription()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[40]).append(" :: ").append(getCardAcceptorTerminalId()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[41]).append(" :: ").append(getCardAcceptorIdCode()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[42]).append(" :: ").append(getCardAcceptorNameLocation()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[43]).append(" :: ").append(getAdditionalResponseData()).append(" :: ").append(getAdditionalResponseDataDesc()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[44]).append(" :: ");
    maskedValue = objUtility.performMasking(getTrack1Data(), true, false, 0, 0);
    
    buffer.append(maskedValue).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[46]).append(" :: ").append(getAdditionalDataNational()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[47]).append(" :: ").append(getAdditionalDataPrivate()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[48]).append(" :: ").append(getTransactionCurrencyCode()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[51]).append(" :: ").append(getPinData()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[53]).append(" :: ");
    maskedValue = objUtility.performMasking(getAmountsAdditional(), true, false, 0, 0);
    
    buffer.append(maskedValue).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[54]).append(" :: ").append(getIccRelatedData()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[55]).append(" :: ").append(getOriginalDataElements()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[59]).append(" :: ").append(getNationalUseData1()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[60]).append(" :: ").append(getNationalUseData2()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[61]).append(" :: ").append(getValidationInformation()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[62]).append(" :: ").append(getVerificationInformation()).append(Constants.NEW_LINE_CHAR);
    buffer.append(com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.FIELD_NAME[63]).append(" :: ").append(getMessageAuthenticationCode()).append(Constants.NEW_LINE_CHAR);
    
    String str = new String();
    if (buffer != null) {
      str = buffer.toString();
    }
    return str;
  }
}
