package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.exception;

public class IPSAPIException
  extends Exception
{
  public IPSAPIException() {}
  
  public IPSAPIException(String message, Throwable cause)
  {
    super(message, cause);
  }
  
  public IPSAPIException(String message)
  {
    super(message);
  }
  
  public IPSAPIException(Throwable cause)
  {
    super(cause);
  }
}
