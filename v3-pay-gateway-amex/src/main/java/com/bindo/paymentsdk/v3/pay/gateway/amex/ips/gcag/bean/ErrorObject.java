package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean;

import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.AuthErrorCodes;

public class ErrorObject
{
  private String errorCode;
  private String errorDescription;
  
  public ErrorObject(String errorCode, String errorDescription)
  {
    this.errorCode = errorCode;
    this.errorDescription = errorDescription;
  }
  
  public ErrorObject(AuthErrorCodes code)
  {
    this(code.getErrorCode(), code.getErrorDescription());
  }
  
  public String getErrorCode()
  {
    return this.errorCode;
  }
  
  public void setErrorCode(String errorCode)
  {
    this.errorCode = errorCode;
  }
  
  public String getErrorDescription()
  {
    return this.errorDescription;
  }
  
  public void setErrorDescription(String errorDescription)
  {
    this.errorDescription = errorDescription;
  }
}
