package com.bindo.paymentsdk.v3.pay.gateway.amex.util.juice;

import java.security.NoSuchAlgorithmException;
import java.util.Hashtable;

public final class JuicePT extends Hashtable {
    private static final long serialVersionUID = 5478453210731334978L;

    public JuicePT(JuicePK pkeys) throws NoSuchAlgorithmException {
        this.init(pkeys);
    }

    public JuicePT(String clsName) throws NoSuchAlgorithmException {
        try {
            this.init((JuicePK)Class.forName(clsName).newInstance());
        } catch (Exception var3) {
            throw new NoSuchAlgorithmException(var3.getClass() + ": " + var3.getMessage());
        }
    }

    private void init(JuicePK partialKeys) throws NoSuchAlgorithmException {
        int[][] pkArray = JuicePK.PKIX;
        this.put("AES/ECB/NoPadding", new JuiceP(1, partialKeys, pkArray[0], 16, true));
        this.put("DESede/ECB/NoPadding", new JuiceP(1, partialKeys, pkArray[1], 8, true));
        this.put("DESede/CBC/PKCS5Padding", new JuiceP(2, partialKeys, pkArray[2], 8, true));
        this.put("SHA-1", new JuiceP(3, partialKeys, (int[])null, 1, true));
        this.put("SHA-256", new JuiceP(3, partialKeys, (int[])null, 1, true));
        this.put("AES", new JuiceP(1, partialKeys, pkArray[0], 16, true));
        this.put("DES-EDE3", new JuiceP(1, partialKeys, pkArray[1], 8, true));
    }
}
