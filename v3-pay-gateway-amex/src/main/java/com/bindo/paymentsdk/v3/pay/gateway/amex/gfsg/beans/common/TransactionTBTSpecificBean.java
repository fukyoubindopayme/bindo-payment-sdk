package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common;

import java.util.List;

public class TransactionTBTSpecificBean
{
  private TransactionBatchTrailerBean transactionBatchTrailerBean;
  private List<TransactionAdviceBasicBean> transactionAdviceBasicType;
  
  public TransactionBatchTrailerBean getTransactionBatchTrailerBean()
  {
    return this.transactionBatchTrailerBean;
  }
  
  public List<TransactionAdviceBasicBean> getTransactionAdviceBasicType()
  {
    return this.transactionAdviceBasicType;
  }
  
  public void setTransactionBatchTrailerBean(TransactionBatchTrailerBean transactionBatchTrailerBean)
  {
    this.transactionBatchTrailerBean = transactionBatchTrailerBean;
  }
  
  public void setTransactionAdviceBasicType(List<TransactionAdviceBasicBean> transactionAdviceBasicType)
  {
    this.transactionAdviceBasicType = transactionAdviceBasicType;
  }
}
