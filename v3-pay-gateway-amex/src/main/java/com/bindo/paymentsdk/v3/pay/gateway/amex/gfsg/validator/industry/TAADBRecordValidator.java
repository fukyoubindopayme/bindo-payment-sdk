package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.industry;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.ErrorObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceBasicBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.RecordType;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.SubmissionErrorCodes;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.exceptions.SettlementException;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.CommonValidator;
import java.util.List;

public class TAADBRecordValidator
{
  public static void validateRecordType(String recordType, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(recordType))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE);
      errorCodes.add(errorObj);
    }
    else if (!RecordType.Transaction_Advice_Addendum.getRecordType().equalsIgnoreCase(recordType))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE);
      errorCodes.add(errorObj);
    }
  }
  
  public static void validateRecordNumber(String recordNumber, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(recordNumber))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD3_RECORDNUMBER);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(recordNumber, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
    {
      String reqLength = CommonValidator.validateLength(recordNumber, 8, 8);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD3_RECORDNUMBER);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD2_RECORDNUMBER);
      errorCodes.add(errorObj);
    }
  }
  
  public static void validateTransactionIdentifier(String transactionIdentifier, TransactionAdviceBasicBean transactionAdviceBasicType, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(transactionIdentifier))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(transactionIdentifier, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
    {
      String reqLength = 
        CommonValidator.validateLength(transactionIdentifier, 15, 15);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER);
        errorCodes.add(errorObj);
      }
      else if (!transactionIdentifier.equalsIgnoreCase(transactionAdviceBasicType.getTransactionIdentifier()))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD67_TRANSACTION_IDENTIFIER);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER);
      errorCodes.add(errorObj);
    }
  }
  
  public static void validateFormatCode(String formatCode, TransactionAdviceBasicBean transactionAdviceBasicType, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(formatCode))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD28_FORMAT_CODE);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(formatCode, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
    {
      String reqLength = CommonValidator.validateLength(formatCode, 2, 2);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD28_FORMAT_CODE);
        errorCodes.add(errorObj);
      }
      else if (!formatCode.equalsIgnoreCase(transactionAdviceBasicType.getFormatCode()))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD138_FORMAT_CODE);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD28_FORMAT_CODE);
      errorCodes.add(errorObj);
    }
  }
  
  public static void validateAddendaTypeCode(String addendaTypeCode, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(addendaTypeCode))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD68_ADDENDA_TYPECODE);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(addendaTypeCode, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
    {
      String reqLength = CommonValidator.validateLength(addendaTypeCode, 2, 2);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD68_ADDENDA_TYPECODE);
        errorCodes.add(errorObj);
      }
      else if (!CommonValidator.isValidAddendaTypeCode(addendaTypeCode))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD70_ADDENDA_TYPECODE
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD70_ADDENDA_TYPECODE
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          "|" + 
          "RecordNumber:" + 
          "|" + 
          "AddendaTypeCode" + 
          "|" + 
          SubmissionErrorCodes.ERROR_DATAFIELD70_ADDENDA_TYPECODE
          .getErrorDescription());
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD68_ADDENDA_TYPECODE);
      errorCodes.add(errorObj);
    }
  }
}
