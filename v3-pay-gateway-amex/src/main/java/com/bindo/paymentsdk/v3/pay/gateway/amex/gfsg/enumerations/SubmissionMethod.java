package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations;

public enum SubmissionMethod {
    Amex_POS_Terminal("01"),
    Non_Amex_POS_Terminal("02"),
    Non_Amex_Integrated_POS_Terminal("03"),
    EIPP("04"),
    Paper_external_vendor_paper_processor("05"),
    Purchase_express("06"),
    PayFlow("07"),
    Dial_Payment_system("10"),
    Automated_Bill_Payment_Platform("13");

    private final String submissionMethod;

    private SubmissionMethod(String submissionMethod) {
        this.submissionMethod = submissionMethod;
    }

    public String getSubmissionMethod() {
        return this.submissionMethod;
    }
}
