package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.utils;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.EncryptedData;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.connection.PropertyReader;
import com.bindo.paymentsdk.v3.pay.gateway.amex.util.juice.Juice;

import org.apache.log4j.Logger;

public class DataConversion
{
  EncryptedData objEncryptedData = new EncryptedData();
  private static final String KEY = "51867F0C0E55766E34E1FE7E053CBFB4";
  private static final Logger LOGGER = Logger.getLogger(DataConversion.class);
  private static final char[] ASCIItranslateEBCDIC = {
  
    '\000', '\001', 
    '\002', 
    '\003', 
    '\004', 
    '\005', 
    '\006', 
    '\007', 
    '\b', 
    '\t', 
    '\n', 
    '\013', 
    '\f', 
    '\r', 
    '\016', 
    '\017', 
    '\020', 
    '\021', 
    '\022', 
    '\023', 
    '\024', 
    '\025', 
    '\026', 
    '\027', 
    '\030', 
    '\031', 
    '\032', 
    '\033', 
    '\034', 
    '\035', 
    '\036', 
    '\037', 
    '@', 
    'Z', 
    '', 
    '{', 
    '[', 
    'l', 
    'P', 
    '}', 
    'M', 
    ']', 
    '\\', 
    'N', 
    'k', 
    '`', 
    'K', 
    'a', 
    'ð', 
    'ñ', 
    'ò', 
    'ó', 
    'ô', 
    'õ', 
    'ö', 
    '÷', 
    'ø', 
    'ù', 
    'z', 
    '^', 
    'L', 
    '~', 
    'n', 
    'o', 
    '|', 
    'Á', 
    'Â', 
    'Ã', 
    'Ä', 
    'Å', 
    'Æ', 
    'Ç', 
    'È', 
    'É', 
    'Ñ', 
    'Ò', 
    'Ó', 
    'Ô', 
    'Õ', 
    'Ö', 
    '×', 
    'Ø', 
    'Ù', 
    'â', 
    'ã', 
    'ä', 
    'å', 
    'æ', 
    'ç', 
    'è', 
    'é', 
    '­', 
    'à', 
    '½', 
    '_', 
    'm', 
    '}', 
    '', 
    '', 
    '', 
    '', 
    '', 
    '', 
    '', 
    '', 
    '', 
    '', 
    '', 
    '', 
    '', 
    '', 
    '', 
    '', 
    '', 
    '', 
    '¢', 
    '£', 
    '¤', 
    '¥', 
    '¦', 
    '§', 
    '¨', 
    '©', 
    'À', 
    'j', 
    'Ð', 
    '¡', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K', 
    'K' };
  
  public String performMasking(String value, boolean isAll, boolean isNotAll, int startIndex, int endIndex)
  {
    String temp = "";
    String temp2 = "";
    String temp3 = "";
    if (isAll)
    {
      value = getMasked(value.length(), "X");
    }
    else if (isNotAll)
    {
      if (startIndex < value.length())
      {
        temp = value.substring(0, startIndex);
        temp2 = value.substring(startIndex, value.length() - endIndex);
        
        temp2 = getMasked(temp2.length(), "X");
        
        temp3 = value.substring(temp.length() + temp2.length(), temp.length() + temp2.length() + endIndex);
      }
      value = temp + temp2 + temp3;
    }
    return value;
  }
  
  public String getMasked(int length, String symbol)
  {
    String temp = "";
    for (int i = 0; i < length; i++) {
      temp = symbol + temp;
    }
    return temp;
  }
  
  public static String convertNumericsToBCD(String getValue)
  {
    String[] BCD = { "0000", "0001", "0010", "0011", "0100", "0101", "0110", "0111", "1000", "1001" };
    
    StringBuffer buffer = new StringBuffer();
    for (int iCount = 0; iCount < getValue.length(); iCount++)
    {
      char temp = getValue.charAt(iCount);
      
      int temp1 = Integer.parseInt(String.valueOf(temp));
      
      buffer.append(BCD[temp1]);
    }
    return buffer.toString();
  }
  
  public static String convertASCIIToEBCDICString(String input)
  {
    StringBuffer output = new StringBuffer("");
    for (int iCount = 0; iCount < input.length(); iCount++)
    {
      char temp = input.charAt(iCount);
      int count = ASCIItranslateEBCDIC[temp];
      output.append(Integer.toHexString(count).toUpperCase());
    }
    return output.toString();
  }
  
  public EncryptedData encryptSFTCredentials(String userID, String passWord)
  {
    try
    {
      Juice juice = new Juice("AES/ECB/NoPadding", "51867F0C0E55766E34E1FE7E053CBFB4");
      String resultUserID = juice.encrypt(userID);
      String resultPwd = juice.encrypt(passWord);
      
      this.objEncryptedData.setEncryptedUserID(resultUserID);
      this.objEncryptedData.setEncryptedPwd(resultPwd);
    }
    catch (Exception e)
    {
      if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
        LOGGER.error(e.getMessage());
      }
    }
    return this.objEncryptedData;
  }
}
