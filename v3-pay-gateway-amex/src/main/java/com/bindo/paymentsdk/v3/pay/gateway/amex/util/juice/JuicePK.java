package com.bindo.paymentsdk.v3.pay.gateway.amex.util.juice;

public abstract class JuicePK {
    protected static final int[][] PKIX = new int[][]{{0, 7, 12, 23}, {18, 8, 2, 11, 40, 54}, {18, 8, 2, 11, 40, 54}};

    protected JuicePK() {
    }

    public abstract String getName();

    protected abstract byte[] getPartialKey(int var1);
}
