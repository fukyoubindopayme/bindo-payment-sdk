package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.confirmation;

import java.util.ArrayList;
import java.util.List;

public class ConfirmationErrorReportBean
{
  private String recordType;
  private String errorCode;
  private List<String> recordNumber = new ArrayList();
  private String cerReserved;
  
  public List<String> getRecordNumber()
  {
    return this.recordNumber;
  }
  
  public void addRecordNumber(String recNumber)
  {
    this.recordNumber.add(recNumber);
  }
  
  public String getRecordType()
  {
    return this.recordType;
  }
  
  public void setRecordType(String recordType)
  {
    this.recordType = recordType;
  }
  
  public String getErrorCode()
  {
    return this.errorCode;
  }
  
  public void setErrorCode(String errorCode)
  {
    this.errorCode = errorCode;
  }
  
  public String getCerReserved()
  {
    return this.cerReserved;
  }
  
  public void setCerReserved(String cerReserved)
  {
    this.cerReserved = cerReserved;
  }
}
