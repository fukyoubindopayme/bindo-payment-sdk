package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.confirmation;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceDetailBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionBatchTrailerBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.LocationDetailTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.AirlineIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.AutoRentalIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.CommunicationServicesIndustryBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.EntertainmentTicketingIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.InsuranceIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.LodgingIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.RailIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.RetailIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.TravelCruiseIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.nonindustrytype.CPSLevel2Bean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.nonindustrytype.DeferredPaymentPlanBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.nonindustrytype.EMVBean;

public class RejectedRecordDataBean
{
  private LocationDetailTAABean locationDetailTAABean = new LocationDetailTAABean();
  private TransactionAdviceDetailBean tadRecordBean = new TransactionAdviceDetailBean();
  private TransactionBatchTrailerBean tbtRecord = new TransactionBatchTrailerBean();
  private RejectedTABRecord tabRecord = new RejectedTABRecord();
  private AirlineIndustryTAABean airlineIndustry = new AirlineIndustryTAABean();
  private AutoRentalIndustryTAABean autoRentalIndustry = new AutoRentalIndustryTAABean();
  private CommunicationServicesIndustryBean communicationServicesIndustry = new CommunicationServicesIndustryBean();
  private EntertainmentTicketingIndustryTAABean entertainmentTicketingIndustry = new EntertainmentTicketingIndustryTAABean();
  private InsuranceIndustryTAABean insuranceIndustry = new InsuranceIndustryTAABean();
  private LodgingIndustryTAABean lodgingIndustry = new LodgingIndustryTAABean();
  private RailIndustryTAABean railIndustry = new RailIndustryTAABean();
  private DeferredPaymentPlanBean dppNonIndustry = new DeferredPaymentPlanBean();
  private RetailIndustryTAABean retailIndustry = new RetailIndustryTAABean();
  private TravelCruiseIndustryTAABean travelCruiseIndustry = new TravelCruiseIndustryTAABean();
  private CPSLevel2Bean cpsLevel2Bean = new CPSLevel2Bean();
  private EMVBean emvBean = new EMVBean();
  
  public DeferredPaymentPlanBean getDppNonIndustry()
  {
    return this.dppNonIndustry;
  }
  
  public void setDppNonIndustry(DeferredPaymentPlanBean dppNonIndustry)
  {
    this.dppNonIndustry = dppNonIndustry;
  }
  
  public LocationDetailTAABean getLocationDetailTAABean()
  {
    return this.locationDetailTAABean;
  }
  
  public void setLocationDetailTAABean(LocationDetailTAABean locationDetailTAABean)
  {
    this.locationDetailTAABean = locationDetailTAABean;
  }
  
  public TransactionAdviceDetailBean getTadRecordBean()
  {
    return this.tadRecordBean;
  }
  
  public void setTadRecordBean(TransactionAdviceDetailBean tadRecordBean)
  {
    this.tadRecordBean = tadRecordBean;
  }
  
  public TransactionBatchTrailerBean getTbtRecord()
  {
    return this.tbtRecord;
  }
  
  public void setTbtRecord(TransactionBatchTrailerBean tbtRecord)
  {
    this.tbtRecord = tbtRecord;
  }
  
  public RejectedTABRecord getTabRecord()
  {
    return this.tabRecord;
  }
  
  public void setTabRecord(RejectedTABRecord tabRecord)
  {
    this.tabRecord = tabRecord;
  }
  
  public AirlineIndustryTAABean getAirlineIndustry()
  {
    return this.airlineIndustry;
  }
  
  public void setAirlineIndustry(AirlineIndustryTAABean airlineIndustry)
  {
    this.airlineIndustry = airlineIndustry;
  }
  
  public AutoRentalIndustryTAABean getAutoRentalIndustry()
  {
    return this.autoRentalIndustry;
  }
  
  public void setAutoRentalIndustry(AutoRentalIndustryTAABean autoRentalIndustry)
  {
    this.autoRentalIndustry = autoRentalIndustry;
  }
  
  public CommunicationServicesIndustryBean getCommunicationServicesIndustry()
  {
    return this.communicationServicesIndustry;
  }
  
  public void setCommunicationServicesIndustry(CommunicationServicesIndustryBean communicationServicesIndustry)
  {
    this.communicationServicesIndustry = communicationServicesIndustry;
  }
  
  public EntertainmentTicketingIndustryTAABean getEntertainmentTicketingIndustry()
  {
    return this.entertainmentTicketingIndustry;
  }
  
  public void setEntertainmentTicketingIndustry(EntertainmentTicketingIndustryTAABean entertainmentTicketingIndustry)
  {
    this.entertainmentTicketingIndustry = entertainmentTicketingIndustry;
  }
  
  public InsuranceIndustryTAABean getInsuranceIndustry()
  {
    return this.insuranceIndustry;
  }
  
  public void setInsuranceIndustry(InsuranceIndustryTAABean insuranceIndustry)
  {
    this.insuranceIndustry = insuranceIndustry;
  }
  
  public LodgingIndustryTAABean getLodgingIndustry()
  {
    return this.lodgingIndustry;
  }
  
  public void setLodgingIndustry(LodgingIndustryTAABean lodgingIndustry)
  {
    this.lodgingIndustry = lodgingIndustry;
  }
  
  public RailIndustryTAABean getRailIndustry()
  {
    return this.railIndustry;
  }
  
  public void setRailIndustry(RailIndustryTAABean railIndustry)
  {
    this.railIndustry = railIndustry;
  }
  
  public RetailIndustryTAABean getRetailIndustry()
  {
    return this.retailIndustry;
  }
  
  public void setRetailIndustry(RetailIndustryTAABean retailIndustry)
  {
    this.retailIndustry = retailIndustry;
  }
  
  public TravelCruiseIndustryTAABean getTravelCruiseIndustry()
  {
    return this.travelCruiseIndustry;
  }
  
  public void setTravelCruiseIndustry(TravelCruiseIndustryTAABean travelCruiseIndustry)
  {
    this.travelCruiseIndustry = travelCruiseIndustry;
  }
  
  public CPSLevel2Bean getCpsLevel2Bean()
  {
    return this.cpsLevel2Bean;
  }
  
  public void setCpsLevel2Bean(CPSLevel2Bean cpsLevel2Bean)
  {
    this.cpsLevel2Bean = cpsLevel2Bean;
  }
  
  public EMVBean getEmvBean()
  {
    return this.emvBean;
  }
  
  public void setEmvBean(EMVBean emvBean)
  {
    this.emvBean = emvBean;
  }
}
