package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.confirmation;

import java.util.ArrayList;
import java.util.List;

public class ConfirmationReportObject
{
  private ConfirmationReportBodyBean confirmationReportBodyBean;
  private List<ConfirmationErrorReportBean> errorsRequiringRejectionRecord = new ArrayList();
  private List<ConfirmationErrorReportBean> errorsNotRequiringRejectionRecord = new ArrayList();
  private List<ConfirmationErrorReportBean> errorsRequiringBatchSuspensionRecord = new ArrayList();
  private List<ConfirmationErrorReportBean> preSuspendedRejectedBatchRecord = new ArrayList();
  private List<ConfirmationErrorReportBean> preSuspendedProcessedRejectedBatchRecord = new ArrayList();
  private List<ConfirmationRejectedRecordBean> rejectionRecords = new ArrayList();
  private List<ConfirmationReportSummaryBean> errorSummary = new ArrayList();
  private ConfirmationReportHeaderBean confirmationReportHeaderBean;
  
  public ConfirmationReportBodyBean getConfirmationReportBodyBean()
  {
    return this.confirmationReportBodyBean;
  }
  
  public void setConfirmationReportBodyBean(ConfirmationReportBodyBean confirmationReportBodyBean)
  {
    this.confirmationReportBodyBean = confirmationReportBodyBean;
  }
  
  public List<ConfirmationErrorReportBean> getErrorsRequiringRejectionRecord()
  {
    return this.errorsRequiringRejectionRecord;
  }
  
  public void addErrorsRequiringRejectionRecord(ConfirmationErrorReportBean errorsRequiringRejectionRecord)
  {
    this.errorsRequiringRejectionRecord.add(errorsRequiringRejectionRecord);
  }
  
  public List<ConfirmationErrorReportBean> getErrorsNotRequiringRejectionRecord()
  {
    return this.errorsNotRequiringRejectionRecord;
  }
  
  public void addErrorsNotRequiringRejectionRecord(ConfirmationErrorReportBean errorsNotRequiringRejectionRecord)
  {
    this.errorsNotRequiringRejectionRecord.add(errorsNotRequiringRejectionRecord);
  }
  
  public List<ConfirmationErrorReportBean> getErrorsRequiringBatchSuspensionRecord()
  {
    return this.errorsRequiringBatchSuspensionRecord;
  }
  
  public void addErrorsRequiringBatchSuspensionRecord(ConfirmationErrorReportBean errorsRequiringBatchSuspensionRecord)
  {
    this.errorsRequiringBatchSuspensionRecord.add(errorsRequiringBatchSuspensionRecord);
  }
  
  public List<ConfirmationErrorReportBean> getPreSuspendedRejectedBatchRecord()
  {
    return this.preSuspendedRejectedBatchRecord;
  }
  
  public void addPreSuspendedRejectedBatchRecord(ConfirmationErrorReportBean preSuspendedRejectBatchRec)
  {
    this.preSuspendedRejectedBatchRecord.add(preSuspendedRejectBatchRec);
  }
  
  public List<ConfirmationErrorReportBean> getPreSuspendedProcessedRejectedBatchRecord()
  {
    return this.preSuspendedProcessedRejectedBatchRecord;
  }
  
  public void addPreSuspendedProcessedRejectedBatchRecord(ConfirmationErrorReportBean preSuspendedProcessedRejectBatchRec)
  {
    this.preSuspendedProcessedRejectedBatchRecord.add(preSuspendedProcessedRejectBatchRec);
  }
  
  public List<ConfirmationRejectedRecordBean> getRejectionRecords()
  {
    return this.rejectionRecords;
  }
  
  public void addRejectionRecords(ConfirmationRejectedRecordBean rejectionRec)
  {
    this.rejectionRecords.add(rejectionRec);
  }
  
  public List<ConfirmationReportSummaryBean> getErrorSummary()
  {
    return this.errorSummary;
  }
  
  public void addErrorSummary(ConfirmationReportSummaryBean errorSummary)
  {
    this.errorSummary.add(errorSummary);
  }
  
  public ConfirmationReportHeaderBean getConfirmationReportHeaderBean()
  {
    return this.confirmationReportHeaderBean;
  }
  
  public void setConfirmationReportHeaderBean(ConfirmationReportHeaderBean confirmationReportHeaderBean)
  {
    this.confirmationReportHeaderBean = confirmationReportHeaderBean;
  }
}
