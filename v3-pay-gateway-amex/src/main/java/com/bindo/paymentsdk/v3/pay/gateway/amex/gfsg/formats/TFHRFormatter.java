package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.formats;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionFileHeaderBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.connection.PropertyReader;
import java.io.IOException;
import java.util.List;
import org.apache.log4j.Logger;

public final class TFHRFormatter
  extends SettlementMessageFormatter
{
  private static final Logger LOGGER = Logger.getLogger(TFHRFormatter.class);
  
  public static void formatTFHRecord(TransactionFileHeaderBean transactionFileHeaderBean, List<String> formatedMessageList)
    throws IOException
  {
    StringBuffer formattedMessage = new StringBuffer();
    
    formatValue(transactionFileHeaderBean.getRecordType(), formattedMessage, 3, false, true, true, false, true);
    formatValue(transactionFileHeaderBean.getRecordNumber(), formattedMessage, 8, true, false, false, true, false);
    
    formatValue(transactionFileHeaderBean.getSubmitterId(), formattedMessage, 11, false, true, true, false, true);
    
    characterSpacesFill(formattedMessage, 21);
    
    formatValue(transactionFileHeaderBean.getSubmitterFileReferenceNumber(), formattedMessage, 9, false, true, true, false, true);
    
    formatValue(transactionFileHeaderBean.getSubmitterFileSequenceNumber(), formattedMessage, 9, true, false, false, true, false);
    
    formatValue(transactionFileHeaderBean.getFileCreationDate(), formattedMessage, 8, true, false, false, true, false);
    formatValue(transactionFileHeaderBean.getFileCreationTime(), formattedMessage, 6, true, false, false, true, false);
    formatValue(transactionFileHeaderBean.getFileVersionNumber(), formattedMessage, 8, false, true, false, false, true);
    if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
      characterSpacesFill(formattedMessage, 617);
    }
    formatedMessageList.add(formattedMessage.toString());
    if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
      LOGGER.info("Formated TFH Record Type :" + formattedMessage.toString());
    }
  }
}
