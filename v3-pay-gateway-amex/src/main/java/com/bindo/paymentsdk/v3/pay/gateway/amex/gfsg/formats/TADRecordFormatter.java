package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.formats;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceDetailBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.connection.PropertyReader;
import java.io.IOException;
import java.util.List;
import org.apache.log4j.Logger;

public final class TADRecordFormatter
{
  private static final Logger LOGGER = Logger.getLogger(TADRecordFormatter.class);
  
  public static void formatTADRecord(TransactionAdviceDetailBean transactionAdviceDetailBean, List<String> formatedMessageList)
    throws IOException
  {
    StringBuffer formattedMessage = new StringBuffer();
    
    SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getRecordType(), 
      formattedMessage, 3, false, true, true, false, true);
    SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getRecordNumber(), 
      formattedMessage, 8, true, false, false, true, false);
    
    SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getTransactionIdentifier(), 
      formattedMessage, 15, true, false, false, true, false);
    
    SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getAdditionalAmountType1(), 
      formattedMessage, 3, true, false, false, false, true);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getAdditionalAmount1(), 
      formattedMessage, 12, true, false, false, true, false);
    
    SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getAdditionalAmountSign1(), 
      formattedMessage, 1, false, true, false, false, true);
    
    SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getAdditionalAmountType2(), 
      formattedMessage, 3, true, false, false, false, true);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getAdditionalAmount2(), 
      formattedMessage, 12, true, false, false, true, false);
    
    SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getAdditionalAmountSign2(), 
      formattedMessage, 1, false, true, false, false, true);
    
    SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getAdditionalAmountType3(), 
      formattedMessage, 3, true, false, false, false, true);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getAdditionalAmount3(), 
      formattedMessage, 12, true, false, false, true, false);
    
    SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getAdditionalAmountSign3(), 
      formattedMessage, 1, false, true, false, false, true);
    SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getAdditionalAmountType4(), 
      formattedMessage, 3, true, false, false, false, true);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getAdditionalAmount4(), 
      formattedMessage, 12, true, false, false, true, false);
    
    SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getAdditionalAmountSign4(), 
      formattedMessage, 1, false, true, false, false, true);
    SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getAdditionalAmountType5(), 
      formattedMessage, 3, true, false, false, false, true);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getAdditionalAmount5(), 
      formattedMessage, 12, true, false, false, true, false);
    
    SettlementMessageFormatter.formatValue(transactionAdviceDetailBean.getAdditionalAmountSign5(), 
      formattedMessage, 1, false, true, false, false, true);
    if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
      SettlementMessageFormatter.characterSpacesFill(formattedMessage, 579);
    }
    formatedMessageList.add(formattedMessage.toString());
    if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
      LOGGER.info("Formated TAD Record Type :" + formattedMessage.toString());
    }
  }
}
