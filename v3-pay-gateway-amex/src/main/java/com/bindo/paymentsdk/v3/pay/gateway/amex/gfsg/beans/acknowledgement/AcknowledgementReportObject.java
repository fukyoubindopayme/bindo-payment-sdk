package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.acknowledgement;

public class AcknowledgementReportObject
{
  private String subFileDate = null;
  private String refNumber = null;
  private String seqNumber = null;
  private String fileTrackNumber = null;
  
  public String getSubFileDate()
  {
    return this.subFileDate;
  }
  
  public void setSubFileDate(String subFileDate)
  {
    this.subFileDate = subFileDate;
  }
  
  public String getRefNumber()
  {
    return this.refNumber;
  }
  
  public void setRefNumber(String refNumber)
  {
    this.refNumber = refNumber;
  }
  
  public String getSeqNumber()
  {
    return this.seqNumber;
  }
  
  public void setSeqNumber(String seqNumber)
  {
    this.seqNumber = seqNumber;
  }
  
  public String getFileTrackNumber()
  {
    return this.fileTrackNumber;
  }
  
  public void setFileTrackNumber(String fileTrackNumber)
  {
    this.fileTrackNumber = fileTrackNumber;
  }
}
