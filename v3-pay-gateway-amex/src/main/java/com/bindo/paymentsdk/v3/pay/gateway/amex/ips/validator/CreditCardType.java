package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.validator;

public abstract interface CreditCardType
{
  public abstract boolean matches(String paramString);
}
