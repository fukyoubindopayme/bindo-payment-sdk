package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.industry;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.ErrorObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceAddendumBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceBasicBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.RailIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.SubmissionErrorCodes;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.exceptions.SettlementException;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.CommonValidator;
import java.util.List;

public class RailIndRecordValidator
{
  public static void validateRailIndRecord(TransactionAdviceBasicBean transactionAdviceBasicType, TransactionAdviceAddendumBean transactionAddendumType, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    RailIndustryTAABean railIndustryTAABean = (RailIndustryTAABean)transactionAddendumType;
    
    TAADBRecordValidator.validateRecordType(railIndustryTAABean
      .getRecordType(), errorCodes);
    TAADBRecordValidator.validateRecordNumber(railIndustryTAABean
      .getRecordNumber(), errorCodes);
    TAADBRecordValidator.validateTransactionIdentifier(railIndustryTAABean
      .getTransactionIdentifier(), transactionAdviceBasicType, 
      errorCodes);
    TAADBRecordValidator.validateAddendaTypeCode(railIndustryTAABean
      .getAddendaTypeCode(), errorCodes);
    TAADBRecordValidator.validateFormatCode(railIndustryTAABean
      .getFormatCode(), transactionAdviceBasicType, errorCodes);
    validateTransactionType(railIndustryTAABean.getTransactionType(), 
      errorCodes);
    validateTicketNumber(railIndustryTAABean.getTicketNumber(), errorCodes);
    validatePassengerName(railIndustryTAABean.getPassengerName(), 
      errorCodes);
    validateIataCarrierCode(railIndustryTAABean.getIataCarrierCode(), 
      errorCodes);
    validateTicketIssuerName(railIndustryTAABean.getTicketIssuerName(), 
      errorCodes);
    validateTicketIssueCity(railIndustryTAABean.getTicketIssueCity(), 
      errorCodes);
    validateDepartureStation1(railIndustryTAABean.getDepartureStation1(), 
      errorCodes);
    validateDepartureDate1(railIndustryTAABean.getDepartureDate1(), 
      errorCodes);
    validateArrivalStation1(railIndustryTAABean.getArrivalStation1(), 
      errorCodes);
    validateDepartureStation2(railIndustryTAABean.getDepartureStation2(), 
      errorCodes);
    validateDepartureDate2(railIndustryTAABean.getDepartureDate2(), 
      errorCodes);
    validateArrivalStation2(railIndustryTAABean.getArrivalStation2(), 
      errorCodes);
    validateDepartureStation3(railIndustryTAABean.getDepartureStation3(), 
      errorCodes);
    validateDepartureDate3(railIndustryTAABean.getDepartureDate3(), 
      errorCodes);
    validateArrivalStation3(railIndustryTAABean.getArrivalStation3(), 
      errorCodes);
    validateDepartureStation4(railIndustryTAABean.getDepartureStation4(), 
      errorCodes);
    validateDepartureDate4(railIndustryTAABean.getDepartureDate4(), 
      errorCodes);
    validateArrivalStation4(railIndustryTAABean.getArrivalStation4(), 
      errorCodes);
  }
  
  private static void validateTransactionType(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD213_TRANSACTION_TYPE);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ã']))
    {
      String reqLength = CommonValidator.validateLength(value, 3, 3);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD213_TRANSACTION_TYPE);
        errorCodes.add(errorObj);
      }
      else if (!value.equalsIgnoreCase("TKT"))
      {
        if (!value.equalsIgnoreCase("REF")) {
          if (!value.equalsIgnoreCase("EXC")) {
            if (!value.equalsIgnoreCase("MSC"))
            {
              errorObj = new ErrorObject(
                SubmissionErrorCodes.ERROR_DATAFIELD213_TRANSACTION_TYPE);
              errorCodes.add(errorObj);
            }
          }
        }
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD213_TRANSACTION_TYPE);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateTicketNumber(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD214_TICKET_NUMBER);
      errorCodes.add(errorObj);
    }
    else
    {
      String reqLength = 
        CommonValidator.validateLength(value, 14, 14);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject("RAIL", 
          "TicketNumber Length is out of Sequence");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validatePassengerName(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD215_PASSENGER_NAME);
      errorCodes.add(errorObj);
    }
    else
    {
      String reqLength = 
        CommonValidator.validateLength(value, 25, 25);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject("RAIL", 
          "PassengerName Length is out of Sequence");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateIataCarrierCode(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Æ']))
      {
        String reqLength = CommonValidator.validateLength(value, 3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject("RAIL", 
            "ItaCarrierCode Length is out of Sequence");
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject("RAIL", 
          "ItaCarrierCode Type is Missing");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateTicketIssuerName(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 32, 32);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject("RAIL", 
          "TicketIssuerName Length is out of Sequence");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateTicketIssueCity(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 18, 18);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject("RAIL", 
          "TicketIssueCity Length is out of Sequence");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateDepartureStation1(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = CommonValidator.validateLength(value, 3, 3);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject("RAIL", 
          "DepartureStation1 Length is out of Sequence");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateDepartureDate1(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ê']))
      {
        String reqLength = CommonValidator.validateLength(value, 8, 8);
        if ((!reqLength.equals("equal")) || 
        
          (!CommonValidator.isValidDate(value, "CCYYMMDD")))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD216_DEPARTURE_DATE1);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD216_DEPARTURE_DATE1);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateArrivalStation1(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = CommonValidator.validateLength(value, 8, 8);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject("RAIL", 
          "validateArrivalStation1 Length is out of Sequence");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateDepartureStation2(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = CommonValidator.validateLength(value, 3, 3);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject("RAIL", 
          "DepartureStation2 Length is out of Sequence");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateDepartureDate2(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Í']))
      {
        String reqLength = CommonValidator.validateLength(value, 8, 8);
        if ((!reqLength.equals("equal")) || 
        
          (!CommonValidator.isValidDate(value, "CCYYMMDD")))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD217_DEPARTURE_DATE2);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD217_DEPARTURE_DATE2);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateArrivalStation2(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = CommonValidator.validateLength(value, 8, 8);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject("RAIL", 
          "validateArrivalStation2 Length is out of Sequence");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateDepartureStation3(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = CommonValidator.validateLength(value, 3, 3);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject("RAIL", 
          "DepartureStation3 Length is out of Sequence");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateDepartureDate3(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ð']))
      {
        String reqLength = CommonValidator.validateLength(value, 8, 8);
        if ((!reqLength.equals("equal")) || 
        
          (!CommonValidator.isValidDate(value, "CCYYMMDD")))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD218_DEPARTURE_DATE3);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD218_DEPARTURE_DATE3);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateArrivalStation3(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = CommonValidator.validateLength(value, 8, 8);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject("RAIL", 
          "validateArrivalStation3 Length is out of Sequence");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateDepartureStation4(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = CommonValidator.validateLength(value, 3, 3);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject("RAIL", 
          "DepartureStation4 Length is out of Sequence");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateDepartureDate4(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ó']))
      {
        String reqLength = CommonValidator.validateLength(value, 8, 8);
        if ((!reqLength.equals("equal")) || 
        
          (!CommonValidator.isValidDate(value, "CCYYMMDD")))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD219_DEPARTURE_DATE4);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD219_DEPARTURE_DATE4);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateArrivalStation4(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = CommonValidator.validateLength(value, 8, 8);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject("RAIL", 
          "validateArrivalStation4 Length is out of Sequence");
        errorCodes.add(errorObj);
      }
    }
  }
}
