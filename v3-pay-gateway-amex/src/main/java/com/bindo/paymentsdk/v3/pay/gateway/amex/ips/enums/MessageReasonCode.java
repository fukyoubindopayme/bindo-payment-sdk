package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums;

public enum MessageReasonCode {
    value1("1900"),
    value2("1400"),
    value3("8700");

    private final String messageReasonCode;

    private MessageReasonCode(String messageReasonCode) {
        this.messageReasonCode = messageReasonCode;
    }

    public String getMessageReasonCode() {
        return this.messageReasonCode;
    }
}
