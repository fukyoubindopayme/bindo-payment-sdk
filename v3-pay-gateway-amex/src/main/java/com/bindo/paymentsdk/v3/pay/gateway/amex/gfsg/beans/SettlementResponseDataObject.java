package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans;

import java.util.List;

public class SettlementResponseDataObject
{
  private String merchantId = "";
  private String numberOfDebits = "";
  private String hashTotalDebitAmount = "";
  private String numberOfCredits = "";
  private String hashTotalCreditAmount = "";
  private String hashTotalAmount = "";
  private String errorCode = "";
  private String fileStatus = "";
  private String submitterId = "";
  private String submitterFileReferenceNumber = "";
  private String submitterFileSequenceNumber = "";
  private String fileCreationDateTime = "";
  private String fileReceiptDateTime = "";
  private List<ErrorObject> actionCodeDescription;
  
  public String getMerchantId()
  {
    return this.merchantId;
  }
  
  public String getNumberOfDebits()
  {
    return this.numberOfDebits;
  }
  
  public String getHashTotalDebitAmount()
  {
    return this.hashTotalDebitAmount;
  }
  
  public String getNumberOfCredits()
  {
    return this.numberOfCredits;
  }
  
  public String getHashTotalCreditAmount()
  {
    return this.hashTotalCreditAmount;
  }
  
  public String getHashTotalAmount()
  {
    return this.hashTotalAmount;
  }
  
  public String getErrorCode()
  {
    return this.errorCode;
  }
  
  public String getFileStatus()
  {
    return this.fileStatus;
  }
  
  public String getSubmitterId()
  {
    return this.submitterId;
  }
  
  public String getSubmitterFileReferenceNumber()
  {
    return this.submitterFileReferenceNumber;
  }
  
  public String getSubmitterFileSequenceNumber()
  {
    return this.submitterFileSequenceNumber;
  }
  
  public String getFileCreationDateTime()
  {
    return this.fileCreationDateTime;
  }
  
  public String getFileReceiptDateTime()
  {
    return this.fileReceiptDateTime;
  }
  
  public void setMerchantId(String merchantId)
  {
    this.merchantId = merchantId;
  }
  
  public void setNumberOfDebits(String numberOfDebits)
  {
    this.numberOfDebits = numberOfDebits;
  }
  
  public void setHashTotalDebitAmount(String hashTotalDebitAmount)
  {
    this.hashTotalDebitAmount = hashTotalDebitAmount;
  }
  
  public void setNumberOfCredits(String numberOfCredits)
  {
    this.numberOfCredits = numberOfCredits;
  }
  
  public void setHashTotalCreditAmount(String hashTotalCreditAmount)
  {
    this.hashTotalCreditAmount = hashTotalCreditAmount;
  }
  
  public void setHashTotalAmount(String hashTotalAmount)
  {
    this.hashTotalAmount = hashTotalAmount;
  }
  
  public void setErrorCode(String errorCode)
  {
    this.errorCode = errorCode;
  }
  
  public void setFileStatus(String fileStatus)
  {
    this.fileStatus = fileStatus;
  }
  
  public void setSubmitterId(String submitterId)
  {
    this.submitterId = submitterId;
  }
  
  public void setSubmitterFileReferenceNumber(String submitterFileReferenceNumber)
  {
    this.submitterFileReferenceNumber = submitterFileReferenceNumber;
  }
  
  public void setSubmitterFileSequenceNumber(String submitterFileSequenceNumber)
  {
    this.submitterFileSequenceNumber = submitterFileSequenceNumber;
  }
  
  public void setFileCreationDateTime(String fileCreationDateTime)
  {
    this.fileCreationDateTime = fileCreationDateTime;
  }
  
  public void setFileReceiptDateTime(String fileReceiptDateTime)
  {
    this.fileReceiptDateTime = fileReceiptDateTime;
  }
  
  public List<ErrorObject> getActionCodeDescription()
  {
    return this.actionCodeDescription;
  }
  
  public void setActionCodeDescription(List<ErrorObject> actionCodeDescription)
  {
    this.actionCodeDescription = actionCodeDescription;
  }
}
