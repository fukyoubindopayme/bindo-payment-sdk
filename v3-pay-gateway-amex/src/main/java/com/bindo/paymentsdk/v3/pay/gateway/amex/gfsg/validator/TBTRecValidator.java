package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.ErrorObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceBasicBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionBatchTrailerBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionTBTSpecificBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.CountryCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.ProcessingCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.RecordType;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.SubmissionErrorCodes;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.SubmissionMethod;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.exceptions.SettlementException;
import java.util.Iterator;
import java.util.List;

public class TBTRecValidator
{
  private static String recordNumber;
  
  public static void validateTBTRecord(TransactionTBTSpecificBean transactionTBTSpecificBean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    recordNumber = 
      transactionTBTSpecificBean.getTransactionBatchTrailerBean().getRecordNumber();
    validateRecordType(transactionTBTSpecificBean
      .getTransactionBatchTrailerBean(), errorCodes);
    validateRecordNumber(transactionTBTSpecificBean
      .getTransactionBatchTrailerBean(), errorCodes);
    validateMerchantId(transactionTBTSpecificBean, errorCodes);
    validateServAgentMerchantId(transactionTBTSpecificBean, errorCodes);
    validateTBTId(transactionTBTSpecificBean
      .getTransactionBatchTrailerBean(), errorCodes);
    validateTBTCreationDate(transactionTBTSpecificBean
      .getTransactionBatchTrailerBean(), errorCodes);
    validateTotalNoOfTabs(transactionTBTSpecificBean, errorCodes);
    validateTBTAmount(transactionTBTSpecificBean, errorCodes);
    
    validateTBTCurrencyCode(transactionTBTSpecificBean, errorCodes);
    validateTBTImageSequenceNumber(transactionTBTSpecificBean, errorCodes);
  }
  
  private static void validateRecordType(TransactionBatchTrailerBean transactionBatchTrailerBean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(transactionBatchTrailerBean.getRecordType()))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE
        .getErrorDescription() + 
        "\n" + 
        "TBT" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "RecordType:" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (!RecordType.Transaction_Batch_Trailer.getRecordType().equalsIgnoreCase(transactionBatchTrailerBean.getRecordType()))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateRecordNumber(TransactionBatchTrailerBean transactionBatchTrailerBean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(transactionBatchTrailerBean.getRecordNumber()))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD585_RECORDNUMBER
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD585_RECORDNUMBER
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TBT" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "RecordNumber:" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(transactionBatchTrailerBean.getRecordNumber(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[112]))
    {
      String reqLength = CommonValidator.validateLength(
        transactionBatchTrailerBean.getRecordNumber(), 8, 8);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD3_RECORDNUMBER
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD3_RECORDNUMBER
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          "TBT" + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "RecordNumber:" + 
          "|" + 
          "This field length Cannot be greater than 8");
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD2_RECORDNUMBER
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD2_RECORDNUMBER
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TBT" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "RecordNumber:" + 
        "|" + 
        "This field can only be Numeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateMerchantId(TransactionTBTSpecificBean transactionTBTSpecifictType, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    
    String value = transactionTBTSpecifictType
      .getTransactionBatchTrailerBean().getMerchantId();
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD117_MERCHANT_ID
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD117_MERCHANT_ID
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TBT" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "MerchantId" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[113]))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 15, 15);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD116_MERCHANT_ID
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD116_MERCHANT_ID
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          "TBT" + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "MerchantId" + 
          "|" + 
          "This field length Cannot be greater than 15");
        errorCodes.add(errorObj);
      }
      else if (CommonValidator.isValidModulusNineCheck(value))
      {
        if (!compareMerchantId(transactionTBTSpecifictType))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD118_MERCHANT_ID
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD118_MERCHANT_ID
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            "TBT" + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "MerchantId" + 
            "|" + 
            SubmissionErrorCodes.ERROR_DATAFIELD118_MERCHANT_ID
            .getErrorDescription());
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD116_MERCHANT_ID
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD116_MERCHANT_ID
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          "TBT" + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "MerchantId" + 
          "|" + 
          SubmissionErrorCodes.ERROR_DATAFIELD116_MERCHANT_ID
          .getErrorDescription());
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD116_MERCHANT_ID
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD116_MERCHANT_ID
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TBT" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "MerchantId" + 
        "|" + 
        "This field can only be AlphaNumeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateServAgentMerchantId(TransactionTBTSpecificBean transactionTBTSpecifictType, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    
    String value = transactionTBTSpecifictType
      .getTransactionBatchTrailerBean().getServAgntMerchID();
    String countryCode = null;
    
    Iterator localIterator = transactionTBTSpecifictType.getTransactionAdviceBasicType().iterator();
    while (localIterator.hasNext())
    {
      TransactionAdviceBasicBean transactionAdviceBasicType = (TransactionAdviceBasicBean)localIterator.next();
      if (transactionAdviceBasicType.getLocationDetailTAABean() != null) {
        countryCode = transactionAdviceBasicType.getLocationDetailTAABean().getLocationCountryCode();
      }
    }
    if (!CommonValidator.isNullOrEmpty(value)) {
      if ((CountryCode.United_States.getCountryCode().equals(countryCode)) && (!CommonValidator.isNullOrEmpty(countryCode)))
      {
        if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ǁ']))
        {
          String reqLength = CommonValidator.validateLength(value, 15, 1);
          if (!reqLength.equals("equal"))
          {
            errorObj = new ErrorObject(
              SubmissionErrorCodes.ERROR_DATAFIELD619_TBTSERV_AGNT_MERCHNUMBER_INVALID
              .getErrorCode(), 
              SubmissionErrorCodes.ERROR_DATAFIELD619_TBTSERV_AGNT_MERCHNUMBER_INVALID
              .getErrorDescription() + 
              "\n" + 
              "RecordType:" + 
              "TBT" + 
              "|" + 
              "RecordNumber:" + 
              recordNumber + 
              "|" + 
              "Service Agent MerchantId" + 
              "|" + 
              "This field length Cannot be greater than 15");
            errorCodes.add(errorObj);
          }
        }
        else
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD619_TBTSERV_AGNT_MERCHNUMBER_INVALID
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD619_TBTSERV_AGNT_MERCHNUMBER_INVALID
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            "TBT" + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "Service Agent MerchantId" + 
            "|" + 
            "This field can only be AlphaNumeric");
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD617_TBTSERV_AGNT_MERCHNUMBER_SPACES
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD617_TBTSERV_AGNT_MERCHNUMBER_SPACES
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          "TBT" + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "Service Agent MerchantId" + 
          "|" + 
          "This field is required only for U.S. Service Agents");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateTBTId(TransactionBatchTrailerBean transactionBatchTrailerBean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(transactionBatchTrailerBean.getTbtIdentificationNumber())) {
      if (CommonValidator.validateData(transactionBatchTrailerBean.getTbtIdentificationNumber(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[115]))
      {
        String reqLength = CommonValidator.validateLength(
          transactionBatchTrailerBean
          .getTbtIdentificationNumber(), 15, 15);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            "TBT" + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "TBTID" + 
            "|" + 
            "This field length Cannot be greater than 15");
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD122_TBTIDENTIFICATION_NUMBER
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD122_TBTIDENTIFICATION_NUMBER
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          "TBT" + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "TBTID" + 
          "|" + 
          "This field can only be Numeric");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateTBTCreationDate(TransactionBatchTrailerBean transactionBatchTrailerBean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(transactionBatchTrailerBean.getTbtCreationDate()))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD123_TBT_CREATION_DATE
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD123_TBT_CREATION_DATE
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TBT" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "TbtCreationDate" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(transactionBatchTrailerBean.getTbtCreationDate(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[116]))
    {
      String reqLength = CommonValidator.validateLength(
        transactionBatchTrailerBean.getTbtCreationDate(), 8, 8);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD123_TBT_CREATION_DATE
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD123_TBT_CREATION_DATE
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          "TBT" + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "TbtCreationDate" + 
          "|" + 
          "This field length Cannot be greater than 8");
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD123_TBT_CREATION_DATE
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD123_TBT_CREATION_DATE
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TBT" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "TbtCreationDate" + 
        "|" + 
        "This field can only be Numeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateTotalNoOfTabs(TransactionTBTSpecificBean transactionTBTSpecificBean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    String noOfTBTTABS = transactionTBTSpecificBean
      .getTransactionBatchTrailerBean().getTotalNoOfTabs();
    if (CommonValidator.isNullOrEmpty(noOfTBTTABS))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD595_NUMBEROF_TABS
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD595_NUMBEROF_TABS
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TBT" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "TotalNoOfTabs" + 
        "|" + 
        "This field can only be Numeric");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(noOfTBTTABS, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[117]))
    {
      String reqLength = CommonValidator.validateLength(noOfTBTTABS, 
        8, 8);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD595_NUMBEROF_TABS
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD595_NUMBEROF_TABS
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          "TBT" + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "TotalNoOfTabs" + 
          "|" + 
          "This field length Cannot be greater than 8");
        errorCodes.add(errorObj);
      }
      else
      {
        int noOfTABS = transactionTBTSpecificBean
          .getTransactionAdviceBasicType().size();
        if (Integer.parseInt(noOfTBTTABS) != noOfTABS)
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD125_TOTAL_NOOFTABS
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD125_TOTAL_NOOFTABS
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            "TBT" + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "TotalNoOfTabs" + 
            "|" + 
            SubmissionErrorCodes.ERROR_DATAFIELD125_TOTAL_NOOFTABS
            .getErrorDescription());
          errorCodes.add(errorObj);
        }
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD124_TOTALNO_OFTABS
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD124_TOTALNO_OFTABS
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TBT" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "TotalNoOfTabs" + 
        "|" + 
        "This field can only be Numeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateTBTAmount(TransactionTBTSpecificBean transactionTBTSpecificBean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    long totalTABAmout = 0L;
    StringBuffer sign = new StringBuffer();
    String tbtAmt = transactionTBTSpecificBean
      .getTransactionBatchTrailerBean().getTbtAmount();
    if (CommonValidator.isNullOrEmpty(tbtAmt))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD595_TBT_AMT
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD595_TBT_AMT
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TBT" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "TBTAmount" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(tbtAmt, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[118]))
    {
      String reqLength = CommonValidator.validateLength(tbtAmt, 20, 
        20);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD129_TBT_AMOUNT
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD129_TBT_AMOUNT
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          "TBT" + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "TBTAmount" + 
          "|" + 
          "This field length Cannot be greater than 20");
        errorCodes.add(errorObj);
      }
      else
      {
        totalTABAmout = calculateTotalTABAmount(transactionTBTSpecificBean, sign);
        if (Long.parseLong(tbtAmt) != totalTABAmout)
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD128_TBT_AMOUNT
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD128_TBT_AMOUNT
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            "TBT" + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "TBTAmount" + 
            "|" + 
            SubmissionErrorCodes.ERROR_DATAFIELD128_TBT_AMOUNT
            .getErrorDescription());
          errorCodes.add(errorObj);
        }
        if (!CommonValidator.isValidAmount(transactionTBTSpecificBean.getTransactionBatchTrailerBean().getTbtCurrencyCode(), tbtAmt))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD129_TBT_AMOUNT
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD129_TBT_AMOUNT
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            "TBT" + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "TBTAmount" + 
            "|" + 
            SubmissionErrorCodes.ERROR_DATAFIELD129_TBT_AMOUNT
            .getErrorDescription());
          errorCodes.add(errorObj);
        }
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD127_TBT_AMOUNT
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD127_TBT_AMOUNT
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TBT" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "TBTAmount" + 
        "|" + 
        "This field can only be Numeric");
      errorCodes.add(errorObj);
    }
    if (!CommonValidator.isNullOrEmpty(sign.toString())) {
      validateTBTAmountSign(
        transactionTBTSpecificBean.getTransactionBatchTrailerBean(), sign.toString(), errorCodes);
    }
  }
  
  private static void validateTBTAmountSign(TransactionBatchTrailerBean transactionBatchTrailerBean, String sign, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(transactionBatchTrailerBean.getTbtAmountSign()))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD130_TBTAMOUNT_SIGN
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD130_TBTAMOUNT_SIGN
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TBT" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "TbtAmountSign" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if ((!CommonValidator.validateData(transactionBatchTrailerBean.getTbtAmountSign(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[119])) ||
      (!transactionBatchTrailerBean.getTbtAmountSign().equals(sign)))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD130_TBTAMOUNT_SIGN
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD130_TBTAMOUNT_SIGN
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TBT" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "TbtAmountSign" + 
        "|" + 
        SubmissionErrorCodes.ERROR_DATAFIELD130_TBTAMOUNT_SIGN.getErrorDescription());
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateTBTCurrencyCode(TransactionTBTSpecificBean transactionTBTSpecificBean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    String tbtCurCode = transactionTBTSpecificBean
      .getTransactionBatchTrailerBean().getTbtCurrencyCode();
    if (CommonValidator.isNullOrEmpty(tbtCurCode))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD132_TBTCURRENCYCODE
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD132_TBTCURRENCYCODE
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TBT" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "TbtCurrencyCode" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(tbtCurCode, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[120]))
    {
      String reqLength = CommonValidator.validateLength(tbtCurCode, 
        3, 3);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD132_TBTCURRENCYCODE
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD132_TBTCURRENCYCODE
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          "TBT" + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "TbtCurrencyCode" + 
          "|" + 
          "This field length Cannot be greater than 3");
        errorCodes.add(errorObj);
      }
      else
      {
        if (!compareCurrencyCode(transactionTBTSpecificBean))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD132_TBTCURRENCYCODE
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD132_TBTCURRENCYCODE
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            "TBT" + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "TbtCurrencyCode" + 
            "|" + 
            SubmissionErrorCodes.ERROR_DATAFIELD132_TBTCURRENCYCODE
            .getErrorDescription());
          errorCodes.add(errorObj);
        }
        if (CommonValidator.isProhibitedCurrencyCode(tbtCurCode))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD133_TBTCURRENCYCODE
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD133_TBTCURRENCYCODE
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            "TBT" + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "TransactionCurrencyCode" + 
            "|" + 
            SubmissionErrorCodes.ERROR_DATAFIELD47_CURRENCY_CODE
            .getErrorDescription());
          errorCodes.add(errorObj);
        }
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD132_TBTCURRENCYCODE
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD132_TBTCURRENCYCODE
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TBT" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "TbtCurrencyCode" + 
        "|" + 
        "This field can only be AlphaNumeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateTBTImageSequenceNumber(TransactionTBTSpecificBean transactionTBTSpecificBean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    
    String imageSeqNum = "";
    
    imageSeqNum = transactionTBTSpecificBean
      .getTransactionBatchTrailerBean().getTbtImageSequenceNumber();
    if (CommonValidator.isNullOrEmpty(imageSeqNum))
    {
      if (!transactionTBTSpecificBean.getTransactionAdviceBasicType().isEmpty())
      {
        Iterator localIterator = transactionTBTSpecificBean.getTransactionAdviceBasicType().iterator();
        while (localIterator.hasNext())
        {
          TransactionAdviceBasicBean tabBean = (TransactionAdviceBasicBean)localIterator.next();
          if (!CommonValidator.isNullOrEmpty(tabBean.getSubmissionMethod())) {
            if (SubmissionMethod.Paper_external_vendor_paper_processor.getSubmissionMethod().equals(tabBean.getSubmissionMethod()))
            {
              errorObj = new ErrorObject(
                SubmissionErrorCodes.ERROR_DATAFIELD136_TBTIMAGE_SEQUENCENUMBER
                .getErrorCode(), 
                SubmissionErrorCodes.ERROR_DATAFIELD136_TBTIMAGE_SEQUENCENUMBER
                .getErrorDescription() + 
                "\n" + 
                "RecordType:" + 
                "TBT" + 
                "|" + 
                "RecordNumber:" + 
                recordNumber);
              errorCodes.add(errorObj);
            }
          }
        }
      }
    }
    else if (CommonValidator.validateData(imageSeqNum, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[124]))
    {
      String reqLength = CommonValidator.validateLength(imageSeqNum, 
        8, 8);
      if (!reqLength.equals("equal"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD135_TBTIMAGE_SEQUENCENUMBER);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD135_TBTIMAGE_SEQUENCENUMBER);
      errorCodes.add(errorObj);
    }
  }
  
  private static boolean compareMerchantId(TransactionTBTSpecificBean transBean)
  {
    boolean isvalid = true;
    
    Iterator localIterator = transBean.getTransactionAdviceBasicType().iterator();
    while (localIterator.hasNext())
    {
      TransactionAdviceBasicBean transactionAdviceBasicType = (TransactionAdviceBasicBean)localIterator.next();
      if (!transBean.getTransactionBatchTrailerBean().getMerchantId().equals(transactionAdviceBasicType.getMerchantId())) {
        isvalid = false;
      }
    }
    return isvalid;
  }
  
  private static long calculateTotalTABAmount(TransactionTBTSpecificBean transactionTBTSpecificBean, StringBuffer sign)
  {
    long totTABAmt = 0L;
    long totDebitTABAmt = 0L;
    long totCreditTABAmt = 0L;
    
    Iterator localIterator = transactionTBTSpecificBean.getTransactionAdviceBasicType().iterator();
    while (localIterator.hasNext())
    {
      TransactionAdviceBasicBean transactionAdviceBasicType = (TransactionAdviceBasicBean)localIterator.next();
      if ((transactionAdviceBasicType.getProcessingCode().equals(ProcessingCode.DEBIT.getProcessingCode())) || (transactionAdviceBasicType.getProcessingCode().equals(ProcessingCode.CREDIT_REVERSALS.getProcessingCode())) || 
        (transactionAdviceBasicType.getProcessingCode().equals(ProcessingCode.BONUS_PAY_DEBIT.getProcessingCode())) || (transactionAdviceBasicType.getProcessingCode().equals(ProcessingCode.BONUS_PAY_CREDIT_REVERSAL.getProcessingCode()))) {
        totDebitTABAmt = totDebitTABAmt + Long.parseLong(transactionAdviceBasicType.getTransactionAmount());
      } else if ((transactionAdviceBasicType.getProcessingCode().equals(ProcessingCode.CREDIT.getProcessingCode())) || (transactionAdviceBasicType.getProcessingCode().equals(ProcessingCode.DEBIT_REVERSALS.getProcessingCode())) || 
        (transactionAdviceBasicType.getProcessingCode().equals(ProcessingCode.BONUS_PAY_CREDIT.getProcessingCode())) || (transactionAdviceBasicType.getProcessingCode().equals(ProcessingCode.BONUS_PAY_DEBIT_REVERSAL.getProcessingCode()))) {
        totCreditTABAmt = totCreditTABAmt + Long.parseLong(transactionAdviceBasicType.getTransactionAmount());
      }
    }
    if (totDebitTABAmt < totCreditTABAmt)
    {
      totTABAmt = totCreditTABAmt - totDebitTABAmt;
      sign.append("-");
    }
    else
    {
      totTABAmt = totDebitTABAmt - totCreditTABAmt;
      sign.append("+");
    }
    return totTABAmt;
  }
  
  private static boolean compareCurrencyCode(TransactionTBTSpecificBean transBean)
  {
    boolean isvalid = true;
    
    Iterator localIterator = transBean.getTransactionAdviceBasicType().iterator();
    while (localIterator.hasNext())
    {
      TransactionAdviceBasicBean transactionAdviceBasicType = (TransactionAdviceBasicBean)localIterator.next();
      if (!transBean.getTransactionBatchTrailerBean().getTbtCurrencyCode().equalsIgnoreCase(transactionAdviceBasicType.getTransactionCurrencyCode())) {
        isvalid = false;
      }
    }
    return isvalid;
  }
}
