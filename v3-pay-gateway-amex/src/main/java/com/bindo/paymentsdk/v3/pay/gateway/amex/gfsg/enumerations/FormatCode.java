package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations;

public enum FormatCode {
    Airline_TAA_Record("01"),
    No_Industry_Specific_TAA_Record("02"),
    Insurance_TAA_Record("04"),
    Auto_Rental_TAA_Record("05"),
    Rail_Rental_TAA_Record("06"),
    Lodging_TAA_Record("11"),
    Communication_Services_TAA_Record("13"),
    Travel_Cruise_TAA_Record("14"),
    General_Retail_TAA_Record("20"),
    Entertainment_Ticketing_TAA_Record("22");

    private final String formatCode;

    private FormatCode(String formatCode) {
        this.formatCode = formatCode;
    }

    public String getFormatCode() {
        return this.formatCode;
    }
}
