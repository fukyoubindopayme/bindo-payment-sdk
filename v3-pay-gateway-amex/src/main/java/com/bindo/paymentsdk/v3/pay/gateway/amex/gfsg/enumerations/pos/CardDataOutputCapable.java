package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.pos;

public enum CardDataOutputCapable
{
  POC_UNKONOWN("0"),  POC_NONE("1"),  MAGNETIC_STRIPE_WRITE("2"),  INTEGRATED_CIRCUIT_CARD(
    "3"),  RESERVE_FOR_ISO_USE_4("4"),  RESERVE_FOR_ISO_USE_5("5"),  RESERVE_FOR_NATIONAL_USE_6(
    "6"),  RESERVE_FOR_NATIONAL_USE_7("7"),  RESERVE_FOR_PRIVATE_USE_8(
    "8"),  RESERVE_FOR_PRIVATE_USE_9("9"),  RESERVE_FOR_ISO_USE_A("A"),  RESERVE_FOR_ISO_USE_B(
    "B"),  RESERVE_FOR_ISO_USE_C("C"),  RESERVE_FOR_ISO_USE_D("D"),  RESERVE_FOR_ISO_USE_E(
    "E"),  RESERVE_FOR_ISO_USE_F("F"),  RESERVE_FOR_ISO_USE_G("G"),  RESERVE_FOR_ISO_USE_H(
    "H"),  RESERVE_FOR_ISO_USE_I("I"),  RESERVE_FOR_NATIONAL_USE_J("J"),  RESERVE_FOR_NATIONAL_USE_K(
    "K"),  RESERVE_FOR_NATIONAL_USE_L("L"),  RESERVE_FOR_NATIONAL_USE_M(
    "M"),  RESERVE_FOR_NATIONAL_USE_N("N"),  RESERVE_FOR_NATIONAL_USE_O(
    "O"),  RESERVE_FOR_NATIONAL_USE_P("P"),  RESERVE_FOR_NATIONAL_USE_Q(
    "Q"),  RESERVE_FOR_NATIONAL_USE_R("R"),  RESERVE_FOR_PRIVATE_USE_S(
    "S"),  RESERVE_FOR_PRIVATE_USE_T("T"),  RESERVE_FOR_PRIVATE_USE_U("U"),  RESERVE_FOR_PRIVATE_USE_V(
    "V"),  RESERVE_FOR_PRIVATE_USE_W("W"),  RESERVE_FOR_PRIVATE_USE_X("X"),  RESERVE_FOR_PRIVATE_USE_Y(
    "Y"),  RESERVE_FOR_PRIVATE_USE_Z("Z");
  
  private final String cardDataOutputCapable;
  
  private CardDataOutputCapable(String cardDataOutputCap)
  {
    this.cardDataOutputCapable = cardDataOutputCap;
  }
  
  public String getCardDataOutputCapable()
  {
    return this.cardDataOutputCapable;
  }
}
