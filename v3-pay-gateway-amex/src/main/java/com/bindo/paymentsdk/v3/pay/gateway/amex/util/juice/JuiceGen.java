package com.bindo.paymentsdk.v3.pay.gateway.amex.util.juice;

public class JuiceGen {
    private String algName;
    private String keyName;
    private String fn;
    private boolean keyToBeStored = false;
    protected int numKeys = 1;

    public JuiceGen() {
    }

    public String describeOptions() {
        return "Options:\n\t-alg\tname\tSet crypto algorithm name (default=DES-EDE3)\n\t-count\tvalue\tSet number of keys to generate (default=1)\n\t-fn\t\tname\tSet properties file name\n\t-keynm\tname\tSet encryption/decryption key name\n\t-store\t\t\tStore generated key in a properties file.\n";
    }

    public String describeUsage() {
        return "Usage:\n    java " + this.getClass().getName() + " [options...]\n" + this.describeOptions();
    }

    private void genKey() throws Exception {
        String appKeyName = this.getKeyName();
        String algNm = this.getAlgName();
        String propFn = this.getFn();
        int nKeys = this.getNumKeys();
        Juice cWrap = null;
        String appRawKey = null;

        try {
            if (algNm == null) {
                cWrap = new Juice();
                algNm = cWrap.getName();
            } else if (algNm.indexOf("/") == -1) {
                algNm = algNm + "/ECB/NoPadding";
            }

            cWrap = new Juice(algNm);
            System.out.println("Generating a new secret key using " + algNm);

            for(int i = 0; i < nKeys; ++i) {
                appRawKey = cWrap.generateKey();
                if (this.isKeyToBeStored()) {
                    nKeys = 1;
                    if (propFn == null || appKeyName == null) {
                        throw new OptionException("File name and key name must be specified to store a generated key", 2);
                    }

                    cWrap.storeEncryptedKey(appRawKey, appKeyName, propFn);
                    System.out.println("New secret key=\n" + appRawKey + "\n stored in " + this.fn + " as " + appKeyName);
                } else {
                    System.out.println("New secret key=\n" + appRawKey);
                }
            }

        } catch (Exception var8) {
            var8.printStackTrace();
            throw new Exception(var8.getClass() + ": " + var8.getMessage());
        }
    }

    public String getAlgName() {
        return this.algName;
    }

    public String getFn() {
        return this.fn;
    }

    public String getKeyName() {
        return this.keyName;
    }

    public int getNumKeys() {
        return this.numKeys;
    }

    public boolean isKeyToBeStored() {
        return this.keyToBeStored;
    }

    public static void main(String[] args) {
        JuiceGen cbt = new JuiceGen();

        try {
            for(int i = 0; i < args.length; ++i) {
                i = cbt.parseOption(args, i);
            }

            cbt.genKey();
            System.out.println("JuiceGen successful");
        } catch (Exception var3) {
            System.out.println("\n*** Exception: " + var3.getMessage());
            var3.printStackTrace(System.out);
        }

    }

    protected int parseOption(String[] args, int offset) throws OptionException {
        if (args[offset].equalsIgnoreCase("-alg")) {
            ++offset;
            this.setAlgName(args[offset]);
        } else if (args[offset].equalsIgnoreCase("-count")) {
            ++offset;
            this.setNumKeys(Integer.parseInt(args[offset]));
        } else if (args[offset].equalsIgnoreCase("-store")) {
            this.setKeyToBeStored(true);
        } else if (args[offset].equalsIgnoreCase("-fn")) {
            ++offset;
            this.setFn(args[offset]);
        } else {
            if (!args[offset].equalsIgnoreCase("-keynm")) {
                System.err.println(this.describeUsage());
                throw new OptionException("Unrecognized option: '" + args[offset] + "'", 1);
            }

            ++offset;
            this.setKeyName(args[offset]);
        }

        return offset;
    }

    protected void setAlgName(String newAlgName) {
        this.algName = newAlgName;
    }

    protected void setFn(String newFn) {
        this.fn = newFn;
    }

    protected void setKeyName(String newKeyName) {
        this.keyName = newKeyName;
    }

    protected void setKeyToBeStored(boolean newKeyToBeStored) {
        this.keyToBeStored = newKeyToBeStored;
    }

    protected void setNumKeys(int newNumKeys) {
        this.numKeys = newNumKeys;
    }
}
