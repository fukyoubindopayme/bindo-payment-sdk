package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.IndustryTypeTAABean;

public class RetailIndustryTAABean
  extends IndustryTypeTAABean
{
  private String retailDepartmentName;
  private String retailItemDescription1;
  private String retailItemQuantity1;
  private String retailItemAmount1;
  private String retailItemDescription2;
  private String retailItemQuantity2;
  private String retailItemAmount2;
  private String retailItemDescription3;
  private String retailItemQuantity3;
  private String retailItemAmount3;
  private String retailItemDescription4;
  private String retailItemQuantity4;
  private String retailItemAmount4;
  private String retailItemDescription5;
  private String retailItemQuantity5;
  private String retailItemAmount5;
  private String retailItemDescription6;
  private String retailItemQuantity6;
  private String retailItemAmount6;
  
  public String getRetailDepartmentName()
  {
    return this.retailDepartmentName;
  }
  
  public void setRetailDepartmentName(String retailDepartmentName)
  {
    this.retailDepartmentName = retailDepartmentName;
  }
  
  public String getRetailItemDescription1()
  {
    return this.retailItemDescription1;
  }
  
  public void setRetailItemDescription1(String retailItemDescription1)
  {
    this.retailItemDescription1 = retailItemDescription1;
  }
  
  public String getRetailItemQuantity1()
  {
    return this.retailItemQuantity1;
  }
  
  public void setRetailItemQuantity1(String retailItemQuantity1)
  {
    this.retailItemQuantity1 = retailItemQuantity1;
  }
  
  public String getRetailItemAmount1()
  {
    return this.retailItemAmount1;
  }
  
  public void setRetailItemAmount1(String retailItemAmount1)
  {
    this.retailItemAmount1 = retailItemAmount1;
  }
  
  public String getRetailItemDescription2()
  {
    return this.retailItemDescription2;
  }
  
  public void setRetailItemDescription2(String retailItemDescription2)
  {
    this.retailItemDescription2 = retailItemDescription2;
  }
  
  public String getRetailItemQuantity2()
  {
    return this.retailItemQuantity2;
  }
  
  public void setRetailItemQuantity2(String retailItemQuantity2)
  {
    this.retailItemQuantity2 = retailItemQuantity2;
  }
  
  public String getRetailItemAmount2()
  {
    return this.retailItemAmount2;
  }
  
  public void setRetailItemAmount2(String retailItemAmount2)
  {
    this.retailItemAmount2 = retailItemAmount2;
  }
  
  public String getRetailItemDescription3()
  {
    return this.retailItemDescription3;
  }
  
  public void setRetailItemDescription3(String retailItemDescription3)
  {
    this.retailItemDescription3 = retailItemDescription3;
  }
  
  public String getRetailItemQuantity3()
  {
    return this.retailItemQuantity3;
  }
  
  public void setRetailItemQuantity3(String retailItemQuantity3)
  {
    this.retailItemQuantity3 = retailItemQuantity3;
  }
  
  public String getRetailItemAmount3()
  {
    return this.retailItemAmount3;
  }
  
  public void setRetailItemAmount3(String retailItemAmount3)
  {
    this.retailItemAmount3 = retailItemAmount3;
  }
  
  public String getRetailItemDescription4()
  {
    return this.retailItemDescription4;
  }
  
  public void setRetailItemDescription4(String retailItemDescription4)
  {
    this.retailItemDescription4 = retailItemDescription4;
  }
  
  public String getRetailItemQuantity4()
  {
    return this.retailItemQuantity4;
  }
  
  public void setRetailItemQuantity4(String retailItemQuantity4)
  {
    this.retailItemQuantity4 = retailItemQuantity4;
  }
  
  public String getRetailItemAmount4()
  {
    return this.retailItemAmount4;
  }
  
  public void setRetailItemAmount4(String retailItemAmount4)
  {
    this.retailItemAmount4 = retailItemAmount4;
  }
  
  public String getRetailItemDescription5()
  {
    return this.retailItemDescription5;
  }
  
  public void setRetailItemDescription5(String retailItemDescription5)
  {
    this.retailItemDescription5 = retailItemDescription5;
  }
  
  public String getRetailItemQuantity5()
  {
    return this.retailItemQuantity5;
  }
  
  public void setRetailItemQuantity5(String retailItemQuantity5)
  {
    this.retailItemQuantity5 = retailItemQuantity5;
  }
  
  public String getRetailItemAmount5()
  {
    return this.retailItemAmount5;
  }
  
  public void setRetailItemAmount5(String retailItemAmount5)
  {
    this.retailItemAmount5 = retailItemAmount5;
  }
  
  public String getRetailItemDescription6()
  {
    return this.retailItemDescription6;
  }
  
  public void setRetailItemDescription6(String retailItemDescription6)
  {
    this.retailItemDescription6 = retailItemDescription6;
  }
  
  public String getRetailItemQuantity6()
  {
    return this.retailItemQuantity6;
  }
  
  public void setRetailItemQuantity6(String retailItemQuantity6)
  {
    this.retailItemQuantity6 = retailItemQuantity6;
  }
  
  public String getRetailItemAmount6()
  {
    return this.retailItemAmount6;
  }
  
  public void setRetailItemAmount6(String retailItemAmount6)
  {
    this.retailItemAmount6 = retailItemAmount6;
  }
}
