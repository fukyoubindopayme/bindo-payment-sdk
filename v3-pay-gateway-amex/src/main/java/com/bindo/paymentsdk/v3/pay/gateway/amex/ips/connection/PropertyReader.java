package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.connection;

import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.utils.ISOUtil;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;

public class PropertyReader {
    public static String MERCHANT_NUMBER = "";
    public static String connectionType = "PaymentSDK";
    public static String USER_AGENT = "Terminal1";
    public static String LOG_FILE_PATH = "d://GCAGDebugLog.log";
    public static String DEBUGGER_FLAG = "ON";
    public static String VALIDATION_FLAG = "ON";
    public static String PROXY = "YES";
    public static String ORIGIN = "SAMPLE-SYSTEMS";
    public static String VALIDATION_ERROR_LOG_FILE_PATH = "D://validationError.log";
    public static String MESSAGE_TYPE = "ISO GCAG";
    public static String BYPASS_VALIDATION_FLAG = "OFF";
    public static String REGION = "USA";
    public static String METHOD_TYPE = "POST";
    public static String ROUTING_INDICATOR = "000";
    public static String MERCHANT_COUNTRY = "";
    public static String TERMINAL_NUMBER = "";
    public static String MCC = "";

    public static String propsLocation = "./authorization.properties";
    public String httpsIP;
    public int Port;
    Properties clientprops = new Properties();
    PropertyReader propertyHolder = null;
    public String PROXY_ADDRESS = "";
    public String PROXY_PORT = "";
    public String PROXY_USER_ID = "";
    public String PROXY_PASSWORD = "";
    public String HTTPS_URL = "";
    public static String ADDITIONAL_DATA_SEC_ID = "";
    public static String proxyAuthentication;
    public static String requestType = "";
    public static String httpsTimeout = "";
    public static String readTimeout = "";

    public PropertyReader() {
    }

    public PropertyReader init() {
        this.propertyHolder = new PropertyReader();
        if (this.clientprops.getProperty("CONNECTION_TYPE").equalsIgnoreCase("DC")) {
            this.propertyHolder.setHttpsIP(this.clientprops.getProperty("HTTPS_IP"));
        } else {
            this.propertyHolder.setPROXY_ADDRESS(this.clientprops.getProperty("PROXY_ADDRESS"));
            this.propertyHolder.setPROXY_PORT(this.clientprops.getProperty("PROXY_PORT"));
            if (!this.clientprops.getProperty("PROXY_USER_ID").equalsIgnoreCase("") && this.clientprops.getProperty("PROXY_USER_ID") != null) {
                this.propertyHolder.setPROXY_USER_ID(this.clientprops.getProperty("PROXY_USER_ID"));
            }

            if (!this.clientprops.getProperty("PROXY_PASSWORD").equalsIgnoreCase("") && this.clientprops.getProperty("PROXY_PASSWORD") != null) {
                this.propertyHolder.setPROXY_PASSWORD(this.clientprops.getProperty("PROXY_PASSWORD"));
            }

            this.propertyHolder.setHTTPS_URL(this.clientprops.getProperty("HTTPS_URL"));
            setProxyAuthentication(this.clientprops.getProperty("PROXY_AUTHENTICATION"));
            setMERCHANT_NUMBER(this.clientprops.getProperty("MERCHANT_NUMBER"));
            setPROXY(this.clientprops.getProperty("PROXY"));
            setREGION(this.clientprops.getProperty("REGION"));
            if (!ISOUtil.IsNullOrEmpty(this.clientprops.getProperty("ROUTING_INDICATOR").trim())) {
                setROUTING_INDICATOR(this.clientprops.getProperty("ROUTING_INDICATOR"));
            }

            setMERCHANT_COUNTRY(this.clientprops.getProperty("MERCHANT_COUNTRY"));
        }

        this.propertyHolder.setPort(new Integer(this.clientprops.getProperty("PORT")));
        setConnectionType(this.clientprops.getProperty("CONNECTION_TYPE"));
        setLOG_FILE_PATH("C://GCAGDebugLog.log");
        setDEBUGGER_FLAG(this.clientprops.getProperty("DEBUGGER_FLAG"));
        setVALIDATION_FLAG(this.clientprops.getProperty("VALIDATION_FLAG"));
        setVALIDATION_ERROR_LOG_FILE_PATH(this.clientprops.getProperty("VALIDATION_ERROR_LOG_FILE_PATH"));
        setBYPASS_VALIDATION_FLAG(this.clientprops.getProperty("BYPASS_VALIDATION_FLAG"));
        setORIGIN(this.clientprops.getProperty("ORIGIN"));
        setADDITIONAL_DATA_SEC_ID(this.clientprops.getProperty("ADDITIONAL_DATA_SEC_ID"));
        setHttpsTimeout(this.clientprops.getProperty("HTTPS_TIMEOUT"));
        setReadTimeout(this.clientprops.getProperty("READ_TIMEOUT"));
        return this.propertyHolder;
    }

    public PropertyReader setProxyDetailsFile(String propertyfilepath) {
        URL url = null;

        try {
            url = PropertyReader.class.getResource(propertyfilepath);
            this.clientprops.load(url.openStream());
            this.propertyHolder = this.init();
        } catch (Exception var12) {
            if ("ON".equalsIgnoreCase(DEBUGGER_FLAG)) {
                Log.e("Error in setProxyDetailsFile() method..  Exception: " + var12);
            }
        } finally {
            try {
                url.openStream().close();
            } catch (IOException var11) {
                if ("ON".equalsIgnoreCase(DEBUGGER_FLAG)) {
                    Log.e("Error in finally block of setProxyDetailsFile() method..  IOException: " + var11);
                }
            }

        }

        return this.propertyHolder;
    }

    public PropertyReader getPropertyValuesDB() {
        Connection connection = null;
        ResultSet resultSet = null;

        try {
            int proxyid = 2;
            Class.forName("com.imaginary.sql.msql.MsqlDriver");
            String url = "jdbc:msql://www.myserver.com:1114/proxy_mgr";
            connection = DriverManager.getConnection(url, "user1", "password");
            String selectStatement = "SELECT * FROM Proxy_Deatils WHERE proxy_id = ?";
            PreparedStatement doSelect = connection.prepareStatement(selectStatement);
            doSelect.setInt(1, proxyid);
            resultSet = doSelect.executeQuery();

            while(resultSet.next()) {
                this.clientprops.put("PROXY_ADDRESS", resultSet.getString(1));
                this.clientprops.put("PROXY_PORT", resultSet.getString(2));
                this.clientprops.put("PROXY_USER_ID", resultSet.getString(3));
                this.clientprops.put("PROXY_PASSWORD", resultSet.getString(4));
            }

            this.propertyHolder = this.init();
        } catch (Exception var15) {
            if ("ON".equalsIgnoreCase(DEBUGGER_FLAG)) {
                Log.e("Error in getPropertyValuesDB() method..  Exception: " + var15);
            }
        } finally {
            try {
                resultSet.close();
                connection.close();
            } catch (Exception var14) {
                if ("ON".equalsIgnoreCase(DEBUGGER_FLAG)) {
                    Log.e("Error in finally block of getPropertyValuesDB() method..  Exception: " + var14);
                }
            }

        }

        return this.propertyHolder;
    }

    public PropertyReader loadPropertyFilesfromAuthJar() throws Exception {
        FileInputStream fis = null;

        try {
            fis = new FileInputStream(propsLocation);
            this.clientprops.load(fis);
            this.propertyHolder = this.init();
        } catch (Exception var6) {
            if ("ON".equalsIgnoreCase(DEBUGGER_FLAG)) {
                Log.e("Error in loadPropertyFilesfromAuthJar() method..  Exception: " + var6);
            }
        } finally {
            fis.close();
        }

        return this.propertyHolder;
    }

    public static String getHttpsTimeout() {
        return httpsTimeout;
    }

    public static void setHttpsTimeout(String httpsTimeout_) {
        httpsTimeout = httpsTimeout_;
    }

    public static String getReadTimeout() {
        return readTimeout;
    }

    public static void setReadTimeout(String readTimeout_) {
        readTimeout = readTimeout_;
    }

    public String getHTTPS_URL() {
        return this.HTTPS_URL;
    }

    public void setHTTPS_URL(String https_url) {
        this.HTTPS_URL = https_url;
    }

    public static String getMERCHANT_NUMBER() {
        return MERCHANT_NUMBER;
    }

    public static void setMERCHANT_NUMBER(String merchant_number) {
        MERCHANT_NUMBER = merchant_number;
    }

    public static String getUSER_AGENT() {
        return USER_AGENT;
    }

    public static void setUSER_AGENT(String user_agent) {
        USER_AGENT = user_agent;
    }

    public static String getLOG_FILE_PATH() {
        return LOG_FILE_PATH;
    }

    public static void setLOG_FILE_PATH(String log_file_path) {
        LOG_FILE_PATH = log_file_path;
    }

    public static String getDEBUGGER_FLAG() {
        return DEBUGGER_FLAG;
    }

    public static void setDEBUGGER_FLAG(String debugger_flag) {
        DEBUGGER_FLAG = debugger_flag;
    }

    public static String getVALIDATION_FLAG() {
        return VALIDATION_FLAG;
    }

    public static void setVALIDATION_FLAG(String validation_flag) {
        VALIDATION_FLAG = validation_flag;
    }

    public static String getPROXY() {
        return PROXY;
    }

    public static void setPROXY(String proxy) {
        PROXY = proxy;
    }

    public static String getORIGIN() {
        return ORIGIN;
    }

    public static void setORIGIN(String origin) {
        ORIGIN = origin;
    }

    public static String getVALIDATION_ERROR_LOG_FILE_PATH() {
        return VALIDATION_ERROR_LOG_FILE_PATH;
    }

    public static void setVALIDATION_ERROR_LOG_FILE_PATH(String validation_error_log_file_path) {
        VALIDATION_ERROR_LOG_FILE_PATH = validation_error_log_file_path;
    }

    public static String getMESSAGE_TYPE() {
        return MESSAGE_TYPE;
    }

    public static void setMESSAGE_TYPE(String message_type) {
        MESSAGE_TYPE = message_type;
    }

    public static String getBYPASS_VALIDATION_FLAG() {
        return BYPASS_VALIDATION_FLAG;
    }

    public static void setBYPASS_VALIDATION_FLAG(String bypass_validation_flag) {
        BYPASS_VALIDATION_FLAG = bypass_validation_flag;
    }

    public static String getREGION() {
        return REGION;
    }

    public static void setREGION(String region) {
        REGION = region;
    }

    public static String getMETHOD_TYPE() {
        return METHOD_TYPE;
    }

    public static void setMETHOD_TYPE(String method_type) {
        METHOD_TYPE = method_type;
    }

    public static String getROUTING_INDICATOR() {
        return ROUTING_INDICATOR;
    }

    public static void setROUTING_INDICATOR(String routing_indicator) {
        ROUTING_INDICATOR = routing_indicator;
    }

    public static String getMERCHANT_COUNTRY() {
        return MERCHANT_COUNTRY;
    }

    public static void setMERCHANT_COUNTRY(String merchant_country) {
        MERCHANT_COUNTRY = merchant_country;
    }


    public static String getTerminalNumber() {
        return TERMINAL_NUMBER;
    }

    public static void setTerminalNumber(String terminalNumber) {
        TERMINAL_NUMBER = terminalNumber;
    }

    public static String getMCC() {
        return MCC;
    }

    public static void setMCC(String MCC) {
        PropertyReader.MCC = MCC;
    }

    public String getPROXY_ADDRESS() {
        return this.PROXY_ADDRESS;
    }

    public void setPROXY_ADDRESS(String proxy_address) {
        this.PROXY_ADDRESS = proxy_address;
    }

    public String getPROXY_PORT() {
        return this.PROXY_PORT;
    }

    public void setPROXY_PORT(String proxy_port) {
        this.PROXY_PORT = proxy_port;
    }

    public String getPROXY_USER_ID() {
        return this.PROXY_USER_ID;
    }

    public void setPROXY_USER_ID(String proxy_user_id) {
        this.PROXY_USER_ID = proxy_user_id;
    }

    public String getPROXY_PASSWORD() {
        return this.PROXY_PASSWORD;
    }

    public void setPROXY_PASSWORD(String proxy_password) {
        this.PROXY_PASSWORD = proxy_password;
    }

    public String getHttpsIP() {
        return this.httpsIP;
    }

    public void setHttpsIP(String httpsIP) {
        this.httpsIP = httpsIP;
    }

    public int getPort() {
        return this.Port;
    }

    public void setPort(int Port) {
        this.Port = Port;
    }

    public static String getConnectionType() {
        return connectionType;
    }

    public static void setConnectionType(String connectionType) {
        connectionType = connectionType;
    }

    public static String getProxyAuthentication() {
        return proxyAuthentication;
    }

    public static void setProxyAuthentication(String proxyAuthentication) {
        proxyAuthentication = proxyAuthentication;
    }

    public static String getADDITIONAL_DATA_SEC_ID() {
        return ADDITIONAL_DATA_SEC_ID;
    }

    public static void setADDITIONAL_DATA_SEC_ID(String additional_data_sec_id) {
        ADDITIONAL_DATA_SEC_ID = additional_data_sec_id;
    }

    public static String getRequestType() {
        return requestType;
    }

    public static void setRequestType(String requestType) {
        requestType = requestType;
    }
}
