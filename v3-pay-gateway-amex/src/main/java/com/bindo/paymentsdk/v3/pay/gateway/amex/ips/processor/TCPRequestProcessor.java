package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.processor;

import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.connection.PropertyReader;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.utils.DataConversion;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.utils.Utility;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class TCPRequestProcessor {
    boolean debugFlag = PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON");

    public TCPRequestProcessor() {
    }

    public synchronized String createConnectionAndRequestSend(String isoRequestMessage, PropertyReader propertyReader) {
        if (this.debugFlag) {
            Log.i("Entered in to createConnectionAndRequestSend");
        }

        DataOutputStream dataOutputStream = null;
        DataInputStream dataInputStream = null;
        Socket socket = null;
        String isoResponseMessage = "";

        try {
            if (this.debugFlag) {
                Log.i("Direct Connection IP Address::" + propertyReader.getHttpsIP());
                Log.i("Direct Connection Port Number::" + propertyReader.getPort());
                Log.i("Before Creating SocketConnection");
            }

            socket = new Socket(propertyReader.getHttpsIP(), propertyReader.getPort());
            if (this.debugFlag) {
                Log.i("SocketConnection is Established");
            }

            byte[] requestByte = this.getHexMessageLength(isoRequestMessage);
            dataOutputStream = new DataOutputStream(socket.getOutputStream());
            dataOutputStream.write(requestByte);
            if (this.debugFlag) {
                Log.i("After Sending Req Message");
            }

            dataOutputStream.flush();
            dataInputStream = new DataInputStream(socket.getInputStream());
            byte[] responseBytes = new byte[1000];
            dataInputStream.read(responseBytes);
            String mesg1 = Utility.getHexMessage(responseBytes);
            if (mesg1 != null && !mesg1.equalsIgnoreCase("")) {
                isoResponseMessage = DataConversion.removeHexMessageLength(mesg1);
            }

            if (this.debugFlag) {
                Log.i("After recieving ISO Response");
            }
        } catch (UnknownHostException var21) {
            if (this.debugFlag) {
                Log.e("Error in Establishing Socket Connection...UnknownHostException: " + var21);
            }
        } catch (IOException var22) {
            if (this.debugFlag) {
                Log.e("Error in Establishing Socket Connection...IOException: " + var22);
            }
        } finally {
            try {
                dataInputStream.close();
                dataOutputStream.close();
                socket.close();
            } catch (IOException var20) {
                if (this.debugFlag) {
                    Log.e("Error in Closing Socket Connection " + var20);
                }
            }

        }

        if (this.debugFlag) {
            Log.i("Exiting from createConnectionAndRequestSend");
        }

        return isoResponseMessage;
    }

    public byte[] getHexMessageLength(String isoRequestMessage) {
        int tempTotalChars = isoRequestMessage.length() / 2 + 2;
        String hexString = Integer.toHexString(tempTotalChars);
        if (hexString.length() < 4) {
            for(int i = hexString.length(); i < 4; ++i) {
                hexString = "0" + hexString;
            }
        }

        isoRequestMessage = hexString + isoRequestMessage;
        byte[] requesByte = DataConversion.decode(isoRequestMessage);
        return requesByte;
    }
}
