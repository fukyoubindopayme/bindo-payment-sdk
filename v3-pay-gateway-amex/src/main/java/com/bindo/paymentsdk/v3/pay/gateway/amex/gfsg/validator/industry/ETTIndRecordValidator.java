package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.industry;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.ErrorObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceAddendumBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceBasicBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.EntertainmentTicketingIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.SubmissionErrorCodes;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.exceptions.SettlementException;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.CommonValidator;
import java.util.List;

public class ETTIndRecordValidator
{
  public static void validateETTIndRecord(TransactionAdviceBasicBean transactionAdviceBasicType, TransactionAdviceAddendumBean transactionAddendumType, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    EntertainmentTicketingIndustryTAABean eIndustryTAABean = (EntertainmentTicketingIndustryTAABean)transactionAddendumType;
    
    TAADBRecordValidator.validateRecordType(eIndustryTAABean
      .getRecordType(), errorCodes);
    TAADBRecordValidator.validateRecordNumber(eIndustryTAABean
      .getRecordNumber(), errorCodes);
    TAADBRecordValidator.validateTransactionIdentifier(eIndustryTAABean
      .getTransactionIdentifier(), transactionAdviceBasicType, 
      errorCodes);
    TAADBRecordValidator.validateAddendaTypeCode(eIndustryTAABean
      .getAddendaTypeCode(), errorCodes);
    TAADBRecordValidator.validateFormatCode(eIndustryTAABean
      .getFormatCode(), transactionAdviceBasicType, errorCodes);
    
    validateEventName(eIndustryTAABean.getEventName(), errorCodes);
    validateEventDate(eIndustryTAABean.getEventDate(), errorCodes);
    validateEventIndividualTicketPriceAmount(eIndustryTAABean
      .getEventIndividualTicketPriceAmount(), errorCodes);
    validateEventTicketQuantity(eIndustryTAABean.getEventTicketQuantity(), 
      errorCodes);
    
    validateEventlocation(eIndustryTAABean.getEventLocation(), errorCodes);
    validateEventRegionCode(eIndustryTAABean, errorCodes);
    validateEventCountryCode(eIndustryTAABean, errorCodes);
  }
  
  private static void validateEventName(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 30, 30);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD294_EVENT_NAME);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateEventDate(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD296_EVENT_DATE);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ī']))
    {
      String reqLength = CommonValidator.validateLength(value, 8, 8);
      if ((!reqLength.equals("equal")) || 
      
        (!CommonValidator.isValidDate(value, "CCYYMMDD")))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD296_EVENT_DATE);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD298_EVENT_DATE);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateEventIndividualTicketPriceAmount(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ĭ']))
      {
        String reqLength = CommonValidator.validateLength(value, 8, 8);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD300_EVENTINDIVIDUAL_TICKETPRICEAMOUNT);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD299_EVENTINDIVIDUAL_TICKETPRICEAMOUNT);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateEventTicketQuantity(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Į']))
      {
        String reqLength = CommonValidator.validateLength(value, 4, 1);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD302_EVENT_TICKETQUANTITY);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD301_EVENT_TICKETQUANTITY);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateEventlocation(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 40, 40);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD303_EVENT_LOCATION);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateEventCountryCode(EntertainmentTicketingIndustryTAABean eIndustryTAABean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(eIndustryTAABean.getEventCountryCode())) {
      if (CommonValidator.validateData(eIndustryTAABean.getEventCountryCode(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['İ']))
      {
        String reqLength = CommonValidator.validateLength(
          eIndustryTAABean.getEventCountryCode(), 3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD308_EVENT_COUNTRYCODE);
          errorCodes.add(errorObj);
        }
        else if (CommonValidator.isNullOrEmpty(eIndustryTAABean.getEventRegionCode()))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD307_EVENT_REGIONCODE);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD309_EVENT_COUNTRYCODE);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateEventRegionCode(EntertainmentTicketingIndustryTAABean eIndustryTAABean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(eIndustryTAABean.getEventRegionCode())) {
      if (CommonValidator.validateData(eIndustryTAABean.getEventRegionCode(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ı']))
      {
        String reqLength = CommonValidator.validateLength(
          eIndustryTAABean.getEventRegionCode(), 3, 1);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD305_EVENT_REGIONCODE);
          errorCodes.add(errorObj);
        }
        else if (CommonValidator.isNullOrEmpty(eIndustryTAABean.getEventCountryCode()))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD310_EVENT_COUNTRYCODE);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD306_EVENT_REGIONCODE);
        errorCodes.add(errorObj);
      }
    }
  }
}
