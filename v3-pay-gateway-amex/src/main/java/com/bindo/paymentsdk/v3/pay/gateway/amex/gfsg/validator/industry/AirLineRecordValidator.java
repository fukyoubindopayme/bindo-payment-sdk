package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.industry;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.ErrorObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceAddendumBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceBasicBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.AirlineIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.connection.PropertyReader;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.SubmissionErrorCodes;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.SubmissionMethod;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.exceptions.SettlementException;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.CommonValidator;

import java.util.List;
import org.apache.log4j.Logger;

public class AirLineRecordValidator
{
  private static final Logger LOGGER = Logger.getLogger(AirLineRecordValidator.class);
  static boolean debugFlag = false;
  
  public static void validateAirLineTypeRec(TransactionAdviceBasicBean transactionAdviceBasicType, TransactionAdviceAddendumBean transactionAddendumType, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    System.out.println("=================Airline Arival Type Record=============");
    if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON"))
    {
      debugFlag = true;
      LOGGER.info("Entering into validateAirLineTypeRec");
    }
    boolean isPaperSubmitter = false;
    if (!CommonValidator.isNullOrEmpty(transactionAdviceBasicType.getSubmissionMethod())) {
      if (SubmissionMethod.Paper_external_vendor_paper_processor.getSubmissionMethod().equals(transactionAdviceBasicType.getSubmissionMethod())) {
        isPaperSubmitter = true;
      }
    }
    AirlineIndustryTAABean airlineIndustryTAABean = (AirlineIndustryTAABean)transactionAddendumType;
    
    TAADBRecordValidator.validateRecordType(airlineIndustryTAABean
      .getRecordType(), errorCodes);
    TAADBRecordValidator.validateRecordNumber(airlineIndustryTAABean
      .getRecordNumber(), errorCodes);
    TAADBRecordValidator.validateTransactionIdentifier(
      airlineIndustryTAABean.getTransactionIdentifier(), 
      transactionAdviceBasicType, errorCodes);
    TAADBRecordValidator.validateAddendaTypeCode(airlineIndustryTAABean
      .getAddendaTypeCode(), errorCodes);
    TAADBRecordValidator.validateFormatCode(airlineIndustryTAABean
      .getFormatCode(), transactionAdviceBasicType, errorCodes);
    
    validateTransactionType(airlineIndustryTAABean.getTransactionType(), 
      errorCodes);
    validateTicketNumber(airlineIndustryTAABean, errorCodes);
    validateDocumentType(airlineIndustryTAABean.getDocumentType(), isPaperSubmitter, 
      errorCodes);
    validateAirlineProcessIdentifier(airlineIndustryTAABean
      .getAirlineProcessIdentifier(), isPaperSubmitter, errorCodes);
    validateIataNumericCode(airlineIndustryTAABean.getIataNumericCode(), 
      isPaperSubmitter, errorCodes);
    validateTicketingCarrierName(airlineIndustryTAABean
      .getTicketingCarrierName(), isPaperSubmitter, errorCodes);
    validateTicketIssueCity(airlineIndustryTAABean.getTicketIssueCity(), 
      isPaperSubmitter, errorCodes);
    validateTicketIssueDate(airlineIndustryTAABean.getTicketIssueDate(), 
      isPaperSubmitter, errorCodes);
    validateNumberInParty(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
    validatePassengerName(airlineIndustryTAABean, errorCodes);
    validateConjunctionTicketIndicator(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
    validateElectronicTicketIndicator(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
    validateTotalNumberOfAirSegments(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
    validateOriginalTicketNumber(airlineIndustryTAABean, errorCodes);
    validateOriginalTransactionAmount(airlineIndustryTAABean, errorCodes);
    
    validateStopperIndicator1(airlineIndustryTAABean
      .getStopoverIndicator1(), errorCodes);
    validateDepartureLocationCodeSegment1(airlineIndustryTAABean, 
      isPaperSubmitter, errorCodes);
    validateDepartureDateSegment1(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
    validateArrivalLocationCodeSegment1(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
    validateSegmentCarrierCode1(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
    validateSegment1FareBasis(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
    validateClassOfServiceCodeSegment1(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
    validateFlightNumberSegment1(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
    validateSegment1Fare(airlineIndustryTAABean, errorCodes);
    if ((!CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getDepartureLocationCodeSegment2())) && (
      (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
      (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))))
    {
      validateStopperIndicator2(
        airlineIndustryTAABean.getStopoverIndicator2(), errorCodes);
      validateDepartureLocationCodeSegment2(airlineIndustryTAABean, 
        isPaperSubmitter, errorCodes);
      validateDepartureDateSegment2(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
      validateArrivalLocationCodeSegment2(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
      validateSegmentCarrierCode2(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
      validateSegment2FareBasis(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
      validateClassOfServiceCodeSegment2(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
      validateFlightNumberSegment2(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
      validateSegment2Fare(airlineIndustryTAABean, errorCodes);
    }
    if ((!CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getDepartureLocationCodeSegment3())) && (
      (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
      (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))))
    {
      validateStopperIndicator3(
        airlineIndustryTAABean.getStopoverIndicator3(), errorCodes);
      validateDepartureLocationCodeSegment3(airlineIndustryTAABean, 
        isPaperSubmitter, errorCodes);
      validateDepartureDateSegment3(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
      validateArrivalLocationCodeSegment3(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
      validateSegmentCarrierCode3(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
      validateSegment3FareBasis(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
      validateClassOfServiceCodeSegment3(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
      validateFlightNumberSegment3(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
      validateSegment3Fare(airlineIndustryTAABean, errorCodes);
    }
    if ((!CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getDepartureLocationCodeSegment4())) && (
      (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
      (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))))
    {
      validateStopperIndicator4(
        airlineIndustryTAABean.getStopoverIndicator4(), errorCodes);
      validateDepartureLocationCodeSegment4(airlineIndustryTAABean, 
        isPaperSubmitter, errorCodes);
      validateDepartureDateSegment4(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
      validateArrivalLocationCodeSegment4(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
      validateSegmentCarrierCode4(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
      validateSegment4FareBasis(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
      validateClassOfServiceCodeSegment4(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
      validateFlightNumberSegment4(airlineIndustryTAABean, isPaperSubmitter, errorCodes);
      validateSegment4Fare(airlineIndustryTAABean, errorCodes);
    }
    validateStopperIndicator5(
      airlineIndustryTAABean.getStopoverIndicator5(), errorCodes);
    validateOriginalCurrencyCode(airlineIndustryTAABean, errorCodes);
    if (debugFlag) {
      LOGGER.info("Exiting from validateAirLineTypeRec");
    }
  }
  
  private static void validateTransactionType(String transactionType, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(transactionType))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD213_TRANSACTION_TYPE);
      
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(transactionType, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
    {
      String reqLength = CommonValidator.validateLength(transactionType, 3, 3);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD213_TRANSACTION_TYPE);
        
        errorCodes.add(errorObj);
      }
      else if (!transactionType.equalsIgnoreCase("TKT"))
      {
        if (!transactionType.equalsIgnoreCase("REF")) {
          if (!transactionType.equalsIgnoreCase("EXC")) {
            if (!transactionType.equalsIgnoreCase("MSC"))
            {
              errorObj = new ErrorObject(
                SubmissionErrorCodes.ERROR_DATAFIELD213_TRANSACTION_TYPE);
              
              errorCodes.add(errorObj);
            }
          }
        }
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD213_TRANSACTION_TYPE);
      
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateTicketNumber(AirlineIndustryTAABean airlineIndustryTAABean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if ((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
    
      (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) {
      if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getTicketNumber()))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD140_TICKET_NUMBER);
        
        errorCodes.add(errorObj);
        
        return;
      }
    }
    if (!CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getTicketNumber()))
    {
      String reqLength = CommonValidator.validateLength(
        airlineIndustryTAABean.getTicketNumber(), 14, 14);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD140_TICKET_NUMBER);
        
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateDocumentType(String documentType, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(documentType))
    {
      if (!isPaperSubmitter)
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD141_DOCUMENT_TYPE);
        
        errorCodes.add(errorObj);
      }
    }
    else if (CommonValidator.validateData(documentType, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
    {
      String reqLength = CommonValidator.validateLength(documentType, 2, 2);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD141_DOCUMENT_TYPE);
        
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD141_DOCUMENT_TYPE);
      
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateAirlineProcessIdentifier(String airlineProcessIdentifier, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(airlineProcessIdentifier))
    {
      if (!isPaperSubmitter)
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD142_AIRLINEPROCESS_IDENTIFIER);
        
        errorCodes.add(errorObj);
      }
    }
    else if (CommonValidator.validateData(airlineProcessIdentifier, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
    {
      String reqLength = CommonValidator.validateLength(airlineProcessIdentifier, 3, 3);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD142_AIRLINEPROCESS_IDENTIFIER);
        
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD142_AIRLINEPROCESS_IDENTIFIER);
      
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateIataNumericCode(String iataNumericCode, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(iataNumericCode))
    {
      if (!isPaperSubmitter)
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD143_IATANUMERIC_CODE);
        
        errorCodes.add(errorObj);
      }
    }
    else if (CommonValidator.validateData(iataNumericCode, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
    {
      String reqLength = CommonValidator.validateLength(iataNumericCode, 8, 8);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD143_IATANUMERIC_CODE);
        
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD143_IATANUMERIC_CODE);
      
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateTicketingCarrierName(String ticketingCarrierName, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(ticketingCarrierName))
    {
      if (!isPaperSubmitter)
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD144_TICKETING_CARRIERNAME);
        
        errorCodes.add(errorObj);
      }
    }
    else
    {
      String reqLength = 
        CommonValidator.validateLength(ticketingCarrierName, 25, 25);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD144_TICKETING_CARRIERNAME);
        
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateTicketIssueCity(String ticketIssueCity, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(ticketIssueCity))
    {
      if (!isPaperSubmitter)
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD145_TICKETISSUE_CITY);
        
        errorCodes.add(errorObj);
      }
    }
    else
    {
      String reqLength = 
        CommonValidator.validateLength(ticketIssueCity, 18, 18);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD145_TICKETISSUE_CITY);
        
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateTicketIssueDate(String ticketIssueDate, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(ticketIssueDate))
    {
      if (!isPaperSubmitter)
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD146_TICKETISSUE_DATE);
        
        errorCodes.add(errorObj);
      }
    }
    else if (CommonValidator.validateData(ticketIssueDate, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
    {
      String reqLength = CommonValidator.validateLength(ticketIssueDate, 8, 8);
      if ((!reqLength.equals("equal")) || 
      
        (!CommonValidator.isValidDate(ticketIssueDate, "CCYYMMDD")))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD146_TICKETISSUE_DATE);
        
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD146_TICKETISSUE_DATE);
      
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateNumberInParty(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getNumberInParty()))
    {
      if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
      
        (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD147_NUMBERIN_PARTY);
        
        errorCodes.add(errorObj);
      }
    }
    else if (CommonValidator.validateData(airlineIndustryTAABean.getNumberInParty(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
    {
      String reqLength = CommonValidator.validateLength(
        airlineIndustryTAABean.getNumberInParty(), 3, 3);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD147_NUMBERIN_PARTY);
        
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD147_NUMBERIN_PARTY);
      
      errorCodes.add(errorObj);
    }
  }
  
  private static void validatePassengerName(AirlineIndustryTAABean airlineIndustryTAABean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if ((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
    
      (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) {
      if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getPassengerName()))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD148_PASSENGER_NAME);
        
        errorCodes.add(errorObj);
        
        return;
      }
    }
    if (!CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getPassengerName()))
    {
      String reqLength = CommonValidator.validateLength(
        airlineIndustryTAABean.getPassengerName(), 25, 25);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD148_PASSENGER_NAME);
        
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateConjunctionTicketIndicator(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getConjunctionTicketIndicator()))
    {
      if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
      
        (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD149_CONJUNCTIONTICKET_INDICATOR);
        
        errorCodes.add(errorObj);
      }
    }
    else if (CommonValidator.validateData(airlineIndustryTAABean.getConjunctionTicketIndicator(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
    {
      String reqLength = CommonValidator.validateLength(
        airlineIndustryTAABean.getConjunctionTicketIndicator(), 
        1, 1);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD149_CONJUNCTIONTICKET_INDICATOR);
        
        errorCodes.add(errorObj);
      }
      else if (!airlineIndustryTAABean.getConjunctionTicketIndicator().equalsIgnoreCase("Y"))
      {
        if (!airlineIndustryTAABean.getConjunctionTicketIndicator().equalsIgnoreCase("N"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD149_CONJUNCTIONTICKET_INDICATOR);
          
          errorCodes.add(errorObj);
        }
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD149_CONJUNCTIONTICKET_INDICATOR);
      
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateOriginalTransactionAmount(AirlineIndustryTAABean airIndustryTAABean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(airIndustryTAABean.getOriginalCurrencyCode())) {
      if (CommonValidator.isNullOrEmpty(airIndustryTAABean.getOriginalTransactionAmount()))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD154_ORIGINALTRANSACTION_AMOUNT);
        
        errorCodes.add(errorObj);
        return;
      }
    }
    if (!CommonValidator.isNullOrEmpty(airIndustryTAABean.getOriginalTransactionAmount())) {
      if (CommonValidator.validateData(airIndustryTAABean.getOriginalTransactionAmount(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
      {
        String reqLength = CommonValidator.validateLength(
          airIndustryTAABean.getOriginalTransactionAmount(), 12, 
          12);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD153_ORIGINALTRANSACTION_AMOUNT);
          
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD152_ORIGINALTRANSACTION_AMOUNT);
        
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateOriginalCurrencyCode(AirlineIndustryTAABean airIndustryTAABean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(airIndustryTAABean.getOriginalTransactionAmount())) {
      if (CommonValidator.isNullOrEmpty(airIndustryTAABean.getOriginalCurrencyCode()))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD156_ORIGINALCURRENCY_CODE);
        
        errorCodes.add(errorObj);
        return;
      }
    }
    if (!CommonValidator.isNullOrEmpty(airIndustryTAABean.getOriginalCurrencyCode())) {
      if (CommonValidator.validateData(airIndustryTAABean.getOriginalCurrencyCode(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
      {
        String reqLength = CommonValidator.validateLength(
          airIndustryTAABean.getOriginalCurrencyCode(), 3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD155_ORIGINALCURRENCY_CODE);
          
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD155_ORIGINALCURRENCY_CODE);
        
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateElectronicTicketIndicator(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getElectronicTicketIndicator()))
    {
      if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
      
        (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD150_ELECTRONIC_TICKETINDICATOR);
        
        errorCodes.add(errorObj);
      }
    }
    else if (CommonValidator.validateData(airlineIndustryTAABean.getElectronicTicketIndicator(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
    {
      String reqLength = CommonValidator.validateLength(
        airlineIndustryTAABean.getElectronicTicketIndicator(), 
        1, 1);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD150_ELECTRONIC_TICKETINDICATOR);
        
        errorCodes.add(errorObj);
      }
      else if (!airlineIndustryTAABean.getElectronicTicketIndicator().equalsIgnoreCase("Y"))
      {
        if (!airlineIndustryTAABean.getElectronicTicketIndicator().equalsIgnoreCase("N"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD150_ELECTRONIC_TICKETINDICATOR);
          
          errorCodes.add(errorObj);
        }
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD150_ELECTRONIC_TICKETINDICATOR);
      
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateTotalNumberOfAirSegments(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getTotalNumberOfAirSegments()))
    {
      if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
      
        (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD151_TOTAL_NUMBER_OF_AIR_SEGMENTS);
        
        errorCodes.add(errorObj);
      }
    }
    else if (CommonValidator.validateData(airlineIndustryTAABean.getTotalNumberOfAirSegments(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
    {
      String reqLength = CommonValidator.validateLength(
        airlineIndustryTAABean.getTotalNumberOfAirSegments(), 
        1, 1);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD151_TOTAL_NUMBER_OF_AIR_SEGMENTS);
        
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD151_TOTAL_NUMBER_OF_AIR_SEGMENTS);
      
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateStopperIndicator1(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
      {
        String reqLength = CommonValidator.validateLength(value, 1, 1);
        if ((reqLength.equals("greaterThanMax")) || 
          (!CommonValidator.isValidStopOverIndicator(value)))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD157_STOPOVER1_INDICATOR);
          
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD157_STOPOVER1_INDICATOR);
        
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateDepartureLocationCodeSegment1(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getDepartureLocationCodeSegment1()))
    {
      if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
      
        (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD158_DEPARTURELOCATION_CODE_SEGMENT1);
        
        errorCodes.add(errorObj);
      }
    }
    else if (CommonValidator.validateData(airlineIndustryTAABean.getDepartureLocationCodeSegment1(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
    {
      String reqLength = CommonValidator.validateLength(
        airlineIndustryTAABean
        .getDepartureLocationCodeSegment1(), 3, 3);
      if (reqLength.equals("greaterThanMax")) {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD159_DEPARTURELOCATION_CODE_SEGMENT1);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD159_DEPARTURELOCATION_CODE_SEGMENT1);
      
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateDepartureDateSegment1(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getDepartureDateSegment1()))
    {
      if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
      
        (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD160_DEPARTURE_DATE_SEGMENT1);
        
        errorCodes.add(errorObj);
      }
    }
    else if (CommonValidator.validateData(airlineIndustryTAABean.getDepartureDateSegment1(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
    {
      String reqLength = 
        CommonValidator.validateLength(airlineIndustryTAABean
        .getDepartureDateSegment1(), 8, 8);
      if ((!reqLength.equals("equal")) || 
        (!CommonValidator.isValidDate(airlineIndustryTAABean.getDepartureDateSegment1(), "CCYYMMDD")))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD161_DEPARTURE_DATE_SEGMENT1);
        
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD161_DEPARTURE_DATE_SEGMENT1);
      
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateArrivalLocationCodeSegment1(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getArrivalLocationCodeSegment1()))
    {
      if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
      
        (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD162_ARRIVALLOCATION_CODESEGMENT1);
        
        errorCodes.add(errorObj);
      }
    }
    else if (CommonValidator.validateData(airlineIndustryTAABean.getArrivalLocationCodeSegment1(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
    {
      String reqLength = 
        CommonValidator.validateLength(airlineIndustryTAABean
        .getArrivalLocationCodeSegment1(), 3, 3);
      if (!reqLength.equals("equal"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD163_ARRIVALLOCATION_CODESEGMENT1);
        
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD163_ARRIVALLOCATION_CODESEGMENT1);
      
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateSegmentCarrierCode1(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getSegmentCarrierCode1()))
    {
      if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
      
        (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD164_SEGMENTCARRIER_CODE1);
        
        errorCodes.add(errorObj);
      }
    }
    else if (CommonValidator.validateData(airlineIndustryTAABean.getSegmentCarrierCode1(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
    {
      String reqLength = CommonValidator.validateLength(
        airlineIndustryTAABean.getSegmentCarrierCode1(), 2, 2);
      if (!reqLength.equals("equal"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD165_SEGMENTCARRIER_CODE1);
        
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD165_SEGMENTCARRIER_CODE1);
      
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateSegment1FareBasis(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getSegment1FareBasis()))
    {
      if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
      
        (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD166_SEGMENT1FARE_BASIS);
        
        errorCodes.add(errorObj);
      }
    }
    else
    {
      String reqLength = CommonValidator.validateLength(
        airlineIndustryTAABean.getSegment1FareBasis(), 15, 15);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject("AIR", 
          "Segment1FareBasis length is out of Sequence");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateClassOfServiceCodeSegment1(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getClassOfServiceCodeSegment1()))
    {
      if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
      
        (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD167_CLASSOFSERVICE_CODESEGMENT1);
        
        errorCodes.add(errorObj);
      }
    }
    else if (CommonValidator.validateData(airlineIndustryTAABean.getClassOfServiceCodeSegment1(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
    {
      String reqLength = CommonValidator.validateLength(
        airlineIndustryTAABean.getClassOfServiceCodeSegment1(), 
        2, 2);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD167_CLASSOFSERVICE_CODESEGMENT1);
        
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD167_CLASSOFSERVICE_CODESEGMENT1);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateFlightNumberSegment1(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getFlightNumberSegment1()))
    {
      if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
      
        (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD168_FLIGHTNUMBER_SEGMENT1);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      String reqLength = CommonValidator.validateLength(
        airlineIndustryTAABean.getFlightNumberSegment1(), 4, 4);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject("AIR", 
          "FlightNumberSegment1 Length is Out of Sequence");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateSegment1Fare(AirlineIndustryTAABean airlineIndustryTAABean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getSegment1Fare())) {
      if (CommonValidator.validateData(airlineIndustryTAABean.getSegment1Fare(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
      {
        String reqLength = CommonValidator.validateLength(
          airlineIndustryTAABean.getSegment1Fare(), 12, 12);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD170_SEGMENT_1FARE);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD169_SEGMENT_1FARE);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateStopperIndicator2(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
      {
        String reqLength = CommonValidator.validateLength(value, 1, 1);
        if ((reqLength.equals("greaterThanMax")) || 
          (!CommonValidator.isValidStopOverIndicator(value)))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD171_STOPOVER2_INDICATOR);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD171_STOPOVER2_INDICATOR);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateDepartureLocationCodeSegment2(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (isPaperSubmitter)
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD173_DEPARTURELOCATION_CODE_SEGMENT2);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(airlineIndustryTAABean.getDepartureLocationCodeSegment2(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['']))
    {
      String reqLength = CommonValidator.validateLength(
        airlineIndustryTAABean
        .getDepartureLocationCodeSegment2(), 3, 3);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD173_DEPARTURELOCATION_CODE_SEGMENT2);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD173_DEPARTURELOCATION_CODE_SEGMENT2);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateDepartureDateSegment2(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getDepartureDateSegment2()))
    {
      if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
      
        (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD174_DEPARTURE_DATE_SEGMENT2);
        errorCodes.add(errorObj);
      }
    }
    else if (CommonValidator.validateData(airlineIndustryTAABean.getDepartureDateSegment2(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[' ']))
    {
      String reqLength = 
        CommonValidator.validateLength(airlineIndustryTAABean
        .getDepartureDateSegment2(), 8, 8);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD175_DEPARTURE_DATE_SEGMENT2);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD175_DEPARTURE_DATE_SEGMENT2);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateArrivalLocationCodeSegment2(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getArrivalLocationCodeSegment2()))
    {
      if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
      
        (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD176_ARRIVALLOCATION_CODESEGMENT2);
        errorCodes.add(errorObj);
      }
    }
    else if (CommonValidator.validateData(airlineIndustryTAABean.getArrivalLocationCodeSegment2(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['¡']))
    {
      String reqLength = 
        CommonValidator.validateLength(airlineIndustryTAABean
        .getArrivalLocationCodeSegment2(), 3, 3);
      if (!reqLength.equals("equal"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD177_ARRIVALLOCATION_CODESEGMENT2);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD177_ARRIVALLOCATION_CODESEGMENT2);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateSegmentCarrierCode2(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getSegmentCarrierCode2()))
    {
      if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
      
        (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD178_SEGMENTCARRIER_CODE2);
        errorCodes.add(errorObj);
      }
    }
    else if (CommonValidator.validateData(airlineIndustryTAABean.getSegmentCarrierCode2(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['¢']))
    {
      String reqLength = CommonValidator.validateLength(
        airlineIndustryTAABean.getSegmentCarrierCode2(), 2, 2);
      if (!reqLength.equals("equal"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD179_SEGMENTCARRIER_CODE2);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD179_SEGMENTCARRIER_CODE2);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateSegment2FareBasis(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getSegment2FareBasis()))
    {
      if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
      
        (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD180_SEGMENT2FARE_BASIS);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      String reqLength = CommonValidator.validateLength(
        airlineIndustryTAABean.getSegment2FareBasis(), 15, 15);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject("AIR", 
          "Segment2FareBasis length is out of Sequence");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateClassOfServiceCodeSegment2(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getClassOfServiceCodeSegment2()))
    {
      if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
      
        (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter))
      {
        errorObj = new ErrorObject("AIR", 
          "ClassOfServiceCodeSegment2 is Missing");
        errorCodes.add(errorObj);
      }
    }
    else if (CommonValidator.validateData(airlineIndustryTAABean.getClassOfServiceCodeSegment2(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['¤']))
    {
      String reqLength = CommonValidator.validateLength(
        airlineIndustryTAABean.getClassOfServiceCodeSegment2(), 
        2, 2);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD181_CLASSOFSERVICE_CODESEGMENT2);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD181_CLASSOFSERVICE_CODESEGMENT2);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateFlightNumberSegment2(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getFlightNumberSegment2()))
    {
      if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
      
        (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD182_FLIGHTNUMBER_SEGMENT2);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      String reqLength = CommonValidator.validateLength(
        airlineIndustryTAABean.getFlightNumberSegment2(), 4, 4);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject("AIR", 
          "FlightNumberSegment2 Length is Out of Sequence");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateSegment2Fare(AirlineIndustryTAABean airlineIndustryTAABean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getSegment2Fare())) {
      if (CommonValidator.validateData(airlineIndustryTAABean.getSegment2Fare(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['§']))
      {
        String reqLength = CommonValidator.validateLength(
          airlineIndustryTAABean.getSegment2Fare(), 12, 12);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD184_SEGMENT_2FARE);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD183_SEGMENT_2FARE);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateStopperIndicator3(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['¨']))
      {
        String reqLength = CommonValidator.validateLength(value, 1, 1);
        if ((reqLength.equals("greaterThanMax")) || 
          (!CommonValidator.isValidStopOverIndicator(value)))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD185_STOPOVER3_INDICATOR);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD185_STOPOVER3_INDICATOR);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateDepartureLocationCodeSegment3(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (isPaperSubmitter)
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD187_DEPARTURELOCATION_CODE_SEGMENT3);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(airlineIndustryTAABean.getDepartureLocationCodeSegment3(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['©']))
    {
      String reqLength = CommonValidator.validateLength(
        airlineIndustryTAABean
        .getDepartureLocationCodeSegment3(), 3, 3);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD187_DEPARTURELOCATION_CODE_SEGMENT3);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD187_DEPARTURELOCATION_CODE_SEGMENT3);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateDepartureDateSegment3(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getDepartureDateSegment3()))
    {
      if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
      
        (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD188_DEPARTURE_DATE_SEGMENT3);
        errorCodes.add(errorObj);
      }
    }
    else if (CommonValidator.validateData(airlineIndustryTAABean.getDepartureDateSegment3(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ª']))
    {
      String reqLength = 
        CommonValidator.validateLength(airlineIndustryTAABean
        .getDepartureDateSegment3(), 8, 8);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD189_DEPARTURE_DATE_SEGMENT3);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD189_DEPARTURE_DATE_SEGMENT3);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateArrivalLocationCodeSegment3(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getArrivalLocationCodeSegment3()))
    {
      if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
      
        (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD190_ARRIVALLOCATION_CODESEGMENT3);
        errorCodes.add(errorObj);
      }
    }
    else if (CommonValidator.validateData(airlineIndustryTAABean.getArrivalLocationCodeSegment3(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['«']))
    {
      String reqLength = 
        CommonValidator.validateLength(airlineIndustryTAABean
        .getArrivalLocationCodeSegment3(), 3, 3);
      if (!reqLength.equals("equal"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD191_ARRIVALLOCATION_CODESEGMENT3);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD191_ARRIVALLOCATION_CODESEGMENT3);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateSegmentCarrierCode3(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getSegmentCarrierCode3()))
    {
      if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
      
        (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD192_SEGMENTCARRIER_CODE3);
        errorCodes.add(errorObj);
      }
    }
    else if (CommonValidator.validateData(airlineIndustryTAABean.getSegmentCarrierCode3(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['¬']))
    {
      String reqLength = CommonValidator.validateLength(
        airlineIndustryTAABean.getSegmentCarrierCode3(), 2, 2);
      if (!reqLength.equals("equal"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD193_SEGMENTCARRIER_CODE3);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD193_SEGMENTCARRIER_CODE3);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateSegment3FareBasis(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getSegment3FareBasis()))
    {
      if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
      
        (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD194_SEGMENT3FARE_BASIS);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      String reqLength = CommonValidator.validateLength(
        airlineIndustryTAABean.getSegment3FareBasis(), 15, 15);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject("AIR", 
          "Segment3FareBasis length is out of Sequence");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateClassOfServiceCodeSegment3(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getClassOfServiceCodeSegment3()))
    {
      if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
      
        (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter))
      {
        errorObj = new ErrorObject("AIR", 
          "ClassOfServiceCodeSegment3 is Missing");
        errorCodes.add(errorObj);
      }
    }
    else if (CommonValidator.validateData(airlineIndustryTAABean.getClassOfServiceCodeSegment3(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['®']))
    {
      String reqLength = CommonValidator.validateLength(
        airlineIndustryTAABean.getClassOfServiceCodeSegment3(), 
        2, 2);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD195_CLASSOFSERVICE_CODESEGMENT3);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD195_CLASSOFSERVICE_CODESEGMENT3);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateFlightNumberSegment3(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getFlightNumberSegment3()))
    {
      if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
      
        (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD196_FLIGHTNUMBER_SEGMENT3);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      String reqLength = CommonValidator.validateLength(
        airlineIndustryTAABean.getFlightNumberSegment3(), 4, 4);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject("AIR", 
          "FlightNumberSegment3 Length is Out of Sequence");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateSegment3Fare(AirlineIndustryTAABean airlineIndustryTAABean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getSegment3Fare())) {
      if (CommonValidator.validateData(airlineIndustryTAABean.getSegment3Fare(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['±']))
      {
        String reqLength = CommonValidator.validateLength(
          airlineIndustryTAABean.getSegment3Fare(), 12, 12);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD198_SEGMENT_3FARE);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD197_SEGMENT_3FARE);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateStopperIndicator4(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['²']))
      {
        String reqLength = CommonValidator.validateLength(value, 1, 1);
        if ((reqLength.equals("greaterThanMax")) || 
          (!CommonValidator.isValidStopOverIndicator(value)))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD199_STOPOVER4_INDICATOR);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD199_STOPOVER4_INDICATOR);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateDepartureLocationCodeSegment4(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (isPaperSubmitter)
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD201_DEPARTURELOCATION_CODE_SEGMENT4);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(airlineIndustryTAABean.getDepartureLocationCodeSegment4(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['³']))
    {
      String reqLength = CommonValidator.validateLength(
        airlineIndustryTAABean
        .getDepartureLocationCodeSegment4(), 3, 3);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD201_DEPARTURELOCATION_CODE_SEGMENT4);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD201_DEPARTURELOCATION_CODE_SEGMENT4);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateDepartureDateSegment4(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getDepartureDateSegment4()))
    {
      if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
      
        (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD202_DEPARTURE_DATE_SEGMENT4);
        errorCodes.add(errorObj);
      }
    }
    else if (CommonValidator.validateData(airlineIndustryTAABean.getDepartureDateSegment4(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['´']))
    {
      String reqLength = 
        CommonValidator.validateLength(airlineIndustryTAABean
        .getDepartureDateSegment4(), 8, 8);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD203_DEPARTURE_DATE_SEGMENT4);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD203_DEPARTURE_DATE_SEGMENT4);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateArrivalLocationCodeSegment4(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getArrivalLocationCodeSegment4()))
    {
      if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
      
        (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD204_ARRIVALLOCATION_CODESEGMENT4);
        errorCodes.add(errorObj);
      }
    }
    else if (CommonValidator.validateData(airlineIndustryTAABean.getArrivalLocationCodeSegment4(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['µ']))
    {
      String reqLength = 
        CommonValidator.validateLength(airlineIndustryTAABean
        .getArrivalLocationCodeSegment4(), 3, 3);
      if (!reqLength.equals("equal"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD205_ARRIVALLOCATION_CODESEGMENT4);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD205_ARRIVALLOCATION_CODESEGMENT4);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateSegmentCarrierCode4(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getSegmentCarrierCode4()))
    {
      if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
      
        (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD206_SEGMENTCARRIER_CODE4);
        errorCodes.add(errorObj);
      }
    }
    else if (CommonValidator.validateData(airlineIndustryTAABean.getSegmentCarrierCode4(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['¶']))
    {
      String reqLength = CommonValidator.validateLength(
        airlineIndustryTAABean.getSegmentCarrierCode4(), 2, 2);
      if (!reqLength.equals("equal"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD207_SEGMENTCARRIER_CODE4);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD207_SEGMENTCARRIER_CODE4);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateSegment4FareBasis(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getSegment4FareBasis()))
    {
      if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
      
        (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD208_SEGMENT4FARE_BASIS);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      String reqLength = CommonValidator.validateLength(
        airlineIndustryTAABean.getSegment4FareBasis(), 15, 15);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject("AIR", 
          "Segment4FareBasis length is out of Sequence");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateClassOfServiceCodeSegment4(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getClassOfServiceCodeSegment4()))
    {
      if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
      
        (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter))
      {
        errorObj = new ErrorObject("AIR", 
          "ClassOfServiceCodeSegment4 is Missing");
        errorCodes.add(errorObj);
      }
    }
    else if (CommonValidator.validateData(airlineIndustryTAABean.getClassOfServiceCodeSegment4(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['¸']))
    {
      String reqLength = CommonValidator.validateLength(
        airlineIndustryTAABean.getClassOfServiceCodeSegment4(), 
        2, 2);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD209_CLASSOFSERVICE_CODESEGMENT4);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD209_CLASSOFSERVICE_CODESEGMENT4);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateFlightNumberSegment4(AirlineIndustryTAABean airlineIndustryTAABean, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getFlightNumberSegment4()))
    {
      if (((airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("TKT")) || 
      
        (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC"))) && (!isPaperSubmitter))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD210_FLIGHTNUMBER_SEGMENT4);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      String reqLength = CommonValidator.validateLength(
        airlineIndustryTAABean.getFlightNumberSegment4(), 4, 4);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject("AIR", 
          "FlightNumberSegment4 Length is Out of Sequence");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateSegment4Fare(AirlineIndustryTAABean airlineIndustryTAABean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getSegment4Fare())) {
      if (CommonValidator.validateData(airlineIndustryTAABean.getSegment4Fare(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['»']))
      {
        String reqLength = CommonValidator.validateLength(
          airlineIndustryTAABean.getSegment4Fare(), 12, 12);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD212_SEGMENT_4FARE);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD211_SEGMENT_4FARE);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateStopperIndicator5(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['²']))
      {
        String reqLength = CommonValidator.validateLength(value, 1, 1);
        if ((reqLength.equals("greaterThanMax")) || 
          (!CommonValidator.isValidStopOverIndicator(value)))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD199_STOPOVER5_INDICATOR);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD199_STOPOVER5_INDICATOR);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateOriginalTicketNumber(AirlineIndustryTAABean airlineIndustryTAABean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (airlineIndustryTAABean.getTransactionType().equalsIgnoreCase("EXC")) {
      if (CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getExchangedOrOriginalTicketNumber()))
      {
        errorObj = new ErrorObject("AIR", "OriginalTicketNumber is Missing");
        errorCodes.add(errorObj);
        
        return;
      }
    }
    if (!CommonValidator.isNullOrEmpty(airlineIndustryTAABean.getExchangedOrOriginalTicketNumber()))
    {
      String reqLength = CommonValidator.validateLength(
        airlineIndustryTAABean
        .getExchangedOrOriginalTicketNumber(), 14, 14);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject("AIR", 
          "OriginalTicketNumber Length exceeded");
        errorCodes.add(errorObj);
      }
    }
  }
}
