package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations;

public enum ProcessingCode {
    CREDIT("200000"),
    DEBIT("000000"),
    CREDIT_REVERSALS("220808"),
    DEBIT_REVERSALS("020000"),
    BONUS_PAY_DEBIT("000090"),
    BONUS_PAY_CREDIT("200090"),
    BONUS_PAY_DEBIT_REVERSAL("020809"),
    BONUS_PAY_CREDIT_REVERSAL("220809");

    private final String processingCode;

    private ProcessingCode(String processingCode) {
        this.processingCode = processingCode;
    }

    public String getProcessingCode() {
        return this.processingCode;
    }
}
