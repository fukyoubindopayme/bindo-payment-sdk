package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean;

import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.connection.PropertyReader;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.Constants;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.AuthErrorCodes;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.utils.ISOUtil;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.validator.Validator;
import java.util.List;

public class OriginalDataElementsBean
{
  private String messageTypeIdOriginal;
  private String systemTraceAuditNumberOriginal;
  private String AcquiringInstIdOriginal;
  private String localTransactionDateAndTimeOriginal;

  public String getMessageTypeIdOriginal()
  {
    return this.messageTypeIdOriginal;
  }
  
  public void setMessageTypeIdOriginal(String messageTypeIdOriginal)
  {
    this.messageTypeIdOriginal = messageTypeIdOriginal;
  }
  
  public String getSystemTraceAuditNumberOriginal()
  {
    return this.systemTraceAuditNumberOriginal;
  }
  
  public void setSystemTraceAuditNumberOriginal(String systemTraceAuditNumberOriginal)
  {
    this.systemTraceAuditNumberOriginal = systemTraceAuditNumberOriginal;
  }
  
  public String getAcquiringInstIdOriginal()
  {
    return this.AcquiringInstIdOriginal;
  }
  
  public void setAcquiringInstIdOriginal(String acquiringInstIdOriginal)
  {
    this.AcquiringInstIdOriginal = acquiringInstIdOriginal;
  }
  
  public String getLocalTransactionDateAndTimeOriginal()
  {
    return this.localTransactionDateAndTimeOriginal;
  }
  
  public void setLocalTransactionDateAndTimeOriginal(String localTransactionDateAndTimeOriginal)
  {
    this.localTransactionDateAndTimeOriginal = localTransactionDateAndTimeOriginal;
  }
  
  public String populateOriginalDataElement(AuthorizationResponseBean authorizationResponseBean)
  {
    StringBuffer originalDataElement = new StringBuffer();
    
    StringBuffer displayBuffer = new StringBuffer();
    if (ISOUtil.IsNullOrEmpty(getMessageTypeIdOriginal()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD561_ORIGINALDATAELEMENTSDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if (Validator.validateSubFields(getMessageTypeIdOriginal(), 4, 4, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[0]))
    {
      originalDataElement.append(getMessageTypeIdOriginal());
      displayBuffer.append("MSG_TYPE_ID_ORIGINAL         ").append(" :: ").append(getMessageTypeIdOriginal()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD561_ORIGINALDATAELEMENTSDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getSystemTraceAuditNumberOriginal()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD562_ORIGINALDATAELEMENTSDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if (Validator.validateSubFields(getSystemTraceAuditNumberOriginal(), 6, 6, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2]))
    {
      originalDataElement.append(getSystemTraceAuditNumberOriginal().toUpperCase());
      displayBuffer.append("SYS_TRACE_AUDIT_NUM_ORIGINAL  ").append(" :: ").append(getSystemTraceAuditNumberOriginal().toUpperCase()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD562_ORIGINALDATAELEMENTSDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getLocalTransactionDateAndTimeOriginal()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD563_ORIGINALDATAELEMENTSDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if (Validator.validateSubFields(getLocalTransactionDateAndTimeOriginal(), 12, 12, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[0]))
    {
      originalDataElement.append(getLocalTransactionDateAndTimeOriginal());
      displayBuffer.append("LOCAL_TSCTN_DATE_TIME_ORIGINAL").append(" :: ").append(getLocalTransactionDateAndTimeOriginal()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD563_ORIGINALDATAELEMENTSDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getAcquiringInstIdOriginal()))
    {
      originalDataElement.append("\\");
    }
    else if (Validator.validateSubFields(getAcquiringInstIdOriginal(), 13, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2]))
    {
      originalDataElement.append(getAcquiringInstIdOriginal());
      displayBuffer.append("ACQ_INST_ID_ORIGINAL          ").append(" :: ").append(getAcquiringInstIdOriginal()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD564_ORIGINALDATAELEMENTSDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    boolean debugflag = PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON");
    if (debugflag)
    {
      Log.i("Original Data Element Bean\r\n----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n-                                 Original Data Element Bean                                                                                                                                       -\r\n---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n" +
      
        displayBuffer.toString() + 
        "\r\n" + "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
      
      Log.i("Populated Original Data Element is -- " + originalDataElement);
    }
    return originalDataElement.toString();
  }
  
  public String toString()
  {
    StringBuffer buffer = new StringBuffer();
    
    buffer.append("MSG_TYPE_ID_ORIGINAL").append(" :: ").append(getMessageTypeIdOriginal()).append(Constants.NEW_LINE_CHAR);
    buffer.append("SYS_TRACE_AUDIT_NUM_ORIGINAL").append(" :: ").append(getSystemTraceAuditNumberOriginal().toUpperCase()).append(Constants.NEW_LINE_CHAR);
    buffer.append("ACQ_INST_ID_ORIGINAL").append(" :: ").append(getAcquiringInstIdOriginal()).append(Constants.NEW_LINE_CHAR);
    buffer.append("LOCAL_TNSCTN_DATE_TIME_ORIGINAL").append(" :: ").append(getLocalTransactionDateAndTimeOriginal()).append(Constants.NEW_LINE_CHAR);
    
    return buffer.toString();
  }
}
