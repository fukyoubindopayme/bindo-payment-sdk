package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.industry;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.ErrorObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceAddendumBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceBasicBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.RetailIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.SubmissionErrorCodes;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.exceptions.SettlementException;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.CommonValidator;
import java.util.List;

public class RetailRecordValidator
{
  public static void validateRetailIndRecord(TransactionAdviceBasicBean transactionAdviceBasicType, TransactionAdviceAddendumBean transactionAddendumType, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    RetailIndustryTAABean retailIndustryTAABean = (RetailIndustryTAABean)transactionAddendumType;
    
    TAADBRecordValidator.validateRecordType(retailIndustryTAABean
      .getRecordType(), errorCodes);
    TAADBRecordValidator.validateRecordNumber(retailIndustryTAABean
      .getRecordNumber(), errorCodes);
    TAADBRecordValidator.validateTransactionIdentifier(
      retailIndustryTAABean.getTransactionIdentifier(), 
      transactionAdviceBasicType, errorCodes);
    TAADBRecordValidator.validateAddendaTypeCode(retailIndustryTAABean
      .getAddendaTypeCode(), errorCodes);
    TAADBRecordValidator.validateFormatCode(retailIndustryTAABean
      .getFormatCode(), transactionAdviceBasicType, errorCodes);
    
    validateRetailDepartmentName(retailIndustryTAABean
      .getRetailDepartmentName(), errorCodes);
    validateRetailItemDescription1(retailIndustryTAABean
      .getRetailItemDescription1(), errorCodes);
    validateRetailItemQuantity1(retailIndustryTAABean
      .getRetailItemQuantity1(), errorCodes);
    validateRetailRetailItemAmount1(retailIndustryTAABean
      .getRetailItemAmount1(), errorCodes);
    
    validateRetailItemDescription2(retailIndustryTAABean
      .getRetailItemDescription2(), errorCodes);
    validateRetailItemQuantity2(retailIndustryTAABean
      .getRetailItemQuantity2(), errorCodes);
    validateRetailRetailItemAmount2(retailIndustryTAABean
      .getRetailItemAmount2(), errorCodes);
    
    validateRetailItemDescription3(retailIndustryTAABean
      .getRetailItemDescription3(), errorCodes);
    validateRetailItemQuantity3(retailIndustryTAABean
      .getRetailItemQuantity3(), errorCodes);
    validateRetailRetailItemAmount3(retailIndustryTAABean
      .getRetailItemAmount3(), errorCodes);
    
    validateRetailItemDescription4(retailIndustryTAABean
      .getRetailItemDescription4(), errorCodes);
    validateRetailItemQuantity4(retailIndustryTAABean
      .getRetailItemQuantity4(), errorCodes);
    validateRetailRetailItemAmount4(retailIndustryTAABean
      .getRetailItemAmount4(), errorCodes);
    
    validateRetailItemDescription5(retailIndustryTAABean
      .getRetailItemDescription5(), errorCodes);
    validateRetailItemQuantity5(retailIndustryTAABean
      .getRetailItemQuantity5(), errorCodes);
    validateRetailRetailItemAmount5(retailIndustryTAABean
      .getRetailItemAmount5(), errorCodes);
    
    validateRetailItemDescription6(retailIndustryTAABean
      .getRetailItemDescription6(), errorCodes);
    validateRetailItemQuantity6(retailIndustryTAABean
      .getRetailItemQuantity6(), errorCodes);
    validateRetailRetailItemAmount6(retailIndustryTAABean
      .getRetailItemAmount6(), errorCodes);
  }
  
  private static void validateRetailDepartmentName(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 40, 40);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject("RETAIL", 
          "RetailDepartmentName Length is out of Sequence");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateRetailItemDescription1(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD220_RETAILITEM_DESCRIPTION1);
      errorCodes.add(errorObj);
    }
    else
    {
      String reqLength = 
        CommonValidator.validateLength(value, 19, 19);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject("RETAIL", 
          " RetailItemDescription1 Length is out of Sequence");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateRetailItemQuantity1(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ý']))
      {
        String reqLength = CommonValidator.validateLength(value, 3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject("RETAIL", 
            "RetailItemQuantity1 Length is out of Sequence");
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD221_RETAILITEM_QUANTITY1);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateRetailRetailItemAmount1(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ß']))
      {
        String reqLength = 
          CommonValidator.validateLength(value, 12, 12);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD223_RETAILITEM_AMOUNT1);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD222_RETAILITEM_AMOUNT1);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateRetailItemDescription2(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 19, 19);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject("RETAIL", 
          " RetailItemDescription2 Length is out of Sequence");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateRetailItemQuantity2(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['á']))
      {
        String reqLength = CommonValidator.validateLength(value, 3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject("RETAIL", 
            "RetailItemQuantity2 Length is out of Sequence");
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD224_RETAILITEM_QUANTITY2);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateRetailRetailItemAmount2(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ã']))
      {
        String reqLength = 
          CommonValidator.validateLength(value, 12, 12);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD226_RETAILITEM_AMOUNT2);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD225_RETAILITEM_AMOUNT2);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateRetailItemDescription3(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 19, 19);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject("RETAIL", 
          " RetailItemDescription3 Length is out of Sequence");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateRetailItemQuantity3(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['å']))
      {
        String reqLength = CommonValidator.validateLength(value, 3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject("RETAIL", 
            "RetailItemQuantity3 Length is out of Sequence");
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD227_RETAILITEM_QUANTITY3);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateRetailRetailItemAmount3(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ç']))
      {
        String reqLength = 
          CommonValidator.validateLength(value, 12, 12);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD229_RETAILITEM_AMOUNT3);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD228_RETAILITEM_AMOUNT3);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateRetailItemDescription4(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 19, 19);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject("RETAIL", 
          " RetailItemDescription4 Length is out of Sequence");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateRetailItemQuantity4(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['é']))
      {
        String reqLength = CommonValidator.validateLength(value, 3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject("RETAIL", 
            "RetailItemQuantity4 Length is out of Sequence");
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD230_RETAILITEM_QUANTITY4);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateRetailRetailItemAmount4(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ë']))
      {
        String reqLength = 
          CommonValidator.validateLength(value, 12, 12);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD232_RETAILITEM_AMOUNT4);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD231_RETAILITEM_AMOUNT4);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateRetailItemDescription5(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 19, 19);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject("RETAIL", 
          " RetailItemDescription5 Length is out of Sequence");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateRetailItemQuantity5(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['í']))
      {
        String reqLength = CommonValidator.validateLength(value, 3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject("RETAIL", 
            "RetailItemQuantity5 Length is out of Sequence");
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD233_RETAILITEM_QUANTITY5);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateRetailRetailItemAmount5(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ï']))
      {
        String reqLength = 
          CommonValidator.validateLength(value, 12, 12);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD235_RETAILITEM_AMOUNT5);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD234_RETAILITEM_AMOUNT5);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateRetailItemDescription6(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 19, 19);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject("RETAIL", 
          " RetailItemDescription6 Length is out of Sequence");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateRetailItemQuantity6(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ñ']))
      {
        String reqLength = CommonValidator.validateLength(value, 3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject("RETAIL", 
            "RetailItemQuantity6 Length is out of Sequence");
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD236_RETAIL_ITEM_QUANTITY6);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateRetailRetailItemAmount6(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ó']))
      {
        String reqLength = 
          CommonValidator.validateLength(value, 12, 12);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD238_RETAILITEM_AMOUNT6);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD237_RETAILITEM_AMOUNT6);
        errorCodes.add(errorObj);
      }
    }
  }
}
