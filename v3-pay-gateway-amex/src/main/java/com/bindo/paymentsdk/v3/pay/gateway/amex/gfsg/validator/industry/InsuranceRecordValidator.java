package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.industry;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.ErrorObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceAddendumBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceBasicBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.InsuranceIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.SubmissionErrorCodes;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.exceptions.SettlementException;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.CommonValidator;
import java.util.List;

public class InsuranceRecordValidator
{
  public static void validateInsuranceRecord(TransactionAdviceBasicBean transactionAdviceBasicType, TransactionAdviceAddendumBean transactionAddendumType, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    InsuranceIndustryTAABean insuranceIndustryTAABean = (InsuranceIndustryTAABean)transactionAddendumType;
    
    TAADBRecordValidator.validateRecordType(insuranceIndustryTAABean
      .getRecordType(), errorCodes);
    TAADBRecordValidator.validateRecordNumber(insuranceIndustryTAABean
      .getRecordNumber(), errorCodes);
    TAADBRecordValidator.validateTransactionIdentifier(
      insuranceIndustryTAABean.getTransactionIdentifier(), 
      transactionAdviceBasicType, errorCodes);
    TAADBRecordValidator.validateAddendaTypeCode(insuranceIndustryTAABean
      .getAddendaTypeCode(), errorCodes);
    TAADBRecordValidator.validateFormatCode(insuranceIndustryTAABean
      .getFormatCode(), transactionAdviceBasicType, errorCodes);
    
    validateInsurancePolicyNumber(insuranceIndustryTAABean
      .getInsurancePolicyNumber(), errorCodes);
    validateInsuranceCoverageStartDate(insuranceIndustryTAABean
      .getInsuranceCoverageStartDate(), errorCodes);
    validateInsuranceCoverageEndDate(insuranceIndustryTAABean, errorCodes);
    validateInsurancePolicyPremiumFrequency(insuranceIndustryTAABean
      .getInsurancePolicyPremiumFrequency(), errorCodes);
    validateAdditionalInsurancePolicyNumber(insuranceIndustryTAABean
      .getAdditionalInsurancePolicyNumber(), errorCodes);
    validateTypeOfPolicy(insuranceIndustryTAABean.getTypeOfPolicy(), 
      errorCodes);
    validateNameOfInsured(insuranceIndustryTAABean.getNameOfInsured(), 
      errorCodes);
  }
  
  private static void validateInsurancePolicyNumber(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD387_INSURANCE_POLICYNUMBER);
      errorCodes.add(errorObj);
    }
    else
    {
      String reqLength = 
        CommonValidator.validateLength(value, 23, 23);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD388_INSURANCE_POLICYNUMBER);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateInsuranceCoverageStartDate(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD390_INSURANCECOVERAGE_STARTDATE);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ť']))
    {
      String reqLength = CommonValidator.validateLength(value, 8, 8);
      if ((!reqLength.equals("equal")) || 
      
        (!CommonValidator.isValidDate(value, "CCYYMMDD")))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD390_INSURANCECOVERAGE_STARTDATE);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD392_INSURANCECOVERAGE_STARTDATE);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateInsuranceCoverageEndDate(InsuranceIndustryTAABean insuranceIndustryTAABean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(insuranceIndustryTAABean.getInsuranceCoverageEndDate()))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD393_INSURANCECOVERAGE_ENDDATE);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(insuranceIndustryTAABean.getInsuranceCoverageEndDate(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ŧ']))
    {
      String reqLength = CommonValidator.validateLength(
        insuranceIndustryTAABean.getInsuranceCoverageEndDate(), 
        8, 8);
      if (reqLength.equals("equal")) {
        if (CommonValidator.isValidDate(insuranceIndustryTAABean.getInsuranceCoverageEndDate(), "CCYYMMDD"))
        {
          if (Integer.parseInt(insuranceIndustryTAABean.getInsuranceCoverageEndDate()) >= Integer.parseInt(insuranceIndustryTAABean
            .getInsuranceCoverageStartDate())) {
            return;
          }
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD394_INSURANCECOVERAGE_ENDDATE);
          errorCodes.add(errorObj);
          
          return;
        }
      }
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD393_INSURANCECOVERAGE_ENDDATE);
      errorCodes.add(errorObj);
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD395_INSURANCECOVERAGE_ENDDATE);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateInsurancePolicyPremiumFrequency(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = CommonValidator.validateLength(value, 7, 7);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD398_INSURANCEPOLICY_PREMIUMFREQUENCY);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateAdditionalInsurancePolicyNumber(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 23, 23);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD400_ADDITIONALINSURANCE_POLICYNUMBER);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateTypeOfPolicy(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD401_TYPEOF_POLICY);
      errorCodes.add(errorObj);
    }
    else
    {
      String reqLength = 
        CommonValidator.validateLength(value, 25, 25);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD403_TYPEOF_POLICY);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateNameOfInsured(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 30, 30);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD403_NAMEOF_INSURED);
        errorCodes.add(errorObj);
      }
    }
  }
}
