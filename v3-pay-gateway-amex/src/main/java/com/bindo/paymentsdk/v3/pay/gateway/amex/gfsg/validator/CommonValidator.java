package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.AddAmtTypeCd;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.AddendaTypeCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.AuditAdjInd;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.AutoRentalDistanceUnitOfMeasure;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.BatchOperation;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.CallTypeCd;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.CommunicationsCallTypeCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.CountryCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.CurrencyCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.ElectronicCommerceIndicator;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.FormatCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.InvoiceFormatType;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.LdgSpecProgCd;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.MediaCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.ProcessingCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.ProhibitedCountryCodes;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.RateClassCd;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.RegionCode3;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.ReturnBatchOperationSummary;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.ReturnInvoiceStatus;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.StopOverIndicator;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.SubmissionMethod;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.TaxTypeCd;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.TransProcCd;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.TravDisUnit;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.TravPackTypeCd;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.TravTransTypeCd;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.VehicleClassCodes;

import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonValidator
{
  public static String validateLength(String value, int maxLength, int minLength)
  {
    String reqLength = "";
    if (value.length() < minLength) {
      reqLength = "lessThanMin";
    } else if (maxLength < value.length()) {
      reqLength = "greaterThanMax";
    } else {
      reqLength = "equal";
    }
    return reqLength;
  }
  
  public static boolean validateData(String value, String expression)
  {
    boolean flag = validateCharacters(value, expression);
    return flag;
  }
  
  public static boolean validateCharacters(String value, String expression)
  {
    boolean isValid = false;
    CharSequence inputStr = value;
    
    Pattern pattern = Pattern.compile(expression);
    
    Matcher matcher = pattern.matcher(inputStr);
    if (matcher.matches()) {
      isValid = true;
    }
    return isValid;
  }
  
  public static boolean isNullOrEmpty(String token)
  {
    boolean isNull = false;
    if ((token == null) || ("".equals(token))) {
      isNull = true;
    }
    return isNull;
  }
  
  public static String validateDate(String fileCreationDate)
  {
    String status = "";
    int year = Integer.parseInt(fileCreationDate.substring(0, 4));
    int month = Integer.parseInt(fileCreationDate.substring(4, 6)) - 1;
    int day = Integer.parseInt(fileCreationDate.substring(6));
    Calendar enteredDate = Calendar.getInstance();
    enteredDate.set(year, month, day);
    Calendar currentDate = Calendar.getInstance();
    Calendar pastDate = Calendar.getInstance();
    pastDate.set(Calendar.DAY_OF_MONTH, currentDate.get(Calendar.DAY_OF_MONTH) - 364);
    if (enteredDate.before(pastDate)) {
      status = "expired";
    } else if (enteredDate.after(currentDate)) {
      status = "futureDate";
    }
    return status;
  }
  
  public static boolean isValidDate(String inDate, String dateFormat)
  {
    boolean validDate = true;
    String year = "0";
    
    String day = "0";
    if (dateFormat.equalsIgnoreCase("YYMM"))
    {
      String month = inDate.substring(2);
      if (!validateMonth(month)) {
        validDate = false;
      }
    }
    else
    {
      year = inDate.substring(0, 4);
      String month = inDate.substring(4, 6);
      day = inDate.substring(6, 8);
      if ((!validateMonth(month)) || (!validateDay(year, month, day))) {
        validDate = false;
      }
    }
    return validDate;
  }
  
  private static boolean validateMonth(String month)
  {
    boolean isValidMonth = true;
    if ((Integer.parseInt(month) <= 0) || (Integer.parseInt(month) > 12)) {
      isValidMonth = false;
    }
    return isValidMonth;
  }
  
  private static boolean validateDay(String year, String month, String day)
  {
    boolean isvalidaDay = true;
    if ((Integer.parseInt(month) == 2) && (Integer.parseInt(year) % 4 == 0) && 
      (Integer.parseInt(day) > 29)) {
      isvalidaDay = false;
    } else if ((Integer.parseInt(month) == 2) && 
      (Integer.parseInt(year) % 4 != 0) && 
      (Integer.parseInt(day) > 28)) {
      isvalidaDay = false;
    } else if (((Integer.parseInt(month) == 4) || (Integer.parseInt(month) == 6) || 
      (Integer.parseInt(month) == 9) || (Integer.parseInt(month) == 11)) && 
      (Integer.parseInt(day) > 30)) {
      isvalidaDay = false;
    } else if (Integer.parseInt(day) > 31) {
      isvalidaDay = false;
    }
    return isvalidaDay;
  }
  
  public static boolean validateTime(String time)
  {
    boolean status = true;
    if (time.length() == 6)
    {
      if (Integer.parseInt(time) >= 235960) {
        status = false;
      }
    }
    else {
      status = false;
    }
    return status;
  }
  
  public static boolean isValidStopOverIndicator(String stopOverIndicator)
  {
    boolean isValidStopOverIndicator = false;
    StopOverIndicator[] arrayOfStopOverIndicator;
    int j = (arrayOfStopOverIndicator = StopOverIndicator.values()).length;
    for (int i = 0; i < j; i++)
    {
      StopOverIndicator stopOverIndicatorvalues = arrayOfStopOverIndicator[i];
      if (stopOverIndicator.equalsIgnoreCase(stopOverIndicatorvalues.getStopOverIndicator()))
      {
        isValidStopOverIndicator = true;
        break;
      }
    }
    return isValidStopOverIndicator;
  }
  
  public static boolean isValidFormatCode(String formatCode)
  {
    boolean isValidFormatCode = false;
    FormatCode[] arrayOfFormatCode;
    int j = (arrayOfFormatCode = FormatCode.values()).length;
    for (int i = 0; i < j; i++)
    {
      FormatCode formatCodevalues = arrayOfFormatCode[i];
      if (formatCode.equalsIgnoreCase(formatCodevalues.getFormatCode()))
      {
        isValidFormatCode = true;
        break;
      }
    }
    return isValidFormatCode;
  }
  
  public static boolean isValidMediaCode(String mediaCode)
  {
    boolean isValidMediaCode = false;
    MediaCode[] arrayOfMediaCode;
    int j = (arrayOfMediaCode = MediaCode.values()).length;
    for (int i = 0; i < j; i++)
    {
      MediaCode mediaCodes = arrayOfMediaCode[i];
      if (mediaCode.equalsIgnoreCase(mediaCodes.getMediaCode()))
      {
        isValidMediaCode = true;
        break;
      }
    }
    return isValidMediaCode;
  }
  
  public static boolean isValidSubCode(String subCode)
  {
    boolean isValidCode = false;
    SubmissionMethod[] arrayOfSubmissionMethod;
    int j = (arrayOfSubmissionMethod = SubmissionMethod.values()).length;
    for (int i = 0; i < j; i++)
    {
      SubmissionMethod subCodes = arrayOfSubmissionMethod[i];
      if (subCode.equalsIgnoreCase(subCodes.getSubmissionMethod()))
      {
        isValidCode = true;
        break;
      }
    }
    return isValidCode;
  }
  
  public static boolean isValidProcessingCode(String processingCode)
  {
    boolean isValidProcessingCode = false;
    ProcessingCode[] arrayOfProcessingCode;
    int j = (arrayOfProcessingCode = ProcessingCode.values()).length;
    for (int i = 0; i < j; i++)
    {
      ProcessingCode posCodes = arrayOfProcessingCode[i];
      if (processingCode.equalsIgnoreCase(posCodes.getProcessingCode()))
      {
        isValidProcessingCode = true;
        break;
      }
    }
    return isValidProcessingCode;
  }
  
  public static boolean isValidAddendaTypeCode(String addendaTypeCode)
  {
    boolean isValidCode = false;
    AddendaTypeCode[] arrayOfAddendaTypeCode;
    int j = (arrayOfAddendaTypeCode = AddendaTypeCode.values()).length;
    for (int i = 0; i < j; i++)
    {
      AddendaTypeCode addCodes = arrayOfAddendaTypeCode[i];
      if (addendaTypeCode.equalsIgnoreCase(addCodes.getAddendaTypeCode()))
      {
        isValidCode = true;
        break;
      }
    }
    return isValidCode;
  }
  
  public static boolean isValidCommunicationsCallTypeCode(String communicationsCallTypeCode)
  {
    boolean isValidCode = false;
    CommunicationsCallTypeCode[] arrayOfCommunicationsCallTypeCode;
    int j = (arrayOfCommunicationsCallTypeCode = CommunicationsCallTypeCode.values()).length;
    for (int i = 0; i < j; i++)
    {
      CommunicationsCallTypeCode communicationsCallTypeCodes = arrayOfCommunicationsCallTypeCode[i];
      if (communicationsCallTypeCode.equalsIgnoreCase(communicationsCallTypeCodes.getCommunicationsCallTypeCode()))
      {
        isValidCode = true;
        break;
      }
    }
    return isValidCode;
  }
  
  public static boolean isValidModulusNineCheck(String merchantId)
  {
    boolean isValid = true;
    if (!isNullOrEmpty(merchantId)) {
      isValid = CheckDigitValidator.modulusNineCheck(merchantId);
    }
    return isValid;
  }
  
  public static boolean isValidBatchOperation(String batchOperation)
  {
    boolean isValidBatchOperation = false;
    BatchOperation[] arrayOfBatchOperation;
    int j = (arrayOfBatchOperation = BatchOperation.values()).length;
    for (int i = 0; i < j; i++)
    {
      BatchOperation batchOperations = arrayOfBatchOperation[i];
      if (batchOperation.equalsIgnoreCase(batchOperations.getBatchOperation()))
      {
        isValidBatchOperation = true;
        break;
      }
    }
    return isValidBatchOperation;
  }
  
  public static boolean isValidReturnBatchOperationSummary(String returnBatchOperationSummary)
  {
    boolean isValidReturnBatchOperationSummary = false;
    ReturnBatchOperationSummary[] arrayOfReturnBatchOperationSummary;
    int j = (arrayOfReturnBatchOperationSummary = ReturnBatchOperationSummary.values()).length;
    for (int i = 0; i < j; i++)
    {
      ReturnBatchOperationSummary returnBatchSummary = arrayOfReturnBatchOperationSummary[i];
      if (returnBatchOperationSummary.equalsIgnoreCase(returnBatchSummary.getReturnBatchOperationSummary()))
      {
        isValidReturnBatchOperationSummary = true;
        break;
      }
    }
    return isValidReturnBatchOperationSummary;
  }
  
  public static boolean isValidCountryCode(String countryCode)
  {
    boolean isValidCountryCode = false;
    CountryCode[] arrayOfCountryCode;
    int j = (arrayOfCountryCode = CountryCode.values()).length;
    for (int i = 0; i < j; i++)
    {
      CountryCode countryCodes = arrayOfCountryCode[i];
      if (countryCode.equalsIgnoreCase(countryCodes.getCountryCode()))
      {
        isValidCountryCode = true;
        break;
      }
    }
    return isValidCountryCode;
  }
  
  public static boolean isValidLdgSpecProgCd(String ldgSpecProgCd)
  {
    boolean isValidLdgSpecProgCd = false;
    LdgSpecProgCd[] arrayOfLdgSpecProgCd;
    int j = (arrayOfLdgSpecProgCd = LdgSpecProgCd.values()).length;
    for (int i = 0; i < j; i++)
    {
      LdgSpecProgCd ldgSpecProgCds = arrayOfLdgSpecProgCd[i];
      if (ldgSpecProgCd.equalsIgnoreCase(ldgSpecProgCds.getLdgSpecProgCd()))
      {
        isValidLdgSpecProgCd = true;
        break;
      }
    }
    return isValidLdgSpecProgCd;
  }
  
  public static boolean isValidTravTransTypeCd(String travTransTypeCd)
  {
    boolean isValidTravTransTypeCd = false;
    TravTransTypeCd[] arrayOfTravTransTypeCd;
    int j = (arrayOfTravTransTypeCd = TravTransTypeCd.values()).length;
    for (int i = 0; i < j; i++)
    {
      TravTransTypeCd travTransTypeCds = arrayOfTravTransTypeCd[i];
      if (travTransTypeCd.equalsIgnoreCase(travTransTypeCds.getTravTransTypeCd()))
      {
        isValidTravTransTypeCd = true;
        break;
      }
    }
    return isValidTravTransTypeCd;
  }
  
  public static boolean isVehClassId(String vehClassId)
  {
    boolean isVehClassId = false;
    VehicleClassCodes[] arrayOfVehicleClassCodes;
    int j = (arrayOfVehicleClassCodes = VehicleClassCodes.values()).length;
    for (int i = 0; i < j; i++)
    {
      VehicleClassCodes vehicleClassCodes = arrayOfVehicleClassCodes[i];
      if (vehClassId.equalsIgnoreCase(vehicleClassCodes.getVehicleClassCodes()))
      {
        isVehClassId = true;
        break;
      }
    }
    return isVehClassId;
  }
  
  public static boolean isValidTravDisUnit(String travDisUnit)
  {
    boolean isValidTravDisUnit = false;
    TravDisUnit[] arrayOfTravDisUnit;
    int j = (arrayOfTravDisUnit = TravDisUnit.values()).length;
    for (int i = 0; i < j; i++)
    {
      TravDisUnit travDisUnits = arrayOfTravDisUnit[i];
      if (travDisUnit.equalsIgnoreCase(travDisUnits.getTravDisUnit()))
      {
        isValidTravDisUnit = true;
        break;
      }
    }
    return isValidTravDisUnit;
  }
  
  public static boolean isValidAuditAdjInd(String auditAdjInd)
  {
    boolean isValidAuditAdjInd = false;
    AuditAdjInd[] arrayOfAuditAdjInd;
    int j = (arrayOfAuditAdjInd = AuditAdjInd.values()).length;
    for (int i = 0; i < j; i++)
    {
      AuditAdjInd auditAdjInds = arrayOfAuditAdjInd[i];
      if (auditAdjInd.equalsIgnoreCase(auditAdjInds.getAuditAdjInd()))
      {
        isValidAuditAdjInd = true;
        break;
      }
    }
    return isValidAuditAdjInd;
  }
  
  public static boolean isValidCallTypeCd(String callTypeCd)
  {
    boolean isValidCallTypeCd = false;
    CallTypeCd[] arrayOfCallTypeCd;
    int j = (arrayOfCallTypeCd = CallTypeCd.values()).length;
    for (int i = 0; i < j; i++)
    {
      CallTypeCd callTypeCds = arrayOfCallTypeCd[i];
      if (callTypeCd.equalsIgnoreCase(callTypeCds.getCallTypeCd()))
      {
        isValidCallTypeCd = true;
        break;
      }
    }
    return isValidCallTypeCd;
  }
  
  public static boolean isValidRateClassCd(String rateClassCd)
  {
    boolean isValidRateClassCd = false;
    RateClassCd[] arrayOfRateClassCd;
    int j = (arrayOfRateClassCd = RateClassCd.values()).length;
    for (int i = 0; i < j; i++)
    {
      RateClassCd rateClassCds = arrayOfRateClassCd[i];
      if (rateClassCd.equalsIgnoreCase(rateClassCds.getRateClassCd()))
      {
        isValidRateClassCd = true;
        break;
      }
    }
    return isValidRateClassCd;
  }
  
  public static boolean isValidTravPackTypeCd(String travPackTypeCd)
  {
    boolean isValidTravPackTypeCd = false;
    TravPackTypeCd[] arrayOfTravPackTypeCd;
    int j = (arrayOfTravPackTypeCd = TravPackTypeCd.values()).length;
    for (int i = 0; i < j; i++)
    {
      TravPackTypeCd travPackTypeCds = arrayOfTravPackTypeCd[i];
      if (travPackTypeCd.equalsIgnoreCase(travPackTypeCds.getTravPackTypeCd()))
      {
        isValidTravPackTypeCd = true;
        break;
      }
    }
    return isValidTravPackTypeCd;
  }
  
  public static boolean isAutoRentalDistanceUnitOfMessure(String autoRentalDistanceUnitOfMeasure)
  {
    boolean isValidCode = false;
    AutoRentalDistanceUnitOfMeasure[] arrayOfAutoRentalDistanceUnitOfMeasure;
    int j = (arrayOfAutoRentalDistanceUnitOfMeasure = AutoRentalDistanceUnitOfMeasure.values()).length;
    for (int i = 0; i < j; i++)
    {
      AutoRentalDistanceUnitOfMeasure autoRentalDistanceUnitOfMessures = arrayOfAutoRentalDistanceUnitOfMeasure[i];
      if (autoRentalDistanceUnitOfMeasure.equalsIgnoreCase(autoRentalDistanceUnitOfMessures.getAutoRentalDistanceUnitOfMeasure()))
      {
        isValidCode = true;
        break;
      }
    }
    return isValidCode;
  }
  
  public static boolean isValidTaxTypeCd(String taxTypeCd)
  {
    boolean isValidTaxTypeCd = false;
    TaxTypeCd[] arrayOfTaxTypeCd;
    int j = (arrayOfTaxTypeCd = TaxTypeCd.values()).length;
    for (int i = 0; i < j; i++)
    {
      TaxTypeCd taxTypeCds = arrayOfTaxTypeCd[i];
      if (taxTypeCd.equalsIgnoreCase(taxTypeCds.getTaxTypeCd()))
      {
        isValidTaxTypeCd = true;
        break;
      }
    }
    return isValidTaxTypeCd;
  }
  
  public static boolean isValidInvoiceFormatType(String invoiceFormatType)
  {
    boolean isValidInvoiceFormatType = false;
    InvoiceFormatType[] arrayOfInvoiceFormatType;
    int j = (arrayOfInvoiceFormatType = InvoiceFormatType.values()).length;
    for (int i = 0; i < j; i++)
    {
      InvoiceFormatType invoiceFormatTypes = arrayOfInvoiceFormatType[i];
      if (invoiceFormatType.equalsIgnoreCase(invoiceFormatTypes.getInvoiceFormatType()))
      {
        isValidInvoiceFormatType = true;
        break;
      }
    }
    return isValidInvoiceFormatType;
  }
  
  public static boolean isValidReturnInvoiceStatus(String returnInvoiceStatus)
  {
    boolean isValidReturnInvoiceStatus = false;
    ReturnInvoiceStatus[] arrayOfReturnInvoiceStatus;
    int j = (arrayOfReturnInvoiceStatus = ReturnInvoiceStatus.values()).length;
    for (int i = 0; i < j; i++)
    {
      ReturnInvoiceStatus returnInvoiceStatusValue = arrayOfReturnInvoiceStatus[i];
      if (returnInvoiceStatus.equalsIgnoreCase(returnInvoiceStatusValue.getReturnInvoiceStatus()))
      {
        isValidReturnInvoiceStatus = true;
        break;
      }
    }
    return isValidReturnInvoiceStatus;
  }
  
  public static boolean isValidElecComrceInd(String elecComrceInd)
  {
    boolean isValidElecComrceInd = false;
    ElectronicCommerceIndicator[] arrayOfElectronicCommerceIndicator;
    int j = (arrayOfElectronicCommerceIndicator = ElectronicCommerceIndicator.values()).length;
    for (int i = 0; i < j; i++)
    {
      ElectronicCommerceIndicator elecComrceInds = arrayOfElectronicCommerceIndicator[i];
      if (elecComrceInd.equalsIgnoreCase(elecComrceInds.getElectronicCommerceIndicator()))
      {
        isValidElecComrceInd = true;
        break;
      }
    }
    return isValidElecComrceInd;
  }
  
  public static boolean isValidAmount(String countryCode, String amount)
  {
    return true;
    /*
    boolean isValid = false;
    boolean isAlpha = false;
    Object localObject;
    int j = (localObject = AlphaCurrencyCode.values()).length;
    for (int i = 0; i < j; i++)
    {
      AlphaCurrencyCode alphaCurrCode = localObject[i];
      if (countryCode.equalsIgnoreCase(alphaCurrCode.getAlphaCurrencyCode())) {
        isAlpha = true;
      }
    }
    if (isAlpha)
    {
      j = (localObject = AlphaCurrencyCode.values()).length;
      for (i = 0; i < j; i++)
      {
        AlphaCurrencyCode alphaCurrCode = localObject[i];
        if (countryCode.equalsIgnoreCase(alphaCurrCode.getAlphaCurrencyCode())) {
          countryCode = alphaCurrCode.getNumCurrencyCode();
        }
      }
      j = (localObject = CurrencyCodeAndAmount.values()).length;
      for (i = 0; i < j; i++)
      {
        CurrencyCodeAndAmount curCodesAndAmounts = localObject[i];
        if ((countryCode.equalsIgnoreCase(curCodesAndAmounts.getCurrencyCode())) && 
          (compareAmounts(amount, curCodesAndAmounts.getAmount())))
        {
          isValid = true;
          break;
        }
      }
    }
    else
    {
      j = (localObject = CurrencyCodeAndAmount.values()).length;
      for (i = 0; i < j; i++)
      {
        CurrencyCodeAndAmount curCodesAndAmounts = localObject[i];
        if ((countryCode.equalsIgnoreCase(curCodesAndAmounts.getCurrencyCode())) && 
          (compareAmounts(amount, curCodesAndAmounts.getAmount())))
        {
          isValid = true;
          break;
        }
      }
    }
    return isValid;
    */
  }
  
  private static boolean compareAmounts(String transactionAmount, String currencySpecificAmount)
  {
    boolean amountStatus = true;
    if ((!isNullOrEmpty(transactionAmount)) && (!isNullOrEmpty(currencySpecificAmount))) {
      if (Long.parseLong(currencySpecificAmount) < Long.parseLong(transactionAmount)) {
        amountStatus = false;
      }
    }
    return amountStatus;
  }
  
  public static boolean isValidModulusTenCheck(String cardNumber)
  {
    boolean isValid = true;
    if (!isNullOrEmpty(cardNumber)) {
      isValid = CheckDigitValidator.modulusTenCheck(cardNumber);
    }
    return isValid;
  }
  
  public static boolean isValidCurrencyCode(String currencyCode)
  {
    boolean isValidCurrencyCode = false;
    CurrencyCode[] arrayOfCurrencyCode;
    int j = (arrayOfCurrencyCode = CurrencyCode.values()).length;
    for (int i = 0; i < j; i++)
    {
      CurrencyCode currencyCodes = arrayOfCurrencyCode[i];
      if (currencyCode.equalsIgnoreCase(currencyCodes.getCurrencyCode()))
      {
        isValidCurrencyCode = true;
        break;
      }
    }
    return isValidCurrencyCode;
  }
  
  public static boolean isValidRegionCode(String regionCode, String countryCode)
  {
    return true;
    /*
    boolean isValidRegionCode = false;
    if (isValidXmlCountryCode(countryCode))
    {
      XmlCountryCode[] arrayOfXmlCountryCode;
      int j = (arrayOfXmlCountryCode = XmlCountryCode.values()).length;
      for (int i = 0; i < j; i++)
      {
        XmlCountryCode countryCodes = arrayOfXmlCountryCode[i];
        if ((countryCode.equalsIgnoreCase(countryCodes.getNumCountryCode())) || 
        
          (countryCode.equalsIgnoreCase(countryCodes.getAlphaCountryCode())))
        {
          String numCountryCode = countryCodes.getNumCountryCode();
          if (validateRegionCode1(regionCode, numCountryCode))
          {
            isValidRegionCode = true;
            
            break;
          }
          if (validateRegionCode2(regionCode, numCountryCode))
          {
            isValidRegionCode = true;
            
            break;
          }
          if (!validateRegionCode3(regionCode, numCountryCode)) {
            break;
          }
          isValidRegionCode = true;
          
          break;
        }
      }
    }
    else
    {
      isValidRegionCode = false;
    }
    return isValidRegionCode;
    */
  }
  
  public static boolean validateRegionCode1(String regionCode, String countryCode)
  {
    return true;
    /*
    boolean isValidRegionCode = false;
    RegionCode1[] arrayOfRegionCode1;
    int j = (arrayOfRegionCode1 = RegionCode1.values()).length;
    for (int i = 0; i < j; i++)
    {
      RegionCode1 regionCodes = arrayOfRegionCode1[i];
      if (countryCode.equalsIgnoreCase(regionCodes.getCountryCode())) {
        if (regionCode.equalsIgnoreCase(regionCodes.getRegionCode()))
        {
          isValidRegionCode = true;
          break;
        }
      }
    }
    return isValidRegionCode;
    */
  }
  
  public static boolean validateRegionCode2(String regionCode, String countryCode)
  {
    return true;
    /*
    boolean isValidRegionCode = false;
    RegionCode2[] arrayOfRegionCode2;
    int j = (arrayOfRegionCode2 = RegionCode2.values()).length;
    for (int i = 0; i < j; i++)
    {
      RegionCode2 regionCodes = arrayOfRegionCode2[i];
      if (countryCode.equalsIgnoreCase(regionCodes.getCountryCode())) {
        if (regionCode.equalsIgnoreCase(regionCodes.getRegionCode()))
        {
          isValidRegionCode = true;
          break;
        }
      }
    }
    return isValidRegionCode;
    */
  }
  
  public static boolean validateRegionCode3(String regionCode, String countryCode)
  {
    boolean isValidRegionCode = false;
    RegionCode3[] arrayOfRegionCode3;
    int j = (arrayOfRegionCode3 = RegionCode3.values()).length;
    for (int i = 0; i < j; i++)
    {
      RegionCode3 regionCodes = arrayOfRegionCode3[i];
      if (countryCode.equalsIgnoreCase(regionCodes.getCountryCode())) {
        if (regionCode.equalsIgnoreCase(regionCodes.getRegionCode()))
        {
          isValidRegionCode = true;
          break;
        }
      }
    }
    return isValidRegionCode;
  }
  
  public static boolean isProhibitedCurrencyCode(String currencyCode)
  {
    boolean isProhibitedCode = false;
    ProhibitedCountryCodes[] arrayOfProhibitedCountryCodes;
    int j = (arrayOfProhibitedCountryCodes = ProhibitedCountryCodes.values()).length;
    for (int i = 0; i < j; i++)
    {
      ProhibitedCountryCodes prohibitedCountryCodes = arrayOfProhibitedCountryCodes[i];
      if (currencyCode.equalsIgnoreCase(prohibitedCountryCodes.getCountryCode()))
      {
        isProhibitedCode = true;
        break;
      }
    }
    return isProhibitedCode;
  }
  
  public static boolean isValidXmlDate(String inDate, String format)
  {
    boolean validDate = true;
    String year = "0";
    String month = "";
    String day = "0";
    if (format.equalsIgnoreCase("YYMM"))
    {
      if (inDate.length() == 4)
      {
        month = inDate.substring(2);
        if (!validateXmlMonth(month)) {
          validDate = false;
        }
      }
      else
      {
        validDate = false;
      }
    }
    else if (inDate.length() == 8)
    {
      year = inDate.substring(0, 4);
      month = inDate.substring(4, 6);
      day = inDate.substring(6, 8);
      if ((!validateXmlMonth(month)) || 
        (!validateXmlDay(year, month, day))) {
        validDate = false;
      }
    }
    else
    {
      validDate = false;
    }
    return validDate;
  }
  
  private static boolean validateXmlMonth(String month)
  {
    boolean isValidMonth = true;
    if ((Integer.parseInt(month) <= 0) || (Integer.parseInt(month) > 12)) {
      isValidMonth = false;
    }
    return isValidMonth;
  }
  
  private static boolean validateXmlDay(String yy, String mm, String dd)
  {
    boolean isValid = true;
    int month = Integer.parseInt(mm);
    int date = Integer.parseInt(dd);
    int year = Integer.parseInt(yy);
    if ((month == 1) || (month == 3) || (month == 5) || (month == 7) || (month == 8) || 
      (month == 10) || (month == 12))
    {
      if ((date > 31) || (date <= 0)) {
        isValid = false;
      }
    }
    else if ((month == 4) || (month == 6) || (month == 9) || (month == 11))
    {
      if ((date > 30) || (date <= 0)) {
        isValid = false;
      }
    }
    else if (month == 2)
    {
      boolean isLeapYear = year % 4 == 0;
      if (isLeapYear)
      {
        if ((date <= 0) || (date > 29)) {
          isValid = false;
        } else {
          isValid = true;
        }
      }
      else if ((date <= 0) || (date > 28)) {
        isValid = false;
      }
    }
    return isValid;
  }
  
  public static boolean isValidAddAmtTypeCd(String addAmtTypeCd)
  {
    boolean isValidAddAmtTypeCd = false;
    AddAmtTypeCd[] arrayOfAddAmtTypeCd;
    int j = (arrayOfAddAmtTypeCd = AddAmtTypeCd.values()).length;
    for (int i = 0; i < j; i++)
    {
      AddAmtTypeCd addAmtTypeCds = arrayOfAddAmtTypeCd[i];
      if (addAmtTypeCd.equalsIgnoreCase(addAmtTypeCds.getAddAmtTypeCd()))
      {
        isValidAddAmtTypeCd = true;
        break;
      }
    }
    return isValidAddAmtTypeCd;
  }
  
  public static boolean isValidXmlTransProcCd(String transProcCd)
  {
    boolean isValidTransProcCd = false;
    TransProcCd[] arrayOfTransProcCd;
    int j = (arrayOfTransProcCd = TransProcCd.values()).length;
    for (int i = 0; i < j; i++)
    {
      TransProcCd posCodes = arrayOfTransProcCd[i];
      if (transProcCd.equalsIgnoreCase(posCodes.getTransProcCd()))
      {
        isValidTransProcCd = true;
        break;
      }
    }
    return isValidTransProcCd;
  }
  
  public static boolean isValidXmlAmount(String currencyCode, String amount)
  {
    return true;
    /*
    boolean isValid = false;
    XmlCurrencyCode[] arrayOfXmlCurrencyCode;
    int j = (arrayOfXmlCurrencyCode = XmlCurrencyCode.values()).length;
    for (int i = 0; i < j; i++)
    {
      XmlCurrencyCode xmlCurrencyCode = arrayOfXmlCurrencyCode[i];
      if ((currencyCode.equalsIgnoreCase(xmlCurrencyCode.getAlphaCurrencyCode())) || 
        (currencyCode.equalsIgnoreCase(xmlCurrencyCode.getNumCurrencyCode()))) {
        if (compareAmounts(amount, xmlCurrencyCode.getMaxTransAmt()))
        {
          isValid = true;
          break;
        }
      }
    }
    return isValid;
    */
  }
  
  public static boolean isValidXmlCurrencyCode(String currencyCode)
  {
    return true;
    /*
    boolean isValidCurrencyCode = false;
    XmlCurrencyCode[] arrayOfXmlCurrencyCode;
    int j = (arrayOfXmlCurrencyCode = XmlCurrencyCode.values()).length;
    for (int i = 0; i < j; i++)
    {
      XmlCurrencyCode currencyCodes = arrayOfXmlCurrencyCode[i];
      if ((currencyCode.equalsIgnoreCase(currencyCodes.getAlphaCurrencyCode())) || 
      
        (currencyCode.equalsIgnoreCase(currencyCodes.getNumCurrencyCode())))
      {
        isValidCurrencyCode = true;
        break;
      }
    }
    return isValidCurrencyCode;
    */
  }
  
  public static boolean isValidXmlCountryCode(String countryCode)
  {
    return true;
    /*
    boolean isValidCountryCode = false;
    XmlCountryCode[] arrayOfXmlCountryCode;
    int j = (arrayOfXmlCountryCode = XmlCountryCode.values()).length;
    for (int i = 0; i < j; i++)
    {
      XmlCountryCode countryCodes = arrayOfXmlCountryCode[i];
      if (!countryCode.equalsIgnoreCase(countryCodes.getNumCountryCode()))
      {
        if (countryCode.equalsIgnoreCase(countryCodes.getAlphaCountryCode())) {
          if (countryCodes.getUsage().equalsIgnoreCase("X")) {}
        }
      }
      else
      {
        isValidCountryCode = true;
        break;
      }
    }
    return isValidCountryCode;
    */
  }
  
  public static String validateTransactionDate(String transactionDate)
  {
    String status = "";
    int year = Integer.parseInt(transactionDate.substring(0, 4));
    int month = Integer.parseInt(transactionDate.substring(4, 6)) - 1;
    int day = Integer.parseInt(transactionDate.substring(6));
    Calendar enteredDate = Calendar.getInstance();
    enteredDate.set(year, month, day);
    Calendar currentDate = Calendar.getInstance();
    Calendar pastDate = Calendar.getInstance();
    pastDate.set(Calendar.DAY_OF_MONTH, currentDate.get(Calendar.DAY_OF_MONTH) - 364);
    Calendar futureDate = Calendar.getInstance();
    futureDate.set(Calendar.DAY_OF_MONTH, currentDate.get(Calendar.DAY_OF_MONTH) + 1);
    if (enteredDate.before(pastDate)) {
      status = "expired";
    } else if (enteredDate.after(futureDate)) {
      status = "futureDate";
    }
    return status;
  }

  /*
  public static boolean isIndustryRecordPresent(DataCaptureRequestBean dataCaptureRequestBean)
  {
    boolean industryPresentflag = false;
    if (dataCaptureRequestBean.getTaaAirIndustry() != null) {
      industryPresentflag = true;
    } else if (dataCaptureRequestBean.getTaaAutoRentalIndustry() != null) {
      industryPresentflag = true;
    } else if (dataCaptureRequestBean.getTaaCommunServIndustry() != null) {
      industryPresentflag = true;
    } else if (dataCaptureRequestBean.getTaaEntertainmentTktIndustry() != null) {
      industryPresentflag = true;
    } else if (dataCaptureRequestBean.getTaaInsuranceIndustry() != null) {
      industryPresentflag = true;
    } else if (dataCaptureRequestBean.getTaaLodgingIndustry() != null) {
      industryPresentflag = true;
    } else if (dataCaptureRequestBean.getTaaRailIndustry() != null) {
      industryPresentflag = true;
    } else if (dataCaptureRequestBean.getTaaRetailIndustry() != null) {
      industryPresentflag = true;
    } else if (dataCaptureRequestBean.getTaaTravCruiseIndustry() != null) {
      industryPresentflag = true;
    }
    return industryPresentflag;
  }
  */

  public static String getMasked(int length, String symbol)
  {
    String temp = "";
    for (int i = 0; i < length; i++) {
      temp = symbol + temp;
    }
    return temp;
  }
}
