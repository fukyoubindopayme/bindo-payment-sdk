package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.connection;

import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.Constants;

public class HTTPSConnectionInfo
{
  private String uri = "";
  private String proxyAddress = "";
  private String userId = "";
  private String password = "";
  private String port = "";
  private String proxy = "";
  private String methodType = "";
  private String userAgent = "";
  private String host = "";
  private String apiVersion = "";
  private String proxyAuthentication;
  
  public String getUserAgent()
  {
    return this.userAgent;
  }
  
  public void setUserAgent(String userAgent)
  {
    this.userAgent = userAgent;
  }
  
  public String getHost()
  {
    return this.host;
  }
  
  public void setHost(String host)
  {
    this.host = host;
  }
  
  public String getMethodType()
  {
    return this.methodType;
  }
  
  public void setMethodType(String methodType)
  {
    this.methodType = methodType;
  }
  
  public String getProxy()
  {
    return this.proxy;
  }
  
  public void setProxy(String proxy)
  {
    this.proxy = proxy;
  }
  
  public String getProxyAddress()
  {
    return this.proxyAddress;
  }
  
  public void setProxyAddress(String proxyAddress)
  {
    this.proxyAddress = proxyAddress;
  }
  
  public String getUserId()
  {
    return this.userId;
  }
  
  public void setUserId(String userId)
  {
    this.userId = userId;
  }
  
  public String getPassword()
  {
    return this.password;
  }
  
  public void setPassword(String password)
  {
    this.password = password;
  }
  
  public String getPort()
  {
    return this.port;
  }
  
  public void setPort(String port)
  {
    this.port = port;
  }
  
  public String getUri()
  {
    return this.uri;
  }
  
  public void setUri(String uri)
  {
    this.uri = uri;
  }
  
  public String getAPIVersion()
  {
    return this.apiVersion;
  }
  
  public void setAPIVersion(String version)
  {
    this.apiVersion = version;
  }
  
  public String getProxyAuthentication()
  {
    return this.proxyAuthentication;
  }
  
  public void setProxyAuthentication(String proxyAuthentication)
  {
    this.proxyAuthentication = proxyAuthentication;
  }
  
  public String toString()
  {
    StringBuffer buffer = new StringBuffer();
    buffer.append("HTTPS_URL").append(" :: ").append(getUri()).append(Constants.NEW_LINE_CHAR);
    buffer.append("PROXY_PORT").append(" :: ").append(getPort()).append(Constants.NEW_LINE_CHAR);
    buffer.append("PROXY").append(" :: ").append(getProxy()).append(Constants.NEW_LINE_CHAR);
    buffer.append("METHOD_TYPE").append(" :: ").append(getMethodType()).append(Constants.NEW_LINE_CHAR);
    buffer.append("USER_AGENT").append(" :: ").append(getUserAgent()).append(Constants.NEW_LINE_CHAR);
    return buffer.toString();
  }
}
