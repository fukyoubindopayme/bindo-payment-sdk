package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums;

public enum MessageTypeID {
    ISO_AuthorizationRequest("1100"),
    ISO_AuthorizationAdjustmentFinancialAdvice("1220"),
    ISO_ReversalAdviceRequest("1420"),
    ISO_NetworkManagementRequest("1804");

    private final String messageTypeIdentifier;

    private MessageTypeID(String messageTypeID) {
        this.messageTypeIdentifier = messageTypeID;
    }

    public String getMessageTypeID() {
        return this.messageTypeIdentifier;
    }
}
