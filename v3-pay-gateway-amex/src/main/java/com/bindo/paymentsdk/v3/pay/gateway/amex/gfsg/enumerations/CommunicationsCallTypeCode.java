package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations;

public enum CommunicationsCallTypeCode
{
  InternationaToll_Free_Number("ITF"),  International_Subscriber_Dialing("ISD"),  Trunk_Dialing_Subscriber("TDS"),  Anuntimed_charge_service("LOCAL");
  
  private final String communicationsCallTypeCode;
  
  private CommunicationsCallTypeCode(String communicationsCallTypeCode)
  {
    this.communicationsCallTypeCode = communicationsCallTypeCode;
  }
  
  public String getCommunicationsCallTypeCode()
  {
    return this.communicationsCallTypeCode;
  }
}
