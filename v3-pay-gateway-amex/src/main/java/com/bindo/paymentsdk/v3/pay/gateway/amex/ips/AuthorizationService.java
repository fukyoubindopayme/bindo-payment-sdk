package com.bindo.paymentsdk.v3.pay.gateway.amex.ips;

import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.connection.Connection;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.connection.PropertyReader;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.Constants;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.AlphaRegionCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.AuthErrorCodes;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.ExecutionType;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.MessageTypeID;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.exception.IPSAPIException;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.exception.SSLConnectionException;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean.AuthorizationRequestBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean.AuthorizationResponseBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean.ErrorObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.messageformatter.AuthorizationMessageFormatter;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.utils.ISOUtil;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.utils.Utility;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AuthorizationService
{
  private boolean debugFlag = false;
  
  public AuthorizationResponseBean validateAuthorizationRequest(AuthorizationRequestBean authorizationRequestBean)
  {
    AuthorizationResponseBean authorizationResponseBean = new AuthorizationResponseBean();
    
    AuthorizationMessageFormatter authMesssageFormatter = new AuthorizationMessageFormatter();
    if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON"))
    {
      this.debugFlag = true;

      Log.i("\r\n----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n-                                 GCAG SDK Logging  File Version                                                                                                                            -\r\n---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
      
      Log.i("Entered into validateAuthorizationRequest()");
    }
    authorizationResponseBean.setAuthErrorList(new ArrayList());
    if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getMessageTypeIdentifier()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD522_MESSAGETYPEIDENTIFIER);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
        Log.e("Message Type Identifier is incorrect");
      }
    }
    else if (!ISOUtil.isValidMessageTypeIdentifier(authorizationRequestBean.getMessageTypeIdentifier()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD523_MESSAGETYPEIDENTIFIER);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
        Log.e("Message Type Identifier is incorrect");
      }
    }
    if (MessageTypeID.ISO_AuthorizationAdjustmentFinancialAdvice.getMessageTypeID().equalsIgnoreCase(authorizationRequestBean.getMessageTypeIdentifier())) {
      authMesssageFormatter.validateAdjustmentFinancialAdviceRequestType(authorizationRequestBean, authorizationResponseBean);
    } else if (MessageTypeID.ISO_NetworkManagementRequest.getMessageTypeID().equalsIgnoreCase(authorizationRequestBean.getMessageTypeIdentifier())) {
      authMesssageFormatter.validateNetworkManagementRequestType(authorizationRequestBean, authorizationResponseBean);
    } else if (MessageTypeID.ISO_ReversalAdviceRequest.getMessageTypeID().equalsIgnoreCase(authorizationRequestBean.getMessageTypeIdentifier())) {
      authMesssageFormatter.validateReversalAdviceRequestType(authorizationRequestBean, authorizationResponseBean);
    } else {
      authMesssageFormatter.validateAuthorizationRequestType(authorizationRequestBean, authorizationResponseBean);
    }
    boolean isValid = false;
    if ((authorizationResponseBean.getAuthErrorList() == null) || (authorizationResponseBean.getAuthErrorList().isEmpty())) {
      isValid = true;
    }
    if ((!isValid) && (this.debugFlag))
    {
      Utility.debug(Utility.trackTrace.toString(), true);
      Utility.trackTrace = new StringBuffer("");
      this.debugFlag = false;
    }
    if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
      Log.i("Exiting validateAuthorizationRequest()");
    }
    return authorizationResponseBean;
  }
  
  public AuthorizationResponseBean validateAuthorizationRequest(AuthorizationRequestBean authorizationRequestBean, ExecutionType exeType)
  {
    AuthorizationResponseBean authorizationResponseBean = new AuthorizationResponseBean();
    
    AuthorizationMessageFormatter authMesssageFormatter = new AuthorizationMessageFormatter();
    if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON"))
    {
      this.debugFlag = true;
      
      Log.i("\r\n----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n-                                  GCAG SDK Logging  File Version                                                                                                                            -\r\n---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
      
      Log.i("Entered into validateAuthorizationRequest()");
    }
    authorizationResponseBean.setAuthErrorList(new ArrayList());
    authMesssageFormatter.validateDigitalGatewayRequestType(authorizationRequestBean, authorizationResponseBean);
    
    boolean isValid = false;
    if ((authorizationResponseBean.getAuthErrorList() == null) || (authorizationResponseBean.getAuthErrorList().isEmpty())) {
      isValid = true;
    }
    if ((!isValid) && (this.debugFlag))
    {
      Utility.debug(Utility.trackTrace.toString(), true);
      Utility.trackTrace = new StringBuffer("");
      this.debugFlag = false;
    }
    if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
      Log.i("Exiting validateAuthorizationRequest()");
    }
    return authorizationResponseBean;
  }
  
  public synchronized AuthorizationResponseBean createAndSendAuthorizationRequest(AuthorizationRequestBean authorizationRequestBean, PropertyReader propertyReader)
    throws SSLConnectionException, IPSAPIException, Exception
  {
    AuthorizationMessageFormatter authMesssageFormatter = new AuthorizationMessageFormatter();
    if ((!"".equals(authorizationRequestBean.getMerchantLocationCountryCode())) && 
      (!MessageTypeID.ISO_NetworkManagementRequest.getMessageTypeID().equalsIgnoreCase(authorizationRequestBean.getMessageTypeIdentifier())))
    {
      PropertyReader.setMERCHANT_COUNTRY(authorizationRequestBean.getMerchantLocationCountryCode());
      AlphaRegionCode[] arrayOfAlphaRegionCode;
      int j = (arrayOfAlphaRegionCode = AlphaRegionCode.values()).length;
      for (int i = 0; i < j; i++)
      {
        AlphaRegionCode regionCodes = arrayOfAlphaRegionCode[i];
        if (authorizationRequestBean.getMerchantLocationCountryCode().equalsIgnoreCase(regionCodes.getCountryCode())) {
          PropertyReader.setREGION(regionCodes.getAlphaRegionCode());
        }
      }
    }
    else
    {
      PropertyReader.setMERCHANT_COUNTRY("840");
      PropertyReader.setREGION("USA");
    }
    String defaultSENumber = "1804000001";
    if ((!"".equals(authorizationRequestBean.getCardAcceptorIdCode())) && 
      (!MessageTypeID.ISO_NetworkManagementRequest.getMessageTypeID().equalsIgnoreCase(authorizationRequestBean.getMessageTypeIdentifier()))) {
      PropertyReader.setMERCHANT_NUMBER(authorizationRequestBean.getCardAcceptorIdCode());
    } else {
      PropertyReader.setMERCHANT_NUMBER(defaultSENumber);
    }
    if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
      Log.i("Entered into createAndSendAuthorizationRequest");
    }
    AuthorizationResponseBean authorizationResponseBean = new AuthorizationResponseBean();
    Connection connection = new Connection();
    if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
      Log.i("Before validating the ConnectionInfo");
    }
    Object connectionInfoErrorList = connection.validateConnectionInfo(propertyReader);
    if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
      Log.i("After validating the ConnectionInfoErrorList:" + connectionInfoErrorList);
    }
    if ((connectionInfoErrorList == null) || (((List)connectionInfoErrorList).isEmpty()))
    {
      if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
        Log.i("Before creating ISOFormatedRequestMessage");
      }
      String isoFormatedRequestMessage = authMesssageFormatter.createISORequest(authorizationRequestBean);
      if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
        Log.i("After converting ISOFormatedRequestMessage:");
      }
      if ((isoFormatedRequestMessage != null) && (!isoFormatedRequestMessage.equals("")))
      {
        if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
          Log.i("Before Sending ISOFormatedRequestMessage:");
        }
        String isoFormatedResposeMessage = connection.getConnectionAndSendRequest(isoFormatedRequestMessage, propertyReader);
        if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
          Log.i("After Receiving ISOFormatedResposeMessage" + Constants.NEW_LINE_CHAR);
        }
        if ((isoFormatedResposeMessage != null) && (!"".equals(isoFormatedResposeMessage)))
        {
          authorizationResponseBean = authMesssageFormatter.convertHexToResponseBean(isoFormatedResposeMessage);
          if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
            authorizationResponseBean.setISOFormattedResponse(isoFormatedResposeMessage);
          }
          String actionCodeDescription = authMesssageFormatter.getActionCodeDescription(authorizationResponseBean.getActionCode());
          String additionalResponseDataDesc = authMesssageFormatter.getAdditionalResponseDesc(authorizationResponseBean.getAdditionalResponseData());
          authorizationResponseBean.setActionCodeDescription(actionCodeDescription);
          authorizationResponseBean.setAdditionalResponseDataDesc(additionalResponseDataDesc);
        }
      }
      authorizationResponseBean.setISOFormattedRequest(isoFormatedRequestMessage);
    }
    else
    {
      authorizationResponseBean.setAuthErrorList((List)connectionInfoErrorList);
    }
    if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON"))
    {
      String strISOResponse = authMesssageFormatter.createMaskedHexString(authorizationResponseBean);
      if (ISOUtil.IsNullOrEmpty(strISOResponse)) {
        strISOResponse = "No response returned";
      }
      Log.i("After Converting RequestBean To Hex ");
      Log.i("Response Details\r\n----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n-                                       Response in Hex format                                                                                                                                      -\r\n---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n" + 
      
        strISOResponse + 
        "\r\n" + "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
      
      Log.i("Exiting from createAndSendAuthorizationRequest");
    }
    return authorizationResponseBean;
  }
  
  public synchronized AuthorizationResponseBean createAndSendAuthorizationRequest(AuthorizationRequestBean authorizationRequestBean, PropertyReader propertyReader, ExecutionType exeType)
    throws IPSAPIException, Exception
  {
    AuthorizationMessageFormatter authMesssageFormatter = new AuthorizationMessageFormatter();
    
    PropertyReader.setMERCHANT_COUNTRY("840");
    AlphaRegionCode[] arrayOfAlphaRegionCode;
    int j = (arrayOfAlphaRegionCode = AlphaRegionCode.values()).length;
    for (int i = 0; i < j; i++)
    {
      AlphaRegionCode regionCodes = arrayOfAlphaRegionCode[i];
      if ("840".equalsIgnoreCase(regionCodes.getCountryCode())) {
        PropertyReader.setREGION(regionCodes.getAlphaRegionCode());
      }
    }
    if (!"".equals(authorizationRequestBean.getCardAcceptorIdCode())) {
      PropertyReader.setMERCHANT_NUMBER(authorizationRequestBean.getCardAcceptorIdCode());
    }
    PropertyReader.setROUTING_INDICATOR("018");
    PropertyReader.setRequestType("DigGatewayReq");
    
    authorizationRequestBean.setMessageTypeIdentifier("1100");
    authorizationRequestBean.setProcessingCode("174800");
    authorizationRequestBean.setAmountTransaction("000000000000");
    authorizationRequestBean.setSystemTraceAuditNumber("222222");
    authorizationRequestBean.setLocalTransactionDateAndTime(getLocalDateTime("yyMMdd") + getLocalDateTime("hhmmss"));
    authorizationRequestBean.setMerchantLocationCountryCode("840");
    authorizationRequestBean.setPosDataCode("0000S0100000");
    authorizationRequestBean.setMessageReasonCode("1900");
    authorizationRequestBean.setCardAcceptorBusinessCode("5399");
    authorizationRequestBean.setTransactionCurrencyCode("840");
    if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
      Log.i("Entered into createAndSendAuthorizationRequest");
    }
    AuthorizationResponseBean authorizationResponseBean = new AuthorizationResponseBean();
    Connection connection = new Connection();
    if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
      Log.i("Before validating the ConnectionInfo");
    }
    Object connectionInfoErrorList = connection.validateConnectionInfo(propertyReader);
    if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
      Log.i("After validating the ConnectionInfoErrorList:" + connectionInfoErrorList);
    }
    if ((connectionInfoErrorList == null) || (((List)connectionInfoErrorList).isEmpty()))
    {
      if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
        Log.i("Before creating ISOFormatedRequestMessage");
      }
      String isoFormatedRequestMessage = authMesssageFormatter.createDigitalGatewayISORequest(authorizationRequestBean);
      if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
        Log.i("After converting ISOFormatedRequestMessage:");
      }
      if ((isoFormatedRequestMessage != null) && (!isoFormatedRequestMessage.equals("")))
      {
        if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
          Log.i("Before Sending ISOFormatedRequestMessage:");
        }
        String isoFormatedResposeMessage = connection.getConnectionAndSendRequest(isoFormatedRequestMessage, propertyReader);
        if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
          Log.i("After Receiving ISOFormatedResposeMessage" + Constants.NEW_LINE_CHAR);
        }
        if ((isoFormatedResposeMessage != null) && (!isoFormatedResposeMessage.equals("")))
        {
          authorizationResponseBean = authMesssageFormatter.convertHexToResponseBean(isoFormatedResposeMessage);
          if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
            authorizationResponseBean.setISOFormattedResponse(isoFormatedResposeMessage);
          }
          String actionCodeDescription = authMesssageFormatter.getDigitalGatewayActionCodeDescription(authorizationResponseBean.getActionCode());
          
          String additionalResponseDataDesc = authMesssageFormatter.getAdditionalResponseDesc(authorizationResponseBean.getAdditionalResponseData());
          authorizationResponseBean.setActionCodeDescription(actionCodeDescription);
          authorizationResponseBean.setAdditionalResponseDataDesc(additionalResponseDataDesc);
        }
      }
      authorizationResponseBean.setISOFormattedRequest(isoFormatedRequestMessage);
    }
    else
    {
      authorizationResponseBean.setAuthErrorList((List)connectionInfoErrorList);
    }
    if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON"))
    {
      String strISOResponse = authMesssageFormatter.createMaskedHexString(authorizationResponseBean);
      if (ISOUtil.IsNullOrEmpty(strISOResponse)) {
        strISOResponse = "No response returned";
      }
      Log.i("After ConvertingRequestBean To Hex ");
      Log.i("Response Details\r\n----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n-                                    Response in Hex format                                                                                                                                      -\r\n---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n" + 
      
        strISOResponse + 
        "\r\n" + "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
      
      Log.i("Exiting from createAndSendAuthorizationRequest");
    }
    PropertyReader.setROUTING_INDICATOR("000");
    PropertyReader.setRequestType("");
    return authorizationResponseBean;
  }
  
  public void flushRequestMemory(AuthorizationRequestBean authorizationRequestBean)
  {
    authorizationRequestBean.getPrimaryAccountNumber().delete(0, 15).append("");
    authorizationRequestBean = null;
    Runtime r = Runtime.getRuntime();
    r.gc();
  }
  
  public void flushResponseMemory(AuthorizationResponseBean authorizationResponseBean)
  {
    authorizationResponseBean.getPrimaryAccountNumber().delete(0, 15).append("");
    authorizationResponseBean = null;
    Runtime r = Runtime.getRuntime();
    r.gc();
  }
  
  private static String getLocalDateTime(String format)
  {
    Calendar cal = Calendar.getInstance();
    SimpleDateFormat sdf = new SimpleDateFormat(format);
    String value = sdf.format(cal.getTime());
    
    return value;
  }
}
