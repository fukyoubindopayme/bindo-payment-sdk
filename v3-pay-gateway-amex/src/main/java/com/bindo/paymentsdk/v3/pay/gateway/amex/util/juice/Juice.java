package com.bindo.paymentsdk.v3.pay.gateway.amex.util.juice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.AlgorithmParameters;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.KeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.Security;
import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Random;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import org.apache.log4j.Logger;

public class Juice {
    private static Provider provider;
    private static Logger logger;
    public static final String DEFAULT_ALGORITHM_NAME = "DESede/ECB/NoPadding";
    private Cipher alg;
    private String algName;
    private String algorithm;
    private String algKey;
    private SecretKey secretKey;
    private SecretKey secretGenKey;
    private boolean debugMode = true;
    private int padTo = 1;
    private JuiceP algP = null;
    private JuicePT aParams = null;
    private MessageDigest algMD;
    private byte[] iv = new byte[8];
    private boolean noPadding = false;
    private String pkClassName = null;
    private String pkClassPath = null;
    private Random r;

    /** @deprecated */
    public Juice() throws GeneralSecurityException {
        this.init("DESede/ECB/NoPadding", (String)null);
    }

    private Juice(JuicePT jpt) throws GeneralSecurityException {
        this.aParams = jpt;
        this.init("DESede/ECB/NoPadding", (String)null);
    }

    /** @deprecated */
    public Juice(String name) throws GeneralSecurityException {
        this.init(name, (String)null);
    }

    public Juice(String name, String key) throws GeneralSecurityException {
        this.init(name, key);
    }

    public Juice(String name, String keyPropName, String fileName) throws GeneralSecurityException, IOException {
        this.init(name, this.getEncryptedKey(keyPropName, fileName));
    }

    private byte[] addPadding(byte[] inData, int offset, int len) {
        if (this.isNoPadding()) {
            return inData;
        } else {
            byte[] bp = null;
            int padChars = this.padTo;
            int partial = (len + 1) % padChars;
            if (partial == 0) {
                padChars = 1;
            } else {
                padChars = padChars - partial + 1;
            }

            if (this.isDebugMode()) {
                this.log("addPadding(): Adding " + padChars + " padding characters.");
            }

            if (this.algP.isModeCBC()) {
                bp = new byte[len + padChars + 8];
                System.arraycopy(this.getNextIV(), 0, bp, 0, 8);
                bp[8] = Byte.parseByte(Integer.toString(padChars));
                System.arraycopy(inData, 0, bp, 9, len);
            } else {
                bp = new byte[len + padChars];
                bp[0] = Byte.parseByte(Integer.toString(padChars));
                System.arraycopy(inData, offset, bp, 1, len);
            }

            return bp;
        }
    }

    private String decodeHex(String strIn) throws KeyException {
        StringBuffer strOut = new StringBuffer("");
        int strLen = strIn.length() - 1;
        byte[] bytTmp = new byte[1];

        for(int intPos = 0; intPos < strLen; intPos += 2) {
            bytTmp[0] = Byte.decode("#" + strIn.substring(intPos, intPos + 2));
            strOut.append(new String(bytTmp));
        }

        return strOut.toString();
    }

    public String decrypt(byte[] ect) throws GeneralSecurityException {
        if (this.algP.isMessageDigest()) {
            throw new InvalidMethodCallException("Invalid Method call for a Message Digest algorithm");
        } else {
            byte[] dct = this.decryptIntoBytes(ect);
            String b = this.decodeHex(Hex.toString(dct));
            if (this.isDebugMode()) {
                this.log("decrypt(): Decrypted: " + b);
            }

            return b;
        }
    }

    public String decrypt(String inData) throws GeneralSecurityException {
        if (this.algP.isMessageDigest()) {
            throw new InvalidMethodCallException("Invalid Method call for a Message Digest algorithm");
        } else {
            return this.decrypt(Hex.fromString(inData));
        }
    }

    public byte[] decryptIntoBytes(byte[] ect) throws GeneralSecurityException {
        if (this.algP.isMessageDigest()) {
            throw new InvalidMethodCallException("Invalid Method call for a Message Digest algorithm");
        } else {
            if (this.isDebugMode()) {
                this.log("decryptIntoBytes(): Raw Input: " + Hex.toString(ect));
            }

            this.alg = Cipher.getInstance(this.algName);
            AlgorithmParameters param = null;
            if (this.algP.getAlgType() == 2) {
                if (this.secretGenKey != null) {
                    this.alg.init(1, this.secretGenKey);
                } else {
                    this.alg.init(1, this.secretKey);
                }

                param = this.alg.getParameters();
            }

            if (this.secretGenKey != null) {
                if (param != null) {
                    this.alg.init(2, this.secretGenKey, param);
                } else {
                    this.alg.init(2, this.secretGenKey);
                }
            } else if (param != null) {
                this.alg.init(2, this.secretKey, param);
            } else {
                this.alg.init(2, this.secretKey);
            }

            byte[] dct = this.removePadding(this.alg.doFinal(ect));
            if (this.isDebugMode()) {
                this.log("decryptIntoBytes(): Hex Value: " + Hex.toString(dct));
            }

            return dct;
        }
    }

    public byte[] decryptIntoBytes(String inData) throws GeneralSecurityException {
        if (this.algP.isMessageDigest()) {
            throw new InvalidMethodCallException("Invalid Method call for a message digest algorithm");
        } else {
            return this.decryptIntoBytes(Hex.fromString(inData));
        }
    }

    public void decryptStream(InputStream in, OutputStream out) throws IOException, GeneralSecurityException {
        if (this.algP.isMessageDigest()) {
            throw new InvalidMethodCallException("Invalid Method call for a Message Digest algorithm");
        } else {
            int cnt = 0;
            Object var4 = null;

            while(cnt >= 0) {
                byte[] buffer = new byte[4];
                cnt = in.read(buffer);
                if (cnt == 4) {
                    String sxLen = Hex.toString(buffer);
                    int iLen = Integer.parseInt(sxLen, 16);
                    if (iLen > 32767) {
                        throw new IllegalBlockSizeException("Juice: Block size found on the data to be decrypted " + iLen + " exceeds maximum allowable value on Juice" + 32767 + "throwing exception assuming the data is corrupted");
                    }

                    buffer = new byte[iLen];
                    cnt = in.read(buffer);
                    if (cnt < iLen) {
                        throw new IOException("Input Stream inconsistent, number of bytes read: " + cnt + " is less than expected: " + iLen);
                    }
                }

                if (cnt > 0) {
                    byte[] eBuf = this.decryptIntoBytes(buffer);
                    out.write(eBuf);
                    out.flush();
                    cnt = 0;
                }
            }

        }
    }

    public String digest(byte[] dct) throws KeyException {
        if (!this.algP.isMessageDigest()) {
            throw new InvalidMethodCallException("Invalid Method call. This Juice instance is not initialzed for Digest");
        } else {
            byte[] ect = this.digestIntoBytes(dct);
            return Hex.toString(ect);
        }
    }

    public String digest(String inData) throws KeyException {
        if (!this.algP.isMessageDigest()) {
            throw new InvalidMethodCallException("Invalid Method call. This Juice instance is not initialzed for Digest");
        } else {
            return this.digest(inData.getBytes());
        }
    }

    public byte[] digestIntoBytes(byte[] dct) throws KeyException {
        if (!this.algP.isMessageDigest()) {
            throw new InvalidMethodCallException("Invalid Method call. This Juice instance is not initialzed for Digest");
        } else {
            if (this.isDebugMode()) {
                this.log("digestIntoBytes(): Hex Value: " + Hex.toString(dct));
            }

            this.algMD.reset();
            byte[] ect = this.algMD.digest(dct);
            if (this.isDebugMode()) {
                this.log("digestIntoBytes(): Encrypted: " + Hex.toString(ect));
            }

            return ect;
        }
    }

    public byte[] digestIntoBytes(String inData) throws KeyException {
        if (!this.algP.isMessageDigest()) {
            throw new InvalidMethodCallException("Invalid Method call. This Juice instance is not initialzed for Digest");
        } else {
            return this.digestIntoBytes(inData.getBytes());
        }
    }

    public String encrypt(byte[] dct) throws GeneralSecurityException {
        if (this.algP.isMessageDigest()) {
            throw new InvalidMethodCallException("Invalid Method call for a message digest algorithm");
        } else {
            return this.encrypt(dct, 0, dct.length);
        }
    }

    public String encrypt(byte[] dct, int offset, int len) throws GeneralSecurityException {
        if (this.algP.isMessageDigest()) {
            throw new InvalidMethodCallException("Invalid Method call for a message digest algorithm");
        } else {
            byte[] ect = this.encryptIntoBytes(dct, offset, len);
            return Hex.toString(ect);
        }
    }

    public String encrypt(String inData) throws GeneralSecurityException {
        if (this.algP.isMessageDigest()) {
            throw new InvalidMethodCallException("Invalid Method call for a message digest algorithm");
        } else {
            return this.encrypt(inData.getBytes());
        }
    }

    public byte[] encryptIntoBytes(byte[] dct) throws GeneralSecurityException {
        if (this.algP.isMessageDigest()) {
            throw new InvalidMethodCallException("Invalid Method call for a message digest algorithm");
        } else {
            return this.encryptIntoBytes(dct, 0, dct.length);
        }
    }

    public byte[] encryptIntoBytes(byte[] dct, int offset, int len) throws GeneralSecurityException {
        if (this.algP.isMessageDigest()) {
            throw new InvalidMethodCallException("Invalid Method call for a message digest algorithm");
        } else if (len > 32767) {
            throw new IllegalBlockSizeException("Juice Block size requested" + len + " exceeds maximum allowable value " + 32767);
        } else {
            if (this.isDebugMode()) {
                this.log("encryptIntoBytes(): Hex Value: " + Hex.toString(this.addPadding(dct, offset, len)));
            }

            if (this.secretGenKey != null) {
                this.alg.init(1, this.secretGenKey);
            } else {
                this.alg.init(1, this.secretKey);
            }

            byte[] ect = this.alg.doFinal(this.addPadding(dct, offset, len));
            if (this.isDebugMode()) {
                this.log("encryptIntoBytes(): Encrypted: " + Hex.toString(ect));
            }

            return ect;
        }
    }

    public byte[] encryptIntoBytes(String inData) throws GeneralSecurityException {
        if (this.algP.isMessageDigest()) {
            throw new InvalidMethodCallException("Invalid Method call for a message digest algorithm");
        } else {
            return this.encryptIntoBytes(inData.getBytes());
        }
    }

    public void encryptStream(int len, InputStream in, OutputStream out) throws IOException, GeneralSecurityException {
        if (this.algP.isMessageDigest()) {
            throw new InvalidMethodCallException("Invalid Method call for a Message Digest algorithm");
        } else if (len > 32767) {
            throw new IllegalBlockSizeException("Juice Block size requested" + len + " exceeds maximum allowable value " + 32767);
        } else {
            int cnt = 0;
            byte[] buffer = new byte[len];
            String dataLen = null;

            while(cnt >= 0) {
                cnt = in.read(buffer, 0, buffer.length);
                if (cnt > 0) {
                    byte[] eBuf = this.encryptIntoBytes(buffer, 0, cnt);
                    dataLen = Hex.intToString(eBuf.length);
                    out.write(Hex.fromString(dataLen));
                    out.write(eBuf);
                    out.flush();
                    cnt = 0;
                }
            }

        }
    }

    public void encryptStream(InputStream in, OutputStream out) throws IOException, GeneralSecurityException {
        if (this.algP.isMessageDigest()) {
            throw new InvalidMethodCallException("Invalid Method call for a Message Digest algorithm");
        } else {
            int len = 1023;
            this.encryptStream(len, in, out);
        }
    }

    public String generateKey() throws NoSuchAlgorithmException, KeyException {
        if (this.algP.isMessageDigest()) {
            throw new KeyException("MessageDigest algorithm does not support key generation");
        } else if (this.algP.isModeCBC()) {
            throw new KeyException("CBC mode algorithm does not support key generation");
        } else {
            KeyGenerator keygen = KeyGenerator.getInstance(this.algorithm);
            this.secretGenKey = keygen.generateKey();
            return this.getEncodedKey();
        }
    }

    protected String getAlgPKey() {
        return this.algP.getKey();
    }

    public String[] getEnabledAlgorithms() {
        if (this.isDebugMode()) {
            this.log("getEnabledAlgorithms(): Params contains " + this.aParams.size() + " keys.");
        }

        String[] pKeys = new String[this.aParams.size()];
        int i = 0;

        for(Enumeration e = this.aParams.keys(); e.hasMoreElements(); ++i) {
            String thisKey = (String)e.nextElement();
            if (this.isDebugMode()) {
                this.log("getEnabledAlgorithms(): Key[" + i + "]=" + thisKey + ", status=");
            }

            pKeys[i] = thisKey;
        }

        return pKeys;
    }

    public String getEncodedKey() {
        return this.secretGenKey != null ? Hex.toString(this.secretGenKey.getEncoded()) : null;
    }

    private String getEncryptedKey(String keyPropName, String fn) throws GeneralSecurityException, IOException {
        Properties p = this.readProperties(fn);
        String eKey = (String)p.get(keyPropName);
        if (eKey == null) {
            throw new KeyException();
        } else {
            Juice cWrap = new Juice();
            String key = cWrap.decrypt(eKey);
            if (key == null) {
                throw new KeyException();
            } else {
                if (this.isDebugMode()) {
                    this.log("getEncryptedKey(): key=" + key + ", encrypted key=" + eKey);
                }

                return key;
            }
        }
    }

    public String getInfo() {
        return "Juice - JAVA UTILITY IMPLEMENTING CRYPTOGRAPHIC EXTENSIONS. \n American Express Java Reusable component. \n Version 2.x";
    }

    public int getKeyLength() {
        return this.algP.getKeyLength();
    }

    public String getName() {
        return this.algName;
    }

    protected byte[] getNextIV() {
        this.r.nextBytes(this.iv);
        return this.iv;
    }

    protected String getPkClassName() {
        return this.pkClassName;
    }

    protected String getPkClassPath() {
        return this.pkClassPath;
    }

    private void getProperties() {
        String propValue = "com.bindo.paymentsdk.v3.pay.gateway.amex.util.juice.JuicePKP";
        if (propValue != null) {
            this.setPkClassName(propValue);
        }

    }

    public String[] getSupportedAlgorithms() {
        if (this.isDebugMode()) {
            this.log("getSupportedAlgorithms(): Params contains " + this.aParams.size() + " keys.");
        }

        String[] pKeys = new String[this.aParams.size()];
        int i = 0;

        for(Enumeration e = this.aParams.keys(); e.hasMoreElements(); ++i) {
            String thisKey = (String)e.nextElement();
            if (this.isDebugMode()) {
                this.log("getSupportedAlgorithms(): Key[" + i + "]=" + thisKey);
            }

            pKeys[i] = thisKey;
        }

        return pKeys;
    }

    private final void init(String name, String key) throws GeneralSecurityException {
        if (provider != null) {
            Security.addProvider(provider);
        }

        if (name.equalsIgnoreCase("AES")) {
            this.algName = "AES/ECB/NoPadding";
        } else if (name.equalsIgnoreCase("DES-EDE3")) {
            this.algName = "DESede/ECB/NoPadding";
        } else {
            this.algName = name;
        }

        int i = this.algName.indexOf("/");
        if (i != -1) {
            this.algorithm = this.algName.substring(0, i);
        } else {
            this.algorithm = this.algName;
        }

        this.getProperties();
        if (this.aParams == null) {
            this.aParams = new JuicePT(this.pkClassName);
        }

        this.algP = (JuiceP)this.aParams.get(name);
        this.isAlgorithmAllowed();
        if (this.algP.isBlockCipher()) {
            if (key == null) {
                this.algKey = this.algP.getKey();
            } else {
                this.algKey = key;
            }

            this.padTo = this.algP.getPadTo();
            if (provider != null) {
                this.alg = Cipher.getInstance(this.algName, provider);
            } else {
                this.alg = Cipher.getInstance(this.algName);
            }

            this.secretKey = new SecretKeySpec(Hex.fromString(this.algKey), this.algorithm);
            this.secretGenKey = null;
            if (this.algP.isModeCBC()) {
                this.seedIV();
            }
        } else {
            if (!this.algP.isMessageDigest()) {
                throw new NoSuchAlgorithmException("Algorithm type is not supported");
            }

            if (provider != null) {
                this.algMD = MessageDigest.getInstance(this.algName, provider);
            } else {
                this.algMD = MessageDigest.getInstance(this.algName);
            }

            this.algMD.reset();
        }

    }

    protected final boolean isAlgorithmAllowed() throws NoSuchAlgorithmException {
        if (this.algP != null && this.algP.isEnabled()) {
            return true;
        } else {
            throw new NoSuchAlgorithmException("Algorithm " + this.getName() + " is not Supported in Juice v2.0");
        }
    }

    public boolean isBlockCipher() {
        return this.algP.isBlockCipher();
    }

    private boolean isDebugMode() {
        return this.debugMode;
    }

    public boolean isKeyWeak(byte[] sKey) {
        return this.algName.equals("DESede/ECB/NoPadding") ? this.isWeak(sKey) : false;
    }

    public boolean isMessageDigest() {
        return this.algP.isMessageDigest();
    }

    public boolean isModeCBC() {
        return this.algP.isModeCBC();
    }

    public boolean isNoPadding() {
        return this.noPadding;
    }

    protected boolean isWeak(byte[] key) {
        if (!this.isWeak(key, 0) && !this.isWeak(key, 8) && !this.isWeak(key, 16)) {
            long k1 = ((long)key[0] & 254L) << 56 | ((long)key[1] & 254L) << 48 | ((long)key[2] & 254L) << 40 | ((long)key[3] & 254L) << 32 | ((long)key[4] & 254L) << 24 | ((long)key[5] & 254L) << 16 | ((long)key[6] & 254L) << 8 | (long)key[7] & 254L;
            long k2 = ((long)key[8] & 254L) << 56 | ((long)key[9] & 254L) << 48 | ((long)key[10] & 254L) << 40 | ((long)key[11] & 254L) << 32 | ((long)key[12] & 254L) << 24 | ((long)key[13] & 254L) << 16 | ((long)key[14] & 254L) << 8 | (long)key[15] & 254L;
            long k3 = ((long)key[16] & 254L) << 56 | ((long)key[17] & 254L) << 48 | ((long)key[18] & 254L) << 40 | ((long)key[19] & 254L) << 32 | ((long)key[20] & 254L) << 24 | ((long)key[21] & 254L) << 16 | ((long)key[22] & 254L) << 8 | (long)key[23] & 254L;
            return k1 == k2 || k2 == k3 || k1 == k3;
        } else {
            return true;
        }
    }

    protected boolean isWeak(byte[] key, int offset) {
        int a = (key[offset] & 254) << 8 | key[offset + 1] & 254;
        int b = (key[offset + 2] & 254) << 8 | key[offset + 3] & 254;
        int c = (key[offset + 4] & 254) << 8 | key[offset + 5] & 254;
        int d = (key[offset + 6] & 254) << 8 | key[offset + 7] & 254;
        return (a == 0 || a == 65278) && (b == 0 || b == 65278) && (c == 0 || c == 65278) && (d == 0 || d == 65278);
    }

    private void log(String logData) {
        logger.debug(logData);
    }

    public void mergeKeys() throws InvalidKeyException {
        byte[] bytekey1 = Hex.fromString(this.algKey);
        byte[] bytekey2 = Hex.fromString(this.algP.getKey());

        for(int i = 0; i < bytekey1.length; ++i) {
            bytekey1[i] ^= bytekey2[i];
        }

        if (this.isDebugMode()) {
            this.log("mergeKeys(): Currkey=" + this.algKey + ", Defkey=" + this.algP.getKey() + ", Newkey=" + Hex.toString(bytekey1));
        }

        if (this.isKeyWeak(bytekey1)) {
            throw new InvalidKeyException("Detected weak Triple-DES key");
        } else {
            this.algKey = Hex.toString(bytekey1);
            this.secretKey = new SecretKeySpec(Hex.fromString(this.algKey), this.algorithm);
        }
    }

    public void mergeKeys(String key1) throws InvalidKeyException {
        byte[] bytekey1 = Hex.fromString(key1);
        byte[] bytekey2 = Hex.fromString(this.algKey);

        for(int i = 0; i < bytekey1.length; ++i) {
            bytekey1[i] ^= bytekey2[i];
        }

        if (this.isDebugMode()) {
            this.log("mergeKeys(): Userkey=" + key1 + ", Currkey=" + this.algKey + ", Newkey=" + Hex.toString(bytekey1));
        }

        if (this.isKeyWeak(bytekey1)) {
            throw new InvalidKeyException("Detected weak Triple-DES key");
        } else {
            this.algKey = Hex.toString(bytekey1);
            this.secretKey = new SecretKeySpec(Hex.fromString(this.algKey), this.algorithm);
        }
    }

    public void mergeKeys(String key1, String key2) throws InvalidKeyException {
        byte[] bytekey1 = Hex.fromString(key1);
        byte[] bytekey2 = Hex.fromString(key2);

        for(int i = 0; i < bytekey1.length; ++i) {
            bytekey1[i] ^= bytekey2[i];
        }

        if (this.isDebugMode()) {
            this.log("mergeKeys(): Userkey1=" + key1 + ", Userkey2=" + key2 + ", Newkey=" + Hex.toString(bytekey1));
        }

        if (this.isKeyWeak(bytekey1)) {
            throw new InvalidKeyException("Detected weak Triple-DES key");
        } else {
            this.algKey = Hex.toString(bytekey1);
            this.secretKey = new SecretKeySpec(Hex.fromString(this.algKey), this.algorithm);
        }
    }

    public boolean needPadding() {
        return this.padTo > 1;
    }

    private Properties readProperties(String fileName) throws IOException {
        InputStream is = null;
        Properties props = new Properties();

        try {
            try {
                is = Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName);
            } catch (Throwable var13) {
                ;
            }

            if (is == null) {
                is = new FileInputStream(new File(fileName));
            }

            props.load((InputStream)is);
        } catch (IOException var14) {
            if (this.isDebugMode()) {
                this.log("readProperties(" + fileName + "): IOException=" + var14.getMessage());
            }

            throw new IOException("readProperties(" + fileName + ") failed - " + var14.getMessage());
        } finally {
            try {
                if (is != null) {
                    ((InputStream)is).close();
                }
            } catch (IOException var12) {
                ;
            }

        }

        return props;
    }

    public byte[] recrypt(byte[] ect, Juice cWrap) throws GeneralSecurityException {
        byte[] dct = this.decryptIntoBytes(ect);
        return cWrap.encryptIntoBytes(dct, 0, dct.length);
    }

    public String recrypt(String inData, Juice cWrap) throws GeneralSecurityException {
        String dData = this.decrypt(Hex.fromString(inData));
        return cWrap.encrypt(dData.getBytes());
    }

    public void recryptStream(InputStream in, OutputStream out, Juice cWrap) throws IOException, GeneralSecurityException {
        int cnt = 0;
        byte[] buffer = null;
        String dataLen = null;

        while(cnt >= 0) {
            buffer = new byte[4];
            cnt = in.read(buffer);
            if (cnt == 4) {
                String sxLen = Hex.toString(buffer);
                int iLen = Integer.parseInt(sxLen, 16);
                buffer = new byte[iLen];
                cnt = in.read(buffer);
                if (cnt < iLen) {
                    throw new IOException("Input Stream inconsistent, number of bytes read: " + cnt + " is less than expected: " + iLen);
                }
            }

            if (cnt > 0) {
                byte[] dBuf = this.decryptIntoBytes(buffer);
                byte[] eBuf = cWrap.encryptIntoBytes(dBuf);
                dataLen = Hex.intToString(eBuf.length);
                out.write(Hex.fromString(dataLen));
                out.write(eBuf);
                out.flush();
                cnt = 0;
            }
        }

    }

    private byte[] removePadding(byte[] inData) {
        if (this.isNoPadding()) {
            return inData;
        } else {
            byte[] bp = null;
            int dataLength = 0;
            int padLength = 0;
            if (this.isDebugMode()) {
                this.log("removePadding(): Hex Value with padding: " + Hex.toString(inData));
            }

            if (this.algP.isModeCBC()) {
                padLength = inData[8];
                dataLength = inData.length - padLength - 8;
                bp = new byte[dataLength];
                System.arraycopy(inData, 9, bp, 0, dataLength);
            } else {
                padLength = inData[0];
                dataLength = inData.length - padLength;
                bp = new byte[dataLength];
                System.arraycopy(inData, 1, bp, 0, dataLength);
            }

            if (this.isDebugMode()) {
                this.log("removePadding(): Removed " + padLength + " padding characters.");
            }

            return bp;
        }
    }

    private void seedIV() {
        long ts = (new Date()).getTime();
        this.r = new Random(ts);
    }

    public void setNewKey(String newKey) {
        if (this.isDebugMode()) {
            this.log("setNewKey(): New key=" + newKey);
        }

        this.algKey = newKey;
        this.secretKey = new SecretKeySpec(Hex.fromString(this.algKey), this.algorithm);
    }

    public void setNoPadding(boolean newNoPadding) {
        this.noPadding = newNoPadding;
    }

    protected void setPkClassName(String newPkClassName) {
        this.pkClassName = newPkClassName;
    }

    protected void setPkClassPath(String newPkClassPath) {
        this.pkClassPath = newPkClassPath;
    }

    public void storeEncryptedKey(String skey, String keyname, String fn) throws GeneralSecurityException, IOException {
        if (this.isDebugMode()) {
            this.log("storeEncryptedKey(): " + keyname + "=" + skey + " to be stored in " + fn);
        }

        Properties pOut = new Properties();
        FileOutputStream out = null;

        try {
            Juice cw = new Juice(this.aParams);
            String eKey = cw.encrypt(skey);
            if (this.isDebugMode()) {
                this.log("storeEncryptedKey(): " + keyname + "=" + eKey + " encrypted");
            }

            cw = null;

            try {
                Properties p = this.readProperties(fn);
                Enumeration en = p.keys();

                while(en.hasMoreElements()) {
                    String key = (String)en.nextElement();
                    String val = (String)p.get(key);
                    if (!key.equals(keyname)) {
                        pOut.put(key, val);
                    }
                }
            } catch (Exception var20) {
                System.out.println("Key file " + fn + "does not exist. Creating new one ");
            }

            if (eKey != null) {
                pOut.put(keyname, eKey);
            }

            out = new FileOutputStream(new File(fn));
            pOut.save(out, "Encryption key updated.");
            if (this.isDebugMode()) {
                this.log("storeEncryptedKey(): " + keyname + "=" + eKey + " written to " + fn);
            }
        } catch (IOException var21) {
            if (this.isDebugMode()) {
                this.log("storeEncryptedKey(): IOException=" + var21.getMessage());
            }

            throw new IOException("storeEncryptedKey failed ... " + var21.getMessage());
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException var19) {
                ;
            }

        }

    }

    static {
        logger = Logger.getLogger(Juice.class);
    }
}
