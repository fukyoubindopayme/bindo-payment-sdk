package com.bindo.paymentsdk.v3.pay.gateway.amex.util.juice;

public class InvalidMethodCallException extends RuntimeException {
    public InvalidMethodCallException() {
    }

    public InvalidMethodCallException(String reason) {
        super(reason);
    }
}
