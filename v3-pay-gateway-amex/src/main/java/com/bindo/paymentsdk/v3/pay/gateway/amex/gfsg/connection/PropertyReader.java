package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.connection;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.CommonValidator;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Logger;

public class PropertyReader {
    private static      String merchantNumber                 = "5021011432";
    public static final String USER_AGENT                     = "Terminal1";
    private static      String debuggerFlag                   = "ON";
    private static      String origin                         = "TEST-SV-SYSTEMS";
    private static      String submissionType                 = "ISO";
    private static      String region                         = "USA";
    private static      String methodType                     = "POST";
    private static      String routingIndicator               = "015";
    private static      String merchantCountry                = "840";
    private static      String ftpPort                        = "22";
    private static      String ftpUserID                      = "GWSJAVARTST";
    private static      String ftpPassword                    = "amex123";
    private static      String ftpURL                         = "148.171.39.41";
    private static      String ftpLocalUploadFilePath         = "";
    private static      String ftpLocalDownloadFilePath       = "";
    private static      String ftpUploadFileName              = "";
    private static      String ftpDownloadAcknowledgeFileName = "TST002.GWS007.A";
    private static      String ftpDownloadPrintedFileName     = "TST002.GWS007.C";
    private static      String ftpDownloadRawFileName         = "TST002.GWS007.R";
    private static      String propsLocation                  = "./settlement.properties";
    private static      String xmlProxyPort                   = "";
    private static String xmlProxyUserid;
    private static String xmlProxyPassword;
    private static String xmlhttpsUrl            = "https://qwww318.americanexpress.com/IPPayments/inter/CardAuthorization.do";
    private static String xmlProxy               = "";
    private static String xmlProxyAddress        = "";
    private static String xmlProxyAuthentication = "Basic";
    private static String fileStorage;
    private static String fileType        = "VARIABLE";
    private static String acknowledgeType = "TEXT";
    private static int    httpsPort;
    private static String userIdentity;
    private static String publicKeyFilePath;
    Properties     clientprops    = new Properties();
    PropertyReader propertyHolder = null;
    public static String httpsTimeout                     = "";
    public static String readTimeout                      = "";
    public static String xmlBATCHADMINREQUESTMESSAGETYPE  = "GFSG XML BAR";
    public static String xmlDATACAPTUREREQUESTMESSAGETYPE = "GFSG XML DCR";
    private static String publicKey;
    private static final Logger LOGGER = Logger.getLogger(PropertyReader.class);

    public PropertyReader() {
    }

    public static String getXmlBATCHADMINREQUESTMESSAGETYPE() {
        return xmlBATCHADMINREQUESTMESSAGETYPE;
    }

    public static void setXmlBATCHADMINREQUESTMESSAGETYPE(String xmlBATCHADMINREQUESTMESSAGETYPE) {
        PropertyReader.xmlBATCHADMINREQUESTMESSAGETYPE = xmlBATCHADMINREQUESTMESSAGETYPE;
    }

    public static String getXmlDATACAPTUREREQUESTMESSAGETYPE() {
        return xmlDATACAPTUREREQUESTMESSAGETYPE;
    }

    public static void setXmlDATACAPTUREREQUESTMESSAGETYPE(String xmlDATACAPTUREREQUESTMESSAGETYPE) {
        PropertyReader.xmlDATACAPTUREREQUESTMESSAGETYPE = xmlDATACAPTUREREQUESTMESSAGETYPE;
    }

    public static String getReadTimeout() {
        return readTimeout;
    }

    public static void setReadTimeout(String readTimeout) {
        PropertyReader.readTimeout = readTimeout;
    }

    public static String getXmlProxy() {
        return xmlProxy;
    }

    public static void setXmlProxy(String xmlProxy) {
        PropertyReader.xmlProxy = xmlProxy;
    }

    private PropertyReader loadISOProperties() {
        this.propertyHolder = new PropertyReader();
        setFtpPort(this.clientprops.getProperty("FTP_PORT"));
        setFtpUserID(this.clientprops.getProperty("FTP_USERID"));
        setFtpPassword(this.clientprops.getProperty("FTP_PASSWORD"));
        setFtpURL(this.clientprops.getProperty("FTP_URL"));
        setXmlProxy(this.clientprops.getProperty("PROXY"));
        setXmlProxyPort(this.clientprops.getProperty("PROXY_PORT"));
        setXmlProxyAddress(this.clientprops.getProperty("PROXY_ADDRESS"));
        if (!CommonValidator.isNullOrEmpty(this.clientprops.getProperty("FTP_UPLOADED_LOCAL_FILE_PATH"))) {
            setFtpLocalUploadFilePath(this.clientprops.getProperty("FTP_UPLOADED_LOCAL_FILE_PATH"));
        }

        if (!CommonValidator.isNullOrEmpty(this.clientprops.getProperty("FTP_DOWNLOADED_LOCAL_FILE_PATH"))) {
            setFtpLocalDownloadFilePath(this.clientprops.getProperty("FTP_DOWNLOADED_LOCAL_FILE_PATH"));
        }

        setFtpUploadFileName(this.clientprops.getProperty("FTP_UPLOAD_FILE_NAME"));
        setAcknowledgeType(this.clientprops.getProperty("ACKNOWLEDGE_TYPE"));
        setFtpDownloadAcknowledgeFileName(this.clientprops.getProperty("FTP_DOWNLOAD_ACKNOWLEDGE_FILE_NAME"));
        setFtpDownloadPrintedFileName(this.clientprops.getProperty("FTP_DOWNLOAD_PRINTED_FILE_NAME"));
        setFtpDownloadRawFileName(this.clientprops.getProperty("FTP_DOWNLOAD_RAW_FILE_NAME"));
        if (!CommonValidator.isNullOrEmpty(this.clientprops.getProperty("FILE_TYPE"))) {
            setFileType(this.clientprops.getProperty("FILE_TYPE"));
        }

        setFileStorage(this.clientprops.getProperty("FILE_UPLOAD_BACKUP"));
        setUserIdentity(this.clientprops.getProperty("USER_IDENTITY"));
        setPublicKeyFilePath(this.clientprops.getProperty("PUBLIC_KEY_FILE_PATH"));
        return this;
    }

    public PropertyReader loadPropertyFile(String propertyfilepath) {
        boolean debugFlag = false;
        if (getDebuggerFlag().equalsIgnoreCase("ON")) {
            debugFlag = true;
            LOGGER.info("Entered into loadPropertyFile()");
        }

        URL url = null;

        try {
            url = PropertyReader.class.getResource(propertyfilepath);

            try {
                this.clientprops.load(url.openStream());
            } catch (Exception var14) {
                if (debugFlag) {
                    LOGGER.fatal("ERROR_Property_Reader_FILE", var14);
                }
            }

            if ("ISO".equals(this.clientprops.getProperty("SUBMISSION_TYPE"))) {
                this.propertyHolder = this.loadISOProperties();
            } else if (this.clientprops.getProperty("SUBMISSION_TYPE").equals("XML")) {
                this.propertyHolder = this.loadXMLProperties();
            }

            setDebuggerFlag(this.clientprops.getProperty("DEBUGGER_FLAG"));
        } catch (Exception var15) {
            if (debugFlag) {
                LOGGER.fatal("ERROR_Property_Reader_FILE", var15);
            }
        } finally {
            try {
                url.openStream().close();
            } catch (IOException var13) {
                if (debugFlag) {
                    LOGGER.fatal("ERROR_READ_WRITE_FILE", var13);
                }
            }

        }

        return this.propertyHolder;
    }

    public PropertyReader getPropertyValuesDataBase() {
        boolean debugFlag = false;
        Connection connection = null;
        ResultSet resultSet = null;

        try {
            int proxyid = 2;
            Class.forName("com.imaginary.sql.msql.MsqlDriver");
            String url = "jdbc:msql://www.myserver.com:1114/proxy_mgr";
            connection = DriverManager.getConnection(url, "user1", "password");
            String selectStatement = "SELECT * FROM Proxy_Deatils WHERE proxy_id = ?";
            PreparedStatement doSelect = connection.prepareStatement(selectStatement);
            doSelect.setInt(1, proxyid);
            resultSet = doSelect.executeQuery();

            while (resultSet.next()) {
                this.clientprops.put("PROXY_ADDRESS", resultSet.getString(1));
                this.clientprops.put("PROXY_PORT", resultSet.getString(2));
                this.clientprops.put("PROXY_USER_ID", resultSet.getString(3));
                this.clientprops.put("PROXY_PASSWORD", resultSet.getString(4));
            }

            this.propertyHolder = this.loadISOProperties();
        } catch (Exception var16) {
            if (debugFlag) {
                LOGGER.fatal("ERROR_Property_Reader_FILE", var16);
            }
        } finally {
            try {
                resultSet.close();
                connection.close();
            } catch (SQLException var15) {
                if (debugFlag) {
                    LOGGER.fatal("ERROR_Property_Reader_FILE", var15);
                }
            }

        }

        return this.propertyHolder;
    }

    public PropertyReader loadPropFileFromSDkJar() throws Exception {
        boolean debugFlag = false;
        FileInputStream fis = null;

        try {
            fis = new FileInputStream(propsLocation);
            this.clientprops.load(fis);
            this.propertyHolder = this.loadISOProperties();
        } catch (Exception var7) {
            if (debugFlag) {
                LOGGER.fatal("ERROR_Property_Reader_FILE", var7);
            }
        } finally {
            fis.close();
        }

        return this.propertyHolder;
    }

    private PropertyReader loadXMLProperties() {
        this.propertyHolder = new PropertyReader();
        setXmlProxyPort(this.clientprops.getProperty("PROXY_PORT"));
        setXmlProxyUserid(this.clientprops.getProperty("PROXY_USER_ID"));
        setXmlProxyPassword(this.clientprops.getProperty("PROXY_PASSWORD"));
        setXmlhttpsUrl(this.clientprops.getProperty("HTTPS_URL"));
        setXmlProxyAddress(this.clientprops.getProperty("PROXY_ADDRESS"));
        setXmlProxyAuthentication(this.clientprops.getProperty("PROXY_AUTHENTICATION"));
        setXmlProxy(this.clientprops.getProperty("PROXY"));
        setMerchantNumber(this.clientprops.getProperty("MERCHANT_NUMBER"));
        setOrigin(this.clientprops.getProperty("ORIGIN"));
        setRegion(this.clientprops.getProperty("REGION"));
        setMerchantCountry(this.clientprops.getProperty("MERCHANT_COUNTRY"));
        setHttpsPort(Integer.valueOf(this.clientprops.getProperty("PORT")));
        setHttpsTimeout(this.clientprops.getProperty("HTTPS_TIMEOUT"));
        setReadTimeout(this.clientprops.getProperty("READ_TIMEOUT"));
        if (!CommonValidator.isNullOrEmpty(this.clientprops.getProperty("XML_BATCH_ADMIN_REQUEST_MESSAGE_TYPE"))) {
            setXmlBATCHADMINREQUESTMESSAGETYPE(this.clientprops.getProperty("XML_BATCH_ADMIN_REQUEST_MESSAGE_TYPE"));
        }

        if (!CommonValidator.isNullOrEmpty(this.clientprops.getProperty("XML_DATA_CAPTURE_REQUEST_MESSAGE_TYPE"))) {
            setXmlDATACAPTUREREQUESTMESSAGETYPE(this.clientprops.getProperty("XML_DATA_CAPTURE_REQUEST_MESSAGE_TYPE"));
        }

        return this.propertyHolder;
    }

    public static String getMerchantNumber() {
        return merchantNumber;
    }

    public static void setMerchantNumber(String merchantNumber) {
        PropertyReader.merchantNumber = merchantNumber;
    }

    public static String getDebuggerFlag() {
        return debuggerFlag;
    }

    public static void setDebuggerFlag(String debuggerFlag) {
        PropertyReader.debuggerFlag = debuggerFlag;
    }

    public static String getOrigin() {
        return origin;
    }

    public static void setOrigin(String origin) {
        PropertyReader.origin = origin;
    }

    public static String getRegion() {
        return region;
    }

    public static void setRegion(String region) {
        PropertyReader.region = region;
    }

    public static String getMethodType() {
        return methodType;
    }

    public static void setMethodType(String methodType) {
        PropertyReader.methodType = methodType;
    }

    public static String getRoutingIndicator() {
        return routingIndicator;
    }

    public static void setRoutingIndicator(String routingIndicator) {
        PropertyReader.routingIndicator = routingIndicator;
    }

    public static String getMerchantCountry() {
        return merchantCountry;
    }

    public static void setMerchantCountry(String merchantCountry) {
        PropertyReader.merchantCountry = merchantCountry;
    }

    public static String getFtpPort() {
        return ftpPort;
    }

    public static void setFtpPort(String ftpPort) {
        PropertyReader.ftpPort = ftpPort;
    }

    public static String getFtpUserID() {
        return ftpUserID;
    }

    public static void setFtpUserID(String ftpUserID) {
        PropertyReader.ftpUserID = ftpUserID;
    }

    public static String getFtpPassword() {
        return ftpPassword;
    }

    public static void setFtpPassword(String ftpPassword) {
        PropertyReader.ftpPassword = ftpPassword;
    }

    public static String getFtpURL() {
        return ftpURL;
    }

    public static void setFtpURL(String ftpURL) {
        PropertyReader.ftpURL = ftpURL;
    }

    public static String getFtpLocalUploadFilePath() {
        return ftpLocalUploadFilePath;
    }

    public static void setFtpLocalUploadFilePath(String ftpLocalUploadFilePath) {
        PropertyReader.ftpLocalUploadFilePath = ftpLocalUploadFilePath;
    }

    public static String getFtpLocalDownloadFilePath() {
        return ftpLocalDownloadFilePath;
    }

    public static void setFtpLocalDownloadFilePath(String ftpLocalDownloadFilePath) {
        PropertyReader.ftpLocalDownloadFilePath = ftpLocalDownloadFilePath;
    }

    public static String getFtpUploadFileName() {
        return ftpUploadFileName;
    }

    public static void setFtpUploadFileName(String ftpUploadFileName) {
        PropertyReader.ftpUploadFileName = ftpUploadFileName;
    }

    public static String getFtpDownloadAcknowledgeFileName() {
        return ftpDownloadAcknowledgeFileName;
    }

    public static void setFtpDownloadAcknowledgeFileName(String ftpDownloadAcknowledgeFileName) {
        PropertyReader.ftpDownloadAcknowledgeFileName = ftpDownloadAcknowledgeFileName;
    }

    public static String getFtpDownloadPrintedFileName() {
        return ftpDownloadPrintedFileName;
    }

    public static void setFtpDownloadPrintedFileName(String ftpDownloadPrintedFileName) {
        PropertyReader.ftpDownloadPrintedFileName = ftpDownloadPrintedFileName;
    }

    public static String getFtpDownloadRawFileName() {
        return ftpDownloadRawFileName;
    }

    public static void setFtpDownloadRawFileName(String ftpDownloadRawFileName) {
        PropertyReader.ftpDownloadRawFileName = ftpDownloadRawFileName;
    }

    public static String getPropsLocation() {
        return propsLocation;
    }

    public static void setPropsLocation(String propsLocation) {
        PropertyReader.propsLocation = propsLocation;
    }

    public static String getXmlProxyAddress() {
        return xmlProxyAddress;
    }

    public static void setXmlProxyAddress(String xmlProxyAddress) {
        PropertyReader.xmlProxyAddress = xmlProxyAddress;
    }

    public static String getFileStorage() {
        return fileStorage;
    }

    public static void setFileStorage(String fileStorage) {
        PropertyReader.fileStorage = fileStorage;
    }

    public static String getFileType() {
        return fileType;
    }

    public static void setFileType(String fileType) {
        PropertyReader.fileType = fileType;
    }

    public static String getSubmissionType() {
        return submissionType;
    }

    public static void setSubmissionType(String submissionType) {
        PropertyReader.submissionType = submissionType;
    }

    public static String getAcknowledgeType() {
        return acknowledgeType;
    }

    public static void setAcknowledgeType(String acknowledgeType) {
        PropertyReader.acknowledgeType = acknowledgeType;
    }

    public static String getXmlProxyPort() {
        return xmlProxyPort;
    }

    public static void setXmlProxyPort(String xmlProxyPort) {
        PropertyReader.xmlProxyPort = xmlProxyPort;
    }

    public static String getXmlProxyUserid() {
        return xmlProxyUserid;
    }

    public static void setXmlProxyUserid(String xmlProxyUserid) {
        PropertyReader.xmlProxyUserid = xmlProxyUserid;
    }

    public static String getXmlProxyPassword() {
        return xmlProxyPassword;
    }

    public static void setXmlProxyPassword(String xmlProxyPassword) {
        PropertyReader.xmlProxyPassword = xmlProxyPassword;
    }

    public static String getXmlhttpsUrl() {
        return xmlhttpsUrl;
    }

    public static void setXmlhttpsUrl(String xmlhttpsUrl) {
        PropertyReader.xmlhttpsUrl = xmlhttpsUrl;
    }

    public static String getXmlProxyAuthentication() {
        return xmlProxyAuthentication;
    }

    public static void setXmlProxyAuthentication(String xmlProxyAuthentication) {
        PropertyReader.xmlProxyAuthentication = xmlProxyAuthentication;
    }

    public int getHttpsPort() {
        return httpsPort;
    }

    public static void setHttpsPort(int httpsPort) {
        PropertyReader.httpsPort = httpsPort;
    }

    public static String getUserIdentity() {
        return userIdentity;
    }

    public static void setUserIdentity(String userIdentity) {
        PropertyReader.userIdentity = userIdentity;
    }

    public static String getPublicKeyFilePath() {
        return publicKeyFilePath;
    }

    public static void setPublicKeyFilePath(String publicKeyFilePath) {
        PropertyReader.publicKeyFilePath = publicKeyFilePath;
    }

    public static String getHttpsTimeout() {
        return httpsTimeout;
    }

    public static void setHttpsTimeout(String httpsTimeout) {
        PropertyReader.httpsTimeout = httpsTimeout;
    }

    public static String getPublicKey() {
        return publicKey;
    }

    public static void setPublicKey(String publicKey) {
        PropertyReader.publicKey = publicKey;
    }
}
