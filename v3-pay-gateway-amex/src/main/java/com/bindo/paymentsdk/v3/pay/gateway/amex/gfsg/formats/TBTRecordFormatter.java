package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.formats;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionBatchTrailerBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.connection.PropertyReader;
import java.io.IOException;
import java.util.List;
import org.apache.log4j.Logger;

public final class TBTRecordFormatter
{
  private static final Logger LOGGER = Logger.getLogger(TBTRecordFormatter.class);
  
  public static void formatTBTRecord(TransactionBatchTrailerBean transactionBatchTrailerBean, List<String> formatedMessageList)
    throws IOException
  {
    StringBuffer formattedMessage = new StringBuffer();
    
    SettlementMessageFormatter.formatValue(transactionBatchTrailerBean.getRecordType(), formattedMessage, 3, false, true, true, false, true);
    
    SettlementMessageFormatter.formatValue(transactionBatchTrailerBean.getRecordNumber(), formattedMessage, 8, true, false, false, true, false);
    
    SettlementMessageFormatter.formatValue(transactionBatchTrailerBean.getMerchantId(), formattedMessage, 15, false, true, false, false, true);
    
    SettlementMessageFormatter.formatValue(transactionBatchTrailerBean.getServAgntMerchID(), formattedMessage, 15, false, true, false, false, true);
    
    SettlementMessageFormatter.formatValue(transactionBatchTrailerBean.getTbtIdentificationNumber(), formattedMessage, 15, true, false, false, true, false);
    
    SettlementMessageFormatter.formatValue(transactionBatchTrailerBean.getTbtCreationDate(), formattedMessage, 8, true, false, false, true, false);
    
    SettlementMessageFormatter.formatValue(transactionBatchTrailerBean.getTotalNoOfTabs(), formattedMessage, 8, true, false, false, true, false);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(transactionBatchTrailerBean.getTbtAmount(), formattedMessage, 20, true, false, false, true, false);
    
    SettlementMessageFormatter.formatValue(transactionBatchTrailerBean.getTbtAmountSign(), formattedMessage, 1, false, true, false, false, true);
    
    SettlementMessageFormatter.formatValue(transactionBatchTrailerBean.getTbtCurrencyCode(), formattedMessage, 3, true, false, true, false, false);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 20);
    
    SettlementMessageFormatter.characterSpacesFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(transactionBatchTrailerBean.getTbtImageSequenceNumber(), formattedMessage, 8, false, true, false, false, true);
    if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
      SettlementMessageFormatter.characterSpacesFill(formattedMessage, 567);
    }
    formatedMessageList.add(formattedMessage.toString());
    if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
      LOGGER.info("Formated TBT Record Type :" + formattedMessage.toString());
    }
  }
}
