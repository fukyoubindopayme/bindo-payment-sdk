package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations;

public enum TaxTypeCd
{
  Sales_Tax("056"),  All_Taxes("TX");
  
  private final String taxTypeCd;
  
  private TaxTypeCd(String taxTypeCd)
  {
    this.taxTypeCd = taxTypeCd;
  }
  
  public String getTaxTypeCd()
  {
    return this.taxTypeCd;
  }
}
