package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.confirmation;

public class ConfirmationReportSummaryBean
{
  private String recordType;
  private String numberOfOccurrences;
  private String errorCode;
  private String errorText;
  private String sumReserved;
  
  public String getRecordType()
  {
    return this.recordType;
  }
  
  public void setRecordType(String recordType)
  {
    this.recordType = recordType;
  }
  
  public String getErrorCode()
  {
    return this.errorCode;
  }
  
  public void setErrorCode(String errorCode)
  {
    this.errorCode = errorCode;
  }
  
  public String getErrorText()
  {
    return this.errorText;
  }
  
  public void setErrorText(String errorText)
  {
    this.errorText = errorText;
  }
  
  public String getSumReserved()
  {
    return this.sumReserved;
  }
  
  public void setSumReserved(String sumReserved)
  {
    this.sumReserved = sumReserved;
  }
  
  public String getNumberOfOccurrences()
  {
    return this.numberOfOccurrences;
  }
  
  public void setNumberOfOccurrences(String numberOfOccurrences)
  {
    this.numberOfOccurrences = numberOfOccurrences;
  }
}
