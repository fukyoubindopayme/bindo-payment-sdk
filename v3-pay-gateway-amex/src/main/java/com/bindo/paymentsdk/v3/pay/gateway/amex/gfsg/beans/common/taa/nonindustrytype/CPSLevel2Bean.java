package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.nonindustrytype;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.NonIndustrySpecificTAABean;

public class CPSLevel2Bean
  extends NonIndustrySpecificTAABean
{
  private String requesterName;
  private String chargeDescription1;
  private String chargeItemQuantity1;
  private String chargeItemAmount1;
  private String chargeDescription2;
  private String chargeItemQuantity2;
  private String chargeItemAmount2;
  private String chargeDescription3;
  private String chargeItemQuantity3;
  private String chargeItemAmount3;
  private String chargeDescription4;
  private String chargeItemQuantity4;
  private String chargeItemAmount4;
  private String cardMemberReferenceNumber;
  private String shipToPostalCode;
  private String totalTaxAmount;
  private String taxTypeCode;
  
  public String getRequesterName()
  {
    return this.requesterName;
  }
  
  public void setRequesterName(String requesterName)
  {
    this.requesterName = requesterName;
  }
  
  public String getChargeDescription1()
  {
    return this.chargeDescription1;
  }
  
  public void setChargeDescription1(String chargeDescription1)
  {
    this.chargeDescription1 = chargeDescription1;
  }
  
  public String getChargeItemQuantity1()
  {
    return this.chargeItemQuantity1;
  }
  
  public void setChargeItemQuantity1(String chargeItemQuantity1)
  {
    this.chargeItemQuantity1 = chargeItemQuantity1;
  }
  
  public String getChargeItemAmount1()
  {
    return this.chargeItemAmount1;
  }
  
  public void setChargeItemAmount1(String chargeItemAmount1)
  {
    this.chargeItemAmount1 = chargeItemAmount1;
  }
  
  public String getChargeDescription2()
  {
    return this.chargeDescription2;
  }
  
  public void setChargeDescription2(String chargeDescription2)
  {
    this.chargeDescription2 = chargeDescription2;
  }
  
  public String getChargeItemQuantity2()
  {
    return this.chargeItemQuantity2;
  }
  
  public void setChargeItemQuantity2(String chargeItemQuantity2)
  {
    this.chargeItemQuantity2 = chargeItemQuantity2;
  }
  
  public String getChargeItemAmount2()
  {
    return this.chargeItemAmount2;
  }
  
  public void setChargeItemAmount2(String chargeItemAmount2)
  {
    this.chargeItemAmount2 = chargeItemAmount2;
  }
  
  public String getChargeDescription3()
  {
    return this.chargeDescription3;
  }
  
  public void setChargeDescription3(String chargeDescription3)
  {
    this.chargeDescription3 = chargeDescription3;
  }
  
  public String getChargeItemQuantity3()
  {
    return this.chargeItemQuantity3;
  }
  
  public void setChargeItemQuantity3(String chargeItemQuantity3)
  {
    this.chargeItemQuantity3 = chargeItemQuantity3;
  }
  
  public String getChargeItemAmount3()
  {
    return this.chargeItemAmount3;
  }
  
  public void setChargeItemAmount3(String chargeItemAmount3)
  {
    this.chargeItemAmount3 = chargeItemAmount3;
  }
  
  public String getChargeDescription4()
  {
    return this.chargeDescription4;
  }
  
  public void setChargeDescription4(String chargeDescription4)
  {
    this.chargeDescription4 = chargeDescription4;
  }
  
  public String getChargeItemQuantity4()
  {
    return this.chargeItemQuantity4;
  }
  
  public void setChargeItemQuantity4(String chargeItemQuantity4)
  {
    this.chargeItemQuantity4 = chargeItemQuantity4;
  }
  
  public String getChargeItemAmount4()
  {
    return this.chargeItemAmount4;
  }
  
  public void setChargeItemAmount4(String chargeItemAmount4)
  {
    this.chargeItemAmount4 = chargeItemAmount4;
  }
  
  public String getCardMemberReferenceNumber()
  {
    return this.cardMemberReferenceNumber;
  }
  
  public void setCardMemberReferenceNumber(String cardMemberReferenceNumber)
  {
    this.cardMemberReferenceNumber = cardMemberReferenceNumber;
  }
  
  public String getShipToPostalCode()
  {
    return this.shipToPostalCode;
  }
  
  public void setShipToPostalCode(String shipToPostalCode)
  {
    this.shipToPostalCode = shipToPostalCode;
  }
  
  public String getTotalTaxAmount()
  {
    return this.totalTaxAmount;
  }
  
  public void setTotalTaxAmount(String totalTaxAmount)
  {
    this.totalTaxAmount = totalTaxAmount;
  }
  
  public String getTaxTypeCode()
  {
    return this.taxTypeCode;
  }
  
  public void setTaxTypeCode(String taxTypeCode)
  {
    this.taxTypeCode = taxTypeCode;
  }
}
