package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants;

public abstract interface XMLSubmissionConstants
{
  public static final String TRAVDISUNIT_MILES = "M";
  public static final String TRAVDISUNIT_KILOMETERS = "K";
  public static final String AUDITADJIND_MULTIPLE_ADJUSTMENTS = "X";
  public static final String AUDITADJIND_ONE_ADJUSTMENT_ONLY_CARDMEMBER_NOTIFIED = "Y";
  public static final String AUDITADJIND_ONE_ADJUSTMENT_ONLY_CARDMEMBER_NOT_NOTIFIED = "Z";
  public static final String BATCHADMIN_VERSION = "12010000";
  public static final String DATA_CAPTURE_VERSION = "12010000";
  public static final String DATE_FORMAT_YYMM = "YYMM";
  public static final String DATE_FORMAT_CCYYMMDD = "CCYYMMDD";
  public static final String DATE_FORMAT_YYYYMMDD = "yyyyMMdd";
  public static final String DATE_FORMAT_CCYYMM = "CCYYMM";
  public static final String ADDENDUM_AIRLINE = "01";
  public static final String ADDENDUM_AUTORENTAL = "05";
  public static final String ADDENDUM_COMMUNICATION = "13";
  public static final String ADDENDUM_INSURANCE = "04";
  public static final String ADDENDUM_TKT_ENT = "22";
  public static final String ADDENDUM_RETAIL = "20";
  public static final String ADDENDUM_TRAV_CRIUSE = "14";
  public static final String ADDENDUM_RAIL = "06";
  public static final String ADDENDUM_LODGING = "11";
  public static final String STATUS_EXPIRED = "expired";
  public static final String STATUS_FUTUREDATE = "futureDate";
  public static final String MULTIPLE_ADJUSTMENTS = "X";
  public static final String ONE_ADJUSTMENT_ONLY_CARDMEMBER_NOTIFIED = "Y";
  public static final String ONE_ADJUSTMENT_ONLY_CARDMEMBER_NOT_NOTIFIED = "Z";
  public static final String EMV_FORMAT_TYPE_PACKED = "00";
  public static final String EMV_FORMAT_TYPE_UNPACKED = "01";
  public static final String EMV_TRANSACTION_TYPE_DEBIT = "00";
  public static final String EMV_TRANSACTION_TYPE_CASH = "01";
  public static final String EMV_TRANSACTION_TYPE_CREDIT = "20";
  public static final String USAGE_PROHIBITED = "X";
  public static final String CAR_PACKAGE = "C";
  public static final String AIRLINE_PACKAGE = "A";
  public static final String BOTH_CAR_AND_AIRLINE_PACKAGE = "B";
  public static final String UNKNOWN_PACKAGE = "N";
  public static final String PLAN_N = "0003";
  public static final String DPP = "0005";
  public static final String[] RECORD_VALUE = {
  
    "[0-9]+$", 
    "[0-9]+$", 
    "[A-Za-z0-9-!\"#$%&'()*+,./:;<=>?@[\\\\]^_`{|}~áàåäâçéêèëíìîïñóôòöúûüùÁÅÄßÉÊÈÍÎÑÓÔÖÚÛ ]+$", 
    
    "[0-9]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9-* ]+$", 
    
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[A-Za-z0-9- ]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[A-Za-z0-9-!\"#$%&'()*+,./:;<=>?@[\\\\]^_`{|}~áàåäâçéêèëíìîïñóôòöúûüùÁÅÄßÉÊÈÍÎÑÓÔÖÚÛ ]+$", 
    
    "[0-9]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[A-Za-z0-9- ]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[A-Za-z0-9-!\"#$%&'()*+,./:;<=>?@[\\\\]^_`{|}~áàåäâçéêèëíìîïñóôòöúûüùÁÅÄßÉÊÈÍÎÑÓÔÖÚÛ]+$", 
    
    "[A-Za-z0-9-!\"#$%&'()*+,./:;<=>?@[\\\\]^_`{|}~áàåäâçéêèëíìîïñóôòöúûüùÁÅÄßÉÊÈÍÎÑÓÔÖÚÛ ]+$", 
    
    "[0-9]+$", 
    "[A-Za-z0-9- ]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[A-Za-z0-9-!\"#$%&'()*+,./:;<=>?@[\\\\]^_`{|}~ ]+$", 
    "[A-Za-z0-9]+$", 
    "[A-Za-z0-9]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[A-Za-z0-9-!\"#$%&'()*+,./:;<=>?@[\\\\]^_`{|}~áàåäâçéêèëíìîïñóôòöúûüùÁÅÄßÉÊÈÍÎÑÓÔÖÚÛ ]+$", 
    
    "[0-9]+$", 
    "[A-Za-z- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[0-9]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z- ]+$", 
    "[A-Za-z- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[0-9]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[A-Za-z0-9]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[A-Za-z0-9- ]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[A-Za-z- ]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9-!\"#$%&'()*+,./:;<=>?@[\\\\]^_`{|}~áàåäâçéêèëíìîïñóôòöúûüùÁÅÄßÉÊÈÍÎÑÓÔÖÚÛ ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9-!\"#$%&'()*+,./:;<=>?@[\\\\]^_`{|}~áàåäâçéêèëíìîïñóôòöúûüùÁÅÄßÉÊÈÍÎÑÓÔÖÚÛ ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z- ]+$", 
    "[A-Za-z- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[0-9]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[0-9]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9-!\"#$%&'()*+,./:;<=>?@[\\\\]^_`{|}~áàåäâçéêèëíìîïñóôòöúûüùÁÅÄßÉÊÈÍÎÑÓÔÖÚÛ ]+$", 
    
    "[A-Za-z0-9-!\"#$%&'()*+,./:;<=>?@[\\\\]^_`{|}~áàåäâçéêèëíìîïñóôòöúûüùÁÅÄßÉÊÈÍÎÑÓÔÖÚÛ ]+$", 
    
    "[0-9]+$", 
    "[0-9]+$", 
    "[A-Za-z0-9-!\"#$%&'()*+,./:;<=>?@[\\\\]^_`{|}~áàåäâçéêèëíìîïñóôòöúûüùÁÅÄßÉÊÈÍÎÑÓÔÖÚÛ ]+$", 
    
    "[A-Za-z0-9-!\"#$%&'()*+,./:;<=>?@[\\\\]^_`{|}~áàåäâçéêèëíìîïñóôòöúûüùÁÅÄßÉÊÈÍÎÑÓÔÖÚÛ ]+$", 
    
    "[0-9]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[A-Za-z0-9- ]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[A-Za-z0-9-!\"#$%&'()*+,./:;<=>?@[\\\\]^_`{|}~ ]+$", 
    "[0-9]+$", 
    "[0-9]+$", 
    "[A-Za-z0-9-!\"#$%&'()*+,./:;<=>?@[\\\\]^_`{|}~áàåäâçéêèëíìîïñóôòöúûüùÁÅÄßÉÊÈÍÎÑÓÔÖÚÛ ]+$", 
    "[A-Za-z0-9-!\"#$%&'()*+,./:;<=>?@[\\\\]^_`{|}~áàåäâçéêèëíìîïñóôòöúûüùÁÅÄßÉÊÈÍÎÑÓÔÖÚÛ ]+$", 
    "[A-Za-z0-9-!\"#$%&'()*+,./:;<=>?@[\\\\]^_`{|}~áàåäâçéêèëíìîïñóôòöúûüùÁÅÄßÉÊÈÍÎÑÓÔÖÚÛ ]+$", 
    "[A-Za-z0-9-!\"#$%&'()*+,./:;<=>?@[\\\\]^_`{|}~áàåäâçéêèëíìîïñóôòöúûüùÁÅÄßÉÊÈÍÎÑÓÔÖÚÛ ]+$", 
    "[A-Za-z0-9]+$" };
}
