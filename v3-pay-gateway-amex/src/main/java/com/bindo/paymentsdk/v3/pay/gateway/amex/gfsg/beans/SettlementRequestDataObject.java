package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionFileHeaderBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionFileSummaryBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionTBTSpecificBean;
import java.util.List;

public class SettlementRequestDataObject
{
  private TransactionFileHeaderBean transactionFileHeaderType;
  private List<TransactionTBTSpecificBean> transactionTBTSpecificType;
  private TransactionFileSummaryBean transactionFileSummaryType;
  private String formatCode;
  private String transactionIdentifier;
  private String merchantId;
  
  public TransactionFileHeaderBean getTransactionFileHeaderType()
  {
    return this.transactionFileHeaderType;
  }
  
  public TransactionFileSummaryBean getTransactionFileSummaryType()
  {
    return this.transactionFileSummaryType;
  }
  
  public String getFormatCode()
  {
    return this.formatCode;
  }
  
  public String getTransactionIdentifier()
  {
    return this.transactionIdentifier;
  }
  
  public String getMerchantId()
  {
    return this.merchantId;
  }
  
  public void setTransactionFileHeaderType(TransactionFileHeaderBean transactionFileHeaderType)
  {
    this.transactionFileHeaderType = transactionFileHeaderType;
  }
  
  public void setTransactionFileSummaryType(TransactionFileSummaryBean transactionFileSummaryType)
  {
    this.transactionFileSummaryType = transactionFileSummaryType;
  }
  
  public void setFormatCode(String formatCode)
  {
    this.formatCode = formatCode;
  }
  
  public void setTransactionIdentifier(String transactionIdentifier)
  {
    this.transactionIdentifier = transactionIdentifier;
  }
  
  public void setMerchantId(String merchantId)
  {
    this.merchantId = merchantId;
  }
  
  public List<TransactionTBTSpecificBean> getTransactionTBTSpecificType()
  {
    return this.transactionTBTSpecificType;
  }
  
  public void setTransactionTBTSpecificType(List<TransactionTBTSpecificBean> transactionTBTSpecificType)
  {
    this.transactionTBTSpecificType = transactionTBTSpecificType;
  }
}
