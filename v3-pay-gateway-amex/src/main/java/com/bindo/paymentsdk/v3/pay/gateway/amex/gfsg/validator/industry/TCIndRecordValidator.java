package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.industry;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.ErrorObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceAddendumBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceBasicBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.TravelCruiseIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.SubmissionErrorCodes;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.exceptions.SettlementException;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.CommonValidator;
import java.util.List;

public class TCIndRecordValidator
{
  public static void validateTCIndRecord(TransactionAdviceBasicBean transactionAdviceBasicType, TransactionAdviceAddendumBean transactionAddendumType, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    TravelCruiseIndustryTAABean tIndustryTAABean = (TravelCruiseIndustryTAABean)transactionAddendumType;
    
    TAADBRecordValidator.validateRecordType(tIndustryTAABean
      .getRecordType(), errorCodes);
    TAADBRecordValidator.validateRecordNumber(tIndustryTAABean
      .getRecordNumber(), errorCodes);
    TAADBRecordValidator.validateTransactionIdentifier(tIndustryTAABean
      .getTransactionIdentifier(), transactionAdviceBasicType, 
      errorCodes);
    TAADBRecordValidator.validateAddendaTypeCode(tIndustryTAABean
      .getAddendaTypeCode(), errorCodes);
    TAADBRecordValidator.validateFormatCode(tIndustryTAABean
      .getFormatCode(), transactionAdviceBasicType, errorCodes);
    
    validateIataCarrierCode(tIndustryTAABean.getIataCarrierCode(), 
      errorCodes);
    validateIataAgencyNumber(tIndustryTAABean.getIataAgencyNumber(), 
      errorCodes);
    validateTravelPackageIndicator(tIndustryTAABean
      .getTravelPackageIndicator(), errorCodes);
    validatePassengerName(tIndustryTAABean.getPassengerName(), errorCodes);
    validateTravelTicketNumber(tIndustryTAABean.getTravelTicketNumber(), 
      errorCodes);
    validateTravelDepartureDateSegment1(tIndustryTAABean
      .getTravelDepartureDateSegment1(), errorCodes);
    validateTravelDestinationCodeSegment1(tIndustryTAABean
      .getTravelDestinationCodeSegment1(), errorCodes);
    validateTravelDepartureAirportSegment1(tIndustryTAABean
      .getTravelDepartureAirportSegment1(), errorCodes);
    validateAirCarrierCodeSegment1(tIndustryTAABean
      .getAirCarrierCodeSegment1(), errorCodes);
    validateFlightNumberSegment1(
      tIndustryTAABean.getFlightNumberSegment1(), errorCodes);
    validateClassCodeSegment1(tIndustryTAABean.getClassCodeSegment1(), 
      errorCodes);
    
    validateClassCodeSegment2(tIndustryTAABean.getClassCodeSegment2(), 
      errorCodes);
    validateClassCodeSegment3(tIndustryTAABean.getClassCodeSegment3(), 
      errorCodes);
    validateClassCodeSegment4(tIndustryTAABean.getClassCodeSegment4(), 
      errorCodes);
    
    validateFlightNumberSegment2(
      tIndustryTAABean.getFlightNumberSegment2(), errorCodes);
    validateFlightNumberSegment3(
      tIndustryTAABean.getFlightNumberSegment3(), errorCodes);
    validateFlightNumberSegment4(
      tIndustryTAABean.getFlightNumberSegment4(), errorCodes);
    
    validateAirCarrierCodeSegment2(tIndustryTAABean
      .getAirCarrierCodeSegment2(), errorCodes);
    validateAirCarrierCodeSegment3(tIndustryTAABean
      .getAirCarrierCodeSegment3(), errorCodes);
    validateAirCarrierCodeSegment4(tIndustryTAABean
      .getAirCarrierCodeSegment4(), errorCodes);
    
    validateTravelDepartureAirportSegment2(tIndustryTAABean
      .getTravelDepartureAirportSegment2(), errorCodes);
    validateTravelDepartureAirportSegment3(tIndustryTAABean
      .getTravelDepartureAirportSegment3(), errorCodes);
    validateTravelDepartureAirportSegment4(tIndustryTAABean
      .getTravelDepartureAirportSegment4(), errorCodes);
    
    validateTravelDestinationCodeSegment2(tIndustryTAABean
      .getTravelDestinationCodeSegment2(), errorCodes);
    validateTravelDestinationCodeSegment3(tIndustryTAABean
      .getTravelDestinationCodeSegment3(), errorCodes);
    validateTravelDestinationCodeSegment4(tIndustryTAABean
      .getTravelDestinationCodeSegment4(), errorCodes);
    
    validateTravelDepartureDateSegment2(tIndustryTAABean
      .getTravelDepartureDateSegment2(), errorCodes);
    validateTravelDepartureDateSegment3(tIndustryTAABean
      .getTravelDepartureDateSegment3(), errorCodes);
    validateTravelDepartureDateSegment4(tIndustryTAABean
      .getTravelDepartureDateSegment4(), errorCodes);
    
    validateLodgingCheckInDate(tIndustryTAABean.getLodgingCheckInDate(), 
      errorCodes);
    validateLodgingCheckOutDate(tIndustryTAABean, errorCodes);
    validateLodgingRoomRate1(tIndustryTAABean.getLodgingRoomRate1(), 
      errorCodes);
    validateNumberOfNightsAtRoomRate1(tIndustryTAABean
      .getNumberOfNightsAtRoomRate1(), errorCodes);
    validateLodgingName(tIndustryTAABean.getLodgingName(), errorCodes);
    validateLodgingRegionCode(tIndustryTAABean.getLodgingRegionCode(), 
      errorCodes);
    validateLodgingCountryCode(tIndustryTAABean.getLodgingCountryCode(), 
      errorCodes);
    validateLodgingCityName(tIndustryTAABean.getLodgingCityName(), 
      errorCodes);
  }
  
  private static void validateIataCarrierCode(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ĸ']))
      {
        String reqLength = CommonValidator.validateLength(value, 3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD311_IATA_CARRIER_CODE);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD312_IATA_CARRIER_CODE);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateIataAgencyNumber(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ĺ']))
      {
        String reqLength = CommonValidator.validateLength(value, 8, 8);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD313_IATA_CARRIER_CODE);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD314_IATA_CARRIER_CODE);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateTravelPackageIndicator(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD316_TRAVEL_PACKAGENDICATOR);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ĺ']))
    {
      String reqLength = CommonValidator.validateLength(value, 1, 1);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD317_TRAVEL_PACKAGENDICATOR);
        errorCodes.add(errorObj);
      }
      else if (!value.equalsIgnoreCase("C"))
      {
        if (!value.equalsIgnoreCase("A")) {
          if (!value.equalsIgnoreCase("B")) {
            if (!value.equalsIgnoreCase("N"))
            {
              errorObj = new ErrorObject(
                SubmissionErrorCodes.ERROR_DATAFIELD315_TRAVEL_PACKAGENDICATOR);
              errorCodes.add(errorObj);
            }
          }
        }
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD318_TRAVEL_PACKAGENDICATOR);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validatePassengerName(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 25, 25);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD318_PASSENGER_NAME);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateTravelTicketNumber(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 14, 14);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD320_TRAVEL_TICKET_NUMBER);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateTravelDepartureDateSegment1(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ľ']))
      {
        String reqLength = CommonValidator.validateLength(value, 8, 8);
        if (!reqLength.equals("equal"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD323_TRAVELDEPARTURE_DATESEGMENT1);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD322_TRAVELDEPARTURE_DATESEGMENT1);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateTravelDepartureDateSegment2(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ľ']))
      {
        String reqLength = CommonValidator.validateLength(value, 8, 8);
        if (!reqLength.equals("equal"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD325_TRAVELDEPARTURE_DATESEGMENT2);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD324_TRAVELDEPARTURE_DATESEGMENT2);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateTravelDepartureDateSegment3(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ŀ']))
      {
        String reqLength = CommonValidator.validateLength(value, 8, 8);
        if (!reqLength.equals("equal"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD327_TRAVELDEPARTURE_DATESEGMENT3);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD326_TRAVELDEPARTURE_DATESEGMENT3);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateTravelDepartureDateSegment4(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ŀ']))
      {
        String reqLength = CommonValidator.validateLength(value, 8, 8);
        if (!reqLength.equals("equal"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD329_TRAVELDEPARTURE_DATESEGMENT4);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD328_TRAVELDEPARTURE_DATESEGMENT4);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateTravelDestinationCodeSegment1(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ł']))
      {
        String reqLength = CommonValidator.validateLength(value, 3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD331_TRAVEL_DESTINATIONCODESEGMENT1);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD330_TRAVEL_DESTINATIONCODESEGMENT1);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateTravelDestinationCodeSegment2(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ł']))
      {
        String reqLength = CommonValidator.validateLength(value, 3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD333_TRAVEL_DESTINATIONCODESEGMENT2);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD332_TRAVEL_DESTINATIONCODESEGMENT2);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateTravelDestinationCodeSegment3(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ń']))
      {
        String reqLength = CommonValidator.validateLength(value, 3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD335_TRAVEL_DESTINATIONCODESEGMENT3);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD334_TRAVEL_DESTINATIONCODESEGMENT3);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateTravelDestinationCodeSegment4(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ń']))
      {
        String reqLength = CommonValidator.validateLength(value, 3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD337_TRAVEL_DESTINATIONCODESEGMENT4);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD336_TRAVEL_DESTINATIONCODESEGMENT4);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateTravelDepartureAirportSegment1(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ņ']))
      {
        String reqLength = CommonValidator.validateLength(value, 3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD339_TRAVELDEPARTURE_AIRPORTSEGMENT1);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD338_TRAVELDEPARTURE_AIRPORTSEGMENT1);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateTravelDepartureAirportSegment2(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ņ']))
      {
        String reqLength = CommonValidator.validateLength(value, 3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD341_TRAVELDEPARTURE_AIRPORTSEGMENT2);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD340_TRAVELDEPARTURE_AIRPORTSEGMENT2);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateTravelDepartureAirportSegment3(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ň']))
      {
        String reqLength = CommonValidator.validateLength(value, 3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD343_TRAVELDEPARTURE_AIRPORTSEGMENT3);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD342_TRAVELDEPARTURE_AIRPORTSEGMENT3);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateTravelDepartureAirportSegment4(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ň']))
      {
        String reqLength = CommonValidator.validateLength(value, 3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD345_TRAVELDEPARTURE_AIRPORTSEGMENT4);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD344_TRAVELDEPARTURE_AIRPORTSEGMENT4);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateAirCarrierCodeSegment1(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ŉ']))
      {
        String reqLength = CommonValidator.validateLength(value, 2, 2);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD347_AIRCARRIER_CODESEGMENT1);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD346_AIRCARRIER_CODESEGMENT1);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateAirCarrierCodeSegment2(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ŋ']))
      {
        String reqLength = CommonValidator.validateLength(value, 2, 2);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD349_AIRCARRIER_CODESEGMENT2);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD348_AIRCARRIER_CODESEGMENT2);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateAirCarrierCodeSegment3(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ŋ']))
      {
        String reqLength = CommonValidator.validateLength(value, 2, 2);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD351_AIRCARRIER_CODESEGMENT3);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD350_AIRCARRIER_CODESEGMENT3);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateAirCarrierCodeSegment4(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ō']))
      {
        String reqLength = CommonValidator.validateLength(value, 2, 2);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD353_AIRCARRIER_CODESEGMENT4);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD352_AIRCARRIER_CODESEGMENT4);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateFlightNumberSegment1(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ō']))
      {
        String reqLength = CommonValidator.validateLength(value, 4, 4);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD355_FLIGHTNUMBER_SEGMENT1);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD354_FLIGHTNUMBER_SEGMENT1);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateFlightNumberSegment2(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ŏ']))
      {
        String reqLength = CommonValidator.validateLength(value, 4, 4);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD357_FLIGHTNUMBER_SEGMENT2);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD356_FLIGHTNUMBER_SEGMENT2);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateFlightNumberSegment3(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ŏ']))
      {
        String reqLength = CommonValidator.validateLength(value, 4, 4);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD359_FLIGHTNUMBER_SEGMENT3);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD358_FLIGHTNUMBER_SEGMENT3);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateFlightNumberSegment4(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ő']))
      {
        String reqLength = CommonValidator.validateLength(value, 4, 4);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD361_FLIGHTNUMBER_SEGMENT4);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD360_FLIGHTNUMBER_SEGMENT4);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateClassCodeSegment1(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ő']))
      {
        String reqLength = CommonValidator.validateLength(value, 1, 1);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD363_CLASSCODE_SEGMENT1);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD362_CLASSCODE_SEGMENT1);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateClassCodeSegment2(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Œ']))
      {
        String reqLength = CommonValidator.validateLength(value, 1, 1);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD365_CLASSCODE_SEGMENT2);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD364_CLASSCODE_SEGMENT2);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateClassCodeSegment3(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['œ']))
      {
        String reqLength = CommonValidator.validateLength(value, 1, 1);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD367_CLASSCODE_SEGMENT3);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD366_CLASSCODE_SEGMENT3);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateClassCodeSegment4(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ŕ']))
      {
        String reqLength = CommonValidator.validateLength(value, 1, 1);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD369_CLASSCODE_SEGMENT4);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD368_CLASSCODE_SEGMENT4);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateLodgingCheckInDate(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ŕ']))
      {
        String reqLength = CommonValidator.validateLength(value, 8, 8);
        if (!reqLength.equals("equal"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD371_LODGINGCHECK_INDATE);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD370_LODGINGCHECK_INDATE);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateLodgingCheckOutDate(TravelCruiseIndustryTAABean tIndustryTAABean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(tIndustryTAABean.getLodgingCheckOutDate())) {
      if (CommonValidator.validateData(tIndustryTAABean.getLodgingCheckOutDate(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ŗ']))
      {
        String reqLength = CommonValidator.validateLength(
          tIndustryTAABean.getLodgingCheckOutDate(), 8, 8);
        if (reqLength.equals("equal"))
        {
          if (Integer.parseInt(tIndustryTAABean.getLodgingCheckOutDate()) < Integer.parseInt(tIndustryTAABean.getLodgingCheckInDate()))
          {
            errorObj = new ErrorObject(
              SubmissionErrorCodes.ERROR_DATAFIELD374_LODGINGCHECK_OUTDATE);
            errorCodes.add(errorObj);
          }
        }
        else
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD373_LODGINGCHECK_OUTDATE);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD372_LODGINGCHECK_OUTDATE);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateLodgingRoomRate1(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ř']))
      {
        String reqLength = 
          CommonValidator.validateLength(value, 12, 12);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD376_LODGING_ROOMRATE1);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD375_LODGING_ROOMRATE1);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateNumberOfNightsAtRoomRate1(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ř']))
      {
        String reqLength = CommonValidator.validateLength(value, 2, 2);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD378_NUMBEROFNIGHTS_ATROOMRATE1);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD377_NUMBEROFNIGHTS_ATROOMRATE1);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateLodgingName(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 20, 20);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD379_LODGING_NAME);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateLodgingRegionCode(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ś']))
      {
        String reqLength = CommonValidator.validateLength(value, 3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD381_LODGING_REGIONCODE);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD382_LODGING_REGIONCODE);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateLodgingCountryCode(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ŝ']))
      {
        String reqLength = CommonValidator.validateLength(value, 3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD383_LODGING_COUNTRYCODE);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD384_LODGING_COUNTRYCODE);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateLodgingCityName(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 18, 18);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD385_LODGING_CITYNAME);
        errorCodes.add(errorObj);
      }
    }
  }
}
