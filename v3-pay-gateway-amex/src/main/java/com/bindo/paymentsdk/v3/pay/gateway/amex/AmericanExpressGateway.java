package com.bindo.paymentsdk.v3.pay.gateway.amex;

//import com.americanexpress.ips.AuthorizationService;
//import com.americanexpress.ips.connection.PropertyReader;
//import com.americanexpress.ips.enums.CountryCode;
//import com.americanexpress.ips.enums.MCCCode;
//import com.americanexpress.ips.enums.MessageReasonCode;
//import com.americanexpress.ips.enums.ProcessingCode;
//import com.americanexpress.ips.enums.pos.CardCapture;
//import com.americanexpress.ips.enums.pos.CardDataInput;
//import com.americanexpress.ips.enums.pos.CardDataInputMode;
//import com.americanexpress.ips.enums.pos.CardDataOutput;
//import com.americanexpress.ips.enums.pos.CardMemberAuthenticationEntity;
//import com.americanexpress.ips.enums.pos.CardMemberAuthenticationMethod;
//import com.americanexpress.ips.enums.pos.CardPresent;
//import com.americanexpress.ips.enums.pos.CardholderAuthentication;
//import com.americanexpress.ips.enums.pos.CardholderPresent;
//import com.americanexpress.ips.enums.pos.OperatingEnvironment;
//import com.americanexpress.ips.enums.pos.PinCapture;
//import com.americanexpress.ips.enums.pos.TerminalOutput;
//import com.americanexpress.ips.gcag.bean.AuthorizationRequestBean;
//import com.americanexpress.ips.gcag.bean.AuthorizationResponseBean;
//import com.americanexpress.ips.gcag.bean.ErrorObject;
//import com.americanexpress.ips.gcag.bean.IntegratedCircuitCardRelatedDataBean;
//import com.americanexpress.ips.gcag.bean.OriginalDataElementsBean;
//import com.americanexpress.ips.gcag.bean.POSDataCodeBean;
//import com.americanexpress.ips.utils.DataConversion;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.AuthorizationService;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.connection.PropertyReader;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.CountryCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.MCCCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.MessageReasonCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.ProcessingCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.pos.CardCapture;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.pos.CardDataInput;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.pos.CardDataInputMode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.pos.CardDataOutput;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.pos.CardMemberAuthenticationEntity;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.pos.CardMemberAuthenticationMethod;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.pos.CardPresent;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.pos.CardholderAuthentication;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.pos.CardholderPresent;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.pos.OperatingEnvironment;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.pos.PinCapture;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.pos.TerminalOutput;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean.AuthorizationRequestBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean.AuthorizationResponseBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean.ErrorObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean.IntegratedCircuitCardRelatedDataBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean.OriginalDataElementsBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean.POSDataCodeBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.utils.DataConversion;
import com.bindo.paymentsdk.v3.pay.common.emv.enums.FlowType;
import com.bindo.paymentsdk.v3.pay.common.CardDetectMode;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Request;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Response;
import com.bindo.paymentsdk.v3.pay.common.emv.enums.EMVTag;
import com.bindo.paymentsdk.v3.pay.common.util.StringUtils;
import com.bindo.paymentsdk.v3.pay.gateway.Gateway;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * American Express 支付网关
 */
@Deprecated
public class AmericanExpressGateway implements Gateway {
    private final String TAG = "AmericanExpressGateway";

    // Gateway 请求参数
    public static final String CONFIG_REQUEST_CARD_ACCEPTOR_TERMINAL_ID = "config_request_card_acceptor_terminal_id";
    public static final String CONFIG_REQUEST_CARD_ACCEPTOR_ID_CODE = "config_request_card_acceptor_id_code";
    // AMEX SDK 配置参数
    public static final String CONFIG_SDK_LOG_FILE_PATH = "config_log_file_path";
    public static final String CONFIG_SDK_ORIGIN = "config_origin";
    public static final String CONFIG_SDK_MERCHANT_COUNTRY = "config_merchant_country";
    public static final String CONFIG_SDK_REGION = "config_region";
    public static final String CONFIG_SDK_MESSAGE_TYPE = "config_message_type";
    public static final String CONFIG_SDK_MERCHANT_NUMBER = "config_merchant_number";
    public static final String CONFIG_SDK_ROUTING_INDICATOR = "config_routing_indicator";
    public static final String CONFIG_SDK_HTTPS_TIMEOUT = "config_https_timeout";
    public static final String CONFIG_SDK_READ_TIMEOUT = "config_read_timeout";

    // 默认配置值
    private final String DEFAULT_REQUEST_CARD_ACCEPTOR_TERMINAL_ID = "03530029";
    private final String DEFAULT_REQUEST_CARD_ACCEPTOR_ID_CODE = "4380171421";
    private final String DEFAULT_SDK_LOG_FILE_PATH = System.getProperty("java.io.tmpdir");
    private final String DEFAULT_SDK_ORIGIN = "BINDO";
    private final String DEFAULT_SDK_MERCHANT_COUNTRY = "344";
    private final String DEFAULT_SDK_REGION = "JAPA";
    private final String DEFAULT_SDK_MESSAGE_TYPE = "ISO GCAG";
    private final String DEFAULT_SDK_MERCHANT_NUMBER = "4380171421";
    private final String DEFAULT_SDK_ROUTING_INDICATOR = "050";
    private final String DEFAULT_SDK_HTTPS_TIMEOUT = "30000";
    private final String DEFAULT_SDK_READ_TIMEOUT = "30000";


    private HashMap<String, Object> mDefaultGatewayConfig = new HashMap<>();

    public AmericanExpressGateway(HashMap<String, Object> configMap) {
        // Gateway 参数默认值
        mDefaultGatewayConfig.put(CONFIG_REQUEST_CARD_ACCEPTOR_TERMINAL_ID, DEFAULT_REQUEST_CARD_ACCEPTOR_TERMINAL_ID);
        mDefaultGatewayConfig.put(CONFIG_REQUEST_CARD_ACCEPTOR_ID_CODE, DEFAULT_REQUEST_CARD_ACCEPTOR_ID_CODE);
        mDefaultGatewayConfig.put(CONFIG_SDK_LOG_FILE_PATH, DEFAULT_SDK_LOG_FILE_PATH);
        mDefaultGatewayConfig.put(CONFIG_SDK_ORIGIN, DEFAULT_SDK_ORIGIN);
        mDefaultGatewayConfig.put(CONFIG_SDK_MERCHANT_COUNTRY, DEFAULT_SDK_MERCHANT_COUNTRY);
        mDefaultGatewayConfig.put(CONFIG_SDK_REGION, DEFAULT_SDK_REGION);
        mDefaultGatewayConfig.put(CONFIG_SDK_MESSAGE_TYPE, DEFAULT_SDK_MESSAGE_TYPE);
        mDefaultGatewayConfig.put(CONFIG_SDK_MERCHANT_NUMBER, DEFAULT_SDK_MERCHANT_NUMBER);
        mDefaultGatewayConfig.put(CONFIG_SDK_ROUTING_INDICATOR, DEFAULT_SDK_ROUTING_INDICATOR);
        mDefaultGatewayConfig.put(CONFIG_SDK_HTTPS_TIMEOUT, DEFAULT_SDK_HTTPS_TIMEOUT);
        mDefaultGatewayConfig.put(CONFIG_SDK_READ_TIMEOUT, DEFAULT_SDK_READ_TIMEOUT);

        if (configMap == null) {
            System.out.println("Please set the gateway parameters");
        } else {
            for (String key : configMap.keySet()) {
                Object value = configMap.get(key);
                mDefaultGatewayConfig.put(key, value);
            }
        }
    }

    public HashMap<String, Object> getDefaultGatewayConfig() {
        return mDefaultGatewayConfig;
    }

    public void setDefaultGatewayConfig(HashMap<String, Object> defaultGatewayConfig) {
        this.mDefaultGatewayConfig = defaultGatewayConfig;
    }

    @Override
    public Response sendRequest(Request request) {
//        if (request.getDCCTransactionType() != null) {
//            return null;
//        }
        // 由于 AMEX 使用的是他们提供的 SDK，所以根据不同的交易类型，调用相应的方法，避免在修改参数等会相互影响结果
        switch (request.getTransactionType()) {
            // 授权
            case AUTHORIZATION:
                return this.authorization(request);
            // 授权撤销
            case AUTHORIZATION_VOID:
                return null;
            // 预授权
            case PRE_AUTHORIZATION:
                return this.authorization(request);
            // 预授权撤销
            case PRE_AUTHORIZATION_VOID:
                return null;
            // 预授权完成
            case PRE_AUTHORIZATION_COMPLETION:
                return null;
            // 冲正
            case REVERSAL:
                return this.reversal(request);
            // 退货
            case REFUND:
                return null;
            // 退货撤销
            case REFUND_VOID:
                return null;
            default:
                return null;
        }
    }

    private Response authorization(Request request) {
        HashMap<String, String> gatewayConfigMap = new HashMap<>();

        for (String key : mDefaultGatewayConfig.keySet()) {
            String value = (String) mDefaultGatewayConfig.get(key);
            gatewayConfigMap.put(key, value);
        }

        if (request.getGatewayConfigs() != null && request.getGatewayConfigs().size() > 0) {
            HashMap<String, String> configMap = request.getGatewayConfigs();
            for (String key : configMap.keySet()) {
                String value = configMap.get(key);
                gatewayConfigMap.put(key, value);
            }
        }

        request.setGatewayConfigs(gatewayConfigMap);

        Response response = new Response(request);
        List<Response.Error> errors = new ArrayList<>();

        String cardDetectMode = request.getCardDetectMode();
        boolean ignoreIccData = false;

        AuthorizationRequestBean requestBean = new AuthorizationRequestBean();
        AuthorizationResponseBean responseBean = new AuthorizationResponseBean();
        //	—	MESSAGE TYPE IDENTIFIER
        requestBean.setMessageTypeIdentifier("1100");
        if (!StringUtils.isEmpty(request.getMessageTypeId())) {
            requestBean.setMessageTypeIdentifier(request.getMessageTypeId());
        }

        //	—	BIT MAP - PRIMARY
        // >>> Not used

        //	1	BIT MAP - SECONDARY
        // >>> Not used

        //	2	PRIMARY ACCOUNT NUMBER (PAN)
        requestBean.setPrimaryAccountNumber(new StringBuffer(request.getPrimaryAccountNumber()));

        //	3	PROCESSING CODE
        requestBean.setProcessingCode("004000");

        //	4	AMOUNT, TRANSACTION
        String amountTransaction = new DecimalFormat("0000000000.00").format(request.getTransactionAmount()).replace(".", "");
        requestBean.setAmountTransaction(amountTransaction);
        //	7	DATE AND TIME, TRANSMISSION
        // >>> Not used

        //	11	SYSTEMS TRACE AUDIT NUMBER
        String systemTraceAuditNumber = String.format("%06d", request.getTransactionSystemTraceAuditNumber());
        requestBean.setSystemTraceAuditNumber(systemTraceAuditNumber);

        //	12	DATE AND TIME, LOCAL TRANSACTION
        requestBean.setLocalTransactionDateAndTime(request.getTransactionDateAndTime());

        //	13	DATE, EFFECTIVE
        // >>> Not used

        //	14	DATE, EXPIRATION
        requestBean.setCardExpirationDate(request.getCardExpirationDate());

        //	15	DATE, SETTLEMENT
        // >>> Not used

        //	18	MERCHANT TYPE
        // >>> Not used

        //	19	COUNTRY CODE, ACQUIRING INSTITUTION
        requestBean.setMerchantLocationCountryCode(CountryCode.HongKong.getCountryCode());

        //	22	POINT OF SERVICE DATA CODE
        POSDataCodeBean posDataCodeBean = new POSDataCodeBean();
        // Position 1 - Card Data Input Capability
        posDataCodeBean.setCardDataInput(CardDataInput.POC_INTEGERATED_CIRCUIT_CARD.getCardDataInput());

        // Position 2 - Cardholder Authentication Capability
        posDataCodeBean.setCardholderAuthentication(CardholderAuthentication.POC_UNKNOWN.getCardholderAuthentication());

        // Position 3 - Card Capture Capability
        posDataCodeBean.setCardCapture(CardCapture.POC_CAPTURE.getCardCapture());

        // Position 4 - Operating Environment
        posDataCodeBean.setOperatingEnvironment(OperatingEnvironment.POC_ON_PREMISES_OF_CARD_ACCEPTOR_ATTENDED.getOperatingEnvironment());

        // Position 5 - Cardholder Present
        posDataCodeBean.setCardholderPresent(CardholderPresent.POC_CARDMEMBER_PRESENT.getCardholderPresent());

        // Position 6 - Card Present
        posDataCodeBean.setCardPresent(CardPresent.POC_CARD_NOT_PRESENT.getCardPresent());

        // Position 7 - Card Data Input Mode
        posDataCodeBean.setCardDataInputMode(CardDataInputMode.POC_UNKNOWN.getCardDataInputMode());

        // Position 8 - Cardmember Authentication Method
        CardMemberAuthenticationMethod cardMemberAuthenticationMethod = CardMemberAuthenticationMethod.POC_UNKNOWN;
        switch (cardDetectMode) {
            case CardDetectMode.CONTACT:
            case CardDetectMode.CONTACTLESS:
                byte[] terminalVerificationResults = request.getTagLengthValues().get(EMVTag.TERMINAL_VERIFICATION_RESULTS.toString());
                byte[] cardholderVerificationMethodResults = request.getTagLengthValues().get(EMVTag.CARDHOLDER_VERIFICATION_METHOD_RESULTS.toString());

                if (cardholderVerificationMethodResults != null && cardholderVerificationMethodResults.length > 0) {
                    switch ((cardholderVerificationMethodResults[0] & 0x3f)) {
                        case 0x01:  // PLAINTEXT_PIN
                        case 0x02:  // ONLINE_ENCIPHERED_PIN
                        case 0x03:  // PAINTEXT_PIN_AND_SIGNATURE
                        case 0x04:  // OFFLINE_ENCIPHERED_PIN
                        case 0x05:  // OFFLINE_ENCIPHERED_PIN_AND_SIGNATURE
                            if (terminalVerificationResults[2] == 0x00) {
                                cardMemberAuthenticationMethod = CardMemberAuthenticationMethod.POC_PINL;
                            }
                            break;
                        case 0x1e:  // SIGNATURE
                            cardMemberAuthenticationMethod = CardMemberAuthenticationMethod.POC_MANUAL_SIGN_VERIFICATION;
                            break;
                    }
                }
                break;
            default:
                cardMemberAuthenticationMethod = CardMemberAuthenticationMethod.POC_MANUAL_SIGN_VERIFICATION;
                break;
        }

        posDataCodeBean.setCardMemberAuthenticationMethod(cardMemberAuthenticationMethod.getCardMemberAuthenticationMethod());

        // Position 9 - Cardmember Authentication Entity
        posDataCodeBean.setCardMemberAuthenticationEntity(CardMemberAuthenticationEntity.POC_INTEGRATED_CKT_CARD.getCardMemberAuthenticationEntity());

        // Position 10 - Card Data Output Capability
        posDataCodeBean.setCardDataOutput(CardDataOutput.INTEGRATED_CIRCUIT_CARD.getCardDataOutput());

        // Position 11 - Terminal Output Capability
        posDataCodeBean.setTerminalOutput(TerminalOutput.POC_PRINTING_AND_DISPLAY.getTerminalOutput());

        // Position 12 - PIN Capture Capability
        posDataCodeBean.setPinCapture(PinCapture.POC_TWELVE_CHARS.getPinCapture());

        switch (cardDetectMode) {
            case CardDetectMode.CONTACT:
                posDataCodeBean.setCardPresent(CardPresent.POC_CARD_PRESENT.getCardPresent());
                posDataCodeBean.setCardDataInputMode(CardDataInputMode.POC_INTEGRATED_CKT_CARD.getCardDataInputMode());
                break;
            case CardDetectMode.CONTACTLESS:
                posDataCodeBean.setCardPresent(CardPresent.POC_CONTACTLESS_TRANSACTION.getCardPresent());
                posDataCodeBean.setCardDataInputMode(CardDataInputMode.POC_INTEGRATED_CKT_CARD.getCardDataInputMode());
                break;
            case CardDetectMode.SWIPE:
            case CardDetectMode.FALLBACK_SWIPE:
                boolean isFallback = CardDetectMode.FALLBACK_SWIPE.equals(cardDetectMode);
                if (!isFallback) {
                    posDataCodeBean.setCardPresent(CardPresent.POC_CARD_PRESENT.getCardPresent());
                    posDataCodeBean.setCardDataInputMode(CardDataInputMode.POC_MAGNETIC_STRIPE_READ.getCardDataInputMode());
                } else {
                    posDataCodeBean.setCardPresent(CardPresent.POC_CARD_PRESENT.getCardPresent());
                    posDataCodeBean.setCardDataInputMode(CardDataInputMode.POC_TECHNICAL_FALLBACK.getCardDataInputMode());
                }
                break;
            case CardDetectMode.MANUAL:
                posDataCodeBean.setCardPresent(CardPresent.POC_CARD_PRESENT.getCardPresent());
                posDataCodeBean.setCardDataInputMode(CardDataInputMode.POC_MANUAL.getCardDataInputMode());
                break;
        }
        requestBean.setPosDataCode(posDataCodeBean.populatePointOfServiceDataCode());

        //	24	FUNCTION CODE
        requestBean.setFunctionCode("100");

        //	25	MESSAGE REASON CODE
        requestBean.setMessageReasonCode(MessageReasonCode.value1.getMessageReasonCode());

        //	26	CARD ACCEPTOR BUSINESS CODE
        requestBean.setCardAcceptorBusinessCode(MCCCode.Department_Stores.getMccCode());
//        requestBean.setCardAcceptorBusinessCode(MCCCode.Lodging_Hotels_Motels_and_Resorts.getMccCode());

        //	27	APPROVAL CODE LENGTH
        requestBean.setApprovalCodeLength("2");

        //	31	ACQUIRER REFERENCE DATA
        // >>> Not used

        //	32	ACQUIRING INSTITUTION IDENTIFICATION CODE
        // >>> Not used

        //	33	FORWARDING INSTITUTION IDENTIFICATION CODE
        // >>> Not used

        //	35	TRACK 2 DATA
        String track2 = request.getTrack2EquivalentData();
        if (CardDetectMode.CONTACTLESS.equals(cardDetectMode)) {
            track2 = request.getTLVHexString(EMVTag.TRACK_2_EQUIVALENT_DATA);

            String unpredictableNumber = request.getTLVHexString(EMVTag.UNPREDICTABLE_NUMBER);
            String applicationCryptogram = request.getTLVHexString(EMVTag.APPLICATION_CRYPTOGRAM);
            String applicationTransactionCounter = request.getTLVHexString(EMVTag.APPLICATION_TRANSACTION_COUNTER);

            unpredictableNumber = unpredictableNumber.substring(unpredictableNumber.length() - 4);
            applicationCryptogram = String.valueOf(Integer.parseInt(applicationCryptogram.substring(applicationCryptogram.length() - 6), 16));
            if (applicationCryptogram.length() > 5) {
                applicationCryptogram = applicationCryptogram.substring(applicationCryptogram.length() - 5);
            }
            applicationCryptogram = "00000".substring(applicationCryptogram.length()) + applicationCryptogram;

            applicationTransactionCounter = String.valueOf(Integer.parseInt(applicationTransactionCounter, 16));
            applicationTransactionCounter = "00000".substring(applicationTransactionCounter.length()) + applicationTransactionCounter;

            byte[] flowTypeByteArray = request.getTagLengthValues().get(EMVTag.CUSTOM_FLOW_TYPE.toString());
            byte flowType = (flowTypeByteArray != null && flowTypeByteArray.length >= 1) ? flowTypeByteArray[0] : -1;

            switch (flowType) {
                case FlowType.AMEX_MAGSRTIPE:        // Landi A8 AMEX ExpressPay Card Magstripe Mode
                case FlowType.AMEX_MOBILE_MAGSTRIPE: // Landi A8 AMEX ExpressPay Mobile Magstripe Mode
                    track2 = track2.replace('D', '=');
                    track2 = track2.substring(0, (track2.indexOf('=') + 8)) + unpredictableNumber + applicationCryptogram + applicationTransactionCounter;
                    // RF Magstripe Mode
                    ignoreIccData = true;
                    posDataCodeBean.setCardDataInputMode(CardDataInputMode.POC_MAGNETIC_STRIPE_READ.getCardDataInputMode());
                    requestBean.setPosDataCode(posDataCodeBean.populatePointOfServiceDataCode());
                    break;
            }

//            if (track2.contains("=")) {
//                track2 = track2.substring(0, (track2.indexOf('=') + 8)) + unpredictableNumber + applicationCryptogram + applicationTransactionCounter;
//                // RF Magstripe Mode
//                ignoreIccData = true;
//                posDataCodeBean.setCardDataInputMode(CardDataInputMode.POC_MAGNETIC_STRIPE_READ.getCardDataInputMode());
//                requestBean.setPosDataCode(posDataCodeBean.populatePointOfServiceDataCode());
//            }
        }
        String track2Data = track2 != null ? track2.replace("F", "") : null;
        if (track2Data != null) {
            requestBean.setTrack2Data(track2Data);
        }
        //	37	RETRIEVAL REFERENCE NUMBER
        // >>> Not used

        //	41	CARD ACCEPTOR TERMINAL IDENTIFICATION
        String cardAcceptorTerminalId = String.valueOf(gatewayConfigMap.get(CONFIG_REQUEST_CARD_ACCEPTOR_TERMINAL_ID));
        requestBean.setCardAcceptorTerminalId(cardAcceptorTerminalId);

        //	42	CARD ACCEPTOR IDENTIFICATION CODE
        String cardAcceptorIdCode = String.valueOf(gatewayConfigMap.get(CONFIG_REQUEST_CARD_ACCEPTOR_ID_CODE));
        requestBean.setCardAcceptorIdCode(cardAcceptorIdCode);

        //	43	CARD ACCEPTOR NAME/LOCATION
        // >>> Not used

        //	45	TRACK 1 DATA
        String track1 = null;
        if (CardDetectMode.SWIPE.equals(cardDetectMode)) {
            track1 = request.getTrack1DiscretionaryData();
        }
        String track1Data = track1 != null ? track1.replace("F", "") : null;
        if (track1Data != null) {
            requestBean.setTrack1Data(track1Data);
        }

        //	47	ADDITIONAL DATA - NATIONAL
        // >>> Not used

        //	48	ADDITIONAL DATA - PRIVATE
        // >>> Not used

        //	49	CURRENCY CODE, TRANSACTION
        requestBean.setTransactionCurrencyCode(request.getTransactionCurrencyCode());

        //	52	PERSONAL IDENTIFICATION NUMBER (PIN) DATA
        // >>> Not used

        //	53	SECURITY RELATED CONTROL INFORMATION
        // >>> Not used

        //	55	INTEGRATED CIRCUIT CARD SYSTEM RELATED DATA
        if (CardDetectMode.CONTACT.equals(cardDetectMode) || (CardDetectMode.CONTACTLESS.equals(cardDetectMode) && !ignoreIccData)) {
            IntegratedCircuitCardRelatedDataBean iccRelatedData = new IntegratedCircuitCardRelatedDataBean();
            // ICC HEADER VERSION NAME
            iccRelatedData.setIccHeaderVersionName("AGNS");
            // ICC HEADER VERSION NUMBER
            iccRelatedData.setIccHeaderVersionNumber("0001");
            //APPLICATION CRYPTOGRAM. User to set the data in Unsigned binary number of fixed length
            iccRelatedData.setAppCryptogram(request.getTLVHexString(EMVTag.APPLICATION_CRYPTOGRAM));
            //ISSUER APPLICATION DATA (IAD). User to set the data in Unsigned binary number of fixed length
            String kernelIAD = request.getTLVHexString(EMVTag.ISSUER_APPLICATION_DATA);
            String IAD = String.format("%02X", kernelIAD.length() / 2) + kernelIAD;
            iccRelatedData.setIssuerAppData(IAD);
            //UNPREDICTABLE NUMBER. User to set the data in Unsigned binary number of fixed length
            iccRelatedData.setUnpredictableNumber(request.getTLVHexString(EMVTag.UNPREDICTABLE_NUMBER));
            //APPLICATION TRANSACTION COUNTER (ATC). User to set the data in Unsigned binary number of fixed length
            iccRelatedData.setAppTransactionCounter(request.getTLVHexString(EMVTag.APPLICATION_TRANSACTION_COUNTER));
            //TERMINAL VERIFICATION RESULTS (TVR). User to set the data in Unsigned binary number of fixed length
            iccRelatedData.setTerminalVerificationResults(request.getTLVHexString(EMVTag.TERMINAL_VERIFICATION_RESULTS));
            //TRANSACTION DATE. User to set the data in BCD format of fixed length
            iccRelatedData.setTransactionDate(request.getTLVHexString(EMVTag.TRANSACTION_DATE));
            //TRANSACTION TYPE. User to set the data in BCD format of fixed length
            iccRelatedData.setTransactionType(request.getTLVHexString(EMVTag.TRANSACTION_TYPE));

            //AMOUNT AUTHORIZED. User to set the data in BCD format of fixed length
            iccRelatedData.setAmountAuthorized(request.getTLVHexString(EMVTag.AMOUNT_AUTHORISED_NUMERIC));
            //TRANSACTION CURRENCY CODE. User to set the data in BCD format of fixed length
            iccRelatedData.setTransactionCurrencyCode(request.getTLVHexString(EMVTag.TRANSACTION_CURRENCY_CODE));
            //TERMINAL COUNTRY CODE. User to set the data in BCD format of fixed length
            iccRelatedData.setTerminalCountryCode(request.getTLVHexString(EMVTag.TERMINAL_COUNTRY_CODE));
            //APPLICATION INTERCHANGE PROFILE (AIP). User to set the data in Unsigned binary number of fixed length
            iccRelatedData.setAppInterchangeProfile(request.getTLVHexString(EMVTag.APPLICATION_INTERCHANGE_PROFILE));
            //AMOUNT, OTHER. User to set the data in BCD format of fixed length
            iccRelatedData.setAmountOther(request.getTLVHexString(EMVTag.AMOUNT_OTHER_NUMERIC));
            //APPLICATION PAN SEQUENCE NUMBER. User to set the data in BCD format of fixed length
            iccRelatedData.setAppPANSequenceNumber(request.getTLVHexString(EMVTag.APPLICATION_PRIMARY_ACCOUNT_NUMBER_SEQUENCE_NUMBER));
            //CRYPTOGRAM INFORMATION DATA (CID). User to set the data in Unsigned binary number of fixed length
            iccRelatedData.setCryptogramInformationData(request.getTLVHexString(EMVTag.CRYPTOGRAM_INFORMATION_DATA));
            //RESERVED FOR FUTURE USE - Please do not set this datafield.
            requestBean.setIccRelatedData(iccRelatedData.populateIntegratedCircuitCardRelatedData(responseBean, requestBean.getTransactionCurrencyCode()));
        }

        //	60	NATIONAL USE DATA
        // >>> Not used

        //	61	NATIONAL USE DATA
        // >>> Not used

        //	62	PRIVATE USE DATA
        // >>> Not used

        //	63	PRIVATE USE DATA
        // >>> Not used

        //	96	KEY MANAGEMENT DATA
        // >>> Not used

        //	128	MESSAGE AUTHENTICATION CODE FIELD
        // >>> Not used

        AuthorizationService authorizationService = new AuthorizationService();
        responseBean = authorizationService.validateAuthorizationRequest(requestBean);

        System.out.println("--------------------------------------------------");
        System.out.println(requestBean.toString());
        System.out.println("--------------------------------------------------");

        if (responseBean.getAuthErrorList() == null || responseBean.getAuthErrorList().isEmpty()) {
            PropertyReader propertyReader = new PropertyReader();
            propertyReader = propertyReader.setProxyDetailsFile("/authorization.properties");
            PropertyReader.setLOG_FILE_PATH(String.valueOf(gatewayConfigMap.get(CONFIG_SDK_LOG_FILE_PATH)));
            PropertyReader.setORIGIN(String.valueOf(gatewayConfigMap.get(CONFIG_SDK_ORIGIN)));
            PropertyReader.setMERCHANT_COUNTRY(String.valueOf(gatewayConfigMap.get(CONFIG_SDK_MERCHANT_COUNTRY)));
            PropertyReader.setREGION(String.valueOf(gatewayConfigMap.get(CONFIG_SDK_REGION)));
            PropertyReader.setMESSAGE_TYPE(String.valueOf(gatewayConfigMap.get(CONFIG_SDK_MESSAGE_TYPE)));
            PropertyReader.setMERCHANT_NUMBER(String.valueOf(gatewayConfigMap.get(CONFIG_SDK_MERCHANT_NUMBER)));
            PropertyReader.setROUTING_INDICATOR(String.valueOf(gatewayConfigMap.get(CONFIG_SDK_ROUTING_INDICATOR)));
            PropertyReader.setPROXY("NO");
            PropertyReader.setHttpsTimeout(String.valueOf(gatewayConfigMap.get(CONFIG_SDK_HTTPS_TIMEOUT)));
            PropertyReader.setReadTimeout(String.valueOf(gatewayConfigMap.get(CONFIG_SDK_READ_TIMEOUT)));
            try {
                responseBean = authorizationService.createAndSendAuthorizationRequest(requestBean, propertyReader);
                IntegratedCircuitCardRelatedDataBean iccRelatedDataBean = responseBean.getIccRelatedDataBean();

                String actionCode = responseBean.getActionCode();
                String actionCodeDescription = responseBean.getActionCodeDescription();
                String messageAuthenticationCode = responseBean.getMessageAuthenticationCode();
                String issuerScriptData = null;
                String issuerAuthenticationData = null;

                if (iccRelatedDataBean != null) {
                    issuerScriptData = iccRelatedDataBean.getIssuerScriptData();
                    String iccRelatedData = responseBean.getIccRelatedData();
                    try {
                        issuerAuthenticationData = iccRelatedData.substring(8, 24);
                        String responseCode = iccRelatedData.substring(24, 26);
                        issuerAuthenticationData += DataConversion.convertASCIIToHexString(responseCode);
                    } catch (StringIndexOutOfBoundsException e) {
                        // 忽略该错误，代表不需要处理 Issuer Authentication Data 数据
                    }
                }


                boolean isApproved = "000".equals(actionCode) || "001".equals(actionCode) || "002".equals(actionCode);
                response.setTransactionId(responseBean.getTransactionId());
                response.setNeedCallIssuerConfirm("107".equals(actionCode));
                response.setApproved(isApproved);
                response.setApprovalCode(responseBean.getApprovalCode());
                response.setActionCode(actionCode);
                response.setActionCodeDescription(actionCodeDescription);

                response.setIccRelatedData(responseBean.getIccRelatedData());
                response.setIccRelatedDataIssuerScriptData(issuerScriptData);
                response.setIccRelatedDataIssuerAuthenticationData(issuerAuthenticationData);

                System.out.println("--------------------------------------------------");
                System.out.println(responseBean.toString());
                System.out.println("--------------------------------------------------");
            } catch (Exception e) {
                Throwable cause = e.getCause();

                String code = Response.Error.ERROR_CODE_UNKNOWN;
                String message = cause.getLocalizedMessage();

                if (cause instanceof UnknownHostException) {
                    code = Response.Error.ERROR_CODE_UNKNOWN_HOST;
                } else if (cause instanceof SocketTimeoutException) {
                    code = Response.Error.ERROR_CODE_TIMEOUT;
                }
                errors.add(new Response.Error(code, message));
            }

            authorizationService.flushRequestMemory(requestBean);
            authorizationService.flushResponseMemory(responseBean);
        } else {
            String message = "";
            System.out.println("--------------------------------------------------");
            Iterator<ErrorObject> errorList = responseBean.getAuthErrorList().iterator();
            while (errorList.hasNext()) {
                ErrorObject errorObject = errorList.next();
                System.out.println(errorObject.getErrorCode() + "-" + errorObject.getErrorDescription());
                message += errorObject.getErrorCode() + "-" + errorObject.getErrorDescription() + "\n";

                Response.Error error = new Response.Error(errorObject.getErrorCode(), errorObject.getErrorDescription());
                errors.add(error);
            }
            System.out.println("--------------------------------------------------");
        }
        if (errors.size() > 0) {
            response.setErrors(errors);
        }
        return response;
    }

    private Response reversal(Request request) {
        HashMap<String, String> gatewayConfigMap = new HashMap<>();

        for (String key : mDefaultGatewayConfig.keySet()) {
            String value = (String) mDefaultGatewayConfig.get(key);
            gatewayConfigMap.put(key, value);
        }

        if (request.getGatewayConfigs() != null && request.getGatewayConfigs().size() > 0) {
            HashMap<String, String> configMap = request.getGatewayConfigs();
            for (String key : configMap.keySet()) {
                String value = configMap.get(key);
                gatewayConfigMap.put(key, value);
            }
        }

        request.setGatewayConfigs(gatewayConfigMap);
        Response response = new Response(request);
        List<Response.Error> errors = new ArrayList<>();

        String cardDetectMode = request.getCardDetectMode();

        /******************************************************************
         *Sample code for creating Reversal Advice Request Message (1420)
         ******************************************************************/
        AuthorizationRequestBean requestBean = new AuthorizationRequestBean();

        AuthorizationResponseBean responseBean = new AuthorizationResponseBean();

        requestBean.setMessageTypeIdentifier("1420");

        //PRIMARY ACCOUNT NUMBER (PAN) - Datafield 2
        requestBean.setPrimaryAccountNumber(new StringBuffer(request.getPrimaryAccountNumber()));

        //PROCESSING CODE - Datafield 3 sample value for Processing Code = 004800
        requestBean.setProcessingCode(ProcessingCode.CARD_AUTHORIZATION_REQUEST.getProcessingCode());

        // AMOUNT,TRANSACTION - Datafield 4
        String amountTransaction = new DecimalFormat("0000000000.00").format(request.getTransactionAmount()).replace(".", "");
        requestBean.setAmountTransaction(amountTransaction);

        //DATE AND TIME, TRANSMISSION - Datafield 7

        //SYSTEMS TRACE AUDIT NUMBER - Datafield 11
        String systemTraceAuditNumber = String.format("%06d", request.getTransactionSystemTraceAuditNumber());
        requestBean.setSystemTraceAuditNumber(systemTraceAuditNumber);

        //DATE AND TIME, LOCAL TRANSACTION - Datafield 12
        requestBean.setLocalTransactionDateAndTime(request.getTransactionDateAndTime());

        //DATE, EXPIRATION - Datafield 14
        requestBean.setCardExpirationDate(request.getCardExpirationDate());

        //COUNTRY CODE, ACQUIRING INSTITUTION - Datafield 19
        requestBean.setMerchantLocationCountryCode(CountryCode.HongKong.getCountryCode());

        //POINT OF SERVICE DATA CODE - Datafield 22 100000154000
        POSDataCodeBean posDataCodeBean = new POSDataCodeBean();
        // Position 1 - Card Data Input Capability
        posDataCodeBean.setCardDataInput(CardDataInput.POC_INTEGERATED_CIRCUIT_CARD.getCardDataInput());

        // Position 2 - Cardholder Authentication Capability
        posDataCodeBean.setCardholderAuthentication(CardholderAuthentication.POC_UNKNOWN.getCardholderAuthentication());

        // Position 3 - Card Capture Capability
        posDataCodeBean.setCardCapture(CardCapture.POC_CAPTURE.getCardCapture());

        // Position 4 - Operating Environment
        posDataCodeBean.setOperatingEnvironment(OperatingEnvironment.POC_ON_PREMISES_OF_CARD_ACCEPTOR_ATTENDED.getOperatingEnvironment());

        // Position 5 - Cardholder Present
        posDataCodeBean.setCardholderPresent(CardholderPresent.POC_CARDMEMBER_PRESENT.getCardholderPresent());

        // Position 6 - Card Present
        posDataCodeBean.setCardPresent(CardPresent.POC_CARD_NOT_PRESENT.getCardPresent());

        // Position 7 - Card Data Input Mode
        posDataCodeBean.setCardDataInputMode(CardDataInputMode.POC_UNKNOWN.getCardDataInputMode());

        // Position 8 - Cardmember Authentication Method
        posDataCodeBean.setCardMemberAuthenticationMethod(CardMemberAuthenticationMethod.POC_UNKNOWN.getCardMemberAuthenticationMethod());

        // Position 9 - Cardmember Authentication Entity
        posDataCodeBean.setCardMemberAuthenticationEntity(CardMemberAuthenticationEntity.POC_INTEGRATED_CKT_CARD.getCardMemberAuthenticationEntity());

        // Position 10 - Card Data Output Capability
        posDataCodeBean.setCardDataOutput(CardDataOutput.INTEGRATED_CIRCUIT_CARD.getCardDataOutput());

        // Position 11 - Terminal Output Capability
        posDataCodeBean.setTerminalOutput(TerminalOutput.POC_PRINTING_AND_DISPLAY.getTerminalOutput());

        // Position 12 - PIN Capture Capability
        posDataCodeBean.setPinCapture(PinCapture.POC_TWELVE_CHARS.getPinCapture());


        switch (cardDetectMode) {
            case CardDetectMode.CONTACT:
                posDataCodeBean.setCardPresent(CardPresent.POC_CARD_PRESENT.getCardPresent());
                posDataCodeBean.setCardDataInputMode(CardDataInputMode.POC_INTEGRATED_CKT_CARD.getCardDataInputMode());
                break;
            case CardDetectMode.CONTACTLESS:
                posDataCodeBean.setCardPresent(CardPresent.POC_CONTACTLESS_TRANSACTION.getCardPresent());
                posDataCodeBean.setCardDataInputMode(CardDataInputMode.POC_INTEGRATED_CKT_CARD.getCardDataInputMode());
                break;
            case CardDetectMode.SWIPE:
                posDataCodeBean.setCardPresent(CardPresent.POC_CARD_PRESENT.getCardPresent());
                posDataCodeBean.setCardDataInputMode(CardDataInputMode.POC_MAGNETIC_STRIPE_READ.getCardDataInputMode());
                break;
            case CardDetectMode.FALLBACK_SWIPE:
                posDataCodeBean.setCardPresent(CardPresent.POC_CARD_PRESENT.getCardPresent());
                posDataCodeBean.setCardDataInputMode(CardDataInputMode.POC_TECHNICAL_FALLBACK.getCardDataInputMode());
                break;
            case CardDetectMode.MANUAL:
                posDataCodeBean.setCardPresent(CardPresent.POC_CARD_PRESENT.getCardPresent());
                posDataCodeBean.setCardDataInputMode(CardDataInputMode.POC_MANUAL.getCardDataInputMode());
                break;
        }

        requestBean.setPosDataCode(posDataCodeBean.populatePointOfServiceDataCode());

        //MESSAGE REASON CODE - Datafield 25
        requestBean.setMessageReasonCode(MessageReasonCode.value2.getMessageReasonCode());

        //CARD ACCEPTOR BUSINESS CODE - Datafield 26
        requestBean.setCardAcceptorBusinessCode(MCCCode.Department_Stores.getMccCode());

        //ACQUIRER REFERENCE DATA - Datafield 31
//        authRequestBean.setTransactionId("123456789012345");

        //ACQUIRING INSTITUTION IDENTIFICATION CODE - Datafield 32
//        authRequestBean.setAcquiringInstitutionIdCode("41238912345");

        //FORWARDING INSTITUTION IDENTIFICATION CODE - Datafield 33
//        authRequestBean.setForwardingInstitutionIdCode("45678312345");

        //RETRIEVAL REFERENCE NUMBER - Datafield 37
//        authRequestBean.setRetrievalReferenceNumber("112341130016");

        //CARD ACCEPTOR TERMINAL IDENTIFICATION - Datafield 41
        String cardAcceptorTerminalId = String.valueOf(gatewayConfigMap.get(CONFIG_REQUEST_CARD_ACCEPTOR_TERMINAL_ID));
        requestBean.setCardAcceptorTerminalId(cardAcceptorTerminalId);

        //CARD ACCEPTOR IDENTIFICATION CODE - Datafield 42
        String cardAcceptorIdCode = String.valueOf(gatewayConfigMap.get(CONFIG_REQUEST_CARD_ACCEPTOR_ID_CODE));
        requestBean.setCardAcceptorIdCode(cardAcceptorIdCode);

        //CURRENCY CODE, TRANSACTION - Datafield 49
        requestBean.setTransactionCurrencyCode(request.getTransactionCurrencyCode());

        //ORIGINAL DATA ELEMENTS ñ Datafield 56
        OriginalDataElementsBean originalDataElementsBean = new OriginalDataElementsBean();
        //MESSAGE TYPE IDENTIFIER ñ subfield1
        originalDataElementsBean.setMessageTypeIdOriginal("1100");
        //SYSTEM TRACE AUDIT NUMBERñ subfield2
        originalDataElementsBean.setSystemTraceAuditNumberOriginal(String.format("%06d", request.getOriginalTransactionSystemTraceAuditNumber()));
        //ACQUIRING INSTITUTION IDENTIFICATION CODE
        originalDataElementsBean.setAcquiringInstIdOriginal("\\");
        //DATE AND TIME, LOCAL TRANSACTION
        originalDataElementsBean.setLocalTransactionDateAndTimeOriginal(request.getOriginalTransactionDateAndTime());
        requestBean.setOriginalDataElements(originalDataElementsBean.populateOriginalDataElement(responseBean));

        AuthorizationService authorizationService = new AuthorizationService();
        responseBean = authorizationService.validateAuthorizationRequest(requestBean);

        if (responseBean.getAuthErrorList() == null || responseBean.getAuthErrorList().isEmpty()) {
            PropertyReader propertyReader = new PropertyReader();
            propertyReader = propertyReader.setProxyDetailsFile("/authorization.properties");
            PropertyReader.setLOG_FILE_PATH(String.valueOf(gatewayConfigMap.get(CONFIG_SDK_LOG_FILE_PATH)));
            PropertyReader.setORIGIN(String.valueOf(gatewayConfigMap.get(CONFIG_SDK_ORIGIN)));
            PropertyReader.setMERCHANT_COUNTRY(String.valueOf(gatewayConfigMap.get(CONFIG_SDK_MERCHANT_COUNTRY)));
            PropertyReader.setREGION(String.valueOf(gatewayConfigMap.get(CONFIG_SDK_REGION)));
            PropertyReader.setMESSAGE_TYPE(String.valueOf(gatewayConfigMap.get(CONFIG_SDK_MESSAGE_TYPE)));
            PropertyReader.setMERCHANT_NUMBER(String.valueOf(gatewayConfigMap.get(CONFIG_SDK_MERCHANT_NUMBER)));
            PropertyReader.setROUTING_INDICATOR(String.valueOf(gatewayConfigMap.get(CONFIG_SDK_ROUTING_INDICATOR)));
            PropertyReader.setPROXY("NO");
            PropertyReader.setHttpsTimeout(String.valueOf(gatewayConfigMap.get(CONFIG_SDK_HTTPS_TIMEOUT)));
            PropertyReader.setReadTimeout(String.valueOf(gatewayConfigMap.get(CONFIG_SDK_READ_TIMEOUT)));
            try {

                System.out.println("--------------------------------------------------");
                System.out.println(requestBean.toString());
                System.out.println("--------------------------------------------------");

                responseBean = authorizationService.createAndSendAuthorizationRequest(requestBean, propertyReader);

                String actionCode = responseBean.getActionCode();
                String actionCodeDescription = responseBean.getActionCodeDescription();


                response.setApproved("400".equals(actionCode));
                response.setApprovalCode(responseBean.getApprovalCode());
                response.setActionCode(actionCode);
                response.setActionCodeDescription(actionCodeDescription);

                System.out.println("--------------------------------------------------");
                System.out.println(responseBean.toString());
                System.out.println("--------------------------------------------------");

                authorizationService.flushRequestMemory(requestBean);
                authorizationService.flushResponseMemory(responseBean);
            } catch (Exception e) {
                Throwable cause = e.getCause();

                String code = Response.Error.ERROR_CODE_UNKNOWN;
                String message = cause.getLocalizedMessage();

                if (cause instanceof UnknownHostException) {
                    code = Response.Error.ERROR_CODE_UNKNOWN_HOST;
                } else if (cause instanceof SocketTimeoutException) {
                    code = Response.Error.ERROR_CODE_TIMEOUT;
                }
                errors.add(new Response.Error(code, message));
            }
        } else {
            String message = "";
            System.out.println("--------------------------------------------------");
            Iterator<ErrorObject> errorList = responseBean.getAuthErrorList().iterator();
            while (errorList.hasNext()) {
                ErrorObject errorObject = errorList.next();
                System.out.println(errorObject.getErrorCode() + "-" + errorObject.getErrorDescription());
                message += errorObject.getErrorCode() + "-" + errorObject.getErrorDescription() + "\n";

                Response.Error error = new Response.Error(errorObject.getErrorCode(), errorObject.getErrorDescription());
                errors.add(error);
            }
            System.out.println("--------------------------------------------------");
        }
        if (errors.size() > 0) {
            response.setErrors(errors);
        }

        return response;
    }
}
