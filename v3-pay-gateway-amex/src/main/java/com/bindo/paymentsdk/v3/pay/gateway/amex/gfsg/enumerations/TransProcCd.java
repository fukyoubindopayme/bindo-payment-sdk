package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations;

public enum TransProcCd {
    CREDIT("200000"),
    DEBIT("000000"),
    DEBIT_VOID("999999"),
    CREDIT_VOID("299999");

    private final String transProcCd;

    private TransProcCd(String transProcCd) {
        this.transProcCd = transProcCd;
    }

    public String getTransProcCd() {
        return this.transProcCd;
    }
}
