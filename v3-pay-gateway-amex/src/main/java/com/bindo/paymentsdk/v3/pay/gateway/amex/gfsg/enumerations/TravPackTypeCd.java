package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations;

public enum TravPackTypeCd {
    Car_package("C"),
    Airline_package("A"),
    Both_car_airline_package("B"),
    Unknown("N");

    private final String travPackTypeCd;

    public String getTravPackTypeCd() {
        return this.travPackTypeCd;
    }

    private TravPackTypeCd(String travPackTypeCd) {
        this.travPackTypeCd = travPackTypeCd;
    }
}
