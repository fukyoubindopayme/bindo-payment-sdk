package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.utils;

public class Util
{
  public static byte[] decode(String ascii)
    throws IndexOutOfBoundsException
  {
    int oddLength = ascii.length() % 2;
    byte[] res = new byte[ascii.length() / 2 + oddLength];
    int i = 0;
    if (oddLength == 1) {
      res[(i++)] = ((byte)Character.digit(ascii.charAt(0), 16));
    }
    for (i = oddLength; i < res.length; i++)
    {
      int v = Character.digit(ascii.charAt(i * 2), 16) << 4;
      v |= Character.digit(ascii.charAt(i * 2 + 1), 16);
      res[i] = ((byte)v);
    }
    return res;
  }
  
  public static String encode(byte[] data)
    throws IndexOutOfBoundsException
  {
    StringBuffer buf = new StringBuffer(data.length * 2);
    char[] CHARACTERS = { '0', '1', '2', '3', '4', '5', '6', '7', 
      '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
    for (int i = 0; i < data.length; i++)
    {
      char c = CHARACTERS[(data[i] >> 4 & 0xF)];
      buf.append(c);
      c = CHARACTERS[(data[i] & 0xF)];
      buf.append(c);
    }
    return buf.toString();
  }
  
  public static boolean IsNullOrEmpty(String token)
  {
    if (("".equals(token)) || (token == null)) {
      return true;
    }
    return false;
  }
}
