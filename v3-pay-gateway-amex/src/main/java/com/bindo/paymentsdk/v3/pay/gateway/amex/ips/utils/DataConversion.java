package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.utils;

import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.connection.PropertyReader;
import java.io.IOException;
import java.io.StringReader;

public class DataConversion {
    static boolean debugflag = PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON");
    private static final char[] ASCII_translate_EBCDIC = new char[]{'\u0000', '\u0001', '\u0002', '\u0003', '\u0004', '\u0005', '\u0006', '\u0007', '\b', '\t', '\n', '\u000b', '\f', '\r', '\u000e', '\u000f', '\u0010', '\u0011', '\u0012', '\u0013', '\u0014', '\u0015', '\u0016', '\u0017', '\u0018', '\u0019', '\u001a', '\u001b', '\u001c', '\u001d', '\u001e', '\u001f', '@', 'Z', '\u007f', '{', '[', 'l', 'P', '}', 'M', ']', '\\', 'N', 'k', '`', 'K', 'a', 'ð', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', '÷', 'ø', 'ù', 'z', '^', 'L', '~', 'n', 'o', '|', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', '×', 'Ø', 'Ù', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', '\u00ad', 'à', '½', '_', 'm', '}', '\u0081', '\u0082', '\u0083', '\u0084', '\u0085', '\u0086', '\u0087', '\u0088', '\u0089', '\u0091', '\u0092', '\u0093', '\u0094', '\u0095', '\u0096', '\u0097', '\u0098', '\u0099', '¢', '£', '¤', '¥', '¦', '§', '¨', '©', 'À', 'j', 'Ð', '¡', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'K'};
    private static final char[] EBCDIC_translate_ASCII = new char[]{'\u0000', '\u0001', '\u0002', '\u0003', '\u0004', '\u0005', '\u0006', '\u0007', '\b', '\t', '\n', '\u000b', '\f', '\r', '\u000e', '\u000f', '\u0010', '\u0011', '\u0012', '\u0013', '\u0014', '\u0015', '\u0016', '\u0017', '\u0018', '\u0019', '\u001a', '\u001b', '\u001c', '\u001d', '\u001e', '\u001f', ' ', '!', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', '.', '.', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '.', '?', ' ', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '<', '(', '+', '|', '&', '.', '.', '.', '.', '.', '.', '.', '.', '.', '!', '$', '*', ')', ';', '^', '-', '/', '.', '.', '.', '.', '.', '.', '.', '.', '|', ',', '%', '_', '>', '?', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', ':', '#', '@', '\'', '=', '"', '.', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', '.', '.', '.', '.', '.', '.', '.', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', '.', '.', '.', '.', '.', '.', '.', '~', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '.', '.', '.', '[', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', ']', '.', '.', '{', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', '.', '.', '.', '.', '.', '.', '}', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', '.', '.', '.', '.', '.', '.', '\\', '.', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '.', '.', '.', '.', '.', '.', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', '.', '.', '.', '.', '.'};

    public DataConversion() {
    }

    public static String convertASCIIToEBCDIC(String input) {
        StringBuffer output = new StringBuffer("");

        for(int i = 0; i < input.length(); ++i) {
            char temp = input.charAt(i);
            output.append(ASCII_translate_EBCDIC[temp]);
        }

        return output.toString();
    }

    public static String convertASCIIToEBCDICString(String input) {
        StringBuffer output = new StringBuffer("");

        for(int i = 0; i < input.length(); ++i) {
            char temp = input.charAt(i);
            int x = ASCII_translate_EBCDIC[temp];
            output.append(Integer.toHexString(x).toUpperCase());
        }

        return output.toString();
    }

    public static String convertEBCDICToASCII(String input) {
        StringBuffer output = new StringBuffer("");

        for(int i = 0; i < input.length(); ++i) {
            char temp = input.charAt(i);
            output.append(EBCDIC_translate_ASCII[temp]);
        }

        return output.toString();
    }

    public static String convertStringToEBCDIC(String input) {
        StringReader inp = new StringReader(input);
        StringBuffer output = new StringBuffer("");

        try {
            int ch = inp.read();

            for(int ch1 = inp.read(); ch != -1 && ch1 != -1; ch1 = inp.read()) {
                int temp = Character.digit((char)ch, 16);
                int temp1 = Character.digit((char)ch1, 16);
                temp <<= 4;
                temp |= temp1;
                output.append((char)temp);
                ch = inp.read();
            }
        } catch (IOException var8) {
            if (debugflag) {
                Log.e("Error in convertStringToEBCDIC() method..  IOException: " + var8);
            }
        }

        return output.toString();
    }

    public static String convertStringToASCII(String input) {
        StringReader inp = new StringReader(input);
        StringBuffer output = new StringBuffer("");

        try {
            int ch = inp.read();

            for(int ch1 = inp.read(); ch != -1 && ch1 != -1; ch1 = inp.read()) {
                int temp = Character.digit((char)ch, 16);
                int temp1 = Character.digit((char)ch1, 16);
                temp <<= 4;
                temp |= temp1;
                output.append((char)temp);
                ch = inp.read();
            }
        } catch (IOException var8) {
            if (debugflag) {
                Log.e("Error in convertStringToASCII() method..  IOException: " + var8);
            }
        }

        return convertEBCDICToASCII(output.toString());
    }

    public static String convertHexToByte(String hexInput) {
        String binOutput = "";
        String binResult = "";

        for(int i = 0; i < hexInput.length(); ++i) {
            binOutput = Integer.toBinaryString(Character.digit(hexInput.charAt(i), 16));
            binOutput = "0000" + binOutput;
            binOutput = binOutput.substring(binOutput.length() - 4);
            binResult = binResult + binOutput;
        }

        return binResult;
    }

    public static String convertHexToASCII(String hexInput) {
        StringBuffer str = new StringBuffer();

        for(int iCount = 1; iCount < hexInput.length(); iCount += 2) {
            int ch = Integer.valueOf("" + hexInput.charAt(iCount - 1) + hexInput.charAt(iCount), 16);
            str.append((char)ch);
        }

        return str.toString();
    }

    public static String convertNumericsToBCD(String getValue) {
        String[] BCD = new String[]{"0000", "0001", "0010", "0011", "0100", "0101", "0110", "0111", "1000", "1001"};
        StringBuffer sb = new StringBuffer();

        for(int iCount = 0; iCount < getValue.length(); ++iCount) {
            char temp = getValue.charAt(iCount);
            int temp1 = Integer.parseInt("" + temp);
            sb.append(BCD[temp1]);
        }

        return sb.toString();
    }

    public static byte[] decode(String ascii) {
        int oddLength = ascii.length() % 2;
        byte[] res = new byte[ascii.length() / 2 + oddLength];
        int iCount = 0;
        if (oddLength == 1) {
            iCount = iCount + 1;
            res[iCount] = (byte)Character.digit(ascii.charAt(0), 16);
        }

        for(iCount = oddLength; iCount < res.length; ++iCount) {
            int v = Character.digit(ascii.charAt(iCount * 2), 16) << 4;
            v |= Character.digit(ascii.charAt(iCount * 2 + 1), 16);
            res[iCount] = (byte)v;
        }

        return res;
    }

    public static String convertASCIIToHexString(String input) {
        StringBuffer output = new StringBuffer("");

        for(int i = 0; i < input.length(); ++i) {
            char temp = input.charAt(i);
            output.append(Integer.toHexString(temp).toUpperCase());
        }

        return output.toString();
    }

    public static String removeHexMessageLength(String message) {
        String respLength = message.substring(0, 4);
        StringBuffer str = new StringBuffer();

        int num;
        for(num = 1; num < respLength.length(); num += 2) {
            Integer ch = Integer.valueOf("" + respLength.charAt(num - 1) + respLength.charAt(num), 16);
            str.append(ch);
        }

        num = Integer.parseInt(str.toString());
        String isoResponseMessage = message.substring(4, message.length());
        return isoResponseMessage.substring(0, num * 2 - 4);
    }

    public static int convertHEXToDecimal(String str) {
        String digits = "0123456789ABCDEF";
        str = str.toUpperCase();
        int val = 0;

        for(int i = 0; i < str.length(); ++i) {
            char c = str.charAt(i);
            int d = digits.indexOf(c);
            val = 16 * val + d;
        }

        return val;
    }
}
