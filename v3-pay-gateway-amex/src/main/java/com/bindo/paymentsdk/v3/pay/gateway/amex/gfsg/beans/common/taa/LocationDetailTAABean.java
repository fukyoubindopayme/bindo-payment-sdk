package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.RecordType;

public class LocationDetailTAABean
{
  private String recordType = RecordType.Transaction_Advice_Addendum.getRecordType();
  private String recordNumber;
  private String transactionIdentifier;
  private String ltaaDataField4Reserved;
  private String addendaTypeCode;
  private String locationName;
  private String locationAddress;
  private String locationCity;
  private String locationRegion;
  private String locationCountryCode;
  private String locationPostalCode;
  private String merchantCategoryCode;
  private String sellerId;
  private String ltaaDataField14Reserved;
  
  public String getRecordType()
  {
    return this.recordType;
  }
  
  public void setRecordType(String recordType)
  {
    this.recordType = recordType;
  }
  
  public String getRecordNumber()
  {
    return this.recordNumber;
  }
  
  public void setRecordNumber(String recordNumber)
  {
    this.recordNumber = recordNumber;
  }
  
  public String getTransactionIdentifier()
  {
    return this.transactionIdentifier;
  }
  
  public void setTransactionIdentifier(String transactionIdentifier)
  {
    this.transactionIdentifier = transactionIdentifier;
  }
  
  public String getLtaaDataField4Reserved()
  {
    return this.ltaaDataField4Reserved;
  }
  
  public void setLtaaDataField4Reserved(String ltaaDataField4Reserved)
  {
    this.ltaaDataField4Reserved = ltaaDataField4Reserved;
  }
  
  public String getAddendaTypeCode()
  {
    return this.addendaTypeCode;
  }
  
  public void setAddendaTypeCode(String addendaTypeCode)
  {
    this.addendaTypeCode = addendaTypeCode;
  }
  
  public String getLocationName()
  {
    return this.locationName;
  }
  
  public void setLocationName(String locationName)
  {
    this.locationName = locationName;
  }
  
  public String getLocationAddress()
  {
    return this.locationAddress;
  }
  
  public void setLocationAddress(String locationAddress)
  {
    this.locationAddress = locationAddress;
  }
  
  public String getLocationCity()
  {
    return this.locationCity;
  }
  
  public void setLocationCity(String locationCity)
  {
    this.locationCity = locationCity;
  }
  
  public String getLocationRegion()
  {
    return this.locationRegion;
  }
  
  public void setLocationRegion(String locationRegion)
  {
    this.locationRegion = locationRegion;
  }
  
  public String getLocationCountryCode()
  {
    return this.locationCountryCode;
  }
  
  public void setLocationCountryCode(String locationCountryCode)
  {
    this.locationCountryCode = locationCountryCode;
  }
  
  public String getLocationPostalCode()
  {
    return this.locationPostalCode;
  }
  
  public void setLocationPostalCode(String locationPostalCode)
  {
    this.locationPostalCode = locationPostalCode;
  }
  
  public String getMerchantCategoryCode()
  {
    return this.merchantCategoryCode;
  }
  
  public void setMerchantCategoryCode(String merchantCategoryCode)
  {
    this.merchantCategoryCode = merchantCategoryCode;
  }
  
  public String getSellerId()
  {
    return this.sellerId;
  }
  
  public void setSellerId(String sellerId)
  {
    this.sellerId = sellerId;
  }
  
  public String getLtaaDataField14Reserved()
  {
    return this.ltaaDataField14Reserved;
  }
  
  public void setLtaaDataField14Reserved(String ltaaDataField14Reserved)
  {
    this.ltaaDataField14Reserved = ltaaDataField14Reserved;
  }
}
