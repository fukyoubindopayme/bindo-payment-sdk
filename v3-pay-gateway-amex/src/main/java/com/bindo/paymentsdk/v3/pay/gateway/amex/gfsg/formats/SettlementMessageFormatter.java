package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.formats;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.SettlementRequestDataObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceAddendumBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceBasicBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionTBTSpecificBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.IndustryTypeTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.LocationDetailTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.NonIndustrySpecificTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.connection.PropertyReader;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.CommonValidator;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

public class SettlementMessageFormatter
{
  private static final Logger LOGGER = Logger.getLogger(SettlementMessageFormatter.class);
  
  public static List<String> formatISORequest(SettlementRequestDataObject settlementReqBean)
    throws IOException
  {
    List<String> formatedMessageList = new ArrayList();
    
    TFHRFormatter.formatTFHRecord(settlementReqBean.getTransactionFileHeaderType(), formatedMessageList);
    if ((settlementReqBean.getTransactionTBTSpecificType() != null) && (!settlementReqBean.getTransactionTBTSpecificType().isEmpty())) {
      for (TransactionTBTSpecificBean transactionTBTSpecifictType : settlementReqBean.getTransactionTBTSpecificType())
      {
        if ((transactionTBTSpecifictType.getTransactionAdviceBasicType() != null) && (!transactionTBTSpecifictType.getTransactionAdviceBasicType().isEmpty())) {
          for (TransactionAdviceBasicBean transactionAdviceBasicType : transactionTBTSpecifictType.getTransactionAdviceBasicType()) {
            formatTABRecordType(transactionAdviceBasicType, formatedMessageList);
          }
        }
        TBTRecordFormatter.formatTBTRecord(transactionTBTSpecifictType.getTransactionBatchTrailerBean(), formatedMessageList);
      }
    }
    TFSRecordFormatter.formatTFSRecord(settlementReqBean.getTransactionFileSummaryType(), formatedMessageList);
    
    return formatedMessageList;
  }
  
  public static void formatTABRecordType(TransactionAdviceBasicBean transactionAdviceBasicBean, List<String> formatedMessageList)
    throws IOException
  {
    StringBuffer formattedMessage = new StringBuffer();
    
    formatValue(transactionAdviceBasicBean.getRecordType(), formattedMessage, 3, false, true, true, false, true);
    
    formatValue(transactionAdviceBasicBean.getRecordNumber(), formattedMessage, 8, true, false, false, true, false);
    
    formatValue(transactionAdviceBasicBean.getTransactionIdentifier(), formattedMessage, 15, true, false, false, true, false);
    
    formatValue(transactionAdviceBasicBean.getFormatCode(), formattedMessage, 2, false, true, false, false, true);
    
    formatValue(transactionAdviceBasicBean.getMediaCode(), formattedMessage, 2, false, true, false, false, true);
    
    formatValue(transactionAdviceBasicBean.getSubmissionMethod(), formattedMessage, 2, false, true, false, false, true);
    
    characterSpacesFill(formattedMessage, 10);
    
    formatValue(transactionAdviceBasicBean.getApprovalCode(), formattedMessage, 6, false, true, false, false, true);
    
    formatPANNumber(transactionAdviceBasicBean.getPrimaryAccountNumber(), formattedMessage, 19, false, true, false, false, true);
    
    formatValue(transactionAdviceBasicBean.getCardExpiryDate(), formattedMessage, 4, true, false, false, true, false);
    
    formatValue(transactionAdviceBasicBean.getTransactionDate(), formattedMessage, 8, true, false, false, true, false);
    
    formatValue(transactionAdviceBasicBean.getTransactionTime(), formattedMessage, 6, true, false, false, true, false);
    
    zeroFill(formattedMessage, 3);
    
    formatValue(transactionAdviceBasicBean.getTransactionAmount(), formattedMessage, 12, true, false, false, true, false);
    
    formatValue(transactionAdviceBasicBean.getProcessingCode(), formattedMessage, 6, true, false, false, true, false);
    
    formatValue(transactionAdviceBasicBean.getTransactionCurrencyCode(), formattedMessage, 3, false, true, true, false, true);
    
    formatValue(transactionAdviceBasicBean.getExtendedPaymentData(), formattedMessage, 2, true, false, false, true, false);
    
    formatValue(transactionAdviceBasicBean.getMerchantId(), formattedMessage, 15, false, true, false, false, true);
    
    formatValue(transactionAdviceBasicBean.getMerchantLocationId(), formattedMessage, 15, false, true, true, false, true);
    
    formatValue(transactionAdviceBasicBean.getMerchantContactInfo(), formattedMessage, 40, false, true, true, false, true);
    
    formatValue(transactionAdviceBasicBean.getTerminalId(), formattedMessage, 8, false, true, true, false, true);
    
    formatValue(transactionAdviceBasicBean.getPosDataCode(), formattedMessage, 12, false, true, true, false, true);
    
    zeroFill(formattedMessage, 3);
    
    zeroFill(formattedMessage, 12);
    
    characterSpacesFill(formattedMessage, 3);
    
    formatValue(transactionAdviceBasicBean.getInvoiceReferenceNumber(), formattedMessage, 30, false, true, true, false, true);
    
    characterSpacesFill(formattedMessage, 15);
    
    formatValue(transactionAdviceBasicBean.getTabImageSequenceNumber(), formattedMessage, 8, false, true, false, false, true);
    
    formatValue(transactionAdviceBasicBean.getMatchingKeyType(), formattedMessage, 2, false, true, false, false, true);
    
    formatValue(transactionAdviceBasicBean.getMatchingKey(), formattedMessage, 21, false, true, true, false, true);
    
    formatValue(transactionAdviceBasicBean.getElectronicCommerceIndicator(), formattedMessage, 2, false, true, false, false, true);
    if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
      characterSpacesFill(formattedMessage, 403);
    }
    formatedMessageList.add(formattedMessage.toString());
    if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
      LOGGER.info("Formated TAB Type Record :" + formattedMessage.toString());
    }
    if (transactionAdviceBasicBean.getTransactionAdviceDetailBean() != null) {
      TADRecordFormatter.formatTADRecord(transactionAdviceBasicBean.getTransactionAdviceDetailBean(), formatedMessageList);
    }
    ArrayList<TransactionAdviceAddendumBean> taaBeanList = transactionAdviceBasicBean.getArrayTAABean();
    if (taaBeanList != null) {
      for (TransactionAdviceAddendumBean taaBean : taaBeanList)
      {
        if ((taaBean instanceof NonIndustrySpecificTAABean))
        {
          NonIndustrySpecificTAABean nonIndustrySpecificTAABean = (NonIndustrySpecificTAABean)taaBean;
          
          NonIndustryTAARecordFormatter.formatNonIndustrySpecificTypeTAARecord(nonIndustrySpecificTAABean, formatedMessageList);
        }
        if ((taaBean instanceof IndustryTypeTAABean))
        {
          IndustryTypeTAABean industrySpecificTAABean = (IndustryTypeTAABean)taaBean;
          IndustryTAARecordFormatter.formatIndustrySpecificTypeTAARecord(industrySpecificTAABean, formatedMessageList);
        }
      }
    }
    if (transactionAdviceBasicBean.getLocationDetailTAABean() == null) {
      transactionAdviceBasicBean.setLocationDetailTAABean(new LocationDetailTAABean());
    }
    LocationTAAFormatter.formatLocationTAARecord(transactionAdviceBasicBean.getLocationDetailTAABean(), formatedMessageList);
  }
  
  public static String formatValue(String fieldValue, StringBuffer stbuf, int maxLength, boolean isRightJustify, boolean isLeftJustify, boolean isUppercase, boolean isZeroFill, boolean isCharSpacesfill)
  {
    String formattedValue = "";
    if (CommonValidator.isNullOrEmpty(fieldValue))
    {
      if (isZeroFill) {
        zeroFill(stbuf, maxLength);
      }
      if (isCharSpacesfill) {
        characterSpacesFill(stbuf, maxLength);
      }
    }
    if (!CommonValidator.isNullOrEmpty(fieldValue))
    {
      formattedValue = fieldValue;
      if (isUppercase) {
        formattedValue = formattedValue.toUpperCase();
      }
      if (isLeftJustify) {
        formattedValue = leftJustify(formattedValue, maxLength);
      }
      if (isRightJustify) {
        formattedValue = rightJustify(formattedValue, maxLength);
      }
      stbuf.append(formattedValue);
    }
    if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
      LOGGER.info("Formated ISO Field Value:" + fieldValue);
    }
    return stbuf.toString();
  }
  
  public static void characterSpacesFill(StringBuffer stringBuffer, int length)
  {
    for (int i = 0; i < length; i++) {
      stringBuffer.append(' ');
    }
  }
  
  public static void zeroFill(StringBuffer stringBuffer, int length)
  {
    for (int i = 0; i < length; i++) {
      stringBuffer.append('0');
    }
  }
  
  private static String rightJustify(String dataField, int length)
  {
    int dfLength = dataField.length();
    String formattedDataField = dataField;
    for (int iCount = dfLength; iCount < length; iCount++) {
      formattedDataField = "0" + formattedDataField;
    }
    return formattedDataField;
  }
  
  private static String leftJustify(String dataField, int length)
  {
    int dfLength = dataField.length();
    String formattedDataField = dataField;
    for (int iCount = dfLength; iCount < length; iCount++) {
      formattedDataField = formattedDataField + " ";
    }
    return formattedDataField;
  }
  
  public static String formatPANNumber(String fieldValue, StringBuffer stbfer, int maxLength, boolean isRightJustify, boolean isLeftJustify, boolean isUppercase, boolean isZeroFill, boolean isCharSpacesfill)
  {
    String formattedValue = "";
    if (CommonValidator.isNullOrEmpty(fieldValue))
    {
      if (isZeroFill) {
        zeroFill(stbfer, maxLength);
      }
      if (isCharSpacesfill) {
        characterSpacesFill(stbfer, maxLength);
      }
    }
    if (!CommonValidator.isNullOrEmpty(fieldValue))
    {
      formattedValue = fieldValue;
      if (isUppercase) {
        formattedValue = formattedValue.toUpperCase();
      }
      if (isLeftJustify) {
        formattedValue = leftJustify(formattedValue, maxLength);
      }
      if (isRightJustify) {
        formattedValue = rightJustify(formattedValue, maxLength);
      }
      stbfer.append(formattedValue);
    }
    if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
      LOGGER.info("Formated ISO Field Value:" + performMasking(fieldValue, false, true, 5, 4));
    }
    return stbfer.toString();
  }
  
  public static String performMasking(String value, boolean isAll, boolean isNotAll, int startIndex, int endIndex)
  {
    String temp = "";
    String temp2 = "";
    String temp3 = "";
    if (isAll)
    {
      value = getMasked(value.length(), "X");
    }
    else if (isNotAll)
    {
      if (startIndex < value.length())
      {
        temp = value.substring(0, startIndex);
        temp2 = value.substring(startIndex, value.length() - endIndex);
        
        temp2 = getMasked(temp2.length(), "X");
        
        temp3 = value.substring(temp.length() + temp2.length(), temp.length() + temp2.length() + endIndex);
      }
      value = temp + temp2 + temp3;
    }
    return value;
  }
  
  public static String getMasked(int length, String symbol)
  {
    String temp = "";
    for (int i = 0; i < length; i++) {
      temp = symbol + temp;
    }
    return temp;
  }
}
