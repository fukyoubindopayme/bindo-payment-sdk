package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.nonindustry;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.ErrorObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceAddendumBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceBasicBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.nonindustrytype.EMVBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.nonindustrytype.IccSystemRelatedDataBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.SubmissionErrorCodes;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.exceptions.SettlementException;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.CommonValidator;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.industry.TAADBRecordValidator;
import java.util.List;

public class EMVRecordValidator
{
  public static void validateEMVRecord(TransactionAdviceBasicBean transactionAdviceBasicType, TransactionAdviceAddendumBean transactionAdviceAddendumType, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    EMVBean emvBean = (EMVBean)transactionAdviceAddendumType;
    
    TAADBRecordValidator.validateRecordType(emvBean.getRecordType(), 
      errorCodes);
    TAADBRecordValidator.validateRecordNumber(emvBean.getRecordNumber(), 
      errorCodes);
    TAADBRecordValidator.validateTransactionIdentifier(emvBean
      .getTransactionIdentifier(), transactionAdviceBasicType, 
      errorCodes);
    TAADBRecordValidator.validateAddendaTypeCode(emvBean
      .getAddendaTypeCode(), errorCodes);
    validateEMVFormatType(emvBean.getEMVFormatType(), errorCodes);
    if (emvBean.getIccSystemRelatedDataBean() != null) {
      if (Integer.parseInt(emvBean.getEMVFormatType()) == 0) {
        validatePcakedFormatICCSystemRelatedData(
          emvBean.getIccSystemRelatedDataBean(), errorCodes);
      } else if (Integer.parseInt(emvBean.getEMVFormatType()) == 1) {
        validateUnPcakedFormatICCSystemRelatedData(
          emvBean.getIccSystemRelatedDataBean(), errorCodes);
      }
    }
  }
  
  private static void validateEMVFormatType(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD404_EMV_FORMATTYPE);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ů']))
    {
      String reqLength = CommonValidator.validateLength(value, 2, 2);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD406_EMV_FORMATTYPE);
        errorCodes.add(errorObj);
      }
      else if (!value.equalsIgnoreCase("00"))
      {
        if (!value.equalsIgnoreCase("01"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD404_EMV_FORMATTYPE);
          errorCodes.add(errorObj);
        }
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD405_EMV_FORMATTYPE);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validatePcakedFormatICCSystemRelatedData(IccSystemRelatedDataBean iccDataBean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    validateHeaderVersionName(iccDataBean.getHeaderVersionName(), 
      errorCodes);
    validateHeaderVersionNumber(iccDataBean.getHeaderVersionNumber(), 
      errorCodes);
    validateApplicationCryptogram(iccDataBean.getApplicationCryptogram(), 
      errorCodes);
    validateIssuerApplicationData(iccDataBean.getIssuerApplicationData(), 
      errorCodes);
    validateUnPredictableNumber(iccDataBean.getUnPredictableNumber(), 
      errorCodes);
    validateApplicationTransactionCounter(iccDataBean
      .getApplicationTransactionCounter(), errorCodes);
    validateTerminalVerificationResults(iccDataBean
      .getTerminalVerificationResults(), errorCodes);
    validateApplicationTransactionDate(iccDataBean.getTransactionDate(), 
      errorCodes);
    validateApplicationTransactionType(iccDataBean.getTransactionType(), 
      errorCodes);
    validateAmountAuthorized(iccDataBean.getAmountAuthorized(), errorCodes);
    validateTerminalTransactionCurrencyCode(iccDataBean
      .getTerminalTransactionCurrencyCode(), errorCodes);
    validateTerminalCountryCode(iccDataBean.getTerminalCountryCode(), 
      errorCodes);
    validateApplicationInterCahngeProfile(iccDataBean
      .getApplicationInterCahngeProfile(), errorCodes);
    validateAmountOther(iccDataBean.getAmountOther(), errorCodes);
    validateApplicationPanSequenceNumber(iccDataBean
      .getApplicationPanSequenceNumber(), errorCodes);
    validateCryptogramInformationData(iccDataBean
      .getCryptogramInformationData(), errorCodes);
  }
  
  private static void validateHeaderVersionName(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD502_HEADER_VERSIONNAME);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ƛ']))
    {
      String reqLength = CommonValidator.validateLength(value, 4, 4);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD503_HEADER_VERSIONNAME);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD504_HEADER_VERSIONNAME);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateHeaderVersionNumber(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD505_HEADER_VERSIONNUMBER);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ɯ']))
    {
      String reqLength = CommonValidator.validateLength(value, 2, 2);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD506_HEADER_VERSIONNUMBER);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD507_HEADER_VERSIONNUMBER);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateApplicationCryptogram(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD508_APPLICATION_CRYPTOGRAM);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ɲ']))
    {
      String reqLength = CommonValidator.validateLength(value, 8, 8);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD509_APPLICATION_CRYPTOGRAM);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD510_APPLICATION_CRYPTOGRAM);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateIssuerApplicationData(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD511_ISSUER_APPLICATION_DATA);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ƞ']))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 33, 33);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD512_ISSUER_APPLICATION_DATA);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD513_ISSUER_APPLICATION_DATA);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateUnPredictableNumber(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD514_UNPREDICTABLE_NUMBER);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ɵ']))
    {
      String reqLength = CommonValidator.validateLength(value, 4, 4);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD515_UNPREDICTABLE_NUMBER);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD516_UNPREDICTABLE_NUMBER);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateApplicationTransactionCounter(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD517_APPLICATION_TRANSACTIONCOUNTER);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ơ']))
    {
      String reqLength = CommonValidator.validateLength(value, 2, 2);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD518_APPLICATION_TRANSACTIONCOUNTER);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD519_APPLICATION_TRANSACTIONCOUNTER);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateTerminalVerificationResults(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD520_TERMINAL_VERIFICATIONRESULTS);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ơ']))
    {
      String reqLength = CommonValidator.validateLength(value, 5, 5);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD521_TERMINAL_VERIFICATIONRESULTS);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD522_TERMINAL_VERIFICATIONRESULTS);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateApplicationTransactionDate(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD523_TRANSACTION_DATE);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ƣ']))
    {
      String reqLength = CommonValidator.validateLength(value, 3, 3);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD524_TRANSACTION_DATE);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD525_TRANSACTION_DATE);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateApplicationTransactionType(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD526_TRANSACTION_TYPE);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ƣ']))
    {
      String reqLength = CommonValidator.validateLength(value, 1, 1);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD527_TRANSACTION_TYPE);
        errorCodes.add(errorObj);
      }
      else if (!value.equalsIgnoreCase("00"))
      {
        if (!value.equalsIgnoreCase("01")) {
          if (!value.equalsIgnoreCase("20"))
          {
            errorObj = new ErrorObject(
              SubmissionErrorCodes.ERROR_DATAFIELD527_TRANSACTION_TYPE);
            errorCodes.add(errorObj);
          }
        }
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD528_TRANSACTION_TYPE);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateAmountAuthorized(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD529_AMOUNT_AUTHORIZED);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ƥ']))
    {
      String reqLength = CommonValidator.validateLength(value, 6, 6);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD530_AMOUNT_AUTHORIZED);
        errorCodes.add(errorObj);
      }
      else if (Integer.parseInt(value) == 0)
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD0530_AMOUNT_AUTHORIZED);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD531_AMOUNT_AUTHORIZED);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateTerminalTransactionCurrencyCode(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD532_TERMINALTRANSACTION_CURRENCYCODE);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ƥ']))
    {
      String reqLength = CommonValidator.validateLength(value, 2, 2);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD533_TERMINALTRANSACTION_CURRENCYCODE);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD534_TERMINALTRANSACTION_CURRENCYCODE);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateTerminalCountryCode(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD535_TERMINAL_COUNTRY_CODE);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ʀ']))
    {
      String reqLength = CommonValidator.validateLength(value, 2, 2);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD536_TERMINAL_COUNTRY_CODE);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD537_TERMINAL_COUNTRY_CODE);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateApplicationInterCahngeProfile(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD538_APPLICATION_INTERCHANGEPROFILE);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ƨ']))
    {
      String reqLength = CommonValidator.validateLength(value, 2, 2);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD539_APPLICATION_INTERCHANGEPROFILE);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD540_APPLICATION_INTERCHANGEPROFILE);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateAmountOther(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD541_AMOUNT_OTHER);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ƨ']))
    {
      String reqLength = CommonValidator.validateLength(value, 6, 6);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD542_AMOUNT_OTHER);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD543_AMOUNT_OTHER);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateApplicationPanSequenceNumber(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD544_APPLICATION_PANSEQUENCENUMBER);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ʃ']))
    {
      String reqLength = CommonValidator.validateLength(value, 1, 1);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD545_APPLICATION_PANSEQUENCENUMBER);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD546_APPLICATION_PANSEQUENCENUMBER);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateCryptogramInformationData(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD547_CRYPTOGRAM_INFORMATIONDATA);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ƪ']))
    {
      String reqLength = CommonValidator.validateLength(value, 1, 1);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD548_CRYPTOGRAM_INFORMATIONDATA);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD549_CRYPTOGRAM_INFORMATIONDATA);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateUnPcakedFormatICCSystemRelatedData(IccSystemRelatedDataBean iccDataBean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    validateUnPackHeaderVersionName(iccDataBean.getHeaderVersionName(), 
      errorCodes);
    validateUnPackHeaderVersionNumber(iccDataBean.getHeaderVersionNumber(), 
      errorCodes);
    validateUnPackApplicationCryptogram(iccDataBean
      .getApplicationCryptogram(), errorCodes);
    validateUnPackIssuerApplicationData(iccDataBean
      .getIssuerApplicationData(), errorCodes);
    validateUnPackUnPredictableNumber(iccDataBean.getUnPredictableNumber(), 
      errorCodes);
    validateUnPackApplicationTransactionCounter(iccDataBean
      .getApplicationTransactionCounter(), errorCodes);
    validateUnPackTerminalVerificationResults(iccDataBean
      .getTerminalVerificationResults(), errorCodes);
    validateUnPackApplicationTransactionDate(iccDataBean
      .getTransactionDate(), errorCodes);
    validateUnPackApplicationTransactionType(iccDataBean
      .getTransactionType(), errorCodes);
    validateUnPackAmountAuthorized(iccDataBean.getAmountAuthorized(), 
      errorCodes);
    validateUnPackTerminalTransactionCurrencyCode(iccDataBean
      .getTerminalTransactionCurrencyCode(), errorCodes);
    validateUnPackTerminalCountryCode(iccDataBean.getTerminalCountryCode(), 
      errorCodes);
    validateUnPackApplicationInterCahngeProfile(iccDataBean
      .getApplicationInterCahngeProfile(), errorCodes);
    validateUnPackAmountOther(iccDataBean.getAmountOther(), errorCodes);
    validateUnPackApplicationPanSequenceNumber(iccDataBean
      .getApplicationPanSequenceNumber(), errorCodes);
    validateUnPackCryptogramInformationData(iccDataBean
      .getCryptogramInformationData(), errorCodes);
  }
  
  private static void validateUnPackHeaderVersionName(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD502_HEADER_VERSIONNAME);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ƭ']))
    {
      String reqLength = CommonValidator.validateLength(value, 4, 4);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD503_HEADER_VERSIONNAME);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD504_HEADER_VERSIONNAME);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateUnPackHeaderVersionNumber(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD505_HEADER_VERSIONNUMBER);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ƭ']))
    {
      String reqLength = CommonValidator.validateLength(value, 4, 4);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD550_HEADER_VERSIONNUMBER);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD551_HEADER_VERSIONNUMBER);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateUnPackApplicationCryptogram(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD508_APPLICATION_CRYPTOGRAM);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ʈ']))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 16, 16);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD552_APPLICATION_CRYPTOGRAM);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD553_APPLICATION_CRYPTOGRAM);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateUnPackIssuerApplicationData(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD511_ISSUER_APPLICATION_DATA);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ư']))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 64, 64);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD554_ISSUER_APPLICATION_DATA);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD555_ISSUER_APPLICATION_DATA);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateUnPackUnPredictableNumber(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD556_UNPREDICTABLE_NUMBER);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ư']))
    {
      String reqLength = CommonValidator.validateLength(value, 8, 8);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD557_UNPREDICTABLE_NUMBER);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD558_UNPREDICTABLE_NUMBER);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateUnPackApplicationTransactionCounter(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD559_APPLICATION_TRANSACTIONCOUNTER);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ʊ']))
    {
      String reqLength = CommonValidator.validateLength(value, 4, 4);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD560_APPLICATION_TRANSACTIONCOUNTER);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD561_APPLICATION_TRANSACTIONCOUNTER);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateUnPackTerminalVerificationResults(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD562_TERMINAL_VERIFICATIONRESULTS);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ʋ']))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 10, 10);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD563_TERMINAL_VERIFICATIONRESULTS);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD564_TERMINAL_VERIFICATIONRESULTS);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateUnPackApplicationTransactionDate(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD523_TRANSACTION_DATE);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ƴ']))
    {
      String reqLength = CommonValidator.validateLength(value, 6, 6);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD565_TRANSACTION_DATE);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD566_TRANSACTION_DATE);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateUnPackApplicationTransactionType(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD526_TRANSACTION_TYPE);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ƴ']))
    {
      String reqLength = CommonValidator.validateLength(value, 2, 2);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD567_TRANSACTION_TYPE);
        errorCodes.add(errorObj);
      }
      else if (!value.equalsIgnoreCase("00"))
      {
        if (!value.equalsIgnoreCase("01")) {
          if (!value.equalsIgnoreCase("20"))
          {
            errorObj = new ErrorObject(
              SubmissionErrorCodes.ERROR_DATAFIELD527_TRANSACTION_TYPE);
            errorCodes.add(errorObj);
          }
        }
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD568_TRANSACTION_TYPE);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateUnPackAmountAuthorized(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD569_AMOUNT_AUTHORIZED);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ƶ']))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 12, 12);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD570_AMOUNT_AUTHORIZED);
        errorCodes.add(errorObj);
      }
      else if (Integer.parseInt(value) == 0)
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD0530_AMOUNT_AUTHORIZED);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD531_AMOUNT_AUTHORIZED);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateUnPackTerminalTransactionCurrencyCode(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD532_TERMINALTRANSACTION_CURRENCYCODE);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ƶ']))
    {
      String reqLength = CommonValidator.validateLength(value, 4, 4);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD570_TERMINALTRANSACTION_CURRENCYCODE);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD571_TERMINALTRANSACTION_CURRENCYCODE);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateUnPackTerminalCountryCode(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD535_TERMINAL_COUNTRY_CODE);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ʒ']))
    {
      String reqLength = CommonValidator.validateLength(value, 4, 4);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD572_TERMINAL_COUNTRY_CODE);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD573_TERMINAL_COUNTRY_CODE);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateUnPackApplicationInterCahngeProfile(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD573_APPLICATION_INTERCHANGEPROFILE);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ƹ']))
    {
      String reqLength = CommonValidator.validateLength(value, 4, 4);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD574_APPLICATION_INTERCHANGEPROFILE);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD575_APPLICATION_INTERCHANGEPROFILE);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateUnPackAmountOther(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD541_AMOUNT_OTHER);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ƹ']))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 12, 12);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD576_AMOUNT_OTHER);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD577_AMOUNT_OTHER);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateUnPackApplicationPanSequenceNumber(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD578_APPLICATION_PANSEQUENCENUMBER);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ƺ']))
    {
      String reqLength = CommonValidator.validateLength(value, 2, 2);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD579_APPLICATION_PANSEQUENCENUMBER);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD580_APPLICATION_PANSEQUENCENUMBER);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateUnPackCryptogramInformationData(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD581_CRYPTOGRAM_INFORMATIONDATA);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ƻ']))
    {
      String reqLength = CommonValidator.validateLength(value, 2, 2);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD582_CRYPTOGRAM_INFORMATIONDATA);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD583_CRYPTOGRAM_INFORMATIONDATA);
      errorCodes.add(errorObj);
    }
  }
}
