package com.bindo.paymentsdk.v3.pay.gateway.amex.util.juice;

public class OptionException extends Exception {
    public static final int FATAL_ERROR = 0;
    public static final int ILLEGAL_ARGUMENTS = 1;
    private int errorcode;
    public static final int MISSING_ARGUMENTS = 2;

    public OptionException(String reason, int code) {
        super(reason);
        this.errorcode = code;
    }

    public int getErrorCode() {
        return this.errorcode;
    }
}
