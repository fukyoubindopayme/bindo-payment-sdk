package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.IndustryTypeTAABean;

public class AirlineIndustryTAABean
  extends IndustryTypeTAABean
{
  private String transactionType;
  private String ticketNumber;
  private String documentType;
  private String airlineProcessIdentifier;
  private String iataNumericCode;
  private String ticketingCarrierName;
  private String ticketIssueCity;
  private String ticketIssueDate;
  private String numberInParty;
  private String passengerName;
  private String conjunctionTicketIndicator;
  private String originalTransactionAmount;
  private String originalCurrencyCode;
  private String electronicTicketIndicator;
  private String totalNumberOfAirSegments;
  private String stopoverIndicator1;
  private String departureLocationCodeSegment1;
  private String departureDateSegment1;
  private String arrivalLocationCodeSegment1;
  private String segmentCarrierCode1;
  private String segment1FareBasis;
  private String classOfServiceCodeSegment1;
  private String flightNumberSegment1;
  private String reserved;
  private String segment1Fare;
  private String stopoverIndicator2;
  private String departureLocationCodeSegment2;
  private String departureDateSegment2;
  private String arrivalLocationCodeSegment2;
  private String segmentCarrierCode2;
  private String segment2FareBasis;
  private String classOfServiceCodeSegment2;
  private String flightNumberSegment2;
  private String segment2Fare;
  private String stopoverIndicator3;
  private String departureLocationCodeSegment3;
  private String departureDateSegment3;
  private String arrivalLocationCodeSegment3;
  private String segmentCarrierCode3;
  private String segment3FareBasis;
  private String classOfServiceCodeSegment3;
  private String flightNumberSegment3;
  private String segment3Fare;
  private String stopoverIndicator4;
  private String departureLocationCodeSegment4;
  private String departureDateSegment4;
  private String arrivalLocationCodeSegment4;
  private String segmentCarrierCode4;
  private String segment4FareBasis;
  private String classOfServiceCodeSegment4;
  private String flightNumberSegment4;
  private String segment4Fare;
  private String stopoverIndicator5;
  private String exchangedOrOriginalTicketNumber;
  
  public String getTransactionType()
  {
    return this.transactionType;
  }
  
  public void setTransactionType(String transactionType)
  {
    this.transactionType = transactionType;
  }
  
  public String getTicketNumber()
  {
    return this.ticketNumber;
  }
  
  public void setTicketNumber(String ticketNumber)
  {
    this.ticketNumber = ticketNumber;
  }
  
  public String getDocumentType()
  {
    return this.documentType;
  }
  
  public void setDocumentType(String documentType)
  {
    this.documentType = documentType;
  }
  
  public String getAirlineProcessIdentifier()
  {
    return this.airlineProcessIdentifier;
  }
  
  public void setAirlineProcessIdentifier(String airlineProcessIdentifier)
  {
    this.airlineProcessIdentifier = airlineProcessIdentifier;
  }
  
  public String getIataNumericCode()
  {
    return this.iataNumericCode;
  }
  
  public void setIataNumericCode(String iataNumericCode)
  {
    this.iataNumericCode = iataNumericCode;
  }
  
  public String getTicketingCarrierName()
  {
    return this.ticketingCarrierName;
  }
  
  public void setTicketingCarrierName(String ticketingCarrierName)
  {
    this.ticketingCarrierName = ticketingCarrierName;
  }
  
  public String getTicketIssueCity()
  {
    return this.ticketIssueCity;
  }
  
  public void setTicketIssueCity(String ticketIssueCity)
  {
    this.ticketIssueCity = ticketIssueCity;
  }
  
  public String getTicketIssueDate()
  {
    return this.ticketIssueDate;
  }
  
  public void setTicketIssueDate(String ticketIssueDate)
  {
    this.ticketIssueDate = ticketIssueDate;
  }
  
  public String getNumberInParty()
  {
    return this.numberInParty;
  }
  
  public void setNumberInParty(String numberInParty)
  {
    this.numberInParty = numberInParty;
  }
  
  public String getPassengerName()
  {
    return this.passengerName;
  }
  
  public void setPassengerName(String passengerName)
  {
    this.passengerName = passengerName;
  }
  
  public String getConjunctionTicketIndicator()
  {
    return this.conjunctionTicketIndicator;
  }
  
  public void setConjunctionTicketIndicator(String conjunctionTicketIndicator)
  {
    this.conjunctionTicketIndicator = conjunctionTicketIndicator;
  }
  
  public String getOriginalTransactionAmount()
  {
    return this.originalTransactionAmount;
  }
  
  public void setOriginalTransactionAmount(String originalTransactionAmount)
  {
    this.originalTransactionAmount = originalTransactionAmount;
  }
  
  public String getOriginalCurrencyCode()
  {
    return this.originalCurrencyCode;
  }
  
  public void setOriginalCurrencyCode(String originalCurrencyCode)
  {
    this.originalCurrencyCode = originalCurrencyCode;
  }
  
  public String getElectronicTicketIndicator()
  {
    return this.electronicTicketIndicator;
  }
  
  public void setElectronicTicketIndicator(String electronicTicketIndicator)
  {
    this.electronicTicketIndicator = electronicTicketIndicator;
  }
  
  public String getTotalNumberOfAirSegments()
  {
    return this.totalNumberOfAirSegments;
  }
  
  public void setTotalNumberOfAirSegments(String totalNumberOfAirSegments)
  {
    this.totalNumberOfAirSegments = totalNumberOfAirSegments;
  }
  
  public String getStopoverIndicator1()
  {
    return this.stopoverIndicator1;
  }
  
  public void setStopoverIndicator1(String stopoverIndicator1)
  {
    this.stopoverIndicator1 = stopoverIndicator1;
  }
  
  public String getDepartureLocationCodeSegment1()
  {
    return this.departureLocationCodeSegment1;
  }
  
  public void setDepartureLocationCodeSegment1(String departureLocationCodeSegment1)
  {
    this.departureLocationCodeSegment1 = departureLocationCodeSegment1;
  }
  
  public String getDepartureDateSegment1()
  {
    return this.departureDateSegment1;
  }
  
  public void setDepartureDateSegment1(String departureDateSegment1)
  {
    this.departureDateSegment1 = departureDateSegment1;
  }
  
  public String getArrivalLocationCodeSegment1()
  {
    return this.arrivalLocationCodeSegment1;
  }
  
  public void setArrivalLocationCodeSegment1(String arrivalLocationCodeSegment1)
  {
    this.arrivalLocationCodeSegment1 = arrivalLocationCodeSegment1;
  }
  
  public String getSegmentCarrierCode1()
  {
    return this.segmentCarrierCode1;
  }
  
  public void setSegmentCarrierCode1(String segmentCarrierCode1)
  {
    this.segmentCarrierCode1 = segmentCarrierCode1;
  }
  
  public String getSegment1FareBasis()
  {
    return this.segment1FareBasis;
  }
  
  public void setSegment1FareBasis(String segment1FareBasis)
  {
    this.segment1FareBasis = segment1FareBasis;
  }
  
  public String getClassOfServiceCodeSegment1()
  {
    return this.classOfServiceCodeSegment1;
  }
  
  public void setClassOfServiceCodeSegment1(String classOfServiceCodeSegment1)
  {
    this.classOfServiceCodeSegment1 = classOfServiceCodeSegment1;
  }
  
  public String getFlightNumberSegment1()
  {
    return this.flightNumberSegment1;
  }
  
  public void setFlightNumberSegment1(String flightNumberSegment1)
  {
    this.flightNumberSegment1 = flightNumberSegment1;
  }
  
  public String getReserved()
  {
    return this.reserved;
  }
  
  public void setReserved(String reserved)
  {
    this.reserved = reserved;
  }
  
  public String getSegment1Fare()
  {
    return this.segment1Fare;
  }
  
  public void setSegment1Fare(String segment1Fare)
  {
    this.segment1Fare = segment1Fare;
  }
  
  public String getStopoverIndicator2()
  {
    return this.stopoverIndicator2;
  }
  
  public void setStopoverIndicator2(String stopoverIndicator2)
  {
    this.stopoverIndicator2 = stopoverIndicator2;
  }
  
  public String getDepartureLocationCodeSegment2()
  {
    return this.departureLocationCodeSegment2;
  }
  
  public void setDepartureLocationCodeSegment2(String departureLocationCodeSegment2)
  {
    this.departureLocationCodeSegment2 = departureLocationCodeSegment2;
  }
  
  public String getDepartureDateSegment2()
  {
    return this.departureDateSegment2;
  }
  
  public void setDepartureDateSegment2(String departureDateSegment2)
  {
    this.departureDateSegment2 = departureDateSegment2;
  }
  
  public String getArrivalLocationCodeSegment2()
  {
    return this.arrivalLocationCodeSegment2;
  }
  
  public void setArrivalLocationCodeSegment2(String arrivalLocationCodeSegment2)
  {
    this.arrivalLocationCodeSegment2 = arrivalLocationCodeSegment2;
  }
  
  public String getSegmentCarrierCode2()
  {
    return this.segmentCarrierCode2;
  }
  
  public void setSegmentCarrierCode2(String segmentCarrierCode2)
  {
    this.segmentCarrierCode2 = segmentCarrierCode2;
  }
  
  public String getSegment2FareBasis()
  {
    return this.segment2FareBasis;
  }
  
  public void setSegment2FareBasis(String segment2FareBasis)
  {
    this.segment2FareBasis = segment2FareBasis;
  }
  
  public String getClassOfServiceCodeSegment2()
  {
    return this.classOfServiceCodeSegment2;
  }
  
  public void setClassOfServiceCodeSegment2(String classOfServiceCodeSegment2)
  {
    this.classOfServiceCodeSegment2 = classOfServiceCodeSegment2;
  }
  
  public String getFlightNumberSegment2()
  {
    return this.flightNumberSegment2;
  }
  
  public void setFlightNumberSegment2(String flightNumberSegment2)
  {
    this.flightNumberSegment2 = flightNumberSegment2;
  }
  
  public String getSegment2Fare()
  {
    return this.segment2Fare;
  }
  
  public void setSegment2Fare(String segment2Fare)
  {
    this.segment2Fare = segment2Fare;
  }
  
  public String getStopoverIndicator3()
  {
    return this.stopoverIndicator3;
  }
  
  public void setStopoverIndicator3(String stopoverIndicator3)
  {
    this.stopoverIndicator3 = stopoverIndicator3;
  }
  
  public String getDepartureLocationCodeSegment3()
  {
    return this.departureLocationCodeSegment3;
  }
  
  public void setDepartureLocationCodeSegment3(String departureLocationCodeSegment3)
  {
    this.departureLocationCodeSegment3 = departureLocationCodeSegment3;
  }
  
  public String getDepartureDateSegment3()
  {
    return this.departureDateSegment3;
  }
  
  public void setDepartureDateSegment3(String departureDateSegment3)
  {
    this.departureDateSegment3 = departureDateSegment3;
  }
  
  public String getArrivalLocationCodeSegment3()
  {
    return this.arrivalLocationCodeSegment3;
  }
  
  public void setArrivalLocationCodeSegment3(String arrivalLocationCodeSegment3)
  {
    this.arrivalLocationCodeSegment3 = arrivalLocationCodeSegment3;
  }
  
  public String getSegmentCarrierCode3()
  {
    return this.segmentCarrierCode3;
  }
  
  public void setSegmentCarrierCode3(String segmentCarrierCode3)
  {
    this.segmentCarrierCode3 = segmentCarrierCode3;
  }
  
  public String getSegment3FareBasis()
  {
    return this.segment3FareBasis;
  }
  
  public void setSegment3FareBasis(String segment3FareBasis)
  {
    this.segment3FareBasis = segment3FareBasis;
  }
  
  public String getClassOfServiceCodeSegment3()
  {
    return this.classOfServiceCodeSegment3;
  }
  
  public void setClassOfServiceCodeSegment3(String classOfServiceCodeSegment3)
  {
    this.classOfServiceCodeSegment3 = classOfServiceCodeSegment3;
  }
  
  public String getFlightNumberSegment3()
  {
    return this.flightNumberSegment3;
  }
  
  public void setFlightNumberSegment3(String flightNumberSegment3)
  {
    this.flightNumberSegment3 = flightNumberSegment3;
  }
  
  public String getSegment3Fare()
  {
    return this.segment3Fare;
  }
  
  public void setSegment3Fare(String segment3Fare)
  {
    this.segment3Fare = segment3Fare;
  }
  
  public String getStopoverIndicator4()
  {
    return this.stopoverIndicator4;
  }
  
  public void setStopoverIndicator4(String stopoverIndicator4)
  {
    this.stopoverIndicator4 = stopoverIndicator4;
  }
  
  public String getDepartureLocationCodeSegment4()
  {
    return this.departureLocationCodeSegment4;
  }
  
  public void setDepartureLocationCodeSegment4(String departureLocationCodeSegment4)
  {
    this.departureLocationCodeSegment4 = departureLocationCodeSegment4;
  }
  
  public String getDepartureDateSegment4()
  {
    return this.departureDateSegment4;
  }
  
  public void setDepartureDateSegment4(String departureDateSegment4)
  {
    this.departureDateSegment4 = departureDateSegment4;
  }
  
  public String getArrivalLocationCodeSegment4()
  {
    return this.arrivalLocationCodeSegment4;
  }
  
  public void setArrivalLocationCodeSegment4(String arrivalLocationCodeSegment4)
  {
    this.arrivalLocationCodeSegment4 = arrivalLocationCodeSegment4;
  }
  
  public String getSegmentCarrierCode4()
  {
    return this.segmentCarrierCode4;
  }
  
  public void setSegmentCarrierCode4(String segmentCarrierCode4)
  {
    this.segmentCarrierCode4 = segmentCarrierCode4;
  }
  
  public String getSegment4FareBasis()
  {
    return this.segment4FareBasis;
  }
  
  public void setSegment4FareBasis(String segment4FareBasis)
  {
    this.segment4FareBasis = segment4FareBasis;
  }
  
  public String getClassOfServiceCodeSegment4()
  {
    return this.classOfServiceCodeSegment4;
  }
  
  public void setClassOfServiceCodeSegment4(String classOfServiceCodeSegment4)
  {
    this.classOfServiceCodeSegment4 = classOfServiceCodeSegment4;
  }
  
  public String getFlightNumberSegment4()
  {
    return this.flightNumberSegment4;
  }
  
  public void setFlightNumberSegment4(String flightNumberSegment4)
  {
    this.flightNumberSegment4 = flightNumberSegment4;
  }
  
  public String getSegment4Fare()
  {
    return this.segment4Fare;
  }
  
  public void setSegment4Fare(String segment4Fare)
  {
    this.segment4Fare = segment4Fare;
  }
  
  public String getStopoverIndicator5()
  {
    return this.stopoverIndicator5;
  }
  
  public void setStopoverIndicator5(String stopoverIndicator5)
  {
    this.stopoverIndicator5 = stopoverIndicator5;
  }
  
  public String getExchangedOrOriginalTicketNumber()
  {
    return this.exchangedOrOriginalTicketNumber;
  }
  
  public void setExchangedOrOriginalTicketNumber(String exchangedOrOriginalTicketNumber)
  {
    this.exchangedOrOriginalTicketNumber = exchangedOrOriginalTicketNumber;
  }
}
