package com.bindo.paymentsdk.v3.pay.gateway.amex.gateway;

import java.util.EventObject;

public interface AmexSubmissionGatewayListener {

    void onProgress(EventObject eventObject);

    void onError(Exception exception);
}
