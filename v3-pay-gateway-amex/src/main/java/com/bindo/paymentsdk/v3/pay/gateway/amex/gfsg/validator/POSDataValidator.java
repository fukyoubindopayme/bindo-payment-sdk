package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.pos.CardCaptureCapability;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.pos.CardDataInputMode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.pos.CardDataOutputCapable;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.pos.CardInputCapability;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.pos.CardMemberAuthEntity;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.pos.CardMemberAuthMethod;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.pos.CardPresent;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.pos.CardholderAuthCapable;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.pos.CardholderPresent;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.pos.OperatingEnvironment;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.pos.PINCaptureCapable;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.pos.TerminalOutputCapable;

public class POSDataValidator
{
  public static boolean validateCardDataInputCapability(String cardDataInputCapability)
  {
    boolean isValid = false;
    String cardDataInputCapable = cardDataInputCapability.toUpperCase();
    CardInputCapability[] arrayOfCardInputCapability;
    int j = (arrayOfCardInputCapability = CardInputCapability.values()).length;
    for (int i = 0; i < j; i++)
    {
      CardInputCapability cardInputCapability = arrayOfCardInputCapability[i];
      if (cardDataInputCapable.equals(cardInputCapability.getCardInputCapability()))
      {
        isValid = true;
        break;
      }
    }
    return isValid;
  }
  
  public static boolean validateCardholderAuthCapability(String cardholderAuthCapability)
  {
    boolean isValid = false;
    String cardHolderAuthCapable = cardholderAuthCapability.toUpperCase();
    CardholderAuthCapable[] arrayOfCardholderAuthCapable;
    int j = (arrayOfCardholderAuthCapable = CardholderAuthCapable.values()).length;
    for (int i = 0; i < j; i++)
    {
      CardholderAuthCapable cardholderdataAuthCapability = arrayOfCardholderAuthCapable[i];
      if (cardHolderAuthCapable.equals(cardholderdataAuthCapability.getCardHolderAuthCapability()))
      {
        isValid = true;
        break;
      }
    }
    return isValid;
  }
  
  public static boolean validateCardCaptureCapability(String cardCaptureCapability)
  {
    boolean isValid = false;
    String cardCaptureCapable = cardCaptureCapability.toUpperCase();
    CardCaptureCapability[] arrayOfCardCaptureCapability;
    int j = (arrayOfCardCaptureCapability = CardCaptureCapability.values()).length;
    for (int i = 0; i < j; i++)
    {
      CardCaptureCapability cardCapturedataCapability = arrayOfCardCaptureCapability[i];
      if (cardCaptureCapable.equals(cardCapturedataCapability.getCardCaptureCapability()))
      {
        isValid = true;
        break;
      }
    }
    return isValid;
  }
  
  public static boolean validateOperatingEnv(String operatingEnv)
  {
    boolean isValid = false;
    String operatingEnvironment = operatingEnv.toUpperCase();
    OperatingEnvironment[] arrayOfOperatingEnvironment;
    int j = (arrayOfOperatingEnvironment = OperatingEnvironment.values()).length;
    for (int i = 0; i < j; i++)
    {
      OperatingEnvironment operatingEnvdata = arrayOfOperatingEnvironment[i];
      if (operatingEnvironment.equals(operatingEnvdata.getOperatingEnvironment()))
      {
        isValid = true;
        break;
      }
    }
    return isValid;
  }
  
  public static boolean validateCardholderPresent(String cardholderPresent)
  {
    boolean isValid = false;
    String cardHolderPresentData = cardholderPresent.toUpperCase();
    CardholderPresent[] arrayOfCardholderPresent;
    int j = (arrayOfCardholderPresent = CardholderPresent.values()).length;
    for (int i = 0; i < j; i++)
    {
      CardholderPresent cardholderPresentdata = arrayOfCardholderPresent[i];
      if (cardHolderPresentData.equals(cardholderPresentdata.getCardholderPresent()))
      {
        isValid = true;
        break;
      }
    }
    return isValid;
  }
  
  public static boolean validateCardPresent(String cardPresent)
  {
    boolean isValid = false;
    cardPresent = cardPresent.toUpperCase();
    CardPresent[] arrayOfCardPresent;
    int j = (arrayOfCardPresent = CardPresent.values()).length;
    for (int i = 0; i < j; i++)
    {
      CardPresent cardPresentdata = arrayOfCardPresent[i];
      if (cardPresent.equals(cardPresentdata.getCardPresent()))
      {
        isValid = true;
        break;
      }
    }
    return isValid;
  }
  
  public static boolean validateCardDataInputMode(String cardDataInputMode)
  {
    boolean isValid = false;
    cardDataInputMode = cardDataInputMode.toUpperCase();
    CardDataInputMode[] arrayOfCardDataInputMode;
    int j = (arrayOfCardDataInputMode = CardDataInputMode.values()).length;
    for (int i = 0; i < j; i++)
    {
      CardDataInputMode cardDataInputModedata = arrayOfCardDataInputMode[i];
      if (cardDataInputMode.equals(cardDataInputModedata.getCardDataInputMode()))
      {
        isValid = true;
        break;
      }
    }
    return isValid;
  }
  
  public static boolean validateCardmemberAuthMethod(String cardmemberAuthMethod)
  {
    boolean isValid = false;
    cardmemberAuthMethod = cardmemberAuthMethod.toUpperCase();
    CardMemberAuthMethod[] arrayOfCardMemberAuthMethod;
    int j = (arrayOfCardMemberAuthMethod = CardMemberAuthMethod.values()).length;
    for (int i = 0; i < j; i++)
    {
      CardMemberAuthMethod cardmemberAuthMethoddata = arrayOfCardMemberAuthMethod[i];
      if (cardmemberAuthMethod.equals(cardmemberAuthMethoddata.getCardMemberAuthMethod()))
      {
        isValid = true;
        break;
      }
    }
    return isValid;
  }
  
  public static boolean validateCardmemberAuthEntity(String cardmemberAuthEntity)
  {
    boolean isValid = false;
    cardmemberAuthEntity = cardmemberAuthEntity.toUpperCase();
    CardMemberAuthEntity[] arrayOfCardMemberAuthEntity;
    int j = (arrayOfCardMemberAuthEntity = CardMemberAuthEntity.values()).length;
    for (int i = 0; i < j; i++)
    {
      CardMemberAuthEntity cardmemberAuthEntitydata = arrayOfCardMemberAuthEntity[i];
      if (cardmemberAuthEntity.equals(cardmemberAuthEntitydata.getCardMemberAuthEntity()))
      {
        isValid = true;
        break;
      }
    }
    return isValid;
  }
  
  public static boolean validateCardDataOutputCapability(String cardDataOutputCapability)
  {
    boolean isValid = false;
    cardDataOutputCapability = cardDataOutputCapability.toUpperCase();
    CardDataOutputCapable[] arrayOfCardDataOutputCapable;
    int j = (arrayOfCardDataOutputCapable = CardDataOutputCapable.values()).length;
    for (int i = 0; i < j; i++)
    {
      CardDataOutputCapable cardDataOutputCapabilitydata = arrayOfCardDataOutputCapable[i];
      if (cardDataOutputCapability.equals(cardDataOutputCapabilitydata.getCardDataOutputCapable()))
      {
        isValid = true;
        break;
      }
    }
    return isValid;
  }
  
  public static boolean validateTerminalOutputCapability(String terminalOutputCapability)
  {
    boolean isValid = false;
    terminalOutputCapability = terminalOutputCapability.toUpperCase();
    TerminalOutputCapable[] arrayOfTerminalOutputCapable;
    int j = (arrayOfTerminalOutputCapable = TerminalOutputCapable.values()).length;
    for (int i = 0; i < j; i++)
    {
      TerminalOutputCapable terminalOutputCapabilitydata = arrayOfTerminalOutputCapable[i];
      if (terminalOutputCapability.equals(terminalOutputCapabilitydata.getTerminalOutputCapable()))
      {
        isValid = true;
        break;
      }
    }
    return isValid;
  }
  
  public static boolean validatePinCaptureCapability(String pinCaptureCapability)
  {
    boolean isValid = false;
    pinCaptureCapability = pinCaptureCapability.toUpperCase();
    PINCaptureCapable[] arrayOfPINCaptureCapable;
    int j = (arrayOfPINCaptureCapable = PINCaptureCapable.values()).length;
    for (int i = 0; i < j; i++)
    {
      PINCaptureCapable pinCaptureCapabilitydata = arrayOfPINCaptureCapable[i];
      if (pinCaptureCapability.equals(pinCaptureCapabilitydata.getPINCaptureCapable()))
      {
        isValid = true;
        break;
      }
    }
    return isValid;
  }
}
