package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean;

import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.connection.PropertyReader;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.Constants;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.AuthErrorCodes;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.utils.ISOUtil;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.validator.Validator;
import java.util.List;

public class AdditionalDataNationalBean
{
  private String primaryId = "AX";
  private String secondaryId;
  private String customer_email_id = "CE ";
  private String customer_email_id_vli;
  private String customer_email;
  private String customer_hostname_id = "CH ";
  private String customer_hostname_id_vli;
  private String customer_hostname;
  private String http_browsertype_id = "HBT";
  private String http_browsertype_id_vli;
  private String http_browsertype;
  private String ship_to_country_id = "STC";
  private String ship_to_country_id_vli = "03";
  private String ship_to_country;
  private String shipping_method_id = "SM ";
  private String shipping_method_id_vli = "02";
  private String shipping_method;
  private String merchant_productsku_id = "MPS";
  private String merchant_productsku_id_vli;
  private String merchant_productsku;
  private String customer_ip;
  private String customer_ani;
  private String customer_info_identifier;
  private String iac_apd_departure_date;
  private String iac_apd_passenger_name_id = "APN";
  private String iac_apd_passenger_name_vli;
  private String iac_apd_passenger_name;
  private String iac_apd_origin;
  private String iac_apd_dest;
  private String iac_apd_routing_id = "RTG";
  private String iac_apd_routing_vli;
  private String iac_apd_number_of_cities;
  private String iac_apd_routing_cities;
  private String iac_apd_carriers_id = "ALC";
  private String iac_apd_carriers_vli;
  private String iac_apd_number_of_carriers;
  private String iac_apd_carriers;
  private String iac_apd_fare_basis;
  private String iac_apd_number_of_passengers;
  private String apd_cardmember_name_id = "CN ";
  private String apd_cardmember_name_vli;
  private String apd_cardmember_name;
  private String apd_eticket_indicator;
  private String apd_reservation_code_id = "RES";
  private String apd_reservation_code_vli;
  private String apd_reservation_code;
  private String cpd_versionNumber;
  private String cpd_goods_sold_id;
  private String cpd_goods_sold_product_code;
  private String cpd_goods_sold_product_code_vli;
  StringBuffer displayBuffer = new StringBuffer();
  boolean debugflag = PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON");
  
  public String getPrimaryId()
  {
    return this.primaryId;
  }
  
  public void setPrimaryId(String primaryId)
  {
    this.primaryId = primaryId;
  }
  
  public String getSecondaryId()
  {
    return this.secondaryId;
  }
  
  public void setSecondaryId(String secondaryId)
  {
    this.secondaryId = secondaryId;
  }
  
  public String getCustomerEmailId()
  {
    return this.customer_email_id;
  }
  
  public void setCustomerEmailId(String customer_email_id)
  {
    this.customer_email_id = customer_email_id;
  }
  
  public String getCustomerEmailIdVli()
  {
    return this.customer_email_id_vli;
  }
  
  public void setCustomerEmailIdVli(String customer_email_id_vli)
  {
    this.customer_email_id_vli = customer_email_id_vli;
  }
  
  public String getCustomerEmail()
  {
    return this.customer_email;
  }
  
  public void setCustomerEmail(String customer_email)
  {
    this.customer_email = customer_email;
  }
  
  public String getCustomerHostnameId()
  {
    return this.customer_hostname_id;
  }
  
  public void setCustomerHostnameId(String customer_hostname_id)
  {
    this.customer_hostname_id = customer_hostname_id;
  }
  
  public String getCustomerHostnameIdVli()
  {
    return this.customer_hostname_id_vli;
  }
  
  public void setCustomerHostnameIdVli(String customer_hostname_id_vli)
  {
    this.customer_hostname_id_vli = customer_hostname_id_vli;
  }
  
  public String getCustomerHostname()
  {
    return this.customer_hostname;
  }
  
  public void setCustomerHostname(String customer_hostname)
  {
    this.customer_hostname = customer_hostname;
  }
  
  public String getHttpBrowserTypeId()
  {
    return this.http_browsertype_id;
  }
  
  public void setHttpBrowserTypeId(String http_browsertype_id)
  {
    this.http_browsertype_id = http_browsertype_id;
  }
  
  public String getHttpBrowserTypeIdVli()
  {
    return this.http_browsertype_id_vli;
  }
  
  public void setHttpBrowserTypeIdVli(String http_browsertype_id_vli)
  {
    this.http_browsertype_id_vli = http_browsertype_id_vli;
  }
  
  public String getHttpBrowserType()
  {
    return this.http_browsertype;
  }
  
  public void setHttpBrowserType(String http_browsertype)
  {
    this.http_browsertype = http_browsertype;
  }
  
  public String getShipToCountryId()
  {
    return this.ship_to_country_id;
  }
  
  public void setShipToCountryId(String ship_to_country_id)
  {
    this.ship_to_country_id = ship_to_country_id;
  }
  
  public String getShipToCountryIdVli()
  {
    return this.ship_to_country_id_vli;
  }
  
  public void setShipToCountryIdVli(String ship_to_country_id_vli)
  {
    this.ship_to_country_id_vli = ship_to_country_id_vli;
  }
  
  public String getShipToCountry()
  {
    return this.ship_to_country;
  }
  
  public void setShipToCountry(String ship_to_country)
  {
    this.ship_to_country = ship_to_country;
  }
  
  public String getShippingMethodId()
  {
    return this.shipping_method_id;
  }
  
  public void setShippingMethodId(String shipping_method_id)
  {
    this.shipping_method_id = shipping_method_id;
  }
  
  public String getShippingMethodIdVli()
  {
    return this.shipping_method_id_vli;
  }
  
  public void setShippingMethodIdVli(String shipping_method_id_vli)
  {
    this.shipping_method_id_vli = shipping_method_id_vli;
  }
  
  public String getShippingMethod()
  {
    return this.shipping_method;
  }
  
  public void setShippingMethod(String shipping_method)
  {
    this.shipping_method = shipping_method;
  }
  
  public String getMerchantProductskuId()
  {
    return this.merchant_productsku_id;
  }
  
  public void setMerchantProductSkuId(String merchant_productsku_id)
  {
    this.merchant_productsku_id = merchant_productsku_id;
  }
  
  public String getMerchantProductSkuIdVli()
  {
    return this.merchant_productsku_id_vli;
  }
  
  public void setMerchantProductSkuIdVli(String merchant_productsku_id_vli)
  {
    this.merchant_productsku_id_vli = merchant_productsku_id_vli;
  }
  
  public String getMerchantProductSku()
  {
    return this.merchant_productsku;
  }
  
  public void setMerchantProductSku(String merchant_productsku)
  {
    this.merchant_productsku = merchant_productsku;
  }
  
  public String getCustomerIP()
  {
    return this.customer_ip;
  }
  
  public void setCustomerIP(String customer_ip)
  {
    this.customer_ip = customer_ip;
  }
  
  public String getCustomerANI()
  {
    return this.customer_ani;
  }
  
  public void setCustomerANI(String customer_ani)
  {
    this.customer_ani = customer_ani;
  }
  
  public String getCustomerInfoIdentifier()
  {
    return this.customer_info_identifier;
  }
  
  public void setCustomerInfoIdentifier(String customer_info_identifier)
  {
    this.customer_info_identifier = customer_info_identifier;
  }
  
  public String getIacApdDepartureDate()
  {
    return this.iac_apd_departure_date;
  }
  
  public void setIacApdDepartureDate(String iac_apd_departure_date)
  {
    this.iac_apd_departure_date = iac_apd_departure_date;
  }
  
  public String getIacApdPassengerNameId()
  {
    return this.iac_apd_passenger_name_id;
  }
  
  public void setIacApdPassengerNameId(String iac_apd_passenger_name_id)
  {
    this.iac_apd_passenger_name_id = iac_apd_passenger_name_id;
  }
  
  public String getIacApdPassengerNameVli()
  {
    return this.iac_apd_passenger_name_vli;
  }
  
  public void setIacApdPassengerNameVli(String iac_apd_passenger_name_vli)
  {
    this.iac_apd_passenger_name_vli = iac_apd_passenger_name_vli;
  }
  
  public String getIacApdPassengerName()
  {
    return this.iac_apd_passenger_name;
  }
  
  public void setIacApdPassengerName(String iac_apd_passenger_name)
  {
    this.iac_apd_passenger_name = iac_apd_passenger_name;
  }
  
  public String getIacApdOrigin()
  {
    return this.iac_apd_origin;
  }
  
  public void setIacApdOrigin(String iac_apd_origin)
  {
    this.iac_apd_origin = iac_apd_origin;
  }
  
  public String getIacApdDest()
  {
    return this.iac_apd_dest;
  }
  
  public void setIacApdDest(String iac_apd_dest)
  {
    this.iac_apd_dest = iac_apd_dest;
  }
  
  public String getIacApdRoutingId()
  {
    return this.iac_apd_routing_id;
  }
  
  public void setIacApdRoutingId(String iac_apd_routing_id)
  {
    this.iac_apd_routing_id = iac_apd_routing_id;
  }
  
  public String getIacApdRoutingVli()
  {
    return this.iac_apd_routing_vli;
  }
  
  public void setIacApdRoutingVli(String iac_apd_routing_vli)
  {
    this.iac_apd_routing_vli = iac_apd_routing_vli;
  }
  
  public String getIacApdNumberOfCities()
  {
    return this.iac_apd_number_of_cities;
  }
  
  public void setIacApdNumberOfCities(String iac_apd_number_of_cities)
  {
    this.iac_apd_number_of_cities = iac_apd_number_of_cities;
  }
  
  public String getIacApdRoutingCities()
  {
    return this.iac_apd_routing_cities;
  }
  
  public void setIacApdRoutingCities(String iac_apd_routing_cities)
  {
    this.iac_apd_routing_cities = iac_apd_routing_cities;
  }
  
  public String getIacApdCarriersId()
  {
    return this.iac_apd_carriers_id;
  }
  
  public void setIacApdCarriersId(String iac_apd_carriers_id)
  {
    this.iac_apd_carriers_id = iac_apd_carriers_id;
  }
  
  public String getIacApdCarriersVli()
  {
    return this.iac_apd_carriers_vli;
  }
  
  public void setIacApdCarriersVli(String iac_apd_carriers_vli)
  {
    this.iac_apd_carriers_vli = iac_apd_carriers_vli;
  }
  
  public String getIacApdNumberOfCarriers()
  {
    return this.iac_apd_number_of_carriers;
  }
  
  public void setIacApdNumberOfCarriers(String iac_apd_number_of_carriers)
  {
    this.iac_apd_number_of_carriers = iac_apd_number_of_carriers;
  }
  
  public String getIacApdCarriers()
  {
    return this.iac_apd_carriers;
  }
  
  public void setIacApdCarriers(String iac_apd_carriers)
  {
    this.iac_apd_carriers = iac_apd_carriers;
  }
  
  public String getIacApdFareBasis()
  {
    return this.iac_apd_fare_basis;
  }
  
  public void setIacApdFareBasis(String iac_apd_fare_basis)
  {
    this.iac_apd_fare_basis = iac_apd_fare_basis;
  }
  
  public String getIacApdNumberOfPassengers()
  {
    return this.iac_apd_number_of_passengers;
  }
  
  public void setIacApdNumberOfPassengers(String iac_apd_number_of_passengers)
  {
    this.iac_apd_number_of_passengers = iac_apd_number_of_passengers;
  }
  
  public String getApdCardMemberNameId()
  {
    return this.apd_cardmember_name_id;
  }
  
  public void setApdCardMemberNameId(String apd_cardmember_name_id)
  {
    this.apd_cardmember_name_id = apd_cardmember_name_id;
  }
  
  public String getApdCardMemberNameVli()
  {
    return this.apd_cardmember_name_vli;
  }
  
  public void setApdCardMemberNameVli(String apd_cardmember_name_vli)
  {
    this.apd_cardmember_name_vli = apd_cardmember_name_vli;
  }
  
  public String getApdCardMemberName()
  {
    return this.apd_cardmember_name;
  }
  
  public void setApdCardMemberName(String apd_cardmember_name)
  {
    this.apd_cardmember_name = apd_cardmember_name;
  }
  
  public String getApdETicketIndicator()
  {
    return this.apd_eticket_indicator;
  }
  
  public void setApdETicketIndicator(String apd_eticket_indicator)
  {
    this.apd_eticket_indicator = apd_eticket_indicator;
  }
  
  public String getApdReservationCodeId()
  {
    return this.apd_reservation_code_id;
  }
  
  public void setApdReservationCodeId(String apd_reservation_code_id)
  {
    this.apd_reservation_code_id = apd_reservation_code_id;
  }
  
  public String getApdReservationCodeVli()
  {
    return this.apd_reservation_code_vli;
  }
  
  public void setApdReservationCodeVli(String apd_reservation_code_vli)
  {
    this.apd_reservation_code_vli = apd_reservation_code_vli;
  }
  
  public String getApdReservationCode()
  {
    return this.apd_reservation_code;
  }
  
  public void setApdReservationCode(String apd_reservation_code)
  {
    this.apd_reservation_code = apd_reservation_code;
  }
  
  public String getCpdVersionNumber()
  {
    return this.cpd_versionNumber;
  }
  
  public void setCpdVersionNumber(String cpd_versionNumber)
  {
    this.cpd_versionNumber = cpd_versionNumber;
  }
  
  public String getCpdGoodsSoldId()
  {
    return this.cpd_goods_sold_id;
  }
  
  public void setCpdGoodsSoldId(String cpd_goods_sold_id)
  {
    this.cpd_goods_sold_id = cpd_goods_sold_id;
  }
  
  public String getCpdGoodsSoldProductCode()
  {
    return this.cpd_goods_sold_product_code;
  }
  
  public void setCpdGoodsSoldProductCode(String cpd_goods_sold_product_code)
  {
    this.cpd_goods_sold_product_code = cpd_goods_sold_product_code;
  }
  
  public String getCpdGoodsSoldProductCodeVli()
  {
    return this.cpd_goods_sold_product_code_vli;
  }
  
  public void setCpdGoodsSoldProductCodeVli(String cpd_goods_sold_product_code_vli)
  {
    this.cpd_goods_sold_product_code_vli = cpd_goods_sold_product_code_vli;
  }
  
  public String populateAdditionalDataNational(AuthorizationResponseBean authorizationResponseBean)
  {
    if (this.debugflag) {
      Log.i("Populating Additional Data National");
    }
    String additionalData = null;
    if (ISOUtil.IsNullOrEmpty(getSecondaryId()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD472_ADDITIONALDATANATIONAL);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if (getSecondaryId().equalsIgnoreCase("ITD"))
    {
      additionalData = populateCardNotPresentData(authorizationResponseBean);
    }
    else if (getSecondaryId().equalsIgnoreCase("IAC"))
    {
      additionalData = populateInternetAirlineCustomerData(authorizationResponseBean);
    }
    else if (getSecondaryId().equalsIgnoreCase("APD"))
    {
      additionalData = populateAirlinePassengerData(authorizationResponseBean);
    }
    else if (getSecondaryId().equalsIgnoreCase("CPD"))
    {
      additionalData = populateCPDData(authorizationResponseBean);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD472_ADDITIONALDATANATIONAL);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (this.debugflag) {
      Log.i("Additional Data National Bean\r\n----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n-                                 Addtnl Data National Bean                                                                                                                                       -\r\n---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n" +
      
        this.displayBuffer.toString() + 
        "\r\n" + "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
    }
    return additionalData;
  }
  
  private String populateCardNotPresentData(AuthorizationResponseBean authorizationResponseBean)
  {
    StringBuffer additionalDataNational1 = new StringBuffer();
    if (!ISOUtil.IsNullOrEmpty(getPrimaryId())) {
      if ((Validator.validateSubFields(getPrimaryId(), 2, 2, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1])) && (getPrimaryId().equalsIgnoreCase("AX")))
      {
        additionalDataNational1.append(getPrimaryId());
        this.displayBuffer.append("PRIMARY_ID               ").append(" :: ").append(getPrimaryId()).append(Constants.NEW_LINE_CHAR);
      }
      else
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD471_ADDITIONALDATANATIONAL);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
    }
    if (!ISOUtil.IsNullOrEmpty(getSecondaryId())) {
      if ((Validator.validateSubFields(getSecondaryId(), 3, 3, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1])) && (getSecondaryId().equalsIgnoreCase("ITD")))
      {
        additionalDataNational1.append(getSecondaryId());
        this.displayBuffer.append("SECONDARY_ID             ").append(" :: ").append(getSecondaryId()).append(Constants.NEW_LINE_CHAR);
      }
      else
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD472_ADDITIONALDATANATIONAL);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
    }
    if (!ISOUtil.IsNullOrEmpty(getCustomerEmailId()))
    {
      setCustomerEmailId(ISOUtil.padString(getCustomerEmailId(), 3, " ", true, false));
      if ((Validator.validateSubFields(getCustomerEmailId(), 3, 3, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1])) && (getCustomerEmailId().equalsIgnoreCase("CE ")))
      {
        additionalDataNational1.append(getCustomerEmailId());
        this.displayBuffer.append("CUST_EMAIL_ID            ").append(" :: ").append(getCustomerEmailId()).append(Constants.NEW_LINE_CHAR);
      }
      else
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD473_ADDITIONALDATANATIONAL);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
    }
    if (ISOUtil.IsNullOrEmpty(getCustomerEmail()))
    {
      additionalDataNational1.append("01");
      additionalDataNational1.append(" ");
    }
    else if (Validator.validateSubFields(getCustomerEmail(), 60, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2]))
    {
      additionalDataNational1.append(ISOUtil.getFieldLength(getCustomerEmail()));
      additionalDataNational1.append(getCustomerEmail());
      this.displayBuffer.append("CUST_EMAIL               ").append(" :: ").append(getCustomerEmail()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD474_ADDITIONALDATANATIONAL);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (!ISOUtil.IsNullOrEmpty(getCustomerHostnameId()))
    {
      setCustomerHostnameId(ISOUtil.padString(getCustomerHostnameId(), 3, " ", true, false));
      if ((Validator.validateSubFields(getCustomerHostnameId(), 3, 3, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1])) && (getCustomerHostnameId().equalsIgnoreCase("CH ")))
      {
        additionalDataNational1.append(getCustomerHostnameId());
        this.displayBuffer.append("CUST_HOST_NAME_ID        ").append(" :: ").append(getCustomerHostnameId()).append(Constants.NEW_LINE_CHAR);
      }
      else
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD475_ADDITIONALDATANATIONAL);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
    }
    if (ISOUtil.IsNullOrEmpty(getCustomerHostname()))
    {
      additionalDataNational1.append("01");
      additionalDataNational1.append(" ");
    }
    else if (Validator.validateSubFields(getCustomerHostname(), 60, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2]))
    {
      additionalDataNational1.append(ISOUtil.getFieldLength(getCustomerHostname()));
      additionalDataNational1.append(getCustomerHostname());
      this.displayBuffer.append("CUST_HOST_NAME           ").append(" :: ").append(getCustomerHostname()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD476_ADDITIONALDATANATIONAL);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (!ISOUtil.IsNullOrEmpty(getHttpBrowserTypeId())) {
      if ((Validator.validateSubFields(getHttpBrowserTypeId(), 3, 3, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1])) && (getHttpBrowserTypeId().equalsIgnoreCase("HBT")))
      {
        additionalDataNational1.append(getHttpBrowserTypeId());
        this.displayBuffer.append("HTTP_BROWSE_TYPE_ID      ").append(" :: ").append(getHttpBrowserTypeId()).append(Constants.NEW_LINE_CHAR);
      }
      else
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD477_ADDITIONALDATANATIONAL);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
    }
    if (ISOUtil.IsNullOrEmpty(getHttpBrowserType()))
    {
      additionalDataNational1.append("01");
      additionalDataNational1.append(" ");
    }
    else if (Validator.validateSubFields(getHttpBrowserType(), 60, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2]))
    {
      additionalDataNational1.append(ISOUtil.getFieldLength(getHttpBrowserType()));
      additionalDataNational1.append(getHttpBrowserType());
      this.displayBuffer.append("HTTP_BROWSE_TYPE         ").append(" :: ").append(getHttpBrowserType()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD478_ADDITIONALDATANATIONAL);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (!ISOUtil.IsNullOrEmpty(getShipToCountryId())) {
      if ((Validator.validateSubFields(getShipToCountryId(), 3, 3, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1])) && (getShipToCountryId().equalsIgnoreCase("STC")))
      {
        additionalDataNational1.append(getShipToCountryId());
        this.displayBuffer.append("SHIP_TO_CNTRY_ID         ").append(" :: ").append(getShipToCountryId()).append(Constants.NEW_LINE_CHAR);
      }
      else
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD479_ADDITIONALDATANATIONAL);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
    }
    if (ISOUtil.IsNullOrEmpty(getShipToCountry()))
    {
      setShipToCountry(ISOUtil.padString(getShipToCountry(), 3, "0", false, true));
      additionalDataNational1.append(ISOUtil.getFieldLength(getShipToCountry()));
      additionalDataNational1.append(getShipToCountry());
    }
    else
    {
      setShipToCountry(ISOUtil.padString(getShipToCountry(), 3, "0", false, true));
      if (Validator.validateSubFields(getShipToCountry(), 3, 3, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1]))
      {
        additionalDataNational1.append(ISOUtil.getFieldLength(getShipToCountry()));
        additionalDataNational1.append(getShipToCountry());
        this.displayBuffer.append("SHIP_TO_CNTRY            ").append(" :: ").append(getShipToCountry()).append(Constants.NEW_LINE_CHAR);
      }
      else
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4710_ADDITIONALDATANATIONAL);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
    }
    if (!ISOUtil.IsNullOrEmpty(getShippingMethodId()))
    {
      setShippingMethodId(ISOUtil.padString(getShippingMethodId(), 3, " ", true, false));
      if ((Validator.validateSubFields(getShippingMethodId(), 3, 3, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1])) && (getShippingMethodId().equalsIgnoreCase("SM ")))
      {
        additionalDataNational1.append(getShippingMethodId());
        this.displayBuffer.append("SHIPPING_MTHD_ID         ").append(" :: ").append(getShippingMethodId()).append(Constants.NEW_LINE_CHAR);
      }
      else
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4711_ADDITIONALDATANATIONAL);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
    }
    additionalDataNational1.append("02");
    this.displayBuffer.append("SHIPPING_MTHD_ID_VLI     ").append(" :: ").append("02").append(Constants.NEW_LINE_CHAR);
    if (ISOUtil.IsNullOrEmpty(getShippingMethod()))
    {
      setShippingMethod(ISOUtil.padString(getShippingMethod(), 2, " ", true, false));
      additionalDataNational1.append(getShippingMethod());
    }
    else if (Validator.validateSubFields(getShippingMethod(), 2, 2, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1]))
    {
      additionalDataNational1.append(getShippingMethod());
      this.displayBuffer.append("SHIPPING_MTHD             ").append(" :: ").append(getShippingMethod()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4712_ADDITIONALDATANATIONAL);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (!ISOUtil.IsNullOrEmpty(getMerchantProductskuId())) {
      if ((Validator.validateSubFields(getMerchantProductskuId(), 3, 3, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1])) && (getMerchantProductskuId().equalsIgnoreCase("MPS")))
      {
        additionalDataNational1.append(getMerchantProductskuId());
        this.displayBuffer.append("MERCH_PROD_SKU_ID        ").append(" :: ").append(getMerchantProductskuId()).append(Constants.NEW_LINE_CHAR);
      }
      else
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4713_ADDITIONALDATANATIONAL);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
    }
    if (ISOUtil.IsNullOrEmpty(getMerchantProductSku()))
    {
      additionalDataNational1.append("01");
      additionalDataNational1.append(" ");
    }
    else if (Validator.validateSubFields(getMerchantProductSku(), 15, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2]))
    {
      additionalDataNational1.append(ISOUtil.getFieldLength(getMerchantProductSku()));
      additionalDataNational1.append(getMerchantProductSku());
      this.displayBuffer.append("MERCH_PROD_SKU           ").append(" :: ").append(getMerchantProductSku()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4714_ADDITIONALDATANATIONAL);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getCustomerIP()))
    {
      setCustomerIP(ISOUtil.padString(getCustomerIP(), 15, " ", true, false));
      additionalDataNational1.append(getCustomerIP());
    }
    else if (Validator.validateSubFields(getCustomerIP(), 15, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2]))
    {
      String customerIP = ISOUtil.padString(getCustomerIP(), 15, " ", true, false);
      additionalDataNational1.append(customerIP);
      this.displayBuffer.append("CUSTOMER_IP              ").append(" :: ").append(customerIP).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4715_ADDITIONALDATANATIONAL);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getCustomerANI()))
    {
      setCustomerANI(ISOUtil.padString(getCustomerANI(), 10, " ", true, false));
      additionalDataNational1.append(getCustomerANI());
    }
    else if (Validator.validateSubFields(getCustomerANI(), 10, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2]))
    {
      setCustomerANI(ISOUtil.padString(getCustomerANI(), 10, " ", true, false));
      additionalDataNational1.append(getCustomerANI());
      this.displayBuffer.append("CUSTOMER_ANI             ").append(" :: ").append(getCustomerANI()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4716_ADDITIONALDATANATIONAL);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getCustomerInfoIdentifier()))
    {
      setCustomerInfoIdentifier(ISOUtil.padString(getCustomerInfoIdentifier(), 2, " ", true, false));
      additionalDataNational1.append(getCustomerInfoIdentifier());
    }
    else if (Validator.validateSubFields(getCustomerInfoIdentifier(), 2, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2]))
    {
      setCustomerInfoIdentifier(ISOUtil.padString(getCustomerInfoIdentifier(), 2, " ", true, false));
      additionalDataNational1.append(getCustomerInfoIdentifier());
      this.displayBuffer.append("CUSTOMER_INFO_IDENTIFIER ").append(" :: ").append(getCustomerInfoIdentifier()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4717_ADDITIONALDATANATIONAL);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (this.debugflag) {
      Log.i("Populated Card Not Present Data is -- " + additionalDataNational1);
    }
    return additionalDataNational1.toString();
  }
  
  private String populateInternetAirlineCustomerData(AuthorizationResponseBean authorizationResponseBean)
  {
    StringBuffer nationalData1 = new StringBuffer();
    if (!ISOUtil.IsNullOrEmpty(getPrimaryId())) {
      if ((Validator.validateSubFields(getPrimaryId(), 2, 2, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1])) && (getPrimaryId().equalsIgnoreCase("AX")))
      {
        nationalData1.append(getPrimaryId());
        this.displayBuffer.append("PRIMARY_ID                ").append(" :: ").append(getPrimaryId()).append(Constants.NEW_LINE_CHAR);
      }
      else
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD471_ADDITIONALDATANATIONAL);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
    }
    if (!ISOUtil.IsNullOrEmpty(getSecondaryId())) {
      if ((Validator.validateSubFields(getSecondaryId(), 3, 3, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1])) && (getSecondaryId().equalsIgnoreCase("IAC")))
      {
        nationalData1.append(getSecondaryId());
        this.displayBuffer.append("SECONDARY_ID             ").append(" :: ").append(getSecondaryId()).append(Constants.NEW_LINE_CHAR);
      }
      else
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD472_ADDITIONALDATANATIONAL);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
    }
    if (ISOUtil.IsNullOrEmpty(getIacApdDepartureDate()))
    {
      setIacApdDepartureDate(ISOUtil.padString(getIacApdDepartureDate(), 8, "0", false, true));
      nationalData1.append(getIacApdDepartureDate());
    }
    else if (Validator.validateSubFields(getIacApdDepartureDate(), 8, 8, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[0]))
    {
      nationalData1.append(getIacApdDepartureDate());
      this.displayBuffer.append("IAC_APT_DEPART_DATE      ").append(" :: ").append(getIacApdDepartureDate()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4719_ADDITIONALDATANATIONAL);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (!ISOUtil.IsNullOrEmpty(getIacApdPassengerNameId())) {
      if ((Validator.validateSubFields(getIacApdPassengerNameId(), 3, 3, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1])) && (getIacApdPassengerNameId().equalsIgnoreCase("APN")))
      {
        nationalData1.append(getIacApdPassengerNameId());
        this.displayBuffer.append("IAC_APT_PSSNGR_NAME_ID").append(" :: ").append(getIacApdPassengerNameId()).append(Constants.NEW_LINE_CHAR);
      }
      else
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4720_ADDITIONALDATANATIONAL);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
    }
    if (ISOUtil.IsNullOrEmpty(getIacApdPassengerName()))
    {
      setIacApdPassengerName(ISOUtil.padString(getIacApdPassengerName(), 23, " ", true, false));
      nationalData1.append(ISOUtil.getFieldLength(getIacApdPassengerName()));
      nationalData1.append(getIacApdPassengerName());
    }
    else if (Validator.validateData(getIacApdPassengerName(), com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2]))
    {
      if (getIacApdPassengerName().length() < 23) {
        setIacApdPassengerName(ISOUtil.padString(getIacApdPassengerName(), 23, " ", true, false));
      } else if (getIacApdPassengerName().length() > 40) {
        setIacApdPassengerName(getIacApdPassengerName().substring(0, 40));
      }
      nationalData1.append(ISOUtil.getFieldLength(getIacApdPassengerName()));
      nationalData1.append(getIacApdPassengerName());
      this.displayBuffer.append("IAC_APT_PSSNGR_NAME      ").append(" :: ").append(getIacApdPassengerName()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4721_ADDITIONALDATANATIONAL);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getIacApdOrigin()))
    {
      nationalData1.append(ISOUtil.padString(getIacApdOrigin(), 5, " ", true, false));
    }
    else if (Validator.validateSubFields(getIacApdOrigin(), 5, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2]))
    {
      nationalData1.append(ISOUtil.padString(getIacApdOrigin(), 5, " ", true, false));
      this.displayBuffer.append("IAC_APT_ORIGIN           ").append(" :: ").append(ISOUtil.padString(getIacApdOrigin(), 5, " ", true, false)).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4722_ADDITIONALDATANATIONAL);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getIacApdDest()))
    {
      nationalData1.append(ISOUtil.padString(getIacApdDest(), 5, " ", true, false));
    }
    else if (Validator.validateSubFields(getIacApdDest(), 5, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2]))
    {
      nationalData1.append(ISOUtil.padString(getIacApdDest(), 5, " ", true, false));
      this.displayBuffer.append("IAC_APT_DEST             ").append(" :: ").append(ISOUtil.padString(getIacApdDest(), 5, " ", true, false)).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4723_ADDITIONALDATANATIONAL);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (!ISOUtil.IsNullOrEmpty(getIacApdRoutingId())) {
      if ((Validator.validateSubFields(getIacApdRoutingId(), 3, 3, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1])) && (getIacApdRoutingId().equalsIgnoreCase("RTG")))
      {
        nationalData1.append(getIacApdRoutingId());
        this.displayBuffer.append("IAC_APT_ROUTING_ID       ").append(" :: ").append(getIacApdRoutingId()).append(Constants.NEW_LINE_CHAR);
      }
      else
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4724_ADDITIONALDATANATIONAL);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
    }
    if (ISOUtil.IsNullOrEmpty(getIacApdNumberOfCities()))
    {
      setIacApdNumberOfCities(ISOUtil.padString(getIacApdNumberOfCities(), 2, "0", false, true));
    }
    else if (Validator.validateSubFields(getIacApdNumberOfCities(), 2, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[0]))
    {
      setIacApdNumberOfCities(ISOUtil.padString(getIacApdNumberOfCities(), 2, "0", false, true));
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4725_ADDITIONALDATANATIONAL);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getIacApdRoutingCities()))
    {
      setIacApdRoutingCities(ISOUtil.padString(getIacApdRoutingCities(), 11, " ", true, false));
    }
    else
    {
      setIacApdRoutingCities(ISOUtil.padString(getIacApdRoutingCities(), 11, " ", true, false));
      if (!Validator.validateSubFields(getIacApdRoutingCities(), 59, 11, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[3]))
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4726_ADDITIONALDATANATIONAL);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
    }
    nationalData1.append(ISOUtil.getFieldLength(getIacApdNumberOfCities() + getIacApdRoutingCities()));
    nationalData1.append(getIacApdNumberOfCities());
    nationalData1.append(getIacApdRoutingCities());
    this.displayBuffer.append("IAC_APT_NO_OF_CITIES     ").append(" :: ").append(getIacApdNumberOfCities()).append(Constants.NEW_LINE_CHAR);
    this.displayBuffer.append("IAC_APT_ROUTING_CITIES   ").append(" :: ").append(getIacApdRoutingCities()).append(Constants.NEW_LINE_CHAR);
    if (!ISOUtil.IsNullOrEmpty(getIacApdCarriersId())) {
      if ((Validator.validateSubFields(getIacApdCarriersId(), 3, 3, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1])) && (getIacApdCarriersId().equalsIgnoreCase("ALC")))
      {
        nationalData1.append(getIacApdCarriersId());
        this.displayBuffer.append("IAC_APT_CARRIERS_ID     ").append(" :: ").append(getIacApdCarriersId()).append(Constants.NEW_LINE_CHAR);
      }
      else
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4727_ADDITIONALDATANATIONAL);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
    }
    if (ISOUtil.IsNullOrEmpty(getIacApdNumberOfCarriers()))
    {
      setIacApdNumberOfCarriers(ISOUtil.padString(getIacApdNumberOfCarriers(), 2, "0", false, true));
    }
    else if (Validator.validateSubFields(getIacApdNumberOfCarriers(), 2, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[0]))
    {
      setIacApdNumberOfCarriers(ISOUtil.padString(getIacApdNumberOfCarriers(), 2, "0", false, true));
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4728_ADDITIONALDATANATIONAL);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getIacApdCarriers()))
    {
      setIacApdCarriers(ISOUtil.padString(getIacApdCarriers(), 5, " ", true, false));
    }
    else
    {
      setIacApdCarriers(ISOUtil.padString(getIacApdCarriers(), 5, " ", true, false));
      if (!Validator.validateSubFields(getIacApdCarriers(), 53, 5, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[3]))
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4729_ADDITIONALDATANATIONAL);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
    }
    nationalData1.append(ISOUtil.getFieldLength(getIacApdNumberOfCarriers() + getIacApdCarriers()));
    nationalData1.append(getIacApdNumberOfCarriers());
    nationalData1.append(getIacApdCarriers());
    this.displayBuffer.append("IAC_APT_NUMBR_OF_CARRIERS").append(" :: ").append(getIacApdNumberOfCarriers()).append(Constants.NEW_LINE_CHAR);
    this.displayBuffer.append("IAC_APT_CARRIERS         ").append(" :: ").append(getIacApdCarriers()).append(Constants.NEW_LINE_CHAR);
    if (ISOUtil.IsNullOrEmpty(getIacApdFareBasis()))
    {
      setIacApdFareBasis(ISOUtil.padString(getIacApdFareBasis(), 24, " ", true, false));
      nationalData1.append(getIacApdFareBasis());
    }
    else if (Validator.validateData(getIacApdFareBasis(), com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2]))
    {
      if (getIacApdFareBasis().length() < 24) {
        setIacApdFareBasis(ISOUtil.padString(getIacApdFareBasis(), 24, " ", true, false));
      } else if (getIacApdFareBasis().length() > 24) {
        setIacApdFareBasis(getIacApdFareBasis().substring(0, 24));
      }
      nationalData1.append(getIacApdFareBasis());
      this.displayBuffer.append("IAC_APT_FARE_BASIS       ").append(" :: ").append(getIacApdFareBasis()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4730_ADDITIONALDATANATIONAL);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getIacApdNumberOfPassengers()))
    {
      setIacApdNumberOfPassengers(ISOUtil.padString(getIacApdNumberOfPassengers(), 3, "0", false, true));
      nationalData1.append(getIacApdNumberOfPassengers());
    }
    else if (Validator.validateSubFields(getIacApdNumberOfPassengers(), 3, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[0]))
    {
      setIacApdNumberOfPassengers(ISOUtil.padString(getIacApdNumberOfPassengers(), 3, "0", false, true));
      nationalData1.append(getIacApdNumberOfPassengers());
      this.displayBuffer.append("IAC_APT_NO_OF_PSSNGRS    ").append(" :: ").append(getIacApdNumberOfPassengers()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4731_ADDITIONALDATANATIONAL);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getCustomerIP()))
    {
      nationalData1.append(ISOUtil.padString(getCustomerIP(), 15, " ", true, false));
    }
    else if (Validator.validateSubFields(getCustomerIP(), 15, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2]))
    {
      String customerIP = ISOUtil.padString(getCustomerIP(), 15, " ", true, false);
      nationalData1.append(customerIP);
      this.displayBuffer.append("CUSTOMER_IP              ").append(" :: ").append(customerIP).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4715_ADDITIONALDATANATIONAL);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (!ISOUtil.IsNullOrEmpty(getCustomerEmailId()))
    {
      setCustomerEmailId(ISOUtil.padString(getCustomerEmailId(), 3, " ", true, false));
      if ((Validator.validateSubFields(getCustomerEmailId(), 3, 3, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1])) && (getCustomerEmailId().equalsIgnoreCase("CE ")))
      {
        nationalData1.append(getCustomerEmailId());
        this.displayBuffer.append("CUST_EMAIL_ID            ").append(" :: ").append(getCustomerEmailId()).append(Constants.NEW_LINE_CHAR);
      }
      else
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD473_ADDITIONALDATANATIONAL);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
    }
    if (ISOUtil.IsNullOrEmpty(getCustomerEmail()))
    {
      nationalData1.append("01");
      nationalData1.append(" ");
    }
    else if (Validator.validateSubFields(getCustomerEmail(), 60, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2]))
    {
      nationalData1.append(ISOUtil.getFieldLength(getCustomerEmail()));
      nationalData1.append(getCustomerEmail());
      this.displayBuffer.append("CUST_EMAIL               ").append(" :: ").append(getCustomerEmail()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD474_ADDITIONALDATANATIONAL);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
      Log.i("Populated Internet Airline Customer Data is -- " + nationalData1);
    }
    return nationalData1.toString();
  }
  
  private String populateAirlinePassengerData(AuthorizationResponseBean authorizationResponseBean)
  {
    StringBuffer nationalData1 = new StringBuffer();
    if (!ISOUtil.IsNullOrEmpty(getPrimaryId())) {
      if ((Validator.validateSubFields(getPrimaryId(), 2, 2, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1])) && (getPrimaryId().equalsIgnoreCase("AX")))
      {
        nationalData1.append(getPrimaryId());
        this.displayBuffer.append("PRIMARY_ID               ").append(" :: ").append(getPrimaryId()).append(Constants.NEW_LINE_CHAR);
      }
      else
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD471_ADDITIONALDATANATIONAL);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
    }
    if (!ISOUtil.IsNullOrEmpty(getSecondaryId())) {
      if ((Validator.validateSubFields(getSecondaryId(), 3, 3, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1])) && (getSecondaryId().equalsIgnoreCase("APD")))
      {
        nationalData1.append(getSecondaryId());
        this.displayBuffer.append("SECONDARY_ID             ").append(" :: ").append(getSecondaryId()).append(Constants.NEW_LINE_CHAR);
      }
      else
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD472_ADDITIONALDATANATIONAL);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
    }
    if (ISOUtil.IsNullOrEmpty(getIacApdDepartureDate()))
    {
      nationalData1.append(ISOUtil.padString(getIacApdDepartureDate(), 8, "0", false, true));
    }
    else if (Validator.validateSubFields(getIacApdDepartureDate(), 8, 8, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[0]))
    {
      nationalData1.append(getIacApdDepartureDate());
      this.displayBuffer.append("IAC_APT_DEPART_DATE       ").append(" :: ").append(getIacApdDepartureDate()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4719_ADDITIONALDATANATIONAL);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (!ISOUtil.IsNullOrEmpty(getIacApdPassengerNameId())) {
      if ((Validator.validateSubFields(getIacApdPassengerNameId(), 3, 3, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1])) && (getIacApdPassengerNameId().equalsIgnoreCase("APN")))
      {
        nationalData1.append(getIacApdPassengerNameId());
        this.displayBuffer.append("IAC_APT_PSSNGR_NAME_ID   ").append(" :: ").append(getIacApdPassengerNameId()).append(Constants.NEW_LINE_CHAR);
      }
      else
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4720_ADDITIONALDATANATIONAL);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
    }
    if (ISOUtil.IsNullOrEmpty(getIacApdPassengerName()))
    {
      setIacApdPassengerName(ISOUtil.padString(getIacApdPassengerName(), 23, " ", true, false));
      nationalData1.append(ISOUtil.getFieldLength(getIacApdPassengerName()));
      nationalData1.append(getIacApdPassengerName());
    }
    else if (Validator.validateData(getIacApdPassengerName(), com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2]))
    {
      if (getIacApdPassengerName().length() < 23) {
        setIacApdPassengerName(ISOUtil.padString(getIacApdPassengerName(), 23, " ", true, false));
      } else if (getIacApdPassengerName().length() > 40) {
        setIacApdPassengerName(getIacApdPassengerName().substring(0, 40));
      }
      nationalData1.append(ISOUtil.getFieldLength(getIacApdPassengerName()));
      nationalData1.append(getIacApdPassengerName());
      this.displayBuffer.append("IAC_APT_PSSNGR_NAME           ").append(" :: ").append(getIacApdPassengerName()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4721_ADDITIONALDATANATIONAL);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (!ISOUtil.IsNullOrEmpty(getApdCardMemberNameId()))
    {
      setApdCardMemberNameId(ISOUtil.padString(getApdCardMemberNameId(), 3, " ", true, false));
      if ((Validator.validateSubFields(getApdCardMemberNameId(), 3, 3, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1])) && (getApdCardMemberNameId().equalsIgnoreCase("CN ")))
      {
        nationalData1.append(getApdCardMemberNameId());
        this.displayBuffer.append("APT_CARD_MEMBR_NAME_ID   ").append(" :: ").append(getApdCardMemberNameId()).append(Constants.NEW_LINE_CHAR);
      }
      else
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4732_ADDITIONALDATANATIONAL);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
    }
    if (ISOUtil.IsNullOrEmpty(getApdCardMemberName()))
    {
      setApdCardMemberName(ISOUtil.padString(getApdCardMemberName(), 23, " ", true, false));
      nationalData1.append(ISOUtil.getFieldLength(getApdCardMemberName()));
      nationalData1.append(getApdCardMemberName());
    }
    else if (Validator.validateData(getApdCardMemberName(), com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2]))
    {
      if (getApdCardMemberName().length() < 23) {
        setApdCardMemberName(ISOUtil.padString(getApdCardMemberName(), 23, " ", true, false));
      } else if (getApdCardMemberName().length() > 40) {
        setApdCardMemberName(getApdCardMemberName().substring(0, 40));
      }
      nationalData1.append(ISOUtil.getFieldLength(getApdCardMemberName()));
      nationalData1.append(getApdCardMemberName());
      this.displayBuffer.append("APT_CARD_MEMBR_NAME      ").append(" :: ").append(getApdCardMemberName()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4733_ADDITIONALDATANATIONAL);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getIacApdOrigin()))
    {
      nationalData1.append(ISOUtil.padString(getIacApdOrigin(), 5, " ", true, false));
    }
    else if (Validator.validateSubFields(getIacApdOrigin(), 5, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2]))
    {
      nationalData1.append(ISOUtil.padString(getIacApdOrigin(), 5, " ", true, false));
      this.displayBuffer.append("IAC_APT_ORIGIN           ").append(" :: ").append(ISOUtil.padString(getIacApdOrigin(), 5, " ", true, false)).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4722_ADDITIONALDATANATIONAL);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getIacApdDest()))
    {
      nationalData1.append(ISOUtil.padString(getIacApdDest(), 5, " ", true, false));
    }
    else if (Validator.validateSubFields(getIacApdDest(), 5, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2]))
    {
      nationalData1.append(ISOUtil.padString(getIacApdDest(), 5, " ", true, false));
      this.displayBuffer.append("IAC_APT_DEST             ").append(" :: ").append(ISOUtil.padString(getIacApdDest(), 5, " ", true, false)).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4723_ADDITIONALDATANATIONAL);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (!ISOUtil.IsNullOrEmpty(getIacApdRoutingId())) {
      if ((Validator.validateSubFields(getIacApdRoutingId(), 3, 3, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1])) && (getIacApdRoutingId().equalsIgnoreCase("RTG")))
      {
        nationalData1.append(getIacApdRoutingId());
        this.displayBuffer.append("IAC_APT_ROUTING_ID       ").append(" :: ").append(getIacApdRoutingId()).append(Constants.NEW_LINE_CHAR);
      }
      else
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4724_ADDITIONALDATANATIONAL);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
    }
    if (ISOUtil.IsNullOrEmpty(getIacApdNumberOfCities()))
    {
      setIacApdNumberOfCities(ISOUtil.padString(getIacApdNumberOfCities(), 2, "0", false, true));
    }
    else if (Validator.validateSubFields(getIacApdNumberOfCities(), 2, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[0]))
    {
      setIacApdNumberOfCities(ISOUtil.padString(getIacApdNumberOfCities(), 2, "0", false, true));
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4725_ADDITIONALDATANATIONAL);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getIacApdRoutingCities()))
    {
      setIacApdRoutingCities(ISOUtil.padString(getIacApdRoutingCities(), 11, " ", true, false));
    }
    else
    {
      setIacApdRoutingCities(ISOUtil.padString(getIacApdRoutingCities(), 11, " ", true, false));
      if (!Validator.validateSubFields(getIacApdRoutingCities(), 59, 11, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[3]))
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4726_ADDITIONALDATANATIONAL);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
    }
    nationalData1.append(ISOUtil.getFieldLength(getIacApdNumberOfCities() + getIacApdRoutingCities()));
    nationalData1.append(getIacApdNumberOfCities());
    nationalData1.append(getIacApdRoutingCities());
    this.displayBuffer.append("IAC_APT_NO_OF_CITIES     ").append(" :: ").append(getIacApdNumberOfCities()).append(Constants.NEW_LINE_CHAR);
    this.displayBuffer.append("IAC_APT_ROUTING_CITIES   ").append(" :: ").append(getIacApdRoutingCities()).append(Constants.NEW_LINE_CHAR);
    if (!ISOUtil.IsNullOrEmpty(getIacApdCarriersId())) {
      if ((Validator.validateSubFields(getIacApdCarriersId(), 3, 3, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1])) && (getIacApdCarriersId().equalsIgnoreCase("ALC")))
      {
        nationalData1.append(getIacApdCarriersId());
        this.displayBuffer.append("IAC_APT_CARRIERS_ID      ").append(" :: ").append(getIacApdCarriersId()).append(Constants.NEW_LINE_CHAR);
      }
      else
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4727_ADDITIONALDATANATIONAL);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
    }
    if (ISOUtil.IsNullOrEmpty(getIacApdNumberOfCarriers()))
    {
      setIacApdNumberOfCarriers(ISOUtil.padString(getIacApdNumberOfCarriers(), 2, "0", false, true));
    }
    else if (Validator.validateSubFields(getIacApdNumberOfCarriers(), 2, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[0]))
    {
      setIacApdNumberOfCarriers(ISOUtil.padString(getIacApdNumberOfCarriers(), 2, "0", false, true));
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4728_ADDITIONALDATANATIONAL);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getIacApdCarriers()))
    {
      setIacApdCarriers(ISOUtil.padString(getIacApdCarriers(), 5, " ", true, false));
    }
    else
    {
      setIacApdCarriers(ISOUtil.padString(getIacApdCarriers(), 5, " ", true, false));
      if (!Validator.validateSubFields(getIacApdCarriers(), 53, 5, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[3]))
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4729_ADDITIONALDATANATIONAL);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
    }
    nationalData1.append(ISOUtil.getFieldLength(getIacApdNumberOfCarriers() + getIacApdCarriers()));
    nationalData1.append(getIacApdNumberOfCarriers());
    nationalData1.append(getIacApdCarriers());
    this.displayBuffer.append("IAC_APT_NUMBR_OF_CARRIERS").append(" :: ").append(getIacApdNumberOfCarriers()).append(Constants.NEW_LINE_CHAR);
    this.displayBuffer.append("IAC_APT_CARRIERS         ").append(" :: ").append(getIacApdCarriers()).append(Constants.NEW_LINE_CHAR);
    if (ISOUtil.IsNullOrEmpty(getIacApdFareBasis()))
    {
      setIacApdFareBasis(ISOUtil.padString(getIacApdFareBasis(), 24, " ", true, false));
      nationalData1.append(getIacApdFareBasis());
    }
    else if (Validator.validateData(getIacApdFareBasis(), com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2]))
    {
      if (getIacApdFareBasis().length() < 24) {
        setIacApdFareBasis(ISOUtil.padString(getIacApdFareBasis(), 24, " ", true, false));
      } else if (getIacApdFareBasis().length() > 24) {
        setIacApdFareBasis(getIacApdFareBasis().substring(0, 24));
      }
      nationalData1.append(getIacApdFareBasis());
      this.displayBuffer.append("IAC_APT_FARE_BASIS       ").append(" :: ").append(getIacApdFareBasis()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4730_ADDITIONALDATANATIONAL);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getIacApdNumberOfPassengers()))
    {
      setIacApdNumberOfPassengers(ISOUtil.padString(getIacApdNumberOfPassengers(), 3, "0", false, true));
      nationalData1.append(getIacApdNumberOfPassengers());
    }
    else if (Validator.validateSubFields(getIacApdNumberOfPassengers(), 3, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[0]))
    {
      setIacApdNumberOfPassengers(ISOUtil.padString(getIacApdNumberOfPassengers(), 3, "0", false, true));
      nationalData1.append(getIacApdNumberOfPassengers());
      this.displayBuffer.append("IAC_APT_NO_OF_PSSNGRS    ").append(" :: ").append(getIacApdNumberOfPassengers()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4731_ADDITIONALDATANATIONAL);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getApdETicketIndicator()))
    {
      setApdETicketIndicator(ISOUtil.padString(getApdETicketIndicator(), 1, " ", true, false));
      nationalData1.append(getApdETicketIndicator());
    }
    else if (Validator.validateSubFields(getApdETicketIndicator(), 1, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2]))
    {
      nationalData1.append(getApdETicketIndicator());
      this.displayBuffer.append("APT_ETICKET_INDICATOR    ").append(" :: ").append(getApdETicketIndicator()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4734_ADDITIONALDATANATIONAL);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (!ISOUtil.IsNullOrEmpty(getApdReservationCodeId())) {
      if ((Validator.validateSubFields(getApdReservationCodeId(), 3, 3, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1])) && (getApdReservationCodeId().equalsIgnoreCase("RES")))
      {
        nationalData1.append(getApdReservationCodeId());
        this.displayBuffer.append("APT_RSRVATN_CODE_ID      ").append(" :: ").append(getApdReservationCodeId()).append(Constants.NEW_LINE_CHAR);
      }
      else
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4735_ADDITIONALDATANATIONAL);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
    }
    if (ISOUtil.IsNullOrEmpty(getApdReservationCode()))
    {
      setApdReservationCode(ISOUtil.padString(getApdReservationCode(), 6, " ", true, false));
      nationalData1.append(ISOUtil.getFieldLength(getApdReservationCode()));
      nationalData1.append(getApdReservationCode());
    }
    else if (Validator.validateSubFields(getApdReservationCode(), 15, 6, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2]))
    {
      nationalData1.append(ISOUtil.getFieldLength(getApdReservationCode()));
      nationalData1.append(getApdReservationCode());
      this.displayBuffer.append("APT_RSRVATN_CODE         ").append(" :: ").append(getApdReservationCode()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4736_ADDITIONALDATANATIONAL);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (this.debugflag) {
      Log.i("Populated Airline Passenger Data is -- " + nationalData1);
    }
    return nationalData1.toString();
  }
  
  private String populateCPDData(AuthorizationResponseBean authorizationResponseBean)
  {
    StringBuffer nationalData1 = new StringBuffer();
    if (!ISOUtil.IsNullOrEmpty(getPrimaryId())) {
      if ((Validator.validateSubFields(getPrimaryId(), 2, 2, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1])) && (getPrimaryId().equalsIgnoreCase("AX")))
      {
        nationalData1.append(getPrimaryId());
        this.displayBuffer.append("PRIMARY_ID                    ").append(" :: ").append(getPrimaryId()).append(Constants.NEW_LINE_CHAR);
      }
      else
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD471_ADDITIONALDATANATIONAL);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
    }
    if (!ISOUtil.IsNullOrEmpty(getSecondaryId())) {
      if ((Validator.validateSubFields(getSecondaryId(), 3, 3, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1])) && (getSecondaryId().equalsIgnoreCase("CPD")))
      {
        nationalData1.append(getSecondaryId());
        this.displayBuffer.append("SECONDARY_ID                  ").append(" :: ").append(getSecondaryId()).append(Constants.NEW_LINE_CHAR);
      }
      else
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD472_ADDITIONALDATANATIONAL);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
    }
    if (!ISOUtil.IsNullOrEmpty(getCpdVersionNumber()))
    {
      if (Validator.validateSubFields(getCpdVersionNumber(), 2, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[0]))
      {
        nationalData1.append(ISOUtil.padString(getCpdVersionNumber(), 2, "0", false, true));
        this.displayBuffer.append("Version_Number                ").append(" :: ").append(getCpdVersionNumber()).append(Constants.NEW_LINE_CHAR);
      }
      else
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4737_ADDITIONALDATANATIONAL);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4737_ADDITIONALDATANATIONAL);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (!ISOUtil.IsNullOrEmpty(getCpdGoodsSoldId()))
    {
      if (Validator.validateSubFields(getCpdGoodsSoldId(), 3, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[4]))
      {
        nationalData1.append(ISOUtil.padString(getCpdGoodsSoldId(), 3, " ", true, false));
        this.displayBuffer.append("Goods_Sold_ID                 ").append(" :: ").append(getCpdGoodsSoldId()).append(Constants.NEW_LINE_CHAR);
      }
      else
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4738_ADDITIONALDATANATIONAL);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4738_ADDITIONALDATANATIONAL);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    nationalData1.append(ISOUtil.padString(Integer.toString(getCpdGoodsSoldProductCode().length()), 2, "0", false, true));
    this.displayBuffer.append("GOODS_SOLD_PRODUCTCODE_VLI    ").append(" :: ").append(ISOUtil.padString(Integer.toString(getCpdGoodsSoldProductCode().length()), 2, "0", false, true)).append(Constants.NEW_LINE_CHAR);
    if (!ISOUtil.IsNullOrEmpty(getCpdGoodsSoldProductCode()))
    {
      if (Validator.validateSubFields(getCpdGoodsSoldProductCode(), 4, 4, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1]))
      {
        nationalData1.append(getCpdGoodsSoldProductCode());
        this.displayBuffer.append("Goods_Sold_PRODUCT_CODE       ").append(" :: ").append(getCpdGoodsSoldProductCode()).append(Constants.NEW_LINE_CHAR);
      }
      else
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4739_ADDITIONALDATANATIONAL);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4739_ADDITIONALDATANATIONAL);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    Log.i("Populated CPD Data is :-- " + nationalData1);
    return nationalData1.toString();
  }
  
  public String toString()
  {
    StringBuffer buffer = new StringBuffer();
    
    buffer.append("PRIMARY_ID").append(" :: ").append(getPrimaryId()).append(Constants.NEW_LINE_CHAR);
    buffer.append("SECONDARY_ID").append(" :: ").append(getSecondaryId()).append(Constants.NEW_LINE_CHAR);
    buffer.append("CUST_EMAIL_ID").append(" :: ").append(getCustomerEmailId()).append(Constants.NEW_LINE_CHAR);
    buffer.append("CUST_EMAIL_ID_VLI").append(" :: ").append(getCustomerEmailIdVli()).append(Constants.NEW_LINE_CHAR);
    buffer.append("CUST_EMAIL").append(" :: ").append(getCustomerEmail()).append(Constants.NEW_LINE_CHAR);
    buffer.append("CUST_HOST_NAME_ID").append(" :: ").append(getCustomerHostnameId()).append(Constants.NEW_LINE_CHAR);
    buffer.append("CUST_HOST_NAME_ID_VLI").append(" :: ").append(getCustomerHostnameIdVli()).append(Constants.NEW_LINE_CHAR);
    buffer.append("CUST_HOST_NAME").append(" :: ").append(getCustomerHostname()).append(Constants.NEW_LINE_CHAR);
    buffer.append("HTTP_BROWSE_TYPE_ID").append(" :: ").append(getHttpBrowserTypeId()).append(Constants.NEW_LINE_CHAR);
    buffer.append("HTTP_BROWSE_TYPE_ID_VLI").append(" :: ").append(getHttpBrowserTypeIdVli()).append(Constants.NEW_LINE_CHAR);
    buffer.append("HTTP_BROWSE_TYPE").append(" :: ").append(getHttpBrowserType()).append(Constants.NEW_LINE_CHAR);
    buffer.append("SHIP_TO_CNTRY_ID").append(" :: ").append(getShipToCountryId()).append(Constants.NEW_LINE_CHAR);
    buffer.append("SHIP_TO_CNTRY_ID_VLI").append(" :: ").append(getShipToCountryIdVli()).append(Constants.NEW_LINE_CHAR);
    buffer.append("SHIP_TO_CNTRY").append(" :: ").append(getShipToCountry()).append(Constants.NEW_LINE_CHAR);
    
    buffer.append("SHIPPING_MTHD_ID").append(" :: ").append(getShippingMethodId()).append(Constants.NEW_LINE_CHAR);
    buffer.append("SHIPPING_MTHD_ID_VLI").append(" :: ").append(getShippingMethodIdVli()).append(Constants.NEW_LINE_CHAR);
    buffer.append("SHIPPING_MTHD").append(" :: ").append(getShippingMethod()).append(Constants.NEW_LINE_CHAR);
    buffer.append("MERCH_PROD_SKU_ID").append(" :: ").append(getMerchantProductskuId()).append(Constants.NEW_LINE_CHAR);
    buffer.append("MERCH_PROD_SKU_ID_VLI").append(" :: ").append(getMerchantProductSkuIdVli()).append(Constants.NEW_LINE_CHAR);
    buffer.append("MERCH_PROD_SKU").append(" :: ").append(getMerchantProductSku()).append(Constants.NEW_LINE_CHAR);
    
    buffer.append("CUSTOMER_IP").append(" :: ").append(getCustomerIP()).append(Constants.NEW_LINE_CHAR);
    buffer.append("CUSTOMER_ANI").append(" :: ").append(getCustomerANI()).append(Constants.NEW_LINE_CHAR);
    buffer.append("CUSTOMER_INFO_IDENTIFIER").append(" :: ").append(getCustomerInfoIdentifier()).append(Constants.NEW_LINE_CHAR);
    buffer.append("IAC_APT_DEPART_DATE").append(" :: ").append(getIacApdDepartureDate()).append(Constants.NEW_LINE_CHAR);
    buffer.append("IAC_APT_PSSNGR_NAME_ID").append(" :: ").append(getIacApdPassengerNameId()).append(Constants.NEW_LINE_CHAR);
    buffer.append("IAC_APT_PSSNGR_NAME_VLI").append(" :: ").append(getIacApdPassengerNameVli()).append(Constants.NEW_LINE_CHAR);
    buffer.append("IAC_APT_PSSNGR_NAME").append(" :: ").append(getIacApdPassengerName()).append(Constants.NEW_LINE_CHAR);
    buffer.append("IAC_APT_ORIGIN").append(" :: ").append(getIacApdOrigin()).append(Constants.NEW_LINE_CHAR);
    
    buffer.append("IAC_APT_DEST").append(" :: ").append(getIacApdDest()).append(Constants.NEW_LINE_CHAR);
    buffer.append("IAC_APT_ROUTING_ID").append(" :: ").append(getIacApdRoutingId()).append(Constants.NEW_LINE_CHAR);
    buffer.append("IAC_APT_ROUTING_VLI").append(" :: ").append(getIacApdRoutingVli()).append(Constants.NEW_LINE_CHAR);
    buffer.append("IAC_APT_NO_OF_CITIES").append(" :: ").append(getIacApdNumberOfCities()).append(Constants.NEW_LINE_CHAR);
    buffer.append("IAC_APT_ROUTING_CITIES").append(" :: ").append(getIacApdRoutingCities()).append(Constants.NEW_LINE_CHAR);
    buffer.append("IAC_APT_CARRIERS_ID").append(" :: ").append(getIacApdCarriersId()).append(Constants.NEW_LINE_CHAR);
    buffer.append("IAC_APT_CARRIERS_VLI").append(" :: ").append(getIacApdCarriersVli()).append(Constants.NEW_LINE_CHAR);
    buffer.append("IAC_APT_NUMBR_OF_CARRIERS").append(" :: ").append(getIacApdNumberOfCarriers()).append(Constants.NEW_LINE_CHAR);
    
    buffer.append("IAC_APT_CARRIERS").append(" :: ").append(getIacApdCarriers()).append(Constants.NEW_LINE_CHAR);
    buffer.append("IAC_APT_FARE_BASIS").append(" :: ").append(getIacApdFareBasis()).append(Constants.NEW_LINE_CHAR);
    buffer.append("IAC_APT_NO_OF_PSSNGRS").append(" :: ").append(getIacApdNumberOfPassengers()).append(Constants.NEW_LINE_CHAR);
    buffer.append("APT_CARD_MEMBR_NAME_ID").append(" :: ").append(getApdCardMemberNameId()).append(Constants.NEW_LINE_CHAR);
    buffer.append("APT_CARD_MEMBR_NAME_VLI").append(" :: ").append(getApdCardMemberNameVli()).append(Constants.NEW_LINE_CHAR);
    buffer.append("APT_CARD_MEMBR_NAME").append(" :: ").append(getApdCardMemberName()).append(Constants.NEW_LINE_CHAR);
    buffer.append("APT_ETICKET_INDICATOR").append(" :: ").append(getApdETicketIndicator()).append(Constants.NEW_LINE_CHAR);
    buffer.append("APT_RSRVATN_CODE_ID").append(" :: ").append(getApdReservationCodeId()).append(Constants.NEW_LINE_CHAR);
    buffer.append("APT_RSRVATN_CODE_VLI").append(" :: ").append(getApdReservationCodeVli()).append(Constants.NEW_LINE_CHAR);
    buffer.append("APT_RSRVATN_CODE").append(" :: ").append(getApdReservationCode()).append(Constants.NEW_LINE_CHAR);
    
    return buffer.toString();
  }
}
