package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations;

public enum RateClassCd {
    Day("D"),
    Evening("E"),
    Night("N"),
    Special_Discount("S");

    private final String rateClassCd;

    private RateClassCd(String rateClassCd) {
        this.rateClassCd = rateClassCd;
    }

    public String getRateClassCd() {
        return this.rateClassCd;
    }
}
