package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean;

import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.connection.PropertyReader;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.Constants;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.AuthErrorCodes;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.utils.ISOUtil;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.validator.Validator;
import java.util.List;

public class SecurityRelatedControlInfoBean
{
  private String primaryId = "AX";
  private String secondaryId = "KSN";
  private String kSN;

  public String getPrimaryId()
  {
    return this.primaryId;
  }
  
  public void setPrimaryId(String primaryId)
  {
    this.primaryId = primaryId;
  }
  
  public String getSecondaryId()
  {
    return this.secondaryId;
  }
  
  public void setSecondaryId(String secondaryId)
  {
    this.secondaryId = secondaryId;
  }
  
  public String getkSN()
  {
    return this.kSN;
  }
  
  public void setkSN(String kSN)
  {
    this.kSN = kSN;
  }
  
  public String populateSecurityRelatedControlInfoBean(AuthorizationResponseBean pAuthorizationResponseBean)
  {
    boolean debugflag = PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON");
    StringBuffer lSecurityRelatedControlInfo = new StringBuffer();
    StringBuffer lDisplayBuffer = new StringBuffer();
    int lSecurityRelatedControlInfoVLI = 0;
    ISOUtil lISOUtil = new ISOUtil();
    String lEncodedVLI = "";
    if (ISOUtil.IsNullOrEmpty(getPrimaryId()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD53_SECURITYRELATEDCONTROLINFOPRIMARYID);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if (!"AX".equalsIgnoreCase(getPrimaryId()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD53_SECURITYRELATEDCONTROLINFOPRIMARYID);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else
    {
      lSecurityRelatedControlInfo.append(lISOUtil.convertTo(getPrimaryId(), "ENC_HEX"));
      lDisplayBuffer.append("PRIMARY_ID                     ").append(" :: ").append(getPrimaryId()).append(Constants.NEW_LINE_CHAR);
      lSecurityRelatedControlInfoVLI = 2;
    }
    if (ISOUtil.IsNullOrEmpty(getSecondaryId()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD53_SECURITYRELATEDCONTROLINFOSECONDARYID);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if (!"KSN".equalsIgnoreCase(getSecondaryId()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD53_SECURITYRELATEDCONTROLINFOSECONDARYID);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else
    {
      lSecurityRelatedControlInfo.append(lISOUtil.convertTo(getSecondaryId(), "ENC_HEX"));
      lDisplayBuffer.append("SECONDARY_ID                     ").append(" :: ").append(getSecondaryId()).append(Constants.NEW_LINE_CHAR);
      lSecurityRelatedControlInfoVLI += 3;
    }
    if (ISOUtil.IsNullOrEmpty(getkSN()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD53_SECURITYRELATEDCONTROLINFOKSN);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if (!Validator.validateSubFields(getkSN(), 20, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[6]))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD53_SECURITYRELATEDCONTROLINFOKSN);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else
    {
      String length = null;
      if (getkSN().length() <= 9) {
        length = "0" + getkSN().length() / 2;
      } else {
        length = String.valueOf(getkSN().length() / 2);
      }
      lSecurityRelatedControlInfo.append(lISOUtil.convertTo(length, "ENC_HEX"));
      
      lDisplayBuffer.append("KSN VLI  ").append(" :: ").append(length).append(Constants.NEW_LINE_CHAR);
      
      lSecurityRelatedControlInfo.append(getkSN());
      lDisplayBuffer.append("KSN    ").append(" :: ").append(getkSN()).append(Constants.NEW_LINE_CHAR);
      lSecurityRelatedControlInfoVLI = lSecurityRelatedControlInfoVLI + 2 + getkSN().length() / 2;
    }
    String lVLI = ISOUtil.padString(String.valueOf(lSecurityRelatedControlInfoVLI), 2, "0", false, true);
    lEncodedVLI = lISOUtil.convertTo(lVLI, "ENC_HEX");
    if (debugflag)
    {
      Log.i("Security Related Control Information Bean\r\n----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n-                                 Security Related Control Information Bean                                                                                                                                       -\r\n---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n" +
      
        lDisplayBuffer.toString() + 
        "\r\n" + "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
      
      Log.i("Populated Security Related Control Information  -- " + lSecurityRelatedControlInfo);
    }
    return lEncodedVLI + lSecurityRelatedControlInfo.toString();
  }
  
  public String toString()
  {
    StringBuffer lSecurityRelatedControlData = new StringBuffer();
    lSecurityRelatedControlData.append("PRIMARY ID").append(" :: ").append(getPrimaryId()).append(Constants.NEW_LINE_CHAR);
    lSecurityRelatedControlData.append("SECONDARY ID").append(" :: ").append(getSecondaryId()).append(Constants.NEW_LINE_CHAR);
    lSecurityRelatedControlData.append("KSN").append(" :: ").append(getkSN()).append(Constants.NEW_LINE_CHAR);
    return lSecurityRelatedControlData.toString();
  }
}
