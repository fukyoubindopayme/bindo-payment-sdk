package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.nonindustrytype;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.NonIndustrySpecificTAABean;

public class DeferredPaymentPlanBean
  extends NonIndustrySpecificTAABean
{
  private String fullTransactionAmount;
  private String typeOfPlanCode;
  private String noOfInstallments;
  private String amountOfInstallment;
  private String installmentNumber = "0001";
  private String contractNumber;
  private String paymentTypeCode1;
  private String paymentTypeAmount1;
  private String paymentTypeCode2;
  private String paymentTypeAmount2;
  private String paymentTypeCode3;
  private String paymentTypeAmount3;
  private String paymentTypeCode4;
  private String paymentTypeAmount4;
  private String paymentTypeCode5;
  private String paymentTypeAmount5;
  
  public String getFullTransactionAmount()
  {
    return this.fullTransactionAmount;
  }
  
  public void setFullTransactionAmount(String fullTransactionAmount)
  {
    this.fullTransactionAmount = fullTransactionAmount;
  }
  
  public String getTypeOfPlanCode()
  {
    return this.typeOfPlanCode;
  }
  
  public void setTypeOfPlanCode(String typeOfPlanCode)
  {
    this.typeOfPlanCode = typeOfPlanCode;
  }
  
  public String getNoOfInstallments()
  {
    return this.noOfInstallments;
  }
  
  public void setNoOfInstallments(String noOfInstallments)
  {
    this.noOfInstallments = noOfInstallments;
  }
  
  public String getAmountOfInstallment()
  {
    return this.amountOfInstallment;
  }
  
  public void setAmountOfInstallment(String amountOfInstallment)
  {
    this.amountOfInstallment = amountOfInstallment;
  }
  
  public String getInstallmentNumber()
  {
    return this.installmentNumber;
  }
  
  public void setInstallmentNumber(String installmentNumber)
  {
    this.installmentNumber = installmentNumber;
  }
  
  public String getContractNumber()
  {
    return this.contractNumber;
  }
  
  public void setContractNumber(String contractNumber)
  {
    this.contractNumber = contractNumber;
  }
  
  public String getPaymentTypeCode1()
  {
    return this.paymentTypeCode1;
  }
  
  public void setPaymentTypeCode1(String paymentTypeCode1)
  {
    this.paymentTypeCode1 = paymentTypeCode1;
  }
  
  public String getPaymentTypeAmount1()
  {
    return this.paymentTypeAmount1;
  }
  
  public void setPaymentTypeAmount1(String paymentTypeAmount1)
  {
    this.paymentTypeAmount1 = paymentTypeAmount1;
  }
  
  public String getPaymentTypeCode2()
  {
    return this.paymentTypeCode2;
  }
  
  public void setPaymentTypeCode2(String paymentTypeCode2)
  {
    this.paymentTypeCode2 = paymentTypeCode2;
  }
  
  public String getPaymentTypeAmount2()
  {
    return this.paymentTypeAmount2;
  }
  
  public void setPaymentTypeAmount2(String paymentTypeAmount2)
  {
    this.paymentTypeAmount2 = paymentTypeAmount2;
  }
  
  public String getPaymentTypeCode3()
  {
    return this.paymentTypeCode3;
  }
  
  public void setPaymentTypeCode3(String paymentTypeCode3)
  {
    this.paymentTypeCode3 = paymentTypeCode3;
  }
  
  public String getPaymentTypeAmount3()
  {
    return this.paymentTypeAmount3;
  }
  
  public void setPaymentTypeAmount3(String paymentTypeAmount3)
  {
    this.paymentTypeAmount3 = paymentTypeAmount3;
  }
  
  public String getPaymentTypeCode4()
  {
    return this.paymentTypeCode4;
  }
  
  public void setPaymentTypeCode4(String paymentTypeCode4)
  {
    this.paymentTypeCode4 = paymentTypeCode4;
  }
  
  public String getPaymentTypeAmount4()
  {
    return this.paymentTypeAmount4;
  }
  
  public void setPaymentTypeAmount4(String paymentTypeAmount4)
  {
    this.paymentTypeAmount4 = paymentTypeAmount4;
  }
  
  public String getPaymentTypeCode5()
  {
    return this.paymentTypeCode5;
  }
  
  public void setPaymentTypeCode5(String paymentTypeCode5)
  {
    this.paymentTypeCode5 = paymentTypeCode5;
  }
  
  public String getPaymentTypeAmount5()
  {
    return this.paymentTypeAmount5;
  }
  
  public void setPaymentTypeAmount5(String paymentTypeAmount5)
  {
    this.paymentTypeAmount5 = paymentTypeAmount5;
  }
}
