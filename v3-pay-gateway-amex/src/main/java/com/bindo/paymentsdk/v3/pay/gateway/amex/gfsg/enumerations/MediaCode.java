package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations;

public enum MediaCode {
    Cardmember_signature_on_File("01"),
    Phone_order("02"),
    Mail_Order("03"),
    Internet_Order("04"),
    Recurring_Billing("05");

    private final String mediacode;

    private MediaCode(String mediaCode) {
        this.mediacode = mediaCode;
    }

    public String getMediaCode() {
        return this.mediacode;
    }
}
