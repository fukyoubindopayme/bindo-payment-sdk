package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.formats;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.NonIndustrySpecificTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.nonindustrytype.CPSLevel2Bean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.nonindustrytype.DeferredPaymentPlanBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.nonindustrytype.EMVBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.connection.PropertyReader;
import java.io.IOException;
import java.util.List;
import org.apache.log4j.Logger;

public final class NonIndustryTAARecordFormatter
{
  private static final Logger LOGGER = Logger.getLogger(NonIndustryTAARecordFormatter.class);
  
  public static void formatNonIndustrySpecificTypeTAARecord(NonIndustrySpecificTAABean nonIndustrySpecificTAABean, List<String> formatedMessageList)
    throws IOException
  {
    if ((nonIndustrySpecificTAABean instanceof CPSLevel2Bean))
    {
      CPSLevel2Bean cpsLevel2Bean = (CPSLevel2Bean)nonIndustrySpecificTAABean;
      formatCPSLevel2Record(cpsLevel2Bean, formatedMessageList);
    }
    else if ((nonIndustrySpecificTAABean instanceof DeferredPaymentPlanBean))
    {
      DeferredPaymentPlanBean deferredPaymentPlanBean = (DeferredPaymentPlanBean)nonIndustrySpecificTAABean;
      formatDeferredPaymentPlanRecord(deferredPaymentPlanBean, formatedMessageList);
    }
    else if ((nonIndustrySpecificTAABean instanceof EMVBean))
    {
      EMVBean emvBean = (EMVBean)nonIndustrySpecificTAABean;
      formatEMVRecord(emvBean, formatedMessageList);
    }
  }
  
  public static void formatEMVRecord(EMVBean emvBean, List<String> formatedMessageList)
    throws IOException
  {
    StringBuffer formattedMessage = new StringBuffer();
    
    SettlementMessageFormatter.formatValue(emvBean.getRecordType(), 
      formattedMessage, 3, false, true, true, false, true);
    SettlementMessageFormatter.formatValue(emvBean.getRecordNumber(), formattedMessage, 8, true, false, false, true, false);
    
    SettlementMessageFormatter.formatValue(emvBean.getTransactionIdentifier(), 
      formattedMessage, 15, true, false, false, true, false);
    
    SettlementMessageFormatter.formatValue(emvBean.getEMVFormatType(), 
      formattedMessage, 2, true, false, false, true, false);
    SettlementMessageFormatter.formatValue(emvBean.getAddendaTypeCode(), 
      formattedMessage, 2, false, true, false, false, true);
    
    SettlementMessageFormatter.formatValue(emvBean.getICCSystemRelatedData(), formattedMessage, 256, false, true, false, false, true);
    if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
      SettlementMessageFormatter.characterSpacesFill(formattedMessage, 414);
    }
    formatedMessageList.add(formattedMessage.toString());
    if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
      LOGGER.info("Formated EMV NonIndustry Type Record :" + formattedMessage.toString());
    }
  }
  
  public static void formatCPSLevel2Record(CPSLevel2Bean cPSLevel2Bean, List<String> formatedMessageList)
    throws IOException
  {
    StringBuffer formattedMessage = new StringBuffer();
    
    SettlementMessageFormatter.formatValue(cPSLevel2Bean.getRecordType(), 
      formattedMessage, 3, false, true, true, false, true);
    SettlementMessageFormatter.formatValue(cPSLevel2Bean.getRecordNumber(), formattedMessage, 8, true, false, false, true, false);
    
    SettlementMessageFormatter.formatValue(cPSLevel2Bean.getTransactionIdentifier(), 
      formattedMessage, 15, true, false, false, true, false);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 2);
    
    SettlementMessageFormatter.formatValue(cPSLevel2Bean.getAddendaTypeCode(), 
      formattedMessage, 2, false, true, false, false, true);
    
    SettlementMessageFormatter.formatValue(cPSLevel2Bean.getRequesterName(), formattedMessage, 38, false, true, true, false, true);
    
    SettlementMessageFormatter.characterSpacesFill(formattedMessage, 2);
    
    SettlementMessageFormatter.formatValue(cPSLevel2Bean.getChargeDescription1(), formattedMessage, 40, false, true, true, false, true);
    
    SettlementMessageFormatter.formatValue(cPSLevel2Bean.getChargeItemQuantity1(), formattedMessage, 3, true, false, false, true, false);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(cPSLevel2Bean.getChargeItemAmount1(), formattedMessage, 12, true, false, false, true, false);
    
    SettlementMessageFormatter.formatValue(cPSLevel2Bean.getChargeDescription2(), formattedMessage, 40, false, true, true, false, true);
    
    SettlementMessageFormatter.formatValue(cPSLevel2Bean.getChargeItemQuantity2(), formattedMessage, 3, true, false, false, true, false);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(cPSLevel2Bean.getChargeItemAmount2(), formattedMessage, 12, true, false, false, true, false);
    
    SettlementMessageFormatter.formatValue(cPSLevel2Bean.getChargeDescription3(), formattedMessage, 40, false, true, true, false, true);
    
    SettlementMessageFormatter.formatValue(cPSLevel2Bean.getChargeItemQuantity3(), formattedMessage, 3, true, false, false, true, false);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(cPSLevel2Bean.getChargeItemAmount3(), formattedMessage, 12, true, false, false, true, false);
    
    SettlementMessageFormatter.formatValue(cPSLevel2Bean.getChargeDescription4(), formattedMessage, 40, false, true, true, false, true);
    
    SettlementMessageFormatter.formatValue(cPSLevel2Bean.getChargeItemQuantity4(), formattedMessage, 3, true, false, false, true, false);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(cPSLevel2Bean.getChargeItemAmount4(), formattedMessage, 12, true, false, false, true, false);
    
    SettlementMessageFormatter.formatValue(cPSLevel2Bean.getCardMemberReferenceNumber(), formattedMessage, 20, false, true, true, false, true);
    
    SettlementMessageFormatter.formatValue(cPSLevel2Bean.getShipToPostalCode(), formattedMessage, 15, false, true, true, false, true);
    
    SettlementMessageFormatter.characterSpacesFill(formattedMessage, 13);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(cPSLevel2Bean.getTotalTaxAmount(), formattedMessage, 12, true, false, false, true, false);
    
    SettlementMessageFormatter.characterSpacesFill(formattedMessage, 20);
    
    SettlementMessageFormatter.formatValue(cPSLevel2Bean.getTaxTypeCode(), formattedMessage, 3, false, true, true, false, true);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 7);
    if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
      SettlementMessageFormatter.characterSpacesFill(formattedMessage, 305);
    }
    formatedMessageList.add(formattedMessage.toString());
    if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
      LOGGER.info("Formated CPSLevel2 NonIndustry Type Record :" + formattedMessage.toString());
    }
  }
  
  public static void formatDeferredPaymentPlanRecord(DeferredPaymentPlanBean deferredPaymentPlanBean, List<String> formatedMessageList)
    throws IOException
  {
    StringBuffer formattedMessage = new StringBuffer();
    
    SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getRecordType(), 
      formattedMessage, 3, false, true, true, false, true);
    SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getRecordNumber(), formattedMessage, 8, true, false, false, true, false);
    
    SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getTransactionIdentifier(), 
      formattedMessage, 15, true, false, false, true, false);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 2);
    
    SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getAddendaTypeCode(), 
      formattedMessage, 2, false, true, false, false, true);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getFullTransactionAmount(), 
      formattedMessage, 12, true, false, false, true, false);
    
    SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getTypeOfPlanCode(), 
      formattedMessage, 4, false, true, false, false, true);
    
    SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getNoOfInstallments(), 
      formattedMessage, 4, true, false, false, true, false);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getAmountOfInstallment(), 
      formattedMessage, 12, true, false, false, true, false);
    
    SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getInstallmentNumber(), 
      formattedMessage, 4, true, false, false, true, false);
    
    SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getContractNumber(), 
      formattedMessage, 14, false, true, false, false, true);
    
    SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getPaymentTypeCode1(), 
      formattedMessage, 2, false, true, false, false, true);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getPaymentTypeAmount1(), 
      formattedMessage, 12, true, false, false, true, false);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 15);
    
    SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getPaymentTypeCode2(), 
      formattedMessage, 2, false, true, false, false, true);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getPaymentTypeAmount2(), 
      formattedMessage, 12, true, false, false, true, false);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 15);
    
    SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getPaymentTypeCode3(), 
      formattedMessage, 2, false, true, false, false, true);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getPaymentTypeAmount3(), 
      formattedMessage, 12, true, false, false, true, false);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 15);
    
    SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getPaymentTypeCode4(), 
      formattedMessage, 2, false, true, false, false, true);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getPaymentTypeAmount4(), 
      formattedMessage, 12, true, false, false, true, false);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 15);
    
    SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getPaymentTypeCode5(), 
      formattedMessage, 2, false, true, false, false, true);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(deferredPaymentPlanBean.getPaymentTypeAmount5(), 
      formattedMessage, 12, true, false, false, true, false);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 15);
    if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
      SettlementMessageFormatter.characterSpacesFill(formattedMessage, 454);
    }
    formatedMessageList.add(formattedMessage.toString());
    if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
      LOGGER.info("Formated DPP NonIndustry Type Record :" + formattedMessage.toString());
    }
  }
}
