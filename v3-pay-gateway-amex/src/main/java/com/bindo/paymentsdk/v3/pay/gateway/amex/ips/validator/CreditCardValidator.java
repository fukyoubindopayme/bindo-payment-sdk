package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.validator;

import java.util.ArrayList;
import java.util.Collection;

public class CreditCardValidator {
    public static final int NONE = 0;
    public static final int AMEX = 1;
    public static final int VISA = 2;
    public static final int MASTERCARD = 4;
    public static final int DISCOVER = 8;
    private Collection<CreditCardType> cardTypes;

    public CreditCardValidator() {
        this(15);
    }

    public CreditCardValidator(int options) {
        this.cardTypes = new ArrayList();
        this.cardTypes.add(new Amex());
    }

    public boolean isValidCardNumber(String card) {
        boolean isValid = true;
        if (card == null || card.length() < 13 || card.length() > 19) {
            isValid = false;
        }

        return isValid;
    }

    public void addAllowedCardType(CreditCardType type) {
        this.cardTypes.add(type);
    }

    protected boolean modulusTenCheck(String cardNumber) {
        int digits = cardNumber.length();
        int oddOrEven = digits & 1;
        long sum = 0L;

        for(int count = 0; count < digits; ++count) {
            boolean var7 = false;

            int digit;
            try {
                digit = Integer.parseInt(String.valueOf(cardNumber.charAt(count)));
            } catch (NumberFormatException var9) {
                return false;
            }

            if ((count & 1 ^ oddOrEven) == 0) {
                digit *= 2;
                if (digit > 9) {
                    digit -= 9;
                }
            }

            sum += (long)digit;
        }

        return sum == 0L ? false : sum % 10L == 0L;
    }
}
