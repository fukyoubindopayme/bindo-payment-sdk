package com.bindo.paymentsdk.v3.pay.gateway.amex.util.juice;

public class JuiceConstants {
    public static final String AES = "AES/ECB/NoPadding";
    public static final String TRIPLEDES = "DESede/ECB/NoPadding";
    public static final String TRIPLEDES_CBC = "DESede/CBC/PKCS5Padding";
    public static final String SHA1 = "SHA-1";
    public static final String SHA256 = "SHA-256";
    public static final int MAXIMUM_DATA_BLOCKSIZE = 32767;
    public static int onlyOnce = 1;
    protected static final String AES_OLD = "AES";
    protected static final String TRIPLEDES_OLD = "DES-EDE3";

    public JuiceConstants() {
    }
}
