package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums;

public enum ExecutionType
{
  DigitalGatewayMerchSoln("DigGateway");
  
  private final String executionType;
  
  private ExecutionType(String executionType)
  {
    this.executionType = executionType;
  }
  
  public String getExecutionType()
  {
    return this.executionType;
  }
}
