package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.messageformatter;

import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.connection.PropertyReader;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.Constants;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.ActionCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.AdditionalResponseData;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.AuthErrorCodes;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.CountryCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.CurrencyCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.FunctionCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.MessageTypeID;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.ProcessingCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.SafeKeyEnabledCountries;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.pos.CardDataInputMode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.pos.CardPresent;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.pos.CardholderPresent;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.exception.IPSAPIException;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean.AuthorizationRequestBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean.AuthorizationResponseBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean.ErrorObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean.IntegratedCircuitCardRelatedDataBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean.KeyManagementDataBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean.PrivateUseData2Bean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.utils.DataConversion;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.utils.FieldBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.utils.ISOUtil;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.utils.Utility;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.validator.Validator;
import java.io.IOException;
import java.io.StringReader;
import java.util.LinkedList;

public class AuthorizationMessageFormatter extends ISOUtil implements AuthorizationConstants {
    long primary_header = 0L;
    long secondary_header = 0L;
    StringBuffer requestParameter = new StringBuffer();
    StringBuffer printRequest = new StringBuffer();
    boolean calculateIccLength = false;
    Utility objUtility = new Utility();
    boolean debugflag = false;
    StringBuffer respMessage = new StringBuffer();
    StringBuffer reqMessage = new StringBuffer();
    StringBuffer printHexResponse = new StringBuffer();

    public AuthorizationMessageFormatter() {
    }

    public String createISORequest(AuthorizationRequestBean authorizationRequestBean) {
        this.reqMessage.append("BitNum   Field Name                                   Value" + NEW_LINE_CHAR);
        String isoFormatedRequestMessage = null;

        try {
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                this.debugflag = true;
                Log.i("Entered into convertRequestBeanToHex()");
            }

            if (authorizationRequestBean != null) {
                if (MessageTypeID.ISO_AuthorizationAdjustmentFinancialAdvice.getMessageTypeID().equalsIgnoreCase(authorizationRequestBean.getMessageTypeIdentifier())) {
                    isoFormatedRequestMessage = this.createAdjustmentFinancialAdviceRequest(authorizationRequestBean);
                } else {
                    isoFormatedRequestMessage = this.createRequest(authorizationRequestBean);
                }
            }
        } catch (IPSAPIException var8) {
            if (this.debugflag) {
                Log.e("Error in trustAllHttpsCertificates() method..  Exception:" + var8);
            }
        } catch (Exception var9) {
            if (this.debugflag) {
                Log.e("Error in trustAllHttpsCertificates() method..  Exception:" + var9);
            }
        } finally {
            if (this.debugflag) {
                Utility.debug(Utility.trackTrace.toString(), true);
                this.debugflag = false;
            }

        }

        if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
            Log.i("After Converting RequestBean To Hex ");
            Log.i("Request Details\r\n----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n-                                 Request in Object format                                                                                                                                       -\r\n---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n" + this.reqMessage.toString() + "\r\n" + "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            Log.i("Exiting from convertRequestBeanToHex()");
        }

        return isoFormatedRequestMessage;
    }

    public String createDigitalGatewayISORequest(AuthorizationRequestBean authorizationRequestBean) {
        this.reqMessage.append("BitNum   Field Name                                   Value" + NEW_LINE_CHAR);
        String isoFormatedRequestMessage = null;

        try {
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                this.debugflag = true;
                Log.i("Entered into convertRequestBeanToHex()");
            }

            if (authorizationRequestBean != null) {
                isoFormatedRequestMessage = this.createDigitalGatewayRequest(authorizationRequestBean);
            }
        } catch (IPSAPIException var8) {
            if (this.debugflag) {
                Log.e("Error in trustAllHttpsCertificates() method..  Exception:" + var8);
            }
        } catch (Exception var9) {
            if (this.debugflag) {
                Log.e("Error in trustAllHttpsCertificates() method..  Exception:" + var9);
            }
        } finally {
            if (this.debugflag) {
                Utility.debug(Utility.trackTrace.toString(), true);
                this.debugflag = false;
            }

        }

        if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
            Log.i("After ConvertingRequestBeanToHex ");
            Log.i("Request Details\r\n----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n-                                         Request in Object format                                                                                                                                       -\r\n---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n" + this.reqMessage.toString() + "\r\n" + "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            Log.i("Exiting from convertRequestBeanToHex()");
        }

        return isoFormatedRequestMessage;
    }

    private String createAdjustmentFinancialAdviceRequest(AuthorizationRequestBean authorizationRequestBean) {
        if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
            Log.i("Entered in to createAdjustmentFinancialAdviceRequest() ");
        }

        if (authorizationRequestBean != null) {
            String strCurrencyCodeDesc;
            int var4;
            int var5;
            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getMessageTypeIdentifier())) {
                strCurrencyCodeDesc = null;
                MessageTypeID[] var6;
                var5 = (var6 = MessageTypeID.values()).length;

                for(var4 = 0; var4 < var5; ++var4) {
                    MessageTypeID messageIds = var6[var4];
                    if (authorizationRequestBean.getMessageTypeIdentifier().equalsIgnoreCase(messageIds.getMessageTypeID())) {
                        strCurrencyCodeDesc = messageIds.name();
                    }
                }

                this.reqMessage.append(" \tMESSAGE TYPE IDENTIFIER             \t" + authorizationRequestBean.getMessageTypeIdentifier());
                this.reqMessage.append("(" + strCurrencyCodeDesc + ")");
                this.reqMessage.append(NEW_LINE_CHAR);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getPrimaryAccountNumber().toString())) {
                this.reqMessage.append("2\tPRIMARY ACCOUNT NUMBER (PAN)         \t" + this.objUtility.performMasking(authorizationRequestBean.getPrimaryAccountNumber().toString(), false, true, 5, 4));
                this.convertToISOFormat(authorizationRequestBean.getPrimaryAccountNumber().toString(), "LL", true, 62, 19, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getProcessingCode())) {
                strCurrencyCodeDesc = null;
                ProcessingCode[] var12;
                var5 = (var12 = ProcessingCode.values()).length;

                for(var4 = 0; var4 < var5; ++var4) {
                    ProcessingCode processingCodes = var12[var4];
                    if (authorizationRequestBean.getProcessingCode().equalsIgnoreCase(processingCodes.getProcessingCode())) {
                        strCurrencyCodeDesc = processingCodes.name();
                    }
                }

                this.reqMessage.append("3\tPROCESSING CODE                      \t" + authorizationRequestBean.getProcessingCode());
                this.reqMessage.append("(" + strCurrencyCodeDesc + ")");
                this.convertToISOFormat(authorizationRequestBean.getProcessingCode(), (String)null, false, 61, 6, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAmountTransaction())) {
                if (!authorizationRequestBean.getProcessingCode().equalsIgnoreCase(ProcessingCode.TRANSACTION_FOR_AUTOMATED_ADDRESS_VERIFICATION_ONLY.getProcessingCode()) && !authorizationRequestBean.getFunctionCode().equalsIgnoreCase(FunctionCode.PREPAID_CARD_PARTIAL_AUTHORIZATION_SUPPORTED.getFunctionCode()) && !authorizationRequestBean.getFunctionCode().equalsIgnoreCase(FunctionCode.PREPAID_CARD_AUTHORIZATION_WITH_BALANCE_RETURN_SUPPORTED.getFunctionCode())) {
                    this.reqMessage.append("4\tAMOUNT, TRANSACTION                 \t" + authorizationRequestBean.getAmountTransaction());
                    this.convertToISOFormat(authorizationRequestBean.getAmountTransaction().toString(), (String)null, false, 60, 12, true, false, true, true);
                } else {
                    authorizationRequestBean.setAmountTransaction("000000000000");
                    this.reqMessage.append("4\tAMOUNT, TRANSACTION             \t" + authorizationRequestBean.getAmountTransaction());
                    this.convertToISOFormat(authorizationRequestBean.getAmountTransaction().toString(), (String)null, false, 60, 12, true, false, true, true);
                }
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getSystemTraceAuditNumber())) {
                this.reqMessage.append("11\tSYSTEMS TRACE AUDIT NUMBER           \t" + authorizationRequestBean.getSystemTraceAuditNumber().toUpperCase());
                this.convertToISOFormat(authorizationRequestBean.getSystemTraceAuditNumber().toUpperCase(), (String)null, false, 53, 6, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getLocalTransactionDateAndTime())) {
                this.reqMessage.append("12\tDATE AND TIME, LOCAL TRANSACTION      \t" + authorizationRequestBean.getLocalTransactionDateAndTime());
                this.convertToISOFormat(authorizationRequestBean.getLocalTransactionDateAndTime(), (String)null, false, 52, 12, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getMerchantLocationCountryCode())) {
                this.reqMessage.append("19\tCOUNTRY CODE, ACQUIRING INSTITUTION \t" + authorizationRequestBean.getMerchantLocationCountryCode());
                this.convertToISOFormat(authorizationRequestBean.getMerchantLocationCountryCode(), (String)null, false, 45, 3, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getPosDataCode())) {
                this.reqMessage.append("22\tPOINT OF SERVICE DATA CODE           \t" + authorizationRequestBean.getPosDataCode());
                this.convertToISOFormat(authorizationRequestBean.getPosDataCode(), (String)null, false, 42, 12, false, true, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getFunctionCode())) {
                strCurrencyCodeDesc = null;
                FunctionCode[] var13;
                var5 = (var13 = FunctionCode.values()).length;

                for(var4 = 0; var4 < var5; ++var4) {
                    FunctionCode functionCodes = var13[var4];
                    if (authorizationRequestBean.getFunctionCode().equalsIgnoreCase(functionCodes.getFunctionCode())) {
                        strCurrencyCodeDesc = functionCodes.name();
                    }
                }

                this.reqMessage.append("24\tFUNCTION CODE                        \t" + authorizationRequestBean.getFunctionCode());
                this.reqMessage.append("(" + strCurrencyCodeDesc + ")");
                this.convertToISOFormat(authorizationRequestBean.getFunctionCode(), (String)null, false, 40, 3, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getMessageReasonCode())) {
                this.reqMessage.append("25\tMESSAGE REASON CODE                   \t" + authorizationRequestBean.getMessageReasonCode());
                this.convertToISOFormat(authorizationRequestBean.getMessageReasonCode(), (String)null, false, 39, 4, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardAcceptorBusinessCode())) {
                this.reqMessage.append("26\tCARD ACCEPTOR BUSINESS CODE            \t" + authorizationRequestBean.getCardAcceptorBusinessCode());
                this.convertToISOFormat(authorizationRequestBean.getCardAcceptorBusinessCode(), (String)null, false, 38, 4, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAmountOriginal())) {
                this.reqMessage.append("30\tAMOUNTS, ORIGINAL                  \t" + authorizationRequestBean.getAmountOriginal());
                this.convertToISOFormat(authorizationRequestBean.getAmountOriginal(), (String)null, false, 34, 24, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getTransactionId())) {
                this.reqMessage.append("31\tTRANSACTION ID                         \t" + authorizationRequestBean.getTransactionId());
                this.convertToISOFormat(authorizationRequestBean.getTransactionId(), "LL", true, 33, 48, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAcquiringInstitutionIdCode())) {
                this.reqMessage.append("32\tACQUIRING INSTITUTION CODE              \t" + authorizationRequestBean.getAcquiringInstitutionIdCode());
                this.convertToISOFormat(authorizationRequestBean.getAcquiringInstitutionIdCode(), "LL", true, 32, 11, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getForwardingInstitutionIdCode())) {
                this.reqMessage.append("33\tFORWARDING INSTITUTION  CODE            \t" + authorizationRequestBean.getForwardingInstitutionIdCode());
                this.convertToISOFormat(authorizationRequestBean.getForwardingInstitutionIdCode(), "LL", true, 31, 11, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getRetrievalReferenceNumber())) {
                this.reqMessage.append("37\tRETRIEVAL REFFERNCE NUMBER               \t" + authorizationRequestBean.getRetrievalReferenceNumber());
                this.convertToISOFormat(authorizationRequestBean.getRetrievalReferenceNumber(), (String)null, false, 27, 12, false, true, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardAcceptorTerminalId())) {
                this.reqMessage.append("41\tCARD ACCEPTOR TERMINAL ID                 \t" + authorizationRequestBean.getCardAcceptorTerminalId());
                this.convertToISOFormat(authorizationRequestBean.getCardAcceptorTerminalId(), (String)null, false, 23, 8, false, true, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardAcceptorIdCode())) {
                this.reqMessage.append("42\tCARD ACCEPTOR ID CODE                    \t" + authorizationRequestBean.getCardAcceptorIdCode());
                this.convertToISOFormat(authorizationRequestBean.getCardAcceptorIdCode(), (String)null, false, 22, 15, false, true, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardAcceptorNameLocation())) {
                this.reqMessage.append("43\tCARD ACCEPTOR NAME LOCATION               \t" + authorizationRequestBean.getCardAcceptorNameLocation());
                this.convertToISOFormat(authorizationRequestBean.getCardAcceptorNameLocation(), "LL", true, 21, 99, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getTransactionCurrencyCode())) {
                strCurrencyCodeDesc = null;
                CurrencyCode[] var14;
                var5 = (var14 = CurrencyCode.values()).length;

                for(var4 = 0; var4 < var5; ++var4) {
                    CurrencyCode currencyCodes = var14[var4];
                    if (authorizationRequestBean.getTransactionCurrencyCode().equalsIgnoreCase(currencyCodes.getCurrencyCode())) {
                        strCurrencyCodeDesc = currencyCodes.name();
                    }
                }

                this.reqMessage.append("49\tTRANSACTION CURRENCY CODE            \t" + authorizationRequestBean.getTransactionCurrencyCode());
                this.reqMessage.append("(" + strCurrencyCodeDesc + ")");
                this.convertToISOFormat(authorizationRequestBean.getTransactionCurrencyCode(), (String)null, false, 15, 3, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getOriginalDataElements())) {
                this.reqMessage.append("56\tORIGINAL DATA ELEMENTS                \t" + authorizationRequestBean.getOriginalDataElements());
                this.convertToISOFormat(authorizationRequestBean.getOriginalDataElements(), "LL", true, 8, 37, false, false, true, true);
            }
        }

        StringBuffer hexMessageType = new StringBuffer();
        hexMessageType.append(DataConversion.convertASCIIToEBCDICString(authorizationRequestBean.getMessageTypeIdentifier()));
        hexMessageType.append(Long.toHexString(this.primary_header).toUpperCase());
        hexMessageType.append(this.requestParameter.toString());
        StringBuffer printHexMessage = new StringBuffer();
        printHexMessage.append(DataConversion.convertASCIIToEBCDICString(authorizationRequestBean.getMessageTypeIdentifier()));
        printHexMessage.append(Long.toHexString(this.primary_header).toUpperCase());
        printHexMessage.append(this.printRequest.toString());
        if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
            Log.i("Hex MessageType Request:");
            Log.i("Hex Message Request Details\r\n----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n-                                 Request in Hex format                                                                                                                                         -\r\n---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n" + ISOUtil.formatMessage(printHexMessage.toString(), 140) + "\r\n" + "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
        }

        this.printRequest = null;
        if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
            Log.i("Exiting from  createAdjustmentFinancialAdviceRequest() ");
        }

        return hexMessageType.toString();
    }

    public String getActionCodeDescription(String actionCode) {
        String actionCodeDescription = "";
        if (actionCode != null && !"".equals(actionCode)) {
            actionCode = actionCode.trim();
            ActionCode[] var6;
            int var5 = (var6 = ActionCode.values()).length;

            for(int var4 = 0; var4 < var5; ++var4) {
                ActionCode actionCodes = var6[var4];
                if (actionCode.equalsIgnoreCase(actionCodes.getActionCode())) {
                    actionCodeDescription = actionCodes.getActionCodeDescription();
                    break;
                }
            }
        }

        return actionCodeDescription;
    }

    public String getDigitalGatewayActionCodeDescription(String actionCode) {
        String actionCodeDescription = "";
        if (actionCode.equals("100")) {
            actionCodeDescription = "Acknowledgement";
        } else {
            actionCodeDescription = "System Unavailable";
        }

        return actionCodeDescription;
    }

    public String getAdditionalResponseDesc(String additionalResponseData) {
        StringBuffer additionalResponseDataDesc = new StringBuffer();
        if (additionalResponseData != null && !"".equals(additionalResponseData) && additionalResponseData.length() <= 2) {
            for(int index = 0; index < additionalResponseData.length(); ++index) {
                String addResponseData = String.valueOf(additionalResponseData.charAt(index));
                if (index == 1) {
                    addResponseData = " " + addResponseData;
                }

                AdditionalResponseData[] var8;
                int var7 = (var8 = AdditionalResponseData.values()).length;

                for(int var6 = 0; var6 < var7; ++var6) {
                    AdditionalResponseData additionalRespData = var8[var6];
                    if (addResponseData.equalsIgnoreCase(additionalRespData.getAdditionalResponseData())) {
                        additionalResponseDataDesc.append(additionalRespData.getAdditionalResponseDataDesc()).append(";");
                        break;
                    }
                }
            }
        }

        return additionalResponseDataDesc.toString();
    }

    private String createRequest(AuthorizationRequestBean authorizationRequestBean) throws IPSAPIException {
        if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
            Log.i("Entered in to createRequest() ");
        }

        if (authorizationRequestBean != null) {
            String strCurrencyCodeDesc;
            int var4;
            int var5;
            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getMessageTypeIdentifier())) {
                strCurrencyCodeDesc = null;
                MessageTypeID[] var6;
                var5 = (var6 = MessageTypeID.values()).length;

                for(var4 = 0; var4 < var5; ++var4) {
                    MessageTypeID messageIds = var6[var4];
                    if (authorizationRequestBean.getMessageTypeIdentifier().equalsIgnoreCase(messageIds.getMessageTypeID())) {
                        strCurrencyCodeDesc = messageIds.name();
                    }
                }

                this.reqMessage.append(" \tMESSAGE TYPE IDENTIFIER         \t" + authorizationRequestBean.getMessageTypeIdentifier());
                this.reqMessage.append("(" + strCurrencyCodeDesc + ")");
                this.reqMessage.append(NEW_LINE_CHAR);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getPrimaryAccountNumber().toString())) {
                this.reqMessage.append("2\tPRIMARY ACCOUNT NUMBER (PAN)         \t" + this.objUtility.performMasking(authorizationRequestBean.getPrimaryAccountNumber().toString(), false, true, 5, 4));
                this.convertToISOFormat(authorizationRequestBean.getPrimaryAccountNumber().toString(), "LL", true, 62, 19, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getProcessingCode())) {
                strCurrencyCodeDesc = null;
                ProcessingCode[] var14;
                var5 = (var14 = ProcessingCode.values()).length;

                for(var4 = 0; var4 < var5; ++var4) {
                    ProcessingCode processingCodes = var14[var4];
                    if (authorizationRequestBean.getProcessingCode().equalsIgnoreCase(processingCodes.getProcessingCode())) {
                        strCurrencyCodeDesc = processingCodes.name();
                    }
                }

                this.reqMessage.append("3\tPROCESSING CODE                      \t" + authorizationRequestBean.getProcessingCode());
                this.reqMessage.append("(" + strCurrencyCodeDesc + ")");
                this.convertToISOFormat(authorizationRequestBean.getProcessingCode(), (String)null, false, 61, 6, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAmountTransaction())) {
                if (authorizationRequestBean.getProcessingCode().equalsIgnoreCase(ProcessingCode.TRANSACTION_FOR_AUTOMATED_ADDRESS_VERIFICATION_ONLY.getProcessingCode())) {
                    authorizationRequestBean.setAmountTransaction("000000000000");
                    this.reqMessage.append("4\tAMOUNT, TRANSACTION             \t" + authorizationRequestBean.getAmountTransaction());
                    this.convertToISOFormat(authorizationRequestBean.getAmountTransaction(), (String)null, false, 60, 12, true, false, true, true);
                } else {
                    this.reqMessage.append("4\tAMOUNT, TRANSACTION                 \t" + authorizationRequestBean.getAmountTransaction());
                    this.convertToISOFormat(authorizationRequestBean.getAmountTransaction().toString(), (String)null, false, 60, 12, true, false, true, true);
                }
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getTransmissionDateAndTime())) {
                this.reqMessage.append("7\tDATE AND TIME, TRANSMISSION             \t" + authorizationRequestBean.getTransmissionDateAndTime());
                this.convertToISOFormat(authorizationRequestBean.getTransmissionDateAndTime(), (String)null, false, 57, 10, true, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getSystemTraceAuditNumber())) {
                this.reqMessage.append("11\tSYSTEMS TRACE AUDIT NUMBER             \t" + authorizationRequestBean.getSystemTraceAuditNumber().toUpperCase());
                this.convertToISOFormat(authorizationRequestBean.getSystemTraceAuditNumber().toUpperCase(), (String)null, false, 53, 6, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getLocalTransactionDateAndTime())) {
                this.reqMessage.append("12\tDATE AND TIME, LOCAL TRANSACTION       \t" + authorizationRequestBean.getLocalTransactionDateAndTime());
                this.convertToISOFormat(authorizationRequestBean.getLocalTransactionDateAndTime(), (String)null, false, 52, 12, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardEffectiveDate())) {
                this.reqMessage.append("13\tDATE, EFFECTIVE                        \t" + authorizationRequestBean.getCardEffectiveDate());
                this.convertToISOFormat(authorizationRequestBean.getCardEffectiveDate(), (String)null, false, 51, 4, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardExpirationDate())) {
                this.reqMessage.append("14\tDATE, EXPIRATION                       \t" + authorizationRequestBean.getCardExpirationDate());
                this.convertToISOFormat(authorizationRequestBean.getCardExpirationDate(), (String)null, false, 50, 4, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardSettlementDate())) {
                this.reqMessage.append("15\tDATE, SETTLEMENT                       \t" + authorizationRequestBean.getCardSettlementDate());
                this.convertToISOFormat(authorizationRequestBean.getCardSettlementDate(), (String)null, false, 49, 6, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getMerchantLocationCountryCode())) {
                this.reqMessage.append("19\tCOUNTRY CODE, ACQUIRING INSTITUTION    \t" + authorizationRequestBean.getMerchantLocationCountryCode());
                this.convertToISOFormat(authorizationRequestBean.getMerchantLocationCountryCode(), (String)null, false, 45, 3, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getPosDataCode())) {
                this.reqMessage.append("22\tPOINT OF SERVICE DATA CODE             \t" + authorizationRequestBean.getPosDataCode());
                this.convertToISOFormat(authorizationRequestBean.getPosDataCode(), (String)null, false, 42, 12, false, true, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getFunctionCode())) {
                strCurrencyCodeDesc = null;
                FunctionCode[] var16;
                var5 = (var16 = FunctionCode.values()).length;

                for(var4 = 0; var4 < var5; ++var4) {
                    FunctionCode functionCodes = var16[var4];
                    if (authorizationRequestBean.getFunctionCode().equalsIgnoreCase(functionCodes.getFunctionCode())) {
                        strCurrencyCodeDesc = functionCodes.name();
                    }
                }

                this.reqMessage.append("24\tFUNCTION CODE                          \t" + authorizationRequestBean.getFunctionCode());
                this.reqMessage.append("(" + strCurrencyCodeDesc + ")");
                this.convertToISOFormat(authorizationRequestBean.getFunctionCode(), (String)null, false, 40, 3, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getMessageReasonCode())) {
                this.reqMessage.append("25\tMESSAGE REASON CODE                    \t" + authorizationRequestBean.getMessageReasonCode());
                this.convertToISOFormat(authorizationRequestBean.getMessageReasonCode(), (String)null, false, 39, 4, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardAcceptorBusinessCode())) {
                this.reqMessage.append("26\tCARD ACCEPTOR BUSINESS CODE            \t" + authorizationRequestBean.getCardAcceptorBusinessCode());
                this.convertToISOFormat(authorizationRequestBean.getCardAcceptorBusinessCode(), (String)null, false, 38, 4, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getApprovalCodeLength())) {
                this.reqMessage.append("27\tAPPROVAL CODE LENGTH                   \t" + authorizationRequestBean.getApprovalCodeLength());
                this.convertToISOFormat(authorizationRequestBean.getApprovalCodeLength(), (String)null, false, 37, 1, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAmountOriginal())) {
                this.reqMessage.append("30\tAMOUNTS, ORIGINAL                      \t" + authorizationRequestBean.getAmountOriginal());
                this.convertToISOFormat(authorizationRequestBean.getAmountOriginal(), (String)null, false, 34, 24, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getTransactionId())) {
                this.reqMessage.append("31\tTRANSACTION ID                         \t" + authorizationRequestBean.getTransactionId());
                this.convertToISOFormat(authorizationRequestBean.getTransactionId(), "LL", true, 33, 48, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAcquiringInstitutionIdCode())) {
                this.reqMessage.append("32\tACQUIRING INSTITUTION CODE             \t" + authorizationRequestBean.getAcquiringInstitutionIdCode());
                this.convertToISOFormat(authorizationRequestBean.getAcquiringInstitutionIdCode(), "LL", true, 32, 11, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getForwardingInstitutionIdCode())) {
                this.reqMessage.append("33\tFORWARDING INSTITUTION  CODE           \t" + authorizationRequestBean.getForwardingInstitutionIdCode());
                this.convertToISOFormat(authorizationRequestBean.getForwardingInstitutionIdCode(), "LL", true, 31, 11, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getTrack2Data())) {
                this.reqMessage.append("35\tTRACK 2 DATA                           \t" + this.objUtility.performMasking(authorizationRequestBean.getTrack2Data(), true, false, 0, 0));
                this.convertToISOFormat(authorizationRequestBean.getTrack2Data(), "LL", true, 29, 37, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getRetrievalReferenceNumber())) {
                this.reqMessage.append("37\tRETRIEVAL REFFERNCE NUMBER             \t" + authorizationRequestBean.getRetrievalReferenceNumber());
                this.convertToISOFormat(authorizationRequestBean.getRetrievalReferenceNumber(), (String)null, false, 27, 12, false, true, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getApprovalCode())) {
                this.reqMessage.append("38\tAPPROVAL CODE                          \t" + authorizationRequestBean.getApprovalCode());
                this.convertToISOFormat(authorizationRequestBean.getApprovalCode(), (String)null, false, 26, 6, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardAcceptorTerminalId())) {
                this.reqMessage.append("41\tCARD ACCEPTOR TERMINAL ID              \t" + authorizationRequestBean.getCardAcceptorTerminalId());
                this.convertToISOFormat(authorizationRequestBean.getCardAcceptorTerminalId(), (String)null, false, 23, 8, false, true, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardAcceptorIdCode())) {
                this.reqMessage.append("42\tCARD ACCEPTOR ID CODE                  \t" + authorizationRequestBean.getCardAcceptorIdCode());
                this.convertToISOFormat(authorizationRequestBean.getCardAcceptorIdCode(), (String)null, false, 22, 15, false, true, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardAcceptorNameLocation())) {
                this.reqMessage.append("43\tCARD ACCEPTOR NAME LOCATION            \t" + authorizationRequestBean.getCardAcceptorNameLocation());
                this.convertToISOFormat(authorizationRequestBean.getCardAcceptorNameLocation(), "LL", true, 21, 99, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAdditionalResponseData())) {
                this.reqMessage.append("44\tADDITIONAL RESPONSE DATA               \t" + authorizationRequestBean.getAdditionalResponseData());
                this.convertToISOFormat(authorizationRequestBean.getAdditionalResponseData(), "LL", true, 20, 27, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getTrack1Data())) {
                this.reqMessage.append("45\tTRACK1 DATA                            \t" + this.objUtility.performMasking(authorizationRequestBean.getTrack1Data(), true, false, 0, 0));
                this.convertToISOFormat(authorizationRequestBean.getTrack1Data(), "LL", true, 19, 76, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAdditionalDataNational())) {
                this.reqMessage.append("47\tADDITIONAL DATA NATIONAL               \t" + authorizationRequestBean.getAdditionalDataNational());
                this.convertToISOFormat(authorizationRequestBean.getAdditionalDataNational(), "LLL", true, 17, 304, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAdditionalDataPrivate())) {
                this.reqMessage.append("48\tADDITIONAL DATA PRIVATE               \t" + authorizationRequestBean.getAdditionalDataPrivate());
                this.convertToISOFormat(authorizationRequestBean.getAdditionalDataPrivate(), "LLL", true, 16, 7, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getTransactionCurrencyCode())) {
                strCurrencyCodeDesc = null;
                CurrencyCode[] var17;
                var5 = (var17 = CurrencyCode.values()).length;

                for(var4 = 0; var4 < var5; ++var4) {
                    CurrencyCode currencyCodes = var17[var4];
                    if (authorizationRequestBean.getTransactionCurrencyCode().equalsIgnoreCase(currencyCodes.getCurrencyCode())) {
                        strCurrencyCodeDesc = currencyCodes.name();
                    }
                }

                this.reqMessage.append("49\tTRANSACTION CURRENCY CODE             \t" + authorizationRequestBean.getTransactionCurrencyCode());
                this.reqMessage.append("(" + strCurrencyCodeDesc + ")");
                this.convertToISOFormat(authorizationRequestBean.getTransactionCurrencyCode(), (String)null, false, 15, 3, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getPinData())) {
                this.reqMessage.append("52\tPIN DATA                              \t" + this.objUtility.performMasking(authorizationRequestBean.getPinData(), true, false, 0, 0));
                this.convertToISOFormat(authorizationRequestBean.getPinData(), (String)null, false, 12, 16, false, false, false, false);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardIdentifierCode()) && authorizationRequestBean.getSecurityRelatedControlInfoBean() == null) {
                this.reqMessage.append("53\tCARD IDENTIFIER CODE                  \t" + this.objUtility.performMasking(authorizationRequestBean.getCardIdentifierCode(), true, false, 0, 0));
                this.convertToISOFormat(authorizationRequestBean.getCardIdentifierCode(), "LL", true, 11, 29, false, false, true, true);
            }

            long bit;
            if (authorizationRequestBean.getSecurityRelatedControlInfoBean() != null) {
                this.reqMessage.append("53\t*CARD IDENTIFIER CODE                    \t" + authorizationRequestBean.getSecurityRelatedControlData());
                this.printRequest.append(authorizationRequestBean.getSecurityRelatedControlData());
                this.reqMessage.append("(" + authorizationRequestBean.getSecurityRelatedControlInfoBean() + ")" + NEW_LINE_CHAR);
                this.requestParameter.append(authorizationRequestBean.getSecurityRelatedControlData());
                bit = 1L;
                this.primary_header |= bit << 11;
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAmountsAdditional())) {
                this.reqMessage.append("54\tAMMOUNTS ADDITIONAL                   \t" + authorizationRequestBean.getAmountsAdditional());
                this.convertToISOFormat(authorizationRequestBean.getAmountsAdditional(), "LLL", true, 10, 123, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getIccRelatedData())) {
                this.reqMessage.append("55\tICC RELATED DATA                      \t" + authorizationRequestBean.getIccRelatedData());
                this.calculateIccLength = true;
                this.convertToISOFormat(authorizationRequestBean.getIccRelatedData(), "LLL", false, 9, 255, false, false, false, false);
                this.calculateIccLength = false;
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getOriginalDataElements())) {
                this.reqMessage.append("56\tORIGINAL DATA ELEMENTS                \t" + authorizationRequestBean.getOriginalDataElements());
                this.convertToISOFormat(authorizationRequestBean.getOriginalDataElements(), "LL", true, 8, 37, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getNationalUseData1())) {
                this.reqMessage.append("60\tNATIONAL USE DATA1                    \t" + authorizationRequestBean.getNationalUseData1());
                this.convertToISOFormat(authorizationRequestBean.getNationalUseData1(), (String)null, false, 4, 316, false, false, false, false);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getNationalUseData2())) {
                this.reqMessage.append("61\tNATIONAL USE DATA2                    \t" + authorizationRequestBean.getNationalUseData2());
                if ("TKN".equals(authorizationRequestBean.getNationalUseData2Bean().getSecondaryId())) {
                    this.printRequest.append(authorizationRequestBean.getNationalUseData2());
                    this.reqMessage.append("(" + authorizationRequestBean.getNationalUseData2() + ")" + NEW_LINE_CHAR);
                    this.requestParameter.append(authorizationRequestBean.getNationalUseData2());
                    bit = 1L;
                    this.primary_header |= bit << 3;
                } else {
                    this.convertToISOFormat(authorizationRequestBean.getNationalUseData2(), "LLL", false, 3, 103, false, false, false, false);
                }
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getValidationInformation())) {
                this.reqMessage.append("62\tVALIDATION INFO                      \t" + authorizationRequestBean.getValidationInformation());
                this.convertToISOFormat(authorizationRequestBean.getValidationInformation(), "LLL", true, 2, 63, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getVerificationInformation())) {
                this.reqMessage.append("63\tVERIFICATION INFO                     \t" + authorizationRequestBean.getVerificationInformation());
                this.convertToISOFormat(authorizationRequestBean.getVerificationInformation(), "LLL", true, 1, 208, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getKeyManagementData())) {
                bit = 1L;
                this.primary_header |= bit << 63;
                this.reqMessage.append("96\tKEY MANAGEMENT DATA                    \t" + authorizationRequestBean.getKeyManagementData());
                this.convertToISOFormat(authorizationRequestBean.getKeyManagementData(), "LLL", true, 96, 17, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getMessageAuthenticationCode())) {
                bit = 1L;
                this.primary_header |= bit;
                this.requestParameter.append(authorizationRequestBean.getMessageAuthenticationCode());
            }
        }

        StringBuffer hexMessageType = new StringBuffer();
        hexMessageType.append(DataConversion.convertASCIIToEBCDICString(authorizationRequestBean.getMessageTypeIdentifier()));
        hexMessageType.append(Long.toHexString(this.primary_header).toUpperCase());
        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getKeyManagementData())) {
            String lSecondayrBitMapHex = ISOUtil.padString(Long.toHexString(this.secondary_header).toUpperCase(), 16, "0", false, true);
            hexMessageType.append(lSecondayrBitMapHex);
        }

        hexMessageType.append(this.requestParameter.toString());
        StringBuffer printHexMessage = new StringBuffer();
        printHexMessage.append(DataConversion.convertASCIIToEBCDICString(authorizationRequestBean.getMessageTypeIdentifier()));
        printHexMessage.append(Long.toHexString(this.primary_header).toUpperCase());
        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getKeyManagementData())) {
            String lSecondayrBitMapHex = ISOUtil.padString(Long.toHexString(this.secondary_header).toUpperCase(), 16, "0", false, true);
            printHexMessage.append(lSecondayrBitMapHex);
        }

        printHexMessage.append(this.printRequest.toString());
        if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
            Log.i("HexMessageType Request:");
            Log.i("Hex Message Request Details\r\n----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n-                                 Request in Hex format                                                                                                                                         -\r\n---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n" + ISOUtil.formatMessage(printHexMessage.toString(), 140) + "\r\n" + "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            Log.i("Exiting from createRequest() ");
        }

        this.printRequest = null;
        return hexMessageType.toString();
    }

    private String createDigitalGatewayRequest(AuthorizationRequestBean authorizationRequestBean) throws IPSAPIException {
        if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
            Log.i("Entered in to createRequest() ");
        }

        if (authorizationRequestBean != null) {
            String strCurrencyCodeDesc;
            int var4;
            int var5;
            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getMessageTypeIdentifier())) {
                strCurrencyCodeDesc = null;
                MessageTypeID[] var6;
                var5 = (var6 = MessageTypeID.values()).length;

                for(var4 = 0; var4 < var5; ++var4) {
                    MessageTypeID messageIds = var6[var4];
                    if (authorizationRequestBean.getMessageTypeIdentifier().equalsIgnoreCase(messageIds.getMessageTypeID())) {
                        strCurrencyCodeDesc = messageIds.name();
                    }
                }

                this.reqMessage.append(" \tMESSAGE TYPE IDENTIFIER         \t" + authorizationRequestBean.getMessageTypeIdentifier());
                this.reqMessage.append("(" + strCurrencyCodeDesc + ")");
                this.reqMessage.append(NEW_LINE_CHAR);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getPrimaryAccountNumber().toString())) {
                this.reqMessage.append("2\tPRIMARY ACCOUNT NUMBER (PAN)         \t" + this.objUtility.performMasking(authorizationRequestBean.getPrimaryAccountNumber().toString(), false, true, 5, 4));
                this.convertToISOFormat(authorizationRequestBean.getPrimaryAccountNumber().toString(), "LL", true, 62, 19, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getProcessingCode())) {
                strCurrencyCodeDesc = null;
                ProcessingCode[] var11;
                var5 = (var11 = ProcessingCode.values()).length;

                for(var4 = 0; var4 < var5; ++var4) {
                    ProcessingCode processingCodes = var11[var4];
                    if (authorizationRequestBean.getProcessingCode().equalsIgnoreCase(processingCodes.getProcessingCode())) {
                        strCurrencyCodeDesc = processingCodes.name();
                    }
                }

                this.reqMessage.append("3\tPROCESSING CODE                      \t" + authorizationRequestBean.getProcessingCode());
                this.reqMessage.append("(" + strCurrencyCodeDesc + ")");
                this.convertToISOFormat(authorizationRequestBean.getProcessingCode(), (String)null, false, 61, 6, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAmountTransaction())) {
                this.reqMessage.append("4\tAMOUNT, TRANSACTION                 \t" + authorizationRequestBean.getAmountTransaction());
                this.convertToISOFormat(authorizationRequestBean.getAmountTransaction().toString(), (String)null, false, 60, 12, true, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getSystemTraceAuditNumber())) {
                this.reqMessage.append("11\tSYSTEMS TRACE AUDIT NUMBER             \t" + authorizationRequestBean.getSystemTraceAuditNumber().toUpperCase());
                this.convertToISOFormat(authorizationRequestBean.getSystemTraceAuditNumber().toUpperCase(), (String)null, false, 53, 6, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getLocalTransactionDateAndTime())) {
                this.reqMessage.append("12\tDATE AND TIME, LOCAL TRANSACTION       \t" + authorizationRequestBean.getLocalTransactionDateAndTime());
                this.convertToISOFormat(authorizationRequestBean.getLocalTransactionDateAndTime(), (String)null, false, 52, 12, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getMerchantLocationCountryCode())) {
                this.reqMessage.append("19\tCOUNTRY CODE, ACQUIRING INSTITUTION    \t" + authorizationRequestBean.getMerchantLocationCountryCode());
                this.convertToISOFormat(authorizationRequestBean.getMerchantLocationCountryCode(), (String)null, false, 45, 3, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getPosDataCode())) {
                this.reqMessage.append("22\tPOINT OF SERVICE DATA CODE             \t" + authorizationRequestBean.getPosDataCode());
                this.convertToISOFormat(authorizationRequestBean.getPosDataCode(), (String)null, false, 42, 12, false, true, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getMessageReasonCode())) {
                this.reqMessage.append("25\tMESSAGE REASON CODE                    \t" + authorizationRequestBean.getMessageReasonCode());
                this.convertToISOFormat(authorizationRequestBean.getMessageReasonCode(), (String)null, false, 39, 4, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardAcceptorBusinessCode())) {
                this.reqMessage.append("26\tCARD ACCEPTOR BUSINESS CODE            \t" + authorizationRequestBean.getCardAcceptorBusinessCode());
                this.convertToISOFormat(authorizationRequestBean.getCardAcceptorBusinessCode(), (String)null, false, 38, 4, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAcquiringInstitutionIdCode())) {
                this.reqMessage.append("32\tACQUIRING INSTITUTION CODE             \t" + authorizationRequestBean.getAcquiringInstitutionIdCode());
                this.convertToISOFormat(authorizationRequestBean.getAcquiringInstitutionIdCode(), "LL", true, 32, 11, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getRetrievalReferenceNumber())) {
                this.reqMessage.append("37\tRETRIEVAL REFFERNCE NUMBER             \t" + authorizationRequestBean.getRetrievalReferenceNumber());
                this.convertToISOFormat(authorizationRequestBean.getRetrievalReferenceNumber(), (String)null, false, 27, 12, false, true, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardAcceptorIdCode())) {
                this.reqMessage.append("42\tCARD ACCEPTOR ID CODE                  \t" + authorizationRequestBean.getCardAcceptorIdCode());
                this.convertToISOFormat(authorizationRequestBean.getCardAcceptorIdCode(), (String)null, false, 22, 15, false, true, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAdditionalResponseData())) {
                this.reqMessage.append("44\tADDITIONAL RESPONSE DATA               \t" + authorizationRequestBean.getAdditionalResponseData());
                this.convertToISOFormat(authorizationRequestBean.getAdditionalResponseData(), "LL", true, 20, 27, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAdditionalDataNational())) {
                this.reqMessage.append("47\tADDITIONAL DATA NATIONAL               \t" + authorizationRequestBean.getAdditionalDataNational());
                this.convertToISOFormat(authorizationRequestBean.getAdditionalDataNational(), "LLL", true, 17, 290, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getTransactionCurrencyCode())) {
                strCurrencyCodeDesc = null;
                CurrencyCode[] var12;
                var5 = (var12 = CurrencyCode.values()).length;

                for(var4 = 0; var4 < var5; ++var4) {
                    CurrencyCode currencyCodes = var12[var4];
                    if (authorizationRequestBean.getTransactionCurrencyCode().equalsIgnoreCase(currencyCodes.getCurrencyCode())) {
                        strCurrencyCodeDesc = currencyCodes.name();
                    }
                }

                this.reqMessage.append("49\tTRANSACTION CURRENCY CODE             \t" + authorizationRequestBean.getTransactionCurrencyCode());
                this.reqMessage.append("(" + strCurrencyCodeDesc + ")");
                this.convertToISOFormat(authorizationRequestBean.getTransactionCurrencyCode(), (String)null, false, 15, 3, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getVerificationInformation())) {
                this.reqMessage.append("63\tVERIFICATION INFO                     \t" + authorizationRequestBean.getVerificationInformation());
                this.convertToISOFormat(authorizationRequestBean.getVerificationInformation(), "LLL", true, 1, 208, false, true, true, true);
            }
        }

        StringBuffer hexMessageType = new StringBuffer();
        hexMessageType.append(DataConversion.convertASCIIToEBCDICString(authorizationRequestBean.getMessageTypeIdentifier()));
        hexMessageType.append(Long.toHexString(this.primary_header).toUpperCase());
        hexMessageType.append(this.requestParameter.toString());
        StringBuffer printHexMessage = new StringBuffer();
        printHexMessage.append(DataConversion.convertASCIIToEBCDICString(authorizationRequestBean.getMessageTypeIdentifier()));
        printHexMessage.append(Long.toHexString(this.primary_header).toUpperCase());
        printHexMessage.append(this.printRequest.toString());
        if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
            Log.i("HexMessageType Request:");
            Log.i("Hex Message Request Details\r\n----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n-                                 Request in Hex format                                                                                                                                         -\r\n---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n" + ISOUtil.formatMessage(printHexMessage.toString(), 140) + "\r\n" + "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            Log.i("Exiting from createRequest() ");
        }

        this.printRequest = null;
        return hexMessageType.toString();
    }

    public void convertToISOFormat(String fieldValue, String varType, boolean calculateLength, int bitNumber, int maxLength, boolean isRightJustify, boolean isLeftJustify, boolean encodeLength, boolean encodeValue) {
        if (this.debugflag) {
            Log.i("Entered into convertToISOFormat() to convert value for bit number :: " + (64 - bitNumber));
        }

        int length = fieldValue.length();
        if (bitNumber == 96) {
            length = (fieldValue.length() - 5) / 2 + 5;
        }

        String len = "";
        long bit = 1L;
        String printValue = "";
        if (bitNumber <= 64) {
            this.primary_header |= bit << bitNumber;
        } else {
            this.secondary_header |= bit << 128 - bitNumber;
        }

        if (calculateLength) {
            len = this.calculateLength(varType, length);
            if (encodeLength) {
                len = this.convertTo(len, "ENC_HEX");
            }

            this.printRequest.append(len);
            this.requestParameter.append(len);
        }

        if (this.calculateIccLength) {
            StringBuffer modifiedValue = new StringBuffer();
            String headerVersionName = fieldValue.substring(0, 4);
            String data = fieldValue.substring(4);
            if (headerVersionName.equalsIgnoreCase("AGNS")) {
                headerVersionName = this.convertTo(headerVersionName, "ENC_HEX");
                length = data.length() / 2 + 4;
            } else {
                length = fieldValue.length() / 2;
            }

            len = this.getRightJustifyString(Integer.toString(length), 3);
            len = this.convertTo(len, "ENC_HEX");
            modifiedValue.append(headerVersionName);
            modifiedValue.append(data);
            this.requestParameter.append(len);
            this.printRequest.append(len);
            fieldValue = modifiedValue.toString();
        }

        if (isRightJustify) {
            fieldValue = this.getRightJustifyString(fieldValue, maxLength);
        }

        if (isLeftJustify) {
            fieldValue = this.getLeftJustifyString(fieldValue, maxLength);
        }

        if (encodeValue) {
            if (bitNumber == 96) {
                fieldValue = this.convertTo(fieldValue.substring(0, 5), "ENC_HEX") + fieldValue.substring(5);
            } else {
                fieldValue = this.convertTo(fieldValue, "ENC_HEX");
            }
        }

        if (bitNumber == 62) {
            printValue = this.objUtility.performHEXMasking(fieldValue, false, true, 10, 8);
            this.printRequest.append(printValue);
            this.reqMessage.append("(" + printValue + ")" + NEW_LINE_CHAR);
        } else if (bitNumber != 19 && bitNumber != 29 && bitNumber != 12 && bitNumber != 11) {
            if (bitNumber == 3) {
                fieldValue = this.convertNationalUseData(fieldValue);
                String modifiedLength = this.getRightJustifyString(Integer.toString(this.nationalUseDatalength), 3);
                modifiedLength = this.convertTo(modifiedLength, "ENC_HEX");
                this.requestParameter.append(modifiedLength);
                this.printRequest.append(modifiedLength);
                this.reqMessage.append("(" + fieldValue + ")" + NEW_LINE_CHAR);
            } else {
                this.printRequest.append(fieldValue);
                this.reqMessage.append("(" + fieldValue + ")" + NEW_LINE_CHAR);
            }
        } else {
            printValue = this.objUtility.performHEXMasking(fieldValue, true, false, 0, 0);
            this.printRequest.append(printValue);
            this.reqMessage.append("(" + printValue + ")" + NEW_LINE_CHAR);
        }

        this.requestParameter.append(fieldValue);
    }

    public String calculateLength(String varType, int length) {
        String len = "";
        if (varType.equalsIgnoreCase("LL")) {
            if (length <= 9) {
                len = "0" + length;
                len = len.trim();
            } else {
                len = Integer.toString(length);
            }
        }

        if (varType.equalsIgnoreCase("LLL")) {
            if (length >= 10 && length < 100) {
                len = "0" + length;
                len = len.trim();
            } else if (length <= 9) {
                len = "00" + length;
                len = len.trim();
            } else {
                len = Integer.toString(length);
            }
        }

        return len;
    }

    public AuthorizationResponseBean convertHexToResponseBean(String serviceResponseString) throws IPSAPIException {
        AuthorizationResponseBean authorizationResponseBean = null;
        this.respMessage.append("BitNum   Field Name                             \t\tValue" + NEW_LINE_CHAR);
        if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
            Log.i("Entered into convertHexToResponseBean()");
        }

        try {
            if (serviceResponseString != null && serviceResponseString.length() > 0) {
                authorizationResponseBean = new AuthorizationResponseBean();
                authorizationResponseBean = this.parseAuthorizationResponse(true, serviceResponseString, authorizationResponseBean);
            }
        } catch (Exception var4) {
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Error in convertHexToResponseBean() method..  Exception: " + var4);
            }

            throw new IPSAPIException("Error occured when convert response message", var4);
        }

        if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
            Log.i("Exiting from convertHexToResponseBean()");
        }

        return authorizationResponseBean;
    }

    private AuthorizationResponseBean parseAuthorizationResponse(boolean withNull, String trace, AuthorizationResponseBean authorizationResponseBean) throws IPSAPIException {
        StringBuffer printResponse = new StringBuffer();
        String printValue = "";
        if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
            Log.i("Entered into parseAuthorizationResponse");
        }

        StringReader str = new StringReader(trace);
        StringBuffer header = new StringBuffer();
        int noOfChar = 2 * this.getHeaderLength();

        int c;
        int i;
        try {
            for(i = 0; i < noOfChar; ++i) {
                c = str.read();
                header.append((char)c);
            }
        } catch (IOException var37) {
            this.errorMessage = "Error occured when reading the Header";
            if (this.debugflag) {
                Log.e(this.errorMessage, var37);
            }

            this.invalidTrace = true;
            return null;
        }

        this.messageType = new StringBuffer();
        noOfChar = 8;
        c = -1;

        try {
            i = 0;

            while(true) {
                if (i >= noOfChar) {
                    printResponse.append(this.messageType.toString());
                    authorizationResponseBean.setMessageTypeIdentifier(DataConversion.convertStringToASCII(this.messageType.toString()));
                    this.respMessage.append(" \tMESSAGE TYPE IDENTIFIER                  \t" + this.messageType.toString() + "(" + DataConversion.convertStringToASCII(this.messageType.toString()) + ")" + NEW_LINE_CHAR);
                    break;
                }

                c = str.read();
                this.messageType.append((char)c);
                ++i;
            }
        } catch (IOException var36) {
            this.errorMessage = "Error occured when reading the Message Type";
            if (this.debugflag) {
                Log.e(this.errorMessage, var36);
            }

            this.invalidTrace = true;
            return null;
        }

        if (c == -1) {
            this.errorMessage = "Error occured when reading the Message Type ( EOF reached )";
            if (this.debugflag) {
                Log.e(this.errorMessage);
            }

            this.invalidTrace = true;
            return null;
        } else {
            noOfChar = 16;
            char[] bitMap = new char[noOfChar];

            int ret;
            try {
                ret = str.read(bitMap, 0, noOfChar);
            } catch (IOException var33) {
                this.errorMessage = "Error occured when reading the bitmap string";
                if (this.debugflag) {
                    Log.e(this.errorMessage, var33);
                }

                this.invalidTrace = true;
                return null;
            }

            if (ret == -1) {
                this.errorMessage = "Error occured when reading the bitmap string ( EOF reached )";
                if (this.debugflag) {
                    Log.e(this.errorMessage);
                }

                this.invalidTrace = true;
                return null;
            } else {
                int secBitMap;
                for(secBitMap = 0; secBitMap < bitMap.length; ++secBitMap) {
                    int c1 = bitMap[secBitMap];
                    printResponse.append((char)c1);
                }

                this.presentFieldList = new LinkedList();
                secBitMap = this.createBitmaps(str, bitMap, noOfChar);
                if (secBitMap == -1) {
                    this.invalidTrace = true;
                    return null;
                } else {
                    char[] value = new char[1];
                    String length = "";
                    boolean on = false;

                    for(i = 0; i < 110; ++i) {
                        on = false;
                        long bit;
                        if (i < 64) {
                            if (secBitMap == 2 && i == 0) {
                                ++i;
                            }

                            bit = 1L << 63 - i;
                            if ((bit & this.primaryBitMap) != 0L) {
                                on = true;
                            }
                        } else if (secBitMap == 2) {
                            bit = 1L << 127 - i;
                            if ((bit & this.secondaryBitMap) != 0L) {
                                on = true;
                            }
                        }

                        FieldBean f;
                        String nationalUsedata;
                        IntegratedCircuitCardRelatedDataBean iccBean;
                        if (on) {
                            char[] len;
                            String dLen;
                            int dataLen;
                            switch(FIELD_LEN_TYPE[i]) {
                            case 1:
                                noOfChar = MAX_FIELD_LEN[i] * 2;
                                value = new char[noOfChar];

                                try {
                                    ret = str.read(value, 0, noOfChar);
                                } catch (IOException var30) {
                                    this.errorMessage = "Error occured when reading the " + (i + 1) + " string";
                                    if (this.debugflag) {
                                        Log.e(this.errorMessage, var30);
                                    }

                                    this.invalidTrace = true;
                                    return null;
                                }

                                if (ret == -1) {
                                    this.errorMessage = "Error occured when reading the " + (i + 1) + " string ( EOF reached )";
                                    if (this.debugflag) {
                                        Log.e(this.errorMessage);
                                    }

                                    this.invalidTrace = true;
                                    return null;
                                }

                                length = "";
                                break;
                            case 2:
                                len = new char[4];

                                try {
                                    ret = str.read(len, 0, 4);
                                } catch (IOException var28) {
                                    this.errorMessage = "Error occured when reading the " + (i + 1) + " string's LL value";
                                    if (this.debugflag) {
                                        Log.e(this.errorMessage, var28);
                                    }

                                    this.invalidTrace = true;
                                    return null;
                                }

                                if (ret == -1) {
                                    this.errorMessage = "Error occured when reading the " + (i + 1) + " string's LL value ( EOF reached )";
                                    if (this.debugflag) {
                                        Log.e(this.errorMessage);
                                    }

                                    this.invalidTrace = true;
                                    return null;
                                }

                                length = new String(len);
                                dLen = new String(DataConversion.convertStringToASCII(length));

                                try {
                                    dataLen = Integer.parseInt(dLen);
                                } catch (NumberFormatException var35) {
                                    this.errorMessage = "Problem calculating length of LLVAR: " + FIELD_NAME[i];
                                    if (this.debugflag) {
                                        Log.e(this.errorMessage, var35);
                                    }

                                    this.invalidTrace = true;
                                    return null;
                                }

                                if (dataLen > MAX_FIELD_LEN[i]) {
                                    this.errorMessage = "The length of " + (i + 1) + "th field " + FIELD_NAME[i] + "is more that allowed";
                                    if (this.debugflag) {
                                        Log.e(this.errorMessage);
                                    }

                                    this.invalidTrace = true;
                                    return null;
                                }

                                noOfChar = dataLen * 2;
                                value = new char[noOfChar];

                                try {
                                    ret = str.read(value, 0, noOfChar);
                                } catch (IOException var31) {
                                    this.errorMessage = "Error occured when reading the " + (i + 1) + " string";
                                    if (this.debugflag) {
                                        Log.e(this.errorMessage, var31);
                                    }

                                    this.invalidTrace = true;
                                    return null;
                                }

                                if (ret == -1) {
                                    this.errorMessage = "Error occured when reading the " + (i + 1) + " string ( EOF reached )";
                                    if (this.debugflag) {
                                        Log.e(this.errorMessage);
                                    }

                                    this.invalidTrace = true;
                                    return null;
                                }
                                break;
                            case 3:
                                len = new char[6];

                                try {
                                    ret = str.read(len, 0, 6);
                                } catch (IOException var32) {
                                    this.errorMessage = "Error occured when reading the " + (i + 1) + " string's LLL value";
                                    if (this.debugflag) {
                                        Log.e(this.errorMessage, var32);
                                    }

                                    this.invalidTrace = true;
                                    return null;
                                }

                                if (ret == -1) {
                                    this.errorMessage = "Error occured when reading the " + (i + 1) + " string's LLL value ( EOF reached )";
                                    if (this.debugflag) {
                                        Log.e(this.errorMessage);
                                    }

                                    this.invalidTrace = true;
                                    return null;
                                }

                                length = new String(len);
                                dLen = new String(DataConversion.convertStringToASCII(length));

                                try {
                                    dataLen = Integer.parseInt(dLen);
                                } catch (NumberFormatException var29) {
                                    this.errorMessage = "Problem calculating length of LLLVAR: " + FIELD_NAME[i];
                                    if (this.debugflag) {
                                        Log.e(this.errorMessage, var29);
                                    }

                                    this.invalidTrace = true;
                                    return null;
                                }

                                noOfChar = dataLen * 2;
                                value = new char[noOfChar];

                                try {
                                    ret = str.read(value, 0, noOfChar);
                                } catch (IOException var34) {
                                    this.errorMessage = "Error occured when reading the " + (i + 1) + " string";
                                    if (this.debugflag) {
                                        Log.e(this.errorMessage, var34);
                                    }

                                    this.invalidTrace = true;
                                    return null;
                                }

                                if (ret == -1) {
                                    this.errorMessage = "Error occured when reading the " + (i + 1) + " string ( EOF reached )";
                                    if (this.debugflag) {
                                        Log.e(this.errorMessage);
                                    }

                                    this.invalidTrace = true;
                                    return null;
                                }
                            }

                            f = new FieldBean(i, FIELD_NAME[i], MAX_FIELD_LEN[i], FIELD_LEN_TYPE[i], 2, length, new String(value));
                            if (f.getBitNo() + 1 == 2) {
                                printValue = this.objUtility.performMasking(f.getValue(), false, true, 10, 8);
                                printResponse.append(f.getLength()).append(printValue);
                            } else if (f.getBitNo() + 1 != 35 && f.getBitNo() + 1 != 45 && f.getBitNo() + 1 != 52 && f.getBitNo() + 1 != 53) {
                                printResponse.append(f.getLength()).append(f.getValue());
                            } else {
                                printValue = this.objUtility.performMasking(f.getValue(), true, false, 0, 0);
                                printResponse.append(f.getLength()).append(printValue);
                            }

                            if (f.getName().equals("PRIMARY ACCOUNT NUMBER (PAN)")) {
                                authorizationResponseBean.setPrimaryAccountNumber(new StringBuffer(f.getAsciiValue()));
                                this.respMessage.append(f.getBitNo() + 1 + "\t" + "PRIMARY ACCOUNT NUMBER (PAN)                  " + "\t" + this.objUtility.performHEXMasking(f.getValue(), false, true, 10, 8) + "(" + this.objUtility.performMasking(f.getAsciiValue(), false, true, 5, 4) + ")" + NEW_LINE_CHAR);
                            } else {
                                int var25;
                                int var26;
                                if (f.getName().equals("PROCESSING CODE")) {
                                    authorizationResponseBean.setProcessingCode(f.getAsciiValue());
                                    nationalUsedata = null;
                                    ProcessingCode[] var27;
                                    var26 = (var27 = ProcessingCode.values()).length;

                                    for(var25 = 0; var25 < var26; ++var25) {
                                        ProcessingCode processingCodes = var27[var25];
                                        if (f.getAsciiValue().equalsIgnoreCase(processingCodes.getProcessingCode())) {
                                            nationalUsedata = processingCodes.name();
                                        }
                                    }

                                    this.respMessage.append(f.getBitNo() + 1 + "\t" + "PROCESSING CODE                               " + "\t" + f.getValue() + "(" + nationalUsedata + ")" + "(" + f.getAsciiValue() + ")" + NEW_LINE_CHAR);
                                } else if (f.getName().equals("AMOUNT, TRANSACTION")) {
                                    authorizationResponseBean.setAmountTransaction(f.getAsciiValue());
                                    this.respMessage.append(f.getBitNo() + 1 + "\t" + "AMOUNT, TRANSACTION                           " + "\t" + f.getValue() + "(" + f.getAsciiValue() + ")" + NEW_LINE_CHAR);
                                } else if (f.getName().equals("DATE AND TIME, TRANSMISSION")) {
                                    authorizationResponseBean.setTransmissionDateAndTime(f.getAsciiValue());
                                    this.respMessage.append(f.getBitNo() + 1 + "\t" + "DATE AND TIME, TRANSMISSION                   " + "\t" + f.getValue() + "(" + f.getAsciiValue() + ")" + NEW_LINE_CHAR);
                                } else if (f.getName().equals("SYSTEMS TRACE AUDIT NUMBER")) {
                                    authorizationResponseBean.setSystemTraceAuditNumber(f.getAsciiValue());
                                    this.respMessage.append(f.getBitNo() + 1 + "\t" + "SYSTEMS TRACE AUDIT NUMBER                    " + "\t" + f.getValue() + "(" + f.getAsciiValue() + ")" + NEW_LINE_CHAR);
                                } else if (f.getName().equals("DATE AND TIME, LOCAL TRANSACTION")) {
                                    authorizationResponseBean.setLocalTransactionDateAndTime(f.getAsciiValue());
                                    this.respMessage.append(f.getBitNo() + 1 + "\t" + "DATE AND TIME, LOCAL TRANSACTION              " + "\t" + f.getValue() + "(" + f.getAsciiValue() + ")" + NEW_LINE_CHAR);
                                } else if (f.getName().equals("DATE, SETTLEMENT")) {
                                    authorizationResponseBean.setCardSettlementDate(f.getAsciiValue());
                                    this.respMessage.append(f.getBitNo() + 1 + "\t" + "DATE, SETTLEMENT                              " + "\t" + f.getValue() + "(" + f.getAsciiValue() + ")" + NEW_LINE_CHAR);
                                } else if (f.getName().equals("AMOUNTS, ORIGINAL")) {
                                    authorizationResponseBean.setAmountOriginal(f.getAsciiValue());
                                    this.respMessage.append(f.getBitNo() + 1 + "\t" + "AMOUNTS, ORIGINAL                             " + "\t" + f.getValue() + "(" + f.getAsciiValue() + ")" + NEW_LINE_CHAR);
                                } else if (f.getName().equals("ACQUIRER REFERENCE DATA")) {
                                    authorizationResponseBean.setTransactionId(f.getAsciiValue());
                                    this.respMessage.append(f.getBitNo() + 1 + "\t" + "ACQUIRER REFERENCE DATA                       " + "\t" + f.getValue() + "(" + f.getAsciiValue() + ")" + NEW_LINE_CHAR);
                                } else if (f.getName().equals("ACQUIRING INSTITUTION IDENTIFICATION CODE")) {
                                    authorizationResponseBean.setAcquiringInstitutionIdCode(f.getAsciiValue());
                                    this.respMessage.append(f.getBitNo() + 1 + "\t" + "ACQUIRING INSTITUTION IDENTIFICATION CODE     " + "\t" + f.getValue() + "(" + f.getAsciiValue() + ")" + NEW_LINE_CHAR);
                                } else if (f.getName().equals("PRIMARY ACCOUNT NUMBER, EXTENDED")) {
                                    authorizationResponseBean.setPrimaryAccountNumberExtended(f.getAsciiValue());
                                    if ("Y".equals(f.getAsciiValue().charAt(0))) {
                                        this.respMessage.append(f.getBitNo() + 1 + "\t" + "PRIMARY ACCOUNT NUMBER, EXTENDED   \t\t\t  " + "\t" + this.objUtility.performHEXMasking(f.getValue(), false, true, 10, 8) + "(" + this.objUtility.performMasking(f.getAsciiValue(), false, true, 5, 4) + ")" + NEW_LINE_CHAR);
                                    } else {
                                        this.respMessage.append(f.getBitNo() + 1 + "\t" + "PRIMARY ACCOUNT NUMBER, EXTENDED   \t\t\t  " + "\t" + f.getValue() + "(" + f.getAsciiValue() + ")" + NEW_LINE_CHAR);
                                    }
                                } else if (f.getName().equals("RETRIEVAL REFERENCE NUMBER")) {
                                    authorizationResponseBean.setRetrievalReferenceNumber(f.getAsciiValue());
                                    this.respMessage.append(f.getBitNo() + 1 + "\t" + "RETRIEVAL REFERENCE NUMBER                    " + "\t" + f.getValue() + "(" + f.getAsciiValue() + ")" + NEW_LINE_CHAR);
                                } else if (f.getName().equals("APPROVAL CODE")) {
                                    authorizationResponseBean.setApprovalCode(f.getAsciiValue());
                                    this.respMessage.append(f.getBitNo() + 1 + "\t" + "APPROVAL CODE                                 " + "\t" + f.getValue() + "(" + f.getAsciiValue() + ")" + NEW_LINE_CHAR);
                                } else if (f.getName().equals("ACTION CODE")) {
                                    authorizationResponseBean.setActionCode(f.getAsciiValue());
                                    nationalUsedata = null;
                                    ActionCode[] var45;
                                    var26 = (var45 = ActionCode.values()).length;

                                    for(var25 = 0; var25 < var26; ++var25) {
                                        ActionCode actionCodes = var45[var25];
                                        if (f.getAsciiValue().equalsIgnoreCase(actionCodes.getActionCode())) {
                                            nationalUsedata = actionCodes.name();
                                        }
                                    }

                                    if (PropertyReader.getRequestType().equals("DigGatewayReq")) {
                                        if ("100".equals(f.getAsciiValue())) {
                                            this.respMessage.append(f.getBitNo() + 1 + "\t" + "ACTION CODE                                   " + "\t" + f.getValue() + "(" + "Acknowledgement".toUpperCase() + ")" + "(" + f.getAsciiValue() + ")" + NEW_LINE_CHAR);
                                        } else {
                                            this.respMessage.append(f.getBitNo() + 1 + "\t" + "ACTION CODE                                   " + "\t" + f.getValue() + "(" + "System Unavailable".toUpperCase() + ")" + "(" + f.getAsciiValue() + ")" + NEW_LINE_CHAR);
                                        }
                                    } else {
                                        this.respMessage.append(f.getBitNo() + 1 + "\t" + "ACTION CODE                                   " + "\t" + f.getValue() + "(" + nationalUsedata + ")" + "(" + f.getAsciiValue() + ")" + NEW_LINE_CHAR);
                                    }
                                } else if (f.getName().equals("CARD ACCEPTOR TERMINAL IDENTIFICATION")) {
                                    authorizationResponseBean.setCardAcceptorTerminalId(f.getAsciiValue());
                                    this.respMessage.append(f.getBitNo() + 1 + "\t" + "CARD ACCEPTOR TERMINAL IDENTIFICATION         " + "\t" + f.getValue() + "(" + f.getAsciiValue() + ")" + NEW_LINE_CHAR);
                                } else if (f.getName().equals("CARD ACCEPTOR IDENTIFICATION CODE")) {
                                    authorizationResponseBean.setCardAcceptorIdCode(f.getAsciiValue());
                                    this.respMessage.append(f.getBitNo() + 1 + "\t" + "CARD ACCEPTOR IDENTIFICATION CODE             " + "\t" + f.getValue() + "(" + f.getAsciiValue() + ")" + NEW_LINE_CHAR);
                                } else if (f.getName().equals("CARD ACCEPTOR NAME/LOCATION")) {
                                    authorizationResponseBean.setCardAcceptorNameLocation(f.getAsciiValue());
                                    this.respMessage.append(f.getBitNo() + 1 + "\t" + "CARD ACCEPTOR NAME/LOCATION                   " + "\t" + f.getValue() + "(" + f.getAsciiValue() + ")" + NEW_LINE_CHAR);
                                } else if (f.getName().equals("ADDITIONAL RESPONSE DATA")) {
                                    authorizationResponseBean.setAdditionalResponseData(f.getAsciiValue());
                                    this.respMessage.append(f.getBitNo() + 1 + "\t" + "ADDITIONAL RESPONSE DATA                      " + "\t" + f.getValue() + "(" + f.getAsciiValue() + ")" + NEW_LINE_CHAR);
                                } else if (f.getName().equals("CURRENCY CODE, TRANSACTION")) {
                                    authorizationResponseBean.setTransactionCurrencyCode(f.getAsciiValue());
                                    nationalUsedata = null;
                                    CurrencyCode[] var46;
                                    var26 = (var46 = CurrencyCode.values()).length;

                                    for(var25 = 0; var25 < var26; ++var25) {
                                        CurrencyCode currencyCodes = var46[var25];
                                        if (f.getAsciiValue().equalsIgnoreCase(currencyCodes.getCurrencyCode())) {
                                            nationalUsedata = currencyCodes.name();
                                        }
                                    }

                                    this.respMessage.append(f.getBitNo() + 1 + "\t" + "CURRENCY CODE, TRANSACTION                    " + "\t" + f.getValue() + "(" + nationalUsedata + ")" + "(" + f.getAsciiValue() + ")" + NEW_LINE_CHAR);
                                } else if (f.getName().equals("AMOUNT, ADDITIONAL")) {
                                    authorizationResponseBean.setAmountsAdditional(f.getAsciiValue());
                                    this.respMessage.append(f.getBitNo() + 1 + "\t" + "AMOUNT, ADDITIONAL                            " + "\t" + f.getValue() + "(" + f.getAsciiValue() + ")" + NEW_LINE_CHAR);
                                } else if (f.getName().equals("INTEGRATED CIRCUIT CARD SYSTEM RELATED DATA")) {
                                    nationalUsedata = "";
                                    iccBean = new IntegratedCircuitCardRelatedDataBean();
                                    if (!ISOUtil.IsNullOrEmpty(f.getValue())) {
                                        iccBean = convertICCResponseData(f.getValue());
                                        nationalUsedata = iccBean.getIccData();
                                        authorizationResponseBean.setIccRelatedData(nationalUsedata);
                                        authorizationResponseBean.setIccRelatedDataBean(iccBean);
                                    }

                                    this.respMessage.append(f.getBitNo() + 1 + "\t" + "INTEGRATED CIRCUIT CARD SYSTEM RELATED DATA   " + "\t" + f.getValue() + "(" + nationalUsedata + ")" + NEW_LINE_CHAR);
                                    this.respMessage.append("\r\n----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" + NEW_LINE_CHAR);
                                    if (!ISOUtil.IsNullOrEmpty(iccBean.getIccHeaderVersionName())) {
                                        this.respMessage.append("    ICC HEADER NAME                                 " + iccBean.getIccHeaderVersionName() + NEW_LINE_CHAR);
                                    }

                                    if (!ISOUtil.IsNullOrEmpty(iccBean.getIccHeaderVersionNumber())) {
                                        this.respMessage.append("    ICC HEADER VERSION                              " + iccBean.getIccHeaderVersionNumber() + NEW_LINE_CHAR);
                                    }

                                    if (!ISOUtil.IsNullOrEmpty(iccBean.getAppResponseCrypt())) {
                                        this.respMessage.append("    ICC AUTHORIZATION RESPONSE CRYPTOGRAM           " + iccBean.getAppResponseCrypt() + NEW_LINE_CHAR);
                                    }

                                    if (!ISOUtil.IsNullOrEmpty(iccBean.getAppResponseCode())) {
                                        this.respMessage.append("    ICC AUTHORIZATION RESPONSE CODE                 " + iccBean.getAppResponseCode() + "(" + getICCAuthResponseDescription(iccBean.getAppResponseCode()) + ")" + NEW_LINE_CHAR);
                                    }

                                    if (!ISOUtil.IsNullOrEmpty(iccBean.getIssuerScriptData())) {
                                        this.respMessage.append("    ICC ISSUER SCRIPT DATA                          " + iccBean.getIssuerScriptData() + NEW_LINE_CHAR);
                                    }

                                    if (!ISOUtil.IsNullOrEmpty(iccBean.getIssuerAuthenticationData())) {
                                        this.respMessage.append("    ICC ISSUER AUTHENTICATION DATA                          " + iccBean.getIssuerAuthenticationData() + NEW_LINE_CHAR);
                                    }
                                } else if (f.getName().equals("NATIONAL USE DATA1")) {
                                    nationalUsedata = f.getValue();
                                    if (!ISOUtil.IsNullOrEmpty(nationalUsedata)) {
                                        String pnationalUsedata = "";
                                        if (nationalUsedata.length() >= 10) {
                                            pnationalUsedata = pnationalUsedata + DataConversion.convertStringToASCII(nationalUsedata.substring(0, 10));
                                        }

                                        if (nationalUsedata.length() >= 18) {
                                            pnationalUsedata = pnationalUsedata + nationalUsedata.substring(10, 18);
                                        }

                                        pnationalUsedata = pnationalUsedata + DataConversion.convertStringToASCII(nationalUsedata.substring(18, nationalUsedata.length()));
                                        authorizationResponseBean.setNationalUseData1(pnationalUsedata);
                                        this.respMessage.append(f.getBitNo() + 1 + "\t" + "NATIONAL USE DATA1                            " + "\t" + f.getValue() + "(" + pnationalUsedata + ")" + NEW_LINE_CHAR);
                                    }
                                } else if (f.getName().equals("NATIONAL USE DATA2")) {
                                    authorizationResponseBean.setNationalUseData2(f.getAsciiValue());
                                    this.respMessage.append(f.getBitNo() + 1 + "\t" + "NATIONAL USE DATA2                            " + "\t" + f.getValue() + "(" + f.getAsciiValue() + ")" + NEW_LINE_CHAR);
                                } else if (f.getName().equals("PRIVATE USE DATA1")) {
                                    authorizationResponseBean.setValidationInformation(f.getAsciiValue());
                                    this.respMessage.append(f.getBitNo() + 1 + "\t" + "PRIVATE USE DATA1                            " + "\t" + f.getValue() + "(" + f.getAsciiValue() + ")" + NEW_LINE_CHAR);
                                } else if (f.getName().equals("PRIVATE USE DATA2")) {
                                    authorizationResponseBean.setVerificationInformation(f.getAsciiValue());
                                    this.respMessage.append(f.getBitNo() + 1 + "\t" + "PRIVATE USE DATA2                             " + "\t" + f.getValue() + "(" + f.getAsciiValue() + ")" + NEW_LINE_CHAR);
                                } else if (f.getName().equals("FUNCTION CODE")) {
                                    authorizationResponseBean.setFunctionCode(f.getAsciiValue());
                                    this.respMessage.append(f.getBitNo() + 1 + "\t" + "FUNCTION CODE                                 " + "\t" + f.getValue() + "(" + f.getAsciiValue() + ")" + NEW_LINE_CHAR);
                                } else if (f.getName().equals("KEY MANAGEMENT DATA") && !ISOUtil.IsNullOrEmpty(f.getAsciiValue())) {
                                    convertKeyManagementData(authorizationResponseBean, f.getAsciiValue());
                                    this.respMessage.append(f.getBitNo() + 1 + "\t" + "KEY MANAGEMENT DATA                                 " + "\t" + f.getValue() + "(" + f.getAsciiValue() + ")" + NEW_LINE_CHAR);
                                }
                            }
                        } else if (withNull) {
                            f = new FieldBean(i, FIELD_NAME[i], MAX_FIELD_LEN[i], FIELD_LEN_TYPE[i], -1, "", "");
                            if (f.getName().equals("PRIMARY ACCOUNT NUMBER (PAN)")) {
                                authorizationResponseBean.setPrimaryAccountNumber(new StringBuffer(f.getAsciiValue()));
                            } else if (f.getName().equals("PROCESSING CODE")) {
                                authorizationResponseBean.setProcessingCode(f.getAsciiValue());
                            } else if (f.getName().equals("AMOUNT, TRANSACTION")) {
                                authorizationResponseBean.setAmountTransaction(f.getAsciiValue());
                            } else if (f.getName().equals("DATE AND TIME, TRANSMISSION")) {
                                authorizationResponseBean.setTransmissionDateAndTime(f.getAsciiValue());
                            } else if (f.getName().equals("SYSTEMS TRACE AUDIT NUMBER")) {
                                authorizationResponseBean.setSystemTraceAuditNumber(f.getAsciiValue());
                            } else if (f.getName().equals("DATE AND TIME, LOCAL TRANSACTION")) {
                                authorizationResponseBean.setLocalTransactionDateAndTime(f.getAsciiValue());
                            } else if (f.getName().equals("DATE, SETTLEMENT")) {
                                authorizationResponseBean.setCardSettlementDate(f.getAsciiValue());
                            } else if (f.getName().equals("AMOUNTS, ORIGINAL")) {
                                authorizationResponseBean.setAmountOriginal(f.getAsciiValue());
                            } else if (f.getName().equals("ACQUIRER REFERENCE DATA")) {
                                authorizationResponseBean.setTransactionId(f.getAsciiValue());
                            } else if (f.getName().equals("ACQUIRING INSTITUTION IDENTIFICATION CODE")) {
                                authorizationResponseBean.setAcquiringInstitutionIdCode(f.getAsciiValue());
                            } else if (f.getName().equals("RETRIEVAL REFERENCE NUMBER")) {
                                authorizationResponseBean.setRetrievalReferenceNumber(f.getAsciiValue());
                            } else if (f.getName().equals("APPROVAL CODE")) {
                                authorizationResponseBean.setApprovalCode(f.getAsciiValue());
                            } else if (f.getName().equals("ACTION CODE")) {
                                authorizationResponseBean.setActionCode(f.getAsciiValue());
                            } else if (f.getName().equals("CARD ACCEPTOR TERMINAL IDENTIFICATION")) {
                                authorizationResponseBean.setCardAcceptorTerminalId(f.getAsciiValue());
                            } else if (f.getName().equals("CARD ACCEPTOR IDENTIFICATION CODE")) {
                                authorizationResponseBean.setCardAcceptorIdCode(f.getAsciiValue());
                            } else if (f.getName().equals("ADDITIONAL RESPONSE DATA")) {
                                authorizationResponseBean.setAdditionalResponseData(f.getAsciiValue());
                            } else if (f.getName().equals("CURRENCY CODE, TRANSACTION")) {
                                authorizationResponseBean.setTransactionCurrencyCode(f.getAsciiValue());
                            } else if (f.getName().equals("AMOUNT, ADDITIONAL")) {
                                authorizationResponseBean.setAmountsAdditional(f.getAsciiValue());
                            } else if (f.getName().equals("INTEGRATED CIRCUIT CARD SYSTEM RELATED DATA")) {
                                new IntegratedCircuitCardRelatedDataBean();
                                if (!ISOUtil.IsNullOrEmpty(f.getValue())) {
                                    iccBean = convertICCResponseData(f.getValue());
                                    nationalUsedata = iccBean.getIccData();
                                    authorizationResponseBean.setIccRelatedData(nationalUsedata);
                                }
                            } else if (f.getName().equals("NATIONAL USE DATA1")) {
                                authorizationResponseBean.setNationalUseData1(f.getAsciiValue());
                            } else if (f.getName().equals("NATIONAL USE DATA2")) {
                                authorizationResponseBean.setNationalUseData2(f.getAsciiValue());
                            } else if (f.getName().equals("PRIVATE USE DATA1")) {
                                authorizationResponseBean.setValidationInformation(f.getAsciiValue());
                            } else if (f.getName().equals("PRIVATE USE DATA2")) {
                                authorizationResponseBean.setVerificationInformation(f.getAsciiValue());
                            } else if (f.getName().equals("FUNCTION CODE")) {
                                authorizationResponseBean.setFunctionCode(f.getAsciiValue());
                            }
                        }
                    }

                    if (this.debugflag) {
                        Log.i("Response::" + printResponse);
                        Log.i("Debug Information  :: Response Bean :" + Constants.NEW_LINE_CHAR + authorizationResponseBean);
                    }

                    if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                        Log.i("After successfully Converting Hex to Bean" + NEW_LINE_CHAR);
                        Log.i("Response Details\r\n----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n-                                 Response in Object format                                                                                                                                       -\r\n---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n" + this.respMessage.toString() + "\r\n" + "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
                        Log.i("Exiting from parseAuthorizationResponse");
                    }

                    return authorizationResponseBean;
                }
            }
        }
    }

    public void printRequestValuesToLog(String fieldName, String fieldValue, int bitNo) {
        String printVal;
        if (bitNo == 62) {
            printVal = this.objUtility.performMasking(fieldValue, false, true, 5, 4);
            if (this.debugflag) {
                Log.i("Debug Information:" + fieldName + printVal);
            }
        } else if (bitNo != 19 && bitNo != 29 && bitNo != 12 && bitNo != 11) {
            if (this.debugflag) {
                Log.i("Debug Information:" + fieldName + fieldValue);
            }
        } else {
            printVal = this.objUtility.performMasking(fieldValue, true, false, 0, 0);
            if (this.debugflag) {
                Log.i("Debug Information:" + fieldName + printVal);
            }
        }

    }

    public void printResponseValuesToLog(String fieldName, String fieldValue, int bitNo) {
        String printVal;
        if (bitNo == 2) {
            printVal = this.objUtility.performMasking(fieldValue, false, true, 5, 4);
            if (this.debugflag) {
                Log.i("Debug Information:" + fieldName + printVal);
            }
        } else if (bitNo != 35 && bitNo != 45 && bitNo != 52 && bitNo != 53) {
            if (this.debugflag) {
                Log.i("Debug Information:" + fieldName + fieldValue);
            }
        } else {
            printVal = this.objUtility.performMasking(fieldValue, true, false, 0, 0);
            if (this.debugflag) {
                Log.i("Debug Information:" + fieldName + printVal);
            }
        }

    }

    public void setDebugFlag(boolean val) {
        this.debugflag = val;
    }

    public static IntegratedCircuitCardRelatedDataBean convertICCResponseData(String iccResponse) {
        StringBuffer iccValue = new StringBuffer();
        String applicationResponseCryptogram = null;
        String headerVersion = "";
        String convertedAsciiValue = "";
        String appResponseCode = "";
        int issuerAppDataLength = 0;
        String issuerScriptData = "";
        IntegratedCircuitCardRelatedDataBean iccBean = new IntegratedCircuitCardRelatedDataBean();
        String issuerAuthenticationData = "";
        if (iccResponse.length() > 12) {
            convertedAsciiValue = DataConversion.convertStringToASCII(iccResponse.substring(0, 8));
            headerVersion = iccResponse.substring(8, 12);
            issuerAppDataLength = DataConversion.convertHEXToDecimal(iccResponse.substring(12, 14));
            issuerAuthenticationData = iccResponse.substring(14, 14 + issuerAppDataLength * 2);
            appResponseCode = DataConversion.convertHexToASCII(iccResponse.substring(14 + (issuerAppDataLength - 2) * 2, 14 + (issuerAppDataLength - 2) * 2 + 4));
            applicationResponseCryptogram = iccResponse.substring(14, 14 + (issuerAppDataLength - 2) * 2);
            if (iccResponse.length() > 34) {
                issuerScriptData = iccResponse.substring(36);
            }

            iccValue.append(convertedAsciiValue).append(headerVersion).append(applicationResponseCryptogram).append(appResponseCode).append(issuerScriptData);
        } else {
            convertedAsciiValue = DataConversion.convertStringToASCII(iccResponse.substring(0, 8));
            headerVersion = iccResponse.substring(8, 12);
            iccValue.append(convertedAsciiValue).append(headerVersion);
        }

        if (!ISOUtil.IsNullOrEmpty(iccValue.toString())) {
            iccBean.setIccData(iccValue.toString());
        }

        if (!ISOUtil.IsNullOrEmpty(convertedAsciiValue)) {
            iccBean.setIccHeaderVersionName(convertedAsciiValue);
        }

        if (!ISOUtil.IsNullOrEmpty(headerVersion)) {
            iccBean.setIccHeaderVersionNumber(headerVersion);
        }

        if (!ISOUtil.IsNullOrEmpty(applicationResponseCryptogram)) {
            iccBean.setAppResponseCrypt(applicationResponseCryptogram);
        }

        if (!ISOUtil.IsNullOrEmpty(appResponseCode)) {
            iccBean.setAppResponseCode(appResponseCode);
        }

        if (!ISOUtil.IsNullOrEmpty(issuerScriptData)) {
            iccBean.setIssuerScriptData(issuerScriptData);
        }

        if (!ISOUtil.IsNullOrEmpty(issuerAuthenticationData)) {
            iccBean.setIssuerAuthenticationData(issuerAuthenticationData);
        }

        return iccBean;
    }

    public static void main(String[] args) {
        try {
            AuthorizationMessageFormatter l = new AuthorizationMessageFormatter();
            IntegratedCircuitCardRelatedDataBean var2 = convertICCResponseData("C1C7D5E200010A9714B599B12409B73030");
        } catch (Exception var3) {
            var3.printStackTrace();
        }

    }

    public static void convertKeyManagementData(AuthorizationResponseBean pAuthorizationResponseBean, String pKeyManagementData) {
        KeyManagementDataBean lKeyManagementDataBean = new KeyManagementDataBean();
        String lKeyManagementData = "";
        if (pKeyManagementData.length() >= 4) {
            lKeyManagementDataBean.setPrimaryId(DataConversion.convertHexToASCII(pKeyManagementData.substring(0, 4)));
            lKeyManagementData = "\r\n Primary ID ::" + lKeyManagementDataBean.getPrimaryId();
        }

        if (pKeyManagementData.length() >= 10) {
            lKeyManagementDataBean.setSecondaryID(DataConversion.convertHexToASCII(pKeyManagementData.substring(4, 10)));
            lKeyManagementData = lKeyManagementData + " \r\n Secondary ID ::" + lKeyManagementDataBean.getSecondaryID();
        }

        if (pKeyManagementData.length() >= 42) {
            lKeyManagementDataBean.setSessionKeyPin(pKeyManagementData.substring(10, 42));
            lKeyManagementData = lKeyManagementData + " \r\n Session PIN Key ::" + lKeyManagementDataBean.getSessionKeyPin();
        }

        if (pKeyManagementData.length() >= 74) {
            lKeyManagementDataBean.setSessionKeyMac(pKeyManagementData.substring(42, 74));
            lKeyManagementData = lKeyManagementData + " \r\n Session MAC Key ::" + lKeyManagementDataBean.getSessionKeyMac();
        }

        if (pKeyManagementData.length() >= 106) {
            lKeyManagementDataBean.setSessionKeyData(pKeyManagementData.substring(74, 106));
            lKeyManagementData = lKeyManagementData + " \r\n Session DATA Key ::" + lKeyManagementDataBean.getSessionKeyData();
        }

        if (pKeyManagementData.length() >= 112) {
            lKeyManagementDataBean.setSessionKeyPinCheckValue(pKeyManagementData.substring(106, 112));
            lKeyManagementData = lKeyManagementData + " \r\n Session PIN Key Check Value ::" + lKeyManagementDataBean.getSessionKeyPinCheckValue();
        }

        if (pKeyManagementData.length() >= 118) {
            lKeyManagementDataBean.setSessionKeyMacCheckValue(pKeyManagementData.substring(112, 118));
            lKeyManagementData = lKeyManagementData + " \r\n Session MAC Key Check Value ::" + lKeyManagementDataBean.getSessionKeyMacCheckValue();
        }

        if (pKeyManagementData.length() >= 124) {
            lKeyManagementDataBean.setSessionKeyDataCheckValue(pKeyManagementData.substring(118, 124));
            lKeyManagementData = lKeyManagementData + " \r\n Session DATA Key Check Value ::" + lKeyManagementDataBean.getSessionKeyDataCheckValue();
        }

        pAuthorizationResponseBean.setKeyManagementDataBean(lKeyManagementDataBean);
        pAuthorizationResponseBean.setKeyManagementData(lKeyManagementData);
    }

    public void validateAuthorizationRequestType(AuthorizationRequestBean authorizationRequestBean, AuthorizationResponseBean authorizationResponseBean) {
        if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
            Log.i("Entered into validateAuthorizationRequestType()");
        }

        String lCardPresent = "";
        String lCardDataInputMode = "";
        String lNationalUseData2SecondaryId = "";
        if (authorizationRequestBean.getNationalUseData2Bean() != null) {
            lNationalUseData2SecondaryId = authorizationRequestBean.getNationalUseData2Bean().getSecondaryId();
        }

        ErrorObject panErrorObj;
        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getPrimaryAccountNumber().toString())) {
            if (!ProcessingCode.AMEX_TRAVELERS_CHEQUE_ENCASHMENT.getProcessingCode().equalsIgnoreCase(authorizationRequestBean.getProcessingCode())) {
                panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD518_PRIMARYACCOUNTNUMBER);
                authorizationResponseBean.getAuthErrorList().add(panErrorObj);
                if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                    Log.e("Primary Account Number is incorrect");
                }
            }
        } else if (ProcessingCode.AMEX_TRAVELERS_CHEQUE_ENCASHMENT.getProcessingCode().equalsIgnoreCase(authorizationRequestBean.getProcessingCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD519_PRIMARYACCOUNTNUMBER);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Primary Account Number is incorrect");
            }
        } else if (Validator.validateCardNumber(authorizationRequestBean.getPrimaryAccountNumber().toString())) {
            if (!Validator.validateValue(authorizationRequestBean.getPrimaryAccountNumber().toString(), 62, 19, 12, true, true, AuthorizationConstants.VALID_CHAR[2])) {
                panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD2_PRIMARYACCOUNTNUMBER);
                authorizationResponseBean.getAuthErrorList().add(panErrorObj);
                if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                    Log.e("Primary Account Number is incorrect");
                }
            } else if (!Validator.validateModulusTen(authorizationRequestBean.getPrimaryAccountNumber().toString())) {
                panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD521_PRIMARYACCOUNTNUMBER);
                authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            }
        } else {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD520_PRIMARYACCOUNTNUMBER);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Primary Account Number is incorrect");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getProcessingCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD3_PROCESSINGCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Processing Code is incorrect");
            }
        } else if (!Validator.validateValue(authorizationRequestBean.getProcessingCode(), 61, 6, 6, true, true, AuthorizationConstants.VALID_CHAR[3]) || !ISOUtil.isValidProcessingCode(authorizationRequestBean.getProcessingCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD3_PROCESSINGCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Processing Code is incorrect");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAmountTransaction())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4_TRANSACTIONAMOUNT);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Amount Transaction is incorrect");
            }
        } else if (!Validator.validateValue(authorizationRequestBean.getAmountTransaction(), 60, 12, 0, true, true, AuthorizationConstants.VALID_CHAR[4])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4_TRANSACTIONAMOUNT);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Amount Transaction is incorrect");
            }
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getTransmissionDateAndTime()) && (!Validator.validateValue(authorizationRequestBean.getTransmissionDateAndTime(), 57, 10, 10, true, true, AuthorizationConstants.VALID_CHAR[7]) || !ISOUtil.isValidDate(authorizationRequestBean.getTransmissionDateAndTime(), "MMDDhhmmss"))) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD7_TRANSMISSIONDATETIME);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Date Time Transmission is incorrect");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getSystemTraceAuditNumber())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD11_SYSTEMTRACEAUDITNUMBER);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("System Trace Audit Number is incorrect");
            }
        } else if (!Validator.validateValue(authorizationRequestBean.getSystemTraceAuditNumber(), 53, 6, 6, true, true, AuthorizationConstants.VALID_CHAR[11])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD11_SYSTEMTRACEAUDITNUMBER);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("System Trace Audit Number is incorrect");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getLocalTransactionDateAndTime())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD12_DATETIMELOCALTRANSACTION);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Date Time Local Transmission is incorrect");
            }
        } else if (!Validator.validateValue(authorizationRequestBean.getLocalTransactionDateAndTime(), 52, 12, 12, true, true, AuthorizationConstants.VALID_CHAR[12]) || !ISOUtil.isValidDate(authorizationRequestBean.getLocalTransactionDateAndTime(), "YYMMDDhhmmss")) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD12_DATETIMELOCALTRANSACTION);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Date Time Local Transmission is incorrect");
            }
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardEffectiveDate()) && (!Validator.validateValue(authorizationRequestBean.getCardEffectiveDate(), 51, 4, 4, true, true, AuthorizationConstants.VALID_CHAR[13]) || !ISOUtil.isValidMonth(authorizationRequestBean.getCardEffectiveDate(), "YYMM"))) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD13_DATEEFFECTIVE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Date Effective is incorrect");
            }
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardExpirationDate()) && (!Validator.validateValue(authorizationRequestBean.getCardExpirationDate(), 50, 4, 4, true, true, AuthorizationConstants.VALID_CHAR[14]) || !ISOUtil.isValidMonth(authorizationRequestBean.getCardExpirationDate(), "YYMM"))) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD14_DATEEXPIRATION);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Date Expiration is incorrect");
            }
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardSettlementDate()) && !Validator.validateValue(authorizationRequestBean.getCardSettlementDate(), 49, 6, 6, true, true, AuthorizationConstants.VALID_CHAR[15])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD15_DATESETTLEMENT);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Date Settlement is incorrect");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getMerchantLocationCountryCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD19_COUNTRYCODEACQUIRINGINSTITUTION);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Country Code Acquiring Institution is incorrect");
            }
        } else if (!ISOUtil.isValidCountryCode(authorizationRequestBean.getMerchantLocationCountryCode()) || !Validator.validateValue(authorizationRequestBean.getMerchantLocationCountryCode(), 45, 3, 3, true, true, AuthorizationConstants.VALID_CHAR[19])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD19_COUNTRYCODEACQUIRINGINSTITUTION);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Country Code Acquiring Institution is incorrect");
            }
        }

        String lCountrycode;
        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getPosDataCode()) && authorizationRequestBean.getPosDataCodeBean() != null) {
            lCountrycode = authorizationRequestBean.getPosDataCodeBean().populatePointOfServiceDataCode();
            authorizationRequestBean.setPosDataCode(lCountrycode);
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getPosDataCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD22_POSDATACODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("POS Data Code is incorrect");
            }
        } else {
            if (!Validator.validateValue(authorizationRequestBean.getPosDataCode(), 42, 12, 12, true, true, AuthorizationConstants.VALID_CHAR[22])) {
                panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD22_POSDATACODE);
                authorizationResponseBean.getAuthErrorList().add(panErrorObj);
                if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                    Log.e("POS Data Code is incorrect");
                }
            }

            if (authorizationRequestBean.getPosDataCode() != null && authorizationRequestBean.getPosDataCode().length() > 5) {
                lCardPresent = String.valueOf(authorizationRequestBean.getPosDataCode().charAt(5));
            }

            if (authorizationRequestBean.getPosDataCode() != null && authorizationRequestBean.getPosDataCode().length() > 6) {
                lCardDataInputMode = String.valueOf(authorizationRequestBean.getPosDataCode().charAt(6));
            }

            if (CardPresent.DIGITAL_WALLET_APPLICATION_INITIATED_TRANSACTIONS.getCardPresent().equalsIgnoreCase(lCardPresent) && !CardDataInputMode.POC_INTEGRATED_CKT_CARD.getCardDataInputMode().equalsIgnoreCase(lCardDataInputMode)) {
                panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD22_POS6VALUEZ);
                authorizationResponseBean.getAuthErrorList().add(panErrorObj);
                if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                    Log.e("Position 6 Value 'Z' must be used with position 7, Value 5");
                }
            }
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getFunctionCode()) && (!Validator.validateValue(authorizationRequestBean.getFunctionCode(), 40, 3, 3, true, true, AuthorizationConstants.VALID_CHAR[24]) || !ISOUtil.isValidFunctionCode(authorizationRequestBean.getFunctionCode()))) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD24_FUNCTIONCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Function Code is incorrect");
            }
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getMessageReasonCode()) && (!Validator.validateValue(authorizationRequestBean.getMessageReasonCode(), 39, 4, 4, true, true, AuthorizationConstants.VALID_CHAR[25]) || !ISOUtil.isValidMessageReasonCode(authorizationRequestBean.getMessageReasonCode()))) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD25_MESSAGEREASONCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Message Reason Code is incorrect");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardAcceptorBusinessCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD524_CARDACCEPTORBUSINESSCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Card Acceptor Business Code is incorrect");
            }
        } else if (!Validator.validateValue(authorizationRequestBean.getCardAcceptorBusinessCode(), 38, 4, 4, true, true, AuthorizationConstants.VALID_CHAR[26])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD525_CARDACCEPTORBUSINESSCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Card Acceptor Business Code is incorrect");
            }
        } else if (!ISOUtil.isValidMCCCode(authorizationRequestBean.getCardAcceptorBusinessCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD526_CARDACCEPTORBUSINESSCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Card Acceptor Business Code is incorrect");
            }
        } else {
            lCountrycode = ISOUtil.convertPaypalMCCCode(authorizationRequestBean.getCardAcceptorBusinessCode());
            authorizationRequestBean.setCardAcceptorBusinessCode(lCountrycode);
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getApprovalCodeLength())) {
            if (!authorizationRequestBean.getApprovalCodeLength().equalsIgnoreCase("2") && !authorizationRequestBean.getApprovalCodeLength().equalsIgnoreCase("6")) {
                panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD27_APPROVALCODELENGTH);
                authorizationResponseBean.getAuthErrorList().add(panErrorObj);
                if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                    Log.e("Approval Code Length is incorrect");
                }
            } else if (!Validator.validateValue(authorizationRequestBean.getApprovalCodeLength(), 37, 1, 1, true, true, AuthorizationConstants.VALID_CHAR[27])) {
                panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD27_APPROVALCODELENGTH);
                authorizationResponseBean.getAuthErrorList().add(panErrorObj);
                if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                    Log.e("Approval Code Length is incorrect");
                }
            }
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAcquiringInstitutionIdCode()) && !Validator.validateValue(authorizationRequestBean.getAcquiringInstitutionIdCode(), 32, 11, 1, true, true, AuthorizationConstants.VALID_CHAR[32])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD32_ACQUIRINGINSTITUTIONIDENTIFICATIONCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Acquiring Institution Identification Code is incorrect");
            }
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getForwardingInstitutionIdCode()) && !Validator.validateValue(authorizationRequestBean.getForwardingInstitutionIdCode(), 31, 11, 1, true, true, AuthorizationConstants.VALID_CHAR[33])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD33_FORWARDINSTITUTIONIDENTIFICATIONCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Forward Institution Identification Code is incorrect");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getTrack2Data())) {
            if (Validator.isValidPOSData(authorizationRequestBean.getPosDataCode()) && (String.valueOf(authorizationRequestBean.getPosDataCode().charAt(5)).equalsIgnoreCase(CardPresent.POC_CARD_PRESENT.getCardPresent()) || String.valueOf(authorizationRequestBean.getPosDataCode().charAt(6)).equalsIgnoreCase(CardDataInputMode.POC_SWIPED.getCardDataInputMode()) || String.valueOf(authorizationRequestBean.getPosDataCode().charAt(6)).equalsIgnoreCase(CardDataInputMode.POC_MAGNETIC_STRIPE_READ.getCardDataInputMode())) && String.valueOf(authorizationRequestBean.getPosDataCode().charAt(6)).equalsIgnoreCase(CardDataInputMode.POC_INTEGRATED_CKT_CARD.getCardDataInputMode())) {
                panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_TRACK2NOTPRESENT);
                authorizationResponseBean.getAuthErrorList().add(panErrorObj);
                if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                    Log.e("Track2 Data must be present");
                }
            }
        } else {
            if (!IsNullOrEmpty(authorizationRequestBean.getPosDataCode()) && Validator.isValidPOSData(authorizationRequestBean.getPosDataCode()) && String.valueOf(authorizationRequestBean.getPosDataCode().charAt(6)).equalsIgnoreCase(CardDataInputMode.POC_INTEGRATED_CKT_CARD.getCardDataInputMode()) && !IsNullOrEmpty(authorizationRequestBean.getTrack1Data())) {
                panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_TRACK1MUSTNOTPRESENT);
                authorizationResponseBean.getAuthErrorList().add(panErrorObj);
                if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                    Log.e("Track1 Data must not be present");
                }
            }

            if (!Validator.validateValue(authorizationRequestBean.getTrack2Data(), 29, 37, 1, true, true, AuthorizationConstants.VALID_CHAR[35])) {
                panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD35_TRACKTWODATA);
                authorizationResponseBean.getAuthErrorList().add(panErrorObj);
                if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                    Log.e("Track Two Data is incorrect");
                }
            }

            if (!IsNullOrEmpty(authorizationRequestBean.getPosDataCode()) && Validator.isValidPOSData(authorizationRequestBean.getPosDataCode()) && String.valueOf(authorizationRequestBean.getPosDataCode().charAt(5)).equalsIgnoreCase(CardPresent.POC_CARD_NOT_PRESENT.getCardPresent())) {
                panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_TRACK1TRACK2PRESENT);
                authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            }
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getTrack1Data()) && !Validator.validateValue(authorizationRequestBean.getTrack1Data(), 19, 76, 1, true, true, AuthorizationConstants.VALID_CHAR[45])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD45_TRACKONEDATA);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Track One Data is incorrect");
            }
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getPosDataCode()) && Validator.isValidPOSData(authorizationRequestBean.getPosDataCode()) && (String.valueOf(authorizationRequestBean.getPosDataCode().charAt(5)).equalsIgnoreCase(CardPresent.POC_CARD_NOT_PRESENT.getCardPresent()) || String.valueOf(authorizationRequestBean.getPosDataCode().charAt(6)).equalsIgnoreCase(CardDataInputMode.POC_INTEGRATED_CKT_CARD.getCardDataInputMode())) && !ISOUtil.IsNullOrEmpty(authorizationRequestBean.getTrack1Data())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_TRACK1TRACK2PRESENT);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Track1 Data and Track 2 Data, must not be used in card not present  transactions");
            }
        } else if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getPosDataCode()) && Validator.isValidPOSData(authorizationRequestBean.getPosDataCode()) && String.valueOf(authorizationRequestBean.getPosDataCode().charAt(6)).equalsIgnoreCase(CardDataInputMode.POC_INTEGRATED_CKT_CARD.getCardDataInputMode()) && !ISOUtil.IsNullOrEmpty(authorizationRequestBean.getTrack1Data())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_TRACK1MUSTNOTPRESENT);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Track1 Data must not be present");
            }
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getRetrievalReferenceNumber()) && !Validator.validateValue(authorizationRequestBean.getRetrievalReferenceNumber(), 27, 12, 12, true, true, AuthorizationConstants.VALID_CHAR[37])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD37_RETRIEVALREFERENCENUMBER);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Retrieval Reference Number is incorrect");
            }
        }

        if (!CountryCode.United_States.getCountryCode().equalsIgnoreCase(authorizationRequestBean.getMerchantLocationCountryCode()) && !CountryCode.Canada.getCountryCode().equalsIgnoreCase(authorizationRequestBean.getMerchantLocationCountryCode()) && ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardAcceptorTerminalId()) && !Validator.validateValue(authorizationRequestBean.getCardAcceptorTerminalId(), 23, 8, 8, true, true, AuthorizationConstants.VALID_CHAR[41])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD41_CARDACCEPTORTERMINALIDENTIFICATION);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Card Acceptor Terminal Identification is incorrect");
            }
        } else if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardAcceptorTerminalId()) && !Validator.validateValue(authorizationRequestBean.getCardAcceptorTerminalId(), 23, 8, 8, true, true, AuthorizationConstants.VALID_CHAR[41])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD41_CARDACCEPTORTERMINALIDENTIFICATION);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Card Acceptor Terminal Identification is incorrect");
            }
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardAcceptorIdCode()) && Validator.validateValue(authorizationRequestBean.getCardAcceptorIdCode(), 22, 15, 1, true, true, AuthorizationConstants.VALID_CHAR[42])) {
            if (ISOUtil.isSENumber(authorizationRequestBean.getCardAcceptorIdCode()) && !Validator.validateSENumber(authorizationRequestBean.getCardAcceptorIdCode())) {
                panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD42_CARDACCEPTORIDENTIFICATIONCODE);
                authorizationResponseBean.getAuthErrorList().add(panErrorObj);
                if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                    Log.e("Card Acceptor Identification Code is incorrect");
                }
            }
        } else {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD42_CARDACCEPTORIDENTIFICATIONCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Card Acceptor Identification Code is incorrect");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardAcceptorNameLocation()) && authorizationRequestBean.getCardAcceptorNameLocationBean() != null) {
            lCountrycode = authorizationRequestBean.getCardAcceptorNameLocationBean().populateCardAcceptorNameLocation(authorizationResponseBean);
            authorizationRequestBean.setCardAcceptorNameLocation(lCountrycode);
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardAcceptorNameLocation()) && !Validator.validateValue(authorizationRequestBean.getCardAcceptorNameLocation(), 21, 99, 1, true, true, AuthorizationConstants.VALID_CHAR[43])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD43_CARDACCEPTORNAMELOCATION);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Card Acceptor Name Location is incorrect");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAdditionalDataNational()) && authorizationRequestBean.getAdditionalDataNationalBean() != null) {
            lCountrycode = authorizationRequestBean.getAdditionalDataNationalBean().populateAdditionalDataNational(authorizationResponseBean);
            authorizationRequestBean.setAdditionalDataNational(lCountrycode);
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAdditionalDataNational()) && !ISOUtil.IsNullOrEmpty(authorizationRequestBean.getPosDataCode()) && Validator.isValidPOSData(authorizationRequestBean.getPosDataCode()) && this.isCardholderNotPresent(authorizationRequestBean.getPosDataCode()) && ISOUtil.IsNullOrEmpty(authorizationRequestBean.getTrack1Data()) && ISOUtil.IsNullOrEmpty(authorizationRequestBean.getTrack2Data()) && !Validator.validateValue(authorizationRequestBean.getAdditionalDataNational(), 17, 301, 16, true, true, AuthorizationConstants.VALID_CHAR[47])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD47_ADDITIONALDATANATIONAL);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Additional Data National is incorrect");
            }
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAdditionalDataNational()) && !authorizationRequestBean.getAdditionalDataNationalBean().getSecondaryId().equalsIgnoreCase("CPD") && !ISOUtil.IsNullOrEmpty(authorizationRequestBean.getPosDataCode()) && Validator.isValidPOSData(authorizationRequestBean.getPosDataCode()) && String.valueOf(authorizationRequestBean.getPosDataCode().charAt(5)).equalsIgnoreCase(CardPresent.POC_CARD_PRESENT.getCardPresent())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_ADDITIONALDATANATIONAL);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Additional Data-National, must not be used in card present transactions");
            }
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAdditionalDataPrivate())) {
            if (!Validator.validateValue(authorizationRequestBean.getAdditionalDataPrivate(), 16, 4, 4, true, true, AuthorizationConstants.VALID_CHAR[48])) {
                panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD48_ADDITIONALDATAPRIVATE);
                authorizationResponseBean.getAuthErrorList().add(panErrorObj);
                if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                    Log.e("Additional Data Private is incorrect");
                }
            } else if (!validateField48Value(authorizationRequestBean.getAdditionalDataPrivate())) {
                panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD48_ADDITIONALDATAPRIVATE);
                authorizationResponseBean.getAuthErrorList().add(panErrorObj);
                if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                    Log.e("Additional Data Private is incorrect");
                }
            }

            if (!authorizationRequestBean.getFunctionCode().equalsIgnoreCase(FunctionCode.AUTHORIZATION_REQUEST.getFunctionCode())) {
                panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD24_FUNCTIONCODE);
                authorizationResponseBean.getAuthErrorList().add(panErrorObj);
                if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                    Log.e("Function Code is incorrect");
                }
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getTransactionCurrencyCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD49_CURRENCYCODETRANSACTION);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Currency Code Transaction is incorrect");
            }
        } else if (!ISOUtil.isValidCurrencyCode(authorizationRequestBean.getTransactionCurrencyCode()) || !Validator.validateValue(authorizationRequestBean.getTransactionCurrencyCode(), 15, 3, 3, true, true, AuthorizationConstants.VALID_CHAR[49])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD49_CURRENCYCODETRANSACTION);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Currency Code Transaction is incorrect");
            }
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getPinData()) && !Validator.validateValue(authorizationRequestBean.getPinData(), 12, 16, 1, true, true, AuthorizationConstants.VALID_CHAR[52])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD52_PERSONALIDENTIFICATIONNUMBERDATA);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Personal Identification Number Data is incorrect");
            }
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardIdentifierCode())) {
            lCountrycode = null;
            boolean isValidPOSData = false;
            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getPosDataCode()) && Validator.isValidPOSData(authorizationRequestBean.getPosDataCode())) {
                lCountrycode = String.valueOf(authorizationRequestBean.getPosDataCode().charAt(6));
                isValidPOSData = true;
            }

            if (isValidPOSData && !CardDataInputMode.POC_TECHNICAL_FALLBACK.getCardDataInputMode().equalsIgnoreCase(lCountrycode) && !CardDataInputMode.POC_MANUALLY_ENTERED.getCardDataInputMode().equalsIgnoreCase(lCountrycode) && !CardDataInputMode.POC_SWIPED.getCardDataInputMode().equalsIgnoreCase(lCountrycode)) {
                panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD53_INCORRECTPOSFORSECURITYRELATEDCONTROLINFO);
                authorizationResponseBean.getAuthErrorList().add(panErrorObj);
                if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                    Log.e("For Security Related Control Information, POS Data Code : Card Data Input Mode should be either Technical fallback,Manually Entered, Swiped transaction with keyed or Magnetic stripe signature with Keyed");
                }
            }

            if (!Validator.validateValue(authorizationRequestBean.getCardIdentifierCode(), 11, 29, 1, true, true, AuthorizationConstants.VALID_CHAR[53])) {
                panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD53_SECURITYRELATEDCONTROLINFORMATION);
                authorizationResponseBean.getAuthErrorList().add(panErrorObj);
                if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                    Log.e("Security Related Control Information is incorrect");
                }
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getIccRelatedData()) && authorizationRequestBean.getIccRelatedDataBean() != null) {
            lCountrycode = authorizationRequestBean.getIccRelatedDataBean().populateIntegratedCircuitCardRelatedData(authorizationResponseBean, authorizationRequestBean.getTransactionCurrencyCode());
            authorizationRequestBean.setIccRelatedData(lCountrycode);
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getIccRelatedData())) {
            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getPosDataCode()) && Validator.isValidPOSData(authorizationRequestBean.getPosDataCode()) && String.valueOf(authorizationRequestBean.getPosDataCode().charAt(6)).equalsIgnoreCase(CardDataInputMode.POC_INTEGRATED_CKT_CARD.getCardDataInputMode())) {
                if (!"TKN".equals(lNationalUseData2SecondaryId) || !CardPresent.DIGITAL_WALLET_APPLICATION_INITIATED_TRANSACTIONS.getCardPresent().equalsIgnoreCase(lCardPresent) || !CardDataInputMode.POC_INTEGRATED_CKT_CARD.getCardDataInputMode().equalsIgnoreCase(lCardDataInputMode)) {
                    panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD55_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
                    authorizationResponseBean.getAuthErrorList().add(panErrorObj);
                }

                if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                    Log.e("Integrated Circuit Card System Related Data is incorrect");
                }
            }
        } else if (!Validator.validateValue(authorizationRequestBean.getIccRelatedData(), 9, 256, 1, true, true, AuthorizationConstants.VALID_CHAR[55])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD55_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Integrated Circuit Card System Related Data is incorrect");
            }
        }

        lCountrycode = authorizationRequestBean.getMerchantLocationCountryCode();
        Boolean lSafeKeyTrue = false;
        SafeKeyEnabledCountries[] var11;
        int var10 = (var11 = SafeKeyEnabledCountries.values()).length;

        for(int var9 = 0; var9 < var10; ++var9) {
            SafeKeyEnabledCountries lsfc = var11[var9];
            if (lsfc.getCountryCode().equalsIgnoreCase(lCountrycode)) {
                lSafeKeyTrue = true;
            }
        }

        String lNationalUseData1;
        if (!ISOUtil.IsNullOrEmpty(lCountrycode) && authorizationRequestBean.getNationalUseData2Bean() != null && (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getNationalUseData2Bean().getSafekeyTransactionId()) || !ISOUtil.IsNullOrEmpty(authorizationRequestBean.getNationalUseData2Bean().getSafekeyTransactionIdValue())) && !lSafeKeyTrue) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD61_NATIONALUSEDATA2AEXPSAFKEYNOTENABLED);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Country code not enabled for SafeKey transaction");
            }
        } else if (authorizationRequestBean.getNationalUseData2Bean() == null && CardPresent.DIGITAL_WALLET_APPLICATION_INITIATED_TRANSACTIONS.getCardPresent().equalsIgnoreCase(lCardPresent)) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD61_NATIONALUSEDATA2);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("National use Data 2 is incorrect");
            }
        } else if (authorizationRequestBean.getNationalUseData2Bean() != null) {
            lNationalUseData1 = "";
            if ("TKN".equals(lNationalUseData2SecondaryId)) {
                lNationalUseData1 = authorizationRequestBean.getNationalUseData2Bean().populateNationalUseDataForPmtTokenization(authorizationResponseBean);
                authorizationRequestBean.setNationalUseData2(lNationalUseData1);
            } else {
                lNationalUseData1 = authorizationRequestBean.getNationalUseData2Bean().populateNationalUseData(authorizationResponseBean);
                authorizationRequestBean.setNationalUseData2(lNationalUseData1);
                if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getNationalUseData2()) && !Validator.validateValue(authorizationRequestBean.getNationalUseData2(), 3, 108, 1, true, true, AuthorizationConstants.VALID_CHAR[61])) {
                    panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD61_NATIONALUSEDATA2);
                    authorizationResponseBean.getAuthErrorList().add(panErrorObj);
                    if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                        Log.e("National use Data 2 is incorrect");
                    }
                }
            }
        }

        if (CardPresent.DIGITAL_WALLET_APPLICATION_INITIATED_TRANSACTIONS.getCardPresent().equalsIgnoreCase(lCardPresent) && ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardExpirationDate())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD14_DATEEXPIRATION);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getValidationInformation())) {
            if (authorizationRequestBean.getProcessingCode().equalsIgnoreCase(ProcessingCode.AMEX_TRAVELERS_CHEQUE_ENCASHMENT.getProcessingCode())) {
                panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD62_PRIVATEUSEDATA1);
                authorizationResponseBean.getAuthErrorList().add(panErrorObj);
                if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                    Log.e("Private Use Data1 is incorrect");
                }
            }
        } else if (!Validator.validateValue(authorizationRequestBean.getValidationInformation(), 2, 60, 1, true, true, AuthorizationConstants.VALID_CHAR[62])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD62_PRIVATEUSEDATA1);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Private Use Data1 is incorrect");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getVerificationInformation()) && authorizationRequestBean.getPrivateUseData2Bean() != null) {
            lNationalUseData1 = authorizationRequestBean.getPrivateUseData2Bean().populatePrivateUseData(authorizationResponseBean);
            authorizationRequestBean.setVerificationInformation(lNationalUseData1);
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getVerificationInformation())) {
            if (authorizationRequestBean.getProcessingCode().equalsIgnoreCase(ProcessingCode.AMEX_TRAVELERS_CHEQUE_ENCASHMENT.getProcessingCode()) || authorizationRequestBean.getProcessingCode().equalsIgnoreCase(ProcessingCode.COMBINATION_AUTOMATED_ADDRESS_VERIFICATION_AND_AUTHORIZATION.getProcessingCode()) || authorizationRequestBean.getProcessingCode().equalsIgnoreCase(ProcessingCode.TRANSACTION_FOR_AUTOMATED_ADDRESS_VERIFICATION_ONLY.getProcessingCode())) {
                panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD63_PRIVATEUSEDATA2);
                authorizationResponseBean.getAuthErrorList().add(panErrorObj);
                if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                    Log.e("Private Use Data 2 is incorrect");
                }
            }
        } else {
            lNationalUseData1 = authorizationRequestBean.getVerificationInformation().substring(2, 4);
            if (!Validator.validateValue(authorizationRequestBean.getVerificationInformation(), 1, 205, 1, true, true, AuthorizationConstants.VALID_CHAR[63])) {
                panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD63_PRIVATEUSEDATA2);
                authorizationResponseBean.getAuthErrorList().add(panErrorObj);
                if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                    Log.e("Private Use Data 2 is incorrect");
                }
            }
        }

        if (authorizationRequestBean.getKeyManagementDataBean() != null) {
            lNationalUseData1 = authorizationRequestBean.getKeyManagementDataBean().populateKeyManagementDataBean(authorizationResponseBean);
            authorizationRequestBean.setKeyManagementData(lNationalUseData1);
        }

        if (authorizationRequestBean.getNationalUseData1Bean() != null) {
            lNationalUseData1 = "";
            boolean isPSPProvider = false;
            if (authorizationRequestBean.getCardAcceptorNameLocationBean() != null) {
                isPSPProvider = authorizationRequestBean.getCardAcceptorNameLocationBean().isPSPProvider();
            }

            if (authorizationRequestBean.getCardAcceptorNameLocationBean() == null && !"TKN".equalsIgnoreCase(lNationalUseData2SecondaryId)) {
                panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD60_NATIONALUSEDATA1FORPSPONLY);
                authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            } else if (authorizationRequestBean.getCardAcceptorNameLocationBean() != null && !authorizationRequestBean.getCardAcceptorNameLocationBean().isPSPProvider() && !"TKN".equalsIgnoreCase(lNationalUseData2SecondaryId)) {
                panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD60_NATIONALUSEDATA1FORPSPONLY);
                authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            } else {
                lNationalUseData1 = authorizationRequestBean.getNationalUseData1Bean().populateNationalUseDataBean(authorizationResponseBean, isPSPProvider);
            }

            authorizationRequestBean.setNationalUseData1(lNationalUseData1);
        }

        if (authorizationRequestBean.getCardAcceptorNameLocationBean() != null && authorizationRequestBean.getCardAcceptorNameLocationBean().isPSPProvider() && authorizationRequestBean.getNationalUseData1Bean() == null) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD60_NATIONALUSEDATA1MISSING);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
        }

        if (authorizationRequestBean.getSecurityRelatedControlInfoBean() != null) {
            lNationalUseData1 = authorizationRequestBean.getSecurityRelatedControlInfoBean().populateSecurityRelatedControlInfoBean(authorizationResponseBean);
            authorizationRequestBean.setSecurityRelatedControlData(lNationalUseData1);
        }

        if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
            Log.i("Exiting validateAuthorizationRequestType()");
        }

    }

    public void validateDigitalGatewayRequestType(AuthorizationRequestBean authorizationRequestBean, AuthorizationResponseBean authorizationResponseBean) {
        if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
            Log.i("Entered in to validateDigitalGatewayRequestType()");
        }

        ErrorObject panErrorObj;
        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getPrimaryAccountNumber().toString())) {
            if (!ProcessingCode.AMEX_TRAVELERS_CHEQUE_ENCASHMENT.getProcessingCode().equalsIgnoreCase(authorizationRequestBean.getProcessingCode())) {
                panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD518_PRIMARYACCOUNTNUMBER);
                authorizationResponseBean.getAuthErrorList().add(panErrorObj);
                if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                    Log.e("Primary Account Number is incorrect");
                }
            }
        } else if (ProcessingCode.AMEX_TRAVELERS_CHEQUE_ENCASHMENT.getProcessingCode().equalsIgnoreCase(authorizationRequestBean.getProcessingCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD519_PRIMARYACCOUNTNUMBER);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Primary Account Number is incorrect");
            }
        } else if (Validator.validateCardNumber(authorizationRequestBean.getPrimaryAccountNumber().toString())) {
            if (!Validator.validateValue(authorizationRequestBean.getPrimaryAccountNumber().toString(), 62, 19, 12, true, true, AuthorizationConstants.VALID_CHAR[2])) {
                panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD2_PRIMARYACCOUNTNUMBER);
                authorizationResponseBean.getAuthErrorList().add(panErrorObj);
                if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                    Log.e("Primary Account Number is incorrect");
                }
            } else if (!Validator.validateModulusTen(authorizationRequestBean.getPrimaryAccountNumber().toString())) {
                panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD521_PRIMARYACCOUNTNUMBER);
                authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            }
        } else {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD520_PRIMARYACCOUNTNUMBER);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Primary Account Number is incorrect");
            }
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardAcceptorIdCode()) && Validator.validateValue(authorizationRequestBean.getCardAcceptorIdCode(), 22, 15, 1, true, true, AuthorizationConstants.VALID_CHAR[42])) {
            if (ISOUtil.isSENumber(authorizationRequestBean.getCardAcceptorIdCode()) && !Validator.validateSENumber(authorizationRequestBean.getCardAcceptorIdCode())) {
                panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD42_CARDACCEPTORIDENTIFICATIONCODE);
                authorizationResponseBean.getAuthErrorList().add(panErrorObj);
                if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                    Log.e("Card Acceptor Identification Code is incorrect");
                }
            }
        } else {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD42_CARDACCEPTORIDENTIFICATIONCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Card Acceptor Identification Code is incorrect");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAdditionalDataNational()) && authorizationRequestBean.getAdditionalDataNationalBean() != null) {
            String additionalDataNational = authorizationRequestBean.getAdditionalDataNationalBean().populateAdditionalDataNational(authorizationResponseBean);
            authorizationRequestBean.setAdditionalDataNational(additionalDataNational);
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAdditionalDataNational()) && !ISOUtil.IsNullOrEmpty("0000S0100000") && Validator.isValidPOSData("0000S0100000") && String.valueOf("0000S0100000".charAt(5)).equalsIgnoreCase(CardPresent.POC_CARD_PRESENT.getCardPresent())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_ADDITIONALDATANATIONAL);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Additional Data-National, must not be used in card present  transactions");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getVerificationInformation()) && authorizationRequestBean.getPrivateUseData2Bean() != null) {
            PrivateUseData2Bean objprivateUseData2 = authorizationRequestBean.getPrivateUseData2Bean();
            objprivateUseData2.setServiceIdentifier("AX");
            objprivateUseData2.setRequestTypeIdentifier("AD");
            authorizationRequestBean.setPrivateUseData2Bean(objprivateUseData2);
            String verificationInformation = authorizationRequestBean.getPrivateUseData2Bean().populatePrivateUseData(authorizationResponseBean);
            authorizationRequestBean.setVerificationInformation(verificationInformation);
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getVerificationInformation()) && !Validator.validateValue(authorizationRequestBean.getVerificationInformation(), 1, 205, 1, true, true, AuthorizationConstants.VALID_CHAR[63])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD63_PRIVATEUSEDATA2);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Private Use Data 2 is incorrect");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAdditionalDataNational()) && ISOUtil.IsNullOrEmpty(authorizationRequestBean.getVerificationInformation())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_FIELD47FIELD63MISSING);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Both Field 47 and Field 63 are missing");
            }
        }

        if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
            Log.i("Exiting validateDigitalGatewayRequestType()");
        }

    }

    public void setResponseBean(AuthorizationRequestBean authorizationRequestBean, AuthorizationResponseBean authorizationResponseBean, String actionCode) {
        if (authorizationRequestBean != null) {
            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getPrimaryAccountNumber().toString())) {
                authorizationResponseBean.setPrimaryAccountNumber(authorizationRequestBean.getPrimaryAccountNumber());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getProcessingCode())) {
                authorizationResponseBean.setProcessingCode(authorizationRequestBean.getProcessingCode());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAmountTransaction())) {
                authorizationResponseBean.setAmountTransaction(authorizationRequestBean.getAmountTransaction());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getTransmissionDateAndTime())) {
                authorizationResponseBean.setTransmissionDateAndTime(authorizationRequestBean.getTransmissionDateAndTime());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getSystemTraceAuditNumber())) {
                authorizationResponseBean.setSystemTraceAuditNumber(authorizationRequestBean.getSystemTraceAuditNumber().toUpperCase());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getLocalTransactionDateAndTime())) {
                authorizationResponseBean.setLocalTransactionDateAndTime(authorizationRequestBean.getLocalTransactionDateAndTime());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardEffectiveDate())) {
                authorizationResponseBean.setCardEffectiveDate(authorizationRequestBean.getCardEffectiveDate());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardExpirationDate())) {
                authorizationResponseBean.setCardExpirationDate(authorizationRequestBean.getCardExpirationDate());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardSettlementDate())) {
                authorizationResponseBean.setCardSettlementDate(authorizationRequestBean.getCardSettlementDate());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getMerchantLocationCountryCode())) {
                authorizationResponseBean.setMerchantLocationCountryCode(authorizationRequestBean.getMerchantLocationCountryCode());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getPosDataCode())) {
                authorizationResponseBean.setPosDataCode(authorizationRequestBean.getPosDataCode());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getFunctionCode())) {
                authorizationResponseBean.setFunctionCode(authorizationRequestBean.getFunctionCode());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getMessageReasonCode())) {
                authorizationResponseBean.setMessageReasonCode(authorizationRequestBean.getMessageReasonCode());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardAcceptorBusinessCode())) {
                authorizationResponseBean.setCardAcceptorBusinessCode(authorizationRequestBean.getCardAcceptorBusinessCode());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getApprovalCodeLength())) {
                authorizationResponseBean.setApprovalCodeLength(authorizationRequestBean.getApprovalCodeLength());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAmountOriginal())) {
                authorizationResponseBean.setAmountOriginal(authorizationRequestBean.getAmountOriginal());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getTransactionId())) {
                authorizationResponseBean.setTransactionId(authorizationRequestBean.getTransactionId());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAcquiringInstitutionIdCode())) {
                authorizationResponseBean.setAcquiringInstitutionIdCode(authorizationRequestBean.getAcquiringInstitutionIdCode());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getForwardingInstitutionIdCode())) {
                authorizationResponseBean.setForwardingInstitutionIdCode(authorizationRequestBean.getForwardingInstitutionIdCode());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getTrack2Data())) {
                authorizationResponseBean.setTrack2Data(authorizationRequestBean.getTrack2Data());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getRetrievalReferenceNumber())) {
                authorizationResponseBean.setRetrievalReferenceNumber(authorizationRequestBean.getRetrievalReferenceNumber());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getApprovalCode())) {
                authorizationResponseBean.setApprovalCode(authorizationRequestBean.getApprovalCode());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardAcceptorTerminalId())) {
                authorizationResponseBean.setCardAcceptorTerminalId(authorizationRequestBean.getCardAcceptorTerminalId());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardAcceptorIdCode())) {
                authorizationResponseBean.setCardAcceptorIdCode(authorizationRequestBean.getCardAcceptorIdCode());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardAcceptorNameLocation())) {
                authorizationResponseBean.setCardAcceptorNameLocation(authorizationRequestBean.getCardAcceptorNameLocation());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAdditionalResponseData())) {
                authorizationResponseBean.setAdditionalResponseData(authorizationRequestBean.getAdditionalResponseData());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getTrack1Data())) {
                authorizationResponseBean.setTrack1Data(authorizationRequestBean.getTrack1Data());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAdditionalDataNational())) {
                authorizationResponseBean.setAdditionalDataNational(authorizationRequestBean.getAdditionalDataNational());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAdditionalDataPrivate())) {
                authorizationResponseBean.setAdditionalDataPrivate(authorizationRequestBean.getAdditionalDataPrivate());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getTransactionCurrencyCode())) {
                authorizationResponseBean.setTransactionCurrencyCode(authorizationRequestBean.getTransactionCurrencyCode());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getPinData())) {
                authorizationResponseBean.setPinData(authorizationRequestBean.getPinData());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardIdentifierCode())) {
                authorizationResponseBean.setCardIdentifierCode(authorizationRequestBean.getCardIdentifierCode());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAmountsAdditional())) {
                authorizationResponseBean.setAmountsAdditional(authorizationRequestBean.getAmountsAdditional());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getIccRelatedData())) {
                authorizationResponseBean.setIccRelatedData(authorizationRequestBean.getIccRelatedData());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getOriginalDataElements())) {
                authorizationResponseBean.setOriginalDataElements(authorizationRequestBean.getOriginalDataElements());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getNationalUseData1())) {
                authorizationResponseBean.setNationalUseData1(authorizationRequestBean.getNationalUseData1());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getNationalUseData2())) {
                authorizationResponseBean.setNationalUseData2(authorizationRequestBean.getNationalUseData2());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getValidationInformation())) {
                authorizationResponseBean.setValidationInformation(authorizationRequestBean.getValidationInformation());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getVerificationInformation())) {
                authorizationResponseBean.setVerificationInformation(authorizationRequestBean.getValidationInformation());
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getMessageAuthenticationCode())) {
                authorizationResponseBean.setMessageAuthenticationCode(authorizationRequestBean.getValidationInformation());
            }

            if (this.debugflag) {
                Log.i("Error in validateTransaction()-- Response code: " + actionCode);
            }

            authorizationResponseBean.setActionCode(actionCode);
        }

    }

    public void validateAdjustmentFinancialAdviceRequestType(AuthorizationRequestBean authorizationRequestBean, AuthorizationResponseBean authorizationResponseBean) {
        if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
            this.debugflag = true;
            Log.i("Entered into validateAdjustmentFinancialAdviceRequestType()");
        }

        ErrorObject panErrorObj;
        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getPrimaryAccountNumber().toString())) {
            if (!ProcessingCode.AMEX_TRAVELERS_CHEQUE_ENCASHMENT.getProcessingCode().equalsIgnoreCase(authorizationRequestBean.getProcessingCode())) {
                panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD518_PRIMARYACCOUNTNUMBER);
                authorizationResponseBean.getAuthErrorList().add(panErrorObj);
                if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                    Log.e("Primary Account Number is not selected");
                }
            }
        } else if (ProcessingCode.AMEX_TRAVELERS_CHEQUE_ENCASHMENT.getProcessingCode().equalsIgnoreCase(authorizationRequestBean.getProcessingCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD519_PRIMARYACCOUNTNUMBER);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Primary Account Number is incorrect");
            }
        } else if (Validator.validateCardNumber(authorizationRequestBean.getPrimaryAccountNumber().toString())) {
            if (!Validator.validateModulusTen(authorizationRequestBean.getPrimaryAccountNumber().toString())) {
                panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD521_PRIMARYACCOUNTNUMBER);
                authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            } else if (!Validator.validateValue(authorizationRequestBean.getPrimaryAccountNumber().toString(), 62, 19, 12, true, true, AuthorizationConstants.VALID_CHAR[2])) {
                panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD2_PRIMARYACCOUNTNUMBER);
                authorizationResponseBean.getAuthErrorList().add(panErrorObj);
                if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                    Log.e("Primary Account Number is not selected");
                }
            }
        } else {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD520_PRIMARYACCOUNTNUMBER);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Primary Account Number is not selected");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getProcessingCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD3_PROCESSINGCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Processing Code is not Entered");
            }
        } else if (!Validator.validateValue(authorizationRequestBean.getProcessingCode(), 61, 6, 6, true, true, AuthorizationConstants.VALID_CHAR[3]) || !ISOUtil.isValidProcessingCode(authorizationRequestBean.getProcessingCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD3_PROCESSINGCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Processing Code is incorrect");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAmountTransaction())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4_TRANSACTIONAMOUNT);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Transaction Amount is not entered ");
            }
        } else if (!Validator.validateValue(authorizationRequestBean.getAmountTransaction(), 60, 12, 0, true, true, AuthorizationConstants.VALID_CHAR[4])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4_TRANSACTIONAMOUNT);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Amount Transaction is incorrect");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getSystemTraceAuditNumber())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD11_SYSTEMTRACEAUDITNUMBER);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("System Trace Audit Number is not selected");
            }
        } else if (!Validator.validateValue(authorizationRequestBean.getSystemTraceAuditNumber(), 53, 6, 6, true, true, AuthorizationConstants.VALID_CHAR[11])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD11_SYSTEMTRACEAUDITNUMBER);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("System Trace Audit Number is incorrect");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getLocalTransactionDateAndTime())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD12_DATETIMELOCALTRANSACTION);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Date Time Local Transmission is not selected");
            }
        } else if (!Validator.validateValue(authorizationRequestBean.getLocalTransactionDateAndTime(), 52, 12, 12, true, true, AuthorizationConstants.VALID_CHAR[12]) || !ISOUtil.isValidDate(authorizationRequestBean.getLocalTransactionDateAndTime(), "YYMMDDhhmmss")) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD12_DATETIMELOCALTRANSACTION);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Date Time Local Transmission is incorrect.");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getMerchantLocationCountryCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD19_COUNTRYCODEACQUIRINGINSTITUTION);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("MerchantLocationCountryCode is not selected");
            }
        } else if (!ISOUtil.isValidCountryCode(authorizationRequestBean.getMerchantLocationCountryCode()) || !Validator.validateValue(authorizationRequestBean.getMerchantLocationCountryCode(), 45, 3, 3, true, true, AuthorizationConstants.VALID_CHAR[19])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD19_COUNTRYCODEACQUIRINGINSTITUTION);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Country Code Acquiring Institution is incorrect");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getPosDataCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD22_POSDATACODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("PosDataCode is not selected");
            }
        } else if (!Validator.validateValue(authorizationRequestBean.getPosDataCode(), 42, 12, 12, true, true, AuthorizationConstants.VALID_CHAR[22])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD22_POSDATACODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("POS Data Code is incorrect");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getFunctionCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD24_FUNCTIONCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Function Code is not selected");
            }
        } else if (!Validator.validateValue(authorizationRequestBean.getFunctionCode(), 40, 3, 3, true, true, AuthorizationConstants.VALID_CHAR[24]) || !ISOUtil.isValidFunctionCode(authorizationRequestBean.getFunctionCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD24_FUNCTIONCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Function Code is incorrect");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getMessageReasonCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD25_MESSAGEREASONCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("MessageReasonCode is not selected");
            }
        } else if (!Validator.validateValue(authorizationRequestBean.getMessageReasonCode(), 39, 4, 4, true, true, AuthorizationConstants.VALID_CHAR[25]) || !ISOUtil.isValidMessageReasonCode(authorizationRequestBean.getMessageReasonCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD25_MESSAGEREASONCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Message Reason Code is incorrect");
            }
        }

        String originalDataElements;
        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardAcceptorBusinessCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD524_CARDACCEPTORBUSINESSCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("CardAcceptorBusinessCode is not selected");
            }
        } else if (!Validator.validateValue(authorizationRequestBean.getCardAcceptorBusinessCode(), 38, 4, 4, true, true, AuthorizationConstants.VALID_CHAR[26])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD525_CARDACCEPTORBUSINESSCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Card Acceptor Business Code is incorrect");
            }
        } else if (!ISOUtil.isValidMCCCode(authorizationRequestBean.getCardAcceptorBusinessCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD526_CARDACCEPTORBUSINESSCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Card Acceptor Business Code is incorrect");
            }
        } else {
            originalDataElements = ISOUtil.convertPaypalMCCCode(authorizationRequestBean.getCardAcceptorBusinessCode());
            authorizationRequestBean.setCardAcceptorBusinessCode(originalDataElements);
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAmountOriginal())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD30_AMOUNTSORIGINAL);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("AmountOriginal is not selected");
            }
        } else if (Validator.validateValue(authorizationRequestBean.getAmountOriginal(), 34, 24, 12, true, true, AuthorizationConstants.VALID_CHAR[30])) {
            originalDataElements = ISOUtil.padString(authorizationRequestBean.getAmountOriginal(), 24, "0", true, false);
            authorizationRequestBean.setAmountOriginal(originalDataElements);
        } else {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD30_AMOUNTSORIGINAL);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Amounts Original is incorrect");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getTransactionId())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD31_ACQUIRERREFERENCEDATA);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("TransactionId is not selected");
            }
        } else if (!Validator.validateValue(authorizationRequestBean.getTransactionId(), 33, 48, 1, true, true, AuthorizationConstants.VALID_CHAR[31])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD31_ACQUIRERREFERENCEDATA);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Acquirer Reference Data is incorrect");
            }
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAcquiringInstitutionIdCode()) && !Validator.validateValue(authorizationRequestBean.getAcquiringInstitutionIdCode(), 32, 11, 1, true, true, AuthorizationConstants.VALID_CHAR[32])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD32_ACQUIRINGINSTITUTIONIDENTIFICATIONCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Acquiring Institution Identification Code is incorrect");
            }
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getForwardingInstitutionIdCode()) && !Validator.validateValue(authorizationRequestBean.getForwardingInstitutionIdCode(), 31, 11, 1, true, true, AuthorizationConstants.VALID_CHAR[33])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD33_FORWARDINSTITUTIONIDENTIFICATIONCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Forward Institution Identification Code is incorrect");
            }
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getRetrievalReferenceNumber()) && !Validator.validateValue(authorizationRequestBean.getRetrievalReferenceNumber(), 27, 12, 12, true, true, AuthorizationConstants.VALID_CHAR[37])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD37_RETRIEVALREFERENCENUMBER);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Retrieval Reference Number is incorrect");
            }
        }

        if (!CountryCode.United_States.getCountryCode().equalsIgnoreCase(authorizationRequestBean.getMerchantLocationCountryCode()) && !CountryCode.Canada.getCountryCode().equalsIgnoreCase(authorizationRequestBean.getMerchantLocationCountryCode()) && ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardAcceptorTerminalId()) && !Validator.validateValue(authorizationRequestBean.getCardAcceptorTerminalId(), 23, 8, 8, true, true, AuthorizationConstants.VALID_CHAR[41])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD41_CARDACCEPTORTERMINALIDENTIFICATION);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Card Acceptor Terminal Identification is incorrect");
            }
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardAcceptorTerminalId()) && !Validator.validateValue(authorizationRequestBean.getCardAcceptorTerminalId(), 23, 8, 8, true, true, AuthorizationConstants.VALID_CHAR[41])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD41_CARDACCEPTORTERMINALIDENTIFICATION);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Card Acceptor Terminal Identification is incorrect");
            }
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardAcceptorIdCode()) && Validator.validateValue(authorizationRequestBean.getCardAcceptorIdCode(), 22, 15, 1, true, true, AuthorizationConstants.VALID_CHAR[42])) {
            if (ISOUtil.isSENumber(authorizationRequestBean.getCardAcceptorIdCode()) && !Validator.validateSENumber(authorizationRequestBean.getCardAcceptorIdCode())) {
                panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD42_CARDACCEPTORIDENTIFICATIONCODE);
                authorizationResponseBean.getAuthErrorList().add(panErrorObj);
                if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                    Log.e("Card Acceptor Identification Code is incorrect");
                }
            }
        } else {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD42_CARDACCEPTORIDENTIFICATIONCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Card Acceptor Identification Code is incorrect");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardAcceptorNameLocation()) && authorizationRequestBean.getCardAcceptorNameLocationBean() != null) {
            originalDataElements = authorizationRequestBean.getCardAcceptorNameLocationBean().populateCardAcceptorNameLocation(authorizationResponseBean);
            authorizationRequestBean.setCardAcceptorNameLocation(originalDataElements);
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardAcceptorNameLocation()) && !Validator.validateValue(authorizationRequestBean.getCardAcceptorNameLocation(), 21, 99, 1, true, true, AuthorizationConstants.VALID_CHAR[43])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD43_CARDACCEPTORNAMELOCATION);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Card Acceptor Name Location is incorrect");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getTransactionCurrencyCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD49_CURRENCYCODETRANSACTION);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Transaction CurrencyCode is not selected");
            }
        } else if (!ISOUtil.isValidCurrencyCode(authorizationRequestBean.getTransactionCurrencyCode()) || !Validator.validateValue(authorizationRequestBean.getTransactionCurrencyCode(), 15, 3, 3, true, true, AuthorizationConstants.VALID_CHAR[49])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD49_CURRENCYCODETRANSACTION);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Transaction CurrencyCode is incorrect");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getOriginalDataElements()) && authorizationRequestBean.getOriginalDataElementsBean() != null) {
            originalDataElements = authorizationRequestBean.getOriginalDataElementsBean().populateOriginalDataElement(authorizationResponseBean);
            authorizationRequestBean.setOriginalDataElements(originalDataElements);
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getOriginalDataElements())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD56_ORIGINALDATAELEMENTSDATA);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Original Data Elements Data is incorrect");
            }
        } else if (!Validator.validateValue(authorizationRequestBean.getOriginalDataElements(), 8, 35, 1, true, true, AuthorizationConstants.VALID_CHAR[56])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD56_ORIGINALDATAELEMENTSDATA);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Original Data Elements Data is incorrect");
            }
        }

        if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
            Log.i("Exiting  validateAdjustmentFinancialAdviceRequestType()");
        }

    }

    public void validateNetworkManagementRequestType(AuthorizationRequestBean authorizationRequestBean, AuthorizationResponseBean authorizationResponseBean) {
        if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
            this.debugflag = true;
            Log.i("Entered into validateNetworkManagementRequestType()");
        }

        ErrorObject panErrorObj;
        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getProcessingCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD3_PROCESSINGCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Processing Code is not selected");
            }
        } else if (!Validator.validateValue(authorizationRequestBean.getProcessingCode(), 61, 6, 6, true, true, AuthorizationConstants.VALID_CHAR[3]) || !ISOUtil.isValidProcessingCode(authorizationRequestBean.getProcessingCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD3_PROCESSINGCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Processing Code is not selected");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getSystemTraceAuditNumber())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD11_SYSTEMTRACEAUDITNUMBER);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("System Trace Audit Number is incorrect");
            }
        } else if (!Validator.validateValue(authorizationRequestBean.getSystemTraceAuditNumber(), 53, 6, 6, true, true, AuthorizationConstants.VALID_CHAR[11])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD11_SYSTEMTRACEAUDITNUMBER);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("System Trace Audit Number is incorrect");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getLocalTransactionDateAndTime())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD12_DATETIMELOCALTRANSACTION);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Date Time Local Transmission is incorrect");
            }
        } else if (!Validator.validateValue(authorizationRequestBean.getLocalTransactionDateAndTime(), 52, 12, 12, true, true, AuthorizationConstants.VALID_CHAR[12]) || !ISOUtil.isValidDate(authorizationRequestBean.getLocalTransactionDateAndTime(), "YYMMDDhhmmss")) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD12_DATETIMELOCALTRANSACTION);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Date Time Local Transmission is incorrect");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getFunctionCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD24_FUNCTIONCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("FunctionCode is not selected");
            }
        } else if (!Validator.validateValue(authorizationRequestBean.getFunctionCode(), 40, 3, 3, true, true, AuthorizationConstants.VALID_CHAR[24]) || !ISOUtil.isValidFunctionCode(authorizationRequestBean.getFunctionCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD24_FUNCTIONCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Function Code is incorrect");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getMessageReasonCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD25_MESSAGEREASONCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("MessageReasonCode is not selected");
            }
        } else if (!Validator.validateValue(authorizationRequestBean.getMessageReasonCode(), 39, 4, 4, true, true, AuthorizationConstants.VALID_CHAR[25]) || !ISOUtil.isValidMessageReasonCode(authorizationRequestBean.getMessageReasonCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD25_MESSAGEREASONCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Message Reason Code is incorrect");
            }
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getForwardingInstitutionIdCode()) && !Validator.validateValue(authorizationRequestBean.getForwardingInstitutionIdCode(), 31, 11, 1, true, true, AuthorizationConstants.VALID_CHAR[33])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD33_FORWARDINSTITUTIONIDENTIFICATIONCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
        }

        if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
            Log.i("Exiting validateNetworkManagementRequestTransFields()");
        }

    }

    public void validateReversalAdviceRequestType(AuthorizationRequestBean authorizationRequestBean, AuthorizationResponseBean authorizationResponseBean) {
        if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
            this.debugflag = true;
            Log.i("Entered into validateReversalAdviceRequestType()");
        }

        ErrorObject panErrorObj;
        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getPrimaryAccountNumber().toString())) {
            if (!ProcessingCode.AMEX_TRAVELERS_CHEQUE_ENCASHMENT.getProcessingCode().equalsIgnoreCase(authorizationRequestBean.getProcessingCode())) {
                panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD518_PRIMARYACCOUNTNUMBER);
                authorizationResponseBean.getAuthErrorList().add(panErrorObj);
                if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                    Log.e("PrimaryAccountNumber is not selected");
                }
            }
        } else if (ProcessingCode.AMEX_TRAVELERS_CHEQUE_ENCASHMENT.getProcessingCode().equalsIgnoreCase(authorizationRequestBean.getProcessingCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD519_PRIMARYACCOUNTNUMBER);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Primary Account Number is incorrect");
            }
        } else if (Validator.validateCardNumber(authorizationRequestBean.getPrimaryAccountNumber().toString())) {
            if (!Validator.validateModulusTen(authorizationRequestBean.getPrimaryAccountNumber().toString())) {
                panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD521_PRIMARYACCOUNTNUMBER);
                authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            } else if (!Validator.validateValue(authorizationRequestBean.getPrimaryAccountNumber().toString(), 62, 19, 12, true, true, AuthorizationConstants.VALID_CHAR[2])) {
                panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD2_PRIMARYACCOUNTNUMBER);
                authorizationResponseBean.getAuthErrorList().add(panErrorObj);
                if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                    Log.e("Primary Account Number is incorrect");
                }
            }
        } else {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD520_PRIMARYACCOUNTNUMBER);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Primary Account Number is incorrect");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getProcessingCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD3_PROCESSINGCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("ProcessingCode is not selected");
            }
        } else if (!Validator.validateValue(authorizationRequestBean.getProcessingCode(), 61, 6, 6, true, true, AuthorizationConstants.VALID_CHAR[3]) || !ISOUtil.isValidProcessingCode(authorizationRequestBean.getProcessingCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD3_PROCESSINGCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Processing Code is incorrect.");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAmountTransaction())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4_TRANSACTIONAMOUNT);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Amount Transaction is not selected");
            }
        } else if (!Validator.validateValue(authorizationRequestBean.getAmountTransaction(), 60, 12, 0, true, true, AuthorizationConstants.VALID_CHAR[4])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD4_TRANSACTIONAMOUNT);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Amount Transaction is incorrect");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getSystemTraceAuditNumber())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD11_SYSTEMTRACEAUDITNUMBER);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("System Trace Audit Number is incorrect");
            }
        } else if (!Validator.validateValue(authorizationRequestBean.getSystemTraceAuditNumber(), 53, 6, 6, true, true, AuthorizationConstants.VALID_CHAR[11])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD11_SYSTEMTRACEAUDITNUMBER);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("System Trace Audit Number is incorrect");
            }
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardExpirationDate()) && !Validator.validateValue(authorizationRequestBean.getCardExpirationDate(), 50, 4, 4, true, true, AuthorizationConstants.VALID_CHAR[14]) || !ISOUtil.isValidMonth(authorizationRequestBean.getCardExpirationDate(), "YYMM")) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD14_DATEEXPIRATION);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Date Expiration is incorrect");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getLocalTransactionDateAndTime())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD12_DATETIMELOCALTRANSACTION);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Date Time Local Transmission is incorrect");
            }
        } else if (!Validator.validateValue(authorizationRequestBean.getLocalTransactionDateAndTime(), 52, 12, 12, true, true, AuthorizationConstants.VALID_CHAR[12]) || !ISOUtil.isValidDate(authorizationRequestBean.getLocalTransactionDateAndTime(), "YYMMDDhhmmss")) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD12_DATETIMELOCALTRANSACTION);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Date Time Local Transmission is incorrect");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getMerchantLocationCountryCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD19_COUNTRYCODEACQUIRINGINSTITUTION);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("MerchantLocationCountryCode is not selected");
            }
        } else if (!ISOUtil.isValidCountryCode(authorizationRequestBean.getMerchantLocationCountryCode()) || !Validator.validateValue(authorizationRequestBean.getMerchantLocationCountryCode(), 45, 3, 3, true, true, AuthorizationConstants.VALID_CHAR[19])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD19_COUNTRYCODEACQUIRINGINSTITUTION);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Country Code Acquiring Institution is incorrect");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getPosDataCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD22_POSDATACODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("PosDataCode is not selected");
            }
        } else if (!Validator.validateValue(authorizationRequestBean.getPosDataCode(), 42, 12, 12, true, true, AuthorizationConstants.VALID_CHAR[22])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD22_POSDATACODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("POS Data Code is incorrect");
            }
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getMessageReasonCode()) && (!Validator.validateValue(authorizationRequestBean.getMessageReasonCode(), 39, 4, 4, true, true, AuthorizationConstants.VALID_CHAR[25]) || !ISOUtil.isValidMessageReasonCode(authorizationRequestBean.getMessageReasonCode()))) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD25_MESSAGEREASONCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Message Reason Code is incorrect");
            }
        }

        String originalDataElements;
        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardAcceptorBusinessCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD524_CARDACCEPTORBUSINESSCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Card Acceptor Business Code is not selected");
            }
        } else if (!Validator.validateValue(authorizationRequestBean.getCardAcceptorBusinessCode(), 38, 4, 4, true, true, AuthorizationConstants.VALID_CHAR[26])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD525_CARDACCEPTORBUSINESSCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Card Acceptor Business Code is incorrect");
            }
        } else if (!ISOUtil.isValidMCCCode(authorizationRequestBean.getCardAcceptorBusinessCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD526_CARDACCEPTORBUSINESSCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Card Acceptor Business Code is incorrect");
            }
        } else {
            originalDataElements = ISOUtil.convertPaypalMCCCode(authorizationRequestBean.getCardAcceptorBusinessCode());
            authorizationRequestBean.setCardAcceptorBusinessCode(originalDataElements);
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getTransactionId()) && !Validator.validateValue(authorizationRequestBean.getTransactionId(), 33, 48, 1, true, true, AuthorizationConstants.VALID_CHAR[31])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD31_ACQUIRERREFERENCEDATA);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Acquirer Reference Data is incorrect");
            }
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getAcquiringInstitutionIdCode()) && !Validator.validateValue(authorizationRequestBean.getAcquiringInstitutionIdCode(), 32, 11, 1, true, true, AuthorizationConstants.VALID_CHAR[32])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD32_ACQUIRINGINSTITUTIONIDENTIFICATIONCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Acquiring Institution Identification Code is incorrect");
            }
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getForwardingInstitutionIdCode()) && !Validator.validateValue(authorizationRequestBean.getForwardingInstitutionIdCode(), 31, 11, 1, true, true, AuthorizationConstants.VALID_CHAR[33])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD33_FORWARDINSTITUTIONIDENTIFICATIONCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Forward Institution Identification Code is incorrect");
            }
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getRetrievalReferenceNumber()) && !Validator.validateValue(authorizationRequestBean.getRetrievalReferenceNumber(), 27, 12, 12, true, true, AuthorizationConstants.VALID_CHAR[37])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD37_RETRIEVALREFERENCENUMBER);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Retrieval Reference Number is incorrect");
            }
        }

        if (!CountryCode.United_States.getCountryCode().equalsIgnoreCase(authorizationRequestBean.getMerchantLocationCountryCode()) && !CountryCode.Canada.getCountryCode().equalsIgnoreCase(authorizationRequestBean.getMerchantLocationCountryCode()) && ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardAcceptorTerminalId()) && !Validator.validateValue(authorizationRequestBean.getCardAcceptorTerminalId(), 23, 8, 8, true, true, AuthorizationConstants.VALID_CHAR[41])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD41_CARDACCEPTORTERMINALIDENTIFICATION);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Card Acceptor Terminal Identification is incorrect");
            }
        } else if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardAcceptorTerminalId()) && !Validator.validateValue(authorizationRequestBean.getCardAcceptorTerminalId(), 23, 8, 8, true, true, AuthorizationConstants.VALID_CHAR[41])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD41_CARDACCEPTORTERMINALIDENTIFICATION);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Card Acceptor Terminal Identification is incorrect");
            }
        }

        if (!ISOUtil.IsNullOrEmpty(authorizationRequestBean.getCardAcceptorIdCode()) && Validator.validateValue(authorizationRequestBean.getCardAcceptorIdCode(), 22, 15, 1, true, true, AuthorizationConstants.VALID_CHAR[42])) {
            if (ISOUtil.isSENumber(authorizationRequestBean.getCardAcceptorIdCode()) && !Validator.validateSENumber(authorizationRequestBean.getCardAcceptorIdCode())) {
                panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD42_CARDACCEPTORIDENTIFICATIONCODE);
                authorizationResponseBean.getAuthErrorList().add(panErrorObj);
                if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                    Log.e("Card Acceptor Identification Code is incorrect");
                }
            }
        } else {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD42_CARDACCEPTORIDENTIFICATIONCODE);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Card Acceptor Identification Code is incorrect");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getTransactionCurrencyCode())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD49_CURRENCYCODETRANSACTION);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Currency Code Transaction is incorrect");
            }
        } else if (!ISOUtil.isValidCurrencyCode(authorizationRequestBean.getTransactionCurrencyCode()) || !Validator.validateValue(authorizationRequestBean.getTransactionCurrencyCode(), 15, 3, 3, true, true, AuthorizationConstants.VALID_CHAR[49])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD49_CURRENCYCODETRANSACTION);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Currency Code Transaction is incorrect");
            }
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getOriginalDataElements()) && authorizationRequestBean.getOriginalDataElementsBean() != null) {
            originalDataElements = authorizationRequestBean.getOriginalDataElementsBean().populateOriginalDataElement(authorizationResponseBean);
            authorizationRequestBean.setOriginalDataElements(originalDataElements);
        }

        if (ISOUtil.IsNullOrEmpty(authorizationRequestBean.getOriginalDataElements())) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD56_ORIGINALDATAELEMENTSDATA);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Original Data Elements Data is incorrect");
            }
        } else if (!Validator.validateValue(authorizationRequestBean.getOriginalDataElements(), 8, 35, 1, true, true, AuthorizationConstants.VALID_CHAR[56])) {
            panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD56_ORIGINALDATAELEMENTSDATA);
            authorizationResponseBean.getAuthErrorList().add(panErrorObj);
            if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
                Log.e("Original Data Elements Data is incorrect");
            }
        }

        if (PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON")) {
            Log.i("Exiting validateReversalAdviceRequestType()");
        }

    }

    private boolean isCardholderNotPresent(String posDataCode) {
        boolean isCardholderNotPresent = false;
        String subfieldCardholderPresent = String.valueOf(posDataCode.charAt(4));
        if (subfieldCardholderPresent.equalsIgnoreCase(CardholderPresent.POC_CARDMEMBER_NOT_PRESENT.getCardholderPresent()) || subfieldCardholderPresent.equalsIgnoreCase(CardholderPresent.POC_CARDMEMBER_NOT_PRESENT_MAIL_ORDER.getCardholderPresent()) || subfieldCardholderPresent.equalsIgnoreCase(CardholderPresent.POC_CARDMEMBER_NOT_PRESENT_TELEPHONE.getCardholderPresent()) || subfieldCardholderPresent.equalsIgnoreCase(CardholderPresent.POC_CARDMEMBER_NOT_PRESENT_STANDING_AUTH.getCardholderPresent()) || subfieldCardholderPresent.equalsIgnoreCase(CardholderPresent.POC_CARDMEMBER_NOT_PRESENT_RECURRENT_BILLING.getCardholderPresent()) || subfieldCardholderPresent.equalsIgnoreCase(CardholderPresent.POC_CARDMEMBER_NOT_PRESENT_ELECTRONIC_TRANSACTION_INTERNET.getCardholderPresent())) {
            isCardholderNotPresent = true;
        }

        return isCardholderNotPresent;
    }

    public String createMaskedHexString(AuthorizationResponseBean authorizationResponseBean) throws IPSAPIException {
        String strMaskedHexResponse = null;
        if (authorizationResponseBean != null) {
            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getPrimaryAccountNumber().toString())) {
                this.convertToMaskedHexString(authorizationResponseBean.getPrimaryAccountNumber().toString(), "LL", true, 62, 19, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getProcessingCode())) {
                this.convertToMaskedHexString(authorizationResponseBean.getProcessingCode(), (String)null, false, 61, 6, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getAmountTransaction())) {
                if (authorizationResponseBean.getProcessingCode().equalsIgnoreCase(ProcessingCode.TRANSACTION_FOR_AUTOMATED_ADDRESS_VERIFICATION_ONLY.getProcessingCode())) {
                    authorizationResponseBean.setAmountTransaction("000000000000");
                    this.convertToMaskedHexString(authorizationResponseBean.getAmountTransaction(), (String)null, false, 60, 12, true, false, true, true);
                } else {
                    this.convertToMaskedHexString(authorizationResponseBean.getAmountTransaction().toString(), (String)null, false, 60, 12, true, false, true, true);
                }
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getTransmissionDateAndTime())) {
                this.convertToMaskedHexString(authorizationResponseBean.getTransmissionDateAndTime(), (String)null, false, 57, 10, true, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getSystemTraceAuditNumber())) {
                this.convertToMaskedHexString(authorizationResponseBean.getSystemTraceAuditNumber().toUpperCase(), (String)null, false, 53, 6, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getLocalTransactionDateAndTime())) {
                this.convertToMaskedHexString(authorizationResponseBean.getLocalTransactionDateAndTime(), (String)null, false, 52, 12, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getCardEffectiveDate())) {
                this.convertToMaskedHexString(authorizationResponseBean.getCardEffectiveDate(), (String)null, false, 51, 4, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getCardExpirationDate())) {
                this.convertToMaskedHexString(authorizationResponseBean.getCardExpirationDate(), (String)null, false, 50, 4, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getCardSettlementDate())) {
                this.convertToMaskedHexString(authorizationResponseBean.getCardSettlementDate(), (String)null, false, 49, 6, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getMerchantLocationCountryCode())) {
                this.convertToMaskedHexString(authorizationResponseBean.getMerchantLocationCountryCode(), (String)null, false, 45, 3, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getPosDataCode())) {
                this.convertToMaskedHexString(authorizationResponseBean.getPosDataCode(), (String)null, false, 42, 12, false, true, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getFunctionCode())) {
                this.convertToMaskedHexString(authorizationResponseBean.getFunctionCode(), (String)null, false, 40, 3, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getMessageReasonCode())) {
                this.convertToMaskedHexString(authorizationResponseBean.getMessageReasonCode(), (String)null, false, 39, 4, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getCardAcceptorBusinessCode())) {
                this.convertToMaskedHexString(authorizationResponseBean.getCardAcceptorBusinessCode(), (String)null, false, 38, 4, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getApprovalCodeLength())) {
                this.convertToMaskedHexString(authorizationResponseBean.getApprovalCodeLength(), (String)null, false, 37, 1, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getAmountOriginal())) {
                this.convertToMaskedHexString(authorizationResponseBean.getAmountOriginal(), (String)null, false, 34, 24, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getTransactionId())) {
                this.convertToMaskedHexString(authorizationResponseBean.getTransactionId(), "LL", true, 33, 48, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getAcquiringInstitutionIdCode())) {
                this.convertToMaskedHexString(authorizationResponseBean.getAcquiringInstitutionIdCode(), "LL", true, 32, 11, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getForwardingInstitutionIdCode())) {
                this.convertToMaskedHexString(authorizationResponseBean.getForwardingInstitutionIdCode(), "LL", true, 31, 11, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getTrack2Data())) {
                this.convertToMaskedHexString(authorizationResponseBean.getTrack2Data(), "LL", true, 29, 37, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getRetrievalReferenceNumber())) {
                this.convertToMaskedHexString(authorizationResponseBean.getRetrievalReferenceNumber(), (String)null, false, 27, 12, false, true, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getApprovalCode())) {
                this.convertToMaskedHexString(authorizationResponseBean.getApprovalCode(), (String)null, false, 26, 6, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getCardAcceptorTerminalId())) {
                this.convertToMaskedHexString(authorizationResponseBean.getCardAcceptorTerminalId(), (String)null, false, 23, 8, false, true, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getCardAcceptorIdCode())) {
                this.convertToMaskedHexString(authorizationResponseBean.getCardAcceptorIdCode(), (String)null, false, 22, 15, false, true, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getCardAcceptorNameLocation())) {
                this.convertToMaskedHexString(authorizationResponseBean.getCardAcceptorNameLocation(), "LL", true, 21, 99, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getAdditionalResponseData())) {
                this.convertToMaskedHexString(authorizationResponseBean.getAdditionalResponseData(), "LL", true, 20, 27, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getTrack1Data())) {
                this.convertToMaskedHexString(authorizationResponseBean.getTrack1Data(), "LL", true, 19, 76, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getAdditionalDataNational())) {
                this.convertToMaskedHexString(authorizationResponseBean.getAdditionalDataNational(), "LLL", true, 17, 290, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getAdditionalDataPrivate())) {
                this.convertToMaskedHexString(authorizationResponseBean.getAdditionalDataPrivate(), "LLL", true, 16, 43, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getTransactionCurrencyCode())) {
                this.convertToMaskedHexString(authorizationResponseBean.getTransactionCurrencyCode(), (String)null, false, 15, 3, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getPinData())) {
                this.convertToMaskedHexString(authorizationResponseBean.getPinData(), (String)null, false, 12, 8, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getCardIdentifierCode())) {
                this.convertToMaskedHexString(authorizationResponseBean.getCardIdentifierCode(), "LL", true, 11, 19, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getAmountsAdditional())) {
                this.convertToMaskedHexString(authorizationResponseBean.getAmountsAdditional(), "LLL", true, 10, 123, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getIccRelatedData())) {
                this.calculateIccLength = true;
                this.convertToMaskedHexString(authorizationResponseBean.getIccRelatedData(), "LLL", false, 9, 255, false, false, false, false);
                this.calculateIccLength = false;
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getOriginalDataElements())) {
                this.convertToMaskedHexString(authorizationResponseBean.getOriginalDataElements(), "LL", true, 8, 37, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getNationalUseData1())) {
                this.convertToMaskedHexString(authorizationResponseBean.getNationalUseData1(), "LLL", true, 4, 303, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getNationalUseData2())) {
                this.convertToMaskedHexString(authorizationResponseBean.getNationalUseData2(), "LLL", true, 3, 103, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getValidationInformation())) {
                this.convertToMaskedHexString(authorizationResponseBean.getValidationInformation(), "LLL", true, 2, 63, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getVerificationInformation())) {
                this.convertToMaskedHexString(authorizationResponseBean.getVerificationInformation(), "LLL", true, 1, 208, false, false, true, true);
            }

            if (!ISOUtil.IsNullOrEmpty(authorizationResponseBean.getMessageAuthenticationCode())) {
                long bit = 1L;
                this.primary_header |= bit;
                this.requestParameter.append(authorizationResponseBean.getMessageAuthenticationCode());
            }
        }

        strMaskedHexResponse = this.printHexResponse.toString();
        this.printHexResponse = null;
        return strMaskedHexResponse;
    }

    public void convertToMaskedHexString(String fieldValue, String varType, boolean calculateLength, int bitNumber, int maxLength, boolean isRightJustify, boolean isLeftJustify, boolean encodeLength, boolean encodeValue) {
        if (this.debugflag) {
            Log.i("Entered into setValue() to set value for bit number :: " + (64 - bitNumber));
        }

        int length = fieldValue.length();
        String len = "";
        long bit = 1L;
        String printValue = "";
        this.primary_header |= bit << bitNumber;
        if (calculateLength) {
            len = this.calculateLength(varType, length);
            if (encodeLength) {
                len = this.convertTo(len, "ENC_HEX");
            }

            this.printHexResponse.append(len);
        }

        if (this.calculateIccLength) {
            length = fieldValue.length();
            length /= 2;
            len = this.getRightJustifyString(Integer.toString(length), 3);
            len = this.convertTo(len, "ENC_HEX");
            this.printHexResponse.append(len);
        }

        if (isRightJustify) {
            fieldValue = this.getRightJustifyString(fieldValue, maxLength);
        }

        if (isLeftJustify) {
            fieldValue = this.getLeftJustifyString(fieldValue, maxLength);
        }

        if (encodeValue) {
            fieldValue = this.convertTo(fieldValue, "ENC_HEX");
        }

        if (bitNumber == 62) {
            printValue = this.objUtility.performHEXMasking(fieldValue, false, true, 10, 8);
            this.printHexResponse.append(printValue);
        } else if (bitNumber != 19 && bitNumber != 29 && bitNumber != 12 && bitNumber != 11) {
            this.printHexResponse.append(fieldValue);
        } else {
            printValue = this.objUtility.performHEXMasking(fieldValue, true, false, 0, 0);
            this.printHexResponse.append(printValue);
        }

    }

    private static boolean validateField48Value(String value) {
        boolean isValid = true;
        if (value.length() < 2) {
            isValid = false;
        } else {
            String paymentPlan = value.substring(0, 2);
            if (!paymentPlan.equals("03") && !paymentPlan.equals("05")) {
                isValid = false;
            }
        }

        return isValid;
    }

    private static String getICCAuthResponseDescription(String authResponseCode) {
        String authResponseCodeDesc = "";
        if (!authResponseCode.equals("00") && !authResponseCode.equals("08") && !authResponseCode.equals("10") && !authResponseCode.equals("11")) {
            if (!authResponseCode.equalsIgnoreCase("01") && !authResponseCode.equalsIgnoreCase("02")) {
                if (authResponseCode.equalsIgnoreCase("Z3")) {
                    authResponseCodeDesc = "Unable to go online (Offline Declined)";
                } else if (authResponseCode.equalsIgnoreCase("Y3")) {
                    authResponseCodeDesc = "Unable to go online (Offline Approved)";
                } else {
                    authResponseCodeDesc = "Issuer has declined the transaction";
                }
            } else {
                authResponseCodeDesc = "Issuer Requested Referral”";
            }
        } else {
            authResponseCodeDesc = "Issuer Approved Transaction";
        }

        return authResponseCodeDesc;
    }
}
