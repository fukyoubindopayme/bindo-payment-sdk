package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations;

public enum ReturnBatchOperationSummary {
    No_summary("00"),
    Send_summary("01");

    private final String returnBatchOperationSummary;

    private ReturnBatchOperationSummary(String returnBatchOperationSummary) {
        this.returnBatchOperationSummary = returnBatchOperationSummary;
    }

    public String getReturnBatchOperationSummary() {
        return this.returnBatchOperationSummary;
    }
}
