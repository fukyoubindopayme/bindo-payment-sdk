package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean;

import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.connection.PropertyReader;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.Constants;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.AuthErrorCodes;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.utils.ISOUtil;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.validator.Validator;
import java.util.List;

public class CardAcceptorNameLocationBean
{
  private String subfield1_name;
  private String subfield1_street;
  private String subfield1_city;
  private String subfield1_oilCompany_stationLocationCode;
  private String subfield2_PostalCode;
  private String subfield3_Region;
  private String subfield4_CountryCode;
  private Boolean IsPSPProvider = Boolean.valueOf(false);
  
  public String getSubfield1Name()
  {
    return this.subfield1_name;
  }
  
  public void setSubfield1Name(String subfield1_name)
  {
    this.subfield1_name = subfield1_name;
  }
  
  public String getSubfield1Street()
  {
    return this.subfield1_street;
  }
  
  public void setSubfield1Street(String subfield1_street)
  {
    this.subfield1_street = subfield1_street;
  }
  
  public String getSubfield1City()
  {
    return this.subfield1_city;
  }
  
  public void setSubfield1City(String subfield1_city)
  {
    this.subfield1_city = subfield1_city;
  }
  
  public String getSubfield1OilCompanyStationLocationCode()
  {
    return this.subfield1_oilCompany_stationLocationCode;
  }
  
  public void setSubfield1OilCompanyStationLocationCode(String subfield1_oilCompany_stationLocationCode)
  {
    this.subfield1_oilCompany_stationLocationCode = subfield1_oilCompany_stationLocationCode;
  }
  
  public String getSubfield2PostalCode()
  {
    return this.subfield2_PostalCode;
  }
  
  public void setSubfield2PostalCode(String subfield2_PostalCode)
  {
    this.subfield2_PostalCode = subfield2_PostalCode;
  }
  
  public String getSubfield3Region()
  {
    return this.subfield3_Region;
  }
  
  public void setSubfield3Region(String subfield3_Region)
  {
    this.subfield3_Region = subfield3_Region;
  }
  
  public String getSubfield4CountryCode()
  {
    return this.subfield4_CountryCode;
  }
  
  public void setSubfield4CountryCode(String subfield4_CountryCode)
  {
    this.subfield4_CountryCode = subfield4_CountryCode;
  }
  
  public String populateCardAcceptorNameLocation(AuthorizationResponseBean authorizationResponseBean)
  {
    String subfield1_Name = new String();
    
    StringBuffer displayBuffer = new StringBuffer();
    StringBuffer cardDetails = new StringBuffer();
    if (getSubfield1Name().contains("="))
    {
      setPSPProvider(Boolean.valueOf(true));
      if (!ISOUtil.IsNullOrEmpty(getSubfield1Name()))
      {
        subfield1_Name = getSubfield1Name();
        displayBuffer.append("SUBFIELD_1_NAME                 ").append(" :: ").append(subfield1_Name).append(Constants.NEW_LINE_CHAR);
      }
      if (!ISOUtil.IsNullOrEmpty(getSubfield1OilCompanyStationLocationCode()))
      {
        subfield1_Name = getSubfield1OilCompanyStationLocationCode();
        displayBuffer.append("SUBFIELD_1_CMPNY_STATN_LOC_CODE ").append(" :: ").append(subfield1_Name).append(Constants.NEW_LINE_CHAR);
      }
      StringBuffer subField1 = new StringBuffer();
      
      subField1.append(subfield1_Name).append("\\");
      if (!ISOUtil.IsNullOrEmpty(getSubfield1Street()))
      {
        subField1.append(getSubfield1Street());
        displayBuffer.append("SUBFIELD_1_STREET               ").append(" :: ").append(getSubfield1Street()).append(Constants.NEW_LINE_CHAR);
      }
      subField1.append("\\");
      if (!ISOUtil.IsNullOrEmpty(getSubfield1City()))
      {
        subField1.append(getSubfield1City());
        displayBuffer.append("SUBFIELD_1_CITY                 ").append(" :: ").append(getSubfield1City()).append(Constants.NEW_LINE_CHAR);
      }
      subField1.append("\\");
      if (!ISOUtil.IsNullOrEmpty(subField1.toString()))
      {
        cardDetails.append(subField1);
        if (!Validator.validateSubFields(getSubfield1Name(), 38, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2]))
        {
          ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD434_CARDACCEPTORNAMELOCATION);
          authorizationResponseBean.getAuthErrorList().add(panErrorObj);
        }
        if (!Validator.validateSubFields(getSubfield1Street(), 30, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2]))
        {
          ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD435_CARDACCEPTORNAMELOCATION);
          authorizationResponseBean.getAuthErrorList().add(panErrorObj);
        }
        if (!Validator.validateSubFields(getSubfield1City(), 15, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2]))
        {
          ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD438_CARDACCEPTORNAMELOCATION);
          authorizationResponseBean.getAuthErrorList().add(panErrorObj);
        }
        if ((ISOUtil.IsNullOrEmpty(getSubfield3Region())) || (!Validator.validateSubFields(getSubfield3Region(), 3, 3, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2])))
        {
          ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD432_CARDACCEPTORNAMELOCATION);
          authorizationResponseBean.getAuthErrorList().add(panErrorObj);
        }
        if ((ISOUtil.IsNullOrEmpty(getSubfield4CountryCode())) || (!Validator.validateSubFields(getSubfield4CountryCode(), 3, 3, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2])))
        {
          ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD433_CARDACCEPTORNAMELOCATION);
          authorizationResponseBean.getAuthErrorList().add(panErrorObj);
        }
        if (ISOUtil.IsNullOrEmpty(getSubfield2PostalCode()))
        {
          ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD431_CARDACCEPTORNAMELOCATION);
          authorizationResponseBean.getAuthErrorList().add(panErrorObj);
        }
      }
      addPostalCode(cardDetails, displayBuffer, subfield1_Name, authorizationResponseBean);
      
      cardDetails.append(ISOUtil.padString(getSubfield3Region(), 3, " ", true, false));
      displayBuffer.append("SUBFIELD_3_REGION_CODE          ").append(" :: ").append(ISOUtil.padString(getSubfield3Region(), 10, " ", true, false))
        .append(Constants.NEW_LINE_CHAR);
      
      cardDetails.append(ISOUtil.padString(getSubfield4CountryCode(), 3, " ", true, false));
      displayBuffer.append("SUBFIELD_4_COUNTRY_CODE         ").append(" :: ").append(ISOUtil.padString(getSubfield4CountryCode(), 10, " ", true, false))
        .append(Constants.NEW_LINE_CHAR);
    }
    else if (!ISOUtil.IsNullOrEmpty(getSubfield1OilCompanyStationLocationCode()))
    {
      subfield1_Name = getSubfield1OilCompanyStationLocationCode();
      displayBuffer.append("SUBFIELD_1_CMPNY_STATN_LOC_CODE ").append(" :: ").append(subfield1_Name).append(Constants.NEW_LINE_CHAR);
      
      StringBuffer subField1 = new StringBuffer();
      subField1.append(subfield1_Name).append("\\");
      subField1.append("\\");
      subField1.append("\\");
      
      cardDetails.append(subField1);
      if ((!ISOUtil.IsNullOrEmpty(getSubfield1OilCompanyStationLocationCode())) && (!Validator.validateData(getSubfield1OilCompanyStationLocationCode(), com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2])))
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD440_CARDACCEPTORNAMELOCATION);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
      if (!Validator.validateLength(subField1.toString(), 83, 1))
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD439_CARDACCEPTORNAMELOCATION);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
      if (ISOUtil.IsNullOrEmpty(getSubfield2PostalCode()))
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD431_CARDACCEPTORNAMELOCATION);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
      addPostalCode(cardDetails, displayBuffer, subfield1_Name, authorizationResponseBean);
      
      cardDetails.append("\\");
      cardDetails.append("\\");
    }
    else
    {
      if (!ISOUtil.IsNullOrEmpty(getSubfield1Name()))
      {
        subfield1_Name = getSubfield1Name();
        displayBuffer.append("SUBFIELD_1_NAME                 ").append(" :: ").append(subfield1_Name).append(Constants.NEW_LINE_CHAR);
      }
      StringBuffer subField1 = new StringBuffer();
      
      subField1.append(subfield1_Name).append("\\");
      if (!ISOUtil.IsNullOrEmpty(getSubfield1Street()))
      {
        subField1.append(getSubfield1Street());
        displayBuffer.append("SUBFIELD_1_STREET               ").append(" :: ").append(getSubfield1Street()).append(Constants.NEW_LINE_CHAR);
      }
      subField1.append("\\");
      if (!ISOUtil.IsNullOrEmpty(getSubfield1City()))
      {
        subField1.append(getSubfield1City());
        displayBuffer.append("SUBFIELD_1_CITY                 ").append(" :: ").append(getSubfield1City()).append(Constants.NEW_LINE_CHAR);
      }
      subField1.append("\\");
      if (!ISOUtil.IsNullOrEmpty(subField1.toString()))
      {
        cardDetails.append(subField1);
        if ((!ISOUtil.IsNullOrEmpty(getSubfield1Name())) && (!Validator.validateData(getSubfield1Name(), com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2])))
        {
          ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD434_CARDACCEPTORNAMELOCATION);
          authorizationResponseBean.getAuthErrorList().add(panErrorObj);
        }
        if ((!ISOUtil.IsNullOrEmpty(getSubfield1Street())) && (!Validator.validateData(getSubfield1Street(), com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2])))
        {
          ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD435_CARDACCEPTORNAMELOCATION);
          authorizationResponseBean.getAuthErrorList().add(panErrorObj);
        }
        if ((!ISOUtil.IsNullOrEmpty(getSubfield1City())) && (!Validator.validateData(getSubfield1City(), com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2])))
        {
          ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD438_CARDACCEPTORNAMELOCATION);
          authorizationResponseBean.getAuthErrorList().add(panErrorObj);
        }
        if (!Validator.validateLength(subField1.toString(), 83, 1))
        {
          ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD439_CARDACCEPTORNAMELOCATION);
          authorizationResponseBean.getAuthErrorList().add(panErrorObj);
        }
        if (!Validator.validateSubFields(getSubfield3Region(), 3, 3, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2]))
        {
          ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD432_CARDACCEPTORNAMELOCATION);
          authorizationResponseBean.getAuthErrorList().add(panErrorObj);
        }
        if (!Validator.validateSubFields(getSubfield4CountryCode(), 3, 3, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2]))
        {
          ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD433_CARDACCEPTORNAMELOCATION);
          authorizationResponseBean.getAuthErrorList().add(panErrorObj);
        }
      }
      addPostalCode(cardDetails, displayBuffer, subfield1_Name, authorizationResponseBean);
      if (!ISOUtil.IsNullOrEmpty(getSubfield3Region()))
      {
        cardDetails.append(ISOUtil.padString(getSubfield3Region(), 3, " ", true, false));
        displayBuffer.append("SUBFIELD_3_REGION_CODE          ").append(" :: ").append(ISOUtil.padString(getSubfield3Region(), 10, " ", true, false))
          .append(Constants.NEW_LINE_CHAR);
      }
      else
      {
        cardDetails.append("\\");
      }
      if (!ISOUtil.IsNullOrEmpty(getSubfield4CountryCode()))
      {
        cardDetails.append(ISOUtil.padString(getSubfield4CountryCode(), 3, " ", true, false));
        displayBuffer.append("SUBFIELD_4_COUNTRY_CODE         ").append(" :: ").append(ISOUtil.padString(getSubfield4CountryCode(), 10, " ", true, false))
          .append(Constants.NEW_LINE_CHAR);
      }
      else
      {
        cardDetails.append("\\");
      }
    }
    boolean debugflag = PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON");
    if (debugflag)
    {
      Log.i("Card Acceptor Name Location Bean\r\n----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n-                                 Card Acceptor Name Location 2 Bean                                                                                                                                       -\r\n---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n" +
      
        displayBuffer.toString() + 
        "\r\n" + "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
      
      Log.i("Populated Card Acceptor Name Location is -- " + cardDetails);
    }
    return cardDetails.toString();
  }
  
  private void addPostalCode(StringBuffer pCardDetails, StringBuffer pDisplayBuffer, String pSubfield1_Name, AuthorizationResponseBean pAuthorizationResponseBean)
  {
    if (ISOUtil.IsNullOrEmpty(getSubfield2PostalCode()))
    {
      if ((!ISOUtil.IsNullOrEmpty(pSubfield1_Name)) || (!ISOUtil.IsNullOrEmpty(getSubfield1Street())) || (!ISOUtil.IsNullOrEmpty(getSubfield1City())))
      {
        pCardDetails.append(ISOUtil.padString(getSubfield2PostalCode(), 10, " ", true, false));
        pDisplayBuffer.append("SUBFIELD_2_POST_CODE            ").append(" :: ").append(ISOUtil.padString(getSubfield2PostalCode(), 10, " ", true, false))
          .append(Constants.NEW_LINE_CHAR);
      }
    }
    else if (Validator.validateSubFields(getSubfield2PostalCode(), 10, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2]))
    {
      String postalCode = ISOUtil.padString(getSubfield2PostalCode(), 10, " ", true, false);
      pCardDetails.append(postalCode);
      pDisplayBuffer.append("SUBFIELD_2_POST_CODE            ").append(" :: ").append(postalCode).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD431_CARDACCEPTORNAMELOCATION);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
  }
  
  private void setPSPProvider(Boolean pIsPSPProvider)
  {
    this.IsPSPProvider = pIsPSPProvider;
  }
  
  public boolean isPSPProvider()
  {
    return this.IsPSPProvider.booleanValue();
  }
  
  public String toString()
  {
    StringBuffer buffer = new StringBuffer();
    
    buffer.append("SUBFIELD_1_NAME").append(" :: ").append(getSubfield1Name()).append(Constants.NEW_LINE_CHAR);
    buffer.append("SUBFIELD_1_STREET").append(" :: ").append(getSubfield1Street()).append(Constants.NEW_LINE_CHAR);
    buffer.append("SUBFIELD_1_CITY").append(" :: ").append(getSubfield1City()).append(Constants.NEW_LINE_CHAR);
    buffer.append("SUBFIELD_1_CMPNY_STATN_LOC_CODE").append(" :: ").append(getSubfield1OilCompanyStationLocationCode()).append(Constants.NEW_LINE_CHAR);
    
    buffer.append("SUBFIELD_2_POSTAL_CODE").append(" :: ").append(getSubfield2PostalCode()).append(Constants.NEW_LINE_CHAR);
    buffer.append("SUBFIELD_3_REGION").append(" :: ").append(getSubfield3Region()).append(Constants.NEW_LINE_CHAR);
    buffer.append("SUBFIELD_4_COUNTRY_CODE").append(" :: ").append(getSubfield4CountryCode()).append(Constants.NEW_LINE_CHAR);
    
    return buffer.toString();
  }
}
