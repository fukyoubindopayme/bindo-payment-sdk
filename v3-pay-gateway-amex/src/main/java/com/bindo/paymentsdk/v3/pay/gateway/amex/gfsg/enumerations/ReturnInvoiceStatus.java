package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations;

public enum ReturnInvoiceStatus {
    No("00"),
    Yes("01");

    private final String returnInvoiceStatus;

    private ReturnInvoiceStatus(String returnInvoiceStatus) {
        this.returnInvoiceStatus = returnInvoiceStatus;
    }

    public String getReturnInvoiceStatus() {
        return this.returnInvoiceStatus;
    }
}
