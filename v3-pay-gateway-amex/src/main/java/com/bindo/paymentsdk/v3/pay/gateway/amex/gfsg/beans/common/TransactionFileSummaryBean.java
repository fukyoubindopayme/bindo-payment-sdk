package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.RecordType;

public class TransactionFileSummaryBean
{
  private String recordType = RecordType.Transaction_File_Summary.getRecordType();
  private String recordNumber;
  private String numberOfDebits;
  private String hashTotalDebitAmount;
  private String numberOfCredits;
  private String hashTotalCreditAmount;
  private String hashTotalAmount;
  private String tfsField4reserved;
  private String tfsField7reserved;
  private String tfsField9reserved;
  private String tfsField11reserved;
  
  public String getRecordType()
  {
    return this.recordType;
  }
  
  public void setRecordType(String recordType)
  {
    this.recordType = recordType;
  }
  
  public String getRecordNumber()
  {
    return this.recordNumber;
  }
  
  public void setRecordNumber(String recordNumber)
  {
    this.recordNumber = recordNumber;
  }
  
  public String getNumberOfDebits()
  {
    return this.numberOfDebits;
  }
  
  public void setNumberOfDebits(String numberOfDebits)
  {
    this.numberOfDebits = numberOfDebits;
  }
  
  public String getHashTotalDebitAmount()
  {
    return this.hashTotalDebitAmount;
  }
  
  public void setHashTotalDebitAmount(String hashTotalDebitAmount)
  {
    this.hashTotalDebitAmount = hashTotalDebitAmount;
  }
  
  public String getNumberOfCredits()
  {
    return this.numberOfCredits;
  }
  
  public void setNumberOfCredits(String numberOfCredits)
  {
    this.numberOfCredits = numberOfCredits;
  }
  
  public String getHashTotalCreditAmount()
  {
    return this.hashTotalCreditAmount;
  }
  
  public void setHashTotalCreditAmount(String hashTotalCreditAmount)
  {
    this.hashTotalCreditAmount = hashTotalCreditAmount;
  }
  
  public String getHashTotalAmount()
  {
    return this.hashTotalAmount;
  }
  
  public void setHashTotalAmount(String hashTotalAmount)
  {
    this.hashTotalAmount = hashTotalAmount;
  }
  
  public String getTfsField4reserved()
  {
    return this.tfsField4reserved;
  }
  
  public void setTfsField4reserved(String tfsField4reserved)
  {
    this.tfsField4reserved = tfsField4reserved;
  }
  
  public String getTfsField7reserved()
  {
    return this.tfsField7reserved;
  }
  
  public void setTfsField7reserved(String tfsField7reserved)
  {
    this.tfsField7reserved = tfsField7reserved;
  }
  
  public String getTfsField9reserved()
  {
    return this.tfsField9reserved;
  }
  
  public void setTfsField9reserved(String tfsField9reserved)
  {
    this.tfsField9reserved = tfsField9reserved;
  }
  
  public String getTfsField11reserved()
  {
    return this.tfsField11reserved;
  }
  
  public void setTfsField11reserved(String tfsField11reserved)
  {
    this.tfsField11reserved = tfsField11reserved;
  }
}
