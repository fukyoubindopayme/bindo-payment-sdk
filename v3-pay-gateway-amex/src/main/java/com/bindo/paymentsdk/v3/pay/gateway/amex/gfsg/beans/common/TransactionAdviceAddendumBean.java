package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.RecordType;

public class TransactionAdviceAddendumBean
{
  private String recordType = RecordType.Transaction_Advice_Addendum.getRecordType();
  private String recordNumber;
  private String transactionIdentifier;
  private String formatCode;
  private String addendaTypeCode;
  private String reserved;
  
  public String getRecordType()
  {
    return this.recordType;
  }
  
  public void setRecordType(String recordType)
  {
    this.recordType = recordType;
  }
  
  public String getRecordNumber()
  {
    return this.recordNumber;
  }
  
  public void setRecordNumber(String recordNumber)
  {
    this.recordNumber = recordNumber;
  }
  
  public String getTransactionIdentifier()
  {
    return this.transactionIdentifier;
  }
  
  public void setTransactionIdentifier(String transactionIdentifier)
  {
    this.transactionIdentifier = transactionIdentifier;
  }
  
  public String getFormatCode()
  {
    return this.formatCode;
  }
  
  public void setFormatCode(String formatCode)
  {
    this.formatCode = formatCode;
  }
  
  public String getAddendaTypeCode()
  {
    return this.addendaTypeCode;
  }
  
  public void setAddendaTypeCode(String addendaTypeCode)
  {
    this.addendaTypeCode = addendaTypeCode;
  }
  
  public String getReserved()
  {
    return this.reserved;
  }
  
  public void setReserved(String reserved)
  {
    this.reserved = reserved;
  }
}
