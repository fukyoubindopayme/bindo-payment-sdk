package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.industry;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.ErrorObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceAddendumBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceBasicBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.LodgingIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.SubmissionErrorCodes;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.exceptions.SettlementException;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.CommonValidator;
import java.util.List;

public class LodgingRecordValidator
{
  public static void validateLodgingTypeRec(TransactionAdviceBasicBean transactionAdviceBasicType, TransactionAdviceAddendumBean transactionAddendumType, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    LodgingIndustryTAABean lodgingIndustryTAABean = (LodgingIndustryTAABean)transactionAddendumType;
    
    TAADBRecordValidator.validateRecordType(lodgingIndustryTAABean
      .getRecordType(), errorCodes);
    TAADBRecordValidator.validateRecordNumber(lodgingIndustryTAABean
      .getRecordNumber(), errorCodes);
    TAADBRecordValidator.validateTransactionIdentifier(
      lodgingIndustryTAABean.getTransactionIdentifier(), 
      transactionAdviceBasicType, errorCodes);
    TAADBRecordValidator.validateAddendaTypeCode(lodgingIndustryTAABean
      .getAddendaTypeCode(), errorCodes);
    TAADBRecordValidator.validateFormatCode(lodgingIndustryTAABean
      .getFormatCode(), transactionAdviceBasicType, errorCodes);
    
    validateLodgingSpecialProgramCode(lodgingIndustryTAABean
      .getLodgingSpecialProgramCode(), errorCodes);
    validateLodgingCheckInDate(lodgingIndustryTAABean
      .getLodgingCheckInDate(), errorCodes);
    validateLodgingCheckOutDate(lodgingIndustryTAABean, errorCodes);
    validateLodgingRoomRate1(lodgingIndustryTAABean.getLodgingRoomRate1(), 
      errorCodes);
    validateNumberOfNightsAtRoomRate1(lodgingIndustryTAABean
      .getNumberOfNightsAtRoomRate1(), errorCodes);
    validateLodgingRoomRate2(lodgingIndustryTAABean.getLodgingRoomRate2(), 
      errorCodes);
    validateNumberOfNightsAtRoomRate2(lodgingIndustryTAABean
      .getNumberOfNightsAtRoomRate2(), errorCodes);
    validateLodgingRoomRate3(lodgingIndustryTAABean.getLodgingRoomRate3(), 
      errorCodes);
    validateNumberOfNightsAtRoomRate3(lodgingIndustryTAABean
      .getNumberOfNightsAtRoomRate3(), errorCodes);
    validateLodgingRenterName(
      lodgingIndustryTAABean.getLodgingRenterName(), errorCodes);
    validateLodgingFolioNumber(lodgingIndustryTAABean
      .getLodgingFolioNumber(), errorCodes);
  }
  
  private static void validateLodgingSpecialProgramCode(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD71_LODGINGSPECIALPROGRAM_CODE);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[72]))
    {
      String reqLength = CommonValidator.validateLength(value, 1, 1);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD71_LODGINGSPECIALPROGRAM_CODE);
        errorCodes.add(errorObj);
      }
      else if ((Integer.parseInt(value) != 1) && 
        (Integer.parseInt(value) != 2) && 
        (Integer.parseInt(value) != 3))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD71_LODGINGSPECIALPROGRAM_CODE);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD71_LODGINGSPECIALPROGRAM_CODE);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateLodgingCheckInDate(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD72_LODGING_CHECK_INDATE);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[73]))
    {
      String reqLength = CommonValidator.validateLength(value, 8, 8);
      if ((!reqLength.equals("equal")) || 
      
        (!CommonValidator.isValidDate(value, "CCYYMMDD")))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD72_LODGING_CHECK_INDATE);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD72_LODGING_CHECK_INDATE);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateLodgingCheckOutDate(LodgingIndustryTAABean lodgingIndustryTAABean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(lodgingIndustryTAABean.getLodgingCheckOutDate()))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD73_LODGING_CHECK_OUTDATE);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(lodgingIndustryTAABean.getLodgingCheckOutDate(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[74]))
    {
      String reqLength = CommonValidator.validateLength(
        lodgingIndustryTAABean.getLodgingCheckOutDate(), 8, 8);
      if (reqLength.equals("equal")) {
        if (CommonValidator.isValidDate(lodgingIndustryTAABean.getLodgingCheckOutDate(), "CCYYMMDD"))
        {
          if (Integer.parseInt(lodgingIndustryTAABean.getLodgingCheckOutDate()) >= Integer.parseInt(lodgingIndustryTAABean
            .getLodgingCheckInDate())) {
            return;
          }
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD74_LODGING_CHECK_OUTDATE);
          errorCodes.add(errorObj);
          
          return;
        }
      }
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD73_LODGING_CHECK_OUTDATE);
      errorCodes.add(errorObj);
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD73_LODGING_CHECK_OUTDATE);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateLodgingRoomRate1(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[76]))
      {
        String reqLength = 
          CommonValidator.validateLength(value, 12, 12);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD76_LODGING_ROOMRATE1);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD75_LODGING_ROOMRATE1);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateNumberOfNightsAtRoomRate1(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[77]))
      {
        String reqLength = CommonValidator.validateLength(value, 2, 2);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD77_NUMBEROF_NIGHTSATROOMRATE1);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD77_NUMBEROF_NIGHTSATROOMRATE1);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateLodgingRoomRate2(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[79]))
      {
        String reqLength = 
          CommonValidator.validateLength(value, 12, 12);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD78_LODGING_ROOMRATE2);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD79_LODGING_ROOMRATE2);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateLodgingRoomRate3(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[80]))
      {
        String reqLength = 
          CommonValidator.validateLength(value, 12, 12);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD81_LODGING_ROOMRATE3);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD82_LODGING_ROOMRATE3);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateNumberOfNightsAtRoomRate2(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[82]))
      {
        String reqLength = CommonValidator.validateLength(value, 2, 2);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD80_NUMBEROF_NIGHTSATROOMRATE2);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD80_NUMBEROF_NIGHTSATROOMRATE2);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateNumberOfNightsAtRoomRate3(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[83]))
      {
        String reqLength = CommonValidator.validateLength(value, 2, 2);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD83_NUMBEROF_NIGHTSATROOMRATE3);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD83_NUMBEROF_NIGHTSATROOMRATE3);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateLodgingRenterName(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 26, 26);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD84_LODGINGRENTER_NAME);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateLodgingFolioNumber(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 12, 12);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD85_LODGINGFOLIO_NUMBER);
        errorCodes.add(errorObj);
      }
    }
  }
}
