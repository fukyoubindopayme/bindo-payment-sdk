package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.ErrorObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceAddendumBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceBasicBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceDetailBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.IndustryTypeTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.LocationDetailTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.NonIndustrySpecificTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.connection.PropertyReader;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.CountryCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.ElectronicCommerceIndicator;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.FormatCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.ProcessingCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.RecordType;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.SubmissionErrorCodes;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.SubmissionMethod;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.pos.CardDataInputMode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.pos.CardPresent;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.exceptions.SettlementException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

public class TABRecValidator
{
  private static String recordNumber;
  private static String recordType;
  static TransactionAdviceBasicBean transactionAdviceBasicType;
  private static final Logger LOGGER = Logger.getLogger(TABRecValidator.class);
  private static boolean debugFlag;
  
  public static void validateTABRecordType(TransactionAdviceBasicBean transactAdviceBasicType, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON"))
    {
      debugFlag = true;
      LOGGER.info("Entered into validateTABRecordType()");
    }
    recordNumber = transactAdviceBasicType.getRecordNumber();
    recordType = transactAdviceBasicType.getRecordType();
    boolean isPaperSubmitter = false;
    if (!CommonValidator.isNullOrEmpty(transactAdviceBasicType.getSubmissionMethod())) {
      if (SubmissionMethod.Paper_external_vendor_paper_processor.getSubmissionMethod().equals(transactAdviceBasicType.getSubmissionMethod())) {
        isPaperSubmitter = true;
      }
    }
    validateRecordType(transactAdviceBasicType.getRecordType(), errorCodes);
    validateRecordNumber(transactAdviceBasicType.getRecordNumber(), 
      errorCodes);
    validateTransactionIdentifier(
      transactAdviceBasicType.getTransactionIdentifier(), 
      isPaperSubmitter, errorCodes);
    validateFormatCode(transactAdviceBasicType, errorCodes);
    validateMediaCode(transactAdviceBasicType.getMediaCode(), errorCodes);
    validateSubmissionMethod(transactAdviceBasicType.getSubmissionMethod(), 
      errorCodes);
    validateApprovalCode(transactAdviceBasicType, errorCodes);
    validatePrimaryAccountNumber(
      transactAdviceBasicType.getPrimaryAccountNumber(), errorCodes);
    validateCardExpiryDate(transactAdviceBasicType.getCardExpiryDate(), 
      errorCodes);
    validateTransactionDate(transactAdviceBasicType.getTransactionDate(), 
      errorCodes);
    validateTransactionTime(transactAdviceBasicType.getTransactionTime(), 
      errorCodes);
    validateTransactionAmount(transactAdviceBasicType, errorCodes);
    validateProcessingCode(transactAdviceBasicType.getProcessingCode(), 
      errorCodes, transactAdviceBasicType.getLocationDetailTAABean()
      .getLocationCountryCode());
    validateTransactionCurrencyCode(
      transactAdviceBasicType.getTransactionCurrencyCode(), 
      errorCodes);
    validateExtendedPaymentData(
      transactAdviceBasicType.getExtendedPaymentData(), errorCodes);
    validateMerchantId(transactAdviceBasicType.getMerchantId(), errorCodes);
    validateMerchantLocationId(
      transactAdviceBasicType.getMerchantLocationId(), errorCodes);
    validateMerchantContactInfo(
      transactAdviceBasicType.getMerchantContactInfo(), errorCodes);
    validateTerminalId(transactAdviceBasicType.getTerminalId(), errorCodes);
    validatePosDataCode(transactAdviceBasicType.getPosDataCode(), 
      isPaperSubmitter, errorCodes);
    validateInvoiceReferenceNumber(
      transactAdviceBasicType.getInvoiceReferenceNumber(), errorCodes);
    validateTabImageSequenceNumber(
      transactAdviceBasicType.getTabImageSequenceNumber(), 
      isPaperSubmitter, errorCodes);
    validateMatchingKeyType(transactAdviceBasicType.getMatchingKeyType(), 
      errorCodes);
    validateMatchingKey(transactAdviceBasicType.getMatchingKey(), 
      errorCodes);
    validateElectronicCommerceIndicator(transactAdviceBasicType.getElectronicCommerceIndicator(), 
      errorCodes);
    
    String posDataCode = transactAdviceBasicType.getPosDataCode();
    List<String> posDataList = getPOSDataArray(posDataCode);
    if ((ElectronicCommerceIndicator.Payment_Token_Data_Present.getElectronicCommerceIndicator().equals(transactAdviceBasicType.getElectronicCommerceIndicator())) && 
      (CardPresent.RESERVE_FOR_PRIVATE_USE_Z.getCardPresent().equals(((String)posDataList.get(5)).toString())) && 
      (!CardDataInputMode.POC_INTEGRATED_CKT_CARD.getCardDataInputMode().equals(((String)posDataList.get(6)).toString())))
    {
      ErrorObject errorObj = null;
      errorObj = new ErrorObject(SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE.getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE.getErrorDescription());
      errorCodes.add(errorObj);
    }
    if (transactAdviceBasicType.getLocationDetailTAABean() == null) {
      transactAdviceBasicType.setLocationDetailTAABean(new LocationDetailTAABean());
    }
    LocationTAARecValidator.validateLocationRecordType(
      transactAdviceBasicType, errorCodes);
    if ((transactAdviceBasicType.getTransactionAdviceDetailBean() instanceof TransactionAdviceDetailBean)) {
      TADRecValidator.validateTADRecord(transactAdviceBasicType, 
        errorCodes);
    }
    ArrayList<TransactionAdviceAddendumBean> taaBeanList = transactAdviceBasicType
      .getArrayTAABean();
    if ((taaBeanList != null) && (!taaBeanList.isEmpty())) {
      for (TransactionAdviceAddendumBean taaBean : taaBeanList)
      {
        if ((taaBean instanceof NonIndustrySpecificTAABean)) {
          NonIndustryTypeRecValidator.validateNonIndustryRecordType(
            transactAdviceBasicType, taaBean, errorCodes);
        }
        if ((taaBean instanceof IndustryTypeTAABean)) {
          IndustryTypeRecValidator.validateIndustryRecordType(
            transactAdviceBasicType, taaBean, errorCodes);
        }
      }
    }
    if (debugFlag) {
      LOGGER.info("Exiting from validateTABRecordType()");
    }
  }
  
  private static void validateRecordType(String recType, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObject = null;
    if (CommonValidator.isNullOrEmpty(recType))
    {
      errorObject = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE
        .getErrorDescription() + 
        "\n" + 
        "TFH" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "RecordType:" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObject);
    }
    else if (!RecordType.Transaction_Advice_Basic.getRecordType().equalsIgnoreCase(recType))
    {
      errorObject = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE
        .getErrorDescription());
      errorCodes.add(errorObject);
    }
  }
  
  private static void validateRecordNumber(String recordNo, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(recordNo))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD585_RECORDNUMBER
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD585_RECORDNUMBER
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "RecordNumber:" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(recordNo, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[11]))
    {
      String reqLength = CommonValidator.validateLength(recordNo, 8, 
        8);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD3_RECORDNUMBER
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD3_RECORDNUMBER
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "RecordNumber:" + 
          "|" + 
          "This field length Cannot be greater than 8");
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD2_RECORDNUMBER
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD2_RECORDNUMBER
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "RecordNumber:" + 
        "|" + 
        "This field can only be Numeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateTransactionIdentifier(String transactionIdentifier, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(transactionIdentifier))
    {
      if (!isPaperSubmitter)
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "TransactionIdentifier" + 
          "|" + 
          "This field is mandatory and cannot be empty");
        errorCodes.add(errorObj);
      }
    }
    else if (CommonValidator.validateData(transactionIdentifier, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[23]))
    {
      String reqLength = CommonValidator.validateLength(
        transactionIdentifier, 15, 15);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "TransactionIdentifier" + 
          "|" + 
          "This field length Cannot be greater than 15");
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "TransactionIdentifier" + 
        "|" + 
        "This field can only be Numeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateFormatCode(TransactionAdviceBasicBean transactAdviceBasicType, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(transactAdviceBasicType.getFormatCode()))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD28_FORMAT_CODE
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD28_FORMAT_CODE
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "FormatCode" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(transactAdviceBasicType.getFormatCode(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[24]))
    {
      String reqLength = CommonValidator.validateLength(
        transactAdviceBasicType.getFormatCode(), 2, 2);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD28_FORMAT_CODE
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD28_FORMAT_CODE
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "FormatCode" + 
          "|" + 
          "This field length Cannot be greater than 2");
        errorCodes.add(errorObj);
      }
      else if (CommonValidator.isValidFormatCode(transactAdviceBasicType.getFormatCode()))
      {
        if (transactAdviceBasicType.getFormatCode().equals(FormatCode.No_Industry_Specific_TAA_Record.getFormatCode())) {
          if ((transactAdviceBasicType.getTransactionAdviceAddendumBean() instanceof IndustryTypeTAABean))
          {
            errorObj = new ErrorObject(
              SubmissionErrorCodes.ERROR_DATAFIELD26_FORMAT_CODE
              .getErrorCode(), 
              SubmissionErrorCodes.ERROR_DATAFIELD26_FORMAT_CODE
              .getErrorDescription() + 
              "\n" + 
              "RecordType:" + 
              recordType + 
              "|" + 
              "RecordNumber:" + 
              recordNumber + 
              "|" + 
              "FormatCode" + 
              "|" + 
              SubmissionErrorCodes.ERROR_DATAFIELD26_FORMAT_CODE
              .getErrorDescription());
            errorCodes.add(errorObj);
          }
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD28_FORMAT_CODE
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD28_FORMAT_CODE
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "FormatCode" + 
          "|" + 
          SubmissionErrorCodes.ERROR_DATAFIELD28_FORMAT_CODE
          .getErrorDescription());
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD28_FORMAT_CODE
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD28_FORMAT_CODE
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "FormatCode" + 
        "|" + 
        "This field can only be AlphaNumeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateMediaCode(String mediaCode, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(mediaCode)) {
      if (CommonValidator.validateData(mediaCode, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[25]))
      {
        String reqLength = CommonValidator.validateLength(mediaCode, 2, 
          2);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD29_MEDIA_CODE
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD29_MEDIA_CODE
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            recordType + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "MediaCode" + 
            "|" + 
            "This field length Cannot be greater than 2");
          errorCodes.add(errorObj);
        }
        else if (!CommonValidator.isValidMediaCode(mediaCode))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD29_MEDIA_CODE
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD29_MEDIA_CODE
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            recordType + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "MediaCode" + 
            "|" + 
            SubmissionErrorCodes.ERROR_DATAFIELD29_MEDIA_CODE
            .getErrorDescription());
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD29_MEDIA_CODE
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD29_MEDIA_CODE
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "MediaCode" + 
          "|" + 
          "This field can only be AlphaNumeric");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateSubmissionMethod(String submissionMethod, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(submissionMethod)) {
      if (CommonValidator.validateData(submissionMethod, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[26]))
      {
        String reqLength = CommonValidator.validateLength(
          submissionMethod, 2, 2);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD30_SUBMISSION_METHOD
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD30_SUBMISSION_METHOD
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            recordType + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "SubmissionMethod" + 
            "|" + 
            "This field length Cannot be greater than 2");
          errorCodes.add(errorObj);
        }
        else if (!CommonValidator.isValidSubCode(submissionMethod))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD30_SUBMISSION_METHOD
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD30_SUBMISSION_METHOD
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            recordType + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "SubmissionMethod" + 
            "|" + 
            SubmissionErrorCodes.ERROR_DATAFIELD30_SUBMISSION_METHOD
            .getErrorDescription());
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD30_SUBMISSION_METHOD
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD30_SUBMISSION_METHOD
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "SubmissionMethod" + 
          "|" + 
          "This field can only be AlphaNumeric");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateApprovalCode(TransactionAdviceBasicBean transactAdviceBasicType, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    String approvalCode = transactAdviceBasicType.getApprovalCode();
    if (CommonValidator.isNullOrEmpty(approvalCode))
    {
      if (!transactAdviceBasicType.getProcessingCode().equalsIgnoreCase(ProcessingCode.CREDIT.getProcessingCode()))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD32_APPROVAL_CODE
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD32_APPROVAL_CODE
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "ApprovalCode" + 
          "|" + 
          "This field is mandatory and cannot be empty");
        errorCodes.add(errorObj);
      }
    }
    else if (CommonValidator.validateData(approvalCode, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[28]))
    {
      String reqLength = CommonValidator.validateLength(approvalCode, 
        6, 6);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD32_APPROVAL_CODE
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD32_APPROVAL_CODE
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "ApprovalCode" + 
          "|" + 
          "This field length Cannot be greater than 6");
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD31_APPROVAL_CODE
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD31_APPROVAL_CODE
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "ApprovalCode" + 
        "|" + 
        "This field can only be AlphaNumeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validatePrimaryAccountNumber(String primaryAccountNo, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(primaryAccountNo))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD35_PRIMARYAC_NUMBER
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD35_PRIMARYAC_NUMBER
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "PrimaryAccountNumber" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(primaryAccountNo, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[29]))
    {
      String reqLength = CommonValidator.validateLength(
        primaryAccountNo, 19, 19);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
        
          SubmissionErrorCodes.ERROR_DATAFIELD602_PRIMARYAC_NUMBER
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD602_PRIMARYAC_NUMBER
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "PrimaryAccountNumber" + 
          "|" + 
          "This field length Cannot be greater than 19");
        errorCodes.add(errorObj);
      }
      else if (!CommonValidator.isValidModulusTenCheck(primaryAccountNo))
      {
        errorObj = new ErrorObject(
        
          SubmissionErrorCodes.ERROR_DATAFIELD603_PRIMARYAC_NUMBER
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD603_PRIMARYAC_NUMBER
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "PrimaryAccountNumber" + 
          "|" + 
          SubmissionErrorCodes.ERROR_DATAFIELD603_PRIMARYAC_NUMBER
          .getErrorDescription());
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
      
        SubmissionErrorCodes.ERROR_DATAFIELD601_PRIMARYAC_NUMBER
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD601_PRIMARYAC_NUMBER
        
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "PrimaryAccountNumber" + 
        "|" + 
        "This field can only be AlphaNumeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateCardExpiryDate(String cardExpiryDate, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(cardExpiryDate)) {
      if (CommonValidator.validateData(cardExpiryDate, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[30]))
      {
        String reqLength = CommonValidator.validateLength(
          cardExpiryDate, 4, 4);
        if ((!reqLength.equals("equal")) || 
        
          (!CommonValidator.isValidDate(cardExpiryDate, "YYMM")))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD37_CARDEXPIRY_DATE
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD37_CARDEXPIRY_DATE
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            recordType + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "CardExpiryDate" + 
            "|" + 
            SubmissionErrorCodes.ERROR_DATAFIELD37_CARDEXPIRY_DATE
            .getErrorDescription());
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD37_CARDEXPIRY_DATE
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD37_CARDEXPIRY_DATE
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "CardExpiryDate" + 
          "|" + 
          "This field can only be Numeric");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateTransactionDate(String transactionDate, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(transactionDate))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD38_TRANSACTION_DATE
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD38_TRANSACTION_DATE
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "TransactionDate" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(transactionDate, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[31]))
    {
      String reqLength = CommonValidator.validateLength(
        transactionDate, 8, 8);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD38_TRANSACTION_DATE
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD38_TRANSACTION_DATE
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "TransactionDate" + 
          "|" + 
          "This field length Cannot be greater than 8");
        errorCodes.add(errorObj);
      }
      else if (CommonValidator.isValidDate(transactionDate, "CCYYMMDD"))
      {
        String dateStatus = 
          CommonValidator.validateTransactionDate(transactionDate);
        if (dateStatus.equals("expired"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD40_TRANSACTION_DATE);
          errorCodes.add(errorObj);
        }
        else if (dateStatus.equals("futureDate"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD39_TRANSACTION_DATE);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD38_TRANSACTION_DATE);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD38_TRANSACTION_DATE
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD38_TRANSACTION_DATE
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "TransactionDate" + 
        "|" + 
        "This field can only be Numeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateTransactionTime(String transactionTime, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(transactionTime)) {
      if (CommonValidator.validateData(transactionTime, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[32]))
      {
        String reqLength = CommonValidator.validateLength(
          transactionTime, 6, 6);
        if ((!reqLength.equals("equal")) || 
          (!CommonValidator.validateTime(transactionTime)))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD41_TRANSACTION_TIME
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD41_TRANSACTION_TIME
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            recordType + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "TransactionTime" + 
            "|" + 
            "This field length should be 6");
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD41_TRANSACTION_TIME
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD41_TRANSACTION_TIME
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "TransactionTime" + 
          "|" + 
          "This field can only be Numeric");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateTransactionAmount(TransactionAdviceBasicBean transactAdviceBasicType, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    String value = transactAdviceBasicType.getTransactionAmount();
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD42_TRANSACTION_AMOUNT
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD42_TRANSACTION_AMOUNT
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "TransactionAmount" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[34]))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 12, 12);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD42_TRANSACTION_AMOUNT
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD42_TRANSACTION_AMOUNT
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "TransactionAmount" + 
          "|" + 
          "This field length Cannot be greater than 12");
        errorCodes.add(errorObj);
      }
      else if (!CommonValidator.isValidAmount(transactAdviceBasicType.getTransactionCurrencyCode(), value))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD43_TRANSACTION_AMOUNT
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD43_TRANSACTION_AMOUNT
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "TransactionAmount" + 
          "|" + 
          SubmissionErrorCodes.ERROR_DATAFIELD43_TRANSACTION_AMOUNT
          .getErrorDescription());
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD42_TRANSACTION_AMOUNT
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD42_TRANSACTION_AMOUNT
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "TransactionAmount" + 
        "|" + 
        "This field can only be Numeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateProcessingCode(String processingCode, List<ErrorObject> errorCodes, String location)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (((processingCode.equals(ProcessingCode.BONUS_PAY_DEBIT.getProcessingCode())) || (processingCode.equals(ProcessingCode.BONUS_PAY_CREDIT.getProcessingCode())) || 
      (processingCode.equals(ProcessingCode.BONUS_PAY_CREDIT_REVERSAL.getProcessingCode())) || (processingCode.equals(ProcessingCode.BONUS_PAY_DEBIT_REVERSAL.getProcessingCode()))) && 
      (!location.equals(CountryCode.Japan.getCountryCode())))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD57_MERCHANT_DATA
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD57_MERCHANT_DATA
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "ProcessingCode" + 
        "|" + 
        "This Processing Code is Only Valid For Japan");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.isNullOrEmpty(processingCode))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD45_PROSESSING_CODE
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD45_PROSESSING_CODE
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "ProcessingCode" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(processingCode, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[35]))
    {
      String reqLength = CommonValidator.validateLength(
        processingCode, 6, 6);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD45_PROSESSING_CODE
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD45_PROSESSING_CODE
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "ProcessingCode" + 
          "|" + 
          "This field length Cannot be greater than 6");
        errorCodes.add(errorObj);
      }
      else if (!CommonValidator.isValidProcessingCode(processingCode))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD45_PROSESSING_CODE
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD45_PROSESSING_CODE
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "ProcessingCode" + 
          "|" + 
          SubmissionErrorCodes.ERROR_DATAFIELD45_PROSESSING_CODE
          .getErrorDescription());
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD45_PROSESSING_CODE
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD45_PROSESSING_CODE
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "ProcessingCode" + 
        "|" + 
        "This field can only be Numeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateTransactionCurrencyCode(String transactionCurrencyCode, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(transactionCurrencyCode))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD46_CURRENCY_CODE
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD46_CURRENCY_CODE
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "TransactionCurrencyCode" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(transactionCurrencyCode, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[36]))
    {
      String reqLength = CommonValidator.validateLength(
        transactionCurrencyCode, 3, 3);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD46_CURRENCY_CODE
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD46_CURRENCY_CODE
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "TransactionCurrencyCode" + 
          "|" + 
          "This field length Cannot be greater than 3");
        errorCodes.add(errorObj);
      }
      else if (CommonValidator.isProhibitedCurrencyCode(transactionCurrencyCode))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD47_CURRENCY_CODE
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD47_CURRENCY_CODE
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "TransactionCurrencyCode" + 
          "|" + 
          SubmissionErrorCodes.ERROR_DATAFIELD47_CURRENCY_CODE
          .getErrorDescription());
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD46_CURRENCY_CODE
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD46_CURRENCY_CODE
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "TransactionCurrencyCode" + 
        "|" + 
        "This field can only be AlphaNumeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateExtendedPaymentData(String extendedPaymentData, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(extendedPaymentData))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD49_EXTENDEDPAYMENT_DATA
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD49_EXTENDEDPAYMENT_DATA
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "ExtendedPaymentData" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(extendedPaymentData, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[37]))
    {
      String reqLength = CommonValidator.validateLength(
        extendedPaymentData, 2, 2);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD49_EXTENDEDPAYMENT_DATA
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD49_EXTENDEDPAYMENT_DATA
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "ExtendedPaymentData" + 
          "|" + 
          "This field length Cannot be greater than 2");
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD49_EXTENDEDPAYMENT_DATA
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD49_EXTENDEDPAYMENT_DATA
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "ExtendedPaymentData" + 
        "|" + 
        "This field can only be Numeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateMerchantId(String merchantID, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(merchantID))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD52_MERCHANT_DATA
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD52_MERCHANT_DATA
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "MerchantId" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(merchantID, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[38]))
    {
      String reqLength = CommonValidator.validateLength(merchantID, 
        15, 15);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD51_MERCHANT_DATA
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD51_MERCHANT_DATA
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "MerchantId" + 
          "|" + 
          "This field length Cannot be greater than 15");
        errorCodes.add(errorObj);
      }
      else if (!CommonValidator.isValidModulusNineCheck(merchantID))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD51_MERCHANT_DATA
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD51_MERCHANT_DATA
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "MerchantId" + 
          "|" + 
          SubmissionErrorCodes.ERROR_DATAFIELD51_MERCHANT_DATA
          .getErrorDescription());
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD51_MERCHANT_DATA
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD51_MERCHANT_DATA
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "MerchantId" + 
        "|" + 
        "This field can only be AlphaNumeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateMerchantLocationId(String merchantLocationID, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(merchantLocationID))
    {
      String reqLength = CommonValidator.validateLength(
        merchantLocationID, 15, 15);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD596_MERCHANT_LOCATIONID
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD596_MERCHANT_LOCATIONID
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "MerchantLocationId" + 
          "|" + 
          "This field length Cannot be greater than 15");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateMerchantContactInfo(String merchantContactInfo, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(merchantContactInfo))
    {
      String reqLength = CommonValidator.validateLength(
        merchantContactInfo, 40, 40);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD596_MERCHANT_CONTACTINFO
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD596_MERCHANT_CONTACTINFO
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "MerchantContactInfo" + 
          "|" + 
          "This field length Cannot be greater than 40");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateTerminalId(String terminalID, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(terminalID))
    {
      String reqLength = CommonValidator.validateLength(terminalID, 8, 8);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD596_TERMINAL_ID
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD596_TERMINAL_ID
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "Terminal Id" + 
          "|" + 
          "This field length Cannot be greater than 8");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validatePosDataCode(String posDataCode, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(posDataCode))
    {
      if (!isPaperSubmitter)
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "PosDataCode" + 
          "|" + 
          "This field is mandatory and cannot be empty");
        errorCodes.add(errorObj);
      }
    }
    else if (CommonValidator.validateData(posDataCode, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[42]))
    {
      String reqLength = CommonValidator.validateLength(posDataCode, 
        12, 12);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "PosDataCode" + 
          "|" + 
          "This field length Cannot be greater than 12");
        errorCodes.add(errorObj);
      }
      else
      {
        List<String> posList = getPOSDataArray(posDataCode);
        String cardDataInputCapability = ((String)posList.get(0)).toString();
        String cardholderAuthCapability = ((String)posList.get(1)).toString();
        String cardCaptureCapability = ((String)posList.get(2)).toString();
        String operatingEnv = ((String)posList.get(3)).toString();
        String cardholderPresent = ((String)posList.get(4)).toString();
        String cardPresent = ((String)posList.get(5)).toString();
        String cardDataInputMode = ((String)posList.get(6)).toString();
        String cardmemberAuthMethod = ((String)posList.get(7)).toString();
        String cardmemberAuthEntity = ((String)posList.get(8)).toString();
        String cardDataOutputCapability = ((String)posList.get(9)).toString();
        String terminalOutputCapability = ((String)posList.get(10))
          .toString();
        String pinCaptureCapability = ((String)posList.get(11)).toString();
        if (!POSDataValidator.validateCardDataInputCapability(cardDataInputCapability))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
            .getErrorDescription());
          errorCodes.add(errorObj);
        }
        else if (!POSDataValidator.validateCardholderAuthCapability(cardholderAuthCapability))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
            .getErrorDescription());
          errorCodes.add(errorObj);
        }
        else if (!POSDataValidator.validateCardCaptureCapability(cardCaptureCapability))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
            .getErrorDescription());
          errorCodes.add(errorObj);
        }
        else if (!POSDataValidator.validateOperatingEnv(operatingEnv))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
            .getErrorDescription());
          errorCodes.add(errorObj);
        }
        else if (!POSDataValidator.validateCardholderPresent(cardholderPresent))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
            .getErrorDescription());
          errorCodes.add(errorObj);
        }
        else if (!POSDataValidator.validateCardPresent(cardPresent))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
            .getErrorDescription());
          errorCodes.add(errorObj);
        }
        else if (!POSDataValidator.validateCardDataInputMode(cardDataInputMode))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
            .getErrorDescription());
          errorCodes.add(errorObj);
        }
        else if (!POSDataValidator.validateCardmemberAuthMethod(cardmemberAuthMethod))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
            .getErrorDescription());
          errorCodes.add(errorObj);
        }
        else if (!POSDataValidator.validateCardmemberAuthEntity(cardmemberAuthEntity))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
            .getErrorDescription());
          errorCodes.add(errorObj);
        }
        else if (!POSDataValidator.validateCardDataOutputCapability(cardDataOutputCapability))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
            .getErrorDescription());
          errorCodes.add(errorObj);
        }
        else if (!POSDataValidator.validateTerminalOutputCapability(terminalOutputCapability))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
            .getErrorDescription());
          errorCodes.add(errorObj);
        }
        else if (!POSDataValidator.validatePinCaptureCapability(pinCaptureCapability))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
            .getErrorDescription());
          errorCodes.add(errorObj);
        }
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD57_POSDATA_CODE
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "PosDataCode" + 
        "|" + 
        "This field can only be AlphaNumeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateInvoiceReferenceNumber(String invoiceReferenceNumber, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(invoiceReferenceNumber))
    {
      String reqLength = CommonValidator.validateLength(
        invoiceReferenceNumber, 30, 30);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD596_INVSSRVSNO
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD596_INVSSRVSNO
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "Invoice Service Number" + 
          "|" + 
          "This field length Cannot be greater than 30");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateTabImageSequenceNumber(String imageSequenceNumber, boolean isPaperSubmitter, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(imageSequenceNumber))
    {
      if (isPaperSubmitter)
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD59_TABIMGSQNC_NUMBER
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD59_TABIMGSQNC_NUMBER
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "TabImageSequenceNumber" + 
          "|" + 
          "This field is mandatory and cannot be empty");
        errorCodes.add(errorObj);
      }
    }
    else if (CommonValidator.validateData(imageSequenceNumber, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[48]))
    {
      String reqLength = CommonValidator.validateLength(
        imageSequenceNumber, 8, 8);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD58_TABIMGSQNC_NUMBER
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD58_TABIMGSQNC_NUMBER
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "TabImageSequenceNumber" + 
          "|" + 
          "This field length Cannot be greater than 8");
        errorCodes.add(errorObj);
      }
      else if (reqLength.equals("lessThanMin"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD58_TABIMGSQNC_NUMBER
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD58_TABIMGSQNC_NUMBER
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "TabImageSequenceNumber" + 
          "|" + 
          "This field length Cannot be less than 8");
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD58_TABIMGSQNC_NUMBER
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD58_TABIMGSQNC_NUMBER
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "TabImageSequenceNumber" + 
        "|" + 
        "This field can only be AlphaNumeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateMatchingKeyType(String matchingKeyType, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(matchingKeyType)) {
      if (CommonValidator.validateData(matchingKeyType, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[49]))
      {
        String reqLength = CommonValidator.validateLength(
          matchingKeyType, 2, 2);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD597_MATCHKEY_TYPE
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD597_MATCHKEY_TYPE
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            recordType + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "MatchingKeyType" + 
            "|" + 
            "This field length Cannot be greater than 2");
          errorCodes.add(errorObj);
        }
        else if (!"01".equalsIgnoreCase(matchingKeyType))
        {
          if (!"02".equalsIgnoreCase(matchingKeyType)) {
            if (!"99".equalsIgnoreCase(matchingKeyType)) {
              if (!"03".equalsIgnoreCase(matchingKeyType)) {
                if (!"04".equalsIgnoreCase(matchingKeyType))
                {
                  errorObj = new ErrorObject(
                    SubmissionErrorCodes.ERROR_DATAFIELD597_MATCHKEY_TYPE
                    .getErrorCode(), 
                    SubmissionErrorCodes.ERROR_DATAFIELD597_MATCHKEY_TYPE
                    .getErrorDescription() + 
                    "\n" + 
                    "RecordType:" + 
                    recordType + 
                    "|" + 
                    "RecordNumber:" + 
                    recordNumber + 
                    "|" + 
                    "MatchingKeyType" + 
                    "|" + 
                    SubmissionErrorCodes.ERROR_DATAFIELD597_MATCHKEY_TYPE
                    .getErrorDescription());
                  errorCodes.add(errorObj);
                }
              }
            }
          }
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD597_MATCHKEY_TYPE
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD597_MATCHKEY_TYPE
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "MatchingKeyType" + 
          "|" + 
          "This field can only be AlphaNumeric");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateMatchingKey(String matchingKey, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(matchingKey)) {
      if (CommonValidator.validateData(matchingKey, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[50]))
      {
        String reqLength = CommonValidator.validateLength(matchingKey, 
          21, 21);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD597_MATCHKEY
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD597_MATCHKEY
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            recordType + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "MatchingKey" + 
            "|" + 
            "This field length Cannot be greater than 21");
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD597_MATCHKEY
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD597_MATCHKEY
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "MatchingKey" + 
          "|" + 
          "This field can only be AlphaNumeric");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateElectronicCommerceIndicator(String electronicCommerceIndicator, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(electronicCommerceIndicator)) {
      if (CommonValidator.validateData(electronicCommerceIndicator, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[51]))
      {
        String reqLength = CommonValidator.validateLength(
          electronicCommerceIndicator, 2, 2);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD598_ELECOMRCIND
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD598_ELECOMRCIND
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            recordType + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "ElectronicCommerceIndicator" + 
            "|" + 
            "This field length Cannot be greater than 2");
          errorCodes.add(errorObj);
        }
        else if (!ElectronicCommerceIndicator.Authenticated.getElectronicCommerceIndicator().equalsIgnoreCase(electronicCommerceIndicator))
        {
          if (!ElectronicCommerceIndicator.Attempted.getElectronicCommerceIndicator().equalsIgnoreCase(electronicCommerceIndicator)) {
            if (!ElectronicCommerceIndicator.Not_authenticated.getElectronicCommerceIndicator().equalsIgnoreCase(electronicCommerceIndicator)) {
              if (!ElectronicCommerceIndicator.Payment_Token_Data_Present.getElectronicCommerceIndicator().equalsIgnoreCase(electronicCommerceIndicator))
              {
                errorObj = new ErrorObject(
                  SubmissionErrorCodes.ERROR_DATAFIELD598_ELECOMRCIND
                  .getErrorCode(), 
                  SubmissionErrorCodes.ERROR_DATAFIELD598_ELECOMRCIND
                  .getErrorDescription() + 
                  "\n" + 
                  "RecordType:" + 
                  recordType + 
                  "|" + 
                  "RecordNumber:" + 
                  recordNumber + 
                  "|" + 
                  "ElectronicCommerceIndicator" + 
                  "|" + 
                  SubmissionErrorCodes.ERROR_DATAFIELD598_ELECOMRCIND
                  .getErrorDescription());
                errorCodes.add(errorObj);
              }
            }
          }
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD598_ELECOMRCIND
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD598_ELECOMRCIND
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "ElectronicCommerceIndicator" + 
          "|" + 
          "This field can only be AlphaNumeric");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static List<String> getPOSDataArray(String posDataCode)
  {
    List<String> listPOSData = new ArrayList();
    for (int iPosition = 0; iPosition < posDataCode.length(); iPosition++)
    {
      String posValue = posDataCode.substring(iPosition, iPosition + 1);
      listPOSData.add(posValue);
    }
    return listPOSData;
  }
}
