package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.utils;

import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.connection.PropertyReader;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

public final class Utility
{
  static boolean debugflag = PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON");
  ISOUtil objISOUtil = new ISOUtil();
  public static StringBuffer trackTrace = new StringBuffer("");
  static final String HEXES = "0123456789ABCDEF";
  
  /* Error */
  public static void debug(String str, boolean appendToFile)
  {
    // Byte code:
    //   0: ldc 47
    //   2: astore_2
    //   3: aconst_null
    //   4: astore_3
    //   5: invokestatic 68	com/americanexpress/ips/connection/PropertyReader:getLOG_FILE_PATH	()Ljava/lang/String;
    //   8: ifnull +14 -> 22
    //   11: invokestatic 68	com/americanexpress/ips/connection/PropertyReader:getLOG_FILE_PATH	()Ljava/lang/String;
    //   14: ldc 47
    //   16: invokevirtual 71	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   19: ifeq +13 -> 32
    //   22: new 75	com/americanexpress/ips/exception/IPSAPIException
    //   25: dup
    //   26: ldc 77
    //   28: invokespecial 79	com/americanexpress/ips/exception/IPSAPIException:<init>	(Ljava/lang/String;)V
    //   31: athrow
    //   32: invokestatic 68	com/americanexpress/ips/connection/PropertyReader:getLOG_FILE_PATH	()Ljava/lang/String;
    //   35: astore_2
    //   36: iload_1
    //   37: ifeq +23 -> 60
    //   40: new 80	java/io/PrintWriter
    //   43: dup
    //   44: new 82	java/io/FileWriter
    //   47: dup
    //   48: aload_2
    //   49: iconst_1
    //   50: invokespecial 84	java/io/FileWriter:<init>	(Ljava/lang/String;Z)V
    //   53: invokespecial 86	java/io/PrintWriter:<init>	(Ljava/io/Writer;)V
    //   56: astore_3
    //   57: goto +19 -> 76
    //   60: new 80	java/io/PrintWriter
    //   63: dup
    //   64: new 82	java/io/FileWriter
    //   67: dup
    //   68: aload_2
    //   69: invokespecial 89	java/io/FileWriter:<init>	(Ljava/lang/String;)V
    //   72: invokespecial 86	java/io/PrintWriter:<init>	(Ljava/io/Writer;)V
    //   75: astore_3
    //   76: aload_3
    //   77: invokestatic 90	com/americanexpress/ips/utils/Utility:getLocalDateTime	()Ljava/lang/String;
    //   80: invokevirtual 93	java/io/PrintWriter:println	(Ljava/lang/String;)V
    //   83: iload_1
    //   84: ifeq +23 -> 107
    //   87: aload_3
    //   88: new 96	java/lang/StringBuilder
    //   91: dup
    //   92: ldc 98
    //   94: invokespecial 100	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   97: aload_0
    //   98: invokevirtual 101	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   101: invokevirtual 105	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   104: invokevirtual 93	java/io/PrintWriter:println	(Ljava/lang/String;)V
    //   107: aload_3
    //   108: invokevirtual 108	java/io/PrintWriter:flush	()V
    //   111: goto +100 -> 211
    //   114: astore 4
    //   116: getstatic 35	com/americanexpress/ips/utils/Utility:debugflag	Z
    //   119: ifeq +26 -> 145
    //   122: getstatic 43	com/americanexpress/ips/utils/Utility:LOGGER	Lorg/apache/log4j/Logger;
    //   125: new 96	java/lang/StringBuilder
    //   128: dup
    //   129: ldc 111
    //   131: invokespecial 100	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   134: aload 4
    //   136: invokevirtual 113	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   139: invokevirtual 105	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   142: invokevirtual 116	org/apache/log4j/Logger:fatal	(Ljava/lang/Object;)V
    //   145: aload_3
    //   146: ifnull +73 -> 219
    //   149: aload_3
    //   150: invokevirtual 120	java/io/PrintWriter:close	()V
    //   153: goto +66 -> 219
    //   156: astore 4
    //   158: getstatic 35	com/americanexpress/ips/utils/Utility:debugflag	Z
    //   161: ifeq +26 -> 187
    //   164: getstatic 43	com/americanexpress/ips/utils/Utility:LOGGER	Lorg/apache/log4j/Logger;
    //   167: new 96	java/lang/StringBuilder
    //   170: dup
    //   171: ldc 123
    //   173: invokespecial 100	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   176: aload 4
    //   178: invokevirtual 113	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   181: invokevirtual 105	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   184: invokevirtual 116	org/apache/log4j/Logger:fatal	(Ljava/lang/Object;)V
    //   187: aload_3
    //   188: ifnull +31 -> 219
    //   191: aload_3
    //   192: invokevirtual 120	java/io/PrintWriter:close	()V
    //   195: goto +24 -> 219
    //   198: astore 5
    //   200: aload_3
    //   201: ifnull +7 -> 208
    //   204: aload_3
    //   205: invokevirtual 120	java/io/PrintWriter:close	()V
    //   208: aload 5
    //   210: athrow
    //   211: aload_3
    //   212: ifnull +7 -> 219
    //   215: aload_3
    //   216: invokevirtual 120	java/io/PrintWriter:close	()V
    //   219: return
    // Line number table:
    //   Java source line #43	-> byte code offset #0
    //   Java source line #44	-> byte code offset #3
    //   Java source line #47	-> byte code offset #5
    //   Java source line #48	-> byte code offset #22
    //   Java source line #51	-> byte code offset #32
    //   Java source line #53	-> byte code offset #36
    //   Java source line #56	-> byte code offset #40
    //   Java source line #58	-> byte code offset #57
    //   Java source line #60	-> byte code offset #60
    //   Java source line #63	-> byte code offset #76
    //   Java source line #65	-> byte code offset #83
    //   Java source line #66	-> byte code offset #87
    //   Java source line #70	-> byte code offset #107
    //   Java source line #71	-> byte code offset #111
    //   Java source line #72	-> byte code offset #116
    //   Java source line #73	-> byte code offset #122
    //   Java source line #82	-> byte code offset #145
    //   Java source line #83	-> byte code offset #149
    //   Java source line #76	-> byte code offset #156
    //   Java source line #77	-> byte code offset #158
    //   Java source line #78	-> byte code offset #164
    //   Java source line #82	-> byte code offset #187
    //   Java source line #83	-> byte code offset #191
    //   Java source line #80	-> byte code offset #198
    //   Java source line #82	-> byte code offset #200
    //   Java source line #83	-> byte code offset #204
    //   Java source line #85	-> byte code offset #208
    //   Java source line #82	-> byte code offset #211
    //   Java source line #83	-> byte code offset #215
    //   Java source line #86	-> byte code offset #219
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	220	0	str	String
    //   0	220	1	appendToFile	boolean
    //   2	67	2	logFilePath	String
    //   4	212	3	printWriter	java.io.PrintWriter
    //   114	21	4	e	com.americanexpress.ips.exception.IPSAPIException
    //   156	21	4	e	IOException
    //   198	11	5	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   5	111	114	com/americanexpress/ips/exception/IPSAPIException
    //   5	111	156	java/io/IOException
    //   5	145	198	finally
    //   156	187	198	finally
  }
  
  public static String getPropertyValue(String propertyName)
  {
    Properties props = new Properties();
    String popertyValue = "";
    try
    {
      URL url = Utility.class
        .getResource("/authorization.properties");
      
      props.load(url.openStream());
      popertyValue = props.getProperty(propertyName);
      if (popertyValue == null) {
        return null;
      }
    }
    catch (IOException e)
    {
      if (debugflag) {
        Log.e("Error in getPropertyValue() method..  IOException: " + e);
      }
    }
    return popertyValue;
  }
  
  public static String now(String dateFormat)
  {
    Calendar cal = Calendar.getInstance();
    SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
    return sdf.format(cal.getTime());
  }
  
  public static String getLocalDateTime()
  {
    return now("yyyy-MM-dd,hh:mm:ss");
  }
  
  public String performMasking(String value, boolean isAll, boolean isNotAll, int startIndex, int endIndex)
  {
    String temp = "";
    String temp2 = "";
    String temp3 = "";
    if (isAll)
    {
      value = this.objISOUtil.getMasked(value.length(), "X");
    }
    else if (isNotAll)
    {
      if (startIndex < value.length())
      {
        temp = value.substring(0, startIndex);
        temp2 = value.substring(startIndex, value.length() - endIndex);
        
        temp2 = this.objISOUtil.getMasked(temp2.length(), "X");
        
        temp3 = value.substring(temp.length() + temp2.length(), temp.length() + temp2.length() + endIndex);
      }
      value = temp + temp2 + temp3;
    }
    return value;
  }
  
  public static String getHexMessage(byte[] raw)
  {
    if (raw == null) {
      return null;
    }
    StringBuilder hex = new StringBuilder(2 * raw.length);
    byte[] arrayOfByte = raw;int j = raw.length;
    for (int i = 0; i < j; i++)
    {
      byte b = arrayOfByte[i];
      hex.append("0123456789ABCDEF".charAt((b & 0xF0) >> 4))
        .append("0123456789ABCDEF".charAt(b & 0xF));
    }
    return hex.toString();
  }
  
  public String performHEXMasking(String value, boolean isAll, boolean isNotAll, int startIndex, int endIndex)
  {
    String temp = "";
    String temp2 = "";
    String temp3 = "";
    if (isAll)
    {
      value = this.objISOUtil.getPositionMasked(value.length());
    }
    else if (isNotAll)
    {
      if (startIndex < value.length())
      {
        temp = value.substring(0, startIndex);
        temp2 = value.substring(startIndex, value.length() - endIndex);
        
        temp2 = this.objISOUtil.getPositionMasked(temp2.length());
        
        temp3 = value.substring(temp.length() + temp2.length(), temp.length() + temp2.length() + endIndex);
      }
      value = temp + temp2 + temp3;
    }
    return value;
  }
  
  public String noValueToBlank(String value, int bitNum)
  {
    String temp = "";
    if (bitNum == 21)
    {
      if (value.length() == 5) {
        temp = " ";
      }
    }
    else {
      temp = " ";
    }
    return temp;
  }
}
