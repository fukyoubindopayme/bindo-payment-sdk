package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean;

import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.connection.PropertyReader;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.Constants;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.AuthErrorCodes;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.utils.ISOUtil;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.validator.Validator;

public class KeyManagementDataBean
{
  private String primaryId = "AX";
  private String secondaryID = "KCV";
  private String sessionKeyPinCheckValue;
  private String sessionKeyMacCheckValue;
  private String sessionKeyDataCheckValue;
  private String sessionKeyPin;
  private String sessionKeyMac;
  private String sessionKeyData;
  
  public String getPrimaryId()
  {
    return this.primaryId;
  }
  
  public void setPrimaryId(String primaryId)
  {
    this.primaryId = primaryId;
  }
  
  public String getSecondaryID()
  {
    return this.secondaryID;
  }
  
  public void setSecondaryID(String secondaryID)
  {
    this.secondaryID = secondaryID;
  }
  
  public String getSessionKeyPinCheckValue()
  {
    return this.sessionKeyPinCheckValue;
  }
  
  public void setSessionKeyPinCheckValue(String sessionkeypincheckvalue)
  {
    this.sessionKeyPinCheckValue = sessionkeypincheckvalue;
  }
  
  public String getSessionKeyMacCheckValue()
  {
    return this.sessionKeyMacCheckValue;
  }
  
  public void setSessionKeyMacCheckValue(String sessionkeymaccheckvalue)
  {
    this.sessionKeyMacCheckValue = sessionkeymaccheckvalue;
  }
  
  public String getSessionKeyDataCheckValue()
  {
    return this.sessionKeyDataCheckValue;
  }
  
  public void setSessionKeyDataCheckValue(String sessionkeydatacheckvalue)
  {
    this.sessionKeyDataCheckValue = sessionkeydatacheckvalue;
  }
  
  public String getSessionKeyPin()
  {
    return this.sessionKeyPin;
  }
  
  public void setSessionKeyPin(String sessionKeyPin)
  {
    this.sessionKeyPin = sessionKeyPin;
  }
  
  public String getSessionKeyMac()
  {
    return this.sessionKeyMac;
  }
  
  public void setSessionKeyMac(String sessionKeyMac)
  {
    this.sessionKeyMac = sessionKeyMac;
  }
  
  public String getSessionKeyData()
  {
    return this.sessionKeyData;
  }
  
  public void setSessionKeyData(String sessionKeyData)
  {
    this.sessionKeyData = sessionKeyData;
  }
  
  public String populateKeyManagementDataBean(AuthorizationResponseBean pAuthorizationResponseBean)
  {
    boolean debugflag = PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON");
    
    StringBuffer lKeyManagementData = new StringBuffer();
    StringBuffer lDisplayBuffer = new StringBuffer();
    if (ISOUtil.IsNullOrEmpty(getPrimaryId()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD96_KEYMANAGEMENTDATAPRIMARYID);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if (!"AX".equalsIgnoreCase(getPrimaryId()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD96_KEYMANAGEMENTDATAPRIMARYID);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else
    {
      lKeyManagementData.append(getPrimaryId());
      lDisplayBuffer.append("PRIMARY_ID                     ").append(" :: ").append(getPrimaryId()).append(Constants.NEW_LINE_CHAR);
    }
    if (ISOUtil.IsNullOrEmpty(getSecondaryID()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD96_KEYMANAGEMENTDATASECONDARYID);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if (!"KCV".equalsIgnoreCase(getSecondaryID()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD96_KEYMANAGEMENTDATASECONDARYID);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else
    {
      lKeyManagementData.append(getSecondaryID());
      lDisplayBuffer.append("SECONDARY_ID                   ").append(" :: ").append(getSecondaryID()).append(Constants.NEW_LINE_CHAR);
    }
    if (ISOUtil.IsNullOrEmpty(getSessionKeyPinCheckValue()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD96_KEYMANAGEMENTDATASESSIONKEYPINCHECKVALUE);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if (!Validator.validateSubFields(getSessionKeyPinCheckValue(), 6, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[6]))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD96_KEYMANAGEMENTDATASESSIONKEYPINCHECKVALUE);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else
    {
      lKeyManagementData.append(getSessionKeyPinCheckValue());
      lDisplayBuffer.append("SESSION_KEY_PIN_CHECK_VALUE    ").append(" :: ").append(getSessionKeyPinCheckValue()).append(Constants.NEW_LINE_CHAR);
    }
    if (ISOUtil.IsNullOrEmpty(getSessionKeyMacCheckValue()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD96_KEYMANAGEMENTDATASESSIONKEYMACCHECKVALUE);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if (!"000000".equalsIgnoreCase(getSessionKeyMacCheckValue()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD96_KEYMANAGEMENTDATASESSIONKEYMACCHECKVALUE);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else
    {
      lKeyManagementData.append(getSessionKeyMacCheckValue());
      lDisplayBuffer.append("SESSION_KEY_MAC_CHECK_VALUE    ").append(" :: ").append(getSessionKeyMacCheckValue()).append(Constants.NEW_LINE_CHAR);
    }
    if (ISOUtil.IsNullOrEmpty(getSessionKeyDataCheckValue()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD96_KEYMANAGEMENTDATASESSIONKEYDATACHECKVALUE);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if (!"000000".equalsIgnoreCase(getSessionKeyMacCheckValue()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD96_KEYMANAGEMENTDATASESSIONKEYDATACHECKVALUE);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else
    {
      lKeyManagementData.append(getSessionKeyDataCheckValue());
      lDisplayBuffer.append("SESSION_KEY_DATA_CHECK_VALUE   ").append(" :: ").append(getSessionKeyDataCheckValue()).append(Constants.NEW_LINE_CHAR);
    }
    if (debugflag)
    {
      Log.i("Key Management Data Bean\r\n----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n-                                 Key Management Data Bean                                                                                                                                       -\r\n---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n" +
      
        lDisplayBuffer.toString() + 
        "\r\n" + "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
      
      Log.i("Populated Key Management Data is -- " + lKeyManagementData);
    }
    return lKeyManagementData.toString();
  }
  
  public String toString()
  {
    StringBuffer lKeyMgmtData = new StringBuffer();
    
    lKeyMgmtData.append("PRIMARY ID").append(" :: ").append(getPrimaryId()).append(Constants.NEW_LINE_CHAR);
    lKeyMgmtData.append("SECONDARY ID").append(" :: ").append(getSecondaryID()).append(Constants.NEW_LINE_CHAR);
    if (!ISOUtil.IsNullOrEmpty(getSessionKeyPin())) {
      lKeyMgmtData.append("SESSION PIN KEY").append(" :: ").append(getSessionKeyPin()).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getSessionKeyMac())) {
      lKeyMgmtData.append("SESSION MAC KEY").append(" :: ").append(getSessionKeyMac()).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getSessionKeyData())) {
      lKeyMgmtData.append("SESSION DATA KEY").append(" :: ").append(getSessionKeyData()).append(Constants.NEW_LINE_CHAR);
    }
    lKeyMgmtData.append("SESSION PIN KEY CHECK VALUE").append(" :: ").append(getSessionKeyPinCheckValue()).append(Constants.NEW_LINE_CHAR);
    lKeyMgmtData.append("SESSION MAC KEY CHECK VALUE").append(" :: ").append(getSessionKeyMacCheckValue()).append(Constants.NEW_LINE_CHAR);
    lKeyMgmtData.append("SESSION DATA KEY CHECK VALUE").append(" :: ").append(getSessionKeyDataCheckValue()).append(Constants.NEW_LINE_CHAR);
    
    return lKeyMgmtData.toString();
  }
}
