package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.formats;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.IndustryTypeTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.AirlineIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.AutoRentalIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.CommunicationServicesIndustryBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.EntertainmentTicketingIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.InsuranceIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.LodgingIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.RailIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.RetailIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.TravelCruiseIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.connection.PropertyReader;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.CommonValidator;
import java.io.IOException;
import java.util.List;
import org.apache.log4j.Logger;

public final class IndustryTAARecordFormatter
{
  private static final Logger LOGGER = Logger.getLogger(IndustryTAARecordFormatter.class);
  
  public static void formatIndustrySpecificTypeTAARecord(IndustryTypeTAABean industryTypeTAABean, List<String> formatedMessageList)
    throws IOException
  {
    if ((industryTypeTAABean instanceof AirlineIndustryTAABean))
    {
      AirlineIndustryTAABean airlineIndustryTAABean = (AirlineIndustryTAABean)industryTypeTAABean;
      formatAirlineIndustryTypeRecord(airlineIndustryTAABean, 
        formatedMessageList);
    }
    else if ((industryTypeTAABean instanceof AutoRentalIndustryTAABean))
    {
      AutoRentalIndustryTAABean autoRentalIndustryTAABean = (AutoRentalIndustryTAABean)industryTypeTAABean;
      formatAutoRentalIndustryTypeRecord(autoRentalIndustryTAABean, 
        formatedMessageList);
    }
    else if ((industryTypeTAABean instanceof CommunicationServicesIndustryBean))
    {
      CommunicationServicesIndustryBean communicationServicesIndustryBean = (CommunicationServicesIndustryBean)industryTypeTAABean;
      formatCommunicationServicesIndustryTypeRecord(
        communicationServicesIndustryBean, formatedMessageList);
    }
    else if ((industryTypeTAABean instanceof EntertainmentTicketingIndustryTAABean))
    {
      EntertainmentTicketingIndustryTAABean entertainmentTicketingIndustryTAABean = (EntertainmentTicketingIndustryTAABean)industryTypeTAABean;
      formatEntertainmentTicketingIndustryTypeRecord(
        entertainmentTicketingIndustryTAABean, formatedMessageList);
    }
    else if ((industryTypeTAABean instanceof InsuranceIndustryTAABean))
    {
      InsuranceIndustryTAABean insuranceIndustryTAABean = (InsuranceIndustryTAABean)industryTypeTAABean;
      formatInsuranceIndustryTypeRecord(insuranceIndustryTAABean, 
        formatedMessageList);
    }
    else if ((industryTypeTAABean instanceof LodgingIndustryTAABean))
    {
      LodgingIndustryTAABean lodgingIndustryTAABean = (LodgingIndustryTAABean)industryTypeTAABean;
      formatLodgingTypeRecord(lodgingIndustryTAABean, formatedMessageList);
    }
    else if ((industryTypeTAABean instanceof RailIndustryTAABean))
    {
      RailIndustryTAABean railIndustryTAABean = (RailIndustryTAABean)industryTypeTAABean;
      formatRailIndustryTypeRecord(railIndustryTAABean, 
        formatedMessageList);
    }
    else if ((industryTypeTAABean instanceof RetailIndustryTAABean))
    {
      RetailIndustryTAABean retailIndustryTAABean = (RetailIndustryTAABean)industryTypeTAABean;
      formatRetailIndustryTypeRecord(retailIndustryTAABean, 
        formatedMessageList);
    }
    else if ((industryTypeTAABean instanceof TravelCruiseIndustryTAABean))
    {
      TravelCruiseIndustryTAABean travelCruiseIndustryTAABean = (TravelCruiseIndustryTAABean)industryTypeTAABean;
      formatTravelCruiseIndustryTypeRecord(travelCruiseIndustryTAABean, 
        formatedMessageList);
    }
  }
  
  public static void formatLodgingTypeRecord(LodgingIndustryTAABean lodgingIndustryTAABean, List<String> formatedMessageList)
    throws IOException
  {
    if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
      LOGGER.info("Entered in to formatLodgingTypeRecord()");
    }
    StringBuffer formattedMessage = new StringBuffer();
    
    SettlementMessageFormatter.formatValue(lodgingIndustryTAABean
      .getRecordType(), formattedMessage, 3, false, true, true, 
      false, true);
    
    SettlementMessageFormatter.formatValue(lodgingIndustryTAABean
      .getRecordNumber(), formattedMessage, 8, true, false, false, 
      true, false);
    
    SettlementMessageFormatter.formatValue(lodgingIndustryTAABean
      .getTransactionIdentifier(), formattedMessage, 15, true, 
      false, false, true, false);
    
    SettlementMessageFormatter.formatValue(lodgingIndustryTAABean
      .getFormatCode(), formattedMessage, 2, false, true, false, 
      false, true);
    
    SettlementMessageFormatter.formatValue(lodgingIndustryTAABean
      .getAddendaTypeCode(), formattedMessage, 2, false, true, 
      false, false, true);
    if (CommonValidator.isNullOrEmpty(lodgingIndustryTAABean.getLodgingSpecialProgramCode())) {
      lodgingIndustryTAABean.setLodgingSpecialProgramCode("1");
    }
    SettlementMessageFormatter.formatValue(lodgingIndustryTAABean
      .getLodgingSpecialProgramCode(), formattedMessage, 1, true, 
      false, false, true, false);
    
    SettlementMessageFormatter.formatValue(lodgingIndustryTAABean
      .getLodgingCheckInDate(), formattedMessage, 8, true, false, 
      false, true, false);
    
    SettlementMessageFormatter.formatValue(lodgingIndustryTAABean
      .getLodgingCheckOutDate(), formattedMessage, 8, true, false, 
      false, true, false);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(lodgingIndustryTAABean
      .getLodgingRoomRate1(), formattedMessage, 12, true, false, 
      false, true, false);
    
    SettlementMessageFormatter.formatValue(lodgingIndustryTAABean
      .getNumberOfNightsAtRoomRate1(), formattedMessage, 2, true,
      false, false, true, false);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(lodgingIndustryTAABean
      .getLodgingRoomRate2(), formattedMessage, 12, true, false, 
      false, true, false);
    
    SettlementMessageFormatter.formatValue(lodgingIndustryTAABean
      .getNumberOfNightsAtRoomRate2(), formattedMessage, 2, true, 
      false, false, true, false);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(lodgingIndustryTAABean
      .getLodgingRoomRate3(), formattedMessage, 12, true, false, 
      false, true, false);
    
    SettlementMessageFormatter.formatValue(lodgingIndustryTAABean
      .getNumberOfNightsAtRoomRate3(), formattedMessage, 2, true, 
      false, false, true, false);
    
    SettlementMessageFormatter.formatValue(lodgingIndustryTAABean
      .getLodgingRenterName(), formattedMessage, 26, false, true, 
      true, false, true);
    
    SettlementMessageFormatter.formatValue(lodgingIndustryTAABean
      .getLodgingFolioNumber(), formattedMessage, 12, false, true, 
      true, false, true);
    if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
      SettlementMessageFormatter.characterSpacesFill(formattedMessage, 564);
    }
    formatedMessageList.add(formattedMessage.toString());
    if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
      LOGGER.info("Formated Lodging Industry Type Record :" + formattedMessage.toString());
    }
  }
  
  public static void formatAirlineIndustryTypeRecord(AirlineIndustryTAABean airlineIndustryTAABean, List<String> formatedMessageList)
    throws IOException
  {
    StringBuffer formattedMessage = new StringBuffer();
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getRecordType(), formattedMessage, 3, false, true, true, 
      false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getRecordNumber(), formattedMessage, 8, true, false, false, 
      true, false);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getTransactionIdentifier(), formattedMessage, 15, true, 
      false, false, true, false);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getFormatCode(), formattedMessage, 2, false, true, false, 
      false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getAddendaTypeCode(), formattedMessage, 2, false, true, 
      false, false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getTransactionType(), formattedMessage, 3, false, true, true, 
      false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getTicketNumber(), formattedMessage, 14, false, true, true, 
      false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getDocumentType(), formattedMessage, 2, false, true, false, 
      false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getAirlineProcessIdentifier(), formattedMessage, 3, false, 
      true, true, false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getIataNumericCode(), formattedMessage, 8, false, true, true, 
      false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getTicketingCarrierName(), formattedMessage, 25, false, true, 
      true, false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getTicketIssueCity(), formattedMessage, 18, false, true, true, 
      false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getTicketIssueDate(), formattedMessage, 8, true, false, 
      false, true, false);
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getNumberInParty(), formattedMessage, 3, true, false, false, 
      true, false);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getPassengerName(), formattedMessage, 25, false, true, true, 
      false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getConjunctionTicketIndicator(), formattedMessage, 1, false, 
      true, false, false, true);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getOriginalTransactionAmount(), formattedMessage, 12, true, 
      false, false, true, false);
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getOriginalCurrencyCode(), formattedMessage, 3, false, true, 
      false, false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getElectronicTicketIndicator(), formattedMessage, 1, false, 
      true, false, false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getTotalNumberOfAirSegments(), formattedMessage, 1, true, 
      false, false, true, false);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getStopoverIndicator1(), formattedMessage, 1, false, true, 
      false, false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getDepartureLocationCodeSegment1(), formattedMessage, 3, 
      false, true, false, false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getDepartureDateSegment1(), formattedMessage, 8, false, true, 
      false, true, false);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getArrivalLocationCodeSegment1(), formattedMessage, 3, false, 
      true, false, false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getSegmentCarrierCode1(), formattedMessage, 2, false, true, 
      false, false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getSegment1FareBasis(), formattedMessage, 15, false, true, 
      true, false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getClassOfServiceCodeSegment1(), formattedMessage, 2, false, 
      true, false, false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getFlightNumberSegment1(), formattedMessage, 4, false, true, 
      true, false, true);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getSegment1Fare(), formattedMessage, 12, true, false, false, 
      true, false);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getStopoverIndicator2(), formattedMessage, 1, false, true, 
      false, false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getDepartureLocationCodeSegment2(), formattedMessage, 3, 
      false, true, false, false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getDepartureDateSegment2(), formattedMessage, 8, true, false, 
      false, true, false);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getArrivalLocationCodeSegment2(), formattedMessage, 3, false, 
      true, false, false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getSegmentCarrierCode2(), formattedMessage, 2, false, true, 
      false, false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getSegment2FareBasis(), formattedMessage, 15, false, true, 
      true, false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getClassOfServiceCodeSegment2(), formattedMessage, 2, false, 
      true, false, false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getFlightNumberSegment2(), formattedMessage, 4, false, true, 
      true, false, true);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getSegment2Fare(), formattedMessage, 12, true, false, false, 
      true, false);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getStopoverIndicator3(), formattedMessage, 1, false, true, 
      false, false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getDepartureLocationCodeSegment3(), formattedMessage, 3, 
      false, true, false, false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getDepartureDateSegment3(), formattedMessage, 8, true, false, 
      false, true, false);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getArrivalLocationCodeSegment3(), formattedMessage, 3, false, 
      true, false, false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getSegmentCarrierCode3(), formattedMessage, 2, false, true, 
      false, false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getSegment3FareBasis(), formattedMessage, 15, false, true, 
      true, false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getClassOfServiceCodeSegment3(), formattedMessage, 2, false, 
      true, false, false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getFlightNumberSegment3(), formattedMessage, 4, false, true, 
      true, false, true);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getSegment3Fare(), formattedMessage, 12, true, false, false, 
      true, false);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getStopoverIndicator4(), formattedMessage, 1, false, true, 
      false, false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getDepartureLocationCodeSegment4(), formattedMessage, 3, 
      false, true, false, false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getDepartureDateSegment4(), formattedMessage, 8, true, false, 
      false, true, false);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getArrivalLocationCodeSegment4(), formattedMessage, 3, false, 
      true, false, false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getSegmentCarrierCode4(), formattedMessage, 2, false, true, 
      false, false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getSegment4FareBasis(), formattedMessage, 15, false, true, 
      true, false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getClassOfServiceCodeSegment4(), formattedMessage, 2, false, 
      true, false, false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getFlightNumberSegment4(), formattedMessage, 4, false, true, 
      true, false, true);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getSegment4Fare(), formattedMessage, 12, true, false, false, 
      true, false);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getStopoverIndicator5(), formattedMessage, 1, false, true, 
      false, false, true);
    
    SettlementMessageFormatter.formatValue(airlineIndustryTAABean
      .getExchangedOrOriginalTicketNumber(), formattedMessage, 14, 
      false, true, true, false, true);
    if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
      SettlementMessageFormatter.characterSpacesFill(formattedMessage, 313);
    }
    formatedMessageList.add(formattedMessage.toString());
    if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
      LOGGER.info("Formated AirLine Industry Type Record :" + formattedMessage.toString());
    }
  }
  
  public static void formatAutoRentalIndustryTypeRecord(AutoRentalIndustryTAABean autoRentalIndustryTAABean, List<String> formatedMessageList)
    throws IOException
  {
    StringBuffer formattedMessage = new StringBuffer();
    
    SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
      .getRecordType(), formattedMessage, 3, false, true, true, 
      false, true);
    
    SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
      .getRecordNumber(), formattedMessage, 8, true, false, false, 
      true, false);
    
    SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
      .getTransactionIdentifier(), formattedMessage, 15, true, 
      false, false, true, false);
    
    SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
      .getFormatCode(), formattedMessage, 2, false, true, false, 
      false, true);
    
    SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
      .getAddendaTypeCode(), formattedMessage, 2, false, true, 
      false, false, true);
    
    SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
      .getAutoRentalAgreementNumber(), formattedMessage, 14, false, 
      true, true, false, true);
    SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
      .getAutoRentalPickupLocation(), formattedMessage, 38, false, 
      true, true, false, true);
    
    SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
      .getAutoRentalPickupCityName(), formattedMessage, 18, false, 
      true, true, false, true);
    
    SettlementMessageFormatter.characterSpacesFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
      .getAutoRentalPickupRegionCode(), formattedMessage, 3, false, 
      true, true, false, true);
    
    SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
      .getAutoRentalPickupCountryCode(), formattedMessage, 3, false, 
      true, true, false, true);
    SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
      .getAutoRentalPickupDate(), formattedMessage, 8, true, false, 
      false, true, false);
    
    SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
      .getAutoRentalPickupTime(), formattedMessage, 6, false, false, 
      false, true, false);
    
    SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
      .getAutoRentalReturnCityName(), formattedMessage, 18, false, 
      true, true, false, true);
    
    SettlementMessageFormatter.characterSpacesFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
      .getAutoRentalReturnRegionCode(), formattedMessage, 3, false, 
      true, true, false, true);
    
    SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
      .getAutoRentalReturnCountryCode(), formattedMessage, 3, false, 
      true, true, false, true);
    SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
      .getAutoRentalReturnDate(), formattedMessage, 8, true, false, 
      false, true, false);
    SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
      .getAutoRentalReturnTime(), formattedMessage, 6, true, false, 
      false, true, false);
    
    SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
      .getAutoRentalRenterName(), formattedMessage, 26, false, true, 
      true, false, true);
    
    SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
      .getAutoRentalVehicleClassId(), formattedMessage, 4, false, 
      true, false, false, true);
    SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
      .getAutoRentalDistance(), formattedMessage, 5, true, false, 
      false, true, false);
    
    SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
      .getAutoRentalDistanceUnitOfMeasure(), formattedMessage, 1, 
      false, true, true, false, true);
    SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
      .getAutoRentalAuditAdjustmentIndicator(), formattedMessage, 1, 
      false, true, true, false, true);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
      .getAutoRentalAuditAdjustmentAmount(), formattedMessage, 12, 
      true, false, false, true, false);
    
    SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
      .getReturnDropoffLocation(), formattedMessage, 38, false, 
      true, true, false, true);
    
    SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
      .getVehicleIdentificationNumber(), formattedMessage, 20, false, 
      true, true, false, true);
    
    SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
      .getDriverIdentificationNumber(), formattedMessage, 20, false, 
      true, true, false, true);
    
    SettlementMessageFormatter.formatValue(autoRentalIndustryTAABean
      .getDriverTaxNumber(), formattedMessage, 20, false, 
      true, true, false, true);
    if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
      SettlementMessageFormatter.characterSpacesFill(formattedMessage, 386);
    }
    formatedMessageList.add(formattedMessage.toString());
    if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
      LOGGER.info("Formated AutoRental Industry Type Record :" + formattedMessage.toString());
    }
  }
  
  public static void formatCommunicationServicesIndustryTypeRecord(CommunicationServicesIndustryBean communicationServicesIndustryBean, List<String> formatedMessageList)
    throws IOException
  {
    StringBuffer formattedMessage = new StringBuffer();
    
    SettlementMessageFormatter.formatValue(
      communicationServicesIndustryBean.getRecordType(), 
      formattedMessage, 3, false, true, true, false, true);
    SettlementMessageFormatter.formatValue(
      communicationServicesIndustryBean.getRecordNumber(), 
      formattedMessage, 8, true, false, false, true, false);
    
    SettlementMessageFormatter.formatValue(
      communicationServicesIndustryBean.getTransactionIdentifier(), 
      formattedMessage, 15, true, false, false, true, false);
    SettlementMessageFormatter.formatValue(
      communicationServicesIndustryBean.getFormatCode(), 
      formattedMessage, 2, false, true, false, false, true);
    SettlementMessageFormatter.formatValue(
      communicationServicesIndustryBean.getAddendaTypeCode(), 
      formattedMessage, 2, false, true, false, false, true);
    SettlementMessageFormatter.formatValue(
      communicationServicesIndustryBean.getCallDate(), 
      formattedMessage, 8, true, false, false, true, false);
    SettlementMessageFormatter.formatValue(
      communicationServicesIndustryBean.getCallTime(), 
      formattedMessage, 6, true, false, false, true, false);
    SettlementMessageFormatter.formatValue(
      communicationServicesIndustryBean.getCallDurationTime(), 
      formattedMessage, 6, true, false, false, true, false);
    SettlementMessageFormatter.formatValue(
      communicationServicesIndustryBean.getCallFromLocationName(), 
      formattedMessage, 18, false, true, true, false, true);
    
    SettlementMessageFormatter.formatValue(
      communicationServicesIndustryBean.getCallFromRegionCode(), 
      formattedMessage, 3, false, true, true, false, true);
    
    SettlementMessageFormatter.formatValue(
      communicationServicesIndustryBean.getCallFromCountryCode(), 
      formattedMessage, 3, false, true, true, false, true);
    SettlementMessageFormatter.formatValue(
      communicationServicesIndustryBean.getCallFromPhoneNumber(), 
      formattedMessage, 16, false, true, true, false, true);
    SettlementMessageFormatter.formatValue(
      communicationServicesIndustryBean.getCallToLocationName(), 
      formattedMessage, 18, false, true, true, false, true);
    
    SettlementMessageFormatter.formatValue(
      communicationServicesIndustryBean.getCallToRegionCode(), 
      formattedMessage, 3, false, true, true, false, true);
    
    SettlementMessageFormatter.formatValue(
      communicationServicesIndustryBean.getCallToCountryCode(), 
      formattedMessage, 3, false, true, true, false, true);
    SettlementMessageFormatter.formatValue(
      communicationServicesIndustryBean.getCallToPhoneNumber(), 
      formattedMessage, 16, false, true, true, false, true);
    
    SettlementMessageFormatter.formatValue(
      communicationServicesIndustryBean.getPhoneCardId(), 
      formattedMessage, 8, false, true, true, false, true);
    SettlementMessageFormatter.formatValue(
      communicationServicesIndustryBean.getServiceDescription(), 
      formattedMessage, 20, false, true, true, false, true);
    
    SettlementMessageFormatter.formatValue(
      communicationServicesIndustryBean.getBillingPeriod(), 
      formattedMessage, 18, false, true, true, false, true);
    
    SettlementMessageFormatter.formatValue(
      communicationServicesIndustryBean
      .getCommunicationsCallTypeCode(), formattedMessage, 5, 
      false, true, true, false, true);
    SettlementMessageFormatter.formatValue(
      communicationServicesIndustryBean.getCommunicationsRateClass(), 
      formattedMessage, 1, false, true, true, false, true);
    if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
      SettlementMessageFormatter.characterSpacesFill(formattedMessage, 518);
    }
    formatedMessageList.add(formattedMessage.toString());
    if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
      LOGGER.info("Formated Communication Services Industry Type Record :" + formattedMessage.toString());
    }
  }
  
  public static void formatEntertainmentTicketingIndustryTypeRecord(EntertainmentTicketingIndustryTAABean entertainmentTicketingIndustryTAABean, List<String> formatedMessageList)
    throws IOException
  {
    StringBuffer formattedMessage = new StringBuffer();
    
    SettlementMessageFormatter.formatValue(
      entertainmentTicketingIndustryTAABean.getRecordType(), 
      formattedMessage, 3, false, true, true, false, true);
    SettlementMessageFormatter.formatValue(
      entertainmentTicketingIndustryTAABean.getRecordNumber(), 
      formattedMessage, 8, true, false, false, true, false);
    
    SettlementMessageFormatter.formatValue(
      entertainmentTicketingIndustryTAABean
      .getTransactionIdentifier(), formattedMessage, 15, 
      false, false, false, true, false);
    
    SettlementMessageFormatter.formatValue(
      entertainmentTicketingIndustryTAABean.getFormatCode(), 
      formattedMessage, 2, false, true, false, false, true);
    SettlementMessageFormatter.formatValue(
      entertainmentTicketingIndustryTAABean.getAddendaTypeCode(), 
      formattedMessage, 2, false, true, false, false, true);
    SettlementMessageFormatter.formatValue(
      entertainmentTicketingIndustryTAABean.getEventName(), 
      formattedMessage, 30, false, true, true, false, true);
    
    SettlementMessageFormatter.formatValue(
      entertainmentTicketingIndustryTAABean.getEventDate(), 
      formattedMessage, 8, true, false, false, true, false);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(
      entertainmentTicketingIndustryTAABean
      .getEventIndividualTicketPriceAmount(), 
      formattedMessage, 12, true, false, false, true, false);
    
    SettlementMessageFormatter.formatValue(
      entertainmentTicketingIndustryTAABean.getEventTicketQuantity(), 
      formattedMessage, 4, true, false, false, true, false);
    
    SettlementMessageFormatter.formatValue(
      entertainmentTicketingIndustryTAABean.getEventLocation(), 
      formattedMessage, 40, false, true, true, false, true);
    
    SettlementMessageFormatter.formatValue(
      entertainmentTicketingIndustryTAABean.getEventRegionCode(), 
      formattedMessage, 3, false, true, true, false, true);
    SettlementMessageFormatter.formatValue(
      entertainmentTicketingIndustryTAABean.getEventCountryCode(), 
      formattedMessage, 3, false, true, true, false, true);
    if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
      SettlementMessageFormatter.characterSpacesFill(formattedMessage, 567);
    }
    formatedMessageList.add(formattedMessage.toString());
    if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
      LOGGER.info("Formated Entertainment Ticketing Industry Type Record :" + formattedMessage.toString());
    }
  }
  
  public static void formatInsuranceIndustryTypeRecord(InsuranceIndustryTAABean insuranceIndustryTAABean, List<String> formatedMessageList)
    throws IOException
  {
    StringBuffer formattedMessage = new StringBuffer();
    
    SettlementMessageFormatter.formatValue(insuranceIndustryTAABean
      .getRecordType(), formattedMessage, 3, false, true, true, 
      false, true);
    
    SettlementMessageFormatter.formatValue(insuranceIndustryTAABean
      .getRecordNumber(), formattedMessage, 8, true, false, false, 
      true, false);
    
    SettlementMessageFormatter.formatValue(insuranceIndustryTAABean
      .getTransactionIdentifier(), formattedMessage, 15, true, 
      false, false, true, false);
    
    SettlementMessageFormatter.formatValue(insuranceIndustryTAABean
      .getFormatCode(), formattedMessage, 2, false, true, false, 
      false, true);
    
    SettlementMessageFormatter.formatValue(insuranceIndustryTAABean
      .getAddendaTypeCode(), formattedMessage, 2, false, true, 
      false, false, true);
    
    SettlementMessageFormatter.formatValue(insuranceIndustryTAABean
      .getInsurancePolicyNumber(), formattedMessage, 23, false, true, 
      true, false, true);
    
    SettlementMessageFormatter.formatValue(insuranceIndustryTAABean
      .getInsuranceCoverageStartDate(), formattedMessage, 8, true, 
      false, false, true, false);
    
    SettlementMessageFormatter.formatValue(insuranceIndustryTAABean
      .getInsuranceCoverageEndDate(), formattedMessage, 8, true, 
      false, false, true, false);
    
    SettlementMessageFormatter.formatValue(insuranceIndustryTAABean
      .getInsurancePolicyPremiumFrequency(), formattedMessage, 7, 
      false, true, true, false, true);
    
    SettlementMessageFormatter.formatValue(insuranceIndustryTAABean
      .getAdditionalInsurancePolicyNumber(), formattedMessage, 23, 
      false, true, true, false, true);
    
    SettlementMessageFormatter.formatValue(insuranceIndustryTAABean
      .getTypeOfPolicy(), formattedMessage, 25, false, true, true, 
      false, true);
    
    SettlementMessageFormatter.formatValue(insuranceIndustryTAABean
      .getNameOfInsured(), formattedMessage, 30, false, true, true, 
      false, true);
    if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
      SettlementMessageFormatter.characterSpacesFill(formattedMessage, 546);
    }
    formatedMessageList.add(formattedMessage.toString());
    if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
      LOGGER.info("Formated Insurance Industry Type Record :" + formattedMessage.toString());
    }
  }
  
  public static void formatRailIndustryTypeRecord(RailIndustryTAABean railIndustryTAABean, List<String> formatedMessageList)
    throws IOException
  {
    StringBuffer formattedMessage = new StringBuffer();
    
    SettlementMessageFormatter.formatValue(railIndustryTAABean
      .getRecordType(), formattedMessage, 3, false, true, true, 
      false, true);
    
    SettlementMessageFormatter.formatValue(railIndustryTAABean
      .getRecordNumber(), formattedMessage, 8, true, false, false, 
      true, false);
    
    SettlementMessageFormatter.formatValue(railIndustryTAABean
      .getTransactionIdentifier(), formattedMessage, 15, true, 
      false, false, true, false);
    
    SettlementMessageFormatter.formatValue(railIndustryTAABean
      .getFormatCode(), formattedMessage, 2, false, true, false, 
      false, true);
    
    SettlementMessageFormatter.formatValue(railIndustryTAABean
      .getAddendaTypeCode(), formattedMessage, 2, false, true, 
      false, false, true);
    
    SettlementMessageFormatter.formatValue(railIndustryTAABean
      .getTransactionType(), formattedMessage, 3, false, true, true, 
      false, true);
    SettlementMessageFormatter.formatValue(railIndustryTAABean
      .getTicketNumber(), formattedMessage, 14, false, true, true, 
      false, true);
    
    SettlementMessageFormatter.formatValue(railIndustryTAABean
      .getPassengerName(), formattedMessage, 25, false, true, true, 
      false, true);
    
    SettlementMessageFormatter.formatValue(railIndustryTAABean
      .getIataCarrierCode(), formattedMessage, 3, false, true, true, 
      false, true);
    
    SettlementMessageFormatter.formatValue(railIndustryTAABean
      .getTicketIssuerName(), formattedMessage, 32, false, true, 
      true, false, true);
    
    SettlementMessageFormatter.formatValue(railIndustryTAABean
      .getTicketIssueCity(), formattedMessage, 18, false, true, true, 
      false, true);
    SettlementMessageFormatter.formatValue(railIndustryTAABean
      .getDepartureStation1(), formattedMessage, 3, false, true, 
      false, false, true);
    SettlementMessageFormatter.formatValue(railIndustryTAABean
      .getDepartureDate1(), formattedMessage, 8, true, false, false, 
      true, false);
    SettlementMessageFormatter.formatValue(railIndustryTAABean
      .getArrivalStation1(), formattedMessage, 3, false, true, 
      false, false, true);
    
    SettlementMessageFormatter.formatValue(railIndustryTAABean
      .getDepartureStation2(), formattedMessage, 3, false, true, 
      false, false, true);
    SettlementMessageFormatter.formatValue(railIndustryTAABean
      .getDepartureDate2(), formattedMessage, 8, true, false, false, 
      true, false);
    SettlementMessageFormatter.formatValue(railIndustryTAABean
      .getArrivalStation2(), formattedMessage, 3, false, true, 
      false, false, true);
    
    SettlementMessageFormatter.formatValue(railIndustryTAABean
      .getDepartureStation3(), formattedMessage, 3, false, true, 
      false, false, true);
    SettlementMessageFormatter.formatValue(railIndustryTAABean
      .getDepartureDate3(), formattedMessage, 8, true, false, false, 
      true, false);
    SettlementMessageFormatter.formatValue(railIndustryTAABean
      .getArrivalStation3(), formattedMessage, 3, false, true, 
      false, false, true);
    
    SettlementMessageFormatter.formatValue(railIndustryTAABean
      .getDepartureStation4(), formattedMessage, 3, false, true, 
      false, false, true);
    SettlementMessageFormatter.formatValue(railIndustryTAABean
      .getDepartureDate4(), formattedMessage, 8, true, false, false, 
      true, false);
    SettlementMessageFormatter.formatValue(railIndustryTAABean
      .getArrivalStation4(), formattedMessage, 3, false, true, 
      false, false, true);
    if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
      SettlementMessageFormatter.characterSpacesFill(formattedMessage, 519);
    }
    formatedMessageList.add(formattedMessage.toString());
    if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
      LOGGER.info("Formated Rail Industry Type Record :" + formattedMessage.toString());
    }
  }
  
  public static void formatRetailIndustryTypeRecord(RetailIndustryTAABean retailIndustryTAABean, List<String> formatedMessageList)
    throws IOException
  {
    StringBuffer formattedMessage = new StringBuffer();
    
    SettlementMessageFormatter.formatValue(retailIndustryTAABean
      .getRecordType(), formattedMessage, 3, false, true, true, 
      false, true);
    
    SettlementMessageFormatter.formatValue(retailIndustryTAABean
      .getRecordNumber(), formattedMessage, 8, true, false, false, 
      true, false);
    
    SettlementMessageFormatter.formatValue(retailIndustryTAABean
      .getTransactionIdentifier(), formattedMessage, 15, true, 
      false, false, true, false);
    
    SettlementMessageFormatter.formatValue(retailIndustryTAABean
      .getFormatCode(), formattedMessage, 2, false, true, false, 
      false, true);
    
    SettlementMessageFormatter.formatValue(retailIndustryTAABean
      .getAddendaTypeCode(), formattedMessage, 2, false, true, 
      false, false, true);
    SettlementMessageFormatter.formatValue(retailIndustryTAABean
      .getRetailDepartmentName(), formattedMessage, 40, false, true, 
      true, false, true);
    
    SettlementMessageFormatter.formatValue(retailIndustryTAABean
      .getRetailItemDescription1(), formattedMessage, 19, false, 
      true, true, false, true);
    SettlementMessageFormatter.formatValue(retailIndustryTAABean
      .getRetailItemQuantity1(), formattedMessage, 3, true, false, 
      false, true, false);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(retailIndustryTAABean
      .getRetailItemAmount1(), formattedMessage, 12, true, false, 
      false, true, false);
    
    SettlementMessageFormatter.formatValue(retailIndustryTAABean
      .getRetailItemDescription2(), formattedMessage, 19, false, 
      true, true, false, true);
    SettlementMessageFormatter.formatValue(retailIndustryTAABean
      .getRetailItemQuantity2(), formattedMessage, 3, true, false, 
      false, true, false);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(retailIndustryTAABean
      .getRetailItemAmount2(), formattedMessage, 12, true, false, 
      false, true, false);
    
    SettlementMessageFormatter.formatValue(retailIndustryTAABean
      .getRetailItemDescription3(), formattedMessage, 19, false, 
      true, true, false, true);
    SettlementMessageFormatter.formatValue(retailIndustryTAABean
      .getRetailItemQuantity3(), formattedMessage, 3, true, false, 
      false, true, false);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(retailIndustryTAABean
      .getRetailItemAmount3(), formattedMessage, 12, true, false, 
      false, true, false);
    
    SettlementMessageFormatter.formatValue(retailIndustryTAABean
      .getRetailItemDescription4(), formattedMessage, 19, false, 
      true, true, false, true);
    SettlementMessageFormatter.formatValue(retailIndustryTAABean
      .getRetailItemQuantity4(), formattedMessage, 3, true, false, 
      false, true, false);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(retailIndustryTAABean
      .getRetailItemAmount4(), formattedMessage, 12, true, false, 
      false, true, false);
    
    SettlementMessageFormatter.formatValue(retailIndustryTAABean
      .getRetailItemDescription5(), formattedMessage, 19, false, 
      true, true, false, true);
    SettlementMessageFormatter.formatValue(retailIndustryTAABean
      .getRetailItemQuantity5(), formattedMessage, 3, true, false, 
      false, true, false);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(retailIndustryTAABean
      .getRetailItemAmount5(), formattedMessage, 12, true, false, 
      false, true, false);
    
    SettlementMessageFormatter.formatValue(retailIndustryTAABean
      .getRetailItemDescription6(), formattedMessage, 19, false, 
      true, true, false, true);
    SettlementMessageFormatter.formatValue(retailIndustryTAABean
      .getRetailItemQuantity6(), formattedMessage, 3, true, false, 
      false, true, false);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(retailIndustryTAABean
      .getRetailItemAmount6(), formattedMessage, 12, true, false, 
      false, true, false);
    if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
      SettlementMessageFormatter.characterSpacesFill(formattedMessage, 408);
    }
    formatedMessageList.add(formattedMessage.toString());
    if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
      LOGGER.info("Formated Rrtail Industry Type Record :" + formattedMessage.toString());
    }
  }
  
  public static void formatTravelCruiseIndustryTypeRecord(TravelCruiseIndustryTAABean travelCruiseIndustryTAABean, List<String> formatedMessageList)
    throws IOException
  {
    StringBuffer formattedMessage = new StringBuffer();
    
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getRecordType(), formattedMessage, 3, false, true, true, 
      false, true);
    
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getRecordNumber(), formattedMessage, 8, true, false, false, 
      true, false);
    
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getTransactionIdentifier(), formattedMessage, 15, true, 
      false, false, true, false);
    
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getFormatCode(), formattedMessage, 2, false, true, false, 
      false, true);
    
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getAddendaTypeCode(), formattedMessage, 2, false, true, 
      false, false, true);
    
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getIataCarrierCode(), formattedMessage, 3, false, true, true, 
      false, true);
    
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getIataAgencyNumber(), formattedMessage, 8, false, true, true, 
      false, true);
    
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getTravelPackageIndicator(), formattedMessage, 1, false, 
      true, false, false, true);
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getPassengerName(), formattedMessage, 25, false, true, true, 
      false, true);
    
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getTravelTicketNumber(), formattedMessage, 14, false, true, 
      true, false, true);
    
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getTravelDepartureDateSegment1(), formattedMessage, 8, true, 
      false, false, true, false);
    
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getTravelDestinationCodeSegment1(), formattedMessage, 3, 
      false, true, false, false, true);
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getTravelDepartureAirportSegment1(), formattedMessage, 3, 
      false, true, false, false, true);
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getAirCarrierCodeSegment1(), formattedMessage, 2, false, 
      true, false, false, true);
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getFlightNumberSegment1(), formattedMessage, 4, false, true, 
      false, false, true);
    
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getClassCodeSegment1(), formattedMessage, 1, false, true, 
      false, false, true);
    
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getTravelDepartureDateSegment2(), formattedMessage, 8, true, 
      false, false, true, false);
    
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getTravelDestinationCodeSegment2(), formattedMessage, 3, 
      false, true, false, false, true);
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getTravelDepartureAirportSegment2(), formattedMessage, 3, 
      false, true, false, false, true);
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getAirCarrierCodeSegment2(), formattedMessage, 2, false, 
      true, false, false, true);
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getFlightNumberSegment2(), formattedMessage, 4, false, true, 
      false, false, true);
    
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getClassCodeSegment2(), formattedMessage, 1, false, true, 
      false, false, true);
    
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getTravelDepartureDateSegment3(), formattedMessage, 8, true, 
      false, false, true, false);
    
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getTravelDestinationCodeSegment3(), formattedMessage, 3, 
      false, true, false, false, true);
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getTravelDepartureAirportSegment3(), formattedMessage, 3, 
      false, true, false, false, true);
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getAirCarrierCodeSegment3(), formattedMessage, 2, false, 
      true, false, false, true);
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getFlightNumberSegment3(), formattedMessage, 4, false, true, 
      false, false, true);
    
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getClassCodeSegment3(), formattedMessage, 1, false, true, 
      false, false, true);
    
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getTravelDepartureDateSegment4(), formattedMessage, 8, true, 
      false, false, true, false);
    
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getTravelDestinationCodeSegment4(), formattedMessage, 3, 
      false, true, false, false, true);
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getTravelDepartureAirportSegment4(), formattedMessage, 3, 
      false, true, false, false, true);
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getAirCarrierCodeSegment4(), formattedMessage, 2, false, 
      true, false, false, true);
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getFlightNumberSegment4(), formattedMessage, 4, false, true, 
      false, false, true);
    
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getClassCodeSegment4(), formattedMessage, 1, false, true, 
      false, false, true);
    
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getLodgingCheckInDate(), formattedMessage, 8, true, false, 
      false, true, false);
    
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getLodgingCheckOutDate(), formattedMessage, 8, true, false, 
      false, true, false);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 3);
    
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getLodgingRoomRate1(), formattedMessage, 12, true, false, 
      false, true, false);
    
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getNumberOfNightsAtRoomRate1(), formattedMessage, 2, true, 
      false, false, true, false);
    
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getLodgingName(), formattedMessage, 20, false, true, true, 
      false, true);
    
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getLodgingRegionCode(), formattedMessage, 3, false, true, 
      true, false, true);
    
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getLodgingCountryCode(), formattedMessage, 3, false, false, 
      true, false, true);
    SettlementMessageFormatter.formatValue(travelCruiseIndustryTAABean
      .getLodgingCityName(), formattedMessage, 18, false, true, true, 
      false, true);
    if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
      SettlementMessageFormatter.characterSpacesFill(formattedMessage, 458);
    }
    formatedMessageList.add(formattedMessage.toString());
    if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
      LOGGER.info("Formated Travel Cruise Industry Type Record :" + formattedMessage.toString());
    }
  }
}
