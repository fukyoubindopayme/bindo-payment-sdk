package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.messagecreator;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceDetailBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionBatchTrailerBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.LocationDetailTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.AirlineIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.AutoRentalIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.CommunicationServicesIndustryBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.EntertainmentTicketingIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.InsuranceIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.LodgingIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.RailIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.RetailIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.TravelCruiseIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.nonindustrytype.CPSLevel2Bean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.nonindustrytype.DeferredPaymentPlanBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.nonindustrytype.EMVBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.confirmation.ConfirmationRejectedRecordBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.confirmation.RejectedRecordDataBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.confirmation.RejectedTABRecord;
import org.apache.log4j.Logger;

public class RejectionRecords
{
  private static final Logger LOGGER = Logger.getLogger(RejectionRecords.class);
  
  public static void setTABRecord(StringBuffer recordType, ConfirmationRejectedRecordBean rejectedTABRecord)
  {
    RejectedRecordDataBean rejRecTabBean = new RejectedRecordDataBean();
    RejectedTABRecord tabRecord = new RejectedTABRecord();
    rejectedTABRecord.setReturnedaRecordType(recordSubstring(recordType.toString(), 0, 3));
    
    rejectedTABRecord.setReturnedRecordNumber(recordSubstring(recordType.toString(), 3, 11));
    rejectedTABRecord.setReturnedRecordData(rejRecTabBean);
    
    tabRecord.setRecordType(recordSubstring(recordType.toString(), 0, 3));
    tabRecord.setRecordNumber(recordSubstring(recordType.toString(), 3, 11));
    tabRecord.setTransactionIdentifier(recordSubstring(recordType.toString(), 11, 26));
    tabRecord.setFormatCode(recordSubstring(recordType.toString(), 26, 28));
    
    tabRecord.setMediaCode(recordSubstring(recordType.toString(), 28, 30));
    tabRecord.setSubmissionMethod(recordSubstring(recordType.toString(), 30, 32));
    tabRecord.setTabField7reserved(recordSubstring(recordType.toString(), 32, 42));
    tabRecord.setApprovalCode(recordSubstring(recordType.toString(), 42, 48));
    
    tabRecord.setPrimaryAccountNumber(recordSubstring(recordType.toString(), 48, 67));
    tabRecord.setCardExpiryDate(recordSubstring(recordType.toString(), 67, 71));
    tabRecord.setTransactionDate(recordSubstring(recordType.toString(), 71, 79));
    tabRecord.setTransactionTime(recordSubstring(recordType.toString(), 79, 85));
    
    tabRecord.setTransactionAmount(recordSubstring(recordType.toString(), 88, 100));
    tabRecord.setProcessingCode(recordSubstring(recordType.toString(), 100, 106));
    tabRecord.setTransactionCurrencyCode(recordSubstring(recordType.toString(), 106, 109));
    tabRecord.setExtendedPaymentData(recordSubstring(recordType.toString(), 109, 111));
    tabRecord.setMerchantId(recordSubstring(recordType.toString(), 111, 126));
    tabRecord.setMerchantLocationId(recordSubstring(recordType.toString(), 126, 141));
    tabRecord.setMerchantContactInfo(recordSubstring(recordType.toString(), 141, 181));
    tabRecord.setTerminalId(recordSubstring(recordType.toString(), 181, 189));
    tabRecord.setPosDataCode(recordSubstring(recordType.toString(), 189, 201));
    tabRecord.setInvoiceReferenceNumber(recordSubstring(recordType.toString(), 219, 249));
    tabRecord.setTabImageSequenceNumber(recordSubstring(recordType.toString(), 264, 272));
    tabRecord.setMatchingKeyType(recordSubstring(recordType.toString(), 272, 274));
    tabRecord.setMatchingKey(recordSubstring(recordType.toString(), 274, 295));
    tabRecord.setElectronicCommerceIndicator(recordSubstring(recordType.toString(), 295, 297));
    rejRecTabBean.setTabRecord(tabRecord);
  }
  
  public static void setTADRecord(StringBuffer recordType, ConfirmationRejectedRecordBean rejectedTADRecord)
  {
    RejectedRecordDataBean rejRecTadBean = new RejectedRecordDataBean();
    
    rejectedTADRecord.setReturnedaRecordType(recordSubstring(recordType.toString(), 0, 3));
    
    rejectedTADRecord.setReturnedRecordNumber(recordSubstring(recordType.toString(), 3, 11));
    
    TransactionAdviceDetailBean tadBean = new TransactionAdviceDetailBean();
    tadBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
    tadBean.setRecordNumber(recordSubstring(recordType.toString(), 3, 11));
    tadBean.setTransactionIdentifier(recordSubstring(recordType.toString(), 11, 26));
    tadBean.setAdditionalAmountType1(recordSubstring(recordType.toString(), 26, 29));
    tadBean.setAdditionalAmount1(recordSubstring(recordType.toString(), 32, 44));
    tadBean.setAdditionalAmountSign1(recordSubstring(recordType.toString(), 44, 45));
    tadBean.setAdditionalAmountType2(recordSubstring(recordType.toString(), 45, 48));
    tadBean.setAdditionalAmount2(recordSubstring(recordType.toString(), 51, 63));
    tadBean.setAdditionalAmountSign2(recordSubstring(recordType.toString(), 63, 64));
    tadBean.setAdditionalAmountType3(recordSubstring(recordType.toString(), 64, 67));
    tadBean.setAdditionalAmount3(recordSubstring(recordType.toString(), 70, 82));
    
    tadBean.setAdditionalAmountSign3(recordSubstring(recordType.toString(), 82, 83));
    tadBean.setAdditionalAmountType4(recordSubstring(recordType.toString(), 83, 86));
    tadBean.setAdditionalAmount4(recordSubstring(recordType.toString(), 89, 101));
    tadBean.setAdditionalAmountSign4(recordSubstring(recordType.toString(), 101, 102));
    tadBean.setAdditionalAmountType5(recordSubstring(recordType.toString(), 102, 105));
    tadBean.setAdditionalAmount5(recordSubstring(recordType.toString(), 108, 120));
    tadBean.setAdditionalAmountSign5(recordSubstring(recordType.toString(), 120, 121));
    rejRecTadBean.setTadRecordBean(tadBean);
    rejectedTADRecord.setReturnedRecordData(rejRecTadBean);
  }
  
  public static void setTBTRecord(StringBuffer recordType, ConfirmationRejectedRecordBean rejectedTBTRecord)
  {
    rejectedTBTRecord.setReturnedaRecordType(recordSubstring(recordType.toString(), 0, 3));
    rejectedTBTRecord.setReturnedRecordNumber(recordSubstring(recordType.toString(), 3, 11));
    RejectedRecordDataBean rejRecTBTBean = new RejectedRecordDataBean();
    TransactionBatchTrailerBean tbtBean = new TransactionBatchTrailerBean();
    tbtBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
    tbtBean.setRecordNumber(recordSubstring(recordType.toString(), 3, 11));
    tbtBean.setMerchantId(recordSubstring(recordType.toString(), 11, 26));
    tbtBean.setTbtIdentificationNumber(recordSubstring(recordType.toString(), 41, 56));
    tbtBean.setTbtCreationDate(recordSubstring(recordType.toString(), 56, 64));
    tbtBean.setTotalNoOfTabs(recordSubstring(recordType.toString(), 64, 72));
    tbtBean.setTbtAmount(recordSubstring(recordType.toString(), 75, 95));
    tbtBean.setTbtAmountSign(recordSubstring(recordType.toString(), 95, 96));
    tbtBean.setTbtCurrencyCode(recordSubstring(recordType.toString(), 96, 99));
    tbtBean.setTbtImageSequenceNumber(recordSubstring(recordType.toString(), 125, 133));
    rejRecTBTBean.setTbtRecord(tbtBean);
    rejectedTBTRecord.setReturnedRecordData(rejRecTBTBean);
  }
  
  public static void setTAAAirRecord(StringBuffer recordType, ConfirmationRejectedRecordBean rejectedTAAAirecord)
  {
    rejectedTAAAirecord.setReturnedaRecordType(recordSubstring(recordType.toString(), 0, 3));
    rejectedTAAAirecord.setReturnedRecordNumber(recordSubstring(recordType.toString(), 3, 11));
    RejectedRecordDataBean rejRecAirlineBean = new RejectedRecordDataBean();
    AirlineIndustryTAABean airlineBean = new AirlineIndustryTAABean();
    
    airlineBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
    
    airlineBean.setRecordNumber(recordSubstring(recordType.toString(), 3, 11));
    
    airlineBean.setTransactionIdentifier(recordSubstring(recordType.toString(), 11, 26));
    
    airlineBean.setFormatCode(recordSubstring(recordType.toString(), 26, 28));
    airlineBean.setAddendaTypeCode(recordSubstring(recordType.toString(), 28, 30));
    airlineBean.setTransactionType(recordSubstring(recordType.toString(), 30, 33));
    airlineBean.setTicketNumber(recordSubstring(recordType.toString(), 33, 47));
    airlineBean.setDocumentType(recordSubstring(recordType.toString(), 47, 49));
    airlineBean.setAirlineProcessIdentifier(recordSubstring(recordType.toString(), 49, 52));
    airlineBean.setIataNumericCode(recordSubstring(recordType.toString(), 52, 60));
    airlineBean.setTicketingCarrierName(recordSubstring(recordType.toString(), 60, 85));
    airlineBean.setTicketIssueCity(recordSubstring(recordType.toString(), 85, 103));
    airlineBean.setTicketIssueDate(recordSubstring(recordType.toString(), 103, 111));
    airlineBean.setNumberInParty(recordSubstring(recordType.toString(), 111, 114));
    airlineBean.setPassengerName(recordSubstring(recordType.toString(), 114, 139));
    airlineBean.setConjunctionTicketIndicator(recordSubstring(recordType.toString(), 139, 140));
    
    airlineBean.setOriginalTransactionAmount(recordSubstring(recordType.toString(), 143, 155));
    airlineBean.setOriginalCurrencyCode(recordSubstring(recordType.toString(), 155, 158));
    airlineBean.setElectronicTicketIndicator(recordSubstring(recordType.toString(), 158, 159));
    airlineBean.setTotalNumberOfAirSegments(recordSubstring(recordType.toString(), 159, 160));
    airlineBean.setStopoverIndicator1(recordSubstring(recordType.toString(), 106, 161));
    airlineBean.setDepartureLocationCodeSegment1(recordSubstring(recordType.toString(), 161, 164));
    airlineBean.setDepartureDateSegment1(recordSubstring(recordType.toString(), 164, 172));
    airlineBean.setArrivalLocationCodeSegment1(recordSubstring(recordType.toString(), 172, 175));
    airlineBean.setSegmentCarrierCode1(recordSubstring(recordType.toString(), 175, 177));
    airlineBean.setSegment1FareBasis(recordSubstring(recordType.toString(), 177, 192));
    airlineBean.setClassOfServiceCodeSegment1(recordSubstring(recordType.toString(), 192, 194));
    airlineBean.setFlightNumberSegment1(recordSubstring(recordType.toString(), 194, 198));
    airlineBean.setSegment1Fare(recordSubstring(recordType.toString(), 201, 213));
    airlineBean.setStopoverIndicator2(recordSubstring(recordType.toString(), 213, 214));
    airlineBean.setDepartureLocationCodeSegment2(recordSubstring(recordType.toString(), 214, 217));
    airlineBean.setDepartureDateSegment2(recordSubstring(recordType.toString(), 217, 225));
    airlineBean.setArrivalLocationCodeSegment2(recordSubstring(recordType.toString(), 225, 228));
    airlineBean.setSegmentCarrierCode2(recordSubstring(recordType.toString(), 228, 230));
    airlineBean.setSegment2FareBasis(recordSubstring(recordType.toString(), 230, 245));
    airlineBean.setClassOfServiceCodeSegment2(recordSubstring(recordType.toString(), 245, 247));
    airlineBean.setFlightNumberSegment2(recordSubstring(recordType.toString(), 247, 251));
    
    airlineBean.setSegment2Fare(recordSubstring(recordType.toString(), 254, 266));
    airlineBean.setStopoverIndicator3(recordSubstring(recordType.toString(), 266, 267));
    airlineBean.setDepartureLocationCodeSegment3(recordSubstring(recordType.toString(), 267, 270));
    airlineBean.setDepartureDateSegment3(recordSubstring(recordType.toString(), 270, 278));
    airlineBean.setArrivalLocationCodeSegment3(recordSubstring(recordType.toString(), 278, 281));
    airlineBean.setSegmentCarrierCode3(recordSubstring(recordType.toString(), 281, 283));
    airlineBean.setSegment3FareBasis(recordSubstring(recordType.toString(), 283, 298));
    airlineBean.setClassOfServiceCodeSegment3(recordSubstring(recordType.toString(), 298, 300));
    airlineBean.setFlightNumberSegment3(recordSubstring(recordType.toString(), 300, 304));
    airlineBean.setSegment3Fare(recordSubstring(recordType.toString(), 307, 319));
    airlineBean.setStopoverIndicator4(recordSubstring(recordType.toString(), 319, 320));
    airlineBean.setDepartureLocationCodeSegment4(recordSubstring(recordType.toString(), 320, 323));
    airlineBean.setDepartureDateSegment4(recordSubstring(recordType.toString(), 323, 331));
    airlineBean.setArrivalLocationCodeSegment4(recordSubstring(recordType.toString(), 331, 334));
    airlineBean.setSegmentCarrierCode4(recordSubstring(recordType.toString(), 334, 336));
    airlineBean.setSegment4FareBasis(recordSubstring(recordType.toString(), 336, 351));
    airlineBean.setClassOfServiceCodeSegment4(recordSubstring(recordType.toString(), 351, 353));
    airlineBean.setFlightNumberSegment4(recordSubstring(recordType.toString(), 353, 357));
    airlineBean.setSegment4Fare(recordSubstring(recordType.toString(), 360, 372));
    airlineBean.setStopoverIndicator5(recordSubstring(recordType.toString(), 372, 373));
    airlineBean.setExchangedOrOriginalTicketNumber(recordSubstring(recordType.toString(), 373, 387));
    rejRecAirlineBean.setAirlineIndustry(airlineBean);
    rejectedTAAAirecord.setReturnedRecordData(rejRecAirlineBean);
  }
  
  public static void setTAALOCRecord(StringBuffer recordType, ConfirmationRejectedRecordBean rejectedLocRecord)
  {
    rejectedLocRecord.setReturnedaRecordType(recordSubstring(recordType.toString(), 0, 3));
    rejectedLocRecord.setReturnedRecordNumber(recordSubstring(recordType.toString(), 3, 11));
    RejectedRecordDataBean rejRecLocaBean = new RejectedRecordDataBean();
    LocationDetailTAABean locationTAABean = new LocationDetailTAABean();
    locationTAABean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
    locationTAABean.setRecordNumber(recordSubstring(recordType.toString(), 3, 11));
    locationTAABean.setTransactionIdentifier(recordSubstring(recordType.toString(), 11, 26));
    locationTAABean.setAddendaTypeCode(recordSubstring(recordType.toString(), 28, 30));
    locationTAABean.setLocationName(recordSubstring(recordType.toString(), 30, 68));
    locationTAABean.setLocationAddress(recordSubstring(recordType.toString(), 68, 106));
    locationTAABean.setLocationCity(recordSubstring(recordType.toString(), 106, 127));
    locationTAABean.setLocationRegion(recordSubstring(recordType.toString(), 127, 130));
    locationTAABean.setLocationCountryCode(recordSubstring(recordType.toString(), 130, 133));
    locationTAABean.setLocationPostalCode(recordSubstring(recordType.toString(), 133, 148));
    locationTAABean.setMerchantCategoryCode(recordSubstring(recordType.toString(), 148, 152));
    locationTAABean.setSellerId(recordSubstring(recordType.toString(), 152, 172));
    rejRecLocaBean.setLocationDetailTAABean(locationTAABean);
    rejectedLocRecord.setReturnedRecordData(rejRecLocaBean);
  }
  
  public static void setTAACommRecord(StringBuffer recordType, ConfirmationRejectedRecordBean rejectedTAACOMMRecord)
  {
    rejectedTAACOMMRecord.setReturnedaRecordType(recordSubstring(recordType.toString(), 0, 3));
    rejectedTAACOMMRecord.setReturnedRecordNumber(recordSubstring(recordType.toString(), 3, 11));
    RejectedRecordDataBean rejRecTAACOMMBean = new RejectedRecordDataBean();
    
    CommunicationServicesIndustryBean communicationBean = new CommunicationServicesIndustryBean();
    communicationBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
    communicationBean.setRecordNumber(recordSubstring(recordType.toString(), 3, 11));
    communicationBean.setTransactionIdentifier(recordSubstring(recordType.toString(), 11, 26));
    communicationBean.setFormatCode(recordSubstring(recordType.toString(), 26, 28));
    communicationBean.setAddendaTypeCode(recordSubstring(recordType.toString(), 28, 30));
    communicationBean.setCallDate(recordSubstring(recordType.toString(), 30, 38));
    communicationBean.setCallTime(recordSubstring(recordType.toString(), 38, 44));
    communicationBean.setCallDurationTime(recordSubstring(recordType.toString(), 44, 50));
    communicationBean.setCallFromLocationName(recordSubstring(recordType.toString(), 50, 68));
    communicationBean.setCallFromRegionCode(recordSubstring(recordType.toString(), 68, 71));
    communicationBean.setCallFromCountryCode(recordSubstring(recordType.toString(), 71, 74));
    communicationBean.setCallFromPhoneNumber(recordSubstring(recordType.toString(), 74, 90));
    communicationBean.setCallToLocationName(recordSubstring(recordType.toString(), 90, 108));
    communicationBean.setCallToRegionCode(recordSubstring(recordType.toString(), 108, 111));
    communicationBean.setCallToCountryCode(recordSubstring(recordType.toString(), 111, 114));
    communicationBean.setCallToPhoneNumber(recordSubstring(recordType.toString(), 114, 130));
    communicationBean.setPhoneCardId(recordSubstring(recordType.toString(), 130, 138));
    communicationBean.setServiceDescription(recordSubstring(recordType.toString(), 138, 158));
    communicationBean.setBillingPeriod(recordSubstring(recordType.toString(), 158, 176));
    communicationBean.setCommunicationsCallTypeCode(recordSubstring(recordType.toString(), 176, 181));
    communicationBean.setCommunicationsRateClass(recordSubstring(recordType.toString(), 181, 182));
    
    rejRecTAACOMMBean.setCommunicationServicesIndustry(communicationBean);
    rejectedTAACOMMRecord.setReturnedRecordData(rejRecTAACOMMBean);
  }
  
  public static void setTAACPSL2Record(StringBuffer recordType, ConfirmationRejectedRecordBean rejectedTAACPSRecord)
  {
    rejectedTAACPSRecord.setReturnedaRecordType(recordSubstring(recordType.toString(), 0, 3));
    rejectedTAACPSRecord.setReturnedRecordNumber(recordSubstring(recordType.toString(), 3, 11));
    RejectedRecordDataBean rejRecTAACPSBean = new RejectedRecordDataBean();
    
    CPSLevel2Bean corporatePurchasingBean = new CPSLevel2Bean();
    corporatePurchasingBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
    corporatePurchasingBean.setRecordNumber(recordSubstring(recordType.toString(), 3, 11));
    corporatePurchasingBean.setTransactionIdentifier(recordSubstring(recordType.toString(), 11, 26));
    corporatePurchasingBean.setAddendaTypeCode(recordSubstring(recordType.toString(), 28, 30));
    corporatePurchasingBean.setRequesterName(recordSubstring(recordType.toString(), 30, 68));
    corporatePurchasingBean.setChargeDescription1(recordSubstring(recordType.toString(), 70, 110));
    corporatePurchasingBean.setChargeItemQuantity1(recordSubstring(recordType.toString(), 110, 113));
    corporatePurchasingBean.setChargeItemAmount1(recordSubstring(recordType.toString(), 116, 128));
    corporatePurchasingBean.setChargeDescription2(recordSubstring(recordType.toString(), 128, 168));
    corporatePurchasingBean.setChargeItemQuantity2(recordSubstring(recordType.toString(), 168, 171));
    corporatePurchasingBean.setChargeItemAmount2(recordSubstring(recordType.toString(), 174, 186));
    corporatePurchasingBean.setChargeDescription3(recordSubstring(recordType.toString(), 186, 226));
    corporatePurchasingBean.setChargeItemQuantity3(recordSubstring(recordType.toString(), 226, 229));
    corporatePurchasingBean.setChargeItemAmount3(recordSubstring(recordType.toString(), 233, 244));
    corporatePurchasingBean.setChargeDescription4(recordSubstring(recordType.toString(), 244, 284));
    corporatePurchasingBean.setChargeItemQuantity4(recordSubstring(recordType.toString(), 284, 287));
    corporatePurchasingBean.setChargeItemAmount4(recordSubstring(recordType.toString(), 290, 302));
    corporatePurchasingBean.setCardMemberReferenceNumber(recordSubstring(recordType.toString(), 302, 319));
    corporatePurchasingBean.setShipToPostalCode(recordSubstring(recordType.toString(), 322, 337));
    corporatePurchasingBean.setTotalTaxAmount(recordSubstring(recordType.toString(), 353, 365));
    corporatePurchasingBean.setTaxTypeCode(recordSubstring(recordType.toString(), 385, 388));
    rejRecTAACPSBean.setCpsLevel2Bean(corporatePurchasingBean);
    rejectedTAACPSRecord.setReturnedRecordData(rejRecTAACPSBean);
  }
  
  public static void setTAADPPRecord(StringBuffer recordType, ConfirmationRejectedRecordBean rejectedTAADPPRecord)
  {
    rejectedTAADPPRecord.setReturnedaRecordType(recordSubstring(recordType.toString(), 0, 3));
    rejectedTAADPPRecord.setReturnedRecordNumber(recordSubstring(recordType.toString(), 3, 11));
    RejectedRecordDataBean rejRecTAADPPBean = new RejectedRecordDataBean();
    
    DeferredPaymentPlanBean dppBean = new DeferredPaymentPlanBean();
    dppBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
    dppBean.setRecordNumber(recordSubstring(recordType.toString(), 3, 11));
    dppBean.setTransactionIdentifier(recordSubstring(recordType.toString(), 11, 26));
    dppBean.setAddendaTypeCode(recordSubstring(recordType.toString(), 28, 30));
    dppBean.setFullTransactionAmount(recordSubstring(recordType.toString(), 33, 45));
    dppBean.setTypeOfPlanCode(recordSubstring(recordType.toString(), 45, 49));
    dppBean.setNoOfInstallments(recordSubstring(recordType.toString(), 49, 53));
    dppBean.setAmountOfInstallment(recordSubstring(recordType.toString(), 56, 68));
    dppBean.setInstallmentNumber(recordSubstring(recordType.toString(), 68, 72));
    dppBean.setContractNumber(recordSubstring(recordType.toString(), 72, 86));
    dppBean.setPaymentTypeCode1(recordSubstring(recordType.toString(), 86, 88));
    dppBean.setPaymentTypeAmount1(recordSubstring(recordType.toString(), 91, 103));
    dppBean.setPaymentTypeCode2(recordSubstring(recordType.toString(), 119, 120));
    dppBean.setPaymentTypeAmount2(recordSubstring(recordType.toString(), 123, 135));
    dppBean.setPaymentTypeCode3(recordSubstring(recordType.toString(), 150, 152));
    dppBean.setPaymentTypeAmount3(recordSubstring(recordType.toString(), 155, 167));
    dppBean.setPaymentTypeCode4(recordSubstring(recordType.toString(), 182, 184));
    dppBean.setPaymentTypeAmount4(recordSubstring(recordType.toString(), 187, 199));
    dppBean.setPaymentTypeCode5(recordSubstring(recordType.toString(), 214, 216));
    dppBean.setPaymentTypeAmount5(recordSubstring(recordType.toString(), 219, 231));
    
    rejRecTAADPPBean.setDppNonIndustry(dppBean);
    rejectedTAADPPRecord.setReturnedRecordData(rejRecTAADPPBean);
  }
  
  public static void setTAAEMVRecord(StringBuffer recordType, ConfirmationRejectedRecordBean rejectedTAAEMVRecord)
  {
    rejectedTAAEMVRecord.setReturnedaRecordType(recordSubstring(recordType.toString(), 0, 3));
    rejectedTAAEMVRecord.setReturnedRecordNumber(recordSubstring(recordType.toString(), 3, 11));
    RejectedRecordDataBean rejRecTAADPPBean = new RejectedRecordDataBean();
    EMVBean emvBean = new EMVBean();
    emvBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
    emvBean.setRecordNumber(recordSubstring(recordType.toString(), 3, 11));
    emvBean.setTransactionIdentifier(recordSubstring(recordType.toString(), 11, 26));
    emvBean.setEMVFormatType(recordSubstring(recordType.toString(), 26, 28));
    emvBean.setAddendaTypeCode(recordSubstring(recordType.toString(), 28, 30));
    emvBean.setICCSystemRelatedData(recordSubstring(recordType.toString(), 30, 286));
    rejRecTAADPPBean.setEmvBean(emvBean);
    rejectedTAAEMVRecord.setReturnedRecordData(rejRecTAADPPBean);
  }
  
  public static void setTAAEntetainRecord(StringBuffer recordType, ConfirmationRejectedRecordBean rejectedTAAEntRecord)
  {
    rejectedTAAEntRecord.setReturnedaRecordType(recordSubstring(recordType.toString(), 0, 3));
    rejectedTAAEntRecord.setReturnedRecordNumber(recordSubstring(recordType.toString(), 3, 11));
    RejectedRecordDataBean rejRecTAAEntBean = new RejectedRecordDataBean();
    EntertainmentTicketingIndustryTAABean entertainmentBean = new EntertainmentTicketingIndustryTAABean();
    entertainmentBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
    entertainmentBean.setRecordNumber(recordSubstring(recordType.toString(), 3, 11));
    entertainmentBean.setTransactionIdentifier(recordSubstring(recordType.toString(), 11, 26));
    entertainmentBean.setFormatCode(recordSubstring(recordType.toString(), 26, 28));
    entertainmentBean.setAddendaTypeCode(recordSubstring(recordType.toString(), 28, 30));
    entertainmentBean.setEventName(recordSubstring(recordType.toString(), 30, 60));
    entertainmentBean.setEventDate(recordSubstring(recordType.toString(), 60, 68));
    entertainmentBean.setEventIndividualTicketPriceAmount(recordSubstring(recordType.toString(), 71, 83));
    entertainmentBean.setEventTicketQuantity(recordSubstring(recordType.toString(), 83, 87));
    entertainmentBean.setEventLocation(recordSubstring(recordType.toString(), 87, 127));
    entertainmentBean.setEventRegionCode(recordSubstring(recordType.toString(), 127, 130));
    entertainmentBean.setEventCountryCode(recordSubstring(recordType.toString(), 130, 133));
    rejRecTAAEntBean.setEntertainmentTicketingIndustry(entertainmentBean);
    rejectedTAAEntRecord.setReturnedRecordData(rejRecTAAEntBean);
  }
  
  public static void setTAAInsuRecord(StringBuffer recordType, ConfirmationRejectedRecordBean rejectedTAAInsuRecord)
  {
    rejectedTAAInsuRecord.setReturnedaRecordType(recordSubstring(recordType.toString(), 0, 3));
    rejectedTAAInsuRecord.setReturnedRecordNumber(recordSubstring(recordType.toString(), 3, 11));
    RejectedRecordDataBean rejRecTAAInsuBean = new RejectedRecordDataBean();
    
    InsuranceIndustryTAABean insuranceBean = new InsuranceIndustryTAABean();
    insuranceBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
    insuranceBean.setRecordNumber(recordSubstring(recordType.toString(), 3, 11));
    insuranceBean.setTransactionIdentifier(recordSubstring(recordType.toString(), 11, 26));
    insuranceBean.setFormatCode(recordSubstring(recordType.toString(), 26, 28));
    insuranceBean.setAddendaTypeCode(recordSubstring(recordType.toString(), 28, 30));
    insuranceBean.setInsurancePolicyNumber(recordSubstring(recordType.toString(), 30, 53));
    insuranceBean.setInsuranceCoverageStartDate(recordSubstring(recordType.toString(), 53, 61));
    insuranceBean.setInsuranceCoverageEndDate(recordSubstring(recordType.toString(), 61, 69));
    insuranceBean.setInsurancePolicyPremiumFrequency(recordSubstring(recordType.toString(), 69, 76));
    insuranceBean.setAdditionalInsurancePolicyNumber(recordSubstring(recordType.toString(), 76, 99));
    insuranceBean.setTypeOfPolicy(recordSubstring(recordType.toString(), 99, 124));
    insuranceBean.setNameOfInsured(recordSubstring(recordType.toString(), 124, 154));
    rejRecTAAInsuBean.setInsuranceIndustry(insuranceBean);
    rejectedTAAInsuRecord.setReturnedRecordData(rejRecTAAInsuBean);
  }
  
  public static void setTAALodgRecord(StringBuffer recordType, ConfirmationRejectedRecordBean rejectedTAALodRecord)
  {
    rejectedTAALodRecord.setReturnedaRecordType(recordSubstring(recordType.toString(), 0, 3));
    rejectedTAALodRecord.setReturnedRecordNumber(recordSubstring(recordType.toString(), 3, 11));
    RejectedRecordDataBean rejRecTAAInsuBean = new RejectedRecordDataBean();
    
    LodgingIndustryTAABean lodgingBean = new LodgingIndustryTAABean();
    lodgingBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
    lodgingBean.setRecordNumber(recordSubstring(recordType.toString(), 3, 11));
    lodgingBean.setTransactionIdentifier(recordSubstring(recordType.toString(), 11, 26));
    lodgingBean.setFormatCode(recordSubstring(recordType.toString(), 26, 28));
    lodgingBean.setAddendaTypeCode(recordSubstring(recordType.toString(), 28, 30));
    lodgingBean.setLodgingSpecialProgramCode(recordSubstring(recordType.toString(), 30, 31));
    lodgingBean.setLodgingCheckInDate(recordSubstring(recordType.toString(), 31, 39));
    lodgingBean.setLodgingCheckOutDate(recordSubstring(recordType.toString(), 39, 47));
    lodgingBean.setLodgingRoomRate1(recordSubstring(recordType.toString(), 50, 62));
    lodgingBean.setNumberOfNightsAtRoomRate1(recordSubstring(recordType.toString(), 62, 64));
    lodgingBean.setLodgingRoomRate2(recordSubstring(recordType.toString(), 67, 79));
    lodgingBean.setNumberOfNightsAtRoomRate2(recordSubstring(recordType.toString(), 79, 81));
    lodgingBean.setLodgingRoomRate3(recordSubstring(recordType.toString(), 84, 96));
    lodgingBean.setNumberOfNightsAtRoomRate3(recordSubstring(recordType.toString(), 96, 98));
    lodgingBean.setLodgingRenterName(recordSubstring(recordType.toString(), 98, 124));
    lodgingBean.setLodgingFolioNumber(recordSubstring(recordType.toString(), 124, 136));
    rejRecTAAInsuBean.setLodgingIndustry(lodgingBean);
    rejectedTAALodRecord.setReturnedRecordData(rejRecTAAInsuBean);
  }
  
  public static void setTAARailRecord(StringBuffer recordType, ConfirmationRejectedRecordBean rejectedTAARailRecord)
  {
    rejectedTAARailRecord.setReturnedaRecordType(recordSubstring(recordType.toString(), 0, 3));
    rejectedTAARailRecord.setReturnedRecordNumber(recordSubstring(recordType.toString(), 3, 11));
    RejectedRecordDataBean rejRecTAARailBean = new RejectedRecordDataBean();
    
    RailIndustryTAABean railBean = new RailIndustryTAABean();
    railBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
    railBean.setRecordNumber(recordSubstring(recordType.toString(), 3, 11));
    railBean.setTransactionIdentifier(recordSubstring(recordType.toString(), 11, 26));
    railBean.setFormatCode(recordSubstring(recordType.toString(), 26, 28));
    railBean.setAddendaTypeCode(recordSubstring(recordType.toString(), 28, 30));
    railBean.setTransactionType(recordSubstring(recordType.toString(), 30, 33));
    railBean.setTicketNumber(recordSubstring(recordType.toString(), 33, 47));
    railBean.setPassengerName(recordSubstring(recordType.toString(), 47, 72));
    railBean.setIataCarrierCode(recordSubstring(recordType.toString(), 72, 75));
    railBean.setTicketIssuerName(recordSubstring(recordType.toString(), 75, 107));
    railBean.setTicketIssueCity(recordSubstring(recordType.toString(), 107, 125));
    railBean.setDepartureStation1(recordSubstring(recordType.toString(), 125, 128));
    railBean.setDepartureDate1(recordSubstring(recordType.toString(), 128, 136));
    railBean.setArrivalStation1(recordSubstring(recordType.toString(), 136, 139));
    railBean.setDepartureStation2(recordSubstring(recordType.toString(), 139, 142));
    railBean.setDepartureDate2(recordSubstring(recordType.toString(), 142, 150));
    railBean.setArrivalStation2(recordSubstring(recordType.toString(), 150, 153));
    railBean.setDepartureStation3(recordSubstring(recordType.toString(), 153, 156));
    railBean.setDepartureDate3(recordSubstring(recordType.toString(), 156, 164));
    railBean.setArrivalStation3(recordSubstring(recordType.toString(), 164, 167));
    railBean.setDepartureStation4(recordSubstring(recordType.toString(), 167, 170));
    railBean.setDepartureDate4(recordSubstring(recordType.toString(), 170, 178));
    railBean.setArrivalStation4(recordSubstring(recordType.toString(), 178, 181));
    rejRecTAARailBean.setRailIndustry(railBean);
    rejectedTAARailRecord.setReturnedRecordData(rejRecTAARailBean);
  }
  
  public static void setTAAReatilRecord(StringBuffer recordType, ConfirmationRejectedRecordBean rejectedTAAReatilRecord)
  {
    rejectedTAAReatilRecord.setReturnedaRecordType(recordSubstring(recordType.toString(), 0, 3));
    rejectedTAAReatilRecord.setReturnedRecordNumber(recordSubstring(recordType.toString(), 3, 11));
    RejectedRecordDataBean rejRecTAAReatilBean = new RejectedRecordDataBean();
    
    RetailIndustryTAABean retailBean = new RetailIndustryTAABean();
    retailBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
    retailBean.setRecordNumber(recordSubstring(recordType.toString(), 3, 11));
    retailBean.setTransactionIdentifier(recordSubstring(recordType.toString(), 11, 26));
    retailBean.setFormatCode(recordSubstring(recordType.toString(), 26, 28));
    retailBean.setAddendaTypeCode(recordSubstring(recordType.toString(), 28, 30));
    retailBean.setRetailDepartmentName(recordSubstring(recordType.toString(), 30, 70));
    
    retailBean.setRetailItemDescription1(recordSubstring(recordType.toString(), 70, 81));
    retailBean.setRetailItemQuantity1(recordSubstring(recordType.toString(), 81, 89));
    retailBean.setRetailItemAmount1(recordSubstring(recordType.toString(), 97, 107));
    retailBean.setRetailItemDescription2(recordSubstring(recordType.toString(), 107, 126));
    retailBean.setRetailItemQuantity2(recordSubstring(recordType.toString(), 126, 129));
    
    retailBean.setRetailItemAmount2(recordSubstring(recordType.toString(), 132, 144));
    retailBean.setRetailItemDescription3(recordSubstring(recordType.toString(), 144, 163));
    retailBean.setRetailItemQuantity3(recordSubstring(recordType.toString(), 163, 166));
    retailBean.setRetailItemAmount3(recordSubstring(recordType.toString(), 170, 181));
    retailBean.setRetailItemDescription4(recordSubstring(recordType.toString(), 181, 200));
    retailBean.setRetailItemQuantity4(recordSubstring(recordType.toString(), 200, 203));
    retailBean.setRetailItemAmount4(recordSubstring(recordType.toString(), 206, 218));
    retailBean.setRetailItemDescription5(recordSubstring(recordType.toString(), 218, 237));
    retailBean.setRetailItemQuantity5(recordSubstring(recordType.toString(), 237, 240));
    retailBean.setRetailItemAmount5(recordSubstring(recordType.toString(), 243, 255));
    retailBean.setRetailItemDescription6(recordSubstring(recordType.toString(), 255, 274));
    retailBean.setRetailItemQuantity6(recordSubstring(recordType.toString(), 274, 277));
    retailBean.setRetailItemAmount6(recordSubstring(recordType.toString(), 280, 292));
    rejRecTAAReatilBean.setRetailIndustry(retailBean);
    rejectedTAAReatilRecord.setReturnedRecordData(rejRecTAAReatilBean);
  }
  
  public static void setTAAAutoRecord(StringBuffer recordType, ConfirmationRejectedRecordBean rejectedTAAAutoRecord)
  {
    rejectedTAAAutoRecord.setReturnedaRecordType(recordSubstring(recordType.toString(), 0, 3));
    rejectedTAAAutoRecord.setReturnedRecordNumber(recordSubstring(recordType.toString(), 3, 11));
    RejectedRecordDataBean rejRecTAAAutoBean = new RejectedRecordDataBean();
    AutoRentalIndustryTAABean autoRentalBean = new AutoRentalIndustryTAABean();
    autoRentalBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
    autoRentalBean.setRecordNumber(recordSubstring(recordType.toString(), 3, 11));
    autoRentalBean.setTransactionIdentifier(recordSubstring(recordType.toString(), 11, 26));
    autoRentalBean.setFormatCode(recordSubstring(recordType.toString(), 26, 28));
    autoRentalBean.setAddendaTypeCode(recordSubstring(recordType.toString(), 28, 30));
    autoRentalBean.setAutoRentalAgreementNumber(recordSubstring(recordType.toString(), 30, 44));
    autoRentalBean.setAutoRentalPickupLocation(recordSubstring(recordType.toString(), 44, 82));
    autoRentalBean.setAutoRentalPickupCityName(recordSubstring(recordType.toString(), 82, 100));
    autoRentalBean.setAutoRentalPickupRegionCode(recordSubstring(recordType.toString(), 103, 106));
    autoRentalBean.setAutoRentalPickupCountryCode(recordSubstring(recordType.toString(), 106, 109));
    autoRentalBean.setAutoRentalPickupDate(recordSubstring(recordType.toString(), 109, 117));
    autoRentalBean.setAutoRentalPickupTime(recordSubstring(recordType.toString(), 117, 123));
    autoRentalBean.setAutoRentalReturnCityName(recordSubstring(recordType.toString(), 123, 141));
    autoRentalBean.setAutoRentalReturnRegionCode(recordSubstring(recordType.toString(), 144, 147));
    autoRentalBean.setAutoRentalReturnCountryCode(recordSubstring(recordType.toString(), 147, 150));
    autoRentalBean.setAutoRentalReturnDate(recordSubstring(recordType.toString(), 150, 158));
    autoRentalBean.setAutoRentalReturnTime(recordSubstring(recordType.toString(), 158, 164));
    autoRentalBean.setAutoRentalRenterName(recordSubstring(recordType.toString(), 164, 190));
    autoRentalBean.setAutoRentalVehicleClassId(recordSubstring(recordType.toString(), 190, 194));
    autoRentalBean.setAutoRentalDistance(recordSubstring(recordType.toString(), 194, 199));
    autoRentalBean.setAutoRentalDistanceUnitOfMeasure(recordSubstring(recordType.toString(), 199, 200));
    autoRentalBean.setAutoRentalAuditAdjustmentIndicator(recordSubstring(recordType.toString(), 200, 201));
    autoRentalBean.setAutoRentalAuditAdjustmentAmount(recordSubstring(recordType.toString(), 204, 216));
    
    rejRecTAAAutoBean.setAutoRentalIndustry(autoRentalBean);
    rejectedTAAAutoRecord.setReturnedRecordData(rejRecTAAAutoBean);
  }
  
  public static void setTAACruiesRecord(StringBuffer recordType, ConfirmationRejectedRecordBean rejectedTAATCruRecord)
  {
    rejectedTAATCruRecord.setReturnedaRecordType(recordSubstring(recordType.toString(), 0, 3));
    rejectedTAATCruRecord.setReturnedRecordNumber(recordSubstring(recordType.toString(), 3, 11));
    RejectedRecordDataBean rejRecTAATCrusBean = new RejectedRecordDataBean();
    TravelCruiseIndustryTAABean travelCruiseBean = new TravelCruiseIndustryTAABean();
    
    travelCruiseBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
    travelCruiseBean.setRecordNumber(recordSubstring(recordType.toString(), 3, 11));
    travelCruiseBean.setTransactionIdentifier(recordSubstring(recordType.toString(), 11, 26));
    travelCruiseBean.setFormatCode(recordSubstring(recordType.toString(), 26, 28));
    travelCruiseBean.setAddendaTypeCode(recordSubstring(recordType.toString(), 28, 30));
    travelCruiseBean.setIataCarrierCode(recordSubstring(recordType.toString(), 30, 33));
    travelCruiseBean.setIataAgencyNumber(recordSubstring(recordType.toString(), 33, 41));
    travelCruiseBean.setTravelPackageIndicator(recordSubstring(recordType.toString(), 41, 42));
    travelCruiseBean.setPassengerName(recordSubstring(recordType.toString(), 42, 67));
    travelCruiseBean.setTravelTicketNumber(recordSubstring(recordType.toString(), 67, 81));
    travelCruiseBean.setTravelDepartureDateSegment1(recordSubstring(recordType.toString(), 81, 89));
    travelCruiseBean.setTravelDestinationCodeSegment1(recordSubstring(recordType.toString(), 89, 92));
    travelCruiseBean.setTravelDepartureAirportSegment1(recordSubstring(recordType.toString(), 92, 95));
    travelCruiseBean.setAirCarrierCodeSegment1(recordSubstring(recordType.toString(), 95, 97));
    travelCruiseBean.setFlightNumberSegment1(recordSubstring(recordType.toString(), 97, 101));
    travelCruiseBean.setClassCodeSegment1(recordSubstring(recordType.toString(), 101, 102));
    travelCruiseBean.setTravelDepartureDateSegment2(recordSubstring(recordType.toString(), 102, 110));
    travelCruiseBean.setTravelDestinationCodeSegment2(recordSubstring(recordType.toString(), 110, 113));
    travelCruiseBean.setTravelDepartureAirportSegment2(recordSubstring(recordType.toString(), 113, 116));
    travelCruiseBean.setAirCarrierCodeSegment2(recordSubstring(recordType.toString(), 116, 118));
    travelCruiseBean.setFlightNumberSegment2(recordSubstring(recordType.toString(), 118, 122));
    travelCruiseBean.setClassCodeSegment2(recordSubstring(recordType.toString(), 122, 123));
    travelCruiseBean.setTravelDepartureDateSegment3(recordSubstring(recordType.toString(), 123, 131));
    travelCruiseBean.setTravelDestinationCodeSegment3(recordSubstring(recordType.toString(), 131, 134));
    travelCruiseBean.setTravelDepartureAirportSegment3(recordSubstring(recordType.toString(), 134, 137));
    travelCruiseBean.setAirCarrierCodeSegment3(recordSubstring(recordType.toString(), 137, 139));
    travelCruiseBean.setFlightNumberSegment3(recordSubstring(recordType.toString(), 139, 143));
    travelCruiseBean.setClassCodeSegment3(recordSubstring(recordType.toString(), 143, 144));
    travelCruiseBean.setTravelDepartureDateSegment4(recordSubstring(recordType.toString(), 144, 152));
    travelCruiseBean.setTravelDestinationCodeSegment4(recordSubstring(recordType.toString(), 152, 155));
    travelCruiseBean.setTravelDepartureAirportSegment4(recordSubstring(recordType.toString(), 155, 158));
    travelCruiseBean.setAirCarrierCodeSegment4(recordSubstring(recordType.toString(), 158, 160));
    travelCruiseBean.setFlightNumberSegment4(recordSubstring(recordType.toString(), 160, 164));
    travelCruiseBean.setClassCodeSegment4(recordSubstring(recordType.toString(), 164, 165));
    travelCruiseBean.setLodgingCheckInDate(recordSubstring(recordType.toString(), 165, 173));
    travelCruiseBean.setLodgingCheckOutDate(recordSubstring(recordType.toString(), 173, 181));
    travelCruiseBean.setLodgingRoomRate1(recordSubstring(recordType.toString(), 184, 196));
    travelCruiseBean.setNumberOfNightsAtRoomRate1(recordSubstring(recordType.toString(), 196, 198));
    travelCruiseBean.setLodgingName(recordSubstring(recordType.toString(), 198, 218));
    travelCruiseBean.setLodgingRegionCode(recordSubstring(recordType.toString(), 218, 221));
    travelCruiseBean.setLodgingCountryCode(recordSubstring(recordType.toString(), 221, 224));
    travelCruiseBean.setLodgingCityName(recordSubstring(recordType.toString(), 224, 242));
    rejRecTAATCrusBean.setTravelCruiseIndustry(travelCruiseBean);
    rejectedTAATCruRecord.setReturnedRecordData(rejRecTAATCrusBean);
  }
  
  private static String recordSubstring(String svalue, int start, int end)
  {
    String substr = svalue.substring(start, end);
    return substr.replaceAll("~", " ");
  }
}
