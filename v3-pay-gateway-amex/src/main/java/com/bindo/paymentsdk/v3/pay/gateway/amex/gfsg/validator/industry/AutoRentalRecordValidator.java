package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.industry;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.ErrorObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceAddendumBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceBasicBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.AutoRentalIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.SubmissionErrorCodes;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.exceptions.SettlementException;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.CommonValidator;
import java.util.List;

public class AutoRentalRecordValidator
{
  private static String recordNumber;
  private static String recordType;
  
  public static void validateAutoRentalRecord(TransactionAdviceBasicBean transactionAdviceBasicType, TransactionAdviceAddendumBean transactionAddendumType, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    AutoRentalIndustryTAABean autoRentalIndustryTAABean = (AutoRentalIndustryTAABean)transactionAddendumType;
    
    recordNumber = autoRentalIndustryTAABean.getRecordNumber();
    recordType = autoRentalIndustryTAABean.getRecordType();
    TAADBRecordValidator.validateRecordType(autoRentalIndustryTAABean
      .getRecordType(), errorCodes);
    TAADBRecordValidator.validateRecordNumber(autoRentalIndustryTAABean
      .getRecordNumber(), errorCodes);
    TAADBRecordValidator.validateTransactionIdentifier(
      autoRentalIndustryTAABean.getTransactionIdentifier(), 
      transactionAdviceBasicType, errorCodes);
    TAADBRecordValidator.validateAddendaTypeCode(autoRentalIndustryTAABean
      .getAddendaTypeCode(), errorCodes);
    TAADBRecordValidator.validateFormatCode(autoRentalIndustryTAABean
      .getFormatCode(), transactionAdviceBasicType, errorCodes);
    validateAutoRentalAgreementNumber(autoRentalIndustryTAABean
      .getAutoRentalAgreementNumber(), errorCodes);
    validateAutoRentalPickupLocation(autoRentalIndustryTAABean
      .getAutoRentalPickupLocation(), errorCodes);
    validateAutoRentalPickupCityName(autoRentalIndustryTAABean
      .getAutoRentalPickupCityName(), errorCodes);
    validateAutoRentalPickupRegionCode(autoRentalIndustryTAABean
      .getAutoRentalPickupRegionCode(), errorCodes);
    validateAutoRentalPickupCountryCode(autoRentalIndustryTAABean
      .getAutoRentalPickupCountryCode(), errorCodes);
    validateAutoRentalPickupDate(autoRentalIndustryTAABean
      .getAutoRentalPickupDate(), errorCodes);
    validateAutoRentalPickupTime(autoRentalIndustryTAABean
      .getAutoRentalPickupTime(), errorCodes);
    validateAutoRentalReturnCityName(autoRentalIndustryTAABean
      .getAutoRentalReturnCityName(), errorCodes);
    validateAutoRentalReturnRegionCode(autoRentalIndustryTAABean
      .getAutoRentalReturnRegionCode(), errorCodes);
    validateAutoRentalReturnCountryCode(autoRentalIndustryTAABean
      .getAutoRentalReturnCountryCode(), errorCodes);
    validateAutoRentalReturnDate(autoRentalIndustryTAABean, errorCodes);
    validateAutoRentalReturnTime(autoRentalIndustryTAABean
      .getAutoRentalReturnTime(), errorCodes);
    validateAutoRentalRenterName(autoRentalIndustryTAABean
      .getAutoRentalRenterName(), errorCodes);
    validateAutoRentalVehicleClassId(autoRentalIndustryTAABean
      .getAutoRentalVehicleClassId(), errorCodes);
    validateAutoRentalDistance(autoRentalIndustryTAABean, errorCodes);
    validateAutoRentalDistanceUnitOfMeasure(autoRentalIndustryTAABean, 
      errorCodes);
    validateAutoRentalAuditAdjustmentIndicator(autoRentalIndustryTAABean, 
      errorCodes);
    validateAutoRentalAuditAdjustmentAmount(autoRentalIndustryTAABean, 
      errorCodes);
    validateReturnDropoffLocation(autoRentalIndustryTAABean, 
      errorCodes);
    validateVehicleIdentificationNumber(autoRentalIndustryTAABean, 
      errorCodes);
    validateDriverIdentificationNumber(autoRentalIndustryTAABean, 
      errorCodes);
    validateDriverTaxNumber(autoRentalIndustryTAABean, 
      errorCodes);
  }
  
  private static void validateAutoRentalAgreementNumber(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD239_AUTORENTAL_AGREEMENTNUMBER
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD239_AUTORENTAL_AGREEMENTNUMBER
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "AutoRentalAgreementNumber" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else
    {
      String reqLength = 
        CommonValidator.validateLength(value, 14, 14);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD239_AUTORENTAL_AGREEMENTNUMBER
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD239_AUTORENTAL_AGREEMENTNUMBER
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "AutoRentalAgreementNumber" + 
          "|" + 
          "This field length Cannot be greater than 14");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateAutoRentalPickupLocation(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 38, 38);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject("AUTO", 
          "AutoRentalPickupLocation Length is out of Sequence");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateAutoRentalPickupCityName(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD240_AUTORENTAL_PICKUPCITYNAME);
      errorCodes.add(errorObj);
    }
    else
    {
      String reqLength = 
        CommonValidator.validateLength(value, 18, 18);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject("AUTO", 
          "AutoRentalPickupCityName Length is out of Sequence");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateAutoRentalPickupRegionCode(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD241_AUTORENTAL_PICKUPREGIONCODE);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['þ']))
    {
      String reqLength = CommonValidator.validateLength(value, 3, 3);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject("AUTO", 
          "AutoRentalPickupRegionCode Length is out of Sequence");
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject("AUTO", 
        "AutoRentalPickupRegionCode Type is Missing");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateAutoRentalPickupCountryCode(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD242_AUTORENTAL_PICKUPCOUNTRYCODE);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ÿ']))
    {
      String reqLength = CommonValidator.validateLength(value, 3, 3);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD242_AUTORENTAL_PICKUPCOUNTRYCODE);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD242_AUTORENTAL_PICKUPCOUNTRYCODE);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateAutoRentalPickupDate(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD243_AUTORENTAL_PICKUPDATE);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ā']))
    {
      String reqLength = CommonValidator.validateLength(value, 8, 8);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD243_AUTORENTAL_PICKUPDATE);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD243_AUTORENTAL_PICKUPDATE);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateAutoRentalPickupTime(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ā']))
      {
        String reqLength = CommonValidator.validateLength(value, 6, 6);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD244_AUTORENTAL_PICKUPTIME);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD244_AUTORENTAL_PICKUPTIME);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateAutoRentalReturnCityName(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD245_AUTORENTAL_RETURNCITYNAME);
      errorCodes.add(errorObj);
    }
    else
    {
      String reqLength = 
        CommonValidator.validateLength(value, 18, 18);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject("AUTO", 
          "AUTO RentalReturnCityName Length is Out Of Sequence");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateAutoRentalReturnRegionCode(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD246_AUTORENTAL_RETURNREGIONCODE);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ą']))
    {
      String reqLength = CommonValidator.validateLength(value, 3, 3);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD246_AUTORENTAL_RETURNREGIONCODE);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD246_AUTORENTAL_RETURNREGIONCODE);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateAutoRentalReturnCountryCode(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD247_AUTORENTAL_RETURNCOUNTRYCODE);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ą']))
    {
      String reqLength = CommonValidator.validateLength(value, 3, 3);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD247_AUTORENTAL_RETURNCOUNTRYCODE);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD247_AUTORENTAL_RETURNCOUNTRYCODE);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateAutoRentalReturnDate(AutoRentalIndustryTAABean autoRentalIndustryTAABean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    
    String returnDate = autoRentalIndustryTAABean.getAutoRentalReturnDate();
    if (CommonValidator.isNullOrEmpty(returnDate))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD248_AUTORENTAL_RETURNDATE);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(returnDate, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ć']))
    {
      String reqLength = CommonValidator.validateLength(returnDate, 
        8, 8);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD248_AUTORENTAL_RETURNDATE);
        errorCodes.add(errorObj);
      }
      else if (Integer.parseInt(returnDate) < Integer.parseInt(autoRentalIndustryTAABean
        .getAutoRentalPickupDate()))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD249_AUTORENTAL_RETURNDATE);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD248_AUTORENTAL_RETURNDATE);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateAutoRentalReturnTime(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject("AUTO", 
        "AUTO RentalReturnTime is Missing");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ć']))
    {
      String reqLength = CommonValidator.validateLength(value, 6, 6);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD250_AUTO_RENTALRETURNTIME);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD250_AUTO_RENTALRETURNTIME);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateAutoRentalRenterName(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD251_AUTO_RENTAL_RENTERNAME);
      errorCodes.add(errorObj);
    }
    else
    {
      String reqLength = 
        CommonValidator.validateLength(value, 26, 26);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject("AUTO", 
          "AUTO AutoRentalRenterName Length is Out Of Sequence ");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateAutoRentalVehicleClassId(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ĉ']))
      {
        String reqLength = CommonValidator.validateLength(value, 4, 4);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD252_AUTO_RENTAL_VEHICLECLASSID);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD252_AUTO_RENTAL_VEHICLECLASSID);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateAutoRentalDistance(AutoRentalIndustryTAABean autoRentalIndustryTAABean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(autoRentalIndustryTAABean.getAutoRentalDistance())) {
      if (!CommonValidator.isNullOrEmpty(autoRentalIndustryTAABean.getAutoRentalDistanceUnitOfMeasure()))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD254_AUTORENTAL_DISTANCE
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD254_AUTORENTAL_DISTANCE
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "AutoRentalDistance" + 
          "|" + 
          SubmissionErrorCodes.ERROR_DATAFIELD254_AUTORENTAL_DISTANCE
          .getErrorDescription());
        errorCodes.add(errorObj);
        
        return;
      }
    }
    if (!CommonValidator.isNullOrEmpty(autoRentalIndustryTAABean.getAutoRentalDistance())) {
      if (CommonValidator.validateData(autoRentalIndustryTAABean.getAutoRentalDistance(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ċ']))
      {
        String reqLength = 
          CommonValidator.validateLength(autoRentalIndustryTAABean
          .getAutoRentalDistance(), 5, 5);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD0253_AUTORENTAL_DISTANCE
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD0253_AUTORENTAL_DISTANCE
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            recordType + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "AutoRentalDistance" + 
            "|" + 
            "This field length Cannot be greater than 5");
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD253_AUTORENTAL_DISTANCE
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD253_AUTORENTAL_DISTANCE
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "AutoRentalDistance" + 
          "|" + 
          "This field can only be Numeric");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateAutoRentalDistanceUnitOfMeasure(AutoRentalIndustryTAABean autoRentalIndustryTAABean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(autoRentalIndustryTAABean.getAutoRentalDistanceUnitOfMeasure())) {
      if (!CommonValidator.isNullOrEmpty(autoRentalIndustryTAABean.getAutoRentalDistance()))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD256_AUTORENTAL_DISTANCEUNITOFMEASURE
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD256_AUTORENTAL_DISTANCEUNITOFMEASURE
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "AutoRentalDistanceUnitOfMeasure" + 
          "|" + 
          SubmissionErrorCodes.ERROR_DATAFIELD256_AUTORENTAL_DISTANCEUNITOFMEASURE
          .getErrorDescription());
        errorCodes.add(errorObj);
        
        return;
      }
    }
    if (!CommonValidator.isNullOrEmpty(autoRentalIndustryTAABean.getAutoRentalDistanceUnitOfMeasure())) {
      if (!CommonValidator.isAutoRentalDistanceUnitOfMessure(autoRentalIndustryTAABean.getAutoRentalDistanceUnitOfMeasure()))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD255_AUTORENTAL_DISTANCEUNITOFMEASURE
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD255_AUTORENTAL_DISTANCEUNITOFMEASURE
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "AutoRentalDistanceUnitOfMeasure" + 
          "|" + 
          SubmissionErrorCodes.ERROR_DATAFIELD255_AUTORENTAL_DISTANCEUNITOFMEASURE
          .getErrorDescription());
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateAutoRentalAuditAdjustmentIndicator(AutoRentalIndustryTAABean autoIndustryTAABean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(autoIndustryTAABean.getAutoRentalAuditAdjustmentIndicator())) {
      if (!autoIndustryTAABean.getAutoRentalAuditAdjustmentIndicator().equalsIgnoreCase("X")) {
        if (!autoIndustryTAABean.getAutoRentalAuditAdjustmentIndicator().equalsIgnoreCase("Y")) {
          if (!autoIndustryTAABean.getAutoRentalAuditAdjustmentIndicator().equalsIgnoreCase("Z"))
          {
            errorObj = new ErrorObject(
              SubmissionErrorCodes.ERROR_DATAFIELD257_AUTORENTAL_AUDITADJUSTMENTINDICATOR);
            errorCodes.add(errorObj);
            
            return;
          }
        }
      }
    }
    if (!CommonValidator.isNullOrEmpty(autoIndustryTAABean.getAutoRentalAuditAdjustmentIndicator())) {
      if (CommonValidator.validateData(autoIndustryTAABean.getAutoRentalAuditAdjustmentIndicator(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Č']))
      {
        String reqLength = CommonValidator.validateLength(
          autoIndustryTAABean
          .getAutoRentalAuditAdjustmentIndicator(), 1, 1);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD257_AUTORENTAL_AUDITADJUSTMENTINDICATOR);
          errorCodes.add(errorObj);
        }
        else if (CommonValidator.isNullOrEmpty(autoIndustryTAABean.getAutoRentalAuditAdjustmentAmount()))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD261_AUTORENTAL_AUDITADJUSTMENTAMOUNT);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD257_AUTORENTAL_AUDITADJUSTMENTINDICATOR);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateAutoRentalAuditAdjustmentAmount(AutoRentalIndustryTAABean autoRentalIndustryTAABean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(autoRentalIndustryTAABean.getAutoRentalAuditAdjustmentAmount())) {
      if (CommonValidator.validateData(autoRentalIndustryTAABean.getAutoRentalAuditAdjustmentAmount(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ď']))
      {
        String reqLength = CommonValidator.validateLength(
          autoRentalIndustryTAABean
          .getAutoRentalAuditAdjustmentAmount(), 12, 12);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD260_AUTORENTAL_AUDITADJUSTMENTAMOUNT);
          errorCodes.add(errorObj);
        }
        else if (CommonValidator.isNullOrEmpty(autoRentalIndustryTAABean.getAutoRentalAuditAdjustmentIndicator()))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD258_AUTORENTAL_AUDITADJUSTMENTINDICATOR);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD259_AUTORENTAL_AUDITADJUSTMENTAMOUNT);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateReturnDropoffLocation(AutoRentalIndustryTAABean autoRentalIndustryTAABean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(autoRentalIndustryTAABean.getReturnDropoffLocation()))
    {
      String reqLength = CommonValidator.validateLength(
        autoRentalIndustryTAABean
        .getReturnDropoffLocation(), 38, 38);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD613_AUTORENTAL_RETURNDROPOFFLOCATION);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateVehicleIdentificationNumber(AutoRentalIndustryTAABean autoRentalIndustryTAABean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(autoRentalIndustryTAABean.getVehicleIdentificationNumber()))
    {
      String reqLength = CommonValidator.validateLength(
        autoRentalIndustryTAABean
        .getVehicleIdentificationNumber(), 20, 20);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD614_AUTORENTAL_VEHICLEIDNUMBER);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateDriverIdentificationNumber(AutoRentalIndustryTAABean autoRentalIndustryTAABean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(autoRentalIndustryTAABean.getDriverIdentificationNumber()))
    {
      String reqLength = CommonValidator.validateLength(
        autoRentalIndustryTAABean
        .getDriverIdentificationNumber(), 20, 20);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD615_AUTORENTAL_DRIVERIDNUMBER);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateDriverTaxNumber(AutoRentalIndustryTAABean autoRentalIndustryTAABean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(autoRentalIndustryTAABean.getDriverTaxNumber()))
    {
      String reqLength = CommonValidator.validateLength(
        autoRentalIndustryTAABean
        .getDriverTaxNumber(), 20, 20);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD616_AUTORENTAL_DRIVERTAXNUMBER);
        errorCodes.add(errorObj);
      }
    }
  }
}
