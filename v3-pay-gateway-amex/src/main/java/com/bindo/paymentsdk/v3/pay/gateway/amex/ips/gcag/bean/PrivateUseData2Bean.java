package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean;

import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.connection.PropertyReader;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.Constants;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.AuthErrorCodes;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.utils.ISOUtil;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.validator.Validator;


public class PrivateUseData2Bean
{
  private String vli;
  private String service_identifier;
  private String request_type_identifier;
  private String cm_billing_postalcode;
  private String cm_billing_address;
  private String cm_first_name;
  private String cm_last_name;
  private String cm_billing_phonenumber;
  private String ship_to_postalcode;
  private String ship_to_address;
  private String ship_to_firstname;
  private String ship_to_lastname;
  private String ship_to_phonenumber;
  private String ship_to_countrycode;

  public String getVli()
  {
    return this.vli;
  }
  
  public void setVli(String vli)
  {
    this.vli = vli;
  }
  
  public String getServiceIdentifier()
  {
    return this.service_identifier;
  }
  
  public void setServiceIdentifier(String service_identifier)
  {
    this.service_identifier = service_identifier;
  }
  
  public String getRequestTypeIdentifier()
  {
    return this.request_type_identifier;
  }
  
  public void setRequestTypeIdentifier(String request_type_identifier)
  {
    this.request_type_identifier = request_type_identifier;
  }
  
  public String getCmBillingPostalcode()
  {
    return this.cm_billing_postalcode;
  }
  
  public void setCmBillingPostalcode(String cm_billing_postalcode)
  {
    this.cm_billing_postalcode = cm_billing_postalcode;
  }
  
  public String getCmBillingAddress()
  {
    return this.cm_billing_address;
  }
  
  public void setCmBillingAddress(String cm_billing_address)
  {
    this.cm_billing_address = cm_billing_address;
  }
  
  public String getCmFirstName()
  {
    return this.cm_first_name;
  }
  
  public void setCmFirstName(String cm_first_name)
  {
    this.cm_first_name = cm_first_name;
  }
  
  public String getCmLastName()
  {
    return this.cm_last_name;
  }
  
  public void setCmLastName(String cm_last_name)
  {
    this.cm_last_name = cm_last_name;
  }
  
  public String getCmBillingPhoneNumber()
  {
    return this.cm_billing_phonenumber;
  }
  
  public void setCmBillingPhoneNumber(String cm_billing_phonenumber)
  {
    this.cm_billing_phonenumber = cm_billing_phonenumber;
  }
  
  public String getShipToPostalCode()
  {
    return this.ship_to_postalcode;
  }
  
  public void setShipToPostalCode(String ship_to_postalcode)
  {
    this.ship_to_postalcode = ship_to_postalcode;
  }
  
  public String getShipToAddress()
  {
    return this.ship_to_address;
  }
  
  public void setShipToAddress(String ship_to_address)
  {
    this.ship_to_address = ship_to_address;
  }
  
  public String getShipToFirstName()
  {
    return this.ship_to_firstname;
  }
  
  public void setShipToFirstname(String ship_to_firstname)
  {
    this.ship_to_firstname = ship_to_firstname;
  }
  
  public String getShipToLastName()
  {
    return this.ship_to_lastname;
  }
  
  public void setShipToLastName(String ship_to_lastname)
  {
    this.ship_to_lastname = ship_to_lastname;
  }
  
  public String getShipToPhoneNumber()
  {
    return this.ship_to_phonenumber;
  }
  
  public void setShipToPhoneNumber(String ship_to_phonenumber)
  {
    this.ship_to_phonenumber = ship_to_phonenumber;
  }
  
  public String getShipToCountryCode()
  {
    return this.ship_to_countrycode;
  }
  
  public void setShipToCountryCode(String ship_to_countrycode)
  {
    this.ship_to_countrycode = ship_to_countrycode;
  }
  
  public String populatePrivateUseData(AuthorizationResponseBean authorizationResponseBean)
  {
    StringBuffer privateData1 = new StringBuffer();
    
    StringBuffer displayBuffer = new StringBuffer();
    if (ISOUtil.IsNullOrEmpty(getServiceIdentifier()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD63_PRIVATEUSEDATA2SERVICEIDENTIFIER);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if (Validator.validateSubFields(getServiceIdentifier(), 2, 2, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1]))
    {
      privateData1.append(getServiceIdentifier());
      displayBuffer.append("SERVICE_IDENTIFIER            ").append(" :: ").append(getServiceIdentifier()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD63_PRIVATEUSEDATA2SERVICEIDENTIFIER);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getRequestTypeIdentifier()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD63_PRIVATEUSEDATA2REQUESTTYPEIDENTIFIER);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if (Validator.validateSubFields(getRequestTypeIdentifier(), 2, 2, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1]))
    {
      privateData1.append(getRequestTypeIdentifier());
      displayBuffer.append("REQUEST_TYPE_IDENTIFIER       ").append(" :: ").append(getRequestTypeIdentifier()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD63_PRIVATEUSEDATA2REQUESTTYPEIDENTIFIER);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if ((!ISOUtil.IsNullOrEmpty(getCmBillingPostalcode())) && 
      (!Validator.validateSubFields(getCmBillingPostalcode(), 9, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2])))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD63_PRIVATEUSEDATA2CARDMEMBERBILLINGPOSTALCODE);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if ((!ISOUtil.IsNullOrEmpty(getCmBillingAddress())) && 
      (!Validator.validateSubFields(getCmBillingAddress(), 20, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2])))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD63_PRIVATEUSEDATA2CARDMEMBERBILLINGADDRESS);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if ((!ISOUtil.IsNullOrEmpty(getCmFirstName())) && 
      (!Validator.validateSubFields(getCmFirstName(), 15, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2])))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD63_PRIVATEUSEDATA2CARDMEMBERFIRSTNAME);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if ((!ISOUtil.IsNullOrEmpty(getCmLastName())) && 
      (!Validator.validateSubFields(getCmLastName(), 30, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2])))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD63_PRIVATEUSEDATA2CARDMEMBERLASTNAME);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if ((!ISOUtil.IsNullOrEmpty(getCmBillingPhoneNumber())) && 
      (!Validator.validateSubFields(getCmBillingPhoneNumber(), 10, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2])))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD63_PRIVATEUSEDATA2CARDMEMBERBILLINGPHONENUMBER);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if ((!ISOUtil.IsNullOrEmpty(getShipToPostalCode())) && 
      (!Validator.validateSubFields(getShipToPostalCode(), 9, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2])))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD63_PRIVATEUSEDATA2SHIPTOPOSTALCODE);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if ((!ISOUtil.IsNullOrEmpty(getShipToAddress())) && 
      (!Validator.validateSubFields(getShipToAddress(), 50, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2])))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD63_PRIVATEUSEDATA2SHIPTOPOSTALADDRESS);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if ((!ISOUtil.IsNullOrEmpty(getShipToFirstName())) && 
      (!Validator.validateSubFields(getShipToFirstName(), 15, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2])))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD63_PRIVATEUSEDATA2SHIPTOPOSTALFIRSTNAME);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if ((!ISOUtil.IsNullOrEmpty(getShipToLastName())) && 
      (!Validator.validateSubFields(getShipToLastName(), 30, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2])))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD63_PRIVATEUSEDATA2SHIPTOPOSTALLASTNAME);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if ((!ISOUtil.IsNullOrEmpty(getShipToPhoneNumber())) && 
      (!Validator.validateSubFields(getShipToPhoneNumber(), 10, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[2])))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD63_PRIVATEUSEDATA2SHIPTOPOSTALPHONENUMBER);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if ((!ISOUtil.IsNullOrEmpty(getShipToCountryCode())) && 
      (!Validator.validateSubFields(getShipToCountryCode(), 3, 3, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[0])))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD63_PRIVATEUSEDATA2SHIPTOPOSTALCOUNTRYCODE);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    String billingPostalCode = ISOUtil.padString(getCmBillingPostalcode(), 9, " ", true, false);
    privateData1.append(billingPostalCode);
    displayBuffer.append("CM_BILLING_POSTAL_CODE        ").append(" :: ").append(billingPostalCode).append(Constants.NEW_LINE_CHAR);
    String billingAddress = ISOUtil.padString(getCmBillingAddress(), 20, " ", true, false);
    privateData1.append(billingAddress);
    displayBuffer.append("CM_BILLING_ADDRESS            ").append(" :: ").append(billingAddress).append(Constants.NEW_LINE_CHAR);
    boolean isBillingNameAdded = false;
    if ((!ISOUtil.IsNullOrEmpty(getCmFirstName())) || (!ISOUtil.IsNullOrEmpty(getCmLastName())))
    {
      String cmFirstName = ISOUtil.padString(getCmFirstName(), 15, " ", true, false);
      privateData1.append(cmFirstName);
      displayBuffer.append("CM_FIRST_NAME                 ").append(" :: ").append(cmFirstName).append(Constants.NEW_LINE_CHAR);
      String cmLastName = ISOUtil.padString(getCmLastName(), 30, " ", true, false);
      privateData1.append(cmLastName);
      displayBuffer.append("CM_LAST_NAME                  ").append(" :: ").append(cmLastName).append(Constants.NEW_LINE_CHAR);
      isBillingNameAdded = true;
    }
    if ((!ISOUtil.IsNullOrEmpty(getCmBillingPhoneNumber())) || 
      (!ISOUtil.IsNullOrEmpty(getShipToPostalCode())) || 
      (!ISOUtil.IsNullOrEmpty(getShipToAddress())) || 
      (!ISOUtil.IsNullOrEmpty(getShipToFirstName())) || 
      (!ISOUtil.IsNullOrEmpty(getShipToLastName())) || 
      (!ISOUtil.IsNullOrEmpty(getShipToPhoneNumber())) || 
      (!ISOUtil.IsNullOrEmpty(getShipToCountryCode())))
    {
      if (!isBillingNameAdded)
      {
        String cmFirstName = ISOUtil.padString(getCmFirstName(), 15, " ", true, false);
        privateData1.append(cmFirstName);
        displayBuffer.append("CM_FIRST_NAME                ").append(" :: ").append(cmFirstName).append(Constants.NEW_LINE_CHAR);
        String cmLastName = ISOUtil.padString(getCmLastName(), 30, " ", true, false);
        privateData1.append(cmLastName);
        displayBuffer.append("CM_LAST_NAME                 ").append(" :: ").append(cmLastName).append(Constants.NEW_LINE_CHAR);
        isBillingNameAdded = true;
      }
      String cmBillingPhoneNo = ISOUtil.padString(getCmBillingPhoneNumber(), 10, " ", true, false);
      privateData1.append(cmBillingPhoneNo);
      displayBuffer.append("CM_BILLING_PHONE_NUMBR                ").append(" :: ").append(cmBillingPhoneNo).append(Constants.NEW_LINE_CHAR);
      String shipToPostalCode = ISOUtil.padString(getShipToPostalCode(), 9, " ", true, false);
      privateData1.append(shipToPostalCode);
      displayBuffer.append("SHIP_TO_POST_NUMBR            ").append(" :: ").append(shipToPostalCode).append(Constants.NEW_LINE_CHAR);
      String shipToAddress = ISOUtil.padString(getShipToAddress(), 50, " ", true, false);
      privateData1.append(shipToAddress);
      displayBuffer.append("SHIP_TO_ADDRESS               ").append(" :: ").append(shipToAddress).append(Constants.NEW_LINE_CHAR);
      String shipToFirstName = ISOUtil.padString(getShipToFirstName(), 15, " ", true, false);
      privateData1.append(shipToFirstName);
      displayBuffer.append("SHIP_TO_FIRST_NAME            ").append(" :: ").append(shipToFirstName).append(Constants.NEW_LINE_CHAR);
      String shipToLastName = ISOUtil.padString(getShipToLastName(), 30, " ", true, false);
      privateData1.append(shipToLastName);
      displayBuffer.append("SHIP_TO_LAST_NAME             ").append(" :: ").append(shipToLastName).append(Constants.NEW_LINE_CHAR);
      String shipToPhoneNo = ISOUtil.padString(getShipToPhoneNumber(), 10, " ", true, false);
      privateData1.append(shipToPhoneNo);
      displayBuffer.append("SHIP_TO_PHONE_NUM             ").append(" :: ").append(shipToPhoneNo).append(Constants.NEW_LINE_CHAR);
      String shipToCountryCode = ISOUtil.padString(getShipToCountryCode(), 3, " ", true, false);
      privateData1.append(shipToCountryCode);
      displayBuffer.append("SHIP_TO_COUNTRY_CODE          ").append(" :: ").append(shipToCountryCode).append(Constants.NEW_LINE_CHAR);
    }
    boolean debugflag = PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON");
    if (debugflag)
    {
      Log.i("Private Use Data 2 Bean\r\n----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n-                                 Private Use Data 2 Bean                                                                                                                                       -\r\n---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n" +
      
        displayBuffer.toString() + 
        "\r\n" + "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
      
      Log.i("Populated Private Use Data is -- " + privateData1 + "length" + privateData1.length());
    }
    return privateData1.toString();
  }
  
  public String toString()
  {
    StringBuffer buffer = new StringBuffer();
    if (!ISOUtil.IsNullOrEmpty(getVli())) {
      buffer.append("VARIABLE_DATA_LENGTH").append(" :: ").append(getVli()).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getServiceIdentifier())) {
      buffer.append("SERVICE_IDENTIFIER").append(" :: ").append(getServiceIdentifier()).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getRequestTypeIdentifier())) {
      buffer.append("REQUEST_TYPE_IDENTIFIER").append(" :: ").append(getRequestTypeIdentifier()).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getCmBillingPostalcode())) {
      buffer.append("CM_BILLING_POSTAL_CODE").append(" :: ").append(getCmBillingPostalcode()).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getCmBillingAddress())) {
      buffer.append("CM_BILLING_ADDRESS").append(" :: ").append(getCmBillingAddress()).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getCmFirstName())) {
      buffer.append("CM_FIRST_NAME").append(" :: ").append(getCmFirstName()).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getCmLastName())) {
      buffer.append("CM_LAST_NAME").append(" :: ").append(getCmLastName()).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getCmBillingPhoneNumber())) {
      buffer.append("CM_BILLING_PHONE").append(" :: ").append(getCmBillingPhoneNumber()).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getShipToPostalCode())) {
      buffer.append("SHIP_TO_POSTAL_CODE").append(" :: ").append(getShipToPostalCode()).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getShipToAddress())) {
      buffer.append("SHIP_TO_ADDRESS").append(" :: ").append(getShipToAddress()).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getShipToFirstName())) {
      buffer.append("SHIP_TO_FIRST_NAME").append(" :: ").append(getShipToFirstName()).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getShipToLastName())) {
      buffer.append("SHIP_TO_LAST_NAME").append(" :: ").append(getShipToLastName()).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getShipToPhoneNumber())) {
      buffer.append("SHIP_TO_PHONE").append(" :: ").append(getShipToPhoneNumber()).append(Constants.NEW_LINE_CHAR);
    }
    if (!ISOUtil.IsNullOrEmpty(getShipToCountryCode())) {
      buffer.append("SHIP_TO_COUNTRY_CODE").append(" :: ").append(getShipToCountryCode()).append(Constants.NEW_LINE_CHAR);
    }
    return buffer.toString();
  }
}
