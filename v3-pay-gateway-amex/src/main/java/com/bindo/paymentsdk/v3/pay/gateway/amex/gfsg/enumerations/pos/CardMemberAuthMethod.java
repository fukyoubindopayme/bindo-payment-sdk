package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.pos;

public enum CardMemberAuthMethod {
    POC_UNKONOWN("0"),
    POC_PINL("1"),
    POC_ELECTRONIC_SGNATURE_ANALYSIS("2"),
    POC_BIOMETRICS("3"),
    POC_BIOGRAPHIC("4"),
    POC_MANUAL_SIGN_VERIFICATION("5"),
    POC_OTHER_MANUAL_VERIFICATION("6"),
    RESERVE_FOR_ISO_USE_7("7"),
    RESERVE_FOR_NATIONAL_USE_8("8"),
    RESERVE_FOR_PRIVATE_USE_9("9"),
    RESERVE_FOR_ISO_USE_A("A"),
    RESERVE_FOR_ISO_USE_B("B"),
    RESERVE_FOR_ISO_USE_C("C"),
    RESERVE_FOR_ISO_USE_D("D"),
    RESERVE_FOR_ISO_USE_E("E"),
    RESERVE_FOR_ISO_USE_F("F"),
    RESERVE_FOR_ISO_USE_G("G"),
    RESERVE_FOR_ISO_USE_H("H"),
    RESERVE_FOR_ISO_USE_I("I"),
    RESERVE_FOR_NATIONAL_USE_J("J"),
    RESERVE_FOR_NATIONAL_USE_K("K"),
    RESERVE_FOR_NATIONAL_USE_L("L"),
    RESERVE_FOR_NATIONAL_USE_M("M"),
    RESERVE_FOR_NATIONAL_USE_N("N"),
    RESERVE_FOR_NATIONAL_USE_O("O"),
    RESERVE_FOR_NATIONAL_USE_P("P"),
    RESERVE_FOR_NATIONAL_USE_Q("Q"),
    RESERVE_FOR_NATIONAL_USE_R("R"),
    POC_ELECTRONIC_TICKET_ENVIRONMENT("S"),
    RESERVE_FOR_PRIVATE_USE_T("T"),
    RESERVE_FOR_PRIVATE_USE_U("U"),
    RESERVE_FOR_PRIVATE_USE_V("V"),
    RESERVE_FOR_PRIVATE_USE_W("W"),
    RESERVE_FOR_PRIVATE_USE_X("X"),
    RESERVE_FOR_PRIVATE_USE_Y("Y"),
    RESERVE_FOR_PRIVATE_USE_Z("Z");

    private final String cardMemberAuthMethod;

    private CardMemberAuthMethod(String crdMemberAuthMeth) {
        this.cardMemberAuthMethod = crdMemberAuthMeth;
    }

    public String getCardMemberAuthMethod() {
        return this.cardMemberAuthMethod;
    }
}
