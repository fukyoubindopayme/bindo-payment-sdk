package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.connection.PropertyReader;
import org.apache.log4j.Logger;

public class CheckDigitValidator
{
  private static final String BLANK = "";
  private static final Logger LOGGER = Logger.getLogger(CheckDigitValidator.class);
  
  public static boolean modulusNineCheck(String number)
  {
    boolean isValid = false;
    if (number.length() == 10)
    {
      int digits = number.length();
      
      long checkDigit = 0L;
      try
      {
        checkDigit = Integer.parseInt(String.valueOf(number.charAt(digits - 1)));
      }
      catch (NumberFormatException nfe)
      {
        if (PropertyReader.getDebuggerFlag().equals("ON")) {
          LOGGER.fatal("NumberFormatException in modulusNineCheck method of class CheckDigitValidator: " + nfe);
        }
        return false;
      }
      number = number.substring(0, digits - 1);
      
      int intfirstThreeDigits = 0;
      try
      {
        intfirstThreeDigits = Integer.parseInt(number.substring(0, 3));
      }
      catch (NumberFormatException nfe)
      {
        if (PropertyReader.getDebuggerFlag().equals("ON")) {
          LOGGER.fatal("NumberFormatException in modulusNineCheck method of class CheckDigitValidator: " + nfe);
        }
        return false;
      }
      if ((intfirstThreeDigits < 930) || (intfirstThreeDigits > 939))
      {
        String strDigits = number.substring(1, number.length());
        number = "0" + strDigits;
      }
      digits = number.length();
      long sum = 0L;
      long oddsum = 0L;
      for (int count = 0; count < digits; count++)
      {
        int digit = 0;
        if (count % 2 == 0)
        {
          try
          {
            digit = Integer.parseInt(String.valueOf(number.charAt(count))) * 2;
          }
          catch (NumberFormatException nfe)
          {
            if (PropertyReader.getDebuggerFlag().equals("ON")) {
              LOGGER.fatal("NumberFormatException in modulusNineCheck method of class CheckDigitValidator: " + nfe);
            }
            return false;
          }
          if (digit > 9) {
            digit -= 9;
          }
          sum += digit;
        }
        else
        {
          try
          {
            oddsum += Integer.parseInt(String.valueOf(number.charAt(count)));
          }
          catch (NumberFormatException nfe)
          {
            if (PropertyReader.getDebuggerFlag().equals("ON")) {
              LOGGER.fatal("NumberFormatException in modulusNineCheck method of class CheckDigitValidator: " + nfe);
            }
            return false;
          }
        }
      }
      sum += oddsum;
      if ((sum % 10L == 0L) && (checkDigit == 0L)) {
        isValid = true;
      }
      if (sum % 10L != 0L)
      {
        long newSum = sum + (10L - sum % 10L);
        if (newSum - sum == checkDigit) {
          isValid = true;
        }
      }
    }
    return isValid;
  }
  
  public static boolean modulusTenCheck(String cardNumber)
  {
    boolean isValid = true;
    int digits = cardNumber.length();
    int oddOrEven = digits & 0x1;
    long sum = 0L;
    for (int count = 0; count < digits; count++)
    {
      int digit = 0;
      try
      {
        digit = Integer.parseInt(String.valueOf(cardNumber.charAt(count)));
      }
      catch (NumberFormatException e)
      {
        if (PropertyReader.getDebuggerFlag().equals("ON")) {
          LOGGER.fatal("NumberFormatException in modulusTenCheck method of class CheckDigitValidator: " + e);
        }
        isValid = false;
        break;
      }
      if ((count & 0x1 ^ oddOrEven) == 0)
      {
        digit *= 2;
        if (digit > 9) {
          digit -= 9;
        }
      }
      sum += digit;
    }
    if (isValid) {
      isValid = sum != 0L;
    }
    return isValid;
  }
}
