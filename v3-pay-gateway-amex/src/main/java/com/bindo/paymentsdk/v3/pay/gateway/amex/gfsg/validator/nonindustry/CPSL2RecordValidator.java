package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.nonindustry;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.ErrorObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceAddendumBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceBasicBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.LocationDetailTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.nonindustrytype.CPSLevel2Bean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.CountryCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.CurrencyCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.SubmissionErrorCodes;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.TaxTypeCd;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.exceptions.SettlementException;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.CommonValidator;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.industry.TAADBRecordValidator;
import java.util.List;

public class CPSL2RecordValidator
{
  public static void validateCPSL2Record(TransactionAdviceBasicBean transactionAdviceBasicType, TransactionAdviceAddendumBean transactionAdviceAddendumType, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    CPSLevel2Bean cspl2Bean = (CPSLevel2Bean)transactionAdviceAddendumType;
    
    LocationDetailTAABean locDetailTAA = transactionAdviceBasicType.getLocationDetailTAABean();
    
    TAADBRecordValidator.validateRecordType(cspl2Bean.getRecordType(), 
      errorCodes);
    TAADBRecordValidator.validateRecordNumber(cspl2Bean.getRecordNumber(), 
      errorCodes);
    TAADBRecordValidator.validateTransactionIdentifier(cspl2Bean
      .getTransactionIdentifier(), transactionAdviceBasicType, 
      errorCodes);
    
    validateAddendaTypeCode(transactionAdviceBasicType, transactionAdviceAddendumType, errorCodes);
    validateRequesterName(cspl2Bean.getRequesterName(), errorCodes);
    validateChargeDescription1(cspl2Bean.getChargeDescription1(), 
      errorCodes);
    
    validateChargeItemQuantity1(cspl2Bean.getChargeItemQuantity1(), 
      errorCodes);
    validateChargeItemAmount1(cspl2Bean.getChargeItemAmount1(), transactionAdviceBasicType.getTransactionCurrencyCode(), errorCodes);
    validateChargeItemAmount2(cspl2Bean.getChargeItemAmount2(), transactionAdviceBasicType.getTransactionCurrencyCode(), errorCodes);
    validateChargeItemAmount3(cspl2Bean.getChargeItemAmount3(), transactionAdviceBasicType.getTransactionCurrencyCode(), errorCodes);
    validateChargeItemAmount4(cspl2Bean.getChargeItemAmount4(), transactionAdviceBasicType.getTransactionCurrencyCode(), errorCodes);
    
    validateChargeItemQuantity2(cspl2Bean.getChargeItemQuantity2(), 
      errorCodes);
    validateChargeItemQuantity3(cspl2Bean.getChargeItemQuantity3(), 
      errorCodes);
    validateChargeItemQuantity4(cspl2Bean.getChargeItemQuantity4(), 
      errorCodes);
    
    validateChargeDescription2(cspl2Bean.getChargeDescription2(), 
      errorCodes);
    validateChargeDescription3(cspl2Bean.getChargeDescription3(), 
      errorCodes);
    validateChargeDescription4(cspl2Bean.getChargeDescription4(), 
      errorCodes);
    
    validateCardMemberReferenceNumber(cspl2Bean
      .getCardMemberReferenceNumber(), errorCodes);
    validateShipToPostalCode(cspl2Bean.getShipToPostalCode(), errorCodes, locDetailTAA.getLocationCountryCode(), cspl2Bean);
    validateTotalTaxAmount(cspl2Bean, errorCodes, locDetailTAA.getLocationCountryCode());
    
    validateTaxTypeCode(cspl2Bean.getTaxTypeCode(), errorCodes, transactionAdviceBasicType.getTransactionCurrencyCode(), locDetailTAA.getLocationCountryCode(), cspl2Bean);
  }
  
  private static void validateAddendaTypeCode(TransactionAdviceBasicBean transactionAdviceBasicType, TransactionAdviceAddendumBean transactionAddendumType, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    String value = transactionAddendumType.getAddendaTypeCode();
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD68_ADDENDA_TYPECODE);
      errorCodes.add(errorObj);
    }
    else if (!CommonValidator.isValidAddendaTypeCode(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD70_ADDENDA_TYPECODE
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD70_ADDENDA_TYPECODE
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "|" + 
        "RecordNumber:" + 
        "|" + 
        "AddendaTypeCode" + 
        "|" + 
        SubmissionErrorCodes.ERROR_DATAFIELD70_ADDENDA_TYPECODE
        .getErrorDescription());
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateRequesterName(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 38, 38);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD408_REQUESTER_NAME);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateChargeDescription1(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 40, 40);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD410_CHARGE_DESCRIPTION1);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateChargeDescription2(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 40, 40);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD412_CHARGE_DESCRIPTION2);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateChargeDescription3(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 40, 40);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD414_CHARGE_DESCRIPTION3);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateChargeDescription4(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 40, 40);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD416_CHARGE_DESCRIPTION4);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateChargeItemQuantity1(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ž']))
      {
        String reqLength = CommonValidator.validateLength(value, 3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD418_CHARGE_ITEMQUANTITY1);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD417_CHARGE_ITEMQUANTITY1);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateChargeItemQuantity2(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ž']))
      {
        String reqLength = CommonValidator.validateLength(value, 3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD420_CHARGE_ITEMQUANTITY2);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD419_CHARGE_ITEMQUANTITY2);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateChargeItemQuantity3(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ž']))
      {
        String reqLength = CommonValidator.validateLength(value, 3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD422_CHARGE_ITEMQUANTITY3);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD421_CHARGE_ITEMQUANTITY3);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateChargeItemQuantity4(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ž']))
      {
        String reqLength = CommonValidator.validateLength(value, 3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD424_CHARGE_ITEMQUANTITY4);
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD423_CHARGE_ITEMQUANTITY4);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateChargeItemAmount1(String value, String transactionCurrencyCode, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ſ']))
      {
        String reqLength = 
          CommonValidator.validateLength(value, 12, 1);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD426_CHARGE_ITEMAMOUNT1);
          errorCodes.add(errorObj);
        }
        else if (!CommonValidator.isNullOrEmpty(transactionCurrencyCode))
        {
          if (!CommonValidator.isValidAmount(transactionCurrencyCode, value))
          {
            errorObj = new ErrorObject(
              SubmissionErrorCodes.ERROR_DATAFIELD426_CHARGE_ITEMAMOUNT1);
            errorCodes.add(errorObj);
          }
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD425_CHARGE_ITEMAMOUNT1);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateChargeItemAmount2(String value, String transactionCurrencyCode, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ſ']))
      {
        String reqLength = 
          CommonValidator.validateLength(value, 12, 1);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD428_CHARGE_ITEMAMOUNT2);
          errorCodes.add(errorObj);
        }
        else if (!CommonValidator.isNullOrEmpty(transactionCurrencyCode))
        {
          if (!CommonValidator.isValidAmount(transactionCurrencyCode, value))
          {
            errorObj = new ErrorObject(
              SubmissionErrorCodes.ERROR_DATAFIELD428_CHARGE_ITEMAMOUNT2);
            errorCodes.add(errorObj);
          }
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD427_CHARGE_ITEMAMOUNT2);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateChargeItemAmount3(String value, String transactionCurrencyCode, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ſ']))
      {
        String reqLength = 
          CommonValidator.validateLength(value, 12, 1);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD430_CHARGE_ITEMAMOUNT3);
          errorCodes.add(errorObj);
        }
        else if (!CommonValidator.isNullOrEmpty(transactionCurrencyCode))
        {
          if (!CommonValidator.isValidAmount(transactionCurrencyCode, value))
          {
            errorObj = new ErrorObject(
              SubmissionErrorCodes.ERROR_DATAFIELD430_CHARGE_ITEMAMOUNT3);
            errorCodes.add(errorObj);
          }
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD429_CHARGE_ITEMAMOUNT3);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateChargeItemAmount4(String value, String transactionCurrencyCode, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['ſ']))
      {
        String reqLength = 
          CommonValidator.validateLength(value, 12, 1);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD432_CHARGE_ITEMAMOUNT4);
          errorCodes.add(errorObj);
        }
        else if (!CommonValidator.isNullOrEmpty(transactionCurrencyCode))
        {
          if (!CommonValidator.isValidAmount(transactionCurrencyCode, value))
          {
            errorObj = new ErrorObject(
              SubmissionErrorCodes.ERROR_DATAFIELD432_CHARGE_ITEMAMOUNT4);
            errorCodes.add(errorObj);
          }
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD431_CHARGE_ITEMAMOUNT4);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateCardMemberReferenceNumber(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 20, 20);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD434_CARDMEMBER_REFERENCENUMBER);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateShipToPostalCode(String value, List<ErrorObject> errorCodes, String countryCode, CPSLevel2Bean cspl2Bean)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if ((!CommonValidator.isNullOrEmpty(countryCode)) && (isValidCountryCodeForCPL2TAA(countryCode)))
    {
      if (CommonValidator.isNullOrEmpty(value))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD435_SHIPTO_POSTALCODE.getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD435_SHIPTO_POSTALCODE
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          cspl2Bean.getRecordType() + 
          "|" + 
          "RecordNumber:" + 
          cspl2Bean.getRecordNumber() + 
          "|" + 
          "ShipToPostalCode" + 
          "|" + 
          "This field is mandatory and cannot be empty");
        errorCodes.add(errorObj);
      }
      else
      {
        String reqLength = 
          CommonValidator.validateLength(value, 15, 15);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD437_SHIPTO_POSTALCODE.getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD437_SHIPTO_POSTALCODE
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            cspl2Bean.getRecordType() + 
            "|" + 
            "RecordNumber:" + 
            cspl2Bean.getRecordNumber() + 
            "|" + 
            "ShipToPostalCode" + 
            "|" + 
            "This field length Cannot be greater than 15");
          errorCodes.add(errorObj);
        }
      }
    }
    else if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = CommonValidator.validateLength(value, 15, 15);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD437_SHIPTO_POSTALCODE.getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD437_SHIPTO_POSTALCODE
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          cspl2Bean.getRecordType() + 
          "|" + 
          "RecordNumber:" + 
          cspl2Bean.getRecordNumber() + 
          "|" + 
          "ShipToPostalCode" + 
          "|" + 
          "This field length Cannot be greater than 15");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateTotalTaxAmount(CPSLevel2Bean csBean, List<ErrorObject> errorCodes, String countryCode)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if ((!CommonValidator.isNullOrEmpty(countryCode)) && (isValidCountryCodeForCPL2TAA(countryCode)))
    {
      if (CommonValidator.isNullOrEmpty(csBean.getTotalTaxAmount()))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD440_TOTAL_TAXAMOUNT.getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD440_TOTAL_TAXAMOUNT
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          csBean.getRecordType() + 
          "|" + 
          "RecordNumber:" + 
          csBean.getRecordNumber() + 
          "|" + 
          "TotalTaxAmount" + 
          "|" + 
          "This field is mandatory and cannot be empty");
        errorCodes.add(errorObj);
      }
      else if (CommonValidator.validateData(csBean.getTotalTaxAmount(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ƅ']))
      {
        String reqLength = CommonValidator.validateLength(csBean
          .getTotalTaxAmount(), 12, 12);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD439_TOTAL_TAXAMOUNT.getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD439_TOTAL_TAXAMOUNT
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            csBean.getRecordType() + 
            "|" + 
            "RecordNumber:" + 
            csBean.getRecordNumber() + 
            "|" + 
            "TotalTaxAmount" + 
            "|" + 
            "This field length Cannot be greater than 12");
          errorCodes.add(errorObj);
        }
        else if (CommonValidator.isNullOrEmpty(csBean.getTaxTypeCode()))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD442_TAX_TYPECODE.getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD442_TAX_TYPECODE
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            csBean.getRecordType() + 
            "|" + 
            "RecordNumber:" + 
            csBean.getRecordNumber() + 
            "|" + 
            "TotalTaxAmount" + 
            "|" + 
            "Tax Type Code required when Total Tax Amount is populated");
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD438_TOTAL_TAXAMOUNT.getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD438_TOTAL_TAXAMOUNT
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          csBean.getRecordType() + 
          "|" + 
          "RecordNumber:" + 
          csBean.getRecordNumber() + 
          "|" + 
          "TotalTaxAmount" + 
          "|" + 
          "This field can only be Numeric");
        errorCodes.add(errorObj);
      }
    }
    else if (!CommonValidator.isNullOrEmpty(csBean.getTotalTaxAmount())) {
      if (CommonValidator.validateData(csBean.getTotalTaxAmount(), com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ƅ']))
      {
        String reqLength = CommonValidator.validateLength(csBean
          .getTotalTaxAmount(), 12, 12);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD439_TOTAL_TAXAMOUNT.getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD439_TOTAL_TAXAMOUNT
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            csBean.getRecordType() + 
            "|" + 
            "RecordNumber:" + 
            csBean.getRecordNumber() + 
            "|" + 
            "TotalTaxAmount" + 
            "|" + 
            "This field length Cannot be greater than 12");
          errorCodes.add(errorObj);
        }
        else if (CommonValidator.isNullOrEmpty(csBean.getTaxTypeCode()))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD442_TAX_TYPECODE.getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD442_TAX_TYPECODE
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            csBean.getRecordType() + 
            "|" + 
            "RecordNumber:" + 
            csBean.getRecordNumber() + 
            "|" + 
            "TotalTaxAmount" + 
            "|" + 
            "Tax Type Code required when Total Tax Amount is populated");
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD438_TOTAL_TAXAMOUNT.getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD438_TOTAL_TAXAMOUNT
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          csBean.getRecordType() + 
          "|" + 
          "RecordNumber:" + 
          csBean.getRecordNumber() + 
          "|" + 
          "TotalTaxAmount" + 
          "|" + 
          "This field can only be Numeric");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateTaxTypeCode(String value, List<ErrorObject> errorCodes, String transactionCurrencyCode, String countryCode, CPSLevel2Bean cspl2Bean)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if ((!CommonValidator.isNullOrEmpty(countryCode)) && (isValidCountryCodeForCPL2TAA(countryCode)))
    {
      if (CommonValidator.isNullOrEmpty(value))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD441_TAX_TYPECODE.getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD441_TAX_TYPECODE
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          cspl2Bean.getRecordType() + 
          "|" + 
          "RecordNumber:" + 
          cspl2Bean.getRecordNumber() + 
          "|" + 
          "TaxTypeCode" + 
          "|" + 
          "This field is mandatory and cannot be empty");
        errorCodes.add(errorObj);
      }
      else if ((!CommonValidator.isNullOrEmpty(transactionCurrencyCode)) && (transactionCurrencyCode.equalsIgnoreCase(CurrencyCode.US_Dollar.getCurrencyCode())))
      {
        if (!value.equalsIgnoreCase(TaxTypeCd.Sales_Tax.getTaxTypeCd()))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD441_TAX_TYPECODE.getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD441_TAX_TYPECODE
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            cspl2Bean.getRecordType() + 
            "|" + 
            "RecordNumber:" + 
            cspl2Bean.getRecordNumber() + 
            "|" + 
            "TaxTypeCode" + 
            "|" + 
            "This field is missing or invalid");
          errorCodes.add(errorObj);
        }
      }
      else if ((!CommonValidator.isNullOrEmpty(transactionCurrencyCode)) && (transactionCurrencyCode.equalsIgnoreCase(CurrencyCode.Canadian_Dollar.getCurrencyCode())))
      {
        if (!value.equalsIgnoreCase(TaxTypeCd.All_Taxes.getTaxTypeCd()))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD441_TAX_TYPECODE.getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD441_TAX_TYPECODE
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            cspl2Bean.getRecordType() + 
            "|" + 
            "RecordNumber:" + 
            cspl2Bean.getRecordNumber() + 
            "|" + 
            "TaxTypeCode" + 
            "|" + 
            "This field is missing or invalid");
          errorCodes.add(errorObj);
        }
      }
      else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ɔ']))
      {
        String reqLength = CommonValidator.validateLength(value, 3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD443_TAX_TYPECODE.getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD443_TAX_TYPECODE
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            cspl2Bean.getRecordType() + 
            "|" + 
            "RecordNumber:" + 
            cspl2Bean.getRecordNumber() + 
            "|" + 
            "TaxTypeCode" + 
            "|" + 
            "This field length Cannot be greater than 3");
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD444_TAX_TYPECODE.getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD444_TAX_TYPECODE
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          cspl2Bean.getRecordType() + 
          "|" + 
          "RecordNumber:" + 
          cspl2Bean.getRecordNumber() + 
          "|" + 
          "TaxTypeCode" + 
          "|" + 
          "This field can only be AlphaNumeric");
        errorCodes.add(errorObj);
      }
    }
    else if (!CommonValidator.isNullOrEmpty(value)) {
      if ((!CommonValidator.isNullOrEmpty(transactionCurrencyCode)) && (transactionCurrencyCode.equalsIgnoreCase(CurrencyCode.US_Dollar.getCurrencyCode())))
      {
        if (!value.equalsIgnoreCase(TaxTypeCd.Sales_Tax.getTaxTypeCd()))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD441_TAX_TYPECODE.getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD441_TAX_TYPECODE
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            cspl2Bean.getRecordType() + 
            "|" + 
            "RecordNumber:" + 
            cspl2Bean.getRecordNumber() + 
            "|" + 
            "TaxTypeCode" + 
            "|" + 
            "This field is missing or invalid");
          errorCodes.add(errorObj);
        }
      }
      else if ((!CommonValidator.isNullOrEmpty(transactionCurrencyCode)) && (transactionCurrencyCode.equalsIgnoreCase(CurrencyCode.Canadian_Dollar.getCurrencyCode())))
      {
        if (!value.equalsIgnoreCase(TaxTypeCd.All_Taxes.getTaxTypeCd()))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD441_TAX_TYPECODE.getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD441_TAX_TYPECODE
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            cspl2Bean.getRecordType() + 
            "|" + 
            "RecordNumber:" + 
            cspl2Bean.getRecordNumber() + 
            "|" + 
            "TaxTypeCode" + 
            "|" + 
            "This field is missing or invalid");
          errorCodes.add(errorObj);
        }
      }
      else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ɔ']))
      {
        String reqLength = CommonValidator.validateLength(value, 3, 3);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD443_TAX_TYPECODE.getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD443_TAX_TYPECODE
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            cspl2Bean.getRecordType() + 
            "|" + 
            "RecordNumber:" + 
            cspl2Bean.getRecordNumber() + 
            "|" + 
            "TaxTypeCode" + 
            "|" + 
            "This field length Cannot be greater than 3");
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD444_TAX_TYPECODE.getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD444_TAX_TYPECODE
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          cspl2Bean.getRecordType() + 
          "|" + 
          "RecordNumber:" + 
          cspl2Bean.getRecordNumber() + 
          "|" + 
          "TaxTypeCode" + 
          "|" + 
          "This field can only be AlphaNumeric");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static boolean isValidCountryCodeForCPL2TAA(String countryCode)
  {
    if ((countryCode.equals(CountryCode.American_Samoa.getCountryCode())) || (countryCode.equals(CountryCode.Guam.getCountryCode())) || 
      (countryCode.equals(CountryCode.Marshall_Islands.getCountryCode())) || (countryCode.equals(CountryCode.Federated_States_Of_Micronesia.getCountryCode())) || 
      (countryCode.equals(CountryCode.PuertoRico.getCountryCode())) || (countryCode.equals(CountryCode.United_States.getCountryCode())) || 
      (countryCode.equals(CountryCode.Northern_Mariana_Islands.getCountryCode())) || (countryCode.equals(CountryCode.Palau.getCountryCode())) || 
      (countryCode.equals(CountryCode.VirginIslandsUS.getCountryCode())) || (countryCode.equals(CountryCode.Canada.getCountryCode()))) {
      return true;
    }
    return false;
  }
}
