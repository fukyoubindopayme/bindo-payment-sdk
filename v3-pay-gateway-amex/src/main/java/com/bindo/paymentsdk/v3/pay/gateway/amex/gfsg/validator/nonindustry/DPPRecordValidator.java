package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.nonindustry;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.ErrorObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceAddendumBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceBasicBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.nonindustrytype.DeferredPaymentPlanBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.SubmissionErrorCodes;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.exceptions.SettlementException;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.CommonValidator;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.industry.TAADBRecordValidator;
import java.util.List;

public class DPPRecordValidator
{
  public static void validateDPPRecord(TransactionAdviceBasicBean transactionAdviceBasicType, TransactionAdviceAddendumBean transactionAdviceAddendumType, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    DeferredPaymentPlanBean dppBean = (DeferredPaymentPlanBean)transactionAdviceAddendumType;
    
    TAADBRecordValidator.validateRecordType(dppBean.getRecordType(), 
      errorCodes);
    TAADBRecordValidator.validateRecordNumber(dppBean.getRecordNumber(), 
      errorCodes);
    TAADBRecordValidator.validateTransactionIdentifier(dppBean
      .getTransactionIdentifier(), transactionAdviceBasicType, 
      errorCodes);
    TAADBRecordValidator.validateAddendaTypeCode(dppBean
      .getAddendaTypeCode(), errorCodes);
    validateFullTransactionAmount(dppBean.getFullTransactionAmount(), 
      errorCodes);
    validateTypeOfPlanCode(dppBean.getTypeOfPlanCode(), errorCodes);
    validateNoOfInstallments(dppBean.getNoOfInstallments(), errorCodes);
    validateAmountOfInstallment(dppBean.getAmountOfInstallment(), 
      errorCodes);
    validateInstallmentNumber(dppBean.getInstallmentNumber(), errorCodes);
    validateContractNumber(dppBean.getContractNumber(), errorCodes);
    validatePaymentTypeCode1(dppBean.getPaymentTypeCode1(), errorCodes);
    validatePaymentTypeCode2(dppBean.getPaymentTypeCode2(), errorCodes);
    validatePaymentTypeCode3(dppBean.getPaymentTypeCode3(), errorCodes);
    validatePaymentTypeCode4(dppBean.getPaymentTypeCode4(), errorCodes);
    validatePaymentTypeCode5(dppBean.getPaymentTypeCode5(), errorCodes);
    validatePaymentTypeAmount1(dppBean.getPaymentTypeAmount1(), errorCodes);
    validatePaymentTypeAmount2(dppBean.getPaymentTypeAmount2(), errorCodes);
    validatePaymentTypeAmount3(dppBean.getPaymentTypeAmount3(), errorCodes);
    validatePaymentTypeAmount4(dppBean.getPaymentTypeAmount4(), errorCodes);
    validatePaymentTypeAmount5(dppBean.getPaymentTypeAmount5(), errorCodes);
  }
  
  private static void validateFullTransactionAmount(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD445_FULLTRANSACTION_AMOUNT);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ə']))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 12, 12);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD446_FULLTRANSACTION_AMOUNT);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD447_FULLTRANSACTION_AMOUNT);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateTypeOfPlanCode(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD454_TYPEOF_PLANCODE);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ɛ']))
    {
      String reqLength = CommonValidator.validateLength(value, 4, 4);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD455_TYPEOF_PLANCODE);
        errorCodes.add(errorObj);
      }
      else if (!"0003".equalsIgnoreCase(value))
      {
        if (!"0005".equalsIgnoreCase(value))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD454_TYPEOF_PLANCODE);
          errorCodes.add(errorObj);
        }
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD456_TYPEOF_PLANCODE);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateNoOfInstallments(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD461_NUMBEROF_INSTALLMENTS);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ƒ']))
    {
      String reqLength = CommonValidator.validateLength(value, 4, 4);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD460_NUMBEROF_INSTALLMENTS);
        errorCodes.add(errorObj);
      }
      else if (Integer.parseInt(value) < 1)
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD459_NUMBEROF_INSTALLMENTS);
        errorCodes.add(errorObj);
      }
      else if (Integer.parseInt(value) > 99)
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD460_NUMBEROF_INSTALLMENTS);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD458_NUMBEROF_INSTALLMENTS);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateAmountOfInstallment(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD462_AMOUNT_OF_INSTALLMENT);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ɠ']))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 12, 12);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD462_AMOUNT_OF_INSTALLMENT);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD462_AMOUNT_OF_INSTALLMENT);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateInstallmentNumber(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD468_INSTALLMENT_NUMBER);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ɣ']))
    {
      String reqLength = CommonValidator.validateLength(value, 4, 4);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD466_INSTALLMENT_NUMBER);
        errorCodes.add(errorObj);
      }
      else if (Integer.parseInt(value) != 1)
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD465_INSTALLMENT_NUMBER);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD467_INSTALLMENT_NUMBER);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateContractNumber(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD469_CONTRACT_NUMBER);
      errorCodes.add(errorObj);
    }
    else
    {
      String reqLength = 
        CommonValidator.validateLength(value, 14, 14);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD471_CONTRACT_NUMBER);
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validatePaymentTypeCode1(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD472_PAYMENT_TYPECODE1);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ɩ']))
    {
      String reqLength = CommonValidator.validateLength(value, 2, 2);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD474_PAYMENT_TYPECODE1);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD473_PAYMENT_TYPECODE1);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validatePaymentTypeCode2(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD475_PAYMENT_TYPECODE2);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ɩ']))
    {
      String reqLength = CommonValidator.validateLength(value, 2, 2);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD477_PAYMENT_TYPECODE2);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD476_PAYMENT_TYPECODE2);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validatePaymentTypeCode3(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD478_PAYMENT_TYPECODE3);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ɩ']))
    {
      String reqLength = CommonValidator.validateLength(value, 2, 2);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD480_PAYMENT_TYPECODE3);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD479_PAYMENT_TYPECODE3);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validatePaymentTypeCode4(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD481_PAYMENT_TYPECODE4);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ɩ']))
    {
      String reqLength = CommonValidator.validateLength(value, 2, 2);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD483_PAYMENT_TYPECODE4);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD482_PAYMENT_TYPECODE4);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validatePaymentTypeCode5(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD484_PAYMENT_TYPECODE5);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ɩ']))
    {
      String reqLength = CommonValidator.validateLength(value, 2, 2);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD486_PAYMENT_TYPECODE5);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD485_PAYMENT_TYPECODE5);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validatePaymentTypeAmount1(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD487_PAYMENTTYPE_AMOUNT1);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ƙ']))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 12, 12);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD488_PAYMENTTYPE_AMOUNT1);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD489_PAYMENTTYPE_AMOUNT1);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validatePaymentTypeAmount2(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD490_PAYMENTTYPE_AMOUNT2);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ƙ']))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 12, 12);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD491_PAYMENTTYPE_AMOUNT2);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD492_PAYMENTTYPE_AMOUNT2);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validatePaymentTypeAmount3(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD493_PAYMENTTYPE_AMOUNT3);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ƙ']))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 12, 12);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD494_PAYMENTTYPE_AMOUNT3);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD495_PAYMENTTYPE_AMOUNT3);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validatePaymentTypeAmount4(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD496_PAYMENTTYPE_AMOUNT4);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ƙ']))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 12, 12);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD497_PAYMENTTYPE_AMOUNT4);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD498_PAYMENTTYPE_AMOUNT4);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validatePaymentTypeAmount5(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD499_PAYMENTTYPE_AMOUNT5);
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE['Ƙ']))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 12, 12);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD500_PAYMENTTYPE_AMOUNT5);
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD501_PAYMENTTYPE_AMOUNT5);
      errorCodes.add(errorObj);
    }
  }
}
