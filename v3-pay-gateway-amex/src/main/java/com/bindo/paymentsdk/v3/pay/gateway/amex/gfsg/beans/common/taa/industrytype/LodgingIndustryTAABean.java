package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.IndustryTypeTAABean;

public class LodgingIndustryTAABean
  extends IndustryTypeTAABean
{
  private String lodgingSpecialProgramCode;
  private String lodgingCheckInDate;
  private String lodgingCheckOutDate;
  private String lodgingRoomRate1;
  private String numberOfNightsAtRoomRate1;
  private String lodgingRoomRate2;
  private String numberOfNightsAtRoomRate2;
  private String lodgingRoomRate3;
  private String numberOfNightsAtRoomRate3;
  private String lodgingRenterName;
  private String lodgingFolioNumber;
  private String lodgingIndustryDataField9Reserved;
  private String lodgingIndustryDataField12Reserved;
  private String lodgingIndustryDataField15Reserved;
  private String lodgingIndustryDataField20Reserved;
  
  public String getLodgingSpecialProgramCode()
  {
    return this.lodgingSpecialProgramCode;
  }
  
  public void setLodgingSpecialProgramCode(String lodgingSpecialProgramCode)
  {
    this.lodgingSpecialProgramCode = lodgingSpecialProgramCode;
  }
  
  public String getLodgingCheckInDate()
  {
    return this.lodgingCheckInDate;
  }
  
  public void setLodgingCheckInDate(String lodgingCheckInDate)
  {
    this.lodgingCheckInDate = lodgingCheckInDate;
  }
  
  public String getLodgingCheckOutDate()
  {
    return this.lodgingCheckOutDate;
  }
  
  public void setLodgingCheckOutDate(String lodgingCheckOutDate)
  {
    this.lodgingCheckOutDate = lodgingCheckOutDate;
  }
  
  public String getLodgingRoomRate1()
  {
    return this.lodgingRoomRate1;
  }
  
  public void setLodgingRoomRate1(String lodgingRoomRate1)
  {
    this.lodgingRoomRate1 = lodgingRoomRate1;
  }
  
  public String getNumberOfNightsAtRoomRate1()
  {
    return this.numberOfNightsAtRoomRate1;
  }
  
  public void setNumberOfNightsAtRoomRate1(String numberOfNightsAtRoomRate1)
  {
    this.numberOfNightsAtRoomRate1 = numberOfNightsAtRoomRate1;
  }
  
  public String getLodgingRoomRate2()
  {
    return this.lodgingRoomRate2;
  }
  
  public void setLodgingRoomRate2(String lodgingRoomRate2)
  {
    this.lodgingRoomRate2 = lodgingRoomRate2;
  }
  
  public String getNumberOfNightsAtRoomRate2()
  {
    return this.numberOfNightsAtRoomRate2;
  }
  
  public void setNumberOfNightsAtRoomRate2(String numberOfNightsAtRoomRate2)
  {
    this.numberOfNightsAtRoomRate2 = numberOfNightsAtRoomRate2;
  }
  
  public String getLodgingRoomRate3()
  {
    return this.lodgingRoomRate3;
  }
  
  public void setLodgingRoomRate3(String lodgingRoomRate3)
  {
    this.lodgingRoomRate3 = lodgingRoomRate3;
  }
  
  public String getNumberOfNightsAtRoomRate3()
  {
    return this.numberOfNightsAtRoomRate3;
  }
  
  public void setNumberOfNightsAtRoomRate3(String numberOfNightsAtRoomRate3)
  {
    this.numberOfNightsAtRoomRate3 = numberOfNightsAtRoomRate3;
  }
  
  public String getLodgingRenterName()
  {
    return this.lodgingRenterName;
  }
  
  public void setLodgingRenterName(String lodgingRenterName)
  {
    this.lodgingRenterName = lodgingRenterName;
  }
  
  public String getLodgingFolioNumber()
  {
    return this.lodgingFolioNumber;
  }
  
  public void setLodgingFolioNumber(String lodgingFolioNumber)
  {
    this.lodgingFolioNumber = lodgingFolioNumber;
  }
  
  public String getLodgingIndustryDataField9Reserved()
  {
    return this.lodgingIndustryDataField9Reserved;
  }
  
  public void setLodgingIndustryDataField9Reserved(String lodgingIndustryDataField9Reserved)
  {
    this.lodgingIndustryDataField9Reserved = lodgingIndustryDataField9Reserved;
  }
  
  public String getLodgingIndustryDataField12Reserved()
  {
    return this.lodgingIndustryDataField12Reserved;
  }
  
  public void setLodgingIndustryDataField12Reserved(String lodgingIndustryDataField12Reserved)
  {
    this.lodgingIndustryDataField12Reserved = lodgingIndustryDataField12Reserved;
  }
  
  public String getLodgingIndustryDataField15Reserved()
  {
    return this.lodgingIndustryDataField15Reserved;
  }
  
  public void setLodgingIndustryDataField15Reserved(String lodgingIndustryDataField15Reserved)
  {
    this.lodgingIndustryDataField15Reserved = lodgingIndustryDataField15Reserved;
  }
  
  public String getLodgingIndustryDataField20Reserved()
  {
    return this.lodgingIndustryDataField20Reserved;
  }
  
  public void setLodgingIndustryDataField20Reserved(String lodgingIndustryDataField20Reserved)
  {
    this.lodgingIndustryDataField20Reserved = lodgingIndustryDataField20Reserved;
  }
}
