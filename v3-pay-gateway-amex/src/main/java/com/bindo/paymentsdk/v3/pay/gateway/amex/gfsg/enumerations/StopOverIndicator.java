package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations;

public enum StopOverIndicator {
    Stopover_permitted("O"),
    No_stopoverpermitted("X"),
    Unknown(" ");

    private final String stopOverIndicator;

    private StopOverIndicator(String StopOverIndicator) {
        this.stopOverIndicator = StopOverIndicator;
    }

    public String getStopOverIndicator() {
        return this.stopOverIndicator;
    }
}
