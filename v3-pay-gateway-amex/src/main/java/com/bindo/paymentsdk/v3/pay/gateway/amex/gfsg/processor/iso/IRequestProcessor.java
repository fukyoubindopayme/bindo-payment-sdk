package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.processor.iso;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.ErrorObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.SettlementRequestDataObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.SettlementResponseDataObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.acknowledgement.AcknowledgementReportObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.confirmation.ConfirmationReportObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.connection.PropertyReader;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.exceptions.SettlementException;
import java.io.File;
import java.util.List;

public abstract interface IRequestProcessor
{
  public abstract SettlementResponseDataObject processRequest(SettlementRequestDataObject paramSettlementRequestDataObject, PropertyReader paramPropertyReader)
    throws SettlementException;
  
  public abstract List<ConfirmationReportObject> fetchConfirmationReportInRawData(PropertyReader paramPropertyReader)
    throws SettlementException;
  
  public abstract List<File> fetchConfirmationReportInPrintedFormat(PropertyReader paramPropertyReader)
    throws SettlementException;
  
  public abstract List<AcknowledgementReportObject> fetchAcknowledgementInPrintedFormat(PropertyReader paramPropertyReader, List<ErrorObject> paramList)
    throws SettlementException;
}
