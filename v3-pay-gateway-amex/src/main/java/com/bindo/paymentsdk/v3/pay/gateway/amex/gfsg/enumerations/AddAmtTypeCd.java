package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations;

public enum AddAmtTypeCd
{
  Goods_and_Services_Tax_GST("001"),  Consumption_Tax("002"),  Provincial_Sales_TaxPST("003"),  Quebec_Sales_TaxQST("004"),  Harmonized_Sales_TaxHST("005"),  Insurance_Premium_TaxIPT("006"),  Circulation_of_Merchandise_and_Service_TaxICMS("007"),  Industrialized_Products_Federal_Tributary_TaxIPI_Federal_Tributary("008"),  Inland_Revenue_Income_Tax_IR_Income_Tax("009"),  International_Students_and_Scholars_Income_TaxISS_Income_Tax("010"),  Income_Security_and_Reform_Tax_ISR_Income_Tax("011"),  Occupancy_Tax("012"),  Room_Tax("013"),  Surcharge_Tax("014"),  Airport_Tax("015"),  Ticket_Tax("043"),  Miscellaneous_Tax("046"),  Sales_Tax("056"),  Stamp_Duty("067"),  Value_Added_Tax_VAT("057"),  Exempt_No_GST_charged("068"),  Bar("019"),  Bar_MiniBar("023"),  Barber_Beauty_Salon("028"),  Beverage("017"),  Business_Center("036"),  Catering_Charges("022"),  Convention_Fees("037"),  Food("016"),  Food_Beverage("018"),  Gift_Shop("030"),  Health_Fitness("029"),  Internet_Service("025"),  Insurance_Purchased("052"),  Laundry_DryCleaning("027"),  Lodging("020"),  Movies_PayPerView("026"),  Pet_Fees("033"),  Phone("024"),  Pro_Shop("031"),  Restaurant_Room_Service("021"),  Reward_Program_Transaction("047"),  Tip_Gratuity("058"),  Tours("034"),  Additional_Miles_Kilometers_Distance("062"),  Auto_Rental_Adjustment("060"),  Cancellation_Adjustment("065"),  Charges_Added_After_CheckOut_Departure("041"),  Convenience_Charge("050"),  Delivery_Charge("051"),  Discount("053"),  Equipment_Rental("035"),  Express_Service_Charge("040"),  Freight_Shipping_Handling("055"),  Fuel_Charge("061"),  Late_Return("063"),  Meeting_Conference_Charges("038"),  Misc_Charges_Fees("042"),  No_Show_Charge("039"),  Order_Processing_Charge("049"),  Parking_Fee("032"),  Policy_Adjustment("066"),  Repairs("064"),  Surcharge("048"),  Tickets_Violations("054"),  Travel_Transaction_Fees("074"),  Travel_Miscellaneous_Charges("075");
  
  private final String addAmtTypeCd;
  
  public String getAddAmtTypeCd()
  {
    return this.addAmtTypeCd;
  }
  
  private AddAmtTypeCd(String addAmtTypeCd)
  {
    this.addAmtTypeCd = addAmtTypeCd;
  }
}
