package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.exceptions;

public class SSLConnectionException
  extends Exception
{
  public SSLConnectionException() {}
  
  public SSLConnectionException(String message)
  {
    super(message);
  }
  
  public SSLConnectionException(Throwable cause)
  {
    super(cause);
  }
  
  public SSLConnectionException(String message, Throwable cause)
  {
    super(message, cause);
  }
}
