package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums;

import java.util.HashMap;
import java.util.Map;

public enum ActionCode {

    APPROVED("000", "Approved"),
    APPROVE_WITH_ID("001", "Approve with ID"),
    PARTIAL_APPROVAL("002", "Partial Approval (Prepaid Cards only)"),
    APPROVE_VIP("003", "Approve VIP"),
    DENY("100", "Deny"),
    EXPIRED_CARD_OR_INVALID_EXPIRATION_DATE("101", "Expired Card / Invalid Expiration Date"),
    EXCEEDED_PIN_ATTEMPTS("106", "Exceeded PIN Attempts"),
    PLEASE_CALL_ISSUER("107", "Please Call Issuer"),
    INVALID_MERCHANT("109", "Invalid merchant"),
    INVALID_AMOUNT("110", "Invalid amount"),
    INVALID_ACCOUNT_OR_INVALID_MICR("111", "Invalid account / Invalid MICR"),
    REQUESTED_FUNCTION_NOT_SUPPORTED("115", "Requested function not supported"),
    INVALID_PIN("117", "Invalid PIN"),
    CARDMEMBER_NOT_ENROLLED_OR_NOT_PERMITTED("119", "Cardmember not enrolled / not permitted"),
    INVALID_CARD_SECURITY_CODE("122", "Invalid card security code"),
    INVALID_EFFECTIVE_DATE("125", "Invalid effective date"),
    FORMAT_ERROR("181", "Format error"),
    PLEASE_WAIT("182", "Please Wait"),
    INVALID_CURRENCY_CODE("183", "Invalid currency code"),
    DENY_NEW_CARD_ISSUED("187", "Deny - New card issued"),
    DENY_ACCOUNT_CANCELED("188", "Deny - Account canceled"),
    DENY_CANCELED_OR_CLOSED_MERCHANT_OR_SE("189", "Deny - Canceled or Closed Merchant/SE"),
    DENY_PICK_UP_CARD("200", "Deny - Pick up card"),
    REVERSAL_ACCEPTED("400", "Reversal Accepted"),
    ACCEPTED("800", "Accepted"),
    ADVICE_ACCEPTED("900", "Advice accepted"),
    SYSTEM_MALFUNCTION("909", "System Malfunction (Cryptographic error)");

    private final String actionCode;
    private final String actionCodeDescription;

    private ActionCode(String actionCode, String actionCodeDescription) {
        this.actionCode = actionCode;
        this.actionCodeDescription = actionCodeDescription;
    }

    public String getActionCode() {
        return this.actionCode;
    }

    public String getActionCodeDescription() {
        return this.actionCodeDescription;
    }

    private static final Map<String, ActionCode> mapActionCodes = new HashMap<String, ActionCode>();

    static {
        for (ActionCode ac : values()) {
            mapActionCodes.put(ac.getActionCode(), ac);
        }
    }

    public static ActionCode getByValue(String actionCode) {
        return mapActionCodes.get(actionCode);
    }
}
