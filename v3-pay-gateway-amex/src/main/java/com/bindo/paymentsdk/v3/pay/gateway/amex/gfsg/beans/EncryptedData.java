package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans;

import java.security.AlgorithmParameters;
import java.security.Key;

public class EncryptedData
{
  String encryptedUserID = null;
  String encryptedPwd = null;
  String decryptedUserID = null;
  String decryptedPwd = null;
  Key encKey = null;
  AlgorithmParameters param = null;
  
  public AlgorithmParameters getParam()
  {
    return this.param;
  }
  
  public void setParam(AlgorithmParameters param)
  {
    this.param = param;
  }
  
  public Key getEncKey()
  {
    return this.encKey;
  }
  
  public void setEncKey(Key encKey)
  {
    this.encKey = encKey;
  }
  
  public String getEncryptedUserID()
  {
    return this.encryptedUserID;
  }
  
  public String getDecryptedUserID()
  {
    return this.decryptedUserID;
  }
  
  public void setDecryptedUserID(String decryptedUserID)
  {
    this.decryptedUserID = decryptedUserID;
  }
  
  public String getDecryptedPwd()
  {
    return this.decryptedPwd;
  }
  
  public void setDecryptedPwd(String decryptedPwd)
  {
    this.decryptedPwd = decryptedPwd;
  }
  
  public void setEncryptedUserID(String encryptedUserID)
  {
    this.encryptedUserID = encryptedUserID;
  }
  
  public String getEncryptedPwd()
  {
    return this.encryptedPwd;
  }
  
  public void setEncryptedPwd(String setEncryptedPwd)
  {
    this.encryptedPwd = setEncryptedPwd;
  }
}
