package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.IndustryTypeTAABean;

public class EntertainmentTicketingIndustryTAABean
  extends IndustryTypeTAABean
{
  private String eventName;
  private String eventDate;
  private String eventIndividualTicketPriceAmount;
  private String eventTicketQuantity;
  private String eventLocation;
  private String eventRegionCode;
  private String eventCountryCode;
  
  public String getEventName()
  {
    return this.eventName;
  }
  
  public void setEventName(String eventName)
  {
    this.eventName = eventName;
  }
  
  public String getEventDate()
  {
    return this.eventDate;
  }
  
  public void setEventDate(String eventDate)
  {
    this.eventDate = eventDate;
  }
  
  public String getEventIndividualTicketPriceAmount()
  {
    return this.eventIndividualTicketPriceAmount;
  }
  
  public void setEventIndividualTicketPriceAmount(String eventIndividualTicketPriceAmount)
  {
    this.eventIndividualTicketPriceAmount = eventIndividualTicketPriceAmount;
  }
  
  public String getEventTicketQuantity()
  {
    return this.eventTicketQuantity;
  }
  
  public void setEventTicketQuantity(String eventTicketQuantity)
  {
    this.eventTicketQuantity = eventTicketQuantity;
  }
  
  public String getEventLocation()
  {
    return this.eventLocation;
  }
  
  public void setEventLocation(String eventLocation)
  {
    this.eventLocation = eventLocation;
  }
  
  public String getEventRegionCode()
  {
    return this.eventRegionCode;
  }
  
  public void setEventRegionCode(String eventRegionCode)
  {
    this.eventRegionCode = eventRegionCode;
  }
  
  public String getEventCountryCode()
  {
    return this.eventCountryCode;
  }
  
  public void setEventCountryCode(String eventCountryCode)
  {
    this.eventCountryCode = eventCountryCode;
  }
}
