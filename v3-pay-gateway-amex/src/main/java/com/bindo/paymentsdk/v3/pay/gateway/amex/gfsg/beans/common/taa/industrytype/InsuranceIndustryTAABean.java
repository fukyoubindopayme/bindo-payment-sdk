package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.IndustryTypeTAABean;

public class InsuranceIndustryTAABean
  extends IndustryTypeTAABean
{
  private String insurancePolicyNumber;
  private String insuranceCoverageStartDate;
  private String insuranceCoverageEndDate;
  private String insurancePolicyPremiumFrequency;
  private String additionalInsurancePolicyNumber;
  private String typeOfPolicy;
  private String nameOfInsured;
  
  public String getInsurancePolicyNumber()
  {
    return this.insurancePolicyNumber;
  }
  
  public void setInsurancePolicyNumber(String insurancePolicyNumber)
  {
    this.insurancePolicyNumber = insurancePolicyNumber;
  }
  
  public String getInsuranceCoverageStartDate()
  {
    return this.insuranceCoverageStartDate;
  }
  
  public void setInsuranceCoverageStartDate(String insuranceCoverageStartDate)
  {
    this.insuranceCoverageStartDate = insuranceCoverageStartDate;
  }
  
  public String getInsuranceCoverageEndDate()
  {
    return this.insuranceCoverageEndDate;
  }
  
  public void setInsuranceCoverageEndDate(String insuranceCoverageEndDate)
  {
    this.insuranceCoverageEndDate = insuranceCoverageEndDate;
  }
  
  public String getInsurancePolicyPremiumFrequency()
  {
    return this.insurancePolicyPremiumFrequency;
  }
  
  public void setInsurancePolicyPremiumFrequency(String insurancePolicyPremiumFrequency)
  {
    this.insurancePolicyPremiumFrequency = insurancePolicyPremiumFrequency;
  }
  
  public String getAdditionalInsurancePolicyNumber()
  {
    return this.additionalInsurancePolicyNumber;
  }
  
  public void setAdditionalInsurancePolicyNumber(String additionalInsurancePolicyNumber)
  {
    this.additionalInsurancePolicyNumber = additionalInsurancePolicyNumber;
  }
  
  public String getTypeOfPolicy()
  {
    return this.typeOfPolicy;
  }
  
  public void setTypeOfPolicy(String typeOfPolicy)
  {
    this.typeOfPolicy = typeOfPolicy;
  }
  
  public String getNameOfInsured()
  {
    return this.nameOfInsured;
  }
  
  public void setNameOfInsured(String nameOfInsured)
  {
    this.nameOfInsured = nameOfInsured;
  }
}
