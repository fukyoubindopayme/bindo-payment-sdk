package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums;

public enum ProcessingCode {
    CARD_AUTHORIZATION_REQUEST("004000"),
    COMBINATION_AUTOMATED_ADDRESS_VERIFICATION_AND_AUTHORIZATION("004800"),
    AMEX_EMERGENCY_CHECK_CASHING("034000"),
    AMEX_TRAVELERS_CHEQUE_ENCASHMENT("064000"),
    TRANSACTION_FOR_AUTOMATED_ADDRESS_VERIFICATION_ONLY("174800"),
    AUTHORIZATION_ADJUSTMENT("220000"),
    CARD_REVERSAL_ADVICE_SYSTEM_GENERATED_REVERSAL("004000"),
    MERCHANT_INITIATED_REVERSAL("024000"),
    SYSTEM_AUDIT_CONTROL_ECHO_MESSAGE("000000");

    private final String processingCode;

    private ProcessingCode(String processingCode) {
        this.processingCode = processingCode;
    }

    public String getProcessingCode() {
        return this.processingCode;
    }
}
