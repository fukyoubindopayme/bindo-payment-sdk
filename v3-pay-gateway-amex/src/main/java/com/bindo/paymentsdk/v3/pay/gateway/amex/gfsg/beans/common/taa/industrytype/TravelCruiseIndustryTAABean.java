package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.IndustryTypeTAABean;

public class TravelCruiseIndustryTAABean
  extends IndustryTypeTAABean
{
  private String iataCarrierCode;
  private String iataAgencyNumber;
  private String travelPackageIndicator;
  private String passengerName;
  private String travelTicketNumber;
  private String travelDepartureDateSegment1;
  private String travelDestinationCodeSegment1;
  private String travelDepartureAirportSegment1;
  private String airCarrierCodeSegment1;
  private String flightNumberSegment1;
  private String classCodeSegment1;
  private String travelDepartureDateSegment2;
  private String travelDestinationCodeSegment2;
  private String travelDepartureAirportSegment2;
  private String airCarrierCodeSegment2;
  private String flightNumberSegment2;
  private String classCodeSegment2;
  private String travelDepartureDateSegment3;
  private String travelDestinationCodeSegment3;
  private String travelDepartureAirportSegment3;
  private String airCarrierCodeSegment3;
  private String flightNumberSegment3;
  private String classCodeSegment3;
  private String travelDepartureDateSegment4;
  private String travelDestinationCodeSegment4;
  private String travelDepartureAirportSegment4;
  private String airCarrierCodeSegment4;
  private String flightNumberSegment4;
  private String classCodeSegment4;
  private String lodgingCheckInDate;
  private String lodgingCheckOutDate;
  private String lodgingRoomRate1;
  private String numberOfNightsAtRoomRate1;
  private String lodgingName;
  private String lodgingRegionCode;
  private String lodgingCountryCode;
  private String lodgingCityName;
  private String reserved;
  
  public String getIataCarrierCode()
  {
    return this.iataCarrierCode;
  }
  
  public void setIataCarrierCode(String iataCarrierCode)
  {
    this.iataCarrierCode = iataCarrierCode;
  }
  
  public String getIataAgencyNumber()
  {
    return this.iataAgencyNumber;
  }
  
  public void setIataAgencyNumber(String iataAgencyNumber)
  {
    this.iataAgencyNumber = iataAgencyNumber;
  }
  
  public String getTravelPackageIndicator()
  {
    return this.travelPackageIndicator;
  }
  
  public void setTravelPackageIndicator(String travelPackageIndicator)
  {
    this.travelPackageIndicator = travelPackageIndicator;
  }
  
  public String getPassengerName()
  {
    return this.passengerName;
  }
  
  public void setPassengerName(String passengerName)
  {
    this.passengerName = passengerName;
  }
  
  public String getTravelTicketNumber()
  {
    return this.travelTicketNumber;
  }
  
  public void setTravelTicketNumber(String travelTicketNumber)
  {
    this.travelTicketNumber = travelTicketNumber;
  }
  
  public String getTravelDepartureDateSegment1()
  {
    return this.travelDepartureDateSegment1;
  }
  
  public void setTravelDepartureDateSegment1(String travelDepartureDateSegment1)
  {
    this.travelDepartureDateSegment1 = travelDepartureDateSegment1;
  }
  
  public String getTravelDestinationCodeSegment1()
  {
    return this.travelDestinationCodeSegment1;
  }
  
  public void setTravelDestinationCodeSegment1(String travelDestinationCodeSegment1)
  {
    this.travelDestinationCodeSegment1 = travelDestinationCodeSegment1;
  }
  
  public String getTravelDepartureAirportSegment1()
  {
    return this.travelDepartureAirportSegment1;
  }
  
  public void setTravelDepartureAirportSegment1(String travelDepartureAirportSegment1)
  {
    this.travelDepartureAirportSegment1 = travelDepartureAirportSegment1;
  }
  
  public String getAirCarrierCodeSegment1()
  {
    return this.airCarrierCodeSegment1;
  }
  
  public void setAirCarrierCodeSegment1(String airCarrierCodeSegment1)
  {
    this.airCarrierCodeSegment1 = airCarrierCodeSegment1;
  }
  
  public String getFlightNumberSegment1()
  {
    return this.flightNumberSegment1;
  }
  
  public void setFlightNumberSegment1(String flightNumberSegment1)
  {
    this.flightNumberSegment1 = flightNumberSegment1;
  }
  
  public String getClassCodeSegment1()
  {
    return this.classCodeSegment1;
  }
  
  public void setClassCodeSegment1(String classCodeSegment1)
  {
    this.classCodeSegment1 = classCodeSegment1;
  }
  
  public String getTravelDepartureDateSegment2()
  {
    return this.travelDepartureDateSegment2;
  }
  
  public void setTravelDepartureDateSegment2(String travelDepartureDateSegment2)
  {
    this.travelDepartureDateSegment2 = travelDepartureDateSegment2;
  }
  
  public String getTravelDestinationCodeSegment2()
  {
    return this.travelDestinationCodeSegment2;
  }
  
  public void setTravelDestinationCodeSegment2(String travelDestinationCodeSegment2)
  {
    this.travelDestinationCodeSegment2 = travelDestinationCodeSegment2;
  }
  
  public String getTravelDepartureAirportSegment2()
  {
    return this.travelDepartureAirportSegment2;
  }
  
  public void setTravelDepartureAirportSegment2(String travelDepartureAirportSegment2)
  {
    this.travelDepartureAirportSegment2 = travelDepartureAirportSegment2;
  }
  
  public String getAirCarrierCodeSegment2()
  {
    return this.airCarrierCodeSegment2;
  }
  
  public void setAirCarrierCodeSegment2(String airCarrierCodeSegment2)
  {
    this.airCarrierCodeSegment2 = airCarrierCodeSegment2;
  }
  
  public String getFlightNumberSegment2()
  {
    return this.flightNumberSegment2;
  }
  
  public void setFlightNumberSegment2(String flightNumberSegment2)
  {
    this.flightNumberSegment2 = flightNumberSegment2;
  }
  
  public String getClassCodeSegment2()
  {
    return this.classCodeSegment2;
  }
  
  public void setClassCodeSegment2(String classCodeSegment2)
  {
    this.classCodeSegment2 = classCodeSegment2;
  }
  
  public String getTravelDepartureDateSegment3()
  {
    return this.travelDepartureDateSegment3;
  }
  
  public void setTravelDepartureDateSegment3(String travelDepartureDateSegment3)
  {
    this.travelDepartureDateSegment3 = travelDepartureDateSegment3;
  }
  
  public String getTravelDestinationCodeSegment3()
  {
    return this.travelDestinationCodeSegment3;
  }
  
  public void setTravelDestinationCodeSegment3(String travelDestinationCodeSegment3)
  {
    this.travelDestinationCodeSegment3 = travelDestinationCodeSegment3;
  }
  
  public String getTravelDepartureAirportSegment3()
  {
    return this.travelDepartureAirportSegment3;
  }
  
  public void setTravelDepartureAirportSegment3(String travelDepartureAirportSegment3)
  {
    this.travelDepartureAirportSegment3 = travelDepartureAirportSegment3;
  }
  
  public String getAirCarrierCodeSegment3()
  {
    return this.airCarrierCodeSegment3;
  }
  
  public void setAirCarrierCodeSegment3(String airCarrierCodeSegment3)
  {
    this.airCarrierCodeSegment3 = airCarrierCodeSegment3;
  }
  
  public String getFlightNumberSegment3()
  {
    return this.flightNumberSegment3;
  }
  
  public void setFlightNumberSegment3(String flightNumberSegment3)
  {
    this.flightNumberSegment3 = flightNumberSegment3;
  }
  
  public String getClassCodeSegment3()
  {
    return this.classCodeSegment3;
  }
  
  public void setClassCodeSegment3(String classCodeSegment3)
  {
    this.classCodeSegment3 = classCodeSegment3;
  }
  
  public String getTravelDepartureDateSegment4()
  {
    return this.travelDepartureDateSegment4;
  }
  
  public void setTravelDepartureDateSegment4(String travelDepartureDateSegment4)
  {
    this.travelDepartureDateSegment4 = travelDepartureDateSegment4;
  }
  
  public String getTravelDestinationCodeSegment4()
  {
    return this.travelDestinationCodeSegment4;
  }
  
  public void setTravelDestinationCodeSegment4(String travelDestinationCodeSegment4)
  {
    this.travelDestinationCodeSegment4 = travelDestinationCodeSegment4;
  }
  
  public String getTravelDepartureAirportSegment4()
  {
    return this.travelDepartureAirportSegment4;
  }
  
  public void setTravelDepartureAirportSegment4(String travelDepartureAirportSegment4)
  {
    this.travelDepartureAirportSegment4 = travelDepartureAirportSegment4;
  }
  
  public String getAirCarrierCodeSegment4()
  {
    return this.airCarrierCodeSegment4;
  }
  
  public void setAirCarrierCodeSegment4(String airCarrierCodeSegment4)
  {
    this.airCarrierCodeSegment4 = airCarrierCodeSegment4;
  }
  
  public String getFlightNumberSegment4()
  {
    return this.flightNumberSegment4;
  }
  
  public void setFlightNumberSegment4(String flightNumberSegment4)
  {
    this.flightNumberSegment4 = flightNumberSegment4;
  }
  
  public String getClassCodeSegment4()
  {
    return this.classCodeSegment4;
  }
  
  public void setClassCodeSegment4(String classCodeSegment4)
  {
    this.classCodeSegment4 = classCodeSegment4;
  }
  
  public String getLodgingCheckInDate()
  {
    return this.lodgingCheckInDate;
  }
  
  public void setLodgingCheckInDate(String lodgingCheckInDate)
  {
    this.lodgingCheckInDate = lodgingCheckInDate;
  }
  
  public String getLodgingCheckOutDate()
  {
    return this.lodgingCheckOutDate;
  }
  
  public void setLodgingCheckOutDate(String lodgingCheckOutDate)
  {
    this.lodgingCheckOutDate = lodgingCheckOutDate;
  }
  
  public String getLodgingRoomRate1()
  {
    return this.lodgingRoomRate1;
  }
  
  public void setLodgingRoomRate1(String lodgingRoomRate1)
  {
    this.lodgingRoomRate1 = lodgingRoomRate1;
  }
  
  public String getNumberOfNightsAtRoomRate1()
  {
    return this.numberOfNightsAtRoomRate1;
  }
  
  public void setNumberOfNightsAtRoomRate1(String numberOfNightsAtRoomRate1)
  {
    this.numberOfNightsAtRoomRate1 = numberOfNightsAtRoomRate1;
  }
  
  public String getLodgingName()
  {
    return this.lodgingName;
  }
  
  public void setLodgingName(String lodgingName)
  {
    this.lodgingName = lodgingName;
  }
  
  public String getLodgingRegionCode()
  {
    return this.lodgingRegionCode;
  }
  
  public void setLodgingRegionCode(String lodgingRegionCode)
  {
    this.lodgingRegionCode = lodgingRegionCode;
  }
  
  public String getLodgingCountryCode()
  {
    return this.lodgingCountryCode;
  }
  
  public void setLodgingCountryCode(String lodgingCountryCode)
  {
    this.lodgingCountryCode = lodgingCountryCode;
  }
  
  public String getLodgingCityName()
  {
    return this.lodgingCityName;
  }
  
  public void setLodgingCityName(String lodgingCityName)
  {
    this.lodgingCityName = lodgingCityName;
  }
  
  public String getReserved()
  {
    return this.reserved;
  }
  
  public void setReserved(String reserved)
  {
    this.reserved = reserved;
  }
}
