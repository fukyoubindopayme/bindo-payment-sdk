package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.RecordType;

public class TransactionBatchTrailerBean
{
  private String recordType = RecordType.Transaction_Batch_Trailer.getRecordType();
  private String recordNumber;
  private String merchantId;
  private String servAgntMerchID;
  private String tbtIdentificationNumber;
  private String tbtCreationDate;
  private String totalNoOfTabs;
  private String tbtAmount;
  private String tbtAmountSign;
  private String tbtCurrencyCode;
  private String tbtImageSequenceNumber;
  private String tbtField4Reserved;
  private String tbtField8Reserved;
  private String tbtField12Reserved;
  private String tbtField13Reserved;
  private String tbtField14Reserved;
  private String tbtField16Reserved;
  
  public String getRecordType()
  {
    return this.recordType;
  }
  
  public void setRecordType(String recordType)
  {
    this.recordType = recordType;
  }
  
  public String getRecordNumber()
  {
    return this.recordNumber;
  }
  
  public void setRecordNumber(String recordNumber)
  {
    this.recordNumber = recordNumber;
  }
  
  public String getMerchantId()
  {
    return this.merchantId;
  }
  
  public void setMerchantId(String merchantId)
  {
    this.merchantId = merchantId;
  }
  
  public String getTbtIdentificationNumber()
  {
    return this.tbtIdentificationNumber;
  }
  
  public void setTbtIdentificationNumber(String tbtIdentificationNumber)
  {
    this.tbtIdentificationNumber = tbtIdentificationNumber;
  }
  
  public String getTbtCreationDate()
  {
    return this.tbtCreationDate;
  }
  
  public void setTbtCreationDate(String tbtCreationDate)
  {
    this.tbtCreationDate = tbtCreationDate;
  }
  
  public String getTotalNoOfTabs()
  {
    return this.totalNoOfTabs;
  }
  
  public void setTotalNoOfTabs(String totalNoOfTabs)
  {
    this.totalNoOfTabs = totalNoOfTabs;
  }
  
  public String getTbtAmount()
  {
    return this.tbtAmount;
  }
  
  public void setTbtAmount(String tbtAmount)
  {
    this.tbtAmount = tbtAmount;
  }
  
  public String getTbtAmountSign()
  {
    return this.tbtAmountSign;
  }
  
  public void setTbtAmountSign(String tbtAmountSign)
  {
    this.tbtAmountSign = tbtAmountSign;
  }
  
  public String getTbtCurrencyCode()
  {
    return this.tbtCurrencyCode;
  }
  
  public void setTbtCurrencyCode(String tbtCurrencyCode)
  {
    this.tbtCurrencyCode = tbtCurrencyCode;
  }
  
  public String getTbtImageSequenceNumber()
  {
    return this.tbtImageSequenceNumber;
  }
  
  public void setTbtImageSequenceNumber(String tbtImageSequenceNumber)
  {
    this.tbtImageSequenceNumber = tbtImageSequenceNumber;
  }
  
  public String getTbtField4Reserved()
  {
    return this.tbtField4Reserved;
  }
  
  public void setTbtField4Reserved(String tbtField4Reserved)
  {
    this.tbtField4Reserved = tbtField4Reserved;
  }
  
  public String getTbtField8Reserved()
  {
    return this.tbtField8Reserved;
  }
  
  public void setTbtField8Reserved(String tbtField8Reserved)
  {
    this.tbtField8Reserved = tbtField8Reserved;
  }
  
  public String getTbtField12Reserved()
  {
    return this.tbtField12Reserved;
  }
  
  public void setTbtField12Reserved(String tbtField12Reserved)
  {
    this.tbtField12Reserved = tbtField12Reserved;
  }
  
  public String getTbtField13Reserved()
  {
    return this.tbtField13Reserved;
  }
  
  public void setTbtField13Reserved(String tbtField13Reserved)
  {
    this.tbtField13Reserved = tbtField13Reserved;
  }
  
  public String getTbtField14Reserved()
  {
    return this.tbtField14Reserved;
  }
  
  public void setTbtField14Reserved(String tbtField14Reserved)
  {
    this.tbtField14Reserved = tbtField14Reserved;
  }
  
  public String getTbtField16Reserved()
  {
    return this.tbtField16Reserved;
  }
  
  public void setTbtField16Reserved(String tbtField16Reserved)
  {
    this.tbtField16Reserved = tbtField16Reserved;
  }
  
  public String getServAgntMerchID()
  {
    return this.servAgntMerchID;
  }
  
  public void setServAgntMerchID(String servAgntMerchID)
  {
    this.servAgntMerchID = servAgntMerchID;
  }
}
