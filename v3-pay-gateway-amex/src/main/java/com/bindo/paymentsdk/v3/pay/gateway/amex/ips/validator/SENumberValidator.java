package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.validator;

public class SENumberValidator {
    public SENumberValidator() {
    }

    public static boolean modulusNineCheck(String seNumber) {
        boolean isVAlid = false;
        if (seNumber.length() != 10) {
            return isVAlid;
        } else {
            int digits = seNumber.length();
            long checkDigit = 0L;

            try {
                checkDigit = (long)Integer.parseInt(String.valueOf(seNumber.charAt(digits - 1)));
            } catch (NumberFormatException var16) {
                return false;
            }

            seNumber = seNumber.substring(0, digits - 1);
            boolean var5 = false;

            int intfirstThreeDigits;
            try {
                intfirstThreeDigits = Integer.parseInt(seNumber.substring(0, 3));
            } catch (NumberFormatException var15) {
                return false;
            }

            if (intfirstThreeDigits < 930 || intfirstThreeDigits > 939) {
                String strDigits = seNumber.substring(1, seNumber.length());
                seNumber = "0" + strDigits;
            }

            digits = seNumber.length();
            long sum = 0L;
            long oddsum = 0L;

            for(int count = 0; count < digits; ++count) {
                int digit = 0;
                if (count % 2 == 0) {
                    try {
                        digit = Integer.parseInt(String.valueOf(seNumber.charAt(count))) * 2;
                    } catch (NumberFormatException var13) {
                        return false;
                    }

                    if (digit > 9) {
                        digit -= 9;
                    }

                    sum += (long)digit;
                } else {
                    try {
                        oddsum += (long)Integer.parseInt(String.valueOf(seNumber.charAt(count)));
                    } catch (NumberFormatException var14) {
                        return false;
                    }
                }
            }

            sum += oddsum;
            if (sum % 10L == 0L && checkDigit == 0L) {
                isVAlid = true;
            }

            long newSum = 0L;
            if (sum % 10L != 0L) {
                newSum = sum + (10L - sum % 10L);
                if (newSum - sum == checkDigit) {
                    isVAlid = true;
                    return isVAlid;
                }
            }

            return isVAlid;
        }
    }
}
