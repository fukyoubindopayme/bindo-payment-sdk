package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean;

import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.connection.PropertyReader;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.Constants;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.AuthErrorCodes;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.utils.ISOUtil;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.validator.Validator;
import java.util.List;

public class NationalUseData2Bean
{
  private String vli;
  private String primary_id = "AX";
  private String secondary_id = "ASK";
  private String electronic_commerce_indicator;
  private String amex_verification_value_id;
  private String amex_verification_value;
  private String safekey_transactionid;
  private String safekey_transactionid_value;
  private String tokenDataBlockAId;
  private String tokenDataBlockA;
  private String tokenDataBlockBId;
  private String tokenDataBlockB;
  
  public String getVli()
  {
    return this.vli;
  }
  
  public void setVli(String vli)
  {
    this.vli = vli;
  }
  
  public String getPrimaryId()
  {
    return this.primary_id;
  }
  
  public void setPrimaryId(String primary_id)
  {
    this.primary_id = primary_id;
  }
  
  public String getSecondaryId()
  {
    return this.secondary_id;
  }
  
  public void setSecondaryId(String secondary_id)
  {
    this.secondary_id = secondary_id;
  }
  
  public String getElectronicCommerceIndicator()
  {
    return this.electronic_commerce_indicator;
  }
  
  public void setElectronicCommerceIndicator(String electronic_commerce_indicator)
  {
    this.electronic_commerce_indicator = electronic_commerce_indicator;
  }
  
  public String getAmexVerificationValueId()
  {
    return this.amex_verification_value_id;
  }
  
  public void setAmexVerificationValueId(String amex_verification_value_id)
  {
    this.amex_verification_value_id = amex_verification_value_id;
  }
  
  public String getAmexVerificationValue()
  {
    return this.amex_verification_value;
  }
  
  public void setAmexVerificationValue(String amex_verification_value)
  {
    this.amex_verification_value = amex_verification_value;
  }
  
  public String getSafekeyTransactionId()
  {
    return this.safekey_transactionid;
  }
  
  public void setSafekeyTransactionId(String safekey_transactionid)
  {
    this.safekey_transactionid = safekey_transactionid;
  }
  
  public String getSafekeyTransactionIdValue()
  {
    return this.safekey_transactionid_value;
  }
  
  public void setSafekeyTransactionIdValue(String safekey_transactionid_value)
  {
    this.safekey_transactionid_value = safekey_transactionid_value;
  }
  
  public String getTokenDataBlockAId()
  {
    return this.tokenDataBlockAId;
  }
  
  public void setTokenDataBlockAId(String tokenDataBlockAId)
  {
    this.tokenDataBlockAId = tokenDataBlockAId;
  }
  
  public String getTokenDataBlockA()
  {
    return this.tokenDataBlockA;
  }
  
  public void setTokenDataBlockA(String tokenDataBlockA)
  {
    this.tokenDataBlockA = tokenDataBlockA;
  }
  
  public String getTokenDataBlockBId()
  {
    return this.tokenDataBlockBId;
  }
  
  public void setTokenDataBlockBId(String tokenDataBlockBId)
  {
    this.tokenDataBlockBId = tokenDataBlockBId;
  }
  
  public String getTokenDataBlockB()
  {
    return this.tokenDataBlockB;
  }
  
  public void setTokenDataBlockB(String tokenDataBlockB)
  {
    this.tokenDataBlockB = tokenDataBlockB;
  }
  
  public String populateNationalUseData(AuthorizationResponseBean authorizationResponseBean)
  {
    boolean debugflag = PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON");
    StringBuffer nationalData = new StringBuffer();
    
    StringBuffer displayBuffer = new StringBuffer();
    if (ISOUtil.IsNullOrEmpty(getPrimaryId()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD61_NATIONALUSEDATA2PRIMARYID);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if ((Validator.validateSubFields(getPrimaryId(), 2, 2, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[4])) && (getPrimaryId().equalsIgnoreCase("AX")))
    {
      nationalData.append(getPrimaryId());
      displayBuffer.append("PRIMARY_ID               ").append(" :: ").append(getPrimaryId()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD61_NATIONALUSEDATA2PRIMARYID);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getSecondaryId()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD61_NATIONALUSEDATA2SECONDARYID);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if ((Validator.validateSubFields(getSecondaryId(), 3, 3, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[4])) && (getSecondaryId().equalsIgnoreCase("ASK")))
    {
      nationalData.append(getSecondaryId());
      displayBuffer.append("SECONDARY_ID             ").append(" :: ").append(getSecondaryId()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD61_NATIONALUSEDATA2SECONDARYID);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getElectronicCommerceIndicator()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD61_NATIONALUSEDATA2ELECTRONICCOMMERCEINDICATOR);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if ((Validator.validateSubFields(getElectronicCommerceIndicator(), 2, 2, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1])) && ((getElectronicCommerceIndicator().equalsIgnoreCase("05")) || (getElectronicCommerceIndicator().equalsIgnoreCase("06")) || (getElectronicCommerceIndicator().equalsIgnoreCase("07"))))
    {
      nationalData.append(getElectronicCommerceIndicator());
      displayBuffer.append("ELEC_COMM_INDICATOR      ").append(" :: ").append(getElectronicCommerceIndicator()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD61_NATIONALUSEDATA2ELECTRONICCOMMERCEINDICATOR);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getAmexVerificationValueId()))
    {
      if (!ISOUtil.IsNullOrEmpty(getAmexVerificationValue()))
      {
        nationalData.append("AEVV");
        displayBuffer.append("AMEX_VERFN_VALUE         ").append(" :: ").append("AEVV").append(Constants.NEW_LINE_CHAR);
      }
    }
    else if ((Validator.validateSubFields(getAmexVerificationValueId(), 4, 4, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[4])) && (getAmexVerificationValueId().equalsIgnoreCase("AEVV")))
    {
      nationalData.append(getAmexVerificationValueId());
      displayBuffer.append("AMEX_VERFN_VALUE_ID      ").append(" :: ").append(getAmexVerificationValueId()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD61_NATIONALUSEDATA2AEXPVERIFICATIONVALUEID);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getAmexVerificationValue()))
    {
      if ((!ISOUtil.IsNullOrEmpty(getElectronicCommerceIndicator())) && ((getElectronicCommerceIndicator().equalsIgnoreCase("05")) || (getElectronicCommerceIndicator().equalsIgnoreCase("06"))))
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD61_NATIONALUSEDATA2AEXPVERIFICATIONVALUEIDVALUE);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
    }
    else if (Validator.validateSubFields(getAmexVerificationValue(), 40, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1]))
    {
      nationalData.append(getAmexVerificationValue());
      displayBuffer.append("AMEX_VERFN_VALUE         ").append(" :: ").append(getAmexVerificationValue()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD61_NATIONALUSEDATA2AEXPVERIFICATIONVALUEIDVALUE);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getSafekeyTransactionId()))
    {
      if (!ISOUtil.IsNullOrEmpty(getSafekeyTransactionIdValue()))
      {
        nationalData.append("XID");
        displayBuffer.append("SAFEKEY_TRANSCTN_VALUE   ").append(" :: ").append("XID").append(Constants.NEW_LINE_CHAR);
      }
    }
    else if ((Validator.validateSubFields(getSafekeyTransactionId(), 3, 3, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[4])) && (getSafekeyTransactionId().equalsIgnoreCase("XID")))
    {
      nationalData.append(getSafekeyTransactionId());
      displayBuffer.append("SAFEKEY_TRANSCTN_ID      ").append(" :: ").append(getSafekeyTransactionId()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD61_NATIONALUSEDATA2AEXPSAFKEYTRANSACTIONID);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (!ISOUtil.IsNullOrEmpty(getSafekeyTransactionIdValue())) {
      if (Validator.validateSubFields(getSafekeyTransactionIdValue(), 40, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1]))
      {
        nationalData.append(getSafekeyTransactionIdValue());
        displayBuffer.append("SAFEKEY_TRANSCTN_VALUE   ").append(" :: ").append(getSafekeyTransactionIdValue()).append(Constants.NEW_LINE_CHAR);
      }
      else
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD61_NATIONALUSEDATA2AEXPSAFKEYTRANSACTIONIDVALUE);
        authorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
    }
    if (debugflag)
    {
      Log.i("National Use Data2 Bean\r\n----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n-                                 National Use Data2 Bean                                                                                                                                       -\r\n---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n" +
      
        displayBuffer.toString() + 
        "\r\n" + "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
      
      Log.i("Populated the National Use Data is -- " + nationalData);
    }
    return nationalData.toString();
  }
  
  public String populateNationalUseDataForPmtTokenization(AuthorizationResponseBean pAuthorizationResponseBean)
  {
    boolean debugflag = PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON");
    
    StringBuffer lNationalUseDataBean = new StringBuffer();
    StringBuffer lDisplayBuffer = new StringBuffer();
    ISOUtil lISOUtil = new ISOUtil();
    int lNationalUseDataVLI = 0;
    String lEncodedVLI = "";
    if (ISOUtil.IsNullOrEmpty(getPrimaryId()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD61_NATIONALUSEDATA2PRIMARYID);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if (!"AX".equalsIgnoreCase(getPrimaryId()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD61_NATIONALUSEDATA2PRIMARYID);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else
    {
      lNationalUseDataBean.append(lISOUtil.convertTo(getPrimaryId(), "ENC_HEX"));
      lDisplayBuffer.append("PRIMARY_ID                ").append(" :: ").append(getPrimaryId()).append(Constants.NEW_LINE_CHAR);
      lNationalUseDataVLI = 2;
    }
    if (ISOUtil.IsNullOrEmpty(getSecondaryId()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD61_NATIONALUSEDATA2SECONDARYID);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if (!"TKN".equalsIgnoreCase(getSecondaryId()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD61_NATIONALUSEDATA2SECONDARYID);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else
    {
      lNationalUseDataBean.append(lISOUtil.convertTo(getSecondaryId(), "ENC_HEX"));
      lDisplayBuffer.append("SECONDARY_ID                ").append(" :: ").append(getSecondaryId()).append(Constants.NEW_LINE_CHAR);
      lNationalUseDataVLI += 3;
    }
    if (ISOUtil.IsNullOrEmpty(getElectronicCommerceIndicator()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD61_NATIONALUSEDATA2ELECTRONICCOMMERCEINDICATOR);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if (!"20".equalsIgnoreCase(getElectronicCommerceIndicator()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD61_NATIONALUSEDATA2ELECTRONICCOMMERCEINDICATOR);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else
    {
      lNationalUseDataBean.append(lISOUtil.convertTo(getElectronicCommerceIndicator(), "ENC_HEX"));
      lDisplayBuffer.append("ELEC_COMM_INDICATOR      ").append(" :: ").append(getElectronicCommerceIndicator()).append(Constants.NEW_LINE_CHAR);
      lNationalUseDataVLI += 2;
    }
    if (ISOUtil.IsNullOrEmpty(getTokenDataBlockAId()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD61_NATIONALUSEDATA2_TKN_DATA_BLOCKA_ID);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if (!"TDBA".equalsIgnoreCase(getTokenDataBlockAId()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD61_NATIONALUSEDATA2_TKN_DATA_BLOCKA_ID);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else
    {
      lNationalUseDataBean.append(lISOUtil.convertTo(getTokenDataBlockAId(), "ENC_HEX"));
      lDisplayBuffer.append("TOKEN DATA BLOCK A ID      ").append(" :: ").append(getTokenDataBlockAId()).append(Constants.NEW_LINE_CHAR);
      lNationalUseDataVLI += 4;
    }
    if (ISOUtil.IsNullOrEmpty(getTokenDataBlockA()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD61_NATIONALUSEDATA2_TKN_DATA_BLOCKA);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if (!Validator.validateSubFields(getTokenDataBlockA(), 40, 40, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[6]))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD61_NATIONALUSEDATA2_TKN_DATA_BLOCKA);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else
    {
      lNationalUseDataBean.append(getTokenDataBlockA());
      lDisplayBuffer.append("TOKEN DATA BLOCK A      ").append(" :: ").append(getTokenDataBlockA()).append(Constants.NEW_LINE_CHAR);
      lNationalUseDataVLI += 20;
    }
    if (!ISOUtil.IsNullOrEmpty(getTokenDataBlockB()))
    {
      if (ISOUtil.IsNullOrEmpty(getTokenDataBlockBId()))
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD61_NATIONALUSEDATA2_TKN_DATA_BLOCKB_ID);
        pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
      else if (!"DBB".equalsIgnoreCase(getTokenDataBlockBId()))
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD61_NATIONALUSEDATA2_TKN_DATA_BLOCKB_ID);
        pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
      else
      {
        lNationalUseDataBean.append(lISOUtil.convertTo(getTokenDataBlockBId(), "ENC_HEX"));
        lDisplayBuffer.append("TOKEN DATA BLOCK B ID      ").append(" :: ").append(getTokenDataBlockBId()).append(Constants.NEW_LINE_CHAR);
        lNationalUseDataVLI += 3;
      }
      if (!Validator.validateSubFields(getTokenDataBlockB(), 40, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[6]))
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD61_NATIONALUSEDATA2_TKN_DATA_BLOCKB);
        pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
      else
      {
        lNationalUseDataBean.append(getTokenDataBlockB());
        lDisplayBuffer.append("TOKEN DATA BLOCK B      ").append(" :: ").append(getTokenDataBlockB()).append(Constants.NEW_LINE_CHAR);
        
        lNationalUseDataVLI += getTokenDataBlockB().length() / 2;
      }
    }
    String lVLI = ISOUtil.padString(String.valueOf(lNationalUseDataVLI), 3, "0", false, true);
    lEncodedVLI = lISOUtil.convertTo(lVLI, "ENC_HEX");
    if (debugflag)
    {
      Log.i("National Use Data2 Bean\r\n----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n-                                 National Use Data2 Bean                                                                                                                                       -\r\n---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n" +
      
        lDisplayBuffer.toString() + 
        "\r\n" + "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
      
      Log.i("Populated the National Use Data is -- " + lNationalUseDataBean);
    }
    return lEncodedVLI + lNationalUseDataBean.toString();
  }
  
  public String toString()
  {
    StringBuffer buffer = new StringBuffer();
    
    buffer.append("VARIABLE_DATA_LENGTH").append(" :: ").append(getVli()).append(Constants.NEW_LINE_CHAR);
    buffer.append("PRIMARY_ID").append(" :: ").append(getPrimaryId()).append(Constants.NEW_LINE_CHAR);
    buffer.append("SECONDARY_ID").append(" :: ").append(getSecondaryId()).append(Constants.NEW_LINE_CHAR);
    buffer.append("ELEC_COMM_INDICATOR").append(" :: ").append(getElectronicCommerceIndicator()).append(Constants.NEW_LINE_CHAR);
    
    buffer.append("AMEX_VERFN_VALUE_ID").append(" :: ").append(getAmexVerificationValueId()).append(Constants.NEW_LINE_CHAR);
    buffer.append("AMEX_VERFN_VALUE").append(" :: ").append(getAmexVerificationValue()).append(Constants.NEW_LINE_CHAR);
    buffer.append("SAFEKEY_TRANSCTN_ID").append(" :: ").append(getSafekeyTransactionId()).append(Constants.NEW_LINE_CHAR);
    buffer.append("SAFEKEY_TRANSCTN_VALUE").append(" :: ").append(getSafekeyTransactionIdValue()).append(Constants.NEW_LINE_CHAR);
    
    buffer.append("TOKEN DATA BLOCK A ID").append(" :: ").append(getTokenDataBlockAId()).append(Constants.NEW_LINE_CHAR);
    buffer.append("TOKEN DATA BLOCK A").append(" :: ").append(getTokenDataBlockA()).append(Constants.NEW_LINE_CHAR);
    buffer.append("TOKEN DATA BLOCK B").append(" :: ").append(getTokenDataBlockB()).append(Constants.NEW_LINE_CHAR);
    buffer.append("TOKEN DATA BLOCK B ID").append(" :: ").append(getTokenDataBlockBId()).append(Constants.NEW_LINE_CHAR);
    return buffer.toString();
  }
}
