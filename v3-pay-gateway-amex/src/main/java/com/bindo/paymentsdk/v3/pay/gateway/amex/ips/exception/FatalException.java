package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.exception;

public class FatalException
  extends Exception
{
  public FatalException() {}
  
  public FatalException(String message, Throwable cause)
  {
    super(message, cause);
  }
  
  public FatalException(String message)
  {
    super(message);
  }
  
  public FatalException(Throwable cause)
  {
    super(cause);
  }
}
