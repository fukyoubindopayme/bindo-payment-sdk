package com.bindo.paymentsdk.v3.pay.gateway.amex.util.juice;

import java.security.NoSuchAlgorithmException;

public class JuiceP {
    private int keyLength;
    private String defaultKey;
    private int padTo;
    private int algType;
    public static final int ALGTYPE_CIPHER = 1;
    public static final int ALGTYPE_CIPHER_CBC = 2;
    public static final int ALGTYPE_MD = 3;
    private boolean enabled;
    private int[] keyPtIx;
    private boolean modeCBC;

    protected JuiceP(int type, JuicePK ptKeys, int[] ix, int pad_to, boolean status) throws NoSuchAlgorithmException {
        this.algType = type;
        if (this.algType == 2) {
            this.modeCBC = true;
        }

        if (this.algType == 1 || this.modeCBC) {
            this.setKeyPtIx(ix, ptKeys);
            this.padTo = pad_to;
        }

        this.enabled = status;
    }

    protected int getAlgType() {
        return this.algType;
    }

    protected String getKey() {
        return this.defaultKey;
    }

    protected int getKeyLength() {
        return this.keyLength;
    }

    protected int[] getKeyPtIx() {
        return this.keyPtIx;
    }

    protected int getPadTo() {
        return this.padTo;
    }

    protected boolean isBlockCipher() {
        return this.algType == 1 || this.algType == 2;
    }

    protected boolean isEnabled() {
        return this.enabled;
    }

    protected boolean isMessageDigest() {
        return this.algType == 3;
    }

    public boolean isModeCBC() {
        return this.modeCBC;
    }

    protected boolean needPadding() {
        return this.padTo > 1;
    }

    private void setKey(String key) {
        this.defaultKey = key;
    }

    private void setKeyLength(int len) {
        this.keyLength = len;
    }

    private void setKeyPtIx(int[] newKeyPtIx, JuicePK partialKeys) throws NoSuchAlgorithmException {
        StringBuffer key = new StringBuffer();
        this.keyPtIx = newKeyPtIx;

        for(int i = 0; i < this.keyPtIx.length; ++i) {
            key.append(Hex.toString(partialKeys.getPartialKey(this.keyPtIx[i])));
        }

        this.setKey(key.toString());
        this.setKeyLength(key.length());
    }

    protected void setModeCBC(boolean newModeCBC) {
        this.modeCBC = newModeCBC;
    }
}
