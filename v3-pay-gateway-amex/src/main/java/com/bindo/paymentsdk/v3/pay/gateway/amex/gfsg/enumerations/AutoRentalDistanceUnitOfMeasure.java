package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations;

public enum AutoRentalDistanceUnitOfMeasure
{
  Miles("M"),  Kilometers("K");
  
  private final String autoRentalDistanceUnitOfMeasure;
  
  private AutoRentalDistanceUnitOfMeasure(String autoRentalDistanceUnitOfMeasure)
  {
    this.autoRentalDistanceUnitOfMeasure = autoRentalDistanceUnitOfMeasure;
  }
  
  public String getAutoRentalDistanceUnitOfMeasure()
  {
    return this.autoRentalDistanceUnitOfMeasure;
  }
}
