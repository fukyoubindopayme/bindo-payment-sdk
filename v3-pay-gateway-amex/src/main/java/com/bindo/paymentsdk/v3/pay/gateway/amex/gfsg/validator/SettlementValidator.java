package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.ErrorObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.SettlementRequestDataObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceBasicBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionTBTSpecificBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.connection.PropertyReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;

public class SettlementValidator {
  private static final Logger LOGGER = Logger.getLogger(SettlementValidator.class);
  private static boolean debugFlag;

  public SettlementValidator() {
  }

  public static List<ErrorObject> validateISOSettlementRecords(SettlementRequestDataObject settlementReqBean) throws Exception {
    if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
      debugFlag = true;
      LOGGER.info("Entered into validateISOSettlementRecords()");
    }

    ArrayList validateErrorList = new ArrayList();

    try {
      com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionTBTSpecificBean transactionTBTSpecificType;
      if (!settlementReqBean.getTransactionTBTSpecificType().isEmpty()) {
        for(Iterator var3 = settlementReqBean.getTransactionTBTSpecificType().iterator(); var3.hasNext(); TBTRecValidator.validateTBTRecord(transactionTBTSpecificType, validateErrorList)) {
          transactionTBTSpecificType = (TransactionTBTSpecificBean)var3.next();
          if (transactionTBTSpecificType.getTransactionAdviceBasicType() != null && !transactionTBTSpecificType.getTransactionAdviceBasicType().isEmpty()) {
            Iterator var5 = transactionTBTSpecificType.getTransactionAdviceBasicType().iterator();

            while(var5.hasNext()) {
              TransactionAdviceBasicBean transactionAdviceBasicType = (TransactionAdviceBasicBean)var5.next();
              TABRecValidator.validateTABRecordType(transactionAdviceBasicType, validateErrorList);
            }
          }
        }
      }

      TFHRecValidator.validateTFHRecord(settlementReqBean.getTransactionFileHeaderType(), validateErrorList);
      TFSRecValidator.validateTFSRecord(settlementReqBean.getTransactionFileSummaryType(), settlementReqBean, validateErrorList);
    } catch (Exception var6) {
      if (debugFlag) {
        LOGGER.fatal("Error" + var6);
      }
    }

    if (debugFlag) {
      LOGGER.info("Exiting from validateISOSettlementRecords()");
    }

    return validateErrorList;
  }

  public static List<ErrorObject> validateConnectionInfo(PropertyReader propertyReader) {
    List<ErrorObject> connectionInfoErrorList = new ArrayList();
    ErrorObject connectionErrorObj;
    if (CommonValidator.isNullOrEmpty(PropertyReader.getFtpURL())) {
      connectionErrorObj = new ErrorObject("2", "Invalid FTP URL");
      connectionInfoErrorList.add(connectionErrorObj);
      if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
        LOGGER.error("Invalid FTP URL");
      }
    }

    if (CommonValidator.isNullOrEmpty(PropertyReader.getFtpUserID())) {
      connectionErrorObj = new ErrorObject("3", "Invalid FTP User ID");
      connectionInfoErrorList.add(connectionErrorObj);
      if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
        LOGGER.error("Invalid FTP User ID");
      }
    }

    if (CommonValidator.isNullOrEmpty(PropertyReader.getFtpPassword())) {
      connectionErrorObj = new ErrorObject("4", "Invalid FTP Password");
      connectionInfoErrorList.add(connectionErrorObj);
      if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
        LOGGER.error("Invalid FTP Password");
      }
    }

    if (CommonValidator.isNullOrEmpty(PropertyReader.getFileStorage()) || !PropertyReader.getFileStorage().equalsIgnoreCase("ON") && !PropertyReader.getFileStorage().equalsIgnoreCase("OFF")) {
      connectionErrorObj = new ErrorObject("5", "Invalid File Storage Flag");
      connectionInfoErrorList.add(connectionErrorObj);
      if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
        LOGGER.error("Invalid File Storage Flag");
      }
    }

    if (!CommonValidator.isNullOrEmpty(PropertyReader.getFileStorage()) && !PropertyReader.getFileStorage().equalsIgnoreCase("ON")) {
      if (CommonValidator.isNullOrEmpty(PropertyReader.getFtpLocalUploadFilePath())) {
        connectionErrorObj = new ErrorObject("6", "Invalid FTP Local Upload File Path");
        connectionInfoErrorList.add(connectionErrorObj);
        if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
          LOGGER.error("Invalid FTP Local Upload File Path");
        }
      }

      if (CommonValidator.isNullOrEmpty(PropertyReader.getFtpLocalDownloadFilePath())) {
        connectionErrorObj = new ErrorObject("7", "Invalid FTP Local Download File Path");
        connectionInfoErrorList.add(connectionErrorObj);
        if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
          LOGGER.error("Invalid FTP Local Download File Path");
        }
      }
    }

    if (CommonValidator.isNullOrEmpty(PropertyReader.getFtpUploadFileName())) {
      connectionErrorObj = new ErrorObject("8", "Invalid FTP Upload File Name");
      connectionInfoErrorList.add(connectionErrorObj);
      if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
        LOGGER.error("Invalid FTP Upload File Name");
      }
    }

    if (CommonValidator.isNullOrEmpty(PropertyReader.getFtpDownloadAcknowledgeFileName())) {
      connectionErrorObj = new ErrorObject("9", "Invalid FTP Download Acknowledge File Name");
      connectionInfoErrorList.add(connectionErrorObj);
      if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
        LOGGER.error("Invalid FTP Download Acknowledge File Name");
      }
    }

    if (CommonValidator.isNullOrEmpty(PropertyReader.getFtpDownloadPrintedFileName())) {
      connectionErrorObj = new ErrorObject("10", "Invalid FTP Download Printed File Name");
      connectionInfoErrorList.add(connectionErrorObj);
      if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
        LOGGER.error("Invalid FTP Download Printed File Name");
      }
    }

    if (CommonValidator.isNullOrEmpty(PropertyReader.getFtpDownloadRawFileName())) {
      connectionErrorObj = new ErrorObject("11", "Invalid FTP Download Raw File Name");
      connectionInfoErrorList.add(connectionErrorObj);
      if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
        LOGGER.error("Invalid FTP Download Raw File Name");
      }
    }

    return connectionInfoErrorList;
  }
}
