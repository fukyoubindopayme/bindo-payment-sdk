package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants;

public abstract interface ErrorMessageConstants
{
  public static final String RECORD_TYPE = "RecordType:";
  public static final String KEY_VALUE_SEPRATER = "|";
  public static final String NEW_LINE_CHAR = "\n";
  public static final String RECORD_TFH = "TFH";
  public static final String RECORD_TFS = "TFS";
  public static final String RECORD_TBT = "TBT";
  public static final String RECORD_RECORDTYPE = "RecordType:";
  public static final String RECORD_NUMBER = "RecordNumber:";
  public static final String RECORD_NUMBERVALUE = "recordNumber";
  public static final String ERROR_SUBDIS_EMPTY = "This field is mandatory and cannot be empty";
  public static final String ERROR_SUBDIS_MAXLEN = "This field length Cannot be greater than 3";
  public static final String ERROR_SUBDIS_MAXLEN1 = "This field length Cannot be greater than 1";
  public static final String ERROR_SUBDIS_MAXLEN2 = "This field length Cannot be greater than 2";
  public static final String ERROR_SUBDIS_ALPHANUMERIC = "This field can only be AlphaNumeric";
  public static final String ERROR_SUBDIS_NUMERIC = "This field can only be Numeric";
  public static final String ERROR_SUBDIS_MAXLEN8 = "This field length Cannot be greater than 8";
  public static final String ERROR_SUBDIS_MAXLEN4 = "This field length Cannot be greater than 4";
  public static final String ERROR_SUBDIS_MAXLEN5 = "This field length Cannot be greater than 5";
  public static final String ERROR_SUBDIS_MAXLEN6 = "This field length Cannot be greater than 6";
  public static final String ERROR_SUBDIS_FIXLEN6 = "This field length should be 6";
  public static final String ERROR_SUBDIS_MAXLEN9 = "This field length Cannot be greater than 9";
  public static final String ERROR_SUBDIS_MAXLEN11 = "This field length Cannot be greater than 11";
  public static final String ERROR_SUBDIS_MAXLEN12 = "This field length Cannot be greater than 12";
  public static final String ERROR_SUBDIS_MAXLEN14 = "This field length Cannot be greater than 14";
  public static final String ERROR_SUBDIS_MAXLEN15 = "This field length Cannot be greater than 15";
  public static final String ERROR_SUBDIS_MAXLEN20 = "This field length Cannot be greater than 20";
  public static final String ERROR_SUBDIS_MAXLEN21 = "This field length Cannot be greater than 21";
  public static final String ERROR_SUBDIS_MAXLEN30 = "This field length Cannot be greater than 30";
  public static final String ERROR_SUBDIS_MAXLEN40 = "This field length Cannot be greater than 40";
  public static final String ERROR_SUBDIS_MAXLEN19 = "This field length Cannot be greater than 19";
  public static final String ERROR_SUBDIS_MAXLEN38 = "This field length Cannot be greater than 38";
  public static final String ERROR_SUBDIS_MINLEN1 = "This field length Cannot be less than 8";
  public static final String RECORD_FIELDNAME = "SubmitterId";
  public static final String RECORD_SUBFILEREFNUMBER = "Submitter File Reference Number:";
  public static final String RECORD_SUBFILESEQNUMBER = "SubmitterFileSequenceNumber:";
  public static final String RECORD_FILECRDATE = "FileCreationDate";
  public static final String ERROR_SUBDIS_FILEPASTDATE = "File Creation Date too many days in past";
  public static final String ERROR_SUBDIS_FILEFUTUREDATE = "File Creation Date too many days in future";
  public static final String RECORD_FILECRTIME = "FileCreationTime";
  public static final String RECORD_FILEVERNO = "FileVersionNumber";
  public static final String FILED_NUMBEROFDEBITS = "numberOfDebits";
  public static final String FILED_HTDAMT = "Hash Total Debit Amount";
  public static final String FILED_NUMBEROFCREDITS = "numberOfCredits";
  public static final String FILED_HTCRDAMT = "Hash Total Credit Amount";
  public static final String FILED_HTAMT = "HashTotalAmount";
  public static final String FILED_MERCHANTID = "MerchantId";
  public static final String FILED_SERVAGNTMERCHANTID = "Service Agent MerchantId";
  public static final String FILED_TBTID = "TBTID";
  public static final String FILED_TBTCRTDDATE = "TbtCreationDate";
  public static final String FILED_TOTNOOFTBS = "TotalNoOfTabs";
  public static final String FILED_TBTAMT = "TBTAmount";
  public static final String FILED_TBTAMTSIGN = "TbtAmountSign";
  public static final String FILED_TBTCURNCYCODE = "TbtCurrencyCode";
  public static final String FILED_TRANSID = "TransactionIdentifier";
  public static final String FILED_FORMATCODE = "FormatCode";
  public static final String FILED_MEDIACODE = "MediaCode";
  public static final String FILED_SUBMISSIONMTHD = "SubmissionMethod";
  public static final String FILED_APPRCODE = "ApprovalCode";
  public static final String FILED_PRIMARYACNO = "PrimaryAccountNumber";
  public static final String FILED_CARDEXPDATE = "CardExpiryDate";
  public static final String FILED_TRANSDATE = "TransactionDate";
  public static final String FILED_TRANSTIME = "TransactionTime";
  public static final String FILED_TRANSAMT = "TransactionAmount";
  public static final String FILED_POCCODE = "ProcessingCode";
  public static final String FILED_TRNCRNCYODE = "TransactionCurrencyCode";
  public static final String FILED_EXDPYMTDATA = "ExtendedPaymentData";
  public static final String FILED_MRCHNTLOCID = "MerchantLocationId";
  public static final String FILED_MRCHNTCONINFO = "MerchantContactInfo";
  public static final String FILED_TERMINALID = "Terminal Id";
  public static final String FILED_POSDATACODE = "PosDataCode";
  public static final String FILED_INVSSRVSNO = "Invoice Service Number";
  public static final String FILED_TABIMGSQNSNO = "TabImageSequenceNumber";
  public static final String FILED_MERCHANTKEYTYPE = "MatchingKeyType";
  public static final String FILED_MERCHANTKEY = "MatchingKey";
  public static final String FILED_ELECMRCIND = "ElectronicCommerceIndicator";
  public static final String FILED_ADDTYPECODE = "AddendaTypeCode";
  public static final String FILED_LOCNAME = "LocationName";
  public static final String FILED_LOCADD = "LocationAddress";
  public static final String FILED_LOCCITY = "LocationCity";
  public static final String FILED_LOCRGN = "LocationRegion";
  public static final String FILED_LOCCNTRYCODE = "LocationCountryCode";
  public static final String FILED_LOCPOSTALCODE = "LocationPostalCode";
  public static final String FILED_MERCHANTCTGRYCODE = "MerchantCategoryCode";
  public static final String FILED_SELLERID = "SellerId";
  public static final String FILED_ADDITIONALAMTTYPE1 = "AdditionalAmountType1";
  public static final String FILED_ADDITIONALAMTTYPE2 = "AdditionalAmountType2";
  public static final String FILED_ADDITIONALAMTTYPE3 = "AdditionalAmountType3";
  public static final String FILED_ADDITIONALAMTTYPE4 = "AdditionalAmountType4";
  public static final String FILED_ADDITIONALAMTTYPE5 = "AdditionalAmountType5";
  public static final String FILED_ADDITIONALAMT1 = "AdditionalAmount1";
  public static final String FILED_ADDITIONALAMT2 = "AdditionalAmount2";
  public static final String FILED_ADDITIONALAMT3 = "AdditionalAmount3";
  public static final String FILED_ADDITIONALAMT4 = "AdditionalAmount4";
  public static final String FILED_ADDITIONALAMT5 = "AdditionalAmount5";
  public static final String FILED_ADDITIONALAMTSIGN1 = "AdditionalAmountSign1";
  public static final String FILED_ADDITIONALAMTSIGN2 = "AdditionalAmountSign2";
  public static final String FILED_ADDITIONALAMTSIGN3 = "AdditionalAmountSign3";
  public static final String FILED_ADDITIONALAMTSIGN4 = "AdditionalAmountSign4";
  public static final String FILED_ADDITIONALAMTSIGN5 = "AdditionalAmountSign5";
  public static final String FILED_AUTORNTL_AGRMNTNO = "AutoRentalAgreementNumber";
  public static final String FILED_AUTORNTL_DISTANCE = "AutoRentalDistance";
  public static final String FILED_AUTORNTL_DISTANCEMESR = "AutoRentalDistanceUnitOfMeasure";
  public static final String FILED_SHIP_TO_POSTAL_CODE = "ShipToPostalCode";
  public static final String FILED_TOTAL_TAX_AMOUNT = "TotalTaxAmount";
  public static final String FILED_TAX_TYPE_CODE = "TaxTypeCode";
  public static final String ERROR_SUBDIS_TAX_TYPE_CODE_REQ = "Tax Type Code required when Total Tax Amount is populated";
  public static final String ERROR_SUBDIS_MISSING = "This field is missing or invalid";
  public static final String ERROR_SUBDIS_PROCESS = "This Processing Code is Only Valid For Japan";
  public static final String ERROR_SUBDIS_PROCESS_COUNTRY = "This Country Is Not Valid For Any Other Processing Code";
}
