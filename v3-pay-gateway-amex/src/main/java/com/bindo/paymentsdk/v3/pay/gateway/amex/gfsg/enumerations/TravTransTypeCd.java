package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations;

public enum TravTransTypeCd {
    Ticket("TKT"),
    Exchange_Ticket("EXC"),
    Refund("REF"),
    Miscellaneous("MSC");

    private final String travTransTypeCd;

    private TravTransTypeCd(String travTransTypeCd) {
        this.travTransTypeCd = travTransTypeCd;
    }

    public String getTravTransTypeCd() {
        return this.travTransTypeCd;
    }
}
