package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.pos;

public enum PinCapture {
    POC_NO_PIN_CAPTURE_CAPABILITY("0"),
    POC_UNKNOWN("1"),
    RESERVE_FOR_ISO_USE_2("2"),
    RESERVE_FOR_ISO_USE_3("3"),
    POC_FOUR_CHARS("4"),
    POC_FIVE_CHARS("5"),
    POC_SIX_CHARS("6"),
    POC_SEVEN_CHARS("7"),
    POC_EIGHT_CHARS("8"),
    POC_NINE_CHARS("9"),
    POC_TEN_CHARS("A"),
    POC_ELEVEN_CHARS("B"),
    POC_TWELVE_CHARS("C"),
    RESERVE_FOR_ISO_USE_D("D"),
    RESERVE_FOR_ISO_USE_E("E"),
    RESERVE_FOR_ISO_USE_F("F"),
    RESERVE_FOR_ISO_USE_G("G"),
    RESERVE_FOR_ISO_USE_H("H"),
    RESERVE_FOR_ISO_USE_I("I"),
    RESERVE_FOR_NATIONAL_USE_J("J"),
    RESERVE_FOR_NATIONAL_USE_K("K"),
    RESERVE_FOR_NATIONAL_USE_L("L"),
    RESERVE_FOR_NATIONAL_USE_M("M"),
    RESERVE_FOR_NATIONAL_USE_N("N"),
    RESERVE_FOR_NATIONAL_USE_O("O"),
    RESERVE_FOR_NATIONAL_USE_P("P"),
    RESERVE_FOR_NATIONAL_USE_Q("Q"),
    RESERVE_FOR_NATIONAL_USE_R("R"),
    RESERVE_FOR_PRIVATE_USE_S("S"),
    RESERVE_FOR_PRIVATE_USE_T("T"),
    RESERVE_FOR_PRIVATE_USE_U("U"),
    RESERVE_FOR_PRIVATE_USE_V("V"),
    RESERVE_FOR_PRIVATE_USE_W("W"),
    RESERVE_FOR_PRIVATE_USE_X("X"),
    RESERVE_FOR_PRIVATE_USE_Y("Y"),
    RESERVE_FOR_PRIVATE_USE_Z("Z");

    private final String pinCapture;

    private PinCapture(String pinCapture) {
        this.pinCapture = pinCapture;
    }

    public String getPinCapture() {
        return this.pinCapture;
    }
}
