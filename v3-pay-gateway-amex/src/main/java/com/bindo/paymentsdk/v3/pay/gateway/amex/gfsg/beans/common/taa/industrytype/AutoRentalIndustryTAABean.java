package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.IndustryTypeTAABean;

public class AutoRentalIndustryTAABean
  extends IndustryTypeTAABean
{
  private String autoRentalAgreementNumber;
  private String autoRentalPickupLocation;
  private String autoRentalPickupCityName;
  private String autoRentalPickupRegionCode;
  private String autoRentalPickupCountryCode;
  private String autoRentalPickupDate;
  private String autoRentalPickupTime;
  private String autoRentalReturnCityName;
  private String autoRentalReturnRegionCode;
  private String autoRentalReturnCountryCode;
  private String autoRentalReturnDate;
  private String autoRentalReturnTime;
  private String autoRentalRenterName;
  private String autoRentalVehicleClassId;
  private String autoRentalDistance;
  private String autoRentalDistanceUnitOfMeasure;
  private String autoRentalAuditAdjustmentIndicator;
  private String autoRentalAuditAdjustmentAmount;
  private String autoreserved;
  private String returnDropoffLocation;
  private String vehicleIdentificationNumber;
  private String driverIdentificationNumber;
  private String driverTaxNumber;
  
  public String getReturnDropoffLocation()
  {
    return this.returnDropoffLocation;
  }
  
  public void setReturnDropoffLocation(String returnDropoffLocation)
  {
    this.returnDropoffLocation = returnDropoffLocation;
  }
  
  public String getVehicleIdentificationNumber()
  {
    return this.vehicleIdentificationNumber;
  }
  
  public void setVehicleIdentificationNumber(String vehicleIdentificationNumber)
  {
    this.vehicleIdentificationNumber = vehicleIdentificationNumber;
  }
  
  public String getDriverIdentificationNumber()
  {
    return this.driverIdentificationNumber;
  }
  
  public void setDriverIdentificationNumber(String driverIdentificationNumber)
  {
    this.driverIdentificationNumber = driverIdentificationNumber;
  }
  
  public String getDriverTaxNumber()
  {
    return this.driverTaxNumber;
  }
  
  public void setDriverTaxNumber(String driverTaxNumber)
  {
    this.driverTaxNumber = driverTaxNumber;
  }
  
  public String getAutoRentalAgreementNumber()
  {
    return this.autoRentalAgreementNumber;
  }
  
  public void setAutoRentalAgreementNumber(String autoRentalAgreementNumber)
  {
    this.autoRentalAgreementNumber = autoRentalAgreementNumber;
  }
  
  public String getAutoRentalPickupLocation()
  {
    return this.autoRentalPickupLocation;
  }
  
  public void setAutoRentalPickupLocation(String autoRentalPickupLocation)
  {
    this.autoRentalPickupLocation = autoRentalPickupLocation;
  }
  
  public String getAutoRentalPickupCityName()
  {
    return this.autoRentalPickupCityName;
  }
  
  public void setAutoRentalPickupCityName(String autoRentalPickupCityName)
  {
    this.autoRentalPickupCityName = autoRentalPickupCityName;
  }
  
  public String getAutoRentalPickupRegionCode()
  {
    return this.autoRentalPickupRegionCode;
  }
  
  public void setAutoRentalPickupRegionCode(String autoRentalPickupRegionCode)
  {
    this.autoRentalPickupRegionCode = autoRentalPickupRegionCode;
  }
  
  public String getAutoRentalPickupCountryCode()
  {
    return this.autoRentalPickupCountryCode;
  }
  
  public void setAutoRentalPickupCountryCode(String autoRentalPickupCountryCode)
  {
    this.autoRentalPickupCountryCode = autoRentalPickupCountryCode;
  }
  
  public String getAutoRentalPickupDate()
  {
    return this.autoRentalPickupDate;
  }
  
  public void setAutoRentalPickupDate(String autoRentalPickupDate)
  {
    this.autoRentalPickupDate = autoRentalPickupDate;
  }
  
  public String getAutoRentalPickupTime()
  {
    return this.autoRentalPickupTime;
  }
  
  public void setAutoRentalPickupTime(String autoRentalPickupTime)
  {
    this.autoRentalPickupTime = autoRentalPickupTime;
  }
  
  public String getAutoRentalReturnCityName()
  {
    return this.autoRentalReturnCityName;
  }
  
  public void setAutoRentalReturnCityName(String autoRentalReturnCityName)
  {
    this.autoRentalReturnCityName = autoRentalReturnCityName;
  }
  
  public String getAutoRentalReturnRegionCode()
  {
    return this.autoRentalReturnRegionCode;
  }
  
  public void setAutoRentalReturnRegionCode(String autoRentalReturnRegionCode)
  {
    this.autoRentalReturnRegionCode = autoRentalReturnRegionCode;
  }
  
  public String getAutoRentalReturnCountryCode()
  {
    return this.autoRentalReturnCountryCode;
  }
  
  public void setAutoRentalReturnCountryCode(String autoRentalReturnCountryCode)
  {
    this.autoRentalReturnCountryCode = autoRentalReturnCountryCode;
  }
  
  public String getAutoRentalReturnDate()
  {
    return this.autoRentalReturnDate;
  }
  
  public void setAutoRentalReturnDate(String autoRentalReturnDate)
  {
    this.autoRentalReturnDate = autoRentalReturnDate;
  }
  
  public String getAutoRentalReturnTime()
  {
    return this.autoRentalReturnTime;
  }
  
  public void setAutoRentalReturnTime(String autoRentalReturnTime)
  {
    this.autoRentalReturnTime = autoRentalReturnTime;
  }
  
  public String getAutoRentalRenterName()
  {
    return this.autoRentalRenterName;
  }
  
  public void setAutoRentalRenterName(String autoRentalRenterName)
  {
    this.autoRentalRenterName = autoRentalRenterName;
  }
  
  public String getAutoRentalVehicleClassId()
  {
    return this.autoRentalVehicleClassId;
  }
  
  public void setAutoRentalVehicleClassId(String autoRentalVehicleClassId)
  {
    this.autoRentalVehicleClassId = autoRentalVehicleClassId;
  }
  
  public String getAutoRentalDistance()
  {
    return this.autoRentalDistance;
  }
  
  public void setAutoRentalDistance(String autoRentalDistance)
  {
    this.autoRentalDistance = autoRentalDistance;
  }
  
  public String getAutoRentalDistanceUnitOfMeasure()
  {
    return this.autoRentalDistanceUnitOfMeasure;
  }
  
  public void setAutoRentalDistanceUnitOfMeasure(String autoRentalDistanceUnitOfMeasure)
  {
    this.autoRentalDistanceUnitOfMeasure = autoRentalDistanceUnitOfMeasure;
  }
  
  public String getAutoRentalAuditAdjustmentIndicator()
  {
    return this.autoRentalAuditAdjustmentIndicator;
  }
  
  public void setAutoRentalAuditAdjustmentIndicator(String autoRentalAuditAdjustmentIndicator)
  {
    this.autoRentalAuditAdjustmentIndicator = autoRentalAuditAdjustmentIndicator;
  }
  
  public String getAutoRentalAuditAdjustmentAmount()
  {
    return this.autoRentalAuditAdjustmentAmount;
  }
  
  public void setAutoRentalAuditAdjustmentAmount(String autoRentalAuditAdjustmentAmount)
  {
    this.autoRentalAuditAdjustmentAmount = autoRentalAuditAdjustmentAmount;
  }
  
  public String getAutoreserved()
  {
    return this.autoreserved;
  }
  
  public void setAutoreserved(String autoreserved)
  {
    this.autoreserved = autoreserved;
  }
}
