package com.bindo.paymentsdk.v3.pay.gateway.amex.gateway;

import com.bindo.paymentsdk.v3.pay.common.CardDetectMode;
import com.bindo.paymentsdk.v3.pay.common.ReversalType;
import com.bindo.paymentsdk.v3.pay.common.TransactionType;
import com.bindo.paymentsdk.v3.pay.common.emv.enums.EMVTag;
import com.bindo.paymentsdk.v3.pay.common.emv.enums.FlowType;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Request;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Response;
import com.bindo.paymentsdk.v3.pay.common.util.CurrencyUtil;
import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.common.util.StringUtils;
import com.bindo.paymentsdk.v3.pay.gateway.Gateway;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.AuthorizationService;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.connection.PropertyReader;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.Constants;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.ActionCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.CountryCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.MCCCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.MessageReasonCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.MessageTypeID;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.ProcessingCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.pos.CardCapture;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.pos.CardDataInput;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.pos.CardDataInputMode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.pos.CardDataOutput;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.pos.CardMemberAuthenticationEntity;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.pos.CardMemberAuthenticationMethod;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.pos.CardPresent;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.pos.CardholderAuthentication;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.pos.CardholderPresent;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.pos.OperatingEnvironment;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.pos.PinCapture;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.pos.TerminalOutput;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean.AuthorizationRequestBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean.AuthorizationResponseBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean.ErrorObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean.IntegratedCircuitCardRelatedDataBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean.OriginalDataElementsBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean.POSDataCodeBean;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * AMERICAN EXPRESS GLOBAL CREDIT AUTHORIZATION GUIDE
 * ISO 8583:1993 (VERSION 1)
 * OCTOBER 2017
 */
public class AmexGateway implements Gateway {

    private final String TAG = "AmExGateway";

    // CONFIGURATION KEYS
    private static final String CONFIG_URL               = "config_url";
    private static final String CONFIG_PORT              = "config_port";
    private static final String CONFIG_ORIGIN            = "config_origin";
    private static final String CONFIG_REGION            = "config_region";
    private static final String CONFIG_MESSAGE_TYPE      = "config_message_type";
    private static final String CONFIG_ROUTING_INDICATOR = "config_routing_indicator";
    private static final String CONFIG_HTTPS_TIMEOUT     = "config_https_timeout";
    private static final String CONFIG_READ_TIMEOUT      = "config_read_timeout";
    private static final String CONFIG_PROXY             = "config_proxy";
    private static final String CONFIG_MERCHANT_ID       = "config_merchant_id";
    private static final String CONFIG_TERMINAL_ID       = "config_terminal_id";
    private static final String CONFIG_MERCHANT_COUNTRY  = "config_merchant_country_code";
    private static final String CONFIG_MCC               = "config_mcc";

    // DEFAULT CONFIGURATION
    private static final String DEFAULT_CONFIG_URL               = "https://qwww318.americanexpress.com/IPPayments/inter/CardAuthorization.do";
    private static final String DEFAULT_CONFIG_PORT              = "443";
    private static final String DEFAULT_CONFIG_ORIGIN            = "BINDO";
    private static final String DEFAULT_CONFIG_REGION            = "JAPA";
    private static final String DEFAULT_CONFIG_MESSAGE_TYPE      = "ISO GCAG";
    private static final String DEFAULT_CONFIG_ROUTING_INDICATOR = "050";
    private static final String DEFAULT_CONFIG_HTTPS_TIMEOUT     = "10000";
    private static final String DEFAULT_CONFIG_READ_TIMEOUT      = "10000";
    private static final String DEFAULT_CONFIG_PROXY             = "NO";
    private static final String DEFAULT_CONFIG_MERCHANT_ID       = "0512860628";
    //private static final String DEFAULT_CONFIG_MERCHANT_ID       = "7273525535";
    private static final String DEFAULT_CONFIG_TERMINAL_ID       = "001";
    private static final String DEFAULT_CONFIG_MERCHANT_COUNTRY  = CountryCode.HongKong.getCountryCode();
    private static final String DEFAULT_CONFIG_MCC               = MCCCode.Computer_Software_Stores.getMccCode();

    private static final SimpleDateFormat DATE_FORMAT_TRANSX_DT = new SimpleDateFormat("yyMMddhhmmss");
    private static final SimpleDateFormat DATE_FORMAT_STAN      = new SimpleDateFormat("HHmmss");
    private static final SimpleDateFormat DATE_FORMAT_TRANSM_DT = new SimpleDateFormat("MMddHHmmss");

    private PropertyReader propertyReader;

    private static final EMVTag[] TAGS = new EMVTag[] {
            EMVTag.APPLICATION_CRYPTOGRAM,
            EMVTag.ISSUER_APPLICATION_DATA,
            EMVTag.UNPREDICTABLE_NUMBER,
            EMVTag.APPLICATION_TRANSACTION_COUNTER,
            EMVTag.TERMINAL_VERIFICATION_RESULTS,
            EMVTag.TRANSACTION_DATE,
            EMVTag.TRANSACTION_TYPE,
            EMVTag.AMOUNT_AUTHORISED_NUMERIC,
            EMVTag.TRANSACTION_CURRENCY_CODE,
            EMVTag.TERMINAL_COUNTRY_CODE,
            EMVTag.APPLICATION_INTERCHANGE_PROFILE,
            EMVTag.AMOUNT_OTHER_NUMERIC,
            EMVTag.APPLICATION_PRIMARY_ACCOUNT_NUMBER_SEQUENCE_NUMBER,
            EMVTag.CRYPTOGRAM_INFORMATION_DATA
    };

    private HashMap<String, Object> mDefaultGatewayConfig = new HashMap<>();

    public HashMap<String, Object> getDefaultGatewayConfig() {
        return mDefaultGatewayConfig;
    }

    public void setDefaultGatewayConfig(HashMap<String, Object> defaultGatewayConfig) {
        this.mDefaultGatewayConfig = defaultGatewayConfig;
    }

    public AmexGateway(HashMap<String, Object> configMap) {
        mDefaultGatewayConfig.put(CONFIG_HTTPS_TIMEOUT, DEFAULT_CONFIG_HTTPS_TIMEOUT);
        mDefaultGatewayConfig.put(CONFIG_MCC, DEFAULT_CONFIG_MCC);
        mDefaultGatewayConfig.put(CONFIG_MERCHANT_COUNTRY, DEFAULT_CONFIG_MERCHANT_COUNTRY);
        mDefaultGatewayConfig.put(CONFIG_MERCHANT_ID, DEFAULT_CONFIG_MERCHANT_ID);
        mDefaultGatewayConfig.put(CONFIG_MESSAGE_TYPE, DEFAULT_CONFIG_MESSAGE_TYPE);
        mDefaultGatewayConfig.put(CONFIG_ORIGIN, DEFAULT_CONFIG_ORIGIN);
        mDefaultGatewayConfig.put(CONFIG_PORT, DEFAULT_CONFIG_PORT);
        mDefaultGatewayConfig.put(CONFIG_PROXY, DEFAULT_CONFIG_PROXY);
        mDefaultGatewayConfig.put(CONFIG_READ_TIMEOUT, DEFAULT_CONFIG_READ_TIMEOUT);
        mDefaultGatewayConfig.put(CONFIG_REGION, DEFAULT_CONFIG_REGION);
        mDefaultGatewayConfig.put(CONFIG_ROUTING_INDICATOR, DEFAULT_CONFIG_ROUTING_INDICATOR);
        mDefaultGatewayConfig.put(CONFIG_TERMINAL_ID, DEFAULT_CONFIG_TERMINAL_ID);
        mDefaultGatewayConfig.put(CONFIG_URL, DEFAULT_CONFIG_URL);

        if (configMap == null) {
            Log.w(TAG, "Please set the gateway parameters");
        } else {
            for (String key : configMap.keySet()) {
                Object value = configMap.get(key);
                mDefaultGatewayConfig.put(key, value);
            }
        }

        this.propertyReader = configureGateway();
    }

    private PropertyReader configureGateway() {
        PropertyReader pr = new PropertyReader();
        pr.setHTTPS_URL                     ((String)mDefaultGatewayConfig.get(CONFIG_URL));
        pr.Port = Integer.parseInt          ((String)mDefaultGatewayConfig.get(CONFIG_PORT));
        PropertyReader.setORIGIN            ((String)mDefaultGatewayConfig.get(CONFIG_ORIGIN));
        PropertyReader.setREGION            ((String)mDefaultGatewayConfig.get(CONFIG_REGION));
        PropertyReader.setMERCHANT_NUMBER   ((String)mDefaultGatewayConfig.get(CONFIG_MERCHANT_ID));
        PropertyReader.setMERCHANT_COUNTRY  ((String)mDefaultGatewayConfig.get(CONFIG_MERCHANT_COUNTRY));
        PropertyReader.setMESSAGE_TYPE      ((String)mDefaultGatewayConfig.get(CONFIG_MESSAGE_TYPE));
        PropertyReader.setROUTING_INDICATOR ((String)mDefaultGatewayConfig.get(CONFIG_ROUTING_INDICATOR));
        PropertyReader.setHttpsTimeout      ((String)mDefaultGatewayConfig.get(CONFIG_HTTPS_TIMEOUT));
        PropertyReader.setReadTimeout       ((String)mDefaultGatewayConfig.get(CONFIG_READ_TIMEOUT));
        PropertyReader.setPROXY             ((String)mDefaultGatewayConfig.get(CONFIG_PROXY));
        PropertyReader.setMCC               ((String)mDefaultGatewayConfig.get(CONFIG_MCC));
        PropertyReader.setTerminalNumber    ((String)mDefaultGatewayConfig.get(CONFIG_TERMINAL_ID));
        return pr;
    }

    @Override
    public Response sendRequest(Request request) {
        List<Response.Error> errors = new ArrayList<>();
        Response response = createResponse(request);

        // approve refunds offline
        if (request.getTransactionType() == TransactionType.REFUND) {
            response.setActionCode("0000");
            response.setIsApproved(true);
            response.setApprovalCode("000000");
            byte[] flowTypeByteArray = request.getTagLengthValues() != null ? request.getTagLengthValues().get(EMVTag.CUSTOM_FLOW_TYPE.toString()) : null;
            byte flowType = (flowTypeByteArray != null && flowTypeByteArray.length >= 1) ? flowTypeByteArray[0] : -1;
            response.setPosDataCode(getPosDataCode(request, flowType).populatePointOfServiceDataCode());
            response.setTransactionId(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()).substring(2));
            return response;
        }

        try {
            AuthorizationRequestBean authorizationRequestBean = convertRequestToISO8583(request);
            response.setPosDataCode(authorizationRequestBean.getPosDataCode());

            Log.d(TAG, "REQUEST: " + request.toString());
            Log.d(TAG, "REQUESTBEAN: " + authorizationRequestBean.toString());

            AuthorizationService authorizationService = new AuthorizationService();

            validateRequest(authorizationService, authorizationRequestBean);
            AuthorizationResponseBean responseBean = authorizationService.createAndSendAuthorizationRequest(authorizationRequestBean, propertyReader);
            response = convertISO8583ToResponse(response, responseBean);

            Log.d(TAG, "RESPONSEBEAN: " + responseBean.toString());
            Log.d(TAG, "RESPONSE: " + response.toString());

            return response;
        } catch (Exception e) {
            Throwable cause = e.getCause();

            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            Log.e(TAG, sw.toString());

            String errorCode;
            String errorMessage = sw.toString();

            if (cause instanceof UnknownHostException) {
                errorCode = Response.Error.ERROR_CODE_UNKNOWN_HOST;
            } else if (cause instanceof SocketTimeoutException) {
                errorCode = Response.Error.ERROR_CODE_TIMEOUT;
            } else {
                errorCode = Response.Error.ERROR_CODE_UNKNOWN;
            }

            errors.add(new Response.Error(errorCode, errorMessage));
//            response.setActionCode(errorCode);
            response.setErrors(errors);
        }
        return response;
    }

    private Response createResponse(Request request) {
        Response response = new Response();
        response.setTransactionType(request.getTransactionType().name());
        response.setTransactionProcessingCode(request.getProcessingCode());
        response.setTransactionCurrencyCode(request.getTransactionCurrencyCode());
        response.setTransactionSystemTraceAuditNumber(request.getTransactionSystemTraceAuditNumber());
        response.setTransactionAmount(request.getTransactionAmount());
        response.setTransactionDate(request.getTransactionDate());
        response.setTransactionTime(request.getTransactionTime());
        response.setTransactionDateAndTime(request.getTransactionDateAndTime());
        return response;
    }

    private Response convertISO8583ToResponse(Response response, AuthorizationResponseBean authorizationResponseBean) {
        String actionCode = authorizationResponseBean.getActionCode();
        if (actionCode != null) {
            boolean isApproved;
            switch (ActionCode.getByValue(authorizationResponseBean.getActionCode())) {
                case APPROVED:
                case APPROVE_WITH_ID:
                case PARTIAL_APPROVAL:
                    isApproved = true;
                    break;
                case REVERSAL_ACCEPTED:
                    isApproved = TransactionType.REVERSAL.name().equals(response.getTransactionType());
                    break;
                default:
                    isApproved = false;
            }
            response.setIsApproved(isApproved);
            response.setActionCode(actionCode);
            response.setActionCodeDescription(ActionCode.getByValue(actionCode).getActionCodeDescription());
            response.setIsNeedCallIssuerConfirm(ActionCode.PLEASE_CALL_ISSUER.getActionCode().equals(actionCode));
        }
        response.setApprovalCode(authorizationResponseBean.getApprovalCode());
        response.setRetrievalReferenceNumber(authorizationResponseBean.getRetrievalReferenceNumber());
        response.setTransactionId(authorizationResponseBean.getTransactionId());

        IntegratedCircuitCardRelatedDataBean iccRelatedDataBean = authorizationResponseBean.getIccRelatedDataBean();
        if (iccRelatedDataBean != null) {
            response.setIccRelatedData(authorizationResponseBean.getIccRelatedData());
            response.setIccRelatedDataIssuerAuthenticationData(iccRelatedDataBean.getIssuerAuthenticationData());
            response.setIccRelatedDataIssuerScriptData(iccRelatedDataBean.getIssuerScriptData());
            response.setIccAuthorizationResponseCode(iccRelatedDataBean.getAppResponseCode());
            response.setIccAuthorizationResponseCryptogram(iccRelatedDataBean.getAppResponseCrypt());
        }
        return response;
    }

    private void validateRequest(AuthorizationService authorizationService, AuthorizationRequestBean authorizationRequestBean) throws Exception {
        AuthorizationResponseBean authorizationResponseBean = authorizationService.validateAuthorizationRequest(authorizationRequestBean);
        if (authorizationResponseBean.getAuthErrorList() != null && !authorizationResponseBean.getAuthErrorList().isEmpty()) {
            Log.e(TAG, "ERRORS IN REQUEST");
            for (ErrorObject errorObject : authorizationResponseBean.getAuthErrorList()) {
                Log.e(TAG, "ERROR" + errorObject.getErrorCode() + " " + errorObject.getErrorDescription());
            }
            throw new Exception("ERRORS IN REQUEST");
        }
    }

    private AuthorizationRequestBean convertRequestToISO8583(Request request) throws Exception {

        AuthorizationRequestBean authorizationRequestBean = new AuthorizationRequestBean();
        byte[] flowTypeByteArray = request.getTagLengthValues() != null ? request.getTagLengthValues().get(EMVTag.CUSTOM_FLOW_TYPE.toString()) : null;
        byte flowType = (flowTypeByteArray != null && flowTypeByteArray.length >= 1) ? flowTypeByteArray[0] : -1;

        // DF0  - MESSAGE TYPE IDENTIFIER
        MessageTypeID messageTypeID;
        String messageReasonCode;
        ProcessingCode processingCode;
        switch (request.getTransactionType()) {
            case AUTHORIZATION:
            case PRE_AUTHORIZATION:
            case SALE:
                messageTypeID = MessageTypeID.ISO_AuthorizationRequest;
                messageReasonCode = MessageReasonCode.value1.getMessageReasonCode();
                processingCode = ProcessingCode.CARD_AUTHORIZATION_REQUEST;
                break;
            case REVERSAL:
                messageTypeID = MessageTypeID.ISO_ReversalAdviceRequest;
                messageReasonCode = MessageReasonCode.value2.getMessageReasonCode();
                if (request.getReversalType() == null) {
                    request.setReversalType(ReversalType.MERCHANT_INITIATED);
                }
                switch (request.getReversalType()) {
                    case MERCHANT_INITIATED:
                        processingCode = ProcessingCode.MERCHANT_INITIATED_REVERSAL;
                        break;
                    case SYSTEM_GENERATED:
                        processingCode = ProcessingCode.CARD_REVERSAL_ADVICE_SYSTEM_GENERATED_REVERSAL;
                        break;
                    default:
                        throw new Exception("UNSUPPORTED REVERSAL TRANSACTION TYPE");
                }
                break;
            default:
                throw new Exception("UNSUPPORTED TRANSACTION TYPE");
        }
        authorizationRequestBean.setMessageTypeIdentifier(messageTypeID.getMessageTypeID());

        // DF2  - PRIMARY ACCOUNT NUMBER (PAN)
        authorizationRequestBean.setPrimaryAccountNumber(new StringBuffer(request.getPrimaryAccountNumber()));

        // DF3  - PROCESSING CODE
        authorizationRequestBean.setProcessingCode(processingCode.getProcessingCode());

        // DF4  - AMOUNT, TRANSACTION
        authorizationRequestBean.setAmountTransaction(CurrencyUtil.formatAmount(request.getTransactionAmount(), request.getTransactionCurrencyCode()));

        // DF11 - SYSTEMS TRACE AUDIT NUMBER
        if (request.getTransactionSystemTraceAuditNumber() == 0) {
            authorizationRequestBean.setSystemTraceAuditNumber(DATE_FORMAT_STAN.format(new Date()));
        } else {
            authorizationRequestBean.setSystemTraceAuditNumber(String.format("%06d", request.getTransactionSystemTraceAuditNumber()));
        }

        // DF12 - DATE AND TIME, LOCAL TRANSACTION
        if (request.getTransactionDateAndTime() != null) {
            authorizationRequestBean.setLocalTransactionDateAndTime(request.getTransactionDateAndTime());
        } else {
            authorizationRequestBean.setLocalTransactionDateAndTime(DATE_FORMAT_TRANSX_DT.format(new Date()));
        }

        // DF14 - DATE, EXPIRATION
        String expirationDate = null;
        if (request.getCardExpirationDate() != null) {
            expirationDate = request.getCardExpirationDate();
        } else if (request.getTrack1DiscretionaryData() != null) {
            String[] track1Data = request.getTrack1DiscretionaryData().split("^");
            if (track1Data.length > 2) {
                expirationDate = track1Data[1].substring(0, 4);
            }

        } else if (request.getTrack2EquivalentData() != null) {
            String[] track2Data = request.getTrack2EquivalentData().split("=");
            if (track2Data.length == 1) {
                track2Data = request.getTrack2EquivalentData().split("D");
            }
            if (track2Data.length > 1) {
                expirationDate = track2Data[1].substring(0, 4);
            }
        }
        authorizationRequestBean.setCardExpirationDate(expirationDate);

        // DF19 - COUNTRY CODE, ACQUIRING INSTITUTION
        authorizationRequestBean.setMerchantLocationCountryCode(PropertyReader.getMERCHANT_COUNTRY());

        // DF22 - POINT OF SERVICE DATA CODE
        authorizationRequestBean.setPosDataCode(getPosDataCode(request, flowType).populatePointOfServiceDataCode());

        // DF25 - MESSAGE REASON CODE
        authorizationRequestBean.setMessageReasonCode(messageReasonCode);

        // DF26 - CARD ACCEPTOR BUSINESS CODE
        authorizationRequestBean.setCardAcceptorBusinessCode(PropertyReader.getMCC());
        request.setMerchantCategoryCode(authorizationRequestBean.getCardAcceptorBusinessCode());

        // DF31 - ACQUIRER REFERENCE DATA
        authorizationRequestBean.setTransactionId(request.getAcquirerReferenceData());

        // DF32 - ACQUIRING INSTITUTION IDENTIFICATION CODE
        authorizationRequestBean.setAcquiringInstitutionIdCode(request.getAcquiringIIC());

        switch (request.getTransactionType()) {
            case AUTHORIZATION:
            case PRE_AUTHORIZATION:
            case SALE:

                // DF7 - DATE AND TIME, TRANSMISSION
                authorizationRequestBean.setTransmissionDateAndTime(DATE_FORMAT_TRANSM_DT.format(new Date()));

                // DF35 - TRACK 2 DATA
                // DF45 - TRACK 1 DATA
                String track1 = "";
                String track2 = "";
                switch (request.getCardDetectMode()) {
                    case CardDetectMode.CONTACT:
                    case CardDetectMode.SWIPE:
                    case CardDetectMode.FALLBACK_SWIPE:
                        track1 = request.getTrack1DiscretionaryData();
                        track2 = request.getTrack2EquivalentData();
                        break;
                    case CardDetectMode.CONTACTLESS:
                        track1 = request.getTrack1DiscretionaryData();
                        track2 = request.getTLVHexString(EMVTag.TRACK_2_EQUIVALENT_DATA);

                        switch (flowType) {
                            case FlowType.AMEX_MAGSRTIPE:        // Landi A8 AMEX ExpressPay Card Magstripe Mode
                            case FlowType.AMEX_MOBILE_MAGSTRIPE: // Landi A8 AMEX ExpressPay Mobile Magstripe Mode
                                // RF Magstripe Mode
                                // Construct pseudo-magstripe according to
                                //  EMV® ContactlessSpecifications for Payment Systems
                                //  Book C-4
                                //  Kernel 4 Specification
                                //  Version 2.6
                                //  February 2016
                                String applicationCryptogram = request.getTLVHexString(EMVTag.APPLICATION_CRYPTOGRAM);
                                applicationCryptogram = String.format("%05d", Integer.parseInt(applicationCryptogram.substring(applicationCryptogram.length() - 6), 16));
                                applicationCryptogram = applicationCryptogram.substring(applicationCryptogram.length() - 5);

                                String applicationTransactionCounter = request.getTLVHexString(EMVTag.APPLICATION_TRANSACTION_COUNTER);
                                applicationTransactionCounter = String.format("%05d", Integer.parseInt(applicationTransactionCounter, 16));
                                applicationTransactionCounter = applicationTransactionCounter.substring(applicationTransactionCounter.length() - 5);

                                String unpredictableNumber = request.getTLVHexString(EMVTag.UNPREDICTABLE_NUMBER);
                                unpredictableNumber = unpredictableNumber.substring(unpredictableNumber.length() - 4);

                                int x = track2.indexOf('D');
                                if (x == -1) {
                                    x = track2.indexOf('=');
                                }
                                track2 = track2.substring(0, (x + 8)) + unpredictableNumber + applicationCryptogram + applicationTransactionCounter;
                                break;
                        }
                        break;
                }
                authorizationRequestBean.setTrack1Data(track1);
                if (!StringUtils.isEmpty(track2) && track2.charAt(track2.length() - 1) == 'F') {
                    track2 = track2.substring(0, track2.length() - 1);
                }
                authorizationRequestBean.setTrack2Data(track2);
                break;
            case REVERSAL:
                break;
        }

        // DF41 - CARD ACCEPTOR TERMINAL IDENTIFICATION
        authorizationRequestBean.setCardAcceptorTerminalId(String.format("%8s", PropertyReader.getTerminalNumber()));
        request.setTerminalIdentification(authorizationRequestBean.getCardAcceptorTerminalId());

        // DF42 - CARD ACCEPTOR IDENTIFICATION CODE
        authorizationRequestBean.setCardAcceptorIdCode(PropertyReader.getMERCHANT_NUMBER());
        request.setMerchantIdentifier(authorizationRequestBean.getCardAcceptorIdCode());

        // DF49 - CURRENCY CODE, TRANSACTION
        authorizationRequestBean.setTransactionCurrencyCode(request.getTransactionCurrencyCode());

        // DF52 - PERSONAL IDENTIFICATION NUMBER (PIN) DATA
        authorizationRequestBean.setPinData(request.getPINData());

        //DF53 - SECURITY RELATED CONTROL INFORMATION
        authorizationRequestBean.setCardIdentifierCode(request.getCardSecurityCode());

        //DF55 - INTEGRATED CIRCUIT CARD SYSTEM RELATED DATA
        switch (request.getTransactionType()) {
            case AUTHORIZATION:
            case PRE_AUTHORIZATION:
            case SALE:
                switch (request.getCardDetectMode()) {
                    case CardDetectMode.CONTACT:
                    case CardDetectMode.CONTACTLESS:
                        switch (flowType) {
                            case FlowType.AMEX_MAGSRTIPE:        // Landi A8 AMEX ExpressPay Card Magstripe Mode
                            case FlowType.AMEX_MOBILE_MAGSTRIPE: // Landi A8 AMEX ExpressPay Mobile Magstripe Mode
                                break;
                            default:
                                if (request.getTagLengthValues() != null) {
                                    StringBuilder icc = new StringBuilder();
                                    icc.append(Constants.HEADER_VERSION_NAME);
                                    icc.append(Constants.HEADER_VERSION_NUMBER);
                                    for (EMVTag tag : TAGS) {
                                        String V = request.getTLVHexString(tag);
                                        if (tag.equals(EMVTag.ISSUER_APPLICATION_DATA)) {
                                            String L = String.format("%02X", V.length() / 2);
                                            icc.append(L);
                                        }
                                        icc.append(V);
                                    }
                                    authorizationRequestBean.setIccRelatedData(icc.toString());
                                }
                        }
                }
        }

        // DF56 - ORIGINAL DATA ELEMENTS
        if (request.getTransactionType() == TransactionType.REVERSAL) {
            OriginalDataElementsBean original = new OriginalDataElementsBean();

            // SF1 - MESSAGE TYPE IDENTIFIER
            original.setMessageTypeIdOriginal(MessageTypeID.ISO_AuthorizationRequest.getMessageTypeID());
            // SF2 - SYSTEM TRACE AUDIT NUMBER
            if (request.getOriginalTransactionSystemTraceAuditNumber() != 0) {
                original.setSystemTraceAuditNumberOriginal(String.format("%06d", request.getOriginalTransactionSystemTraceAuditNumber()));
            } else {
                original.setSystemTraceAuditNumberOriginal(Constants.FIELD_SEPARATOR);
            }
            // SF3 - DATE AND TIME, LOCAL TRANSACTION
            if (request.getOriginalTransactionDateAndTime() != null) {
                original.setLocalTransactionDateAndTimeOriginal(request.getOriginalTransactionDateAndTime());
            } else {
                original.setLocalTransactionDateAndTimeOriginal(Constants.FIELD_SEPARATOR);
            }
            // SF4 - ACQUIRING INSTITUTION IDENTIFICATION CODE
            if (request.getAcquiringIIC() != null) {
                original.setAcquiringInstIdOriginal(request.getAcquiringIIC());
            } else {
                original.setAcquiringInstIdOriginal(Constants.FIELD_SEPARATOR);
            }
            authorizationRequestBean.setOriginalDataElementsBean(original);
        }

        /*
        // DF63 - PRIVATE USE DATA
        PrivateUseData2Bean pud2 = new PrivateUseData2Bean();
        int pud2Length = 0;
        pud2.setServiceIdentifier(SERVICE_IDENTIFIER_AX);
        pud2Length += SERVICE_IDENTIFIER_AX.length();
        pud2.setRequestTypeIdentifier(REQUEST_TYPE_IDENTIFIER_AE);
        pud2Length += REQUEST_TYPE_IDENTIFIER_AE.length();
        pud2.setVli(String.format("%03d", pud2Length));
        authorizationRequestBean.setPrivateUseData2Bean(pud2);
        */

        return authorizationRequestBean;
    }

    private POSDataCodeBean getPosDataCode(Request request, byte flowType) {
        POSDataCodeBean posDataCodeBean = new POSDataCodeBean();

        // Position 1
        // Card Data Input Capability — This subfield indicates the maximum capability of the device
        // used to originate this transaction
        CardDataInput cardDataInput;
        cardDataInput = CardDataInput.POC_INTEGERATED_CIRCUIT_CARD;
        posDataCodeBean.setCardDataInput(cardDataInput.getCardDataInput());

        // Position 2
        // Cardholder Authentication Capability — This subfield indicates the primary means used to
        // verify the Cardmember’s identity at this terminal.
        CardholderAuthentication cardholderAuthentication;
        if (request.getPINData() != null) {
            cardholderAuthentication = CardholderAuthentication.POC_PIN;
        } else {
            cardholderAuthentication = CardholderAuthentication.POC_UNKNOWN;
        }
        posDataCodeBean.setCardholderAuthentication(cardholderAuthentication.getCardholderAuthentication());

        // Position 3
        // Card Capture Capability — This subfield indicates if the terminal is capable of capturing
        // card data.
        CardCapture cardCapture;
        cardCapture = CardCapture.POC_CAPTURE;
        posDataCodeBean.setCardCapture(cardCapture.getCardCapture());

        // Position 4
        // Operating Environment — This subfield indicates the terminal’s location, and if it is attended
        // by the card acceptor.
        OperatingEnvironment operatingEnvironment;
        operatingEnvironment = OperatingEnvironment.POC_ON_PREMISES_OF_CARD_ACCEPTOR_ATTENDED;
        posDataCodeBean.setOperatingEnvironment(operatingEnvironment.getOperatingEnvironment());

        // Position 5
        // Cardholder Present — This subfield indicates if the Cardmember is present at the point of
        // service; and if not, the reason why.
        CardholderPresent cardholderPresent;
        cardholderPresent = CardholderPresent.POC_CARDMEMBER_PRESENT;
        posDataCodeBean.setCardholderPresent(cardholderPresent.getCardholderPresent());

        // Position 6
        // Card Present — This subfield indicates if the card is present at the point of service.
        CardPresent cardPresent;
        switch (request.getCardDetectMode()) {
            case CardDetectMode.CONTACTLESS:
                cardPresent = CardPresent.POC_CONTACTLESS_TRANSACTION;
                break;
            default:
                cardPresent = CardPresent.POC_CARD_PRESENT;
        }
        posDataCodeBean.setCardPresent(cardPresent.getCardPresent());

        // Position 7
        // Card Data Input Mode — This subfield indicates the method used to capture information
        // from the card.
        CardDataInputMode cardDataInputMode;
        switch (request.getCardDetectMode()) {
            case CardDetectMode.CONTACT:
            case CardDetectMode.CONTACTLESS:
                cardDataInputMode = CardDataInputMode.POC_INTEGRATED_CKT_CARD;
                break;
            case CardDetectMode.SWIPE:
                cardDataInputMode = CardDataInputMode.POC_MAGNETIC_STRIPE_READ;
                break;
            case CardDetectMode.FALLBACK_SWIPE:
                cardDataInputMode = CardDataInputMode.POC_TECHNICAL_FALLBACK;
                break;
            case CardDetectMode.MANUAL:
                cardDataInputMode = CardDataInputMode.POC_KEY_ENTERED;
                break;
            default:
                cardDataInputMode = CardDataInputMode.POC_UNKNOWN;
        }
        if (request.getCardSecurityCode() != null) {
            if (request.getTrack2EquivalentData() != null) {
                cardDataInputMode = CardDataInputMode.POC_SWIPED;
            } else {
                cardDataInputMode = CardDataInputMode.POC_MANUALLY_ENTERED;
            }
        }
        switch (flowType) {
            case FlowType.AMEX_MAGSRTIPE:        // Landi A8 AMEX ExpressPay Card Magstripe Mode
            case FlowType.AMEX_MOBILE_MAGSTRIPE: // Landi A8 AMEX ExpressPay Mobile Magstripe Mode
                cardDataInputMode = CardDataInputMode.POC_MAGNETIC_STRIPE_READ;
                break;
        }
        posDataCodeBean.setCardDataInputMode(cardDataInputMode.getCardDataInputMode());

        // Position 8
        // Cardmember Authentication Method — This subfield indicates the method for verifying the
        // Cardmember identity.
        CardMemberAuthenticationMethod cardMemberAuthenticationMethod;
        cardMemberAuthenticationMethod = CardMemberAuthenticationMethod.POC_UNKNOWN;
        switch (request.getCardDetectMode()) {
            case CardDetectMode.CONTACT:
            case CardDetectMode.CONTACTLESS:
                byte[] terminalVerificationResults = request.getTagLengthValues().get(EMVTag.TERMINAL_VERIFICATION_RESULTS.toString());
                byte[] cardholderVerificationMethodResults = request.getTagLengthValues().get(EMVTag.CARDHOLDER_VERIFICATION_METHOD_RESULTS.toString());

                if (cardholderVerificationMethodResults != null && cardholderVerificationMethodResults.length > 0) {
                    switch ((cardholderVerificationMethodResults[0] & 0x3f)) {
                        case 0x01:  // PLAINTEXT_PIN
                        case 0x02:  // ONLINE_ENCIPHERED_PIN
                        case 0x03:  // PAINTEXT_PIN_AND_SIGNATURE
                        case 0x04:  // OFFLINE_ENCIPHERED_PIN
                        case 0x05:  // OFFLINE_ENCIPHERED_PIN_AND_SIGNATURE
                            if (terminalVerificationResults[2] == 0x00) {
                                cardMemberAuthenticationMethod = CardMemberAuthenticationMethod.POC_PINL;
                            }
                            break;
                        case 0x1e:  // SIGNATURE
                            cardMemberAuthenticationMethod = CardMemberAuthenticationMethod.POC_MANUAL_SIGN_VERIFICATION;
                            break;
                    }
                }
                break;
            default:
                cardMemberAuthenticationMethod = CardMemberAuthenticationMethod.POC_MANUAL_SIGN_VERIFICATION;
                break;
        }
        posDataCodeBean.setCardMemberAuthenticationMethod(cardMemberAuthenticationMethod.getCardMemberAuthenticationMethod());

        // Position 9
        // Cardmember Authentication Entity — This subfield indicates component or person who
        // verified Cardmember identity reported in Cardmember Authentication (Position 8).
        CardMemberAuthenticationEntity cardMemberAuthenticationEntity;
        cardMemberAuthenticationEntity = CardMemberAuthenticationEntity.POC_INTEGRATED_CKT_CARD;
        posDataCodeBean.setCardMemberAuthenticationEntity(cardMemberAuthenticationEntity.getCardMemberAuthenticationEntity());


        // Position 10
        // Card Data Output Capability — This subfield indicates the ability of the terminal to update
        // the card.
        CardDataOutput cardDataOutput;
        cardDataOutput = CardDataOutput.INTEGRATED_CIRCUIT_CARD;
        posDataCodeBean.setCardDataOutput(cardDataOutput.getCardDataOutput());

        // Position 11
        // Terminal Output Capability — This subfield indicates the ability of the terminal to print
        // and/or display messages.
        TerminalOutput terminalOutput;
        terminalOutput = TerminalOutput.POC_PRINTING_AND_DISPLAY;
        posDataCodeBean.setTerminalOutput(terminalOutput.getTerminalOutput());

        // Position 12
        // PIN Capture Capability — This subfield indicates the PIN length that the terminal is capable
        // of capturing.
        PinCapture pinCapture;
        pinCapture = PinCapture.POC_TWELVE_CHARS;
        posDataCodeBean.setPinCapture(pinCapture.getPinCapture());

        return posDataCodeBean;
    }
}