package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.SubmissionErrorCodes;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.XMLSubmissionErrorCodes;

public class ErrorObject
{
  private String errorCode;
  private String errorDescription;
  
  public ErrorObject(String errorCode, String errorDescription)
  {
    this.errorCode = errorCode;
    this.errorDescription = errorDescription;
  }
  
  public ErrorObject(SubmissionErrorCodes code)
  {
    this(code.getErrorCode(), code.getErrorDescription());
  }
  
  public ErrorObject(XMLSubmissionErrorCodes code)
  {
    this(code.getErrorCode(), code.getErrorDescription());
  }
  
  public String getErrorCode()
  {
    return this.errorCode;
  }
  
  public String getErrorDescription()
  {
    return this.errorDescription;
  }
}
