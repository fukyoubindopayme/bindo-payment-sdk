package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.validator;

import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.connection.PropertyReader;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.utils.ISOUtil;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator
{
  protected static Validator objValidator;
  static boolean debugFlag = PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON");
  
  public static Validator getSingleton()
  {
    if (objValidator == null) {
      getInstance();
    }
    return objValidator;
  }
  
  private static synchronized Validator getInstance()
  {
    if (objValidator == null) {
      try
      {
        objValidator = new Validator();
      }
      catch (Exception e)
      {
        return null;
      }
    }
    return objValidator;
  }
  
  public static boolean validateValue(String value, int bitNumber, int maxLength, int minLength, boolean validateLength, boolean validateData, String regEx)
  {
    if (debugFlag) {
      Log.i("Entered into validateValue() to validate bit number :: " + (64 - bitNumber));
    }
    boolean flag = true;
    if (validateLength)
    {
      flag = validateLength(value, maxLength, minLength);
      if (!flag) {
        return flag;
      }
    }
    if (validateData) {
      flag = validateData(value, regEx);
    }
    if (debugFlag) {
      Log.i("Validation conditions met successfully for bitNumber:" + (64 - bitNumber) + ":" + flag);
    }
    return flag;
  }
  
  public static boolean validateLength(String value, int maxLength, int minLength)
  {
    boolean flag = true;
    if ((value.length() > maxLength) || (value.length() < minLength)) {
      flag = false;
    }
    return flag;
  }
  
  public static boolean validateData(String value, String validChars)
  {
    boolean flag = true;
    flag = validateCharacters(value, validChars);
    return flag;
  }
  
  private static boolean validateCharacters(String value, String expression)
  {
    boolean isValid = false;
    CharSequence inputStr = value;
    
    Pattern pattern = Pattern.compile(expression);
    
    Matcher matcher = pattern.matcher(inputStr);
    if (matcher.matches()) {
      isValid = true;
    }
    return isValid;
  }
  
  public static boolean validateCardNumber(String cardNumber)
  {
    boolean isValid = true;
    if ((cardNumber != null) && (!cardNumber.equalsIgnoreCase("")))
    {
      CreditCardValidator objCreditCardValidator = new CreditCardValidator();
      isValid = objCreditCardValidator.isValidCardNumber(cardNumber);
    }
    return isValid;
  }
  
  public static boolean validateModulusTen(String cardNumber)
  {
    boolean isValid = true;
    if ((cardNumber != null) && (!cardNumber.equalsIgnoreCase("")))
    {
      CreditCardValidator objCreditCardValidator = new CreditCardValidator();
      isValid = objCreditCardValidator.modulusTenCheck(cardNumber);
    }
    return isValid;
  }
  
  public static boolean validateSENumber(String SENumber)
  {
    boolean isValid = true;
    if ((SENumber != null) && (!SENumber.equalsIgnoreCase(""))) {
      isValid = SENumberValidator.modulusNineCheck(SENumber);
    }
    return isValid;
  }
  
  public static boolean validateSubFields(String value, int maxLength, int minLength, String regEx)
  {
    boolean isValid = true;
    
    isValid = validateLength(value, maxLength, minLength);
    if (!isValid) {
      return isValid;
    }
    isValid = validateCharacters(value, regEx);
    if (!isValid) {
      return isValid;
    }
    return isValid;
  }
  
  public static boolean isValidPOSData(String pOSvalue)
  {
    boolean flag = true;
    if (ISOUtil.IsNullOrEmpty(pOSvalue)) {
      flag = false;
    } else if ((pOSvalue.length() > 12) || (pOSvalue.length() < 12)) {
      flag = false;
    } else if (!validateData(pOSvalue, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR[22])) {
      flag = false;
    }
    return flag;
  }
}
