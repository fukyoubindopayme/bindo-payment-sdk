package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.processor.iso;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.ErrorObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.SettlementRequestDataObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.SettlementResponseDataObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.acknowledgement.AcknowledgementReportObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.confirmation.ConfirmationReportObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.connection.PropertyReader;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.exceptions.SettlementException;
import java.io.File;
import java.util.List;

public class RequestProcessorFactory
{
  public static SettlementResponseDataObject processRequest(SettlementRequestDataObject requestDataObject, PropertyReader propertyReader)
    throws SettlementException
  {
    SettlementResponseDataObject responseObject = null;
    FTPRequestProcessor ftpRequest = new FTPRequestProcessor();
    responseObject = ftpRequest.processRequest(requestDataObject, propertyReader);
    return responseObject;
  }
  
  public static List<File> fetchConfirmationReportInPrintedFormat(PropertyReader propertyReader, List<ErrorObject> connErrorList)
    throws SettlementException
  {
    List<File> file = null;
    
    FTPRequestProcessor ftpRequest = new FTPRequestProcessor();
    file = ftpRequest.fetchConfirmationReportInPrintedFormat(
      propertyReader);
    
    return file;
  }
  
  public static List<AcknowledgementReportObject> fetchAcknowledgementInPrintedFormat(PropertyReader propertyReader, List<ErrorObject> connErrorList)
    throws SettlementException
  {
    List<AcknowledgementReportObject> ackReptList = null;
    FTPRequestProcessor ftpRequest = new FTPRequestProcessor();
    
    ackReptList = ftpRequest.fetchAcknowledgementInPrintedFormat(propertyReader, 
      connErrorList);
    return ackReptList;
  }
  
  public static List<ConfirmationReportObject> fetchConfirmationReportInRawData(PropertyReader propertyReader, List<ErrorObject> connErrorList)
    throws SettlementException
  {
    List<ConfirmationReportObject> confReptList = null;
    
    FTPRequestProcessor ftpRequest = new FTPRequestProcessor();
    confReptList = ftpRequest.fetchConfirmationReportInRawData(
      propertyReader);
    
    return confReptList;
  }
}
