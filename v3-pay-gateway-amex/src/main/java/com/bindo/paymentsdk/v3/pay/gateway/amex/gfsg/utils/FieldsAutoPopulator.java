package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.utils;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.SettlementRequestDataObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceAddendumBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceBasicBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceDetailBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionBatchTrailerBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionFileHeaderBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionFileSummaryBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionTBTSpecificBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.LocationDetailTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.connection.PropertyReader;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.ProcessingCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.CommonValidator;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

public class FieldsAutoPopulator
{
  private static final Logger LOGGER = Logger.getLogger(FieldsAutoPopulator.class);
  
  public static void updateTFSRecord(SettlementRequestDataObject settlementReqObject)
  {
    int numberOfDebits = 0;int numberOfcredits = 0;
    long hashTotalDebitAmount = 0L;long hashTotalCreditAmount = 0L;long hashTotalAmount = 0L;
    label350:
    for (TransactionTBTSpecificBean transactionTBTSpecifictType : settlementReqObject.getTransactionTBTSpecificType()) {
      if (!transactionTBTSpecifictType.getTransactionAdviceBasicType().isEmpty()) {
        for (TransactionAdviceBasicBean transactionAdviceBasicType : transactionTBTSpecifictType.getTransactionAdviceBasicType())
        {
          if ((transactionAdviceBasicType.getProcessingCode().equals(ProcessingCode.DEBIT.getProcessingCode())) || (transactionAdviceBasicType.getProcessingCode().equals(ProcessingCode.CREDIT_REVERSALS.getProcessingCode())) || 
            (transactionAdviceBasicType.getProcessingCode().equals(ProcessingCode.BONUS_PAY_DEBIT.getProcessingCode())) || (transactionAdviceBasicType.getProcessingCode().equals(ProcessingCode.BONUS_PAY_CREDIT_REVERSAL.getProcessingCode())))
          {
            numberOfDebits++;
            if (!CommonValidator.isNullOrEmpty(transactionAdviceBasicType.getTransactionAmount()))
            {
              try
              {
                hashTotalDebitAmount += Long.parseLong(transactionAdviceBasicType.getTransactionAmount());
              }
              catch (NumberFormatException nfe)
              {
                if (!PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
                  break label350;
                }
                LOGGER.fatal("NumberFormatException in updateTFSRecord method of class FieldsAutoPopulator:" + nfe);
              }
            }
          }
          else if ((transactionAdviceBasicType.getProcessingCode().equals(ProcessingCode.CREDIT.getProcessingCode())) || (transactionAdviceBasicType.getProcessingCode().equals(ProcessingCode.DEBIT_REVERSALS.getProcessingCode())) || 
            (transactionAdviceBasicType.getProcessingCode().equals(ProcessingCode.BONUS_PAY_CREDIT.getProcessingCode())) || (transactionAdviceBasicType.getProcessingCode().equals(ProcessingCode.BONUS_PAY_DEBIT_REVERSAL.getProcessingCode())))
          {
            numberOfcredits++;
            if (!CommonValidator.isNullOrEmpty(transactionAdviceBasicType.getTransactionAmount())) {
              try
              {
                hashTotalCreditAmount += Long.parseLong(transactionAdviceBasicType.getTransactionAmount());
              }
              catch (NumberFormatException nfe)
              {
                if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
                  LOGGER.fatal("NumberFormatException in updateTFSRecord method of class FieldsAutoPopulator:" + nfe);
                }
              }
            }
          }
          if (!CommonValidator.isNullOrEmpty(transactionAdviceBasicType.getTransactionAmount())) {
            try
            {
              hashTotalAmount += Long.parseLong(transactionAdviceBasicType.getTransactionAmount());
            }
            catch (NumberFormatException nfe)
            {
              if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
                LOGGER.fatal("NumberFormatException in updateTFSRecord method of class FieldsAutoPopulator:" + nfe);
              }
            }
          }
        }
      }
    }
    if (settlementReqObject != null)
    {
      TransactionFileSummaryBean tfsBean = settlementReqObject.getTransactionFileSummaryType();
      tfsBean.setNumberOfDebits(String.valueOf(numberOfDebits));
      tfsBean.setNumberOfCredits(String.valueOf(numberOfcredits));
      tfsBean.setHashTotalCreditAmount(String.valueOf(hashTotalCreditAmount));
      tfsBean.setHashTotalDebitAmount(String.valueOf(hashTotalDebitAmount));
      tfsBean.setHashTotalAmount(String.valueOf(hashTotalAmount));
      settlementReqObject.setTransactionFileSummaryType(tfsBean);
    }
  }
  
  public static void updateRecordNumber(SettlementRequestDataObject settlementRequestBean)
  {
    TransactionFileHeaderBean tfh = settlementRequestBean
      .getTransactionFileHeaderType();
    String recordNumber = "1";
    String nextRecordNumber = getNextRecordNumber(recordNumber);
    if (CommonValidator.isNullOrEmpty(tfh.getRecordNumber())) {
      tfh.setRecordNumber(padString(recordNumber, 8, "0", false, true));
    }
    List<TransactionTBTSpecificBean> tbtSpecificBeanList = settlementRequestBean.getTransactionTBTSpecificType();
    if ((tbtSpecificBeanList != null) && (!tbtSpecificBeanList.isEmpty())) {
      for (TransactionTBTSpecificBean tbtSpecificBean : tbtSpecificBeanList)
      {
        List<TransactionAdviceBasicBean> tabBeanList = tbtSpecificBean
          .getTransactionAdviceBasicType();
        int totalNoOfTABs = 0;
        if ((tabBeanList != null) && (!tabBeanList.isEmpty()))
        {
          totalNoOfTABs = tabBeanList.size();
          for (TransactionAdviceBasicBean tabBean : tabBeanList)
          {
            tabBean.setRecordNumber(nextRecordNumber);
            nextRecordNumber = getNextRecordNumber(nextRecordNumber);
            TransactionAdviceDetailBean tadBean = tabBean
              .getTransactionAdviceDetailBean();
            if (tadBean != null)
            {
              tadBean.setRecordNumber(nextRecordNumber);
              nextRecordNumber = getNextRecordNumber(nextRecordNumber);
            }
            ArrayList<TransactionAdviceAddendumBean> taaBeanList = tabBean.getArrayTAABean();
            if (taaBeanList != null) {
              for (TransactionAdviceAddendumBean taaBean : taaBeanList) {
                if (taaBean != null)
                {
                  taaBean.setRecordNumber(nextRecordNumber);
                  nextRecordNumber = getNextRecordNumber(nextRecordNumber);
                }
              }
            }
            LocationDetailTAABean locationTAA = tabBean
              .getLocationDetailTAABean();
            if (locationTAA != null)
            {
              locationTAA.setRecordNumber(nextRecordNumber);
              nextRecordNumber = getNextRecordNumber(nextRecordNumber);
            }
          }
        }
        TransactionBatchTrailerBean tbtBean = tbtSpecificBean
          .getTransactionBatchTrailerBean();
        if (tbtBean != null)
        {
          tbtBean.setTotalNoOfTabs(String.valueOf(totalNoOfTABs));
          tbtBean.setRecordNumber(nextRecordNumber);
          nextRecordNumber = getNextRecordNumber(nextRecordNumber);
        }
      }
    }
    TransactionFileSummaryBean tfs = settlementRequestBean.getTransactionFileSummaryType();
    if (tfs != null) {
      tfs.setRecordNumber(nextRecordNumber);
    }
  }
  
  private static String getNextRecordNumber(String recordNumber)
  {
    int currentRecordNo = Integer.parseInt(recordNumber);
    currentRecordNo++;String nextRecordNo = padString(String.valueOf(currentRecordNo), 8, 
      "0", false, true);
    return nextRecordNo;
  }
  
  private static String padString(String value, int maxLength, String padWith, boolean isLeftJustify, boolean isRightJustify)
  {
    String paddedValue = value;
    if (paddedValue == null) {
      paddedValue = "";
    }
    int length = paddedValue.length();
    StringBuffer newValue = new StringBuffer();
    for (; length < maxLength; length++) {
      newValue = newValue.append(padWith);
    }
    if (isLeftJustify) {
      paddedValue = value + newValue;
    } else if (isRightJustify) {
      paddedValue = newValue + paddedValue;
    }
    return paddedValue;
  }
}
