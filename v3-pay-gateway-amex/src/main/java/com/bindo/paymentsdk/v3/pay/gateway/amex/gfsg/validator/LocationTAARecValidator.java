package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.ErrorObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceBasicBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.RecordType;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.SubmissionErrorCodes;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.exceptions.SettlementException;
import java.util.List;

public class LocationTAARecValidator
{
  private static String recordNumber;
  private static String recordType;
  
  public static void validateLocationRecordType(TransactionAdviceBasicBean transactionAdviceBasicBean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    recordNumber = 
      transactionAdviceBasicBean.getLocationDetailTAABean().getRecordNumber();
    recordType = transactionAdviceBasicBean.getLocationDetailTAABean()
      .getRecordType();
    validateRecordType(transactionAdviceBasicBean
      .getLocationDetailTAABean().getRecordType(), errorCodes);
    validateRecordNumber(transactionAdviceBasicBean
      .getLocationDetailTAABean().getRecordNumber(), errorCodes);
    validateTransactionIdentifier(transactionAdviceBasicBean, errorCodes);
    validateAddendaTypeCode(transactionAdviceBasicBean
      .getLocationDetailTAABean().getAddendaTypeCode(), errorCodes);
    validateLocationName(transactionAdviceBasicBean
      .getLocationDetailTAABean().getLocationName(), errorCodes);
    validatelocationAddress(transactionAdviceBasicBean
      .getLocationDetailTAABean().getLocationAddress(), errorCodes);
    validateLocationCity(transactionAdviceBasicBean
      .getLocationDetailTAABean().getLocationCity(), errorCodes);
    validateLocationRegion(transactionAdviceBasicBean
      .getLocationDetailTAABean().getLocationRegion(), errorCodes);
    validateLocationCountryCode(transactionAdviceBasicBean
      .getLocationDetailTAABean().getLocationCountryCode(), 
      errorCodes);
    validateLocationPostalCode(transactionAdviceBasicBean
      .getLocationDetailTAABean().getLocationPostalCode(), errorCodes);
    validateMerchantCategoryCode(transactionAdviceBasicBean
      .getLocationDetailTAABean().getMerchantCategoryCode(), 
      errorCodes);
    validateSellerId(transactionAdviceBasicBean.getLocationDetailTAABean()
      .getSellerId(), errorCodes);
  }
  
  private static void validateRecordType(String recType, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(recType))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE
        .getErrorDescription() + 
        "\n" + 
        "TFH" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "RecordType:" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (!RecordType.Transaction_Advice_Addendum.getRecordType().equalsIgnoreCase(recType))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateRecordNumber(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD585_RECORDNUMBER
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD585_RECORDNUMBER
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "RecordNumber:" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[54]))
    {
      String reqLength = CommonValidator.validateLength(value, 8, 8);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD3_RECORDNUMBER
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD3_RECORDNUMBER
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "RecordNumber:" + 
          "|" + 
          "This field length Cannot be greater than 8");
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD2_RECORDNUMBER
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD2_RECORDNUMBER
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "RecordNumber:" + 
        "|" + 
        "This field can only be Numeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateTransactionIdentifier(TransactionAdviceBasicBean transactionAdviceBasicBean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    String transIdLOC = transactionAdviceBasicBean
      .getLocationDetailTAABean().getTransactionIdentifier();
    if (CommonValidator.isNullOrEmpty(transIdLOC))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "TransactionIdentifier" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(transIdLOC, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[55]))
    {
      String reqLength = CommonValidator.validateLength(transIdLOC, 
        15, 15);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "TransactionIdentifier" + 
          "|" + 
          "This field length Cannot be greater than 15");
        errorCodes.add(errorObj);
      }
      else if (!transIdLOC.equalsIgnoreCase(transactionAdviceBasicBean.getTransactionIdentifier()))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD67_TRANSACTION_IDENTIFIER
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD67_TRANSACTION_IDENTIFIER
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "TransactionIdentifier" + 
          "|" + 
          SubmissionErrorCodes.ERROR_DATAFIELD67_TRANSACTION_IDENTIFIER
          .getErrorDescription());
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD25_TRANSACTION_IDENTIFIER
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "TransactionIdentifier" + 
        "|" + 
        "This field can only be Numeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateAddendaTypeCode(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD68_ADDENDA_TYPECODE
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD68_ADDENDA_TYPECODE
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "AddendaTypeCode" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[57]))
    {
      String reqLength = CommonValidator.validateLength(value, 2, 2);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD68_ADDENDA_TYPECODE
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD68_ADDENDA_TYPECODE
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "AddendaTypeCode" + 
          "|" + 
          "This field length Cannot be greater than 2");
        errorCodes.add(errorObj);
      }
      else if (!CommonValidator.isValidAddendaTypeCode(value))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD70_ADDENDA_TYPECODE
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD70_ADDENDA_TYPECODE
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "AddendaTypeCode" + 
          "|" + 
          SubmissionErrorCodes.ERROR_DATAFIELD70_ADDENDA_TYPECODE
          .getErrorDescription());
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD68_ADDENDA_TYPECODE
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD68_ADDENDA_TYPECODE
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "AddendaTypeCode" + 
        "|" + 
        "This field can only be AlphaNumeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateLocationName(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD61_LOCATION_NAME
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD61_LOCATION_NAME
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "LocationName" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else
    {
      String reqLength = 
        CommonValidator.validateLength(value, 38, 38);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD61_LOCATION_NAME
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD61_LOCATION_NAME
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "LocationName" + 
          "|" + 
          "This field length Cannot be greater than 38");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validatelocationAddress(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD62_LOCATION_ADDRESS
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD62_LOCATION_ADDRESS
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "LocationAddress" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else
    {
      String reqLength = 
        CommonValidator.validateLength(value, 38, 38);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD62_LOCATION_ADDRESS
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD62_LOCATION_ADDRESS
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "LocationAddress" + 
          "|" + 
          "This field length Cannot be greater than 38");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateLocationCity(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD63_LOCATION_CITY
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD63_LOCATION_CITY
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "LocationCity" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else
    {
      String reqLength = 
        CommonValidator.validateLength(value, 21, 21);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD63_LOCATION_CITY
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD63_LOCATION_CITY
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "LocationCity" + 
          "|" + 
          "This field length Cannot be greater than 21");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateLocationRegion(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD64_LOCATION_REGION
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD64_LOCATION_REGION
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "LocationRegion" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[61]))
    {
      String reqLength = CommonValidator.validateLength(value, 3, 3);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD64_LOCATION_REGION
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD64_LOCATION_REGION
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "LocationRegion" + 
          "|" + 
          "This field length Cannot be greater than 3");
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD64_LOCATION_REGION
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD64_LOCATION_REGION
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "LocationRegion" + 
        "|" + 
        "This field can only be AlphaNumeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateLocationCountryCode(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD65_LOCATIONCNTRY_CODE
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD65_LOCATIONCNTRY_CODE
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "LocationCountryCode" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[62]))
    {
      String reqLength = CommonValidator.validateLength(value, 3, 3);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD65_LOCATIONCNTRY_CODE
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD65_LOCATIONCNTRY_CODE
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "LocationCountryCode" + 
          "|" + 
          "This field length Cannot be greater than 3");
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD65_LOCATIONCNTRY_CODE
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD65_LOCATIONCNTRY_CODE
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "LocationCountryCode" + 
        "|" + 
        "This field can only be AlphaNumeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateLocationPostalCode(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD66_LOCATIONPOSTAL_CODE
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD66_LOCATIONPOSTAL_CODE
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "LocationPostalCode" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else
    {
      String reqLength = 
        CommonValidator.validateLength(value, 15, 15);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD66_LOCATIONPOSTAL_CODE
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD66_LOCATIONPOSTAL_CODE
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "LocationPostalCode" + 
          "|" + 
          "This field length Cannot be greater than 15");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateMerchantCategoryCode(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD623_MRCHNT_CATGRY_CODE
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD623_MRCHNT_CATGRY_CODE
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "MerchantCategoryCode" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[64]))
    {
      String reqLength = CommonValidator.validateLength(value, 4, 4);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD623_MRCHNT_CATGRY_CODE
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD623_MRCHNT_CATGRY_CODE
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "MerchantCategoryCode" + 
          "|" + 
          "This field length Cannot be greater than 4");
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD623_MRCHNT_CATGRY_CODE
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD623_MRCHNT_CATGRY_CODE
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        recordType + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "MerchantCategoryCode" + 
        "|" + 
        "This field can only be Numeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateSellerId(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 20, 20);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD600_SELLERID
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD600_SELLERID
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          recordType + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "SellerId" + 
          "|" + 
          "This field length Cannot be greater than 20");
        errorCodes.add(errorObj);
      }
    }
  }
}
