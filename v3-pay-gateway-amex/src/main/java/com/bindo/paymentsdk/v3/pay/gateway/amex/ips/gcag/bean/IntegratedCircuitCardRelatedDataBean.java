package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean;

import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.connection.PropertyReader;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.Constants;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.AuthErrorCodes;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.utils.ISOUtil;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.validator.Validator;
import java.util.List;

public class IntegratedCircuitCardRelatedDataBean
{
  private String vli;
  private String icc_header_version_name;
  private String icc_header_version_number;
  private String app_cryptogram;
  private String issuer_app_data;
  private String unpredictable_number;
  private String app_transaction_counter;
  private String terminal_verification_results;
  private String transaction_date;
  private String transaction_type;
  private String amount_authorized;
  private String transaction_currency_code;
  private String terminal_country_code;
  private String app_interchange_profile;
  private String amount_other;
  private String app_pan_sequence_number;
  private String cryptogram_information_data;
  private String reserved_future_use;
  private String iccData;
  private String appResponseCrypt;
  private String appResponseCode;
  private String issuerScriptData;
  private String issuerAuthenticationData;

  public String getVli()
  {
    return this.vli;
  }
  
  public void setVli(String vli)
  {
    this.vli = vli;
  }
  
  public String getIccHeaderVersionName()
  {
    return this.icc_header_version_name;
  }
  
  public void setIccHeaderVersionName(String icc_header_version_name)
  {
    this.icc_header_version_name = icc_header_version_name;
  }
  
  public String getIccHeaderVersionNumber()
  {
    return this.icc_header_version_number;
  }
  
  public void setIccHeaderVersionNumber(String icc_header_version_number)
  {
    this.icc_header_version_number = icc_header_version_number;
  }
  
  public String getAppCryptogram()
  {
    return this.app_cryptogram;
  }
  
  public void setAppCryptogram(String app_cryptogram)
  {
    this.app_cryptogram = app_cryptogram;
  }
  
  public String getIssuerAppData()
  {
    return this.issuer_app_data;
  }
  
  public void setIssuerAppData(String issuer_app_data)
  {
    this.issuer_app_data = issuer_app_data;
  }
  
  public String getUnpredictableNumber()
  {
    return this.unpredictable_number;
  }
  
  public void setUnpredictableNumber(String unpredictable_number)
  {
    this.unpredictable_number = unpredictable_number;
  }
  
  public String getAppTransactionCounter()
  {
    return this.app_transaction_counter;
  }
  
  public void setAppTransactionCounter(String app_transaction_counter)
  {
    this.app_transaction_counter = app_transaction_counter;
  }
  
  public String getTerminalVerificationResults()
  {
    return this.terminal_verification_results;
  }
  
  public void setTerminalVerificationResults(String terminal_verification_results)
  {
    this.terminal_verification_results = terminal_verification_results;
  }
  
  public String getTransactionDate()
  {
    return this.transaction_date;
  }
  
  public void setTransactionDate(String transaction_date)
  {
    this.transaction_date = transaction_date;
  }
  
  public String getTransactionType()
  {
    return this.transaction_type;
  }
  
  public void setTransactionType(String transaction_type)
  {
    this.transaction_type = transaction_type;
  }
  
  public String getAmountAuthorized()
  {
    return this.amount_authorized;
  }
  
  public void setAmountAuthorized(String amount_authorized)
  {
    this.amount_authorized = amount_authorized;
  }
  
  public String getTransactionCurrencyCode()
  {
    return this.transaction_currency_code;
  }
  
  public void setTransactionCurrencyCode(String transaction_currency_code)
  {
    this.transaction_currency_code = transaction_currency_code;
  }
  
  public String getTerminalCountryCode()
  {
    return this.terminal_country_code;
  }
  
  public void setTerminalCountryCode(String terminal_country_code)
  {
    this.terminal_country_code = terminal_country_code;
  }
  
  public String getAppInterchangeProfile()
  {
    return this.app_interchange_profile;
  }
  
  public void setAppInterchangeProfile(String app_interchange_profile)
  {
    this.app_interchange_profile = app_interchange_profile;
  }
  
  public String getAmountOther()
  {
    return this.amount_other;
  }
  
  public void setAmountOther(String amount_other)
  {
    this.amount_other = amount_other;
  }
  
  public String getAppPANSequenceNumber()
  {
    return this.app_pan_sequence_number;
  }
  
  public void setAppPANSequenceNumber(String app_pan_sequence_number)
  {
    this.app_pan_sequence_number = app_pan_sequence_number;
  }
  
  public String getCryptogramInformationData()
  {
    return this.cryptogram_information_data;
  }
  
  public void setCryptogramInformationData(String cryptogram_information_data)
  {
    this.cryptogram_information_data = cryptogram_information_data;
  }
  
  public String getReservedFutureUse()
  {
    return this.reserved_future_use;
  }
  
  public void setReservedFutureUse(String reserved_future_use)
  {
    this.reserved_future_use = reserved_future_use;
  }
  
  public String getIccData()
  {
    return this.iccData;
  }
  
  public void setIccData(String iccData)
  {
    this.iccData = iccData;
  }
  
  public String getAppResponseCrypt()
  {
    return this.appResponseCrypt;
  }
  
  public void setAppResponseCrypt(String appResponseCrypt)
  {
    this.appResponseCrypt = appResponseCrypt;
  }
  
  public String getAppResponseCode()
  {
    return this.appResponseCode;
  }
  
  public void setAppResponseCode(String appResponseCode)
  {
    this.appResponseCode = appResponseCode;
  }
  
  public String getIssuerScriptData()
  {
    return this.issuerScriptData;
  }
  
  public void setIssuerScriptData(String issuerScriptData)
  {
    this.issuerScriptData = issuerScriptData;
  }
  
  public String getIssuerAuthenticationData()
  {
    return this.issuerAuthenticationData;
  }
  
  public void setIssuerAuthenticationData(String issuerAuthenticationData)
  {
    this.issuerAuthenticationData = issuerAuthenticationData;
  }
  
  public String populateIntegratedCircuitCardRelatedData(AuthorizationResponseBean authorizationResponseBean, String currencyCode)
  {
    StringBuffer iccData = new StringBuffer();
    
    StringBuffer displayBuffer = new StringBuffer();
    if (ISOUtil.IsNullOrEmpty(getIccHeaderVersionName()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD551_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if ((Validator.validateSubFields(getIccHeaderVersionName(), 4, 4, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1])) && (getIccHeaderVersionName().equalsIgnoreCase("AGNS")))
    {
      iccData.append(getIccHeaderVersionName());
      displayBuffer.append("ICC_HEADER_VERSION_NAME     ").append(" :: ").append(getIccHeaderVersionName()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD551_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getIccHeaderVersionNumber()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD552_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if ((Validator.validateData(getIccHeaderVersionNumber(), com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[0])) && (getIccHeaderVersionNumber().equalsIgnoreCase("0001")))
    {
      iccData.append(getIccHeaderVersionNumber());
      displayBuffer.append("ICC_HEADER_VERSION_NUMBER     ").append(" :: ").append(getIccHeaderVersionNumber()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD552_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getAppCryptogram()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD553_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if (Validator.validateSubFields(getAppCryptogram(), 16, 16, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1]))
    {
      iccData.append(getAppCryptogram());
      displayBuffer.append("APP_CRYPTOGRAM                ").append(" :: ").append(getAppCryptogram()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD553_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getIssuerAppData()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD554_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if ((Validator.validateData(getIssuerAppData(), com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1])) && (getIssuerAppData().length() <= 66))
    {
      iccData.append(getIssuerAppData());
      displayBuffer.append("ISSUER_APP_DATA               ").append(" :: ").append(getIssuerAppData()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD554_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getUnpredictableNumber()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD555_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if (Validator.validateSubFields(getUnpredictableNumber(), 8, 8, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1]))
    {
      iccData.append(getUnpredictableNumber());
      displayBuffer.append("ICC_UNPREDICTATBLE_NUM        ").append(" :: ").append(getUnpredictableNumber()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD555_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getAppTransactionCounter()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD556_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if (Validator.validateSubFields(getAppTransactionCounter(), 4, 4, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1]))
    {
      iccData.append(getAppTransactionCounter());
      displayBuffer.append("APP_TRANSACTION_COUNTER       ").append(" :: ").append(getAppTransactionCounter()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD556_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getTerminalVerificationResults()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD557_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if (Validator.validateSubFields(getTerminalVerificationResults(), 10, 10, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1]))
    {
      iccData.append(getTerminalVerificationResults());
      displayBuffer.append("TERMINAL_VERIFY_RESULTS       ").append(" :: ").append(getTerminalVerificationResults()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD557_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getTransactionDate()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD558_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if (Validator.validateData(getTransactionDate(), com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[0]))
    {
      iccData.append(getTransactionDate());
      displayBuffer.append("TRANSACTION_DATE              ").append(" :: ").append(getTransactionDate()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD558_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getTransactionType()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD559_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if ((Validator.validateData(getTransactionType(), com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[0])) && (getTransactionType().equalsIgnoreCase("00")))
    {
      iccData.append(getTransactionType());
      displayBuffer.append("TRANSACTION_TYPE              ").append(" :: ").append(getTransactionType()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD559_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getAmountAuthorized()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD5510_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if (Validator.validateSubFields(getAmountAuthorized(), 12, 12, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[0]))
    {
      iccData.append(getAmountAuthorized());
      displayBuffer.append("AMOUNT_AUTHORIZED             ").append(" :: ").append(getAmountAuthorized()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD5510_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getTransactionCurrencyCode()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD5511_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if ((Validator.validateSubFields(getTransactionCurrencyCode(), 4, 4, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[0])) && (getTransactionCurrencyCode().substring(1).equalsIgnoreCase(currencyCode)))
    {
      iccData.append(getTransactionCurrencyCode());
      displayBuffer.append("TRANSACTION_CURRENCY_CODE    ").append(" :: ").append(getTransactionCurrencyCode()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD5511_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getTerminalCountryCode()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD5512_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if (Validator.validateSubFields(getTerminalCountryCode(), 4, 4, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[0]))
    {
      iccData.append(getTerminalCountryCode());
      displayBuffer.append("TERMINAL_COUNTRY_CODE         ").append(" :: ").append(getTerminalCountryCode()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD5512_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getAppInterchangeProfile()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD5513_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if (Validator.validateSubFields(getAppInterchangeProfile(), 4, 4, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1]))
    {
      iccData.append(getAppInterchangeProfile());
      displayBuffer.append("APP_INTERCHANGE_PROFILE       ").append(" :: ").append(getAppInterchangeProfile()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD5513_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getAmountOther()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD5514_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if (Validator.validateSubFields(getAmountOther(), 12, 12, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[0]))
    {
      iccData.append(getAmountOther());
      displayBuffer.append("AMOUNT_OTHER                  ").append(" :: ").append(getAmountOther()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD5514_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getAppPANSequenceNumber()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD5515_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if (Validator.validateSubFields(getAppPANSequenceNumber(), 2, 2, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[0]))
    {
      iccData.append(getAppPANSequenceNumber());
      displayBuffer.append("APP_PAN_SEQ_NUMBER            ").append(" :: ").append(getAppPANSequenceNumber()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD5515_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (ISOUtil.IsNullOrEmpty(getCryptogramInformationData()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD5516_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if (Validator.validateSubFields(getCryptogramInformationData(), 2, 2, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1]))
    {
      iccData.append(getCryptogramInformationData());
      displayBuffer.append("CRYPTOGRAM_INFO_DATA          ").append(" :: ").append(getCryptogramInformationData()).append(Constants.NEW_LINE_CHAR);
    }
    else
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD5516_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    if (!ISOUtil.IsNullOrEmpty(getReservedFutureUse()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD5517_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA);
      authorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    boolean debugflag = PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON");
    if (debugflag)
    {
      Log.i("ICC Data Bean\r\n----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n-                                 ICC Related Data Bean                                                                                                                                       -\r\n---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n" +
      
        displayBuffer.toString() + 
        "\r\n" + "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
      
      Log.i("Populated Integrated Circuit Card Related Data is -- " + iccData);
    }
    return iccData.toString();
  }
  
  public String toString()
  {
    StringBuffer buffer = new StringBuffer();
    
    buffer.append("VARIABLE_DATA_LENGTH").append(" :: ").append(getVli()).append(Constants.NEW_LINE_CHAR);
    buffer.append("ICC_HEADER_VERSION_NAME").append(" :: ").append(getIccHeaderVersionName()).append(Constants.NEW_LINE_CHAR);
    buffer.append("ICC_HEADER_VERSION_NUMBER").append(" :: ").append(getIccHeaderVersionNumber()).append(Constants.NEW_LINE_CHAR);
    buffer.append("APP_CRYPTOGRAM").append(" :: ").append(getAppCryptogram()).append(Constants.NEW_LINE_CHAR);
    buffer.append("ISSUER_APP_DATA").append(" :: ").append(getIssuerAppData()).append(Constants.NEW_LINE_CHAR);
    buffer.append("ICC_UNPREDICTATBLE_NUM").append(" :: ").append(getUnpredictableNumber()).append(Constants.NEW_LINE_CHAR);
    buffer.append("APP_TRANSACTION_COUNTER").append(" :: ").append(getAppTransactionCounter()).append(Constants.NEW_LINE_CHAR);
    buffer.append("TERMINAL_VERIFY_RESULTS").append(" :: ").append(getTerminalVerificationResults()).append(Constants.NEW_LINE_CHAR);
    buffer.append("TRANSACTION_DATE").append(" :: ").append(getTransactionDate()).append(Constants.NEW_LINE_CHAR);
    buffer.append("TRANSACTION_TYPE").append(" :: ").append(getTransactionType()).append(Constants.NEW_LINE_CHAR);
    buffer.append("AMOUNT_AUTHORIZED").append(" :: ").append(getAmountAuthorized()).append(Constants.NEW_LINE_CHAR);
    buffer.append("TRANSACTION_CURRENCY_CODE").append(" :: ").append(getTransactionCurrencyCode()).append(Constants.NEW_LINE_CHAR);
    buffer.append("TERMINAL_COUNTRY_CODE").append(" :: ").append(getTerminalCountryCode()).append(Constants.NEW_LINE_CHAR);
    buffer.append("APP_INTERCHANGE_PROFILE").append(" :: ").append(getAppInterchangeProfile()).append(Constants.NEW_LINE_CHAR);
    buffer.append("AMOUNT_OTHER").append(" :: ").append(getAmountOther()).append(Constants.NEW_LINE_CHAR);
    buffer.append("APP_PAN_SEQ_NUMBER").append(" :: ").append(getAppPANSequenceNumber()).append(Constants.NEW_LINE_CHAR);
    buffer.append("CRYPTOGRAM_INFO_DATA").append(" :: ").append(getCryptogramInformationData()).append(Constants.NEW_LINE_CHAR);
    buffer.append("RESRVD_FUTURE_USE").append(" :: ").append(getReservedFutureUse()).append(Constants.NEW_LINE_CHAR);
    
    return buffer.toString();
  }
}
