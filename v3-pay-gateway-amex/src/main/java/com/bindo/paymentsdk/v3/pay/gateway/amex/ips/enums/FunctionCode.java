package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums;

public enum FunctionCode {
    AUTHORIZATION_REQUEST("100"),
    BATCH_AUTHORIZATION("180"),
    PREPAID_CARD_PARTIAL_AUTHORIZATION_SUPPORTED("181"),
    PREPAID_CARD_AUTHORIZATION_WITH_BALANCE_RETURN_SUPPORTED("182"),
    AUTHORIZATION_ADJUSTMENT("202"),
    SYSTEM_AUDIT_CONTROL_ECHO_TEST("831"),
    ACCOUNT_STATUS_CHECK("190"),
    ATC_SYNCRONIZATION("191"),
    PAN_REQUEST("194"),
    SIGN_ON("801"),
    SIGN_OFF("802"),
    DYNAMIC_KEY_EXCHANGE("811"),
    EXPRESSWAY_TRANSLATION("196");

    private final String functionCode;

    private FunctionCode(String functionCode) {
        this.functionCode = functionCode;
    }

    public String getFunctionCode() {
        return this.functionCode;
    }
}
