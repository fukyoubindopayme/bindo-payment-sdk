package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.processor.iso;

import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.connection.PropertyReader;

import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.PGPCompressedData;
import org.bouncycastle.openpgp.PGPCompressedDataGenerator;
import org.bouncycastle.openpgp.PGPEncryptedDataGenerator;
import org.bouncycastle.openpgp.PGPEncryptedDataList;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPLiteralData;
import org.bouncycastle.openpgp.PGPObjectFactory;
import org.bouncycastle.openpgp.PGPOnePassSignatureList;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyEncryptedData;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPPublicKeyRingCollection;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.PGPSecretKeyRingCollection;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.util.encoders.Hex;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.SecureRandom;
import java.security.Security;
import java.util.Iterator;

public class BouncyCastlePGP {
  private static final Provider PROVIDER = new BouncyCastleProvider();
  private static final String PROVIDERNAME = "BC";
  static boolean debugFlag = true;

  private static final String TAG = "BouncyCastlePGP";
  
  public BouncyCastlePGP() {
  }

  private static PGPPublicKey readPublicKey(InputStream in, String ID) throws IOException, PGPException {
    InputStream iStream = null;
    iStream = PGPUtil.getDecoderStream(in);
    PGPPublicKeyRingCollection pgpPub = new PGPPublicKeyRingCollection(iStream);
    Iterator<?> rIt = pgpPub.getKeyRings();
    StringBuffer userIds = new StringBuffer();

    while(true) {
      label68:
      while(rIt.hasNext()) {
        PGPPublicKeyRing kRing = (PGPPublicKeyRing)rIt.next();
        Iterator kIt = kRing.getPublicKeys();

        PGPPublicKey k;
        label66:
        do {
          do {
            if (!kIt.hasNext()) {
              continue label68;
            }

            k = (PGPPublicKey)kIt.next();
            if (k.isMasterKey()) {
              Iterator<?> userIdItr = k.getUserIDs();
              userIds.delete(0, userIds.length());

              while(userIdItr.hasNext()) {
                userIds.append(userIdItr.next().toString());
              }
            }

            if (k.isMasterKey() && k.isRevoked()) {
              if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
                debugFlag = true;
                Log.d(TAG,"Key ID: " + Long.toHexString(k.getKeyID()) + " is revoked.");
                Log.d(TAG,"            Algorithm: " + getAlgorithm(k.getAlgorithm()));
                Log.d(TAG,"            Fingerprint: " + new String(Hex.encode(k.getFingerprint())));
                Log.d(TAG,"            Creation Time: " + k.getCreationTime());
                Log.d(TAG,"            Is Master Key: " + k.isMasterKey());
                Log.d(TAG,"            Is Encryption Key: " + k.isEncryptionKey());
                Log.d(TAG,"            Is Revoked Key: " + k.isRevoked());
                Log.d(TAG,"            Bit Strength: " + k.getBitStrength());
              }
              continue label68;
            }

            if (k.isMasterKey() && !k.isRevoked() && userIds.toString().toLowerCase().indexOf(ID) >= 0) {
              continue label66;
            }
          } while(k.isMasterKey() || !k.isEncryptionKey() || k.isRevoked() || userIds.toString().toLowerCase().indexOf(ID) < 0);

          if (debugFlag) {
            Log.d(TAG,"Sub Key ID: " + Long.toHexString(k.getKeyID()) + " (subkey)");
            Log.d(TAG,"            Algorithm: " + getAlgorithm(k.getAlgorithm()));
            Log.d(TAG,"            Fingerprint: " + new String(Hex.encode(k.getFingerprint())));
            Log.d(TAG,"            Creation Time: " + k.getCreationTime());
            Log.d(TAG,"            Is Master Key: " + k.isMasterKey());
            Log.d(TAG,"            Is Encryption Key: " + k.isEncryptionKey());
            Log.d(TAG,"            Is Revoked Key: " + k.isRevoked());
            Log.d(TAG,"            Bit Strength: " + k.getBitStrength());
          }

          return k;
        } while(kIt.hasNext() || !k.isEncryptionKey());

        if (debugFlag) {
          Log.d(TAG, "Key ID: " + Long.toHexString(k.getKeyID()));
          Log.d(TAG, "            Algorithm: " + getAlgorithm(k.getAlgorithm()));
          Log.d(TAG, "            Fingerprint: " + new String(Hex.encode(k.getFingerprint())));
          Log.d(TAG, "            Creation Time: " + k.getCreationTime());
          Log.d(TAG, "            Is Master Key: " + k.isMasterKey());
          Log.d(TAG, "            Is Encryption Key: " + k.isEncryptionKey());
          Log.d(TAG, "            Is Revoked Key: " + k.isRevoked());
          Log.d(TAG, "            Bit Strength: " + k.getBitStrength());
        }

        return k;
      }

      throw new IllegalArgumentException("Can't find encryption key in key ring or the key is revoked");
    }
  }

  private static String getAlgorithm(int algId) {
    switch(algId) {
      case 1:
        return "RSA_GENERAL";
      case 2:
        return "RSA_ENCRYPT";
      case 3:
        return "RSA_SIGN";
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
      case 10:
      case 11:
      case 12:
      case 13:
      case 14:
      case 15:
      default:
        return "unknown";
      case 16:
        return "ELGAMAL_ENCRYPT";
      case 17:
        return "DSA";
      case 18:
        return "EC";
      case 19:
        return "ECDSA";
      case 20:
        return "ELGAMAL_GENERAL";
      case 21:
        return "DIFFIE_HELLMAN";
    }
  }

  private static PGPPrivateKey findSecretKey(PGPSecretKeyRingCollection pgpSec, long keyID, char[] pass) throws PGPException, NoSuchProviderException {
    PGPSecretKey pgpSecKey = pgpSec.getSecretKey(keyID);
    return pgpSecKey == null ? null : pgpSecKey.extractPrivateKey(pass, "BC");
  }

  public static void decryptFile(String inFileName, String privateKeyRingFile, String passwd) throws Exception {
    if (Security.getProvider("BC") == null) {
      Security.addProvider(PROVIDER);
    }

    if (inFileName == null) {
      throw new IllegalArgumentException("Encrypted data file not provided.");
    } else if (privateKeyRingFile == null) {
      throw new IllegalArgumentException("private key ring file not provided");
    } else {
      String outFile = (new File(inFileName)).getAbsolutePath();
      outFile = outFile.substring(0, outFile.lastIndexOf(46));
      decryptFile(inFileName, privateKeyRingFile, passwd.toCharArray(), outFile);
    }
  }

  public static void decryptFile(String inFileName, String privateKeyRingFile, String passwd, String outFile) throws IllegalArgumentException, FileNotFoundException, Exception {
    if (Security.getProvider("BC") == null) {
      Security.addProvider(PROVIDER);
    }

    if (inFileName == null) {
      throw new IllegalArgumentException("Encrypted data file not provided.");
    } else if (privateKeyRingFile == null) {
      throw new IllegalArgumentException("Private key ring file not provided");
    } else if (privateKeyRingFile == null) {
      throw new IllegalArgumentException("Output file not provided");
    } else {
      decryptFile(inFileName, privateKeyRingFile, passwd.toCharArray(), outFile);
    }
  }

  private static void decryptFile(String inFile, String privateKeyRingFile, char[] passwd, String decryptFileName) throws Exception {
    InputStream in = new FileInputStream(inFile);
    InputStream keyIn = new FileInputStream(privateKeyRingFile);
    in = PGPUtil.getDecoderStream(in);
    FileOutputStream fOut = new FileOutputStream(decryptFileName);

    try {
      PGPObjectFactory pgpF = new PGPObjectFactory(in);
      Object o = pgpF.nextObject();
      PGPEncryptedDataList enc;
      if (o instanceof PGPEncryptedDataList) {
        enc = (PGPEncryptedDataList)o;
      } else {
        enc = (PGPEncryptedDataList)pgpF.nextObject();
      }

      Iterator<?> it = enc.getEncryptedDataObjects();
      PGPPrivateKey sKey = null;
      PGPPublicKeyEncryptedData pbe = null;

      for(PGPSecretKeyRingCollection pgpSec = new PGPSecretKeyRingCollection(PGPUtil.getDecoderStream(keyIn)); sKey == null && it.hasNext(); sKey = findSecretKey(pgpSec, pbe.getKeyID(), passwd)) {
        pbe = (PGPPublicKeyEncryptedData)it.next();
      }

      if (sKey == null) {
        throw new IllegalArgumentException("secret key for message not found.");
      }

      InputStream clear = pbe.getDataStream(sKey, "BC");
      PGPObjectFactory plainFact = new PGPObjectFactory(clear);
      Object message = plainFact.nextObject();
      if (message instanceof PGPCompressedData) {
        PGPCompressedData cData = (PGPCompressedData)message;
        PGPObjectFactory pgpFact = new PGPObjectFactory(cData.getDataStream());
        message = pgpFact.nextObject();
      }

      if (!(message instanceof PGPLiteralData)) {
        if (message instanceof PGPOnePassSignatureList) {
          throw new PGPException("encrypted message contains a signed message - not literal data.");
        }

        throw new PGPException("message is not a simple encrypted file - type unknown.");
      }

      PGPLiteralData ld = (PGPLiteralData)message;
      InputStream unc = ld.getInputStream();

      int ch;
      while((ch = unc.read()) >= 0) {
        fOut.write(ch);
      }
    } catch (PGPException var33) {
      if (debugFlag) {
        Log.e(TAG, var33.getMessage());
      }

      if (var33.getUnderlyingException() != null && debugFlag) {
        Log.e(TAG, var33.getUnderlyingException().getMessage());
      }

      throw var33;
    } finally {
      if (keyIn != null) {
        try {
          keyIn.close();
        } catch (Exception var32) {
          if (debugFlag) {
            Log.e(TAG, "Exception in class BouncyCastlePGP. Exception: " + var32);
          }
        }
      }

      if (in != null) {
        try {
          in.close();
        } catch (Exception var31) {
          if (debugFlag) {
            Log.e(TAG, "Exception in class BouncyCastlePGP. Exception: " + var31);
          }
        }
      }

      if (fOut != null) {
        try {
          fOut.close();
        } catch (Exception var30) {
          if (debugFlag) {
            Log.e(TAG, "Exception in class BouncyCastlePGP. Exception: " + var30);
          }
        }
      }

    }

  }

  public static void encryptFile(String inFileName, String publicKeyringfile, boolean armor, String userId) throws IOException, NoSuchProviderException, PGPException {
    if (Security.getProvider("BC") == null) {
      Security.addProvider(PROVIDER);
    }

    String id = userId.toLowerCase();
    if (publicKeyringfile == null) {
      throw new IllegalArgumentException("Public key ring file not provided.");
    } else if (inFileName == null) {
      throw new IllegalArgumentException("Input file not provided.");
    } else if (userId == null) {
      throw new IllegalArgumentException("User Id not provided.");
    } else {
      InputStream keyIn = null;

      keyIn = new ByteArrayInputStream(publicKeyringfile.getBytes());
      FileOutputStream out = null;
      if (armor) {
        out = new FileOutputStream(inFileName + ".asc");
      } else {
        out = new FileOutputStream(inFileName + ".pgp");
      }

      encryptFile(out, inFileName, readPublicKey(keyIn, id), armor);
    }
  }

  protected static void encryptFile(OutputStream out, String inFileName, PGPPublicKey encKey, boolean armor) throws IOException, NoSuchProviderException, PGPException {
    OutputStream outStream = null;
    if (armor) {
      outStream = new ArmoredOutputStream(out);
    }

    try {
      ByteArrayOutputStream bOut = new ByteArrayOutputStream();
      PGPCompressedDataGenerator comData = new PGPCompressedDataGenerator(1);
      PGPUtil.writeFileToLiteralData(comData.open(bOut), 'b', new File(inFileName));
      comData.close();
      PGPEncryptedDataGenerator cPk = new PGPEncryptedDataGenerator(3, false, new SecureRandom(), "BC");
      cPk.addMethod(encKey);
      byte[] bytes = bOut.toByteArray();
      OutputStream cOut = cPk.open(outStream, (long)bytes.length);
      cOut.write(bytes);
      cOut.close();
    } catch (PGPException var17) {
      if (debugFlag) {
        Log.e(TAG, var17.getMessage());
        var17.printStackTrace(System.out);
        if (var17.getUnderlyingException() != null) {
            var17.getUnderlyingException().printStackTrace(System.out);
        }
      }

      if (var17.getUnderlyingException() != null && debugFlag) {
        Log.e(TAG, var17.getUnderlyingException().getMessage());
      }

      throw var17;
    } finally {
      if (outStream != null) {
        try {
          outStream.close();
        } catch (Exception var16) {
          if (debugFlag) {
            Log.e(TAG, "Exception in class BouncyCastlePGP. Exception: " + var16);
          }
        }
      }

    }

  }
}
