package com.bindo.paymentsdk.v3.pay.gateway.amex.gateway;

import com.bindo.paymentsdk.v3.pay.common.TransactionData;
import com.bindo.paymentsdk.v3.pay.common.emv.enums.EMVTag;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Request;
import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.EncryptedData;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.ErrorObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.SettlementRequestDataObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.SettlementResponseDataObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceBasicBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceDetailBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionBatchTrailerBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionFileHeaderBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionFileSummaryBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionTBTSpecificBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.LocationDetailTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.LodgingIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.RetailIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.nonindustrytype.EMVBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.connection.PropertyReader;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.AddAmtTypeCd;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.AddendaTypeCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.CurrencyCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.FormatCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.MediaCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.ProcessingCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.SubmissionMethod;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.exceptions.SettlementException;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.processor.iso.RequestProcessorFactory;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.utils.DataConversion;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.Constants;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.MCCCode;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.io.File;
import java.security.Security;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.EventObject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AmexSubmissionGateway {

    private final String TAG = "AmExSubmissionGateway";

    // CONFIGURATION KEYS
    private static final String CONFIG_DEBUGGER_FLAG     = "debugger_flag";
    private static final String CONFIG_FILE_STORAGE      = "file_storage";
    private static final String CONFIG_USER_IDENTITY     = "user_id";
    private static final String CONFIG_PUBLIC_KEY_FILE   = "public_key";
    private static final String CONFIG_FTP_LOCAL_UPLOAD  = "ftp_local_upload";
    private static final String CONFIG_FTP_LOCAL_DNLOAD  = "ftp_local_dnload";
    private static final String CONFIG_FTP_UPLOAD_FILE   = "ftp_upload_file";
    private static final String CONFIG_ORIGIN            = "origin";
    private static final String CONFIG_REGION            = "region";
    private static final String CONFIG_ROUTING_INDICATOR = "routing_indicator";
    private static final String CONFIG_XML_PROXY         = "xml_proxy";
    private static final String CONFIG_FTP_URL           = "ftp_url";
    private static final String CONFIG_FILE_TYPE         = "file_type";
    private static final String CONFIG_PUBLIC_KEY        = "public_key";
    private static final String CONFIG_TERMINAL_ID       = "terminal_id";
    private static final String CONFIG_MERCHANT_ID       = "merchant_id";
    private static final String CONFIG_MERCHANT_COUNTRY  = "merchant_country";
    private static final String CONFIG_MERCHANT_NAME     = "merchant_name";
    private static final String CONFIG_MERCHANT_ADDRESS  = "merchant_address";
    private static final String CONFIG_MERCHANT_CITY     = "merchant_city";
    private static final String CONFIG_MERCHANT_REGION   = "merchant_region";
    private static final String CONFIG_MERCHANT_ZIP      = "merchant_zip";
    private static final String CONFIG_MERCHANT_MCC      = "merchant_mcc";
    private static final String CONFIG_MERCHANT_LOCATION = "merchant_location_id";

    // DEFAULT CONFIGURATION
    private static final String DEFAULT_CONFIG_DEBUGGER_FLAG     = "ON";
    private static final String DEFAULT_CONFIG_FILE_STORAGE      = "ON";
    private static final String DEFAULT_CONFIG_USER_IDENTITY     = "SFT generated key";
    private static final String DEFAULT_CONFIG_PUBLIC_KEY_FILE   = "/BINLABHKGTST_INC_PK01.txt";
    private static final String DEFAULT_CONFIG_FTP_LOCAL_UPLOAD  = "";
    private static final String DEFAULT_CONFIG_FTP_LOCAL_DNLOAD  = "";
    private static final String DEFAULT_CONFIG_FTP_UPLOAD_FILE   = "/BINLABHKG.SUB.BINLABHKGTST";
    private static final String DEFAULT_CONFIG_ORIGIN            = "BINDO";
    private static final String DEFAULT_CONFIG_REGION            = "JAPA";
    private static final String DEFAULT_CONFIG_TERMINAL_ID       = "001";
    private static final String DEFAULT_CONFIG_ROUTING_INDICATOR = "050";
    private static final String DEFAULT_CONFIG_XML_PROXY         = "NO";
    private static final String DEFAULT_CONFIG_FTP_URL           = "fsgateway.aexp.com";
    private static final String DEFAULT_CONFIG_FILE_TYPE         = "FIXED";
    private static final String DEFAULT_CONFIG_MERCHANT_ID       = "0512860628";
    //private static final String DEFAULT_CONFIG_MERCHANT_ID       = "7273525535";
    private static final String DEFAULT_CONFIG_MERCHANT_COUNTRY  = "344";
    private static final String DEFAULT_CONFIG_MERCHANT_NAME     = "BINDO";
    private static final String DEFAULT_CONFIG_MERCHANT_ADDRESS  = "1 Bindo Rd.";
    private static final String DEFAULT_CONFIG_MERCHANT_CITY     = "Hong Kong";
    private static final String DEFAULT_CONFIG_MERCHANT_REGION   = "HK";
    private static final String DEFAULT_CONFIG_MERCHANT_ZIP      = "71000";
    private static final String DEFAULT_CONFIG_MERCHANT_LOCATION = "HKG100000089012";
    private static final String DEFAULT_CONFIG_MERCHANT_MCC      = MCCCode.Computer_Software_Stores.getMccCode();
    //private static final String DEFAULT_CONFIG_MERCHANT_MCC      = MCCCode.Lodging_Hotels_Motels_and_Resorts.getMccCode();

    private PropertyReader propertyReader;

    private HashMap<String, Object> mDefaultGatewayConfig = new HashMap<>();

    public HashMap<String, Object> getDefaultGatewayConfig() {
        return mDefaultGatewayConfig;
    }

    private AmexSubmissionGatewayListener listener;

    public void setDefaultGatewayConfig(HashMap<String, Object> defaultGatewayConfig) {
        this.mDefaultGatewayConfig = defaultGatewayConfig;
    }

    public AmexSubmissionGateway(HashMap<String, Object> configMap, File localFilesDir, String publicKey) {
        Log.setsUseSystemOut(true);

        mDefaultGatewayConfig.put(CONFIG_PUBLIC_KEY_FILE, DEFAULT_CONFIG_PUBLIC_KEY_FILE);
        mDefaultGatewayConfig.put(CONFIG_DEBUGGER_FLAG, DEFAULT_CONFIG_DEBUGGER_FLAG);
        mDefaultGatewayConfig.put(CONFIG_FILE_STORAGE, DEFAULT_CONFIG_FILE_STORAGE);
        mDefaultGatewayConfig.put(CONFIG_FILE_TYPE, DEFAULT_CONFIG_FILE_TYPE);
        mDefaultGatewayConfig.put(CONFIG_FTP_LOCAL_DNLOAD, DEFAULT_CONFIG_FTP_LOCAL_DNLOAD);
        mDefaultGatewayConfig.put(CONFIG_FTP_LOCAL_UPLOAD, localFilesDir.getAbsolutePath());
        mDefaultGatewayConfig.put(CONFIG_FTP_UPLOAD_FILE, DEFAULT_CONFIG_FTP_UPLOAD_FILE);
        mDefaultGatewayConfig.put(CONFIG_FTP_URL, DEFAULT_CONFIG_FTP_URL);
        mDefaultGatewayConfig.put(CONFIG_ORIGIN, DEFAULT_CONFIG_ORIGIN);
        mDefaultGatewayConfig.put(CONFIG_REGION, DEFAULT_CONFIG_REGION);
        mDefaultGatewayConfig.put(CONFIG_ROUTING_INDICATOR, DEFAULT_CONFIG_ROUTING_INDICATOR);
        mDefaultGatewayConfig.put(CONFIG_USER_IDENTITY, DEFAULT_CONFIG_USER_IDENTITY);
        mDefaultGatewayConfig.put(CONFIG_XML_PROXY, DEFAULT_CONFIG_XML_PROXY);
        mDefaultGatewayConfig.put(CONFIG_TERMINAL_ID, DEFAULT_CONFIG_TERMINAL_ID);
        mDefaultGatewayConfig.put(CONFIG_MERCHANT_ID, DEFAULT_CONFIG_MERCHANT_ID);
        mDefaultGatewayConfig.put(CONFIG_MERCHANT_COUNTRY, DEFAULT_CONFIG_MERCHANT_COUNTRY);
        mDefaultGatewayConfig.put(CONFIG_MERCHANT_NAME, DEFAULT_CONFIG_MERCHANT_NAME);
        mDefaultGatewayConfig.put(CONFIG_MERCHANT_ADDRESS, DEFAULT_CONFIG_MERCHANT_ADDRESS);
        mDefaultGatewayConfig.put(CONFIG_MERCHANT_CITY, DEFAULT_CONFIG_MERCHANT_CITY);
        mDefaultGatewayConfig.put(CONFIG_MERCHANT_REGION, DEFAULT_CONFIG_MERCHANT_REGION);
        mDefaultGatewayConfig.put(CONFIG_MERCHANT_ZIP, DEFAULT_CONFIG_MERCHANT_ZIP);
        mDefaultGatewayConfig.put(CONFIG_MERCHANT_MCC, DEFAULT_CONFIG_MERCHANT_MCC);
        mDefaultGatewayConfig.put(CONFIG_MERCHANT_LOCATION, DEFAULT_CONFIG_MERCHANT_LOCATION);

        mDefaultGatewayConfig.put(CONFIG_PUBLIC_KEY, publicKey);

        if (configMap == null) {
            Log.w(TAG, "Please set the gateway parameters");
        } else {
            for (String key : configMap.keySet()) {
                Object value = configMap.get(key);
                mDefaultGatewayConfig.put(key, value);
            }
        }

        this.propertyReader = configureGateway();
    }

    private PropertyReader configureGateway() {

        Security.removeProvider(BouncyCastleProvider.PROVIDER_NAME);
        Security.insertProviderAt(new BouncyCastleProvider(), 1);

        PropertyReader pr = new PropertyReader();
        DataConversion dataConversion = new DataConversion();
        EncryptedData encryptedData = new EncryptedData();
        EncryptedData encryptedCredentials = dataConversion.encryptSFTCredentials("BINLABHKGTST", "amex123");
        PropertyReader.setFtpUserID(encryptedCredentials.getEncryptedUserID());
        PropertyReader.setFtpPassword(encryptedCredentials.getEncryptedPwd());

        PropertyReader.setFtpUserID("BINLABHKGTST");
        PropertyReader.setFtpPassword("amex123");

        PropertyReader.setDebuggerFlag((String) mDefaultGatewayConfig.get(CONFIG_DEBUGGER_FLAG));
        PropertyReader.setPublicKeyFilePath((String) mDefaultGatewayConfig.get(CONFIG_PUBLIC_KEY_FILE));
        PropertyReader.setFileStorage((String) mDefaultGatewayConfig.get(CONFIG_FILE_STORAGE));
        PropertyReader.setFileType((String) mDefaultGatewayConfig.get(CONFIG_FILE_TYPE));
        PropertyReader.setFtpLocalDownloadFilePath((String) mDefaultGatewayConfig.get(CONFIG_FTP_LOCAL_DNLOAD));
        PropertyReader.setFtpLocalUploadFilePath((String) mDefaultGatewayConfig.get(CONFIG_FTP_LOCAL_UPLOAD));
        PropertyReader.setFtpUploadFileName((String) mDefaultGatewayConfig.get(CONFIG_FTP_UPLOAD_FILE));
        PropertyReader.setFtpURL((String) mDefaultGatewayConfig.get(CONFIG_FTP_URL));
        PropertyReader.setMerchantCountry((String) mDefaultGatewayConfig.get(CONFIG_MERCHANT_COUNTRY));
        PropertyReader.setMerchantNumber((String) mDefaultGatewayConfig.get(CONFIG_MERCHANT_ID));
        PropertyReader.setOrigin((String) mDefaultGatewayConfig.get(CONFIG_ORIGIN));
        PropertyReader.setRegion((String) mDefaultGatewayConfig.get(CONFIG_REGION));
        PropertyReader.setRoutingIndicator((String) mDefaultGatewayConfig.get(CONFIG_ROUTING_INDICATOR));
        PropertyReader.setUserIdentity((String) mDefaultGatewayConfig.get(CONFIG_USER_IDENTITY));
        PropertyReader.setXmlProxy((String) mDefaultGatewayConfig.get(CONFIG_XML_PROXY));
        PropertyReader.setPublicKey((String) mDefaultGatewayConfig.get(CONFIG_PUBLIC_KEY));
        return pr;
    }

    private static final EMVTag[] TAGS = new EMVTag[] {
            EMVTag.APPLICATION_CRYPTOGRAM,
            EMVTag.ISSUER_APPLICATION_DATA,
            EMVTag.UNPREDICTABLE_NUMBER,
            EMVTag.APPLICATION_TRANSACTION_COUNTER,
            EMVTag.TERMINAL_VERIFICATION_RESULTS,
            EMVTag.TRANSACTION_DATE,
            EMVTag.TRANSACTION_TYPE,
            EMVTag.AMOUNT_AUTHORISED_NUMERIC,
            EMVTag.TRANSACTION_CURRENCY_CODE,
            EMVTag.TERMINAL_COUNTRY_CODE,
            EMVTag.APPLICATION_INTERCHANGE_PROFILE,
            EMVTag.AMOUNT_OTHER_NUMERIC,
            EMVTag.APPLICATION_PRIMARY_ACCOUNT_NUMBER_SEQUENCE_NUMBER,
            EMVTag.CRYPTOGRAM_INFORMATION_DATA
    };

    public void send(Request request) {

        List<TransactionData> transactionDataList = request.getTransactionDataList();
        if (transactionDataList == null || transactionDataList.size() == 0) {
            Log.w("EMPTY TRANSACTION LIST");
            return;
        }

        String SUBMITTER_ID = "BINLABHKG";
        SimpleDateFormat dfYYYYMMDD = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat dfHHMMSS = new SimpleDateFormat("HHmmss");
        SimpleDateFormat dfYYYYMMDDHHMM = new SimpleDateFormat("yyyyMMddHHmm");
        SimpleDateFormat dfYYYY = new SimpleDateFormat("yyyy");

        /*---------------------------------------------------------------------------------------------------
         Initialize TFH Values
        ---------------------------------------------------------------------------------------------------*/
        Date now = new Date();
        TransactionFileHeaderBean transactionFileHeaderBean = new TransactionFileHeaderBean();
        transactionFileHeaderBean.setSubmitterId(SUBMITTER_ID);
        transactionFileHeaderBean.setSubmitterFileReferenceNumber(dfYYYYMMDDHHMM.format(now).substring(3));
        transactionFileHeaderBean.setSubmitterFileSequenceNumber(String.valueOf(request.getAmexFileSequence()));
        transactionFileHeaderBean.setFileCreationDate(dfYYYYMMDD.format(now));
        transactionFileHeaderBean.setFileCreationTime(dfHHMMSS.format(now));

        double totalAmountFile = 0;
        int totalNoOfDebits = 0;
        double totalAmountOfDebits = 0;
        int totalNoOfCredits = 0;
        double totalAmountOfCredits = 0;

        Map<String, List> batchList = new HashMap<String, List>();
        for (TransactionData transactionData : transactionDataList) {
            String mid = transactionData.getMerchantId();
            if (mid == null) {
                Log.w("MERCHANTID IS NULL, USING DEFAULT, RRN:" + transactionData.getTransactionId());
                mid = String.valueOf(mDefaultGatewayConfig.get(CONFIG_MERCHANT_ID));
            }
            List<TransactionData> batch;
            if (batchList.containsKey(mid)) {
                batch = batchList.get(mid);
            } else {
                batch = new ArrayList<TransactionData>();
                batchList.put(mid, batch);
            }
            batch.add(transactionData);
        }

        List<TransactionTBTSpecificBean> transactionTBTSpecificType = new ArrayList<TransactionTBTSpecificBean>();
        for (Map.Entry<String, List> batchListEntry : batchList.entrySet()) {

            int totalNoOfTabs = 0;
            double totalAmount = 0;
            List<TransactionData> batch = batchListEntry.getValue();
            String batchMerchantID = batchListEntry.getKey();

            List<TransactionAdviceBasicBean> transactionAdviceBasicType = new ArrayList<TransactionAdviceBasicBean>();

            for (TransactionData transactionData : batch) {
                /*---------------------------------------------------------------------------------------------------
                Initialize TAB Values
                ---------------------------------------------------------------------------------------------------*/
                // Create a list that will store list of TransactionAdviceBasicBean objects

                Log.d("PAN: " + transactionData.getPrimaryAccountNumber() + " " + transactionData.getTransactionId());
                totalNoOfTabs++;
                TransactionAdviceBasicBean transactionAdviceBasicBean = new TransactionAdviceBasicBean();
                boolean isDebit = true;
                switch (transactionData.getTransactionType()) {
                    case "REFUND":
                        isDebit = false;
                        break;
                }

                String transactionIdentifier = "000000000000000";
                if (isDebit) {
                    if (transactionData.getTransactionId() != null) {
                        transactionIdentifier = transactionData.getTransactionId();
                    } else {
                        Log.w("NO TRANSACTION IDENTIFIER");
                        continue;
                    }
                }
                transactionAdviceBasicBean.setTransactionIdentifier(transactionIdentifier);
                transactionAdviceBasicBean.setFormatCode(FormatCode.No_Industry_Specific_TAA_Record.getFormatCode());
                /*
                if (batchMerchantID.equals("0512860628")) {
                    transactionAdviceBasicBean.setFormatCode(FormatCode.General_Retail_TAA_Record.getFormatCode());
                } else {
                    transactionAdviceBasicBean.setFormatCode(FormatCode.Lodging_TAA_Record.getFormatCode());
                }
                */

                transactionAdviceBasicBean.setMediaCode(MediaCode.Cardmember_signature_on_File.getMediaCode());
                transactionAdviceBasicBean.setSubmissionMethod(SubmissionMethod.Amex_POS_Terminal.getSubmissionMethod());
                if (isDebit) {
                    if (transactionData.getApprovalCode() != null) {
                        transactionAdviceBasicBean.setApprovalCode(transactionData.getApprovalCode());
                    } else {
                        transactionAdviceBasicBean.setApprovalCode("000000");
                    }
                } else {
                    transactionAdviceBasicBean.setApprovalCode("      ");
                }
                transactionAdviceBasicBean.setPrimaryAccountNumber(transactionData.getPrimaryAccountNumber());
                transactionAdviceBasicBean.setCardExpiryDate(transactionData.getCardExpirationDate());
                transactionAdviceBasicBean.setTransactionDate(dfYYYY.format(now).substring(0, 2) + transactionData.getTransactionDate());
                transactionAdviceBasicBean.setTransactionTime(transactionData.getTransactionTime());
                double transactionAmount = transactionData.getTransactionAmount();
                transactionAdviceBasicBean.setTransactionAmount(String.format("%012d", Math.round(transactionAmount * 100)));
                if (isDebit) {
                    totalAmount += transactionAmount;
                    totalAmountOfDebits += transactionAmount;
                    totalNoOfDebits++;
                } else {
                    totalAmount -= transactionAmount;
                    totalAmountOfCredits += transactionAmount;
                    totalNoOfCredits++;
                }
                ProcessingCode processingCode = isDebit ? ProcessingCode.DEBIT : ProcessingCode.CREDIT;
                transactionAdviceBasicBean.setProcessingCode(processingCode.getProcessingCode());
                transactionAdviceBasicBean.setTransactionCurrencyCode(transactionData.getTransactionCurrencyCode());
                transactionAdviceBasicBean.setExtendedPaymentData("01");
                transactionAdviceBasicBean.setMerchantId(batchMerchantID);
                transactionAdviceBasicBean.setMerchantLocationId((String) mDefaultGatewayConfig.get(CONFIG_MERCHANT_LOCATION));
                transactionAdviceBasicBean.setMerchantContactInfo(" ");
                transactionAdviceBasicBean.setTerminalId(transactionData.getTerminalId());
                transactionAdviceBasicBean.setPosDataCode(transactionData.getPosDataCode());
                transactionAdviceBasicBean.setInvoiceReferenceNumber(transactionData.getTransactionId());
                transactionAdviceBasicBean.setTabImageSequenceNumber("");
                transactionAdviceBasicBean.setMatchingKeyType("");
                transactionAdviceBasicBean.setMatchingKey("");
                transactionAdviceBasicBean.setElectronicCommerceIndicator("");

                ArrayList taaEmvArray = new ArrayList();

                if (isDebit && transactionData.getApplicationCryptogram() != null) {
                    EMVBean emvBean = new EMVBean();
                    emvBean.setTransactionIdentifier(transactionIdentifier);
                    emvBean.setEMVFormatType(com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.Constants.EMV_FORMAT_TYPE_UNPACKED);
                    emvBean.setFormatCode(FormatCode.No_Industry_Specific_TAA_Record.getFormatCode());
                    emvBean.setAddendaTypeCode(AddendaTypeCode.EMV_Addendum_Message.getAddendaTypeCode());

                    StringBuilder icc = new StringBuilder();
                    icc.append(Constants.HEADER_VERSION_NAME);
                    icc.append(Constants.HEADER_VERSION_NUMBER);
                    icc.append(transactionData.getApplicationCryptogram());
                    icc.append(String.format("%-64s", transactionData.getIssuerApplicationData()));
                    icc.append(transactionData.getUnpredictableNumber());
                    icc.append(transactionData.getApplicationTransactionCounter());
                    icc.append(transactionData.getTerminalVerificationResults());
                    icc.append(transactionData.getIccTransactionDate());
                    icc.append(transactionData.getIccTransactionType());
                    icc.append(transactionData.getAmountAuthorized());
                    icc.append(transactionData.getIccTransactionCurrencyCode());
                    icc.append(transactionData.getTerminalCountryCode());
                    icc.append(transactionData.getApplicationInterchangeProfile());
                    icc.append(transactionData.getAmountOther());
                    icc.append(transactionData.getApplicationPanSequence());
                    icc.append(transactionData.getCryptogramInformationData());
                    emvBean.setICCSystemRelatedData(icc.toString());

                    taaEmvArray.add(emvBean);
                }

                /*
                if (!batchMerchantID.equals("0512860628") && transactionData.getTransactionAmount() == 105000.00) {
                    TransactionAdviceDetailBean transactionAdviceDetailBean = new TransactionAdviceDetailBean();

                    transactionAdviceDetailBean.setTransactionIdentifier(transactionIdentifier);
                    transactionAdviceDetailBean.setAdditionalAmount1(String.format("%012d", Math.round(8.99 * 100)));
                    transactionAdviceDetailBean.setAdditionalAmountType1(AddAmtTypeCd.Bar_MiniBar.getAddAmtTypeCd());
                    transactionAdviceDetailBean.setAdditionalAmountSign1("+");
                    transactionAdviceDetailBean.setAdditionalAmount2(String.format("%012d", Math.round(20 * 100)));
                    transactionAdviceDetailBean.setAdditionalAmountType2(AddAmtTypeCd.Barber_Beauty_Salon.getAddAmtTypeCd());
                    transactionAdviceDetailBean.setAdditionalAmountSign2("+");
                    transactionAdviceDetailBean.setAdditionalAmount3(String.format("%012d", Math.round(10 * 100)));
                    transactionAdviceDetailBean.setAdditionalAmountType3(AddAmtTypeCd.Discount.getAddAmtTypeCd());
                    transactionAdviceDetailBean.setAdditionalAmountSign3("-");
                    transactionAdviceDetailBean.setAdditionalAmount4(String.format("%012d", Math.round(14.20 * 100)));
                    transactionAdviceDetailBean.setAdditionalAmountType4(AddAmtTypeCd.Gift_Shop.getAddAmtTypeCd());
                    transactionAdviceDetailBean.setAdditionalAmountSign4("+");
                    transactionAdviceDetailBean.setAdditionalAmount5(String.format("%012d", Math.round(12 * 100)));
                    transactionAdviceDetailBean.setAdditionalAmountType5(AddAmtTypeCd.Laundry_DryCleaning.getAddAmtTypeCd());
                    transactionAdviceDetailBean.setAdditionalAmountSign5("+");

                    transactionAdviceBasicBean.setTransactionAdviceDetailBean(transactionAdviceDetailBean);
                }

                if (transactionAdviceBasicBean.getFormatCode().equals(FormatCode.General_Retail_TAA_Record.getFormatCode())) {
                    RetailIndustryTAABean retailIndustryTAABean = new RetailIndustryTAABean();
                    retailIndustryTAABean.setTransactionIdentifier(transactionIdentifier);
                    retailIndustryTAABean.setAddendaTypeCode("03");
                    retailIndustryTAABean.setFormatCode("20");
                    retailIndustryTAABean.setRetailDepartmentName("BINDO1");
                    if (transactionAmount == 1) {
                        retailIndustryTAABean.setRetailItemAmount1(String.format("%012d", Math.round(1*100)));
                        retailIndustryTAABean.setRetailItemDescription1("Peanut");
                        retailIndustryTAABean.setRetailItemQuantity1("1");
                    } else if (transactionAmount == 31) {
                        retailIndustryTAABean.setRetailItemAmount1(String.format("%012d", Math.round(31*100)));
                        retailIndustryTAABean.setRetailItemDescription1("Multiplexer");
                        retailIndustryTAABean.setRetailItemQuantity1("1");
                    } else if (transactionAmount == 71) {
                        retailIndustryTAABean.setRetailItemAmount1(String.format("%012d", Math.round(20*100)));
                        retailIndustryTAABean.setRetailItemDescription1("Headphones");
                        retailIndustryTAABean.setRetailItemQuantity1("1");
                        retailIndustryTAABean.setRetailItemAmount2(String.format("%012d", Math.round(30*100)));
                        retailIndustryTAABean.setRetailItemDescription2("Earplugs");
                        retailIndustryTAABean.setRetailItemQuantity2("1");
                        retailIndustryTAABean.setRetailItemAmount3(String.format("%012d", Math.round(21*100)));
                        retailIndustryTAABean.setRetailItemDescription3("DVD Rick Astley");
                        retailIndustryTAABean.setRetailItemQuantity3("1");
                    } else if (transactionAmount == 151) {
                        retailIndustryTAABean.setRetailItemAmount1(String.format("%012d", Math.round(20*100)));
                        retailIndustryTAABean.setRetailItemDescription1("Headphones");
                        retailIndustryTAABean.setRetailItemQuantity1("1");
                        retailIndustryTAABean.setRetailItemAmount2(String.format("%012d", Math.round(30*100)));
                        retailIndustryTAABean.setRetailItemDescription2("Earplugs");
                        retailIndustryTAABean.setRetailItemQuantity2("1");
                        retailIndustryTAABean.setRetailItemAmount3(String.format("%012d", Math.round(40*100)));
                        retailIndustryTAABean.setRetailItemDescription3("DVD Lady Gaga");
                        retailIndustryTAABean.setRetailItemQuantity3("1");
                        retailIndustryTAABean.setRetailItemAmount4(String.format("%012d", Math.round(50*100)));
                        retailIndustryTAABean.setRetailItemDescription4("Flowers");
                        retailIndustryTAABean.setRetailItemQuantity4("1");
                        retailIndustryTAABean.setRetailItemAmount5(String.format("%012d", Math.round(10*100)));
                        retailIndustryTAABean.setRetailItemDescription5("Flamingo decor");
                        retailIndustryTAABean.setRetailItemQuantity5("1");
                        retailIndustryTAABean.setRetailItemAmount6(String.format("%012d", Math.round(1*100)));
                        retailIndustryTAABean.setRetailItemDescription6("Peanut");
                        retailIndustryTAABean.setRetailItemQuantity6("1");
                    }
                    taaEmvArray.add(retailIndustryTAABean);
                } else if (transactionAdviceBasicBean.getFormatCode().equals(FormatCode.Lodging_TAA_Record.getFormatCode())) {
                    LodgingIndustryTAABean lodgingIndustryTAABean = new LodgingIndustryTAABean();
                    lodgingIndustryTAABean.setTransactionIdentifier(transactionIdentifier);
                    lodgingIndustryTAABean.setAddendaTypeCode("03");
                    lodgingIndustryTAABean.setFormatCode("11");
                    lodgingIndustryTAABean.setLodgingCheckInDate("20180501");
                    lodgingIndustryTAABean.setLodgingCheckOutDate("20180507");
                    lodgingIndustryTAABean.setLodgingSpecialProgramCode("1");
                    lodgingIndustryTAABean.setLodgingFolioNumber("A112        ");
                    if (transactionAmount == 105000) {
                        lodgingIndustryTAABean.setLodgingRoomRate1(String.format("%012d", Math.round(1000 * 100)));
                        lodgingIndustryTAABean.setNumberOfNightsAtRoomRate1(String.format("%02d", Math.round(5)));
                        lodgingIndustryTAABean.setLodgingRoomRate2(String.format("%012d", Math.round(50000 * 100)));
                        lodgingIndustryTAABean.setNumberOfNightsAtRoomRate2(String.format("%02d", Math.round(1)));
                        lodgingIndustryTAABean.setLodgingRoomRate3(String.format("%012d", Math.round(50000 * 100)));
                        lodgingIndustryTAABean.setNumberOfNightsAtRoomRate3(String.format("%02d", Math.round(1)));
                    } else {
                        lodgingIndustryTAABean.setLodgingRoomRate1(String.format("%012d", Math.round(transactionAmount * 100)));
                        lodgingIndustryTAABean.setNumberOfNightsAtRoomRate1(String.format("%02d", Math.round(4)));
                        lodgingIndustryTAABean.setLodgingRoomRate2(String.format("%012d", Math.round(1 * 100)));
                        lodgingIndustryTAABean.setNumberOfNightsAtRoomRate2(String.format("%02d", Math.round(2)));
                        lodgingIndustryTAABean.setLodgingRoomRate3(String.format("%012d", Math.round(2 * 100)));
                        lodgingIndustryTAABean.setNumberOfNightsAtRoomRate3(String.format("%02d", Math.round(1)));
                    }
                    taaEmvArray.add(lodgingIndustryTAABean);
                }
                */

                transactionAdviceBasicBean.setArrayTAABean(taaEmvArray);
                /*---------------------------------------------------------------------------------------------------
                Initialize Location TAA Values
                ---------------------------------------------------------------------------------------------------*/

                LocationDetailTAABean locationDetailTAABean = new LocationDetailTAABean();
                locationDetailTAABean.setTransactionIdentifier(transactionIdentifier);
                locationDetailTAABean.setAddendaTypeCode(AddendaTypeCode.Location_Detail_Addendum_Message.getAddendaTypeCode());
                locationDetailTAABean.setLocationName((String) mDefaultGatewayConfig.get(CONFIG_MERCHANT_NAME));
                locationDetailTAABean.setLocationAddress((String) mDefaultGatewayConfig.get(CONFIG_MERCHANT_ADDRESS));
                locationDetailTAABean.setLocationCity((String) mDefaultGatewayConfig.get(CONFIG_MERCHANT_CITY));
                locationDetailTAABean.setLocationRegion((String) mDefaultGatewayConfig.get(CONFIG_MERCHANT_REGION));
                locationDetailTAABean.setLocationCountryCode((String) mDefaultGatewayConfig.get(CONFIG_MERCHANT_COUNTRY));
                locationDetailTAABean.setLocationPostalCode((String) mDefaultGatewayConfig.get(CONFIG_MERCHANT_ZIP));
                locationDetailTAABean.setMerchantCategoryCode((String) mDefaultGatewayConfig.get(CONFIG_MERCHANT_MCC));
                locationDetailTAABean.setSellerId("");

                transactionAdviceBasicBean.setLocationDetailTAABean(locationDetailTAABean);
                transactionAdviceBasicType.add(transactionAdviceBasicBean);
            }

            // Start TransactionBatchTrailer creation
            TransactionBatchTrailerBean transactionBatchTrailerBean = new TransactionBatchTrailerBean();
            transactionBatchTrailerBean.setMerchantId(batchMerchantID);
            transactionBatchTrailerBean.setTbtIdentificationNumber("000000000000000");
            transactionBatchTrailerBean.setTbtCreationDate(dfYYYYMMDD.format(now));
            transactionBatchTrailerBean.setTotalNoOfTabs(String.valueOf(totalNoOfTabs));
            transactionBatchTrailerBean.setTbtAmount(String.format("%012d", Math.round(Math.abs(totalAmount) * 100)));
            transactionBatchTrailerBean.setTbtAmountSign(totalAmount >= 0 ? "+" : "-");
            transactionBatchTrailerBean.setTbtCurrencyCode(CurrencyCode.Hong_Kong_Dollar.getCurrencyCode());
            transactionBatchTrailerBean.setTbtImageSequenceNumber("");
            // End TransactionBatchTrailer creation

            TransactionTBTSpecificBean transactionTBTSpecificBean = new TransactionTBTSpecificBean();

            transactionTBTSpecificBean.setTransactionAdviceBasicType(transactionAdviceBasicType);
            transactionTBTSpecificBean.setTransactionBatchTrailerBean(transactionBatchTrailerBean);

            transactionTBTSpecificType.add(transactionTBTSpecificBean);
            //RequestProcessorFactory.fetchConfirmationReportInRawData();

            totalAmountFile += totalAmount;
        }

		/*---------------------------------------------------------------------------------------------------
        Initialize Transaction File Summary Values
        ---------------------------------------------------------------------------------------------------*/

        TransactionFileSummaryBean transactionFileSummaryBean = new TransactionFileSummaryBean();
        transactionFileSummaryBean.setNumberOfDebits(String.valueOf(totalNoOfDebits));
        transactionFileSummaryBean.setHashTotalDebitAmount(String.format("%012d", Math.round(totalAmountOfDebits * 100)));
        transactionFileSummaryBean.setHashTotalCreditAmount(String.format("%012d", Math.round(totalAmountOfCredits * 100)));
        transactionFileSummaryBean.setHashTotalAmount(String.format("%012d", Math.round(Math.abs(totalAmountFile) * 100)));
        transactionFileSummaryBean.setNumberOfCredits(String.valueOf(totalNoOfCredits));

        SettlementRequestDataObject settlementRequestDataObject = new SettlementRequestDataObject();

        settlementRequestDataObject.setTransactionFileHeaderType(transactionFileHeaderBean);
        settlementRequestDataObject.setTransactionTBTSpecificType(transactionTBTSpecificType);
        settlementRequestDataObject.setTransactionFileSummaryType(transactionFileSummaryBean);

        // Upload the file
        //SettlementResponseDataObject settlementResponseDataObject=null;
        //SettlementMessageFormatter.formatTABRecordType(transactionAdviceBasicBean,errorCodes);
        try {
            if (listener != null) {
                listener.onProgress(new EventObject("Send settlement file"));
            }
            SettlementResponseDataObject settlementResponseDataObject = RequestProcessorFactory.processRequest(settlementRequestDataObject, propertyReader);
            if (listener != null) {
                listener.onProgress(new EventObject("Settlement file sent"));
            }
            Log.d(TAG, settlementResponseDataObject.getFileCreationDateTime());
            Log.d(TAG, settlementResponseDataObject.getFileReceiptDateTime());
            Log.d(TAG, settlementResponseDataObject.getFileStatus());
            Log.d(TAG, settlementResponseDataObject.getHashTotalAmount());
            Log.d(TAG, settlementResponseDataObject.getSubmitterFileReferenceNumber());
            List<ErrorObject> errors = settlementResponseDataObject.getActionCodeDescription();
            if (errors != null) {
                for (ErrorObject error : errors) {
                    Log.d(error.getErrorCode() + error.getErrorDescription());
                }
            }
        } catch (SettlementException e) {
            e.printStackTrace();
            if (listener != null) {
                listener.onError(e);
            }
        }
    }

    public AmexSubmissionGatewayListener getListener() {
        return listener;
    }

    public void setListener(AmexSubmissionGatewayListener listener) {
        this.listener = listener;
    }
}
