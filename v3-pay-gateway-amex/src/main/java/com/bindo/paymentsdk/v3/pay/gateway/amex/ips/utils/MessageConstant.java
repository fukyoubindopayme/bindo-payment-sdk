package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.utils;

public abstract interface MessageConstant
{
  public static final String INVALID_URL = "INVALID URL";
  public static final String CONNNECTION_SUCCESSFUL = "CONNNECTION SUCCESSFUL";
  public static final String CONNNECTION_FAILURE = "CONNNECTION FAILURE";
  public static final String LOG_FILE_PATH_EMPTY = "LOG FILE PATH IS EMPTY.";
}
