package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.utils;

public class FieldBean
{
  private int bitNo;
  private String name;
  private int maxLen;
  private int lenType;
  private int type;
  private String length;
  private String value;
  
  public FieldBean(int bitNo, String name, int maxLen, int lenType, int type, String length, String value)
  {
    this.bitNo = bitNo;
    this.name = name;
    this.maxLen = maxLen;
    this.lenType = lenType;
    this.type = type;
    this.length = length;
    this.value = value;
  }
  
  public void setBitNo(int bitNo)
  {
    this.bitNo = bitNo;
  }
  
  public void setName(String name)
  {
    this.name = name;
  }
  
  public void setMaxLen(int maxLen)
  {
    this.maxLen = maxLen;
  }
  
  public void setLenType(int lenType)
  {
    this.lenType = lenType;
  }
  
  public void setType(int type)
  {
    this.type = type;
  }
  
  public void setLength(String length)
  {
    this.length = length;
  }
  
  public void setValue(String value)
  {
    this.value = value;
  }
  
  public int getBitNo()
  {
    return this.bitNo;
  }
  
  public String getName()
  {
    return this.name;
  }
  
  public int getMaxLen()
  {
    return this.maxLen;
  }
  
  public int getLenType()
  {
    return this.lenType;
  }
  
  public int getType()
  {
    return this.type;
  }
  
  public String getLength()
  {
    return this.length;
  }
  
  public String getValue()
  {
    return this.value;
  }
  
  public String getAsciiValue()
  {
    if (this.type == 2)
    {
      if ((this.bitNo == 0) || (this.bitNo == 95)) {
        return this.value;
      }
      return DataConversion.convertStringToASCII(this.value);
    }
    if ((this.bitNo <= 34) || (this.bitNo == 48)) {
      return this.value;
    }
    try
    {
      return DataConversion.convertHexToASCII(this.value);
    }
    catch (Exception e) {}
    return "";
  }
  
  public String getEbcdicValue()
  {
    return DataConversion.convertStringToEBCDIC(this.value);
  }
}
