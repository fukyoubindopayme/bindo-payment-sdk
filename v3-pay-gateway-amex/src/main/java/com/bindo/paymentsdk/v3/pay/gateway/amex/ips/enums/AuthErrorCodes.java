package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums;

public enum AuthErrorCodes {
    ERROR_DATAFIELD_MESSAGETYPEIDENTIFIER("APIERR001", "Message Type Identifier is incorrect."),
    ERROR_DATAFIELD2_PRIMARYACCOUNTNUMBER("APIERR002", "Primary Account Number is incorrect."),
    ERROR_DATAFIELD3_PROCESSINGCODE("APIERR003", "Processing Code is incorrect."),
    ERROR_DATAFIELD4_TRANSACTIONAMOUNT("APIERR004", "Amount Transaction is incorrect."),
    ERROR_DATAFIELD5_RECONCILIATIONAMOUNT("APIERR005", "Amount Reconciliation is incorrect."),
    ERROR_DATAFIELD6_AMOUNTCARDHOLDERBILLING("APIERR006", "Amount Card Holder Billing is incorrect."),
    ERROR_DATAFIELD7_TRANSMISSIONDATETIME("APIERR007", "Date Time Transmission is incorrect."),
    ERROR_DATAFIELD8_AMOUNTCARDHOLDERBILLINGFEE("APIERR008", "Amount Card Holder Billing Fee is incorrect."),
    ERROR_DATAFIELD9_CONVERSIONRATERECONCILIATION("APIERR009", "Conversion Rate Reconciliation is incorrect."),
    ERROR_DATAFIELD10_CONVERSIONRATECARDHOLDERBILLING("APIERR010", "Conversion Rate Card Holder Billing is incorrect."),
    ERROR_DATAFIELD11_SYSTEMTRACEAUDITNUMBER("APIERR011", "System Trace Audit Number is incorrect."),
    ERROR_DATAFIELD12_DATETIMELOCALTRANSACTION("APIERR012", "Date Time Local Transmission is incorrect."),
    ERROR_DATAFIELD13_DATEEFFECTIVE("APIERR013", "Date Effective is incorrect."),
    ERROR_DATAFIELD14_DATEEXPIRATION("APIERR014", "Date Expiration is incorrect."),
    ERROR_DATAFIELD15_DATESETTLEMENT("APIERR015", "Date Settlement is incorrect."),
    ERROR_DATAFIELD16_DATECONVERSION("APIERR016", "Date Conversion is incorrect."),
    ERROR_DATAFIELD17_DATECAPTURE("APIERR017", "Date Capture is incorrect."),
    ERROR_DATAFIELD18_MERCHANTTYPE("APIERR018", "Merchant Type is incorrect."),
    ERROR_DATAFIELD19_COUNTRYCODEACQUIRINGINSTITUTION("APIERR019", "Country Code Acquiring Institution is incorrect."),
    ERROR_DATAFIELD20_COUNTRYCODEPRIMARYACCOUNTNUMBER("APIERR020", "Country Code Primary Account Number is incorrect."),
    ERROR_DATAFIELD21_COUNTRYCODEFORWARDINGINSTITUTION("APIERR021", "Country Code Forward Institution is incorrect."),
    ERROR_DATAFIELD22_POSDATACODE("APIERR022", "POS Data Code is incorrect."),
    ERROR_DATAFIELD22_POS6VALUEZ("APIERR022.1", "POS Data Code : Position 6 Value 'Z' must be used with position 7, Value 5"),
    ERROR_DATAFIELD23_CARDSEQUENCENUMBER("APIERR023", "Card Sequence Number is incorrect."),
    ERROR_DATAFIELD24_FUNCTIONCODE("APIERR024", "Function Code is incorrect."),
    ERROR_DATAFIELD25_MESSAGEREASONCODE("APIERR025", "Message Reason Code is incorrect."),
    ERROR_DATAFIELD26_CARDACCEPTORBUSINESSCODE("APIERR026", "Card Acceptor Business Code is incorrect."),
    ERROR_DATAFIELD27_APPROVALCODELENGTH("APIERR027", "Approval Code Length is incorrect."),
    ERROR_DATAFIELD28_DATERECONCILIATION("APIERR028", "Date Reconciliation is incorrect."),
    ERROR_DATAFIELD29_RECONCILIATIONINDICATOR("APIERR029", "Reconciliation Indicator is incorrect"),
    ERROR_DATAFIELD30_AMOUNTSORIGINAL("APIERR030", "Amounts Original is incorrect."),
    ERROR_DATAFIELD31_ACQUIRERREFERENCEDATA("APIERR031", "Acquirer Reference Data is incorrect."),
    ERROR_DATAFIELD32_ACQUIRINGINSTITUTIONIDENTIFICATIONCODE("APIERR032", "Acquiring Institution Identification Code is incorrect."),
    ERROR_DATAFIELD33_FORWARDINSTITUTIONIDENTIFICATIONCODE("APIERR033", "Forward Institution Identification Code is incorrect."),
    ERROR_DATAFIELD34_PRIMARYACCOUNTNUMBEREXTENDED("APIERR034", "Primary Account Number Extended is incorrect."),
    ERROR_DATAFIELD35_TRACKTWODATA("APIERR035", "Track Two Data is incorrect"),
    ERROR_DATAFIELD36_TRACKTHREEDATA("APIERR036", "Track Three Data is incorrect."),
    ERROR_DATAFIELD37_RETRIEVALREFERENCENUMBER("APIERR037", "Retrieval Reference Number is incorrect."),
    ERROR_DATAFIELD38_APPROVALCODE("APIERR038", "Approval Code is incorrect."),
    ERROR_DATAFIELD39_ACTIONCODE("APIERR039", "Action Code is incorrect."),
    ERROR_DATAFIELD40_SERVICECODE("APIERR040", "Service Code is incorrect."),
    ERROR_DATAFIELD41_CARDACCEPTORTERMINALIDENTIFICATION("APIERR041", "Card Acceptor Terminal Identification is incorrect."),
    ERROR_DATAFIELD42_CARDACCEPTORIDENTIFICATIONCODE("APIERR042", "Card Acceptor Identification Code is incorrect."),
    ERROR_DATAFIELD43_CARDACCEPTORNAMELOCATION("APIERR043", "Card Acceptor Name Location is incorrect"),
    ERROR_DATAFIELD431_CARDACCEPTORNAMELOCATION("APIERR0431", "Card Acceptor Postal Code is incorrect"),
    ERROR_DATAFIELD432_CARDACCEPTORNAMELOCATION("APIERR0432", "Card Acceptor Region Code is incorrect"),
    ERROR_DATAFIELD433_CARDACCEPTORNAMELOCATION("APIERR0433", "Card Acceptor Country Code is incorrect"),
    ERROR_DATAFIELD434_CARDACCEPTORNAMELOCATION("APIERR0434", "Card Acceptor Name is incorrect"),
    ERROR_DATAFIELD435_CARDACCEPTORNAMELOCATION("APIERR0435", "Card Acceptor Seller Street is incorrect"),
    ERROR_DATAFIELD436_CARDACCEPTORNAMELOCATION("APIERR0436", "Card Acceptor Phone Number is incorrect"),
    ERROR_DATAFIELD437_CARDACCEPTORNAMELOCATION("APIERR0437", "Card Acceptor Email is incorrect"),
    ERROR_DATAFIELD438_CARDACCEPTORNAMELOCATION("APIERR0438", "Card Acceptor Seller City is incorrect"),
    ERROR_DATAFIELD439_CARDACCEPTORNAMELOCATION("APIERR0439", "Card Acceptor Name Location Length is incorrect"),
    ERROR_DATAFIELD440_CARDACCEPTORNAMELOCATION("APIERR0440", "Oil Company Station Location Code is incorrect"),
    ERROR_DATAFIELD44_ADDITIONALRESPONSEDATA("APIERR044", "Additional Response Data is incorrect"),
    ERROR_DATAFIELD45_TRACKONEDATA("APIERR045", "Track One Data is incorrect"),
    ERROR_DATAFIELD46_AMOUNTSFEE("APIERR046", "Amounts Fee is incorrect"),
    ERROR_DATAFIELD47_ADDITIONALDATANATIONAL("APIERR047", "Additional Data National is incorrect"),
    ERROR_DATAFIELD471_ADDITIONALDATANATIONAL("APIERR0471", "Additional Data National Primary Id is incorrect"),
    ERROR_DATAFIELD472_ADDITIONALDATANATIONAL("APIERR0472", "Additional Data National Secondary Id is incorrect"),
    ERROR_DATAFIELD473_ADDITIONALDATANATIONAL("APIERR0473", "Additional Data National Customer Email Id is incorrect"),
    ERROR_DATAFIELD474_ADDITIONALDATANATIONAL("APIERR0474", "Additional Data National Customer Email  is incorrect"),
    ERROR_DATAFIELD475_ADDITIONALDATANATIONAL("APIERR0475", "Additional Data National Customer HostName Id is incorrect"),
    ERROR_DATAFIELD476_ADDITIONALDATANATIONAL("APIERR0476", "Additional Data National Customer HostName is incorrect"),
    ERROR_DATAFIELD477_ADDITIONALDATANATIONAL("APIERR0477", "Additional Data National Customer HttpBrowser Type Id is incorrect"),
    ERROR_DATAFIELD478_ADDITIONALDATANATIONAL("APIERR0478", "Additional Data National Customer HttpBrowser Type  is incorrect"),
    ERROR_DATAFIELD479_ADDITIONALDATANATIONAL("APIERR0479", "Additional Data National Customer Ship To Country Id  is incorrect"),
    ERROR_DATAFIELD4710_ADDITIONALDATANATIONAL("APIERR04710", "Additional Data National Customer Ship To Country  is incorrect"),
    ERROR_DATAFIELD4711_ADDITIONALDATANATIONAL("APIERR04711", "Additional Data National Customer Shipping Method Id  is incorrect"),
    ERROR_DATAFIELD4712_ADDITIONALDATANATIONAL("APIERR04712", "Additional Data National Customer Shipping Method  is incorrect"),
    ERROR_DATAFIELD4713_ADDITIONALDATANATIONAL("APIERR04713", "Additional Data National Merchant Product Sku Id is incorrect"),
    ERROR_DATAFIELD4714_ADDITIONALDATANATIONAL("APIERR04714", "Additional Data National Merchant Product Sku is incorrect"),
    ERROR_DATAFIELD4715_ADDITIONALDATANATIONAL("APIERR04715", "Additional Data National Merchant Customer IP is incorrect"),
    ERROR_DATAFIELD4716_ADDITIONALDATANATIONAL("APIERR04716", "Additional Data National Merchant Customer ANI is incorrect"),
    ERROR_DATAFIELD4717_ADDITIONALDATANATIONAL("APIERR04717", "Additional Data National Customer Info Identifier is incorrect"),
    ERROR_DATAFIELD4718_ADDITIONALDATANATIONAL("APIERR04718", "Additional Data National Customer Info Identifier is incorrect"),
    ERROR_DATAFIELD4719_ADDITIONALDATANATIONAL("APIERR04719", "Additional Data National Departure Date is incorrect"),
    ERROR_DATAFIELD4720_ADDITIONALDATANATIONAL("APIERR04720", "Additional Data National Passanger Name Id is incorrect"),
    ERROR_DATAFIELD4721_ADDITIONALDATANATIONAL("APIERR04721", "Additional Data National Passanger Name is incorrect"),
    ERROR_DATAFIELD4722_ADDITIONALDATANATIONAL("APIERR04722", "Additional Data National Origin is incorrect"),
    ERROR_DATAFIELD4723_ADDITIONALDATANATIONAL("APIERR04723", "Additional Data National Destination is incorrect"),
    ERROR_DATAFIELD4724_ADDITIONALDATANATIONAL("APIERR04724", "Additional Data National Routing Id is incorrect"),
    ERROR_DATAFIELD4725_ADDITIONALDATANATIONAL("APIERR04725", "Additional Data National No Of Cities is incorrect"),
    ERROR_DATAFIELD4726_ADDITIONALDATANATIONAL("APIERR04726", "Additional Data National Routing Cities is incorrect"),
    ERROR_DATAFIELD4727_ADDITIONALDATANATIONAL("APIERR04727", "Additional Data National Airline Carriers Id is incorrect"),
    ERROR_DATAFIELD4728_ADDITIONALDATANATIONAL("APIERR04728", "Additional Data National No Of Airline Carriers is incorrect"),
    ERROR_DATAFIELD4729_ADDITIONALDATANATIONAL("APIERR04729", "Additional Data National Airline Carriers is incorrect"),
    ERROR_DATAFIELD4730_ADDITIONALDATANATIONAL("APIERR04730", "Additional Data National Fare Basis is incorrect"),
    ERROR_DATAFIELD4731_ADDITIONALDATANATIONAL("APIERR04731", "Additional Data National No Of Passengers is incorrect"),
    ERROR_DATAFIELD4732_ADDITIONALDATANATIONAL("APIERR04732", "Additional Data National Cardmember Id is incorrect"),
    ERROR_DATAFIELD4733_ADDITIONALDATANATIONAL("APIERR04733", "Additional Data National Cardmember Name is incorrect"),
    ERROR_DATAFIELD4734_ADDITIONALDATANATIONAL("APIERR04734", "Additional Data National E-Ticket Indicator is incorrect"),
    ERROR_DATAFIELD4735_ADDITIONALDATANATIONAL("APIERR04735", "Additional Data National Reservation Code Id is incorrect"),
    ERROR_DATAFIELD4736_ADDITIONALDATANATIONAL("APIERR04736", "Additional Data National Reservation Code is incorrect"),
    ERROR_DATAFIELD4737_ADDITIONALDATANATIONAL("APIERR04737", "Additional Data National Version Number is incorrect"),
    ERROR_DATAFIELD4738_ADDITIONALDATANATIONAL("APIERR04738", "Additional Data National Goods Sold Id is incorrect"),
    ERROR_DATAFIELD4739_ADDITIONALDATANATIONAL("APIERR04739", "Additional Data National Goods Sold Product Code is incorrect"),
    ERROR_DATAFIELD48_ADDITIONALDATAPRIVATE("APIERR048", "Additional Data Private is incorrect"),
    ERROR_DATAFIELD49_CURRENCYCODETRANSACTION("APIERR049", "Currency Code Transaction is incorrect"),
    ERROR_DATAFIELD50_CURRENCYCODERECONCILIATION("APIERR050", "Currency Code Reconciliation is incorrect"),
    ERROR_DATAFIELD51_CURRENCYCODECARDHOLDERBILLING("APIERR051", "Currency Code Card Holder Billing is incorrect"),
    ERROR_DATAFIELD52_PERSONALIDENTIFICATIONNUMBERDATA("APIERR052", "Personal Identification Number Data is incorrect"),
    ERROR_DATAFIELD53_SECURITYRELATEDCONTROLINFORMATION("APIERR053", "Security Related Control Information is incorrect"),
    ERROR_DATAFIELD53_SECURITYRELATEDCONTROLINFOPRIMARYID("APIERR053.1", "Security Related Control Information : Primary ID is incorrect"),
    ERROR_DATAFIELD53_SECURITYRELATEDCONTROLINFOSECONDARYID("APIERR053.2", "Security Related Control Information : Secondary Id is incorrect"),
    ERROR_DATAFIELD53_SECURITYRELATEDCONTROLINFOKSN("APIERR053.3", "Security Related Control Information : KSN is incorrect"),
    ERROR_DATAFIELD54_AMOUNTSADDITIONAL("APIERR054", "Amounts Additional is incorrect"),
    ERROR_DATAFIELD55_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA("APIERR055", "Integrated Circuit Card System Related Data is incorrect"),
    ERROR_DATAFIELD551_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA("APIERR0551", "ICC Header Version Name is incorrect"),
    ERROR_DATAFIELD552_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA("APIERR0552", " ICC Header Version Number is incorrect"),
    ERROR_DATAFIELD553_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA("APIERR0553", " ICC Application Cryptogram is incorrect"),
    ERROR_DATAFIELD554_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA("APIERR0554", " ICC Issuer Application Data is incorrect"),
    ERROR_DATAFIELD555_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA("APIERR0555", " ICC Unpredictable Number is incorrect"),
    ERROR_DATAFIELD556_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA("APIERR0556", " ICC Application Transaction Counter is incorrect"),
    ERROR_DATAFIELD557_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA("APIERR0557", " ICC Terminal Verification Results is incorrect"),
    ERROR_DATAFIELD558_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA("APIERR0558", " ICC Transaction Date is incorrect"),
    ERROR_DATAFIELD559_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA("APIERR0559", " ICC Transaction Type is incorrect"),
    ERROR_DATAFIELD5510_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA("APIERR05510", " ICC Amount Authorized is incorrect"),
    ERROR_DATAFIELD5511_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA("APIERR05511", " ICC Transaction Currency Code is incorrect"),
    ERROR_DATAFIELD5512_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA("APIERR05512", " ICC Terminal Country Code is incorrect"),
    ERROR_DATAFIELD5513_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA("APIERR05513", " ICC Application Interchange Profile is incorrect"),
    ERROR_DATAFIELD5514_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA("APIERR05514", " ICC Amount, Other is incorrect"),
    ERROR_DATAFIELD5515_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA("APIERR05515", " ICC Application PAN Sequence Number is incorrect"),
    ERROR_DATAFIELD5516_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA("APIERR05516", " ICC Cryptogram Information Data is incorrect"),
    ERROR_DATAFIELD5517_INTEGRATEDCIRCUITCARDSYSTEMRELATEDDATA("APIERR05517", " ICC Reserved For Future Use should not be used"),
    ERROR_DATAFIELD56_ORIGINALDATAELEMENTSDATA("APIERR056", "Original Data Elements Data is incorrect"),
    ERROR_DATAFIELD561_ORIGINALDATAELEMENTSDATA("APIERR0561", "Original Data Elements Data Message Type Identifier  is incorrect"),
    ERROR_DATAFIELD562_ORIGINALDATAELEMENTSDATA("APIERR0562", "Original Data Elements Data System Trace Audit Number  is incorrect"),
    ERROR_DATAFIELD563_ORIGINALDATAELEMENTSDATA("APIERR0563", "Original Data Elements Data Date and Time, Local Transaction is incorrect"),
    ERROR_DATAFIELD564_ORIGINALDATAELEMENTSDATA("APIERR0564", "Original Data Elements Data Acquiring Institution Identification Code is incorrect"),
    ERROR_DATAFIELD57_AUTHORIZATIONLIFECYCLECODE("APIERR057", "Authorization Life Cycle Code is incorrect"),
    ERROR_DATAFIELD58_AUTHORIZATIONAGENTINSTITUTIONIDENTIFICATIONCODE("APIERR058", "Authorization Agent Institution Identification Code is incorrect"),
    ERROR_DATAFIELD59_TRANSPORTDATA("APIERR059", "Transport Data is incorrect"),
    ERROR_DATAFIELD60_NATIONALUSEDATA1("APIERR060", "National Use Data1 is incorrect"),
    ERROR_DATAFIELD60_NATIONALUSEDATA1PRIMARYID("APIERR060.1", "National Use Data 1 : Primary ID is incorrect"),
    ERROR_DATAFIELD60_NATIONALUSEDATA1SECONDARYID("APIERR060.2", "National Use Data 1 : Secondary ID is incorrect"),
    ERROR_DATAFIELD60_NATIONALUSEDATA1SELLERID("APIERR060.4", "National Use Data 1 : Seller Id is incorrect"),
    ERROR_DATAFIELD60_NATIONALUSEDATA1SELLEREMAILID("APIERR060.5", "National Use Data 1 : Seller Email Address is incorrect"),
    ERROR_DATAFIELD60_NATIONALUSEDATA1SELLERTELEPHONE("APIERR060.6", "National Use Data 1 : Seller Telephone is incorrect"),
    ERROR_DATAFIELD60_NATIONALUSEDATA1MISSING("APIERR060.7", "National Use Data1 must be present for 'Payment Service Providers (Aggregators) and OptBlue participants' to support seller details."),
    ERROR_DATAFIELD60_NATIONALUSEDATA1FORPSPONLY("APIERR060.8", "National Use Data1 must be present only for 'Payment Service Providers (Aggregators) and OptBlue participants' to support seller details."),
    ERROR_DATAFIELD60_NATIONALUSEDATA1TOKENREQUESTORID("APIERR060.9", "National Use Data 1 : Token Requester ID (TRID) is incorrect"),
    ERROR_DATAFIELD60_NATIONALUSEDATA1LAST4PANRETIND("APIERR060.10", "National Use Data 1 : LAST 4 PAN RETURN INDICATOR is incorrect"),
    ERROR_DATAFIELD61_NATIONALUSEDATA2("APIERR061", "National use Data 2 is incorrect"),
    ERROR_DATAFIELD61_NATIONALUSEDATA2PRIMARYID("APIERR061.1", "National use Data2 : Primary ID is incorrect"),
    ERROR_DATAFIELD61_NATIONALUSEDATA2SECONDARYID("APIERR061.2", "National use Data2 : Secondary ID is incorrect"),
    ERROR_DATAFIELD61_NATIONALUSEDATA2ELECTRONICCOMMERCEINDICATOR("APIERR061.3", "National use Data2 : Electronic Commerce Indicator is incorrect"),
    ERROR_DATAFIELD61_NATIONALUSEDATA2AEXPVERIFICATIONVALUEID("APIERR061.4", "National use Data2 : AEXP Verification Value ID is incorrect"),
    ERROR_DATAFIELD61_NATIONALUSEDATA2AEXPVERIFICATIONVALUEIDVALUE("APIERR061.5", "National use Data2 : AEXP Verification Value is incorrect"),
    ERROR_DATAFIELD61_NATIONALUSEDATA2AEXPSAFKEYTRANSACTIONID("APIERR061.6", "National use Data2 : AEXP AEXP Safkey Transaction ID is incorrect"),
    ERROR_DATAFIELD61_NATIONALUSEDATA2AEXPSAFKEYTRANSACTIONIDVALUE("APIERR061.7", "National use Data2 : Safkey Transaction ID Value is incorrect"),
    ERROR_DATAFIELD61_NATIONALUSEDATA2AEXPSAFKEYNOTENABLED("APIERR061.8", "Country code not enabled for SafeKey transaction"),
    ERROR_DATAFIELD61_NATIONALUSEDATA2_TKN_DATA_BLOCKA("APIERR061.9", "Token Data Block A is incorrect"),
    ERROR_DATAFIELD61_NATIONALUSEDATA2_TKN_DATA_BLOCKA_ID("APIERR061.10", "Token Data Block A ID is incorrect"),
    ERROR_DATAFIELD61_NATIONALUSEDATA2_TKN_DATA_BLOCKB("APIERR061.11", "Token Data Block B is incorrect"),
    ERROR_DATAFIELD61_NATIONALUSEDATA2_TKN_DATA_BLOCKB_ID("APIERR061.12", "Token Data Block B ID is incorrect"),
    ERROR_DATAFIELD62_PRIVATEUSEDATA1("APIERR062", "Private Use Data1 is incorrect"),
    ERROR_DATAFIELD63_PRIVATEUSEDATA2("APIERR063", "Private Use Data2 is incorrect"),
    ERROR_DATAFIELD63_PRIVATEUSEDATA2SERVICEIDENTIFIER("APIERR063.1", "Private use data2 Service Identifier is incorrect"),
    ERROR_DATAFIELD63_PRIVATEUSEDATA2REQUESTTYPEIDENTIFIER("APIERR063.2", "Private use data2 Request Type Identifier is incorrect"),
    ERROR_DATAFIELD63_PRIVATEUSEDATA2CARDMEMBERBILLINGPOSTALCODE("APIERR063.3", "Private use data2 Cardmember Billing Postal Code is incorrect"),
    ERROR_DATAFIELD63_PRIVATEUSEDATA2CARDMEMBERBILLINGADDRESS("APIERR063.4", "Private use data2 Cardmember Billing Address is incorrect"),
    ERROR_DATAFIELD63_PRIVATEUSEDATA2CARDMEMBERFIRSTNAME("APIERR063.5", "Private use data2 Cardmember First Name is incorrect"),
    ERROR_DATAFIELD63_PRIVATEUSEDATA2CARDMEMBERLASTNAME("APIERR063.6", "Private use data2 Cardmember Last Name is incorrect"),
    ERROR_DATAFIELD63_PRIVATEUSEDATA2CARDMEMBERBILLINGPHONENUMBER("APIERR063.7", "Private use data2 Cardmember Billing Phone Number is incorrect"),
    ERROR_DATAFIELD63_PRIVATEUSEDATA2SHIPTOPOSTALCODE("APIERR063.8", "Private use data2 Ship-To Postal Code is incorrect"),
    ERROR_DATAFIELD63_PRIVATEUSEDATA2SHIPTOPOSTALADDRESS("APIERR063.9", "Private use data2 Ship-To Address is incorrect"),
    ERROR_DATAFIELD63_PRIVATEUSEDATA2SHIPTOPOSTALFIRSTNAME("APIERR063.10", "Private use data2 Ship-To First Name is incorrect"),
    ERROR_DATAFIELD63_PRIVATEUSEDATA2SHIPTOPOSTALLASTNAME("APIERR063.11", "Private use data2 Ship-To Last Name is incorrect"),
    ERROR_DATAFIELD63_PRIVATEUSEDATA2SHIPTOPOSTALPHONENUMBER("APIERR063.12", "Private use data2 Ship-To Phone Number is incorrect"),
    ERROR_DATAFIELD63_PRIVATEUSEDATA2SHIPTOPOSTALCOUNTRYCODE("APIERR063.13", "Private use data2 Ship-To Country Code is incorrect"),
    ERROR_DATAFIELD64_MESSAGEAUTHENTICATIONCODEFIELD("APIERR064", "Message Authentication Code Field is incorrect"),
    ERROR_TIMELOCALTRANSACTION("APIERR065", "Time Local Transaction is incorrect"),
    ERROR_TRACK1TRACK2PRESENT("APIERR066", "Track1 Data and Track 2 Data, must not be used in card not present transactions"),
    ERROR_ADDITIONALDATANATIONAL("APIERR067", "Additional Data-National, must be not used in card present transactions"),
    ERROR_MESSAGEEXCEEDS900("APIERR068", "Message exceeds 900 bytes total length"),
    ERROR_TRACK1TRACK2NOTPRESENT("APIERR069", "Track1 Data or Track2 Data must be present"),
    ERROR_TRACK2NOTPRESENT("APIERR070", "Track2 Data must be present"),
    ERROR_TRACK1MUSTNOTPRESENT("APIERR071", "Track1 Data must not be present"),
    ERROR_DATAFIELD518_PRIMARYACCOUNTNUMBER("1112.0", "Primary Account Number is null or empty."),
    ERROR_DATAFIELD519_PRIMARYACCOUNTNUMBER("1112.1", "Primary Account Number is incorrect: if using American Express Traveller cheques then this field must not be set."),
    ERROR_DATAFIELD520_PRIMARYACCOUNTNUMBER("1112.2", "Primary Account Number is other than length <13 or >19."),
    ERROR_DATAFIELD521_PRIMARYACCOUNTNUMBER("1112.3", "Primary Account Number is incorrect: Not Passing Mod 10 Check."),
    ERROR_DATAFIELD522_MESSAGETYPEIDENTIFIER("1111.0", "Message Type identifier is null or empty."),
    ERROR_DATAFIELD523_MESSAGETYPEIDENTIFIER("1111.1", "Message Type identifier is any value other than 1100,1220,1420 and 1804."),
    ERROR_DATAFIELD524_CARDACCEPTORBUSINESSCODE("1126.0", "Card Acceptor Business Code is incorrect: is Null or empty."),
    ERROR_DATAFIELD525_CARDACCEPTORBUSINESSCODE("1126.1", "Card Acceptor Business Code is incorrect: min length(4) maxlength(4)"),
    ERROR_DATAFIELD526_CARDACCEPTORBUSINESSCODE("1126.2", "Card Acceptor Business Code is incorrect: If set any other value : Must be one from the valid mcc codes list given on gcag guide."),
    ERROR_CARDACCEPTORBUSINESSCODE_INCORRECT("APIERR24.1", "Card Acceptor Business Code is incorrect"),
    ERROR_FIELD47FIELD63MISSING("APIERR05518", "Please enter value for either Data Field 47[Additional Data National] or Data Field 63[Information Verification] or Both"),
    ERROR_DATAFIELD96_KEYMANAGEMENTDATAPRIMARYID("APIERR096.1", "Key Management Data : Primary ID is incorrect"),
    ERROR_DATAFIELD96_KEYMANAGEMENTDATASECONDARYID("APIERR096.2", "Key Management Data : Secondary Id is incorrect"),
    ERROR_DATAFIELD96_KEYMANAGEMENTDATASESSIONKEYPINCHECKVALUE("APIERR096.3", "Key Management Data : Session PIN Key Check Value is incorrect"),
    ERROR_DATAFIELD96_KEYMANAGEMENTDATASESSIONKEYMACCHECKVALUE("APIERR096.4", "Key Management Data : Session MAC Key Check Value is incorrect"),
    ERROR_DATAFIELD96_KEYMANAGEMENTDATASESSIONKEYDATACHECKVALUE("APIERR096.5", "Key Management Data : Session Data Key Check Value is incorrect"),
    ERROR_DATAFIELD53_INCORRECTPOSFORSECURITYRELATEDCONTROLINFO("APIERR053.1", "For Security Related Control Information, POS Data Code : Card Data Input Mode should be either Technical fallback,Manually Entered, Swiped transaction with keyed or Magnetic stripe signature with Keyed");

    private final String errorCode;
    private final String errorDescription;

    private AuthErrorCodes(String errorCode, String errorDescription) {
        this.errorCode = errorCode;
        this.errorDescription = errorDescription;
    }

    public String getErrorCode() {
        return this.errorCode;
    }

    public String getErrorDescription() {
        return this.errorDescription;
    }
}
