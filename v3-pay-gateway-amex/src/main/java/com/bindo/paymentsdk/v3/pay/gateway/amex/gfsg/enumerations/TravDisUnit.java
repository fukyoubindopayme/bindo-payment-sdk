package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations;

public enum TravDisUnit {
    Miles("M"),
    Kilometers("K");

    private final String travDisUnit;

    private TravDisUnit(String travDisUnit) {
        this.travDisUnit = travDisUnit;
    }

    public String getTravDisUnit() {
        return this.travDisUnit;
    }
}
