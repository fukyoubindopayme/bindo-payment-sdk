package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.confirmation;

public class ConfirmationRejectedRecordBean
{
  private String returnedaRecordType;
  private String returnedRecordNumber;
  private RejectedRecordDataBean returnedRecordData;
  
  public String getReturnedaRecordType()
  {
    return this.returnedaRecordType;
  }
  
  public void setReturnedaRecordType(String returnedaRecordType)
  {
    this.returnedaRecordType = returnedaRecordType;
  }
  
  public String getReturnedRecordNumber()
  {
    return this.returnedRecordNumber;
  }
  
  public void setReturnedRecordNumber(String returnedRecordNumber)
  {
    this.returnedRecordNumber = returnedRecordNumber;
  }
  
  public RejectedRecordDataBean getReturnedRecordData()
  {
    return this.returnedRecordData;
  }
  
  public void setReturnedRecordData(RejectedRecordDataBean returnedRecordData)
  {
    this.returnedRecordData = returnedRecordData;
  }
}
