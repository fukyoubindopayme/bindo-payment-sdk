package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.RecordType;

public class TransactionAdviceDetailBean
{
  private String recordType = RecordType.Transaction_Advice_Detail.getRecordType();
  private String recordNumber;
  private String transactionIdentifier;
  private String additionalAmountType1;
  private String additionalAmount1;
  private String additionalAmountSign1;
  private String additionalAmountType2;
  private String additionalAmount2;
  private String additionalAmountSign2;
  private String additionalAmountType3;
  private String additionalAmount3;
  private String additionalAmountSign3;
  private String additionalAmountType4;
  private String additionalAmount4;
  private String additionalAmountSign4;
  private String additionalAmountType5;
  private String additionalAmount5 = "";
  private String additionalAmountSign5 = "";
  private String tadDataField5Reserved;
  private String tadDataField9Reserved;
  private String tadDataField13Reserved;
  private String tadDataField17Reserved;
  private String tadDataField21Reserved;
  private String tadDataField24Reserved;
  
  public String getRecordType()
  {
    return this.recordType;
  }
  
  public void setRecordType(String recordType)
  {
    this.recordType = recordType;
  }
  
  public String getRecordNumber()
  {
    return this.recordNumber;
  }
  
  public void setRecordNumber(String recordNumber)
  {
    this.recordNumber = recordNumber;
  }
  
  public String getTransactionIdentifier()
  {
    return this.transactionIdentifier;
  }
  
  public void setTransactionIdentifier(String transactionIdentifier)
  {
    this.transactionIdentifier = transactionIdentifier;
  }
  
  public String getAdditionalAmountType1()
  {
    return this.additionalAmountType1;
  }
  
  public void setAdditionalAmountType1(String additionalAmountType1)
  {
    this.additionalAmountType1 = additionalAmountType1;
  }
  
  public String getAdditionalAmount1()
  {
    return this.additionalAmount1;
  }
  
  public void setAdditionalAmount1(String additionalAmount1)
  {
    this.additionalAmount1 = additionalAmount1;
  }
  
  public String getAdditionalAmountSign1()
  {
    return this.additionalAmountSign1;
  }
  
  public void setAdditionalAmountSign1(String additionalAmountSign1)
  {
    this.additionalAmountSign1 = additionalAmountSign1;
  }
  
  public String getAdditionalAmountType2()
  {
    return this.additionalAmountType2;
  }
  
  public void setAdditionalAmountType2(String additionalAmountType2)
  {
    this.additionalAmountType2 = additionalAmountType2;
  }
  
  public String getAdditionalAmount2()
  {
    return this.additionalAmount2;
  }
  
  public void setAdditionalAmount2(String additionalAmount2)
  {
    this.additionalAmount2 = additionalAmount2;
  }
  
  public String getAdditionalAmountSign2()
  {
    return this.additionalAmountSign2;
  }
  
  public void setAdditionalAmountSign2(String additionalAmountSign2)
  {
    this.additionalAmountSign2 = additionalAmountSign2;
  }
  
  public String getAdditionalAmountType3()
  {
    return this.additionalAmountType3;
  }
  
  public void setAdditionalAmountType3(String additionalAmountType3)
  {
    this.additionalAmountType3 = additionalAmountType3;
  }
  
  public String getAdditionalAmount3()
  {
    return this.additionalAmount3;
  }
  
  public void setAdditionalAmount3(String additionalAmount3)
  {
    this.additionalAmount3 = additionalAmount3;
  }
  
  public String getAdditionalAmountSign3()
  {
    return this.additionalAmountSign3;
  }
  
  public void setAdditionalAmountSign3(String additionalAmountSign3)
  {
    this.additionalAmountSign3 = additionalAmountSign3;
  }
  
  public String getAdditionalAmountType4()
  {
    return this.additionalAmountType4;
  }
  
  public void setAdditionalAmountType4(String additionalAmountType4)
  {
    this.additionalAmountType4 = additionalAmountType4;
  }
  
  public String getAdditionalAmount4()
  {
    return this.additionalAmount4;
  }
  
  public void setAdditionalAmount4(String additionalAmount4)
  {
    this.additionalAmount4 = additionalAmount4;
  }
  
  public String getAdditionalAmountSign4()
  {
    return this.additionalAmountSign4;
  }
  
  public void setAdditionalAmountSign4(String additionalAmountSign4)
  {
    this.additionalAmountSign4 = additionalAmountSign4;
  }
  
  public String getAdditionalAmountType5()
  {
    return this.additionalAmountType5;
  }
  
  public void setAdditionalAmountType5(String additionalAmountType5)
  {
    this.additionalAmountType5 = additionalAmountType5;
  }
  
  public String getAdditionalAmount5()
  {
    return this.additionalAmount5;
  }
  
  public void setAdditionalAmount5(String additionalAmount5)
  {
    this.additionalAmount5 = additionalAmount5;
  }
  
  public String getAdditionalAmountSign5()
  {
    return this.additionalAmountSign5;
  }
  
  public void setAdditionalAmountSign5(String additionalAmountSign5)
  {
    this.additionalAmountSign5 = additionalAmountSign5;
  }
  
  public String getTadDataField5Reserved()
  {
    return this.tadDataField5Reserved;
  }
  
  public void setTadDataField5Reserved(String tadDataField5Reserved)
  {
    this.tadDataField5Reserved = tadDataField5Reserved;
  }
  
  public String getTadDataField9Reserved()
  {
    return this.tadDataField9Reserved;
  }
  
  public void setTadDataField9Reserved(String tadDataField9Reserved)
  {
    this.tadDataField9Reserved = tadDataField9Reserved;
  }
  
  public String getTadDataField13Reserved()
  {
    return this.tadDataField13Reserved;
  }
  
  public void setTadDataField13Reserved(String tadDataField13Reserved)
  {
    this.tadDataField13Reserved = tadDataField13Reserved;
  }
  
  public String getTadDataField17Reserved()
  {
    return this.tadDataField17Reserved;
  }
  
  public void setTadDataField17Reserved(String tadDataField17Reserved)
  {
    this.tadDataField17Reserved = tadDataField17Reserved;
  }
  
  public String getTadDataField21Reserved()
  {
    return this.tadDataField21Reserved;
  }
  
  public void setTadDataField21Reserved(String tadDataField21Reserved)
  {
    this.tadDataField21Reserved = tadDataField21Reserved;
  }
  
  public String getTadDataField24Reserved()
  {
    return this.tadDataField24Reserved;
  }
  
  public void setTadDataField24Reserved(String tadDataField24Reserved)
  {
    this.tadDataField24Reserved = tadDataField24Reserved;
  }
}
