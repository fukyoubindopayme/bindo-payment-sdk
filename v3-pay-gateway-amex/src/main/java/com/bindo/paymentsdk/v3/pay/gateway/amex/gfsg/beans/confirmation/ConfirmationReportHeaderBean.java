package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.confirmation;

public class ConfirmationReportHeaderBean
{
  private String reocrdType;
  private String reportDateAndTime;
  private String reportID;
  private String hdrReserved;
  
  public String getReocrdType()
  {
    return this.reocrdType;
  }
  
  public void setReocrdType(String reocrdType)
  {
    this.reocrdType = reocrdType;
  }
  
  public String getReportDateAndTime()
  {
    return this.reportDateAndTime;
  }
  
  public void setReportDateAndTime(String reportDateAndTime)
  {
    this.reportDateAndTime = reportDateAndTime;
  }
  
  public String getReportID()
  {
    return this.reportID;
  }
  
  public void setReportID(String reportID)
  {
    this.reportID = reportID;
  }
  
  public String getHdrReserved()
  {
    return this.hdrReserved;
  }
  
  public void setHdrReserved(String hdrReserved)
  {
    this.hdrReserved = hdrReserved;
  }
}
