package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.IndustryTypeTAABean;

public class RailIndustryTAABean
  extends IndustryTypeTAABean
{
  private String transactionType;
  private String ticketNumber;
  private String passengerName;
  private String iataCarrierCode;
  private String ticketIssueCity;
  private String ticketIssuerName;
  private String departureStation1;
  private String departureDate1;
  private String arrivalStation1;
  private String departureStation2;
  private String departureDate2;
  private String arrivalStation2;
  private String departureStation3;
  private String departureDate3;
  private String arrivalStation3;
  private String departureStation4;
  private String departureDate4;
  private String arrivalStation4;
  
  public String getTransactionType()
  {
    return this.transactionType;
  }
  
  public void setTransactionType(String transactionType)
  {
    this.transactionType = transactionType;
  }
  
  public String getTicketNumber()
  {
    return this.ticketNumber;
  }
  
  public void setTicketNumber(String ticketNumber)
  {
    this.ticketNumber = ticketNumber;
  }
  
  public String getPassengerName()
  {
    return this.passengerName;
  }
  
  public void setPassengerName(String passengerName)
  {
    this.passengerName = passengerName;
  }
  
  public String getIataCarrierCode()
  {
    return this.iataCarrierCode;
  }
  
  public void setIataCarrierCode(String iataCarrierCode)
  {
    this.iataCarrierCode = iataCarrierCode;
  }
  
  public String getTicketIssueCity()
  {
    return this.ticketIssueCity;
  }
  
  public void setTicketIssueCity(String ticketIssueCity)
  {
    this.ticketIssueCity = ticketIssueCity;
  }
  
  public String getTicketIssuerName()
  {
    return this.ticketIssuerName;
  }
  
  public void setTicketIssuerName(String ticketIssuerName)
  {
    this.ticketIssuerName = ticketIssuerName;
  }
  
  public String getDepartureStation1()
  {
    return this.departureStation1;
  }
  
  public void setDepartureStation1(String departureStation1)
  {
    this.departureStation1 = departureStation1;
  }
  
  public String getDepartureDate1()
  {
    return this.departureDate1;
  }
  
  public void setDepartureDate1(String departureDate1)
  {
    this.departureDate1 = departureDate1;
  }
  
  public String getArrivalStation1()
  {
    return this.arrivalStation1;
  }
  
  public void setArrivalStation1(String arrivalStation1)
  {
    this.arrivalStation1 = arrivalStation1;
  }
  
  public String getDepartureStation2()
  {
    return this.departureStation2;
  }
  
  public void setDepartureStation2(String departureStation2)
  {
    this.departureStation2 = departureStation2;
  }
  
  public String getDepartureDate2()
  {
    return this.departureDate2;
  }
  
  public void setDepartureDate2(String departureDate2)
  {
    this.departureDate2 = departureDate2;
  }
  
  public String getArrivalStation2()
  {
    return this.arrivalStation2;
  }
  
  public void setArrivalStation2(String arrivalStation2)
  {
    this.arrivalStation2 = arrivalStation2;
  }
  
  public String getDepartureStation3()
  {
    return this.departureStation3;
  }
  
  public void setDepartureStation3(String departureStation3)
  {
    this.departureStation3 = departureStation3;
  }
  
  public String getDepartureDate3()
  {
    return this.departureDate3;
  }
  
  public void setDepartureDate3(String departureDate3)
  {
    this.departureDate3 = departureDate3;
  }
  
  public String getArrivalStation3()
  {
    return this.arrivalStation3;
  }
  
  public void setArrivalStation3(String arrivalStation3)
  {
    this.arrivalStation3 = arrivalStation3;
  }
  
  public String getDepartureStation4()
  {
    return this.departureStation4;
  }
  
  public void setDepartureStation4(String departureStation4)
  {
    this.departureStation4 = departureStation4;
  }
  
  public String getDepartureDate4()
  {
    return this.departureDate4;
  }
  
  public void setDepartureDate4(String departureDate4)
  {
    this.departureDate4 = departureDate4;
  }
  
  public String getArrivalStation4()
  {
    return this.arrivalStation4;
  }
  
  public void setArrivalStation4(String arrivalStation4)
  {
    this.arrivalStation4 = arrivalStation4;
  }
}
