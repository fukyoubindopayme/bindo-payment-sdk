package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.ErrorObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionFileHeaderBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.RecordType;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.SubmissionErrorCodes;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.exceptions.SettlementException;
import java.util.List;

public class TFHRecValidator
{
  private static String recordNumber;
  
  public static void validateTFHRecord(TransactionFileHeaderBean transactionFileHeaderType, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    recordNumber = transactionFileHeaderType.getRecordNumber();
    validateRecordType(transactionFileHeaderType.getRecordType(), 
      errorCodes);
    validateRecordNumber(transactionFileHeaderType.getRecordNumber(), 
      errorCodes);
    validateSubmitterId(transactionFileHeaderType.getSubmitterId(), 
      errorCodes);
    validateSubmitterFileReferenceNumber(transactionFileHeaderType
      .getSubmitterFileReferenceNumber(), errorCodes);
    validateSubmitterFileSequenceNumber(transactionFileHeaderType
      .getSubmitterFileSequenceNumber(), errorCodes);
    validateFileCreationDate(transactionFileHeaderType
      .getFileCreationDate(), errorCodes);
    validateFileCreationTime(transactionFileHeaderType
      .getFileCreationTime(), errorCodes);
    validateFileVersionNumber(transactionFileHeaderType
      .getFileVersionNumber(), errorCodes);
  }
  
  private static void validateRecordType(String recordType, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(recordType))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE
        .getErrorDescription() + 
        "\n" + 
        "TFH" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "RecordType:" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (!RecordType.Transaction_File_Header.getRecordType().equalsIgnoreCase(recordType))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateRecordNumber(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD585_RECORDNUMBER
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD585_RECORDNUMBER
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TFH" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "RecordNumber:" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[2]))
    {
      String reqLength = CommonValidator.validateLength(value, 8, 8);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD3_RECORDNUMBER
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD3_RECORDNUMBER
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          "TFH" + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "RecordNumber:" + 
          "|" + 
          "This field length Cannot be greater than 8");
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD2_RECORDNUMBER
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD2_RECORDNUMBER
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TFH" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "RecordNumber:" + 
        "|" + 
        "This field can only be Numeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateSubmitterId(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD604_SUBMITTERID
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD604_SUBMITTERID
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TFH" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "SubmitterId" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[3]))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 11, 11);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
        
          SubmissionErrorCodes.ERROR_DATAFIELD606_SUBMITTERID
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD606_SUBMITTERID
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          "TFH" + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "SubmitterId" + 
          "|" + 
          "This field length Cannot be greater than 11");
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
      
        SubmissionErrorCodes.ERROR_DATAFIELD605_SUBMITTERID
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD605_SUBMITTERID
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TFH" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "SubmitterId" + 
        "|" + 
        "This field can only be AlphaNumeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateSubmitterFileReferenceNumber(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD6_SUBMITTERFILE_REFERENCENUMBER
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD6_SUBMITTERFILE_REFERENCENUMBER
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TFH" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "Submitter File Reference Number:" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else
    {
      String reqLength = CommonValidator.validateLength(value, 9, 9);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
        
          SubmissionErrorCodes.ERROR_DATAFIELD612_SUBMITTERFILE_REFERENCENUMBER
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD612_SUBMITTERFILE_REFERENCENUMBER
          
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          "TFH" + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "Submitter File Reference Number:" + 
          "|" + 
          "This field length Cannot be greater than 9");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateSubmitterFileSequenceNumber(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[6]))
      {
        String reqLength = CommonValidator.validateLength(value, 9, 9);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
          
            SubmissionErrorCodes.ERROR_DATAFIELD607_SUBMITTERFILE_SEQUENCENUMBER
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD607_SUBMITTERFILE_SEQUENCENUMBER
            
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            "TFH" + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "SubmitterFileSequenceNumber:" + 
            "|" + 
            "This field length Cannot be greater than 9");
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
        
          SubmissionErrorCodes.ERROR_DATAFIELD608_SUBMITTERFILE_SEQUENCENUMBER
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD608_SUBMITTERFILE_SEQUENCENUMBER
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          "TFH" + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "SubmitterFileSequenceNumber:" + 
          "|" + 
          "This field can only be Numeric");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateFileCreationDate(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD11_FILECREATION_DATE
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD11_FILECREATION_DATE
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TFH" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "FileCreationDate" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[7]))
    {
      String reqLength = CommonValidator.validateLength(value, 8, 8);
      if (reqLength.equals("equal")) {
        if (CommonValidator.isValidDate(value, "CCYYMMDD"))
        {
          String dateStatus = CommonValidator.validateDate(value);
          if (dateStatus.equals("expired"))
          {
            errorObj = new ErrorObject(
              SubmissionErrorCodes.ERROR_DATAFIELD13_FILECREATIONDATE
              .getErrorCode(), 
              SubmissionErrorCodes.ERROR_DATAFIELD13_FILECREATIONDATE
              .getErrorDescription() + 
              "\n" + 
              "RecordType:" + 
              "TFH" + 
              "|" + 
              "RecordNumber:" + 
              recordNumber + 
              "|" + 
              "FileCreationDate" + 
              "|" + 
              "File Creation Date too many days in past");
            errorCodes.add(errorObj);
            return;
          }
          if (!dateStatus.equals("futureDate")) {
            return;
          }
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD12_FILECREATION_DATE
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD12_FILECREATION_DATE
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            "TFH" + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "FileCreationDate" + 
            "|" + 
            "File Creation Date too many days in future");
          errorCodes.add(errorObj);
          
          return;
        }
      }
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD592_FILECREATION_DATE
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD592_FILECREATION_DATE
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TFH" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "FileCreationDate" + 
        "|" + 
        "This field length Cannot be greater than 8");
      errorCodes.add(errorObj);
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD591_FILECREATION_DATE
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD591_FILECREATION_DATE
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TFH" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "FileCreationDate" + 
        "|" + 
        "This field can only be Numeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateFileCreationTime(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (!CommonValidator.isNullOrEmpty(value)) {
      if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[8]))
      {
        String reqLength = CommonValidator.validateLength(value, 6, 6);
        if (reqLength.equals("greaterThanMax"))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD14_FILECREATION_TIME
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD14_FILECREATION_TIME
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            "TFH" + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "FileCreationTime" + 
            "|" + 
            "This field length Cannot be greater than 6");
          errorCodes.add(errorObj);
        }
        else if (!CommonValidator.validateTime(value))
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD14_FILECREATION_TIME
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD14_FILECREATION_TIME
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            "TFH" + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "FileCreationTime" + 
            "|" + 
            SubmissionErrorCodes.ERROR_DATAFIELD14_FILECREATION_TIME
            .getErrorDescription());
          errorCodes.add(errorObj);
        }
      }
      else
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD593_FILECREATION_TIME
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD593_FILECREATION_TIME
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          "TFH" + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "FileCreationTime" + 
          "|" + 
          "This field can only be Numeric");
        errorCodes.add(errorObj);
      }
    }
  }
  
  private static void validateFileVersionNumber(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
      
        SubmissionErrorCodes.ERROR_DATAFIELD609_FILEVERSION_NUMBER
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD609_FILEVERSION_NUMBER
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TFH" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "FileVersionNumber" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[9]))
    {
      String reqLength = CommonValidator.validateLength(value, 8, 8);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
        
          SubmissionErrorCodes.ERROR_DATAFIELD611_FILEVERSION_NUMBER
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD611_FILEVERSION_NUMBER
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          "TFH" + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "FileVersionNumber" + 
          "|" + 
          "This field length Cannot be greater than 8");
        errorCodes.add(errorObj);
      }
      else if (reqLength.equals("lessThanMin"))
      {
        errorObj = new ErrorObject(
        
          SubmissionErrorCodes.ERROR_DATAFIELD611_FILEVERSION_NUMBER
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD611_FILEVERSION_NUMBER
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          "TFH" + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "FileVersionNumber" + 
          "|" + 
          "This field length Cannot be less than 8");
        errorCodes.add(errorObj);
      }
      else if (!value.equals("12010000"))
      {
        errorObj = new ErrorObject(
        
          SubmissionErrorCodes.ERROR_DATAFIELD611_FILEVERSION_NUMBER
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD611_FILEVERSION_NUMBER
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          "TFH" + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "FileVersionNumber" + 
          "|" + 
          "This field is missing or invalid");
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
      
        SubmissionErrorCodes.ERROR_DATAFIELD610_FILEVERSION_NUMBER
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD610_FILEVERSION_NUMBER
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TFH" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "FileVersionNumber" + 
        "|" + 
        "This field can only be AlphaNumeric");
      errorCodes.add(errorObj);
    }
  }
}
