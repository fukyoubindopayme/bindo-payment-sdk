package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.ErrorObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.SettlementRequestDataObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceBasicBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionFileSummaryBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionTBTSpecificBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.ProcessingCode;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.RecordType;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.SubmissionErrorCodes;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.exceptions.SettlementException;
import java.util.Iterator;
import java.util.List;

public class TFSRecValidator
{
  private static String recordNumber;
  
  public static void validateTFSRecord(TransactionFileSummaryBean transactionFileSummaryType, SettlementRequestDataObject settlementReqBean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    recordNumber = transactionFileSummaryType.getRecordNumber();
    validateRecordType(transactionFileSummaryType.getRecordType(), 
      errorCodes);
    validateRecordNumber(transactionFileSummaryType.getRecordNumber(), 
      errorCodes);
    validateNumberOfDebits(transactionFileSummaryType.getNumberOfDebits(), 
      settlementReqBean, errorCodes);
    validateHashTotalDebitAmount(transactionFileSummaryType
      .getHashTotalDebitAmount(), settlementReqBean, errorCodes);
    validateNumberOfCredits(
      transactionFileSummaryType.getNumberOfCredits(), 
      settlementReqBean, errorCodes);
    validateHashTotalCreditAmount(transactionFileSummaryType
      .getHashTotalCreditAmount(), settlementReqBean, errorCodes);
    validateHashTotalAmount(
      transactionFileSummaryType.getHashTotalAmount(), 
      settlementReqBean, errorCodes);
  }
  
  private static void validateRecordType(String recordType, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(recordType))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TFS" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "RecordNumber:" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (!RecordType.Transaction_File_Summary.getRecordType().equalsIgnoreCase(recordType))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD1_RECORDTYPE);
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateRecordNumber(String value, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD585_RECORDNUMBER
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD585_RECORDNUMBER
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TFS" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "RecordNumber:" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[11]))
    {
      String reqLength = CommonValidator.validateLength(value, 8, 8);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD3_RECORDNUMBER
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD3_RECORDNUMBER
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          "TFS" + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "RecordNumber:" + 
          "|" + 
          "This field length Cannot be greater than 8");
        errorCodes.add(errorObj);
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD2_RECORDNUMBER
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD2_RECORDNUMBER
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TFS" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "RecordNumber:" + 
        "|" + 
        "This field can only be Numeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateNumberOfDebits(String value, SettlementRequestDataObject settlementReqBean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD594_NUMBEROF_DEBITS
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD594_NUMBEROF_DEBITS
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TFS" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "numberOfDebits" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[12]))
    {
      String reqLength = CommonValidator.validateLength(value, 8, 8);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD016_NUMBEROF_DEBITS
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD016_NUMBEROF_DEBITS
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          "TFS" + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "numberOfDebits" + 
          "|" + 
          "This field length Cannot be greater than 8");
        errorCodes.add(errorObj);
      }
      else
      {
        int totTABDebits = noOfTABDebits(settlementReqBean);
        int noOfDebits = Integer.parseInt(value);
        if (totTABDebits != noOfDebits)
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD16_NUMBEROF_DEBITS
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD16_NUMBEROF_DEBITS
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            "TFS" + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "numberOfDebits" + 
            "|" + 
            SubmissionErrorCodes.ERROR_DATAFIELD16_NUMBEROF_DEBITS
            .getErrorDescription());
          errorCodes.add(errorObj);
        }
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD15_NUMBEROF_DEBITS
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD15_NUMBEROF_DEBITS
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TFS" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "numberOfDebits" + 
        "|" + 
        "This field can only be Numeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateHashTotalDebitAmount(String value, SettlementRequestDataObject settlementReqBean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD18_HASH_TOTALDEBITAMOUNT
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD18_HASH_TOTALDEBITAMOUNT
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TFS" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "Hash Total Debit Amount" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[14]))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 20, 20);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD18_HASH_TOTALDEBITAMOUNT
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD18_HASH_TOTALDEBITAMOUNT
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          "TFS" + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "Hash Total Debit Amount" + 
          "|" + 
          "This field length Cannot be greater than 20");
        errorCodes.add(errorObj);
      }
      else
      {
        long tabDbtAmt = totalDebitAmount(settlementReqBean);
        long hasTotDbtAmt = Long.parseLong(value);
        if (tabDbtAmt != hasTotDbtAmt)
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD18_HASH_TOTALDEBITAMOUNT
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD18_HASH_TOTALDEBITAMOUNT
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            "TFS" + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "Hash Total Debit Amount" + 
            "|" + 
            SubmissionErrorCodes.ERROR_DATAFIELD18_HASH_TOTALDEBITAMOUNT
            .getErrorDescription());
          errorCodes.add(errorObj);
        }
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD17_HASH_TOTALDEBITAMOUNT
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD17_HASH_TOTALDEBITAMOUNT
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TFS" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "Hash Total Debit Amount" + 
        "|" + 
        "This field can only be Numeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateNumberOfCredits(String value, SettlementRequestDataObject settlementReqBean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD20_NUMBEROFCREDITS
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD20_NUMBEROFCREDITS
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TFS" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "numberOfCredits" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[15]))
    {
      String reqLength = CommonValidator.validateLength(value, 8, 8);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD20_NUMBEROFCREDITS
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD20_NUMBEROFCREDITS
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          "TFS" + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "numberOfCredits" + 
          "|" + 
          "This field length Cannot be greater than 8");
        errorCodes.add(errorObj);
      }
      else
      {
        int tabCredits = noOfTABCredits(settlementReqBean);
        int noOfCredits = Integer.parseInt(value);
        if (tabCredits != noOfCredits)
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD20_NUMBEROFCREDITS
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD20_NUMBEROFCREDITS
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            "TFS" + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "numberOfCredits" + 
            "|" + 
            SubmissionErrorCodes.ERROR_DATAFIELD20_NUMBEROFCREDITS
            .getErrorDescription());
          errorCodes.add(errorObj);
        }
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD19_NUMBEROFCREDITS
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD19_NUMBEROFCREDITS
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TFS" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "numberOfCredits" + 
        "|" + 
        "This field can only be Numeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateHashTotalCreditAmount(String value, SettlementRequestDataObject settlementReqBean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD22_HASHTOTALCREDITAMOUNT
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD22_HASHTOTALCREDITAMOUNT
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TFS" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "Hash Total Credit Amount" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[17]))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 20, 20);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD21_HASHTOTALCREDITAMOUNT
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD21_HASHTOTALCREDITAMOUNT
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          "TFS" + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "Hash Total Credit Amount" + 
          "|" + 
          "This field length Cannot be greater than 20");
        errorCodes.add(errorObj);
      }
      else
      {
        long tabCreditAmt = totalCreditAmount(settlementReqBean);
        long hsahCreditAmt = Long.parseLong(value);
        if (tabCreditAmt != hsahCreditAmt)
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD22_HASHTOTALCREDITAMOUNT
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD22_HASHTOTALCREDITAMOUNT
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            "TFS" + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "Hash Total Credit Amount" + 
            "|" + 
            SubmissionErrorCodes.ERROR_DATAFIELD22_HASHTOTALCREDITAMOUNT
            .getErrorDescription());
          errorCodes.add(errorObj);
        }
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD21_HASHTOTALCREDITAMOUNT
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD21_HASHTOTALCREDITAMOUNT
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TFS" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "Hash Total Credit Amount" + 
        "|" + 
        "This field can only be Numeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static void validateHashTotalAmount(String value, SettlementRequestDataObject settlementReqBean, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    ErrorObject errorObj = null;
    if (CommonValidator.isNullOrEmpty(value))
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD24_HASH_TOTAL_AMOUNT
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD24_HASH_TOTAL_AMOUNT
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TFS" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "HashTotalAmount" + 
        "|" + 
        "This field is mandatory and cannot be empty");
      errorCodes.add(errorObj);
    }
    else if (CommonValidator.validateData(value, com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.constants.SubmissionConstants.RECORD_VALUE[19]))
    {
      String reqLength = 
        CommonValidator.validateLength(value, 20, 20);
      if (reqLength.equals("greaterThanMax"))
      {
        errorObj = new ErrorObject(
          SubmissionErrorCodes.ERROR_DATAFIELD23_HASH_TOTAL_AMOUNT
          .getErrorCode(), 
          SubmissionErrorCodes.ERROR_DATAFIELD23_HASH_TOTAL_AMOUNT
          .getErrorDescription() + 
          "\n" + 
          "RecordType:" + 
          "TFS" + 
          "|" + 
          "RecordNumber:" + 
          recordNumber + 
          "|" + 
          "HashTotalAmount" + 
          "|" + 
          "This field length Cannot be greater than 20");
        errorCodes.add(errorObj);
      }
      else
      {
        long totTabAmount = totalTabAmount(settlementReqBean);
        long hashTotalAmt = Long.parseLong(value);
        if (hashTotalAmt != totTabAmount)
        {
          errorObj = new ErrorObject(
            SubmissionErrorCodes.ERROR_DATAFIELD24_HASH_TOTAL_AMOUNT
            .getErrorCode(), 
            SubmissionErrorCodes.ERROR_DATAFIELD24_HASH_TOTAL_AMOUNT
            .getErrorDescription() + 
            "\n" + 
            "RecordType:" + 
            "TFS" + 
            "|" + 
            "RecordNumber:" + 
            recordNumber + 
            "|" + 
            "HashTotalAmount" + 
            "|" + 
            SubmissionErrorCodes.ERROR_DATAFIELD24_HASH_TOTAL_AMOUNT
            .getErrorDescription());
          errorCodes.add(errorObj);
        }
      }
    }
    else
    {
      errorObj = new ErrorObject(
        SubmissionErrorCodes.ERROR_DATAFIELD23_HASH_TOTAL_AMOUNT
        .getErrorCode(), 
        SubmissionErrorCodes.ERROR_DATAFIELD23_HASH_TOTAL_AMOUNT
        .getErrorDescription() + 
        "\n" + 
        "RecordType:" + 
        "TFS" + 
        "|" + 
        "RecordNumber:" + 
        recordNumber + 
        "|" + 
        "HashTotalAmount" + 
        "|" + 
        "This field can only be Numeric");
      errorCodes.add(errorObj);
    }
  }
  
  private static int noOfTABDebits(SettlementRequestDataObject settlementReqBean)
  {
    int totTABDebits = 0;
    if (!settlementReqBean.getTransactionTBTSpecificType().isEmpty())
    {
      Iterator localIterator1 = settlementReqBean.getTransactionTBTSpecificType().iterator();
      while (localIterator1.hasNext())
      {
        TransactionTBTSpecificBean transactionTBTSpecificType = (TransactionTBTSpecificBean)localIterator1.next();
        if (!transactionTBTSpecificType.getTransactionAdviceBasicType().isEmpty())
        {
          Iterator localIterator2 = transactionTBTSpecificType.getTransactionAdviceBasicType().iterator();
          while (localIterator2.hasNext())
          {
            TransactionAdviceBasicBean transactionAdviceBasicType = (TransactionAdviceBasicBean)localIterator2.next();
            if ((ProcessingCode.DEBIT.getProcessingCode().equalsIgnoreCase(transactionAdviceBasicType.getProcessingCode())) || 
            
              (ProcessingCode.CREDIT_REVERSALS.getProcessingCode().equalsIgnoreCase(transactionAdviceBasicType.getProcessingCode())) || 
              
              (ProcessingCode.BONUS_PAY_DEBIT.getProcessingCode().equalsIgnoreCase(transactionAdviceBasicType.getProcessingCode())) || 
              
              (ProcessingCode.BONUS_PAY_CREDIT_REVERSAL.getProcessingCode().equalsIgnoreCase(transactionAdviceBasicType.getProcessingCode()))) {
              totTABDebits++;
            }
          }
        }
      }
    }
    return totTABDebits;
  }
  
  private static int noOfTABCredits(SettlementRequestDataObject settlementReqBean)
  {
    int totTABCredits = 0;
    if (!settlementReqBean.getTransactionTBTSpecificType().isEmpty())
    {
      Iterator localIterator1 = settlementReqBean.getTransactionTBTSpecificType().iterator();
      while (localIterator1.hasNext())
      {
        TransactionTBTSpecificBean transactionTBTSpecificType = (TransactionTBTSpecificBean)localIterator1.next();
        if (!transactionTBTSpecificType.getTransactionAdviceBasicType().isEmpty())
        {
          Iterator localIterator2 = transactionTBTSpecificType.getTransactionAdviceBasicType().iterator();
          while (localIterator2.hasNext())
          {
            TransactionAdviceBasicBean transactionAdviceBasicType = (TransactionAdviceBasicBean)localIterator2.next();
            if ((ProcessingCode.CREDIT.getProcessingCode().equalsIgnoreCase(transactionAdviceBasicType.getProcessingCode())) || 
            
              (ProcessingCode.DEBIT_REVERSALS.getProcessingCode().equalsIgnoreCase(transactionAdviceBasicType.getProcessingCode())) || 
              
              (ProcessingCode.BONUS_PAY_CREDIT.getProcessingCode().equalsIgnoreCase(transactionAdviceBasicType.getProcessingCode())) || 
              
              (ProcessingCode.BONUS_PAY_DEBIT_REVERSAL.getProcessingCode().equalsIgnoreCase(transactionAdviceBasicType.getProcessingCode()))) {
              totTABCredits++;
            }
          }
        }
      }
    }
    return totTABCredits;
  }
  
  private static long totalDebitAmount(SettlementRequestDataObject settlementReqBean)
  {
    long totalDbtAmt = 0L;
    if (!settlementReqBean.getTransactionTBTSpecificType().isEmpty())
    {
      Iterator localIterator1 = settlementReqBean.getTransactionTBTSpecificType().iterator();
      while (localIterator1.hasNext())
      {
        TransactionTBTSpecificBean transactionTBTSpecificType = (TransactionTBTSpecificBean)localIterator1.next();
        if (!transactionTBTSpecificType.getTransactionAdviceBasicType().isEmpty())
        {
          Iterator localIterator2 = transactionTBTSpecificType.getTransactionAdviceBasicType().iterator();
          while (localIterator2.hasNext())
          {
            TransactionAdviceBasicBean transactionAdviceBasicType = (TransactionAdviceBasicBean)localIterator2.next();
            if ((ProcessingCode.DEBIT.getProcessingCode().equalsIgnoreCase(transactionAdviceBasicType.getProcessingCode())) || 
            
              (ProcessingCode.CREDIT_REVERSALS.getProcessingCode().equalsIgnoreCase(transactionAdviceBasicType.getProcessingCode())) || 
              
              (ProcessingCode.BONUS_PAY_DEBIT.getProcessingCode().equalsIgnoreCase(transactionAdviceBasicType.getProcessingCode())) || 
              
              (ProcessingCode.BONUS_PAY_CREDIT_REVERSAL.getProcessingCode().equalsIgnoreCase(transactionAdviceBasicType.getProcessingCode()))) {
              totalDbtAmt = totalDbtAmt + Long.parseLong(transactionAdviceBasicType
                .getTransactionAmount());
            }
          }
        }
      }
    }
    return totalDbtAmt;
  }
  
  private static long totalCreditAmount(SettlementRequestDataObject settlementReqBean)
  {
    long totalCreditAmt = 0L;
    if (!settlementReqBean.getTransactionTBTSpecificType().isEmpty())
    {
      Iterator localIterator1 = settlementReqBean.getTransactionTBTSpecificType().iterator();
      while (localIterator1.hasNext())
      {
        TransactionTBTSpecificBean transactionTBTSpecificType = (TransactionTBTSpecificBean)localIterator1.next();
        if (!transactionTBTSpecificType.getTransactionAdviceBasicType().isEmpty())
        {
          Iterator localIterator2 = transactionTBTSpecificType.getTransactionAdviceBasicType().iterator();
          while (localIterator2.hasNext())
          {
            TransactionAdviceBasicBean transactionAdviceBasicType = (TransactionAdviceBasicBean)localIterator2.next();
            if ((ProcessingCode.CREDIT.getProcessingCode().equalsIgnoreCase(transactionAdviceBasicType.getProcessingCode())) || 
            
              (ProcessingCode.DEBIT_REVERSALS.getProcessingCode().equalsIgnoreCase(transactionAdviceBasicType.getProcessingCode())) || 
              
              (ProcessingCode.BONUS_PAY_CREDIT.getProcessingCode().equalsIgnoreCase(transactionAdviceBasicType.getProcessingCode())) || 
              
              (ProcessingCode.BONUS_PAY_DEBIT_REVERSAL.getProcessingCode().equalsIgnoreCase(transactionAdviceBasicType.getProcessingCode()))) {
              totalCreditAmt = totalCreditAmt + Long.parseLong(transactionAdviceBasicType
                .getTransactionAmount());
            }
          }
        }
      }
    }
    return totalCreditAmt;
  }
  
  private static long totalTabAmount(SettlementRequestDataObject settlementReqBean)
  {
    long totalAmt = 0L;
    if (!settlementReqBean.getTransactionTBTSpecificType().isEmpty())
    {
      Iterator localIterator1 = settlementReqBean.getTransactionTBTSpecificType().iterator();
      while (localIterator1.hasNext())
      {
        TransactionTBTSpecificBean transactionTBTSpecificType = (TransactionTBTSpecificBean)localIterator1.next();
        if (!transactionTBTSpecificType.getTransactionAdviceBasicType().isEmpty())
        {
          Iterator localIterator2 = transactionTBTSpecificType.getTransactionAdviceBasicType().iterator();
          while (localIterator2.hasNext())
          {
            TransactionAdviceBasicBean transactionAdviceBasicType = (TransactionAdviceBasicBean)localIterator2.next();
            
            totalAmt = totalAmt + Long.parseLong(transactionAdviceBasicType
              .getTransactionAmount());
          }
        }
      }
    }
    return totalAmt;
  }
}
