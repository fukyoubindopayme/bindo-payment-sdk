package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.processor;

import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.connection.HTTPSConnectionInfo;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.connection.PropertyReader;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.exception.IPSAPIException;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.exception.SSLConnectionException;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.header.RequestHeader;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.messageformatter.AuthorizationMessageFormatter;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.utils.ISOUtil;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.Properties;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
//import org.apache.commons.codec.binary.Base64;

public class PaymentSDKRequestProcessor {
    private String endPointUrl = null;
    private Properties requestProp = null;
    private Properties inputProp = null;
    private URL callingURL = null;
    private String payload = null;
    private HttpsURLConnection httpsconn = null;
    private HttpURLConnection httpconn = null;
    private String serverRespCode = null;
    boolean debugFlag = PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON");
    AuthorizationMessageFormatter authMesssageFormatter = new AuthorizationMessageFormatter();

    public PaymentSDKRequestProcessor() {
    }

    public final void createHTTPConnection(HTTPSConnectionInfo httpsConnectionInfo) throws IOException, IPSAPIException, SSLConnectionException {
        if (this.debugFlag) {
            this.debugFlag = true;
            Log.i("Entered into createHTTPConnection()");
        }

        try {
            if (httpsConnectionInfo != null && httpsConnectionInfo.getUri() != null) {
                this.endPointUrl = httpsConnectionInfo.getUri();
                if (this.debugFlag) {
                    Log.i("Connection Setting details\r\n--------------------------------------------------------------------------------------------------\r\n-                                 Connection Setting details                                     -\r\n-------------------------------------------------------------------------------------------------\r\n" + httpsConnectionInfo + "\r\n" + "-------------------------------------------------------------------------------------------------");
                }

                this.callingURL = new URL(this.endPointUrl);
                if (httpsConnectionInfo.getProxy() != null && httpsConnectionInfo.getProxy().trim().equalsIgnoreCase("YES")) {
                    if (this.debugFlag) {
                        Log.i("Sending request through proxy :");
                    }

                    System.getProperties().put("proxySet", "true");
                    if (httpsConnectionInfo != null && httpsConnectionInfo.getProxyAddress() != null && !httpsConnectionInfo.getProxyAddress().trim().equals("")) {
                        System.getProperties().put("proxyHost", httpsConnectionInfo.getProxyAddress());
                    }

                    if (httpsConnectionInfo != null && httpsConnectionInfo.getPort() != null && !httpsConnectionInfo.getPort().trim().equals("")) {
                        System.getProperties().put("proxyPort", httpsConnectionInfo.getPort());
                    }
                }

                this.httpconn = (HttpURLConnection)this.callingURL.openConnection();
                if (!ISOUtil.IsNullOrEmpty(PropertyReader.getHttpsTimeout())) {
                    this.httpconn.setConnectTimeout(new Integer(PropertyReader.getHttpsTimeout()));
                }

                if (!ISOUtil.IsNullOrEmpty(PropertyReader.getReadTimeout())) {
                    this.httpconn.setReadTimeout(new Integer(PropertyReader.getReadTimeout()));
                }

                if (this.debugFlag) {
                    this.debugFlag = true;
                    Log.i("Http connection is Created");
                }

                if (httpsConnectionInfo.getProxy() != null && httpsConnectionInfo.getProxy().trim().equalsIgnoreCase("YES")) {
                    if (this.debugFlag) {
                        Log.i("Setting the User-Agent,Host,Proxy-Authorization");
                    }

                    if (httpsConnectionInfo != null && httpsConnectionInfo.getUserAgent() != null && !httpsConnectionInfo.getUserAgent().trim().equals("")) {
                        this.httpconn.setRequestProperty("User-Agent", httpsConnectionInfo.getUserAgent());
                    }

                    if (httpsConnectionInfo != null && httpsConnectionInfo.getHost() != null && !httpsConnectionInfo.getHost().trim().equals("")) {
                        this.httpconn.setRequestProperty("Host", httpsConnectionInfo.getHost());
                    }

                    if (httpsConnectionInfo != null && httpsConnectionInfo.getUserId() != null && httpsConnectionInfo.getPassword() != null && !httpsConnectionInfo.getUserId().trim().equals("") && !httpsConnectionInfo.getPassword().trim().equals("")) {
                        String proxyAuthentication = "";
                        if (!AuthorizationMessageFormatter.IsNullOrEmpty(httpsConnectionInfo.getProxyAuthentication())) {
                            proxyAuthentication = httpsConnectionInfo.getProxyAuthentication();
                        }

                        String authentication = proxyAuthentication + " ";// + new String(Base64.encodeBase64((httpsConnectionInfo.getUserId() + ":" + httpsConnectionInfo.getPassword()).getBytes()));
                        this.httpconn.setRequestProperty("WWW-Authenticate", authentication);
                    }
                }
            }
        } catch (SocketTimeoutException var9) {
            if (this.debugFlag) {
                Log.e(" Connection timeout in createHTTPConnection() method.. EXCEPTION : " + var9.getMessage());
            }

            throw new SSLConnectionException(var9.getMessage(), var9);
        } catch (IOException var10) {
            if (this.debugFlag) {
                Log.e("Error in createHTTPConnection() method.. EXCEPTION : " + var10.getMessage());
            }

            throw new SSLConnectionException(var10.getMessage(), var10);
        } catch (Exception var11) {
            if (this.debugFlag) {
                Log.e("Error in createHTTPConnection() method..  EXCEPTION : " + var11);
            }

            throw new IPSAPIException(var11.getMessage(), var11);
        } finally {
            this.httpconn.disconnect();
        }

    }

    public final void createHTTPsConnection(HTTPSConnectionInfo httpsConnectionInfo) throws IOException, IPSAPIException, SSLConnectionException {
        if (this.debugFlag) {
            this.debugFlag = true;
            Log.i("Entered into createHTTPsConnection()");
        }

        try {
            if (httpsConnectionInfo != null && httpsConnectionInfo.getUri() != null) {
                this.endPointUrl = httpsConnectionInfo.getUri();
                if (this.debugFlag) {
                    Log.i("Connection Setting details\r\n--------------------------------------------------------------------------------------------------\r\n-                                 Connection Setting details                                     -\r\n-------------------------------------------------------------------------------------------------\r\n" + httpsConnectionInfo + "\r\n" + "-------------------------------------------------------------------------------------------------");
                }

                this.callingURL = new URL(this.endPointUrl);
                this.trustAllHttpsCertificates();
                if (httpsConnectionInfo.getProxy() != null && httpsConnectionInfo.getProxy().trim().equalsIgnoreCase("YES")) {
                    if (this.debugFlag) {
                        Log.i("Sending request through proxy :");
                    }

                    System.getProperties().put("proxySet", "true");
                    if (httpsConnectionInfo != null && httpsConnectionInfo.getProxyAddress() != null && !httpsConnectionInfo.getProxyAddress().trim().equals("")) {
                        System.getProperties().put("proxyHost", httpsConnectionInfo.getProxyAddress());
                    }

                    if (httpsConnectionInfo != null && httpsConnectionInfo.getPort() != null && !httpsConnectionInfo.getPort().trim().equals("")) {
                        System.getProperties().put("proxyPort", httpsConnectionInfo.getPort());
                    }
                }

                this.httpsconn = (HttpsURLConnection)this.callingURL.openConnection();
                if (!ISOUtil.IsNullOrEmpty(PropertyReader.getHttpsTimeout())) {
                    this.httpsconn.setConnectTimeout(new Integer(PropertyReader.getHttpsTimeout()));
                }

                if (!ISOUtil.IsNullOrEmpty(PropertyReader.getReadTimeout())) {
                    this.httpsconn.setReadTimeout(new Integer(PropertyReader.getReadTimeout()));
                }

                if (this.debugFlag) {
                    this.debugFlag = true;
                    Log.i("Https connection is Created");
                }

                if (httpsConnectionInfo.getProxy() != null && httpsConnectionInfo.getProxy().trim().equalsIgnoreCase("YES")) {
                    if (this.debugFlag) {
                        Log.i("Setting the User-Agent,Host,Proxy-Authorization");
                    }

                    if (httpsConnectionInfo != null && httpsConnectionInfo.getUserAgent() != null && !httpsConnectionInfo.getUserAgent().trim().equals("")) {
                        this.httpsconn.setRequestProperty("User-Agent", httpsConnectionInfo.getUserAgent());
                    }

                    if (httpsConnectionInfo != null && httpsConnectionInfo.getHost() != null && !httpsConnectionInfo.getHost().trim().equals("")) {
                        this.httpsconn.setRequestProperty("Host", httpsConnectionInfo.getHost());
                    }

                    if (httpsConnectionInfo != null && httpsConnectionInfo.getUserId() != null && httpsConnectionInfo.getPassword() != null && !httpsConnectionInfo.getUserId().trim().equals("") && !httpsConnectionInfo.getPassword().trim().equals("")) {
                        String proxyAuthentication = "";
                        if (!AuthorizationMessageFormatter.IsNullOrEmpty(httpsConnectionInfo.getProxyAuthentication())) {
                            proxyAuthentication = httpsConnectionInfo.getProxyAuthentication();
                        }

                        String authentication = proxyAuthentication + " ";// + new String(Base64.encodeBase64((httpsConnectionInfo.getUserId() + ":" + httpsConnectionInfo.getPassword()).getBytes()));
                        this.httpsconn.setRequestProperty("WWW-Authenticate", authentication);
                    }
                }
            }
        } catch (SocketTimeoutException var9) {
            if (this.debugFlag) {
                Log.e(" Connection timeout in createHTTPsConnection() method.. EXCEPTION : " + var9.getMessage());
            }

            throw new SSLConnectionException(var9.getMessage(), var9);
        } catch (IOException var10) {
            if (this.debugFlag) {
                Log.e("Error in createHTTPsConnection() method.. EXCEPTION : " + var10.getMessage());
            }

            throw new SSLConnectionException(var10.getMessage(), var10);
        } catch (Exception var11) {
            if (this.debugFlag) {
                Log.e("Error in createHTTPsConnection() method..  EXCEPTION : " + var11);
            }

            throw new IPSAPIException(var11.getMessage(), var11);
        } finally {
            this.httpsconn.disconnect();
        }

    }

    public final String sendHttpsRequest(String gcagReqString, RequestHeader requestHeader) throws Exception {
        if (this.debugFlag) {
            Log.i("Entered into sendHttpsRequest()");
        }

        DataOutputStream out = null;
        BufferedReader buffReader = null;
        String serviceResponseString = "";

        try {
            System.setProperty("sun.net.http.allowRestrictedHeaders", "true");
            this.httpsconn.setDoInput(true);
            this.httpsconn.setDoOutput(true);
            this.requestProp = new Properties();
            if (this.debugFlag) {
                Log.i("Setting Request Header Info");
            }

            this.requestProp.setProperty("Content-Type", "text/plain");
            if (requestHeader.getRegion() != null) {
                this.requestProp.setProperty("region", requestHeader.getRegion());
            }

            if (requestHeader.getCountry() != null) {
                this.requestProp.setProperty("country", requestHeader.getCountry());
            }

            if (requestHeader.getMessage() != null) {
                this.requestProp.setProperty("message", requestHeader.getMessage());
            }

            if (requestHeader.getMerchantNumber() != null) {
                this.requestProp.setProperty("MerchNbr", requestHeader.getMerchantNumber());
            }

            if (requestHeader.getRoutingIndicator() != null) {
                this.requestProp.setProperty("RtInd", requestHeader.getRoutingIndicator());
            }

            if (requestHeader.getOrigin() != null) {
                this.requestProp.setProperty("Origin", requestHeader.getOrigin());
            }

            if (requestHeader.getAPIVersion() != null) {
                this.requestProp.setProperty("APIVersion", requestHeader.getAPIVersion());
            }

            if (this.debugFlag) {
                Log.i("After setting Req Header Info:\r\n--------------------------------------------------------------------------------------------------\r\n-                                 Request Header Details                                         -\r\n-------------------------------------------------------------------------------------------------\r\n" + requestHeader + "\r\n" + "-------------------------------------------------------------------------------------------------");
            }

            this.inputProp = new Properties();
            if (gcagReqString != null) {
                this.payload = gcagReqString;
            }

            if (this.payload != null) {
                this.inputProp.setProperty("AuthorizationRequestParam", this.payload);
            }

            String argString = "";
            if (this.inputProp != null) {
                argString = this.toEncodedString(this.inputProp);
            }

            if (this.httpsconn != null) {
                if (this.requestProp != null) {
                    Enumeration names = this.requestProp.propertyNames();

                    while(names.hasMoreElements()) {
                        String name = (String)names.nextElement();
                        String value = this.requestProp.getProperty(name);
                        this.httpsconn.setRequestProperty(name, value);
                    }
                }

                this.httpsconn.setUseCaches(false);
                if (!ISOUtil.IsNullOrEmpty(PropertyReader.getHttpsTimeout())) {
                    this.httpsconn.setConnectTimeout(new Integer(PropertyReader.getHttpsTimeout()));
                }

                if (!ISOUtil.IsNullOrEmpty(PropertyReader.getReadTimeout())) {
                    this.httpsconn.setReadTimeout(new Integer(PropertyReader.getReadTimeout()));
                }

                out = new DataOutputStream(this.httpsconn.getOutputStream());
                out.writeBytes(argString);
                out.flush();
                buffReader = new BufferedReader(new InputStreamReader(this.httpsconn.getInputStream()));

                for(String outputString = buffReader.readLine(); outputString != null; outputString = buffReader.readLine()) {
                    serviceResponseString = serviceResponseString + outputString;
                }
            }
        } catch (SocketTimeoutException var15) {
            var15.printStackTrace();
            if (this.debugFlag) {
                Log.e(" Connection timeout in createHTTPsConnection() method.. EXCEPTION : " + var15.getMessage());
            }

            throw var15;
        } catch (IOException var16) {
            if (this.debugFlag) {
                Log.e("Error in sendHttpsRequest() method..  Exception :" + var16);
            }

            throw var16;
        } catch (Exception var17) {
            if (this.httpsconn != null && this.httpsconn.getResponseCode() > -1) {
                this.serverRespCode = Integer.toString(this.httpsconn.getResponseCode());
            }

            this.httpsconn.getResponseMessage();
            if (this.debugFlag) {
                Log.e("Error in sendHttpsRequest() method..  Exception:" + var17);
                Log.e("Server Response Code:" + this.serverRespCode);
            }

            throw var17;
        } finally {
            this.httpsconn.disconnect();
            if (out != null) {
                out.close();
            }

            if (buffReader != null) {
                buffReader.close();
            }

        }

        if ((serviceResponseString == null || serviceResponseString.equalsIgnoreCase("")) && this.debugFlag) {
            Log.i("No response returned");
            throw new Exception("No response returned");
        }

        if (this.debugFlag) {
            Log.i("Exiting from sendHttpsRequest()");
        }

        return serviceResponseString;
    }

    public final String sendHttpRequest(String gcagReqString, RequestHeader requestHeader) throws Exception {
        if (this.debugFlag) {
            Log.i("Entered into sendHttpRequest()");
        }

        DataOutputStream out = null;
        BufferedReader buffReader = null;
        String serviceResponseString = "";

        try {
            System.setProperty("sun.net.http.allowRestrictedHeaders", "true");
            this.httpconn.setDoInput(true);
            this.httpconn.setDoOutput(true);
            this.requestProp = new Properties();
            if (this.debugFlag) {
                Log.i("Setting Request Header Info");
            }

            this.requestProp.setProperty("Content-Type", "text/plain");
            if (requestHeader.getRegion() != null) {
                this.requestProp.setProperty("region", requestHeader.getRegion());
            }

            if (requestHeader.getCountry() != null) {
                this.requestProp.setProperty("country", requestHeader.getCountry());
            }

            if (requestHeader.getMessage() != null) {
                this.requestProp.setProperty("message", requestHeader.getMessage());
            }

            if (requestHeader.getMerchantNumber() != null) {
                this.requestProp.setProperty("MerchNbr", requestHeader.getMerchantNumber());
            }

            if (requestHeader.getRoutingIndicator() != null) {
                this.requestProp.setProperty("RtInd", requestHeader.getRoutingIndicator());
            }

            if (requestHeader.getOrigin() != null) {
                this.requestProp.setProperty("Origin", requestHeader.getOrigin());
            }

            if (requestHeader.getAPIVersion() != null) {
                this.requestProp.setProperty("APIVersion", requestHeader.getAPIVersion());
            }

            if (this.debugFlag) {
                Log.i("After setting Req Header Info:\r\n--------------------------------------------------------------------------------------------------\r\n-                                 Request Header Details                                         -\r\n-------------------------------------------------------------------------------------------------\r\n" + requestHeader + "\r\n" + "-------------------------------------------------------------------------------------------------");
            }

            this.inputProp = new Properties();
            if (gcagReqString != null) {
                this.payload = gcagReqString;
            }

            if (this.payload != null) {
                this.inputProp.setProperty("AuthorizationRequestParam", this.payload);
            }

            String argString = "";
            if (this.inputProp != null) {
                argString = this.toEncodedString(this.inputProp);
            }

            if (this.httpconn != null) {
                if (this.requestProp != null) {
                    Enumeration names = this.requestProp.propertyNames();

                    while(names.hasMoreElements()) {
                        String name = (String)names.nextElement();
                        String value = this.requestProp.getProperty(name);
                        this.httpconn.setRequestProperty(name, value);
                    }
                }

                this.httpconn.setUseCaches(false);
                if (!ISOUtil.IsNullOrEmpty(PropertyReader.getHttpsTimeout())) {
                    this.httpconn.setConnectTimeout(new Integer(PropertyReader.getHttpsTimeout()));
                }

                if (!ISOUtil.IsNullOrEmpty(PropertyReader.getReadTimeout())) {
                    this.httpconn.setReadTimeout(new Integer(PropertyReader.getReadTimeout()));
                }

                out = new DataOutputStream(this.httpconn.getOutputStream());
                out.writeBytes(argString);
                out.flush();
                buffReader = new BufferedReader(new InputStreamReader(this.httpconn.getInputStream()));

                for(String outputString = buffReader.readLine(); outputString != null; outputString = buffReader.readLine()) {
                    serviceResponseString = serviceResponseString + outputString;
                }
            }
        } catch (SocketTimeoutException var15) {
            var15.printStackTrace();
            if (this.debugFlag) {
                Log.e(" Connection timeout in sendHttpRequest() method.. EXCEPTION : " + var15.getMessage());
            }

            throw var15;
        } catch (IOException var16) {
            if (this.debugFlag) {
                Log.e("Error in sendHttpRequest() method..  Exception :" + var16);
            }

            throw var16;
        } catch (Exception var17) {
            if (this.httpconn != null && this.httpconn.getResponseCode() > -1) {
                this.serverRespCode = Integer.toString(this.httpconn.getResponseCode());
            }

            this.httpconn.getResponseMessage();
            if (this.debugFlag) {
                Log.e("Error in sendHttpRequest() method..  Exception:" + var17);
                Log.e("Server Response Code:" + this.serverRespCode);
            }

            throw var17;
        } finally {
            this.httpconn.disconnect();
            if (out != null) {
                out.close();
            }

            if (buffReader != null) {
                buffReader.close();
            }

        }

        if ((serviceResponseString == null || serviceResponseString.equalsIgnoreCase("")) && this.debugFlag) {
            Log.i("No response returned");
        }

        if (this.debugFlag) {
            Log.i("Exiting from sendHttpsRequest()");
        }

        return serviceResponseString;
    }

    private void trustAllHttpsCertificates() throws IPSAPIException, SSLConnectionException {
        try {
            TrustManager[] trustAllCerts = new TrustManager[1];
            TrustManager trusrManager = new PaymentSDKRequestProcessor.miTM();
            trustAllCerts[0] = trusrManager;
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init((KeyManager[])null, trustAllCerts, (SecureRandom)null);
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (NoSuchAlgorithmException var4) {
            if (this.debugFlag) {
                Log.e("Error in trustAllHttpsCertificates() method..  NoSuchAlgorithmException: " + var4);
            }

            throw new SSLConnectionException("Error in trustAllHttpsCertificates() method..  Exception:", var4);
        } catch (IllegalArgumentException var5) {
            if (this.debugFlag) {
                Log.e("Error in trustAllHttpsCertificates() method..  IllegalArgumentException: " + var5);
            }

            throw new SSLConnectionException(var5.getMessage(), var5);
        } catch (Exception var6) {
            if (this.debugFlag) {
                Log.e("Error in trustAllHttpsCertificates() method..  Exception:" + var6);
            }

            throw new IPSAPIException(var6);
        }
    }

    private String toEncodedString(Properties args) {
        StringBuffer buf = new StringBuffer();
        Enumeration names = args.propertyNames();

        while(names.hasMoreElements()) {
            String name = (String)names.nextElement();
            String value = args.getProperty(name);
            buf.append(name);
            buf.append('=');
            buf.append(value);
            if (names.hasMoreElements()) {
                buf.append('&');
            }
        }

        return buf.toString();
    }

    public static class miTM implements TrustManager, X509TrustManager {
        public miTM() {
        }

        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }

        public boolean isServerTrusted(X509Certificate[] certs) {
            return true;
        }

        public boolean isClientTrusted(X509Certificate[] certs) {
            return true;
        }

        public void checkServerTrusted(X509Certificate[] certs, String authType) throws CertificateException {
        }

        public void checkClientTrusted(X509Certificate[] certs, String authType) {
        }
    }
}
