package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations;

public enum CallTypeCd
{
  International_Subscriber_Dialing("ISD"),  International_TollFree("ITF"),  Local_Call("LOCAL"),  Trunk_Dialing_Subscriber("TDS");
  
  private final String callTypeCd;
  
  private CallTypeCd(String callTypeCd)
  {
    this.callTypeCd = callTypeCd;
  }
  
  public String getCallTypeCd()
  {
    return this.callTypeCd;
  }
}
