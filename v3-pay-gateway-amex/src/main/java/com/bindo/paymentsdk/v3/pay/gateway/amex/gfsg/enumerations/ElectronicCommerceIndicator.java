package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations;

public enum ElectronicCommerceIndicator {
    Authenticated("05"),
    Attempted("06"),
    Not_authenticated("07"),
    Payment_Token_Data_Present("20");

    private final String electronicCommerceIndicator;

    private ElectronicCommerceIndicator(String electronicCommerceIndicator) {
        this.electronicCommerceIndicator = electronicCommerceIndicator;
    }

    public String getElectronicCommerceIndicator() {
        return this.electronicCommerceIndicator;
    }
}
