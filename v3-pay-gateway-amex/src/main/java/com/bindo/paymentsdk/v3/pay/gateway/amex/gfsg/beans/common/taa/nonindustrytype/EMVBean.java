package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.nonindustrytype;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.NonIndustrySpecificTAABean;

public class EMVBean
  extends NonIndustrySpecificTAABean
{
  private String eMVFormatType;
  private String iCCSystemRelatedData;
  private String emvField7Reserved;
  private IccSystemRelatedDataBean iccSystemRelatedDataBean;
  
  public String getEMVFormatType()
  {
    return this.eMVFormatType;
  }
  
  public void setEMVFormatType(String formatType)
  {
    this.eMVFormatType = formatType;
  }
  
  public String getICCSystemRelatedData()
  {
    return this.iCCSystemRelatedData;
  }
  
  public void setICCSystemRelatedData(String systemRelatedData)
  {
    this.iCCSystemRelatedData = systemRelatedData;
  }
  
  public String getEmvField7Reserved()
  {
    return this.emvField7Reserved;
  }
  
  public void setEmvField7Reserved(String emvField7Reserved)
  {
    this.emvField7Reserved = emvField7Reserved;
  }
  
  public IccSystemRelatedDataBean getIccSystemRelatedDataBean()
  {
    return this.iccSystemRelatedDataBean;
  }
  
  public void setIccSystemRelatedDataBean(IccSystemRelatedDataBean iccSystemRelatedDataBean)
  {
    this.iccSystemRelatedDataBean = iccSystemRelatedDataBean;
  }
}
