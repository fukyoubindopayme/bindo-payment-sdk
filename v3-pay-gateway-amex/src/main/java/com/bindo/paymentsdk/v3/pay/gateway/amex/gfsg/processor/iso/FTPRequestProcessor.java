package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.processor.iso;

import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.EncryptedData;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.ErrorObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.SettlementRequestDataObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.SettlementResponseDataObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.acknowledgement.AcknowledgementReportObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.confirmation.ConfirmationReportObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.connection.PropertyReader;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.exceptions.SettlementException;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.formats.SettlementMessageFormatter;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.messagecreator.AcknowledgementReportObjectCreator;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.messagecreator.ConfirmationReportObjectCreator;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.utils.FieldsAutoPopulator;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.CommonValidator;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.SettlementValidator;
import com.bindo.paymentsdk.v3.pay.gateway.amex.util.juice.Juice;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.ProxyHTTP;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

public class FTPRequestProcessor implements IRequestProcessor {
  private final static String TAG = "FTPRequestProcessor";

  private boolean debugFlag;
  Properties clientprops1 = new Properties();
  private String bACKUP_FILE_ENCRYPTION = "YES";

  public FTPRequestProcessor() {
  }

  public SettlementResponseDataObject processRequest(SettlementRequestDataObject requestDataObject, PropertyReader propertyReader) throws SettlementException {
    if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
      this.debugFlag = true;
      Log.i(TAG, "Entered into processRequest()");
    }

    SettlementResponseDataObject settlementResBean = new SettlementResponseDataObject();
    StringBuffer localUploadFilePath = new StringBuffer();
    List validationErrorList = null;

    try {
      FieldsAutoPopulator.updateRecordNumber(requestDataObject);
      FieldsAutoPopulator.updateTFSRecord(requestDataObject);
      validationErrorList = SettlementValidator.validateISOSettlementRecords(requestDataObject);
      List<ErrorObject> connectionInfoErrorList = SettlementValidator.validateConnectionInfo(propertyReader);
      if (connectionInfoErrorList != null && !connectionInfoErrorList.isEmpty()) {
        validationErrorList.addAll(connectionInfoErrorList);
      }

      if (validationErrorList != null && !validationErrorList.isEmpty()) {
        settlementResBean.setActionCodeDescription(validationErrorList);
      } else {
        List<String> settlementRecordList = SettlementMessageFormatter.formatISORequest(requestDataObject);
        if (settlementRecordList != null && !settlementRecordList.isEmpty()) {
          if (PropertyReader.getFileStorage() != null && PropertyReader.getFileStorage().equalsIgnoreCase("ON")) {
            localUploadFilePath.append(PropertyReader.getFtpLocalUploadFilePath());
          }

          localUploadFilePath.append(PropertyReader.getFtpUploadFileName());
          List<ErrorObject> sftpUploadErrorList = this.createSettlementFileAndAppendData(localUploadFilePath.toString(), settlementRecordList);
          if (sftpUploadErrorList != null && !sftpUploadErrorList.isEmpty()) {
            validationErrorList.addAll(sftpUploadErrorList);
            settlementResBean.setActionCodeDescription(validationErrorList);
          }
        }
      }
    } catch (FileNotFoundException var9) {
      if (this.debugFlag) {
        Log.e(TAG,"ERROR_FILE_NOT_FOUND", var9);
      }

      throw new SettlementException("ERROR_FILE_NOT_FOUND", var9);
    } catch (IOException var10) {
      if (this.debugFlag) {
        Log.e(TAG,"ERROR_READ_WRITE_FILE", var10);
      }

      throw new SettlementException("ERROR_READ_WRITE_FILE", var10);
    } catch (Exception var11) {
      if (this.debugFlag) {
        Log.e(TAG,"ERROR_Settlement_REQ_FILE", var11);
      }

      throw new SettlementException("ERROR_Settlement_REQ_FILE", var11);
    }

    if (this.debugFlag) {
      Log.i(TAG,"Exiting from processRequest()");
    }

    return settlementResBean;
  }

  public List<File> fetchConfirmationReportInPrintedFormat(PropertyReader propertyReader) throws SettlementException {
    Log.i(TAG,"LOGGER_ENTER");
    List<File> file = null;
    List<ErrorObject> connErrorList = SettlementValidator.validateConnectionInfo(propertyReader);
    if (connErrorList == null || connErrorList.isEmpty()) {
      StringBuffer convertedFilePath = new StringBuffer();
      String localDirectoryPath = PropertyReader.getFtpLocalDownloadFilePath();
      String downloadFileName = PropertyReader.getFtpDownloadPrintedFileName();
      if (PropertyReader.getFileStorage() != null && PropertyReader.getFileStorage().equalsIgnoreCase("ON")) {
        convertedFilePath.append(localDirectoryPath);
        convertedFilePath.append(downloadFileName);
        if (this.debugFlag) {
          Log.i(TAG,"Download File Path:" + convertedFilePath.toString());
        }
      }

      try {
        file = this.downloadFileFromSFTP(downloadFileName, convertedFilePath.toString());
      } catch (Exception var8) {
        if (this.debugFlag) {
          Log.e(TAG,"Exception in fetchConfirmationReportInPrintedFormat method of class FTPRequestProcessor: " + var8);
        }

        throw new SettlementException("XXXX.5 -   SFT Download failed");
      }
    }

    return file;
  }

  public List<ConfirmationReportObject> fetchConfirmationReportInRawData(PropertyReader propertyReader) throws SettlementException {
    Log.i(TAG,"LOGGER_ENTER");
    List<ConfirmationReportObject> confirmationReportObjectList = null;
    List<ErrorObject> connErrorList = SettlementValidator.validateConnectionInfo(propertyReader);
    if (connErrorList == null || connErrorList.isEmpty()) {
      StringBuffer convertedFilePath = new StringBuffer();
      String localDirectoryPath = PropertyReader.getFtpLocalDownloadFilePath();
      String downloadFileName = PropertyReader.getFtpDownloadRawFileName();
      if (PropertyReader.getFileStorage() != null && PropertyReader.getFileStorage().equalsIgnoreCase("ON")) {
        convertedFilePath.append(localDirectoryPath);
        convertedFilePath.append(downloadFileName);
        if (this.debugFlag) {
          Log.i(TAG,"Download File Path:" + convertedFilePath.toString());
        }
      }

      try {
        List<File> fileList = this.downloadFileFromSFTP(downloadFileName, localDirectoryPath);
        confirmationReportObjectList = ConfirmationReportObjectCreator.convertRawFiletoResponse(fileList);
      } catch (Exception var8) {
        if (this.debugFlag) {
          Log.e(TAG,"Exception in fetchConfirmationReportInRawData method of class FTPRequestProcessor: " + var8);
        }

        throw new SettlementException("XXXX.5 -   SFT Download failed");
      }
    }

    return confirmationReportObjectList;
  }

  public List<AcknowledgementReportObject> fetchAcknowledgementInPrintedFormat(PropertyReader propertyReader, List<ErrorObject> connErrorList) throws SettlementException {
    if (this.debugFlag) {
      Log.i(TAG,"LOGGER_ENTER");
    }

    List<File> file = null;
    List<AcknowledgementReportObject> acknowledgementReportObjectList = null;
    List<ErrorObject> errorList = null;
    errorList = SettlementValidator.validateConnectionInfo(propertyReader);
    if (errorList == null || errorList.isEmpty()) {
      StringBuffer convertedFilePath = new StringBuffer();
      String localDirectoryPath = PropertyReader.getFtpLocalDownloadFilePath();
      String downloadFileName = PropertyReader.getFtpDownloadAcknowledgeFileName();
      if (PropertyReader.getFileStorage() != null && PropertyReader.getFileStorage().equalsIgnoreCase("ON") && PropertyReader.getAcknowledgeType().equals("TEXT")) {
        convertedFilePath.append(localDirectoryPath);
        convertedFilePath.append(downloadFileName);
        if (this.debugFlag) {
          Log.i(TAG,"Download File Path:" + convertedFilePath.toString());
        }
      }

      try {
        file = this.downloadFileFromSFTP(downloadFileName, convertedFilePath.toString());
        acknowledgementReportObjectList = AcknowledgementReportObjectCreator.convertAckFiletoResponse(file);
      } catch (Exception var10) {
        if (this.debugFlag) {
          Log.e(TAG,"Exception in fetchAcknowledgementInPrintedFormat method of class FTPRequestProcessor: " + var10);
        }

        throw new SettlementException("XXXX.5 -   SFT Download failed");
      }
    }

    return acknowledgementReportObjectList;
  }

  private List<ErrorObject> createSettlementFileAndAppendData(String filePath, List<String> settlementRecords) throws SettlementException, IOException {
    //InputStream in = this.getClass().getClassLoader().getResourceAsStream("settlement.properties");
    //this.clientprops1.load(in);
    //this.bACKUP_FILE_ENCRYPTION = "YES"; //this.clientprops1.getProperty("BACKUP_FILE_ENCRYPTION");
    if (CommonValidator.isNullOrEmpty(this.bACKUP_FILE_ENCRYPTION)) {
      this.bACKUP_FILE_ENCRYPTION = "YES";
    }

    if (this.debugFlag) {
      Log.i(TAG,"File Path::" + filePath);
    }

    boolean isUploaded = false;
    ArrayList sftpUploadErrorList = new ArrayList();

    try {
      if (filePath != null && !filePath.equals("")) {
        File settlementFile = this.createSettlementFile(filePath, settlementRecords);
        new EncryptedData();
        EncryptedData var7 = this.decryptSFTCredentials(PropertyReader.getFtpUserID(), PropertyReader.getFtpPassword());

        try {
          BouncyCastlePGP.encryptFile(settlementFile.getAbsolutePath(), PropertyReader.getPublicKey(), true, PropertyReader.getUserIdentity());
        } catch (Exception var34) {
          if (this.debugFlag) {
            Log.i(TAG,"File encryption failed " + var34);
          }
        }

        String encryptedFilePath = "";
        if (PropertyReader.getFileStorage().equalsIgnoreCase("OFF")) {
          encryptedFilePath = settlementFile.getAbsolutePath() + ".asc";
        } else if (PropertyReader.getFileStorage().equalsIgnoreCase("ON")) {
          StringBuffer localUploadFilePath = new StringBuffer();
          localUploadFilePath.append(PropertyReader.getFtpLocalUploadFilePath());
          localUploadFilePath.append(PropertyReader.getFtpUploadFileName());
          localUploadFilePath.append(".asc");
          encryptedFilePath = localUploadFilePath.toString();
        }

        File encryptedFile = new File(encryptedFilePath);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(encryptedFile));

        try {
          String readFileContent = bufferedReader.readLine();
          if (readFileContent != null) {
            isUploaded = this.uploadFileToSFTP(encryptedFile, "/inbox");
          } else if (this.debugFlag) {
            Log.i(TAG,"Encrypted submission file is empty");
          }
        } catch (Exception var32) {
          ErrorObject sftpErrorObj = new ErrorObject("XXXX.4", "SFT Upload Failed");
          if (this.debugFlag) {
            Log.e(TAG,var32.getMessage());
          }

          sftpUploadErrorList.add(sftpErrorObj);
          String var13 = var32.getMessage();
        } finally {
          bufferedReader.close();
          System.gc();
          boolean isDeleted;
          if (PropertyReader.getFileStorage().equalsIgnoreCase("OFF")) {
            if (settlementFile.exists()) {
              isDeleted = settlementFile.delete();
              if (isDeleted && this.debugFlag) {
                Log.i(TAG,"Submission upload File deleted successfully");
              }
            }

            if (encryptedFile.exists()) {
              isDeleted = encryptedFile.delete();
              if (isDeleted && this.debugFlag) {
                Log.i(TAG,"Encrypted Submission upload File deleted successfully");
              }
            }
          } else if (PropertyReader.getFileStorage().equalsIgnoreCase("ON")) {
            if (this.bACKUP_FILE_ENCRYPTION.equalsIgnoreCase("NO")) {
              if (encryptedFile.exists()) {
                isDeleted = encryptedFile.delete();
                StringBuffer localUploadFilePath = new StringBuffer();
                localUploadFilePath.append(PropertyReader.getFtpLocalUploadFilePath());
                localUploadFilePath.append(PropertyReader.getFtpUploadFileName());
                File file = new File(localUploadFilePath.toString());
                if (file.exists()) {
                  long fileCreationDateTime = file.lastModified();
                  Date date = new Date(fileCreationDateTime);
                  SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
                  String shortDateInstance = dateFormat.format(date);
                  new SimpleDateFormat("yyyyMMdd");
                  String shortTimeInstance = DateFormat.getTimeInstance().format(date);
                  StringBuffer renamedFileName = new StringBuffer(localUploadFilePath);
                  renamedFileName.append("-");
                  renamedFileName.append(shortDateInstance);
                  renamedFileName.append(shortTimeInstance.substring(0, shortTimeInstance.indexOf(" ")).replaceAll(":", ""));
                  File renamedFile = new File(renamedFileName.toString());
                  file.renameTo(renamedFile);
                }

                if (isDeleted && this.debugFlag) {
                  Log.i(TAG,"Encrypted Submission upload File deleted successfully");
                }
              }
            } else if (this.bACKUP_FILE_ENCRYPTION.equalsIgnoreCase("YES") && settlementFile.exists()) {
              isDeleted = settlementFile.delete();
              if (isDeleted && this.debugFlag) {
                Log.i(TAG,"Submission upload File deleted successfully");
              }
            }
          }

        }

        if (isUploaded && PropertyReader.getFileStorage() != null && PropertyReader.getFileStorage().equalsIgnoreCase("ON")) {
          if (this.debugFlag) {
            Log.i(TAG,"Local Uploaded File Path:" + PropertyReader.getFtpLocalUploadFilePath());
          }

          StringBuffer localUploadFilePath = new StringBuffer();
          localUploadFilePath.append(PropertyReader.getFtpLocalUploadFilePath());
          localUploadFilePath.append(PropertyReader.getFtpUploadFileName());
          localUploadFilePath.append(".asc");
          Log.i(TAG,"Before file object Creation");
          File file = new File(localUploadFilePath.toString());
          Log.i(TAG,"After File object Creation.... ");
          Log.i(TAG,"localUploadFilePath.toString() : " + localUploadFilePath.toString());
          if (file.exists()) {
            Log.i(TAG,"Entered into file.exists if condition.....");
            long fileCreationDateTime = file.lastModified();
            Log.i(TAG,"fileCreationDateTime value is : " + fileCreationDateTime);
            Date date = new Date(fileCreationDateTime);
            Log.i(TAG,"After date variable....");
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            Log.i(TAG,"After simpledateformat variable dateFormat");
            String shortDateInstance = dateFormat.format(date);
            Log.i(TAG,"shortDateInstance value is : " + shortDateInstance);
            new SimpleDateFormat("yyyyMMdd");
            Log.i(TAG,"After simpledateformat variable dateFormat");
            String shortTimeInstance = DateFormat.getTimeInstance().format(date);
            Log.i(TAG,"shortTimeInstance is " + shortTimeInstance);
            StringBuffer renamedFileName = new StringBuffer(localUploadFilePath);
            Log.i(TAG,"renamedFileName is " + renamedFileName);
            renamedFileName.append("-");
            Log.i(TAG,"Appending the - in renamedFilename");
            renamedFileName.append(shortDateInstance);
            Log.i(TAG,"Appending shortDateInstance in renamedFilename:");
            shortTimeInstance = shortTimeInstance.replaceAll(":", "");
            if (shortTimeInstance.contains("AM")) {
              shortTimeInstance = shortTimeInstance.replace(" AM", "");
            } else if (shortTimeInstance.contains("PM")) {
              shortTimeInstance = shortTimeInstance.replace(" PM", "");
            } else if (shortTimeInstance.contains("am")) {
              shortTimeInstance = shortTimeInstance.replace(" am", "");
            } else if (shortTimeInstance.contains("pm")) {
              shortTimeInstance = shortTimeInstance.replace(" pm", "");
            }

            renamedFileName.append(shortTimeInstance);
            Log.i(TAG,"after appending the renamedFilename with shortTimeInstance ");
            File renamedFile = new File(renamedFileName.toString());
            Log.i(TAG," File variable renamedFile created ....");
            file.renameTo(renamedFile);
            Log.i(TAG," after file.reanameTo statement.............");
          }
        }

        return sftpUploadErrorList;
      } else {
        throw new SettlementException("FILE PATH IS EMPTY");
      }
    } catch (FileNotFoundException var35) {
      if (this.debugFlag) {
        Log.e(TAG,"FileNotFoundException in FTPRequestProcessor class: " + var35);
      }

      throw new SettlementException("ERROR_FILE_NOT_FOUND", var35);
    } catch (IOException var36) {
      if (this.debugFlag) {
        Log.e(TAG,"IOException in FTPRequestProcessor class: " + var36);
      }

      throw new SettlementException("ERROR_READ_WRITE_FILE", var36);
    } catch (Exception var37) {
      if (this.debugFlag) {
        Log.e(TAG,"Exception in FTPRequestProcessor class: " + var37);
      }

      throw new SettlementException("ERROR_Submission_REQ_FILE", var37);
    }
  }

  private boolean uploadFileToSFTP(File uploadFile, String ftpRemoteDirectory) throws SettlementException {
    if (this.debugFlag) {
      Log.i(TAG,"LOGGER_ENTER");
    }

    ChannelSftp sftpChannel = null;
    Session ftpSession = null;

    boolean isUploded;
    try {
      ftpSession = this.getFtpSession();
      sftpChannel = this.getSftpChannel(ftpSession);
      if (this.debugFlag) {
        Log.i(TAG,"Get FTP Channel...");
      }

      sftpChannel.cd(ftpRemoteDirectory);
      FileInputStream fileInputStream = new FileInputStream(uploadFile);
      sftpChannel.put(fileInputStream, uploadFile.getName());
      if (this.debugFlag) {
        Log.i(TAG,"File uploaded successfully ");
        Log.i(TAG,"Stored file as remote filename#: " + uploadFile.getName());
      }

      isUploded = true;
      fileInputStream.close();
    } catch (FileNotFoundException var13) {
      if (this.debugFlag) {
        Log.e(TAG,"Error in locating file. Exception is " + var13.getMessage());
      }

      throw new SettlementException("CRITICAL_SFTP_FILE_NOT_FOUND", var13);
    } catch (SftpException var14) {
      if (this.debugFlag) {
        Log.e(TAG,"Error in uploading file to FTP server. Exception is ", var14);
      }

      throw new SettlementException("ERROR_SFTP_Upload_File");
    } catch (Exception var15) {
      if (this.debugFlag) {
        Log.e(TAG,"Error in uploading file to FTP server. Exception is ", var15);
      }

      throw new SettlementException("ERROR_SFTP_Upload_File", var15);
    } finally {
      if (sftpChannel != null && sftpChannel.isConnected()) {
        sftpChannel.disconnect();
      }

      if (ftpSession != null && ftpSession.isConnected()) {
        ftpSession.disconnect();
      }

      sftpChannel = null;
      uploadFile = null;
      ftpSession = null;
    }

    if (this.debugFlag) {
      Log.i(TAG,"LOGGER_EXIT");
    }

    return isUploded;
  }

  public List<File> downloadFileFromSFTP(String downloadFilePath, String localDirectoryPath) throws SettlementException {
    if (this.debugFlag) {
      Log.i(TAG,"LOGGER_ENTER");
    }

    String ftpRemoteDirectory = "/outbox";
    File file = null;
    Vector<?> vectorFiles = null;
    List<File> fileList = new ArrayList();
    LsEntry lsEntry = null;
    StringBuffer outputFileName = null;
    ChannelSftp sftpChannel = null;
    Session ftpSession = null;
    String remoteFileName = null;

    try {
      ftpSession = this.getFtpSession();
      sftpChannel = this.getSftpChannel(ftpSession);
      sftpChannel.cd(ftpRemoteDirectory);
      vectorFiles = sftpChannel.ls(ftpRemoteDirectory);
      outputFileName = new StringBuffer();
      if (this.debugFlag) {
        Log.i(TAG,"Local DownLoaded File Path:" + PropertyReader.getFtpLocalDownloadFilePath());
      }

      if (vectorFiles.isEmpty()) {
        if (this.debugFlag) {
          Log.i(TAG,"No files are available for download.");
        }
      } else {
        for(int i = 0; i < vectorFiles.size(); ++i) {
          lsEntry = null;
          lsEntry = (LsEntry)vectorFiles.get(i);
          remoteFileName = null;
          remoteFileName = lsEntry.getFilename();
          outputFileName.setLength(0);
          file = null;
          if (this.debugFlag) {
            Log.i(TAG," Current file in Vector List#: " + remoteFileName);
          }

          if (!".".equals(remoteFileName) && !"..".equals(remoteFileName) && remoteFileName.startsWith(downloadFilePath)) {
            if (!localDirectoryPath.equalsIgnoreCase("")) {
              outputFileName.append(localDirectoryPath);
              outputFileName.append(File.separator);
            }

            outputFileName.append(remoteFileName);
            file = new File(outputFileName.toString());
            sftpChannel.get(remoteFileName, new FileOutputStream(file));
            fileList.add(file);
            Log.i(TAG,"Downloaded file#: " + remoteFileName);
          }
        }
      }
    } catch (FileNotFoundException var18) {
      if (this.debugFlag) {
        Log.e(TAG,"Error in locating file. Exception is " + var18.getMessage());
      }

      throw new SettlementException("CRITICAL_SFTP_FILE_NOT_FOUND", var18);
    } catch (SftpException var19) {
      if (this.debugFlag) {
        Log.e(TAG,"Error in downloading file from FTP server. Exception is ", var19);
      }

      throw new SettlementException("ERROR_SFTP_Download_File", var19);
    } catch (Exception var20) {
      if (this.debugFlag) {
        Log.e(TAG,"Error in downloading file from FTP server. Exception is ", var20);
      }

      throw new SettlementException("ERROR_SFTP_Download_File", var20);
    } finally {
      if (sftpChannel != null && sftpChannel.isConnected()) {
        sftpChannel.disconnect();
      }

      if (ftpSession != null && ftpSession.isConnected()) {
        ftpSession.disconnect();
      }

      vectorFiles = null;
      lsEntry = null;
      outputFileName = null;
      sftpChannel = null;
      ftpSession = null;
      remoteFileName = null;
    }

    if (this.debugFlag) {
      Log.i(TAG,"LOGGER_EXIT");
    }

    return fileList;
  }

  private Session getFtpSession() throws SettlementException {
    new EncryptedData();
    EncryptedData encryptData = this.decryptSFTCredentials(PropertyReader.getFtpUserID(), PropertyReader.getFtpPassword());
    if (this.debugFlag) {
      Log.i(TAG,"LOGGER_ENTER");
    }

    String ftpHost = PropertyReader.getFtpURL();
    if (this.debugFlag) {
      Log.i(TAG,"FTP Server Connection URL:" + ftpHost);
    }

    int ftpPort = Integer.valueOf(PropertyReader.getFtpPort());
    if (this.debugFlag) {
      Log.i(TAG,"FTP Server Connection Port Number:" + ftpPort);
    }

    String ftpUserName = PropertyReader.getFtpUserID();   // encryptData.getDecryptedUserID().trim();
    String ftpPassword = PropertyReader.getFtpPassword(); //encryptData.getDecryptedPwd().trim();
    JSch jsch = null;
    Properties properties = null;
    Session ftpSession = null;

    try {
      jsch = new JSch();
      jsch.setKnownHosts(ftpHost);
      ftpSession = jsch.getSession(ftpUserName, ftpHost, ftpPort);
      ftpSession.setPassword(ftpPassword);
      if (PropertyReader.getXmlProxy() != null && "YES".equalsIgnoreCase(PropertyReader.getXmlProxy())) {
        ftpSession.setProxy(new ProxyHTTP(PropertyReader.getXmlProxyAddress(), Integer.parseInt(PropertyReader.getXmlProxyPort())));
      }

      if (this.debugFlag) {
        Log.i(TAG,"Proxy Address " + PropertyReader.getXmlProxyAddress());
        Log.i(TAG,"Proxy Port " + PropertyReader.getXmlProxyPort());
      }

      properties = new Properties();
      properties.put("StrictHostKeyChecking", "no");
      ftpSession.setConfig(properties);
      ftpSession.connect();
      if (this.debugFlag) {
        Log.i(TAG,"FTP server connected ");
      }
    } catch (JSchException var13) {
      if (this.debugFlag) {
        Log.e(TAG,"Error creating FTP session. Exception is " + var13.getMessage());
      }

      throw new SettlementException("CRITICAL_SFTP_CONNECTION_FAILED", var13);
    } finally {
      properties = null;
      jsch = null;
    }

    Log.i(TAG,"LOGGER_EXIT");
    return ftpSession;
  }

  private ChannelSftp getSftpChannel(Session ftpSession) throws SettlementException {
    if (this.debugFlag) {
      Log.i(TAG,"LOGGER_ENTER");
    }

    ChannelSftp sftpChannel = null;
    Channel channel = null;

    try {
      channel = ftpSession.openChannel("sftp");
      channel.connect();
      sftpChannel = (ChannelSftp)channel;
    } catch (JSchException var5) {
      if (this.debugFlag) {
        Log.e(TAG,"Error creating SFTP connection. Exception is " + var5.getMessage());
      }

      throw new SettlementException("CRITICAL_SFTP_CONNECTION_FAILED");
    }

    if (this.debugFlag) {
      Log.i(TAG,"LOGGER_EXIT");
    }

    return sftpChannel;
  }

  private File createSettlementFile(String filePath, List<String> settlementRecords) throws IOException {
    File settlementFile = new File(filePath);
    FileWriter fileWriter = new FileWriter(settlementFile);
    BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
    if (!settlementRecords.isEmpty()) {
      for(Iterator var7 = settlementRecords.iterator(); var7.hasNext(); bufferedWriter.flush()) {
        String settlementRecordValue = (String)var7.next();
        bufferedWriter.write(settlementRecordValue);
        if (PropertyReader.getFileType().equalsIgnoreCase("VARIABLE")) {
          bufferedWriter.write("\n");
        }
      }
    }

    fileWriter.close();
    return settlementFile;
  }

  private EncryptedData decryptSFTCredentials(String userID, String passWord) {
    String key = "51867F0C0E55766E34E1FE7E053CBFB4";
    EncryptedData objEncryptedData = new EncryptedData();

    try {
      Juice juice = new Juice("AES/ECB/NoPadding", "51867F0C0E55766E34E1FE7E053CBFB4");
      String resultDUserID = juice.decrypt(userID);
      String resultDPwd = juice.decrypt(passWord);
      objEncryptedData.setDecryptedUserID(resultDUserID.trim());
      objEncryptedData.setDecryptedPwd(resultDPwd.trim());
    } catch (Exception var8) {
      if (this.debugFlag) {
        Log.e(TAG,"Exception in decryptSFTCredentials method of class FTPRequestProcessor: " + var8);
      }
    }

    return objEncryptedData;
  }
}
