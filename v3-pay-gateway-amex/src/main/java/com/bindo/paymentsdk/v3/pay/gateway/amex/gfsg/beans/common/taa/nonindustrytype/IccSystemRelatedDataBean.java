package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.nonindustrytype;

public class IccSystemRelatedDataBean
{
  private String headerVersionName;
  private String headerVersionNumber;
  private String applicationCryptogram;
  private String issuerApplicationData;
  private String unPredictableNumber;
  private String applicationTransactionCounter;
  private String terminalVerificationResults;
  private String transactionDate;
  private String transactionType;
  private String amountAuthorized;
  private String terminalTransactionCurrencyCode;
  private String terminalCountryCode;
  private String applicationInterCahngeProfile;
  private String amountOther;
  private String applicationPanSequenceNumber;
  private String cryptogramInformationData;
  
  public String getHeaderVersionName()
  {
    return this.headerVersionName;
  }
  
  public void setHeaderVersionName(String headerVersionName)
  {
    this.headerVersionName = headerVersionName;
  }
  
  public String getHeaderVersionNumber()
  {
    return this.headerVersionNumber;
  }
  
  public void setHeaderVersionNumber(String headerVersionNumber)
  {
    this.headerVersionNumber = headerVersionNumber;
  }
  
  public String getApplicationCryptogram()
  {
    return this.applicationCryptogram;
  }
  
  public void setApplicationCryptogram(String applicationCryptogram)
  {
    this.applicationCryptogram = applicationCryptogram;
  }
  
  public String getIssuerApplicationData()
  {
    return this.issuerApplicationData;
  }
  
  public void setIssuerApplicationData(String issuerApplicationData)
  {
    this.issuerApplicationData = issuerApplicationData;
  }
  
  public String getUnPredictableNumber()
  {
    return this.unPredictableNumber;
  }
  
  public void setUnPredictableNumber(String unPredictableNumber)
  {
    this.unPredictableNumber = unPredictableNumber;
  }
  
  public String getApplicationTransactionCounter()
  {
    return this.applicationTransactionCounter;
  }
  
  public void setApplicationTransactionCounter(String applicationTransactionCounter)
  {
    this.applicationTransactionCounter = applicationTransactionCounter;
  }
  
  public String getTerminalVerificationResults()
  {
    return this.terminalVerificationResults;
  }
  
  public void setTerminalVerificationResults(String terminalVerificationResults)
  {
    this.terminalVerificationResults = terminalVerificationResults;
  }
  
  public String getTransactionDate()
  {
    return this.transactionDate;
  }
  
  public void setTransactionDate(String transactionDate)
  {
    this.transactionDate = transactionDate;
  }
  
  public String getTransactionType()
  {
    return this.transactionType;
  }
  
  public void setTransactionType(String transactionType)
  {
    this.transactionType = transactionType;
  }
  
  public String getAmountAuthorized()
  {
    return this.amountAuthorized;
  }
  
  public void setAmountAuthorized(String amountAuthorized)
  {
    this.amountAuthorized = amountAuthorized;
  }
  
  public String getTerminalTransactionCurrencyCode()
  {
    return this.terminalTransactionCurrencyCode;
  }
  
  public void setTerminalTransactionCurrencyCode(String terminalTransactionCurrencyCode)
  {
    this.terminalTransactionCurrencyCode = terminalTransactionCurrencyCode;
  }
  
  public String getTerminalCountryCode()
  {
    return this.terminalCountryCode;
  }
  
  public void setTerminalCountryCode(String terminalCountryCode)
  {
    this.terminalCountryCode = terminalCountryCode;
  }
  
  public String getApplicationInterCahngeProfile()
  {
    return this.applicationInterCahngeProfile;
  }
  
  public void setApplicationInterCahngeProfile(String applicationInterCahngeProfile)
  {
    this.applicationInterCahngeProfile = applicationInterCahngeProfile;
  }
  
  public String getAmountOther()
  {
    return this.amountOther;
  }
  
  public void setAmountOther(String amountOther)
  {
    this.amountOther = amountOther;
  }
  
  public String getApplicationPanSequenceNumber()
  {
    return this.applicationPanSequenceNumber;
  }
  
  public void setApplicationPanSequenceNumber(String applicationPanSequenceNumber)
  {
    this.applicationPanSequenceNumber = applicationPanSequenceNumber;
  }
  
  public String getCryptogramInformationData()
  {
    return this.cryptogramInformationData;
  }
  
  public void setCryptogramInformationData(String cryptogramInformationData)
  {
    this.cryptogramInformationData = cryptogramInformationData;
  }
}
