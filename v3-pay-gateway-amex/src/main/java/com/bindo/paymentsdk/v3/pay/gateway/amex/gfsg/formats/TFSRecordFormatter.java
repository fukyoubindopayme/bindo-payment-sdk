package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.formats;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionFileSummaryBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.connection.PropertyReader;
import java.io.IOException;
import java.util.List;
import org.apache.log4j.Logger;

public final class TFSRecordFormatter
  extends SettlementMessageFormatter
{
  private static final Logger LOGGER = Logger.getLogger(TFSRecordFormatter.class);
  
  public static void formatTFSRecord(TransactionFileSummaryBean transactionFileSummaryBean, List<String> formatedMessageList)
    throws IOException
  {
    StringBuffer formattedMessage = new StringBuffer();
    
    formatValue(transactionFileSummaryBean.getRecordType(), formattedMessage, 3, false, true, true, false, true);
    formatValue(transactionFileSummaryBean.getRecordNumber(), formattedMessage, 8, true, false, false, true, false);
    
    formatValue(transactionFileSummaryBean.getNumberOfDebits(), formattedMessage, 8, true, false, false, true, false);
    
    zeroFill(formattedMessage, 3);
    
    formatValue(transactionFileSummaryBean.getHashTotalDebitAmount(), formattedMessage, 20, true, false, false, true, false);
    formatValue(transactionFileSummaryBean.getNumberOfCredits(), formattedMessage, 8, true, false, false, true, false);
    
    zeroFill(formattedMessage, 3);
    formatValue(transactionFileSummaryBean.getHashTotalCreditAmount(), formattedMessage, 20, true, false, false, true, false);
    
    zeroFill(formattedMessage, 3);
    
    formatValue(transactionFileSummaryBean.getHashTotalAmount(), formattedMessage, 20, true, false, false, true, false);
    if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
      characterSpacesFill(formattedMessage, 604);
    }
    formatedMessageList.add(formattedMessage.toString());
    if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON"))
    {
      LOGGER.info("After Formatting TFS Rec:" + formattedMessage.toString());
      LOGGER.info("Formatted TFS Rec Length:" + formattedMessage.toString().length());
    }
  }
}
