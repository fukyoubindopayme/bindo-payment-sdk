package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations;

public enum InvoiceFormatType {
    EMEA_APACS("000"),
    US_LID("001"),
    LAC_Argentina("002"),
    LAC_Brazil("003"),
    LAC_Mexico("004");

    private final String invoiceFormatType;

    private InvoiceFormatType(String invoiceFormatType) {
        this.invoiceFormatType = invoiceFormatType;
    }

    public String getInvoiceFormatType() {
        return this.invoiceFormatType;
    }
}
