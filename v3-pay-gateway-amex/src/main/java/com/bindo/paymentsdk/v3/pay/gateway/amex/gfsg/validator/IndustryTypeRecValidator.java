package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.ErrorObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceAddendumBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceBasicBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.AirlineIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.AutoRentalIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.CommunicationServicesIndustryBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.EntertainmentTicketingIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.InsuranceIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.LodgingIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.RailIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.RetailIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype.TravelCruiseIndustryTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.exceptions.SettlementException;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.industry.AirLineRecordValidator;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.industry.AutoRentalRecordValidator;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.industry.CMSIndRecordValidator;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.industry.ETTIndRecordValidator;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.industry.InsuranceRecordValidator;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.industry.LodgingRecordValidator;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.industry.RailIndRecordValidator;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.industry.RetailRecordValidator;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.industry.TCIndRecordValidator;
import java.util.List;

public class IndustryTypeRecValidator
{
  public static void validateIndustryRecordType(TransactionAdviceBasicBean transactionAdviceBasicType, TransactionAdviceAddendumBean transactionAddendumType, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    if ((transactionAddendumType instanceof AirlineIndustryTAABean)) {
      AirLineRecordValidator.validateAirLineTypeRec(transactionAdviceBasicType, transactionAddendumType, errorCodes);
    } else if ((transactionAddendumType instanceof AutoRentalIndustryTAABean)) {
      AutoRentalRecordValidator.validateAutoRentalRecord(transactionAdviceBasicType, transactionAddendumType, errorCodes);
    } else if ((transactionAddendumType instanceof CommunicationServicesIndustryBean)) {
      CMSIndRecordValidator.validateCMSIndRecord(transactionAdviceBasicType, transactionAddendumType, errorCodes);
    } else if ((transactionAddendumType instanceof EntertainmentTicketingIndustryTAABean)) {
      ETTIndRecordValidator.validateETTIndRecord(transactionAdviceBasicType, transactionAddendumType, errorCodes);
    } else if ((transactionAddendumType instanceof InsuranceIndustryTAABean)) {
      InsuranceRecordValidator.validateInsuranceRecord(transactionAdviceBasicType, transactionAddendumType, errorCodes);
    } else if ((transactionAddendumType instanceof LodgingIndustryTAABean)) {
      LodgingRecordValidator.validateLodgingTypeRec(transactionAdviceBasicType, transactionAddendumType, errorCodes);
    } else if ((transactionAddendumType instanceof RailIndustryTAABean)) {
      RailIndRecordValidator.validateRailIndRecord(transactionAdviceBasicType, transactionAddendumType, errorCodes);
    } else if ((transactionAddendumType instanceof RetailIndustryTAABean)) {
      RetailRecordValidator.validateRetailIndRecord(transactionAdviceBasicType, transactionAddendumType, errorCodes);
    } else if ((transactionAddendumType instanceof TravelCruiseIndustryTAABean)) {
      TCIndRecordValidator.validateTCIndRecord(transactionAdviceBasicType, transactionAddendumType, errorCodes);
    }
  }
}
