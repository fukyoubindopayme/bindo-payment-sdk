package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.exceptions;

public class SettlementException
  extends Exception
{
  public SettlementException() {}
  
  public SettlementException(String message)
  {
    super(message);
  }
  
  public SettlementException(Throwable cause)
  {
    super(cause);
  }
  
  public SettlementException(String message, Throwable cause)
  {
    super(message, cause);
  }
}
