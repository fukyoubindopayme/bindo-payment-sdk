package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations.RecordType;

public class TransactionFileHeaderBean
{
  private String recordType = RecordType.Transaction_File_Header.getRecordType();
  private String recordNumber = "00000001";
  private String submitterId;
  private String submitterFileReferenceNumber;
  private String submitterFileSequenceNumber;
  private String fileCreationDate;
  private String fileCreationTime;
  private String fileVersionNumber = "12010000";
  private String tfhField4reserved;
  private String tfhField10reserved;
  
  public String getTfhField10reserved()
  {
    return this.tfhField10reserved;
  }
  
  public void setTfhField10reserved(String tfhField10reserved)
  {
    this.tfhField10reserved = tfhField10reserved;
  }
  
  public String getTfhField4reserved()
  {
    return this.tfhField4reserved;
  }
  
  public void setTfhField4reserved(String tfhField4reserved)
  {
    this.tfhField4reserved = tfhField4reserved;
  }
  
  public String getRecordType()
  {
    return this.recordType;
  }
  
  public String getRecordNumber()
  {
    return this.recordNumber;
  }
  
  public String getSubmitterId()
  {
    return this.submitterId;
  }
  
  public String getSubmitterFileReferenceNumber()
  {
    return this.submitterFileReferenceNumber;
  }
  
  public String getSubmitterFileSequenceNumber()
  {
    return this.submitterFileSequenceNumber;
  }
  
  public String getFileCreationDate()
  {
    return this.fileCreationDate;
  }
  
  public String getFileCreationTime()
  {
    return this.fileCreationTime;
  }
  
  public String getFileVersionNumber()
  {
    return this.fileVersionNumber;
  }
  
  public void setRecordType(String recordType)
  {
    this.recordType = recordType;
  }
  
  public void setRecordNumber(String recordNumber)
  {
    this.recordNumber = recordNumber;
  }
  
  public void setSubmitterId(String submitterId)
  {
    this.submitterId = submitterId;
  }
  
  public void setSubmitterFileReferenceNumber(String submitterFileReferenceNumber)
  {
    this.submitterFileReferenceNumber = submitterFileReferenceNumber;
  }
  
  public void setSubmitterFileSequenceNumber(String submitterFileSequenceNumber)
  {
    this.submitterFileSequenceNumber = submitterFileSequenceNumber;
  }
  
  public void setFileCreationDate(String fileCreationDate)
  {
    this.fileCreationDate = fileCreationDate;
  }
  
  public void setFileCreationTime(String fileCreationTime)
  {
    this.fileCreationTime = fileCreationTime;
  }
  
  public void setFileVersionNumber(String fileVersionNumber)
  {
    this.fileVersionNumber = fileVersionNumber;
  }
}
