package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations;

public enum VehicleClassCodes {
    Mini("0001"),
    Subcompact("0002"),
    Economy("0003"),
    Compact("0004"),
    Midsize("0005"),
    Intermediate("0006"),
    Standard("0007"),
    Full_size("0008"),
    Luxury("0009"),
    Premium("0010"),
    Minivan("0011"),
    passenger_van_12("0012"),
    Moving_van("0013"),
    passenger_van_15("0014"),
    Cargo_van("0015"),
    foot_truck_12("0016"),
    foot_truck_20("0017"),
    foot_truck_24("0018"),
    foot_truck_26("0019"),
    Moped("0020"),
    Stretch("0021"),
    Regular("0022"),
    Unique("0023"),
    Exotic("0024"),
    Small_medium_truck("0025"),
    Large_truck("0026"),
    Small_SUV("0027"),
    Medium_SUV("0028"),
    Large_SUV("0029"),
    Exotic_SUV("0030"),
    Four_Wheel_Drive("0031"),
    Special("0032"),
    Taxi("0099"),
    Miscellaneous("9999");

    private final String vehicleClassCodes;

    public String getVehicleClassCodes() {
        return this.vehicleClassCodes;
    }

    private VehicleClassCodes(String vehicleClassCodes) {
        this.vehicleClassCodes = vehicleClassCodes;
    }
}
