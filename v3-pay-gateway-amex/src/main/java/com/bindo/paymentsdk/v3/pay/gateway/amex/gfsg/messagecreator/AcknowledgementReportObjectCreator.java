package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.messagecreator;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.acknowledgement.AcknowledgementReportObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.connection.PropertyReader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;

public class AcknowledgementReportObjectCreator {
  private static final Logger LOGGER = Logger.getLogger(AcknowledgementReportObjectCreator.class);

  public AcknowledgementReportObjectCreator() {
  }

  public static List<AcknowledgementReportObject> convertAckFiletoResponse(List<File> acknowledgementFile) {
    List<AcknowledgementReportObject> acknowledgementReportObjectList = new ArrayList();
    BufferedReader bufferedReader = null;

    try {
      if (!acknowledgementFile.isEmpty() && acknowledgementFile != null) {
        Iterator var4 = acknowledgementFile.iterator();

        while(var4.hasNext()) {
          File file = (File)var4.next();
          bufferedReader = new BufferedReader(new FileReader(file));
          AcknowledgementReportObject acknowledgementReportObject = new AcknowledgementReportObject();

          String line;
          while((line = bufferedReader.readLine()) != null) {
            String fileTrackNum;
            if (line.indexOf("submission file dated") != -1) {
              fileTrackNum = line.substring(line.indexOf("submission file dated") + 22, line.indexOf("with reference")).trim();
              acknowledgementReportObject.setSubFileDate(fileTrackNum);
            }

            if (line.indexOf("reference number") != -1) {
              fileTrackNum = line.substring(line.indexOf("reference number") + 16, line.indexOf("and sequence")).trim();
              acknowledgementReportObject.setRefNumber(fileTrackNum);
            }

            if (line.indexOf("was received by") != -1) {
              fileTrackNum = line.substring(0, line.indexOf("was received by") - 1);
              acknowledgementReportObject.setSeqNumber(fileTrackNum);
            }

            if (line.indexOf("File Tracking Number") != -1) {
              fileTrackNum = line.substring(line.indexOf("File Tracking Number") + 22).trim();
              acknowledgementReportObject.setFileTrackNumber(fileTrackNum);
            }
          }

          acknowledgementReportObjectList.add(acknowledgementReportObject);
        }

        if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
          LOGGER.info("Acknowledgement Report Object List Size:" + acknowledgementReportObjectList.size());
        }
      }
    } catch (Exception var16) {
      if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
        LOGGER.fatal("Acknowledgement Report Object parser Error: " + var16);
      }
    } finally {
      try {
        if (bufferedReader != null) {
          bufferedReader.close();
        }
      } catch (IOException var15) {
        if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
          LOGGER.error("IO Exception occured: " + var15);
        }
      }

    }

    return acknowledgementReportObjectList;
  }
}
