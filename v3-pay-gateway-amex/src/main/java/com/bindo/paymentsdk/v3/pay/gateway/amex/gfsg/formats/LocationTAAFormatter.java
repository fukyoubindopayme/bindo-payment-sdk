package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.formats;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.LocationDetailTAABean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.connection.PropertyReader;
import java.io.IOException;
import java.util.List;
import org.apache.log4j.Logger;

public final class LocationTAAFormatter
{
  private static final Logger LOGGER = Logger.getLogger(LocationTAAFormatter.class);
  
  public static void formatLocationTAARecord(LocationDetailTAABean locationDetailTAABean, List<String> formatedMessageList)
    throws IOException
  {
    StringBuffer formattedMessage = new StringBuffer();
    
    SettlementMessageFormatter.formatValue(locationDetailTAABean
      .getRecordType(), formattedMessage, 3, false, true, true, 
      false, true);
    
    SettlementMessageFormatter.formatValue(locationDetailTAABean
      .getRecordNumber(), formattedMessage, 8, true, false, false, 
      true, false);
    
    SettlementMessageFormatter.formatValue(locationDetailTAABean
      .getTransactionIdentifier(), formattedMessage, 15, true, false, 
      false, true, false);
    
    SettlementMessageFormatter.zeroFill(formattedMessage, 2);
    
    SettlementMessageFormatter.formatValue(locationDetailTAABean
      .getAddendaTypeCode(), formattedMessage, 2, false, true, false, 
      false, true);
    
    SettlementMessageFormatter.formatValue(locationDetailTAABean
      .getLocationName(), formattedMessage, 38, false, true, true, 
      false, true);
    
    SettlementMessageFormatter.formatValue(locationDetailTAABean
      .getLocationAddress(), formattedMessage, 38, false, true, true, 
      false, true);
    
    SettlementMessageFormatter.formatValue(locationDetailTAABean
      .getLocationCity(), formattedMessage, 21, false, true, true, 
      false, true);
    
    SettlementMessageFormatter.formatValue(locationDetailTAABean
      .getLocationRegion(), formattedMessage, 3, false, true, true, 
      false, true);
    
    SettlementMessageFormatter.formatValue(locationDetailTAABean
      .getLocationCountryCode(), formattedMessage, 3, false, true, 
      true, false, true);
    
    SettlementMessageFormatter.formatValue(locationDetailTAABean
      .getLocationPostalCode(), formattedMessage, 15, false, true, 
      true, false, true);
    
    SettlementMessageFormatter.formatValue(locationDetailTAABean
      .getMerchantCategoryCode(), formattedMessage, 4, true, false, 
      false, true, false);
    
    SettlementMessageFormatter.formatValue(locationDetailTAABean
      .getSellerId(), formattedMessage, 20, false, true, true, false, 
      true);
    if (PropertyReader.getFileType().equalsIgnoreCase("FIXED")) {
      SettlementMessageFormatter.characterSpacesFill(formattedMessage, 
        528);
    }
    formatedMessageList.add(formattedMessage.toString());
    if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
      LOGGER.info("Formated TAALocation Type Record :" + formattedMessage.toString());
    }
  }
}
