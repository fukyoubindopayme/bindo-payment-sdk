package com.bindo.paymentsdk.v3.pay.gateway.amex.ips.gcag.bean;

import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.connection.PropertyReader;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.Constants;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.enums.AuthErrorCodes;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.utils.ISOUtil;
import com.bindo.paymentsdk.v3.pay.gateway.amex.ips.validator.Validator;
import java.util.List;

public class NationalUseDataBean
{
  private String primaryId = "AX";
  private String secondaryID = "AAD";
  private String sellerId;
  private String sellerEmailIdVli;
  private String sellerEmailId;
  private String sellerTelephone;
  private String tokenRequestorId;
  private String last4PanReturnInd;
  private long nationalUseBitmap = 0L;

  public String getPrimaryId()
  {
    return this.primaryId;
  }
  
  public void setPrimaryId(String primaryId)
  {
    this.primaryId = primaryId;
  }
  
  public String getSecondaryID()
  {
    return this.secondaryID;
  }
  
  public void setSecondaryID(String secondaryID)
  {
    this.secondaryID = secondaryID;
  }
  
  public String getSellerId()
  {
    return this.sellerId;
  }
  
  public void setSellerId(String sellerId)
  {
    this.sellerId = sellerId;
  }
  
  public String getSellerEmailId()
  {
    return this.sellerEmailId;
  }
  
  public void setSellerEmailId(String sellerEmailId)
  {
    this.sellerEmailId = sellerEmailId;
  }
  
  public String getSellerTelephone()
  {
    return this.sellerTelephone;
  }
  
  public void setSellerTelephone(String sellerTelephone)
  {
    this.sellerTelephone = sellerTelephone;
  }
  
  public String getSellerEmailIdVli()
  {
    return this.sellerEmailIdVli;
  }
  
  public void setSellerEmailIdVli(String sellerEmailIdVli)
  {
    this.sellerEmailIdVli = sellerEmailIdVli;
  }
  
  public String getTokenRequestorId()
  {
    return this.tokenRequestorId;
  }
  
  public void setTokenRequestorId(String tokenRequestorId)
  {
    this.tokenRequestorId = tokenRequestorId;
  }
  
  public String getLast4PanReturnInd()
  {
    return this.last4PanReturnInd;
  }
  
  public void setLast4PanReturnInd(String last4PanReturnInd)
  {
    this.last4PanReturnInd = last4PanReturnInd;
  }
  
  private String computeBitMapIdentifier()
  {
    long bit = 1L;
    if (!ISOUtil.IsNullOrEmpty(getSellerId())) {
      this.nationalUseBitmap |= bit << 30;
    }
    if (!ISOUtil.IsNullOrEmpty(getSellerEmailId())) {
      this.nationalUseBitmap |= bit << 29;
    }
    if (!ISOUtil.IsNullOrEmpty(getSellerTelephone())) {
      this.nationalUseBitmap |= bit << 28;
    }
    if (!ISOUtil.IsNullOrEmpty(getTokenRequestorId())) {
      this.nationalUseBitmap |= bit << 27;
    }
    if (!ISOUtil.IsNullOrEmpty(getLast4PanReturnInd())) {
      this.nationalUseBitmap |= bit << 26;
    }
    return Long.toHexString(this.nationalUseBitmap).toUpperCase();
  }
  
  public String populateNationalUseDataBean(AuthorizationResponseBean pAuthorizationResponseBean, boolean isPSPProvider)
  {
    boolean debugflag = PropertyReader.getDEBUGGER_FLAG().equalsIgnoreCase("ON");
    
    StringBuffer lNationalUseDataBean = new StringBuffer();
    StringBuffer lDisplayBuffer = new StringBuffer();
    ISOUtil lISOUtil = new ISOUtil();
    int lNationalUseDataVLI = 0;
    String lEncodedVLI = "";
    if (ISOUtil.IsNullOrEmpty(getPrimaryId()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD60_NATIONALUSEDATA1PRIMARYID);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else
    {
      if (!"AX".equalsIgnoreCase(getPrimaryId()))
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD60_NATIONALUSEDATA1PRIMARYID);
        pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
      else
      {
        lNationalUseDataBean.append(lISOUtil.convertTo(getPrimaryId(), "ENC_HEX"));
        lDisplayBuffer.append("PRIMARY_ID                ").append(" :: ").append(getPrimaryId()).append(Constants.NEW_LINE_CHAR);
      }
      lNationalUseDataVLI = 2;
    }
    if (ISOUtil.IsNullOrEmpty(getSecondaryID()))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD60_NATIONALUSEDATA1SECONDARYID);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else
    {
      if (!"AAD".equalsIgnoreCase(getSecondaryID()))
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD60_NATIONALUSEDATA1SECONDARYID);
        pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
      else
      {
        lNationalUseDataBean.append(lISOUtil.convertTo(getSecondaryID(), "ENC_HEX"));
        lDisplayBuffer.append("SECONDARY_ID                ").append(" :: ").append(getSecondaryID()).append(Constants.NEW_LINE_CHAR);
      }
      lNationalUseDataVLI += 3;
    }
    String lBitMapIdentifier = computeBitMapIdentifier();
    if (lBitMapIdentifier != null)
    {
      lBitMapIdentifier = ISOUtil.padString(lBitMapIdentifier.toUpperCase(), 8, "0", false, true);
      lNationalUseDataVLI += 4;
    }
    lNationalUseDataBean.append(lBitMapIdentifier);
    lDisplayBuffer.append("BITMAP IDENTIFIER         ").append(" :: ").append(lBitMapIdentifier).append(Constants.NEW_LINE_CHAR);
    if ((ISOUtil.IsNullOrEmpty(getSellerId())) && (isPSPProvider))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD60_NATIONALUSEDATA1SELLERID);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if ((!ISOUtil.IsNullOrEmpty(getSellerId())) && (!Validator.validateSubFields(getSellerId(), 20, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[7])))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD60_NATIONALUSEDATA1SELLERID);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else
    {
      String lSellerId = ISOUtil.padString(getSellerId(), 20, " ", true, false);
      lNationalUseDataBean.append(lISOUtil.convertTo(lSellerId, "ENC_HEX"));
      lDisplayBuffer.append("SELLER ID                 ").append(" :: ").append(getSellerId()).append(Constants.NEW_LINE_CHAR);
      lNationalUseDataVLI += 20;
    }
    if ((ISOUtil.IsNullOrEmpty(getSellerEmailId())) && (isPSPProvider))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD60_NATIONALUSEDATA1SELLEREMAILID);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else
    {
      lNationalUseDataBean.append(lISOUtil.convertTo(ISOUtil.getFieldLength(getSellerEmailId()), "ENC_HEX"));
      lDisplayBuffer.append("SELLER EMAIL ADDRESS VLI  ").append(" :: ").append(ISOUtil.getFieldLength(getSellerEmailId())).append(Constants.NEW_LINE_CHAR);
      if ((!ISOUtil.IsNullOrEmpty(getSellerEmailId())) && (!Validator.validateSubFields(getSellerEmailId(), 40, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[8])))
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD60_NATIONALUSEDATA1SELLEREMAILID);
        pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
      else
      {
        lNationalUseDataBean.append(lISOUtil.convertTo(getSellerEmailId(), "ENC_HEX"));
        lDisplayBuffer.append("SELLER EMAIL ADDRESS      ").append(" :: ").append(getSellerEmailId()).append(Constants.NEW_LINE_CHAR);
        lNationalUseDataVLI = lNationalUseDataVLI + 2 + getSellerEmailId().length();
      }
    }
    if ((ISOUtil.IsNullOrEmpty(getSellerTelephone())) && (isPSPProvider))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD60_NATIONALUSEDATA1SELLERTELEPHONE);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if ((!ISOUtil.IsNullOrEmpty(getSellerTelephone())) && (!Validator.validateSubFields(getSellerTelephone(), 20, 1, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[1])))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD60_NATIONALUSEDATA1SELLERTELEPHONE);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else
    {
      String lSellerTelephone = ISOUtil.padString(getSellerTelephone(), 20, " ", true, false);
      lNationalUseDataBean.append(lISOUtil.convertTo(lSellerTelephone, "ENC_HEX"));
      lDisplayBuffer.append("SELLER TELEPHONE          ").append(" :: ").append(getSellerTelephone()).append(Constants.NEW_LINE_CHAR);
      lNationalUseDataVLI += 20;
    }
    if (!ISOUtil.IsNullOrEmpty(getTokenRequestorId()))
    {
      if (!Validator.validateSubFields(getTokenRequestorId(), 11, 11, com.bindo.paymentsdk.v3.pay.gateway.amex.ips.constants.AuthorizationConstants.VALID_CHAR_SUBFIELDS[7]))
      {
        ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD60_NATIONALUSEDATA1TOKENREQUESTORID);
        pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
      }
      else
      {
        lNationalUseDataBean.append(lISOUtil.convertTo(getTokenRequestorId(), "ENC_HEX"));
        lDisplayBuffer.append("TOKEN REQUESTOR ID (TRID)                ").append(" :: ").append(getTokenRequestorId()).append(Constants.NEW_LINE_CHAR);
      }
      lNationalUseDataVLI += 11;
    }
    if ((!ISOUtil.IsNullOrEmpty(getLast4PanReturnInd())) && (!"Y".equalsIgnoreCase(getLast4PanReturnInd())))
    {
      ErrorObject panErrorObj = new ErrorObject(AuthErrorCodes.ERROR_DATAFIELD60_NATIONALUSEDATA1LAST4PANRETIND);
      pAuthorizationResponseBean.getAuthErrorList().add(panErrorObj);
    }
    else if (!ISOUtil.IsNullOrEmpty(getLast4PanReturnInd()))
    {
      lNationalUseDataBean.append(lISOUtil.convertTo(getLast4PanReturnInd(), "ENC_HEX"));
      lDisplayBuffer.append("LAST 4 PAN RETURN INDICATOR                ").append(" :: ").append(getLast4PanReturnInd()).append(Constants.NEW_LINE_CHAR);
      lNationalUseDataVLI++;
    }
    String lVLI = ISOUtil.padString(String.valueOf(lNationalUseDataVLI), 3, "0", false, true);
    lEncodedVLI = lISOUtil.convertTo(lVLI, "ENC_HEX");
    if (debugflag)
    {
      Log.i("National Use Data Bean\r\n----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n-                                 National Use Data Bean                                                                                                                                       -\r\n---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n" +
      
        lDisplayBuffer.toString() + 
        "\r\n" + "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
      
      Log.i("Populated National Use Data is -- " + lNationalUseDataBean);
      Log.i("National Use Data BIT MAP -- " + lBitMapIdentifier);
    }
    return lEncodedVLI + lNationalUseDataBean.toString();
  }
  
  public String toString()
  {
    StringBuffer lNationalUseData = new StringBuffer();
    
    lNationalUseData.append("PRIMARY ID").append(" :: ").append(getPrimaryId()).append(Constants.NEW_LINE_CHAR);
    lNationalUseData.append("SECONDARY ID").append(" :: ").append(getSecondaryID()).append(Constants.NEW_LINE_CHAR);
    
    lNationalUseData.append("SELLER ID").append(" :: ").append(getSellerId()).append(Constants.NEW_LINE_CHAR);
    lNationalUseData.append("SELLER EMAIL ID VLI").append(" :: ").append(getSellerEmailIdVli()).append(Constants.NEW_LINE_CHAR);
    lNationalUseData.append("SELLER EMAIL ID").append(" :: ").append(getSellerEmailId()).append(Constants.NEW_LINE_CHAR);
    lNationalUseData.append("SELLER TELEPHONE").append(" :: ").append(getSellerTelephone()).append(Constants.NEW_LINE_CHAR);
    lNationalUseData.append("TOKEN REQUESTOR ID (TRID)").append(" :: ").append(getTokenRequestorId()).append(Constants.NEW_LINE_CHAR);
    lNationalUseData.append("LAST 4 PAN RETURN INDICATOR").append(" :: ").append(getLast4PanReturnInd()).append(Constants.NEW_LINE_CHAR);
    
    return lNationalUseData.toString();
  }
}
