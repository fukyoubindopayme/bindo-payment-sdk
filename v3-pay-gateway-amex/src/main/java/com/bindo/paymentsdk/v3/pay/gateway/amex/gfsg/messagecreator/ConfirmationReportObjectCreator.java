package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.messagecreator;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.confirmation.ConfirmationErrorReportBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.confirmation.ConfirmationRejectedRecordBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.confirmation.ConfirmationReportBodyBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.confirmation.ConfirmationReportHeaderBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.confirmation.ConfirmationReportObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.confirmation.ConfirmationReportSummaryBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.connection.PropertyReader;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.messagecreator.RejectionRecords;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;

public class ConfirmationReportObjectCreator {
  private static final Logger LOGGER = Logger.getLogger(com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.messagecreator.ConfirmationReportObjectCreator.class);
  static boolean debugFlag = false;

  public ConfirmationReportObjectCreator() {
  }

  public static List<ConfirmationReportObject> convertRawFiletoResponse(List<File> confirmationFile) {
    List<ConfirmationReportObject> confirmationReportObjectList = new ArrayList();
    BufferedReader bufferedReader = null;

    try {
      int length = 700;
      if (!confirmationFile.isEmpty() && confirmationFile != null) {
        ConfirmationReportObject confirmationReportObject;
        label498:
        for(Iterator var5 = confirmationFile.iterator(); var5.hasNext(); confirmationReportObjectList.add(confirmationReportObject)) {
          File file = (File)var5.next();
          bufferedReader = new BufferedReader(new FileReader(file));
          confirmationReportObject = new ConfirmationReportObject();
          StringBuffer stringBuffer = new StringBuffer();

          while(true) {
            do {
              String line;
              if ((line = bufferedReader.readLine()) == null) {
                continue label498;
              }

              stringBuffer.append(line);
            } while(length != stringBuffer.length());

            String reportPart = stringBuffer.substring(0, 3);
            if (reportPart.equalsIgnoreCase("HDR")) {
              if (PropertyReader.getDebuggerFlag().equalsIgnoreCase("ON")) {
                debugFlag = true;
                LOGGER.info("Confirmation Report Report Header:" + stringBuffer.toString());
              }

              parseReportHeader(stringBuffer, confirmationReportObject);
            } else if (reportPart.equalsIgnoreCase("BDY")) {
              if (debugFlag) {
                LOGGER.info("Confirmation Report Body:" + stringBuffer.toString());
              }

              parseReportBody(stringBuffer, confirmationReportObject);
            } else if (reportPart.equalsIgnoreCase("SUM")) {
              if (debugFlag) {
                LOGGER.info("Confirmation Report Summary:" + stringBuffer.toString());
              }

              parseReportSummary(stringBuffer, confirmationReportObject);
            } else if (reportPart.equalsIgnoreCase("REJ")) {
              parseReportRequiringRejection(stringBuffer, confirmationReportObject);
            } else if (reportPart.equalsIgnoreCase("ACC")) {
              if (debugFlag) {
                LOGGER.info("Confirmation Report  NotRequiring Rejection:" + stringBuffer.toString());
              }

              parseReportNotRequiringRejection(stringBuffer, confirmationReportObject);
            } else if (reportPart.equalsIgnoreCase("BRS")) {
              if (debugFlag) {
                LOGGER.info("Confirmation Report Batch Suspension:" + stringBuffer.toString());
              }

              parseReportBatchSuspension(stringBuffer, confirmationReportObject);
            } else if (reportPart.equalsIgnoreCase("BRR")) {
              if (debugFlag) {
                LOGGER.info("Confirmation Report Rejected Batch:" + stringBuffer.toString());
              }

              parseReportRejectedBatch(stringBuffer, confirmationReportObject);
            } else if (reportPart.equalsIgnoreCase("BRP")) {
              if (debugFlag) {
                LOGGER.info("Confirmation Report Processed Batch:" + stringBuffer.toString());
              }

              parseReportProcessedBatch(stringBuffer, confirmationReportObject);
            } else {
              ConfirmationRejectedRecordBean rejectedTAARecord;
              if (reportPart.equalsIgnoreCase("TAB")) {
                if (debugFlag) {
                  LOGGER.info("Confirmation TAB Rejected Record:" + stringBuffer.toString());
                }

                rejectedTAARecord = new ConfirmationRejectedRecordBean();
                com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.messagecreator.RejectionRecords.setTABRecord(stringBuffer, rejectedTAARecord);
                confirmationReportObject.getRejectionRecords().add(rejectedTAARecord);
              } else if (reportPart.equalsIgnoreCase("TAD")) {
                if (debugFlag) {
                  LOGGER.info("Confirmation TAD Rejected Record:" + stringBuffer.toString());
                }

                rejectedTAARecord = new ConfirmationRejectedRecordBean();
                com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.messagecreator.RejectionRecords.setTADRecord(stringBuffer, rejectedTAARecord);
                confirmationReportObject.getRejectionRecords().add(rejectedTAARecord);
              } else if (reportPart.equalsIgnoreCase("TBT")) {
                rejectedTAARecord = new ConfirmationRejectedRecordBean();
                com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.messagecreator.RejectionRecords.setTBTRecord(stringBuffer, rejectedTAARecord);
                if (debugFlag) {
                  LOGGER.info("Confirmation TBT Rejected Record:" + stringBuffer.toString());
                }

                parseReportProcessedBatch(stringBuffer, confirmationReportObject);
                confirmationReportObject.getRejectionRecords().add(rejectedTAARecord);
              } else if (reportPart.equalsIgnoreCase("TAA")) {
                if (debugFlag) {
                  LOGGER.info("Confirmation TAA Rejected Record:" + stringBuffer.toString());
                }

                rejectedTAARecord = new ConfirmationRejectedRecordBean();
                String formatCode = stringBuffer.substring(26, 28);
                String addendTypeCode;
                if (!formatCode.equals("00") && !formatCode.equals("0")) {
                  if (!formatCode.equals("01") && !formatCode.equals("1")) {
                    if (!formatCode.equals("04") && !formatCode.equals("4")) {
                      if (!formatCode.equals("05") && !formatCode.equals("5")) {
                        if (!formatCode.equals("06") && !formatCode.equals("6")) {
                          if (formatCode.equals("11")) {
                            com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.messagecreator.RejectionRecords.setTAALodgRecord(stringBuffer, rejectedTAARecord);
                          } else if (formatCode.equals("13")) {
                            com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.messagecreator.RejectionRecords.setTAACommRecord(stringBuffer, rejectedTAARecord);
                          } else if (formatCode.equals("14")) {
                            com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.messagecreator.RejectionRecords.setTAACruiesRecord(stringBuffer, rejectedTAARecord);
                          } else if (formatCode.equals("20")) {
                            com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.messagecreator.RejectionRecords.setTAAReatilRecord(stringBuffer, rejectedTAARecord);
                          } else if (formatCode.equals("22")) {
                            com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.messagecreator.RejectionRecords.setTAAEntetainRecord(stringBuffer, rejectedTAARecord);
                          }
                        } else {
                          com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.messagecreator.RejectionRecords.setTAARailRecord(stringBuffer, rejectedTAARecord);
                        }
                      } else {
                        com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.messagecreator.RejectionRecords.setTAAAutoRecord(stringBuffer, rejectedTAARecord);
                      }
                    } else {
                      com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.messagecreator.RejectionRecords.setTAAInsuRecord(stringBuffer, rejectedTAARecord);
                    }
                  } else {
                    addendTypeCode = stringBuffer.substring(28, 30);
                    if (!addendTypeCode.equals("07") && !addendTypeCode.equals("7")) {
                      com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.messagecreator.RejectionRecords.setTAAAirRecord(stringBuffer, rejectedTAARecord);
                    } else {
                      com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.messagecreator.RejectionRecords.setTAAEMVRecord(stringBuffer, rejectedTAARecord);
                    }
                  }
                } else {
                  addendTypeCode = stringBuffer.substring(28, 30);
                  if (!addendTypeCode.equals("02") && !addendTypeCode.equals("2")) {
                    if (!addendTypeCode.equals("05") && !addendTypeCode.equals("5")) {
                      if (!addendTypeCode.equals("07") && !addendTypeCode.equals("7")) {
                        if (addendTypeCode.equals("99")) {
                          com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.messagecreator.RejectionRecords.setTAALOCRecord(stringBuffer, rejectedTAARecord);
                        }
                      } else {
                        com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.messagecreator.RejectionRecords.setTAAEMVRecord(stringBuffer, rejectedTAARecord);
                      }
                    } else {
                      com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.messagecreator.RejectionRecords.setTAACPSL2Record(stringBuffer, rejectedTAARecord);
                    }
                  } else {
                    RejectionRecords.setTAADPPRecord(stringBuffer, rejectedTAARecord);
                  }
                }

                confirmationReportObject.getRejectionRecords().add(rejectedTAARecord);
              }
            }

            stringBuffer.delete(0, 700);
          }
        }

        if (debugFlag) {
          LOGGER.info("confirmation Report Object List Size:" + confirmationReportObjectList.size());
        }
      }
    } catch (Exception var21) {
      if (debugFlag) {
        LOGGER.fatal("Confirmation Report Object parser Error: " + var21);
      }
    } finally {
      try {
        if (bufferedReader != null) {
          bufferedReader.close();
        }
      } catch (IOException var20) {
        if (debugFlag) {
          LOGGER.error("IO Exception occured: " + var20);
        }
      }

    }

    return confirmationReportObjectList;
  }

  private static void parseReportHeader(StringBuffer recordType, ConfirmationReportObject confirmationReportObject) {
    ConfirmationReportHeaderBean reportHeader = new ConfirmationReportHeaderBean();
    reportHeader.setReocrdType(recordSubstring(recordType.toString(), 0, 3));
    reportHeader.setReportDateAndTime(recordSubstring(recordType.toString(), 3, 29));
    reportHeader.setReportID(recordSubstring(recordType.toString(), 29, 35));
    confirmationReportObject.setConfirmationReportHeaderBean(reportHeader);
  }

  private static void parseReportBody(StringBuffer recordType, ConfirmationReportObject confirmationReportObject) {
    ConfirmationReportBodyBean reportBodyBean = new ConfirmationReportBodyBean();
    reportBodyBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
    reportBodyBean.setReportID(recordSubstring(recordType.toString(), 3, 9));
    reportBodyBean.setReportName(recordSubstring(recordType.toString(), 9, 69));
    reportBodyBean.setSubmitterName(recordSubstring(recordType.toString(), 69, 109));
    reportBodyBean.setReportDateTime(recordSubstring(recordType.toString(), 109, 135));
    reportBodyBean.setSubmitterID(recordSubstring(recordType.toString(), 135, 146));
    reportBodyBean.setSubmitterFileRefNumber(recordSubstring(recordType.toString(), 146, 155));
    reportBodyBean.setFileStatus(recordSubstring(recordType.toString(), 155, 195));
    reportBodyBean.setFileCreationDateTime(recordSubstring(recordType.toString(), 195, 217));
    reportBodyBean.setFileTrackingNumber(recordSubstring(recordType.toString(), 217, 231));
    reportBodyBean.setFileReceiptDateTime(recordSubstring(recordType.toString(), 231, 257));
    reportBodyBean.setTotalNumberOfRecords(recordSubstring(recordType.toString(), 257, 265));
    reportBodyBean.setHashTotalAmount(recordSubstring(recordType.toString(), 265, 285));
    reportBodyBean.setTotalNumberOfDebits(recordSubstring(recordType.toString(), 285, 293));
    reportBodyBean.setDebitHashTotalAmount(recordSubstring(recordType.toString(), 293, 313));
    reportBodyBean.setTotalNumberOfCredits(recordSubstring(recordType.toString(), 313, 321));
    reportBodyBean.setCreditHashTotalAmount(recordSubstring(recordType.toString(), 321, 341));
    reportBodyBean.setTotalRejectedRecords(recordSubstring(recordType.toString(), 341, 349));
    reportBodyBean.setTotalSuspendedBatches(recordSubstring(recordType.toString(), 349, 357));
    reportBodyBean.setTotalSuspendedRecords(recordSubstring(recordType.toString(), 357, 365));
    confirmationReportObject.setConfirmationReportBodyBean(reportBodyBean);
  }

  private static void parseReportSummary(StringBuffer recordType, ConfirmationReportObject confirmationReportObject) {
    ConfirmationReportSummaryBean reportSummaryBean = new ConfirmationReportSummaryBean();
    reportSummaryBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
    reportSummaryBean.setErrorCode(recordSubstring(recordType.toString(), 3, 7));
    reportSummaryBean.setNumberOfOccurrences(recordSubstring(recordType.toString(), 7, 15));
    reportSummaryBean.setErrorText(recordSubstring(recordType.toString(), 15, 115));
    confirmationReportObject.addErrorSummary(reportSummaryBean);
  }

  private static void parseReportRequiringRejection(StringBuffer recordType, ConfirmationReportObject confirmationReportObject) {
    ConfirmationErrorReportBean errorReportBean = new ConfirmationErrorReportBean();
    errorReportBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
    errorReportBean.setErrorCode(recordSubstring(recordType.toString(), 3, 7));
    recordNumSubstring(recordType.substring(7, 695), errorReportBean);
    confirmationReportObject.addErrorsRequiringRejectionRecord(errorReportBean);
  }

  private static void parseReportNotRequiringRejection(StringBuffer recordType, ConfirmationReportObject confirmationReportObject) {
    ConfirmationErrorReportBean errorReportBean = new ConfirmationErrorReportBean();
    errorReportBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
    errorReportBean.setErrorCode(recordSubstring(recordType.toString(), 3, 7));
    recordNumSubstring(recordType.substring(7, 695), errorReportBean);
    confirmationReportObject.addErrorsNotRequiringRejectionRecord(errorReportBean);
  }

  private static void parseReportBatchSuspension(StringBuffer recordType, ConfirmationReportObject confirmationReportObject) {
    ConfirmationErrorReportBean errorReportBean = new ConfirmationErrorReportBean();
    errorReportBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
    errorReportBean.setErrorCode(recordSubstring(recordType.toString(), 3, 7));
    recordNumSubstring(recordType.substring(7, 695), errorReportBean);
    confirmationReportObject.addErrorsRequiringBatchSuspensionRecord(errorReportBean);
  }

  private static void parseReportRejectedBatch(StringBuffer recordType, ConfirmationReportObject confirmationReportObject) {
    ConfirmationErrorReportBean errorReportBean = new ConfirmationErrorReportBean();
    errorReportBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
    errorReportBean.setErrorCode(recordSubstring(recordType.toString(), 3, 7));
    recordNumSubstring(recordType.substring(7, 695), errorReportBean);
    confirmationReportObject.addPreSuspendedRejectedBatchRecord(errorReportBean);
  }

  private static void parseReportProcessedBatch(StringBuffer recordType, ConfirmationReportObject confirmationReportObject) {
    ConfirmationErrorReportBean errorReportBean = new ConfirmationErrorReportBean();
    errorReportBean.setRecordType(recordSubstring(recordType.toString(), 0, 3));
    errorReportBean.setErrorCode(recordSubstring(recordType.toString(), 3, 7));
    recordNumSubstring(recordType.substring(7, 695), errorReportBean);
    confirmationReportObject.addPreSuspendedProcessedRejectedBatchRecord(errorReportBean);
  }

  private static String recordSubstring(String svalue, int start, int end) {
    String substr = svalue.substring(start, end);
    return substr.replaceAll("~", " ");
  }

  private static void recordNumSubstring(String svalue, ConfirmationErrorReportBean errorReportBean) {
    for(int i = 0; i < svalue.length(); i += 8) {
      if (!svalue.substring(i, i + 8).equals("00000000")) {
        errorReportBean.addRecordNumber(svalue.substring(i, i + 8));
      }
    }

  }
}
