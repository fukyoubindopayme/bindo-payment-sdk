package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.industrytype;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.IndustryTypeTAABean;

public class CommunicationServicesIndustryBean
  extends IndustryTypeTAABean
{
  private String callDate;
  private String callTime;
  private String callDurationTime;
  private String callFromLocationName;
  private String callFromRegionCode;
  private String callFromCountryCode;
  private String callFromPhoneNumber;
  private String callToLocationName;
  private String callToRegionCode;
  private String callToCountryCode;
  private String callToPhoneNumber;
  private String phoneCardId;
  private String serviceDescription;
  private String billingPeriod;
  private String communicationsCallTypeCode;
  private String communicationsRateClass;
  
  public String getCallDate()
  {
    return this.callDate;
  }
  
  public void setCallDate(String callDate)
  {
    this.callDate = callDate;
  }
  
  public String getCallTime()
  {
    return this.callTime;
  }
  
  public void setCallTime(String callTime)
  {
    this.callTime = callTime;
  }
  
  public String getCallDurationTime()
  {
    return this.callDurationTime;
  }
  
  public void setCallDurationTime(String callDurationTime)
  {
    this.callDurationTime = callDurationTime;
  }
  
  public String getCallFromLocationName()
  {
    return this.callFromLocationName;
  }
  
  public void setCallFromLocationName(String callFromLocationName)
  {
    this.callFromLocationName = callFromLocationName;
  }
  
  public String getCallFromRegionCode()
  {
    return this.callFromRegionCode;
  }
  
  public void setCallFromRegionCode(String callFromRegionCode)
  {
    this.callFromRegionCode = callFromRegionCode;
  }
  
  public String getCallFromCountryCode()
  {
    return this.callFromCountryCode;
  }
  
  public void setCallFromCountryCode(String callFromCountryCode)
  {
    this.callFromCountryCode = callFromCountryCode;
  }
  
  public String getCallFromPhoneNumber()
  {
    return this.callFromPhoneNumber;
  }
  
  public void setCallFromPhoneNumber(String callFromPhoneNumber)
  {
    this.callFromPhoneNumber = callFromPhoneNumber;
  }
  
  public String getCallToLocationName()
  {
    return this.callToLocationName;
  }
  
  public void setCallToLocationName(String callToLocationName)
  {
    this.callToLocationName = callToLocationName;
  }
  
  public String getCallToRegionCode()
  {
    return this.callToRegionCode;
  }
  
  public void setCallToRegionCode(String callToRegionCode)
  {
    this.callToRegionCode = callToRegionCode;
  }
  
  public String getCallToCountryCode()
  {
    return this.callToCountryCode;
  }
  
  public void setCallToCountryCode(String callToCountryCode)
  {
    this.callToCountryCode = callToCountryCode;
  }
  
  public String getCallToPhoneNumber()
  {
    return this.callToPhoneNumber;
  }
  
  public void setCallToPhoneNumber(String callToPhoneNumber)
  {
    this.callToPhoneNumber = callToPhoneNumber;
  }
  
  public String getPhoneCardId()
  {
    return this.phoneCardId;
  }
  
  public void setPhoneCardId(String phoneCardId)
  {
    this.phoneCardId = phoneCardId;
  }
  
  public String getServiceDescription()
  {
    return this.serviceDescription;
  }
  
  public void setServiceDescription(String serviceDescription)
  {
    this.serviceDescription = serviceDescription;
  }
  
  public String getBillingPeriod()
  {
    return this.billingPeriod;
  }
  
  public void setBillingPeriod(String billingPeriod)
  {
    this.billingPeriod = billingPeriod;
  }
  
  public String getCommunicationsCallTypeCode()
  {
    return this.communicationsCallTypeCode;
  }
  
  public void setCommunicationsCallTypeCode(String communicationsCallTypeCode)
  {
    this.communicationsCallTypeCode = communicationsCallTypeCode;
  }
  
  public String getCommunicationsRateClass()
  {
    return this.communicationsRateClass;
  }
  
  public void setCommunicationsRateClass(String communicationsRateClass)
  {
    this.communicationsRateClass = communicationsRateClass;
  }
}
