package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator;

import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.ErrorObject;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceAddendumBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.TransactionAdviceBasicBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.nonindustrytype.CPSLevel2Bean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.nonindustrytype.DeferredPaymentPlanBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.beans.common.taa.nonindustrytype.EMVBean;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.exceptions.SettlementException;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.nonindustry.CPSL2RecordValidator;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.nonindustry.DPPRecordValidator;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.validator.nonindustry.EMVRecordValidator;
import java.util.List;

public class NonIndustryTypeRecValidator
{
  public static void validateNonIndustryRecordType(TransactionAdviceBasicBean transactionAdviceBasicType, TransactionAdviceAddendumBean transactionAdviceAddendumType, List<ErrorObject> errorCodes)
    throws SettlementException
  {
    if ((transactionAdviceAddendumType instanceof CPSLevel2Bean)) {
      CPSL2RecordValidator.validateCPSL2Record(transactionAdviceBasicType, transactionAdviceAddendumType, errorCodes);
    } else if ((transactionAdviceAddendumType instanceof DeferredPaymentPlanBean)) {
      DPPRecordValidator.validateDPPRecord(transactionAdviceBasicType, transactionAdviceAddendumType, errorCodes);
    } else if ((transactionAdviceAddendumType instanceof EMVBean)) {
      EMVRecordValidator.validateEMVRecord(transactionAdviceBasicType, transactionAdviceAddendumType, errorCodes);
    }
  }
}
