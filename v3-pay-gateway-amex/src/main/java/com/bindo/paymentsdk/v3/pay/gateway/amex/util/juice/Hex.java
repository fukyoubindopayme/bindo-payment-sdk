package com.bindo.paymentsdk.v3.pay.gateway.amex.util.juice;

public class Hex {
    private static final char[] hexDigits = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    private Hex() {
    }

    public static String toString(byte[] ba, int offset, int length) {
        char[] buf = new char[length * 2];
        int j = 0;

        for(int i = offset; i < offset + length; ++i) {
            int k = ba[i];
            buf[j++] = hexDigits[k >>> 4 & 15];
            buf[j++] = hexDigits[k & 15];
        }

        return new String(buf);
    }

    public static String toString(byte[] ba) {
        return toString((byte[])ba, 0, ba.length);
    }

    public static String toString(int[] ia, int offset, int length) {
        char[] buf = new char[length * 8];
        int j = 0;

        for(int i = offset; i < offset + length; ++i) {
            int k = ia[i];
            buf[j++] = hexDigits[k >>> 28 & 15];
            buf[j++] = hexDigits[k >>> 24 & 15];
            buf[j++] = hexDigits[k >>> 20 & 15];
            buf[j++] = hexDigits[k >>> 16 & 15];
            buf[j++] = hexDigits[k >>> 12 & 15];
            buf[j++] = hexDigits[k >>> 8 & 15];
            buf[j++] = hexDigits[k >>> 4 & 15];
            buf[j++] = hexDigits[k & 15];
        }

        return new String(buf);
    }

    public static String toString(int[] ia) {
        return toString((int[])ia, 0, ia.length);
    }

    public static String toReversedString(byte[] b, int offset, int length) {
        char[] buf = new char[length * 2];
        int j = 0;

        for(int i = offset + length - 1; i >= offset; --i) {
            buf[j++] = hexDigits[b[i] >>> 4 & 15];
            buf[j++] = hexDigits[b[i] & 15];
        }

        return new String(buf);
    }

    public static String toReversedString(byte[] b) {
        return toReversedString(b, 0, b.length);
    }

    public static byte[] fromString(String hex) {
        int len = hex.length();
        byte[] buf = new byte[(len + 1) / 2];
        int i = 0;
        int j = 0;
        if (len % 2 == 1) {
            buf[j++] = (byte)fromDigit(hex.charAt(i++));
        }

        while(i < len) {
            buf[j++] = (byte)(fromDigit(hex.charAt(i++)) << 4 | fromDigit(hex.charAt(i++)));
        }

        return buf;
    }

    public static byte[] fromReversedString(String hex) {
        int len = hex.length();
        byte[] buf = new byte[(len + 1) / 2];
        int j = 0;
        if (len % 2 == 1) {
            throw new IllegalArgumentException("string must have an even number of digits");
        } else {
            while(len > 0) {
                int var10001 = j++;
                --len;
                int var10002 = fromDigit(hex.charAt(len));
                --len;
                buf[var10001] = (byte)(var10002 | fromDigit(hex.charAt(len)) << 4);
            }

            return buf;
        }
    }

    public static char toDigit(int n) {
        try {
            return hexDigits[n];
        } catch (ArrayIndexOutOfBoundsException var2) {
            throw new IllegalArgumentException(n + " is out of range for a hex digit");
        }
    }

    public static int fromDigit(char ch) {
        if (ch >= '0' && ch <= '9') {
            return ch - 48;
        } else if (ch >= 'A' && ch <= 'F') {
            return ch - 65 + 10;
        } else if (ch >= 'a' && ch <= 'f') {
            return ch - 97 + 10;
        } else {
            throw new IllegalArgumentException("invalid hex digit '" + ch + "'");
        }
    }

    public static String byteToString(int n) {
        char[] buf = new char[]{hexDigits[n >>> 4 & 15], hexDigits[n & 15]};
        return new String(buf);
    }

    public static String shortToString(int n) {
        char[] buf = new char[]{hexDigits[n >>> 12 & 15], hexDigits[n >>> 8 & 15], hexDigits[n >>> 4 & 15], hexDigits[n & 15]};
        return new String(buf);
    }

    public static String intToString(int n) {
        char[] buf = new char[8];

        for(int i = 7; i >= 0; --i) {
            buf[i] = hexDigits[n & 15];
            n >>>= 4;
        }

        return new String(buf);
    }

    public static String longToString(long n) {
        char[] buf = new char[16];

        for(int i = 15; i >= 0; --i) {
            buf[i] = hexDigits[(int)n & 15];
            n >>>= 4;
        }

        return new String(buf);
    }

    public static String dumpString(byte[] data, int offset, int length, String m) {
        if (data == null) {
            return m + "null\n";
        } else {
            StringBuffer sb = new StringBuffer(length * 3);
            if (length > 32) {
                sb.append(m).append("Hexadecimal dump of ").append(length).append(" bytes...\n");
            }

            int end = offset + length;
            int l = Integer.toString(length).length();
            if (l < 4) {
                l = 4;
            }

            while(offset < end) {
                if (length > 32) {
                    String s = "         " + offset;
                    sb.append(m).append(s.substring(s.length() - l)).append(": ");
                }

                int i;
                for(i = 0; i < 32 && offset + i + 7 < end; i += 8) {
                    sb.append(toString((byte[])data, offset + i, 8)).append(' ');
                }

                if (i < 32) {
                    while(i < 32 && offset + i < end) {
                        sb.append(byteToString(data[offset + i]));
                        ++i;
                    }
                }

                sb.append('\n');
                offset += 32;
            }

            return sb.toString();
        }
    }

    public static String dumpString(byte[] data) {
        return data == null ? "null\n" : dumpString((byte[])data, 0, data.length, "");
    }

    public static String dumpString(byte[] data, String m) {
        return data == null ? "null\n" : dumpString((byte[])data, 0, data.length, m);
    }

    public static String dumpString(byte[] data, int offset, int length) {
        return dumpString(data, offset, length, "");
    }

    public static String dumpString(int[] data, int offset, int length, String m) {
        if (data == null) {
            return m + "null\n";
        } else {
            StringBuffer sb = new StringBuffer(length * 3);
            if (length > 8) {
                sb.append(m).append("Hexadecimal dump of ").append(length).append(" integers...\n");
            }

            int end = offset + length;
            int x = Integer.toString(length).length();
            if (x < 8) {
                x = 8;
            }

            while(offset < end) {
                if (length > 8) {
                    String s = "         " + offset;
                    sb.append(m).append(s.substring(s.length() - x)).append(": ");
                }

                for(int i = 0; i < 8 && offset < end; ++i) {
                    sb.append(intToString(data[offset++])).append(' ');
                }

                sb.append('\n');
            }

            return sb.toString();
        }
    }

    public static String dumpString(int[] data) {
        return dumpString((int[])data, 0, data.length, "");
    }

    public static String dumpString(int[] data, String m) {
        return dumpString((int[])data, 0, data.length, m);
    }

    public static String dumpString(int[] data, int offset, int length) {
        return dumpString(data, offset, length, "");
    }
}
