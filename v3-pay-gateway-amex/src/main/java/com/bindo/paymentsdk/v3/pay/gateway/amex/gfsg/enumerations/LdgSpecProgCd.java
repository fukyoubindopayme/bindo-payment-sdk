package com.bindo.paymentsdk.v3.pay.gateway.amex.gfsg.enumerations;

public enum LdgSpecProgCd {
    Lodging("1"),
    No_Show("2"),
    Advanced_Deposit("3");

    private final String ldgSpecProgCd;

    private LdgSpecProgCd(String ldgSpecProgCd) {
        this.ldgSpecProgCd = ldgSpecProgCd;
    }

    public String getLdgSpecProgCd() {
        return this.ldgSpecProgCd;
    }
}
