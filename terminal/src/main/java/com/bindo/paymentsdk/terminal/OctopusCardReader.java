package com.bindo.paymentsdk.terminal;

public interface OctopusCardReader {
    boolean isSupported();

    void setListener(Listener listener);

    void start();

    void stop();

    interface Listener {
        void onStart();

        void onStop();

        void onResult(String data);

        void onError(String code, String message);
    }
}
