package com.bindo.paymentsdk.terminal;

public interface Terminal {
    Printer getBuiltInPrinter();

    BarcodeReader getBarcodeReader();

    GiftCardReader getGiftCardReader();

    OctopusCardReader getOctopusCardReader();

}
