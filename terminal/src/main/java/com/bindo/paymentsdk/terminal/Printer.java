package com.bindo.paymentsdk.terminal;

import java.util.ArrayList;
import java.util.List;

public abstract class Printer {
    protected Config mConfig;
    protected Listener mListener;

    public Printer() {
        mConfig = new Config();
    }

    public abstract boolean isSupported();

    public Config getConfig() {
        return mConfig;
    }

    public void setConfig(Config config) {
        mConfig = config;
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }

    public abstract void print(Paper paper);

    public static interface Listener {
        void onPrinted();

        void onError(String code, String message);
    }

    public static class Config {
        private int paperPrintWidth = 384;
        private int paperPrintPadding = 0;

        public Config() {
        }

        public Config(int paperPrintWidth) {
            this.paperPrintWidth = paperPrintWidth;
        }

        public int getPaperPrintWidth() {
            return paperPrintWidth;
        }

        public void setPaperPrintWidth(int paperPrintWidth) {
            this.paperPrintWidth = paperPrintWidth;
        }

        public int getPaperPrintPadding() {
            return paperPrintPadding;
        }

        public void setPaperPrintPadding(int paperPrintPadding) {
            this.paperPrintPadding = paperPrintPadding;
        }
    }

    public static class Paper {
        public static final String COMMAND_NEW_LINE = "command_new_line";
        public static final String COMMAND_TEXT = "command_text";
        public static final String COMMAND_IMAGE = "command_image";
        public static final String COMMAND_LINE = "command_line";
        public static final String COMMAND_BARCODE = "command_barcode";
        public static final String COMMAND_QRCODE = "command_qrcode";
        public static final String COMMAND_CUT_PAPER = "command_cut_paper";

        public static final String ALIGN_LEFT = "left";
        public static final String ALIGN_CENTER = "center";
        public static final String ALIGN_RIGHT = "right";

        public static final String SIZE_DEFAULT = "default";
        public static final String SIZE_LARGE = "large";
        public static final String SIZE_BIG = "big";

        private List<Command> mCommands;

        public Paper() {
            this.mCommands = new ArrayList<>();
        }

        public void pushCommand(String key) {
            this.mCommands.add(new Command(key, null, ALIGN_LEFT, SIZE_DEFAULT));
        }

        public void pushCommand(String key, String value) {
            this.mCommands.add(new Command(key, value, ALIGN_LEFT, SIZE_DEFAULT));
        }

        public void pushCommand(String key, String value, String align, String size) {
            this.mCommands.add(new Command(key, value, align, size));
        }

        public List<Command> getCommands() {
            return mCommands;
        }

        public class Command {
            private String key;
            private String value;
            private String align;
            private String size;

            public Command(String key, String value) {
                this.key = key;
                this.value = value;
                this.align = ALIGN_LEFT;
                this.size = SIZE_DEFAULT;
            }

            public Command(String key, String value, String align, String size) {
                this.key = key;
                this.value = value;
                this.align = align;
                this.size = size;
            }

            public String getKey() {
                return key;
            }

            public void setKey(String key) {
                this.key = key;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public String getAlign() {
                if (align == null) {
                    return ALIGN_LEFT;
                }
                return align;
            }

            public void setAlign(String align) {
                this.align = align;
            }

            public String getSize() {
                if (size == null) {
                    return SIZE_DEFAULT;
                }
                return size;
            }

            public void setSize(String size) {
                this.size = size;
            }
        }

        public static class Builder {
            private Paper mPaper;

            public Builder() {
                mPaper = new Paper();
            }

            public Builder text(String text) {
                mPaper.pushCommand(COMMAND_TEXT, text, ALIGN_LEFT, SIZE_DEFAULT);
                return this;
            }

            public Builder text(String text, String align) {
                mPaper.pushCommand(COMMAND_TEXT, text, align, SIZE_DEFAULT);
                return this;
            }

            public Builder text(String text, String align, String size) {
                mPaper.pushCommand(COMMAND_TEXT, text, align, size);
                return this;
            }

            public Builder image(String base64Image) {
                mPaper.pushCommand(COMMAND_IMAGE, base64Image);
                return this;
            }

            public Builder barCode(String value, String align) {
                mPaper.pushCommand(COMMAND_BARCODE, value, align, SIZE_DEFAULT);
                return this;
            }

            public Builder newLine() {
                mPaper.pushCommand(COMMAND_NEW_LINE, "\n");
                return this;
            }

            public Builder line() {
                mPaper.pushCommand(COMMAND_LINE);
                return this;
            }

            public Builder barCode(String value) {
                mPaper.pushCommand(COMMAND_BARCODE, value);
                return this;
            }

            public Builder qrCode(String value) {
                mPaper.pushCommand(COMMAND_QRCODE, value);
                return this;
            }

            public Builder cutPaper() {
                mPaper.pushCommand(COMMAND_CUT_PAPER);
                return this;
            }

            public Paper create() {
                return mPaper;
            }
        }
    }
}
