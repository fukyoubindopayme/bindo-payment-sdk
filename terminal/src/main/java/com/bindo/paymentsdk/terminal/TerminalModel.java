package com.bindo.paymentsdk.terminal;

public class TerminalModel {
    public static final String LANDI_APOS_A8 = "APOS A8";
    public static final String LANDI_APOS_A9 = "APOS A9";
    public static final String LANDI_P960 = "P960";
    public static final String UROVO_I9000S = "i9000S";
}
