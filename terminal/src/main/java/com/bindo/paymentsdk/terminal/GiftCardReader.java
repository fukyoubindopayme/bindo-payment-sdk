package com.bindo.paymentsdk.terminal;

public interface GiftCardReader {
    boolean isSupported();
    void setListener(Listener listener);
    void start();
    void stop();

    interface Listener {
        void onStart();
        void onStop();
        void onResult(String track1, String track2, String track3);
        void onError(String code, String message);
    }
}
