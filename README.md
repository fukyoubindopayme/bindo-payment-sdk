# Bindo Payment SDK

## How to use

Simply add the repository to your build.gradle file:

```
repositories {
    maven { url 'http://jfrog-artifactory-oss.bindolabs.com/artifactory/libs-release-local' }
    ...
}
```

And you can use the artifacts like this:

```gradle
ext {
    paymentSdkVersion = '2.0.115-SNAPSHOT@aar'
}
dependencies {
    compile "com.bindo.paymentsdk:payx-emv:$paymentSdkVersion"
    compile "com.bindo.paymentsdk:payx-emv-landi:$paymentSdkVersion"
    compile "com.bindo.paymentsdk:payx-emv-urovo:$paymentSdkVersion"
    compile "com.bindo.paymentsdk:terminal:$paymentSdkVersion"
    compile "com.bindo.paymentsdk:terminal-general:$paymentSdkVersion"
    compile "com.bindo.paymentsdk:terminal-landi:$paymentSdkVersion"
    compile "com.bindo.paymentsdk:terminal-urovo:$paymentSdkVersion"
    compile "com.bindo.paymentsdk:v3-pay:$paymentSdkVersion"
    compile "com.bindo.paymentsdk:v3-pay-common:$paymentSdkVersion"
    compile "com.bindo.paymentsdk:v3-pay-gateway:$paymentSdkVersion"
    compile "com.bindo.paymentsdk:v3-pay-gateway-amex:$paymentSdkVersion"
}
```

Notes:

    Nothing
    
## Development

### Update your home gradle.properties

This will include the username and password to upload to the Maven server and so that they are kept local on your machine. The location defaults to `USER_HOME/.gradle/gradle.properties`.

It may also include your signing key id, password, and secret key ring file (for signed uploads). Signing is only necessary if you're putting release builds of your project on maven central.

```
// http://jfrog-artifactory-oss.bindolabs.com
NEXUS_USERNAME=admin
NEXUS_PASSWORD=4wA2s[Yp92

signing.keyId=ABCDEF12
signing.password=n1c3try
signing.secretKeyRingFile=~/.gnupg/secring.gpg
```

### Build and Push

You can now build and push:

```
$ ./gradlew uploadArchives
```

### Export jar files

```
$ ./gradlew exportJar
```

***output jar files path in `/dist/<VERSION_NAME>`***

## SDK Documents

### Devices
- Landi APOS A8 https://bindolabs.atlassian.net/browse/BPAY-212
- Landi P960 https://bindolabs.atlassian.net/browse/BPAY-302
- Urovo I9000s https://bindolabs.atlassian.net/browse/BPAY-213

### Payment Gateways
- American Express https://bindolabs.atlassian.net/browse/SPS-1347
- Octopus https://bindolabs.atlassian.net/browse/BPAY-211