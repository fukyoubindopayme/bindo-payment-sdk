package com.bindo.paymentsdk.v3.pay.gateway;

import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Request;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Response;

/**
 * 支付网关
 */
public interface Gateway {
    /**
     * 发起一个交易请求，所有类型的交易均通过些方法发起
     *
     * @param request
     * @return
     */
    Response sendRequest(Request request);
}
