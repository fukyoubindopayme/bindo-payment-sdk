package com.bindo.paymentsdk.terminal.landi;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.Base64;

import com.bindo.paymentsdk.terminal.Printer.Paper;
import com.bindo.paymentsdk.terminal.Printer.Listener;
import com.landicorp.android.eptapi.device.Printer;
import com.landicorp.android.eptapi.utils.ImageTransformer;
import com.landicorp.android.eptapi.utils.QrCode;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;

public class APOSA8PrinterProgress extends Printer.Progress {
    private static final String TAG = "APOSA8PrinterProgress";

    private Paper mPaper;
    private Listener mListener;

    public APOSA8PrinterProgress(Paper paper, Listener listener) {
        this.mPaper = paper;
        this.mListener = listener;
    }

    @Override
    public void doPrint(Printer printer) throws Exception {
        Printer.Format format = new Printer.Format();

        List<Paper.Command> commands = mPaper.getCommands();

        format.setAscSize(Printer.Format.ASC_DOT5x7);
        format.setAscScale(Printer.Format.ASC_SC1x2);

        for (Paper.Command command : commands) {
            Printer.Alignment align = Printer.Alignment.LEFT;
            switch (command.getAlign()) {
                case Paper.ALIGN_LEFT:
                    align = Printer.Alignment.LEFT;
                    break;
                case Paper.ALIGN_CENTER:
                    align = Printer.Alignment.CENTER;
                    break;
                case Paper.ALIGN_RIGHT:
                    align = Printer.Alignment.RIGHT;
                    break;
                default:
                    break;
            }

            switch (command.getSize()) {
                case Paper.SIZE_DEFAULT:
                    format.setAscScale(Printer.Format.ASC_SC1x1);
                    break;
                case Paper.SIZE_LARGE:
                    format.setAscScale(Printer.Format.ASC_SC1x2);
                    break;
                case Paper.SIZE_BIG:
                    format.setAscScale(Printer.Format.ASC_SC2x2);
                    break;
            }
            printer.setFormat(format);

            switch (command.getKey()) {
                case Paper.COMMAND_NEW_LINE:
                    printer.printText("\n");
                    break;
                case Paper.COMMAND_TEXT:
                    printer.printText(align, String.format("%s\n", command.getValue()));
                    break;
                case Paper.COMMAND_IMAGE:
                    byte[] base64Bytes = Base64.decode(command.getValue(), Base64.DEFAULT);
                    Bitmap originalBitmap = BitmapFactory.decodeByteArray(base64Bytes, 0, base64Bytes.length);

                    final float scale = (384f / (float) originalBitmap.getWidth());

                    final int width = (int) ((float) originalBitmap.getWidth() * scale);
                    final int height = (int) ((float) originalBitmap.getHeight() * scale);

                    Bitmap resizedBitmap = Bitmap.createScaledBitmap(originalBitmap, width, height, false);

                    Bitmap newBitmap = Bitmap.createBitmap(resizedBitmap.getWidth(), resizedBitmap.getHeight(), resizedBitmap.getConfig());
                    Canvas canvas = new Canvas(newBitmap);
                    canvas.drawColor(Color.WHITE);
                    canvas.drawBitmap(resizedBitmap, 0, 0, null);

                    ByteArrayOutputStream byteArrayOutputStream = ImageTransformer.convert1BitBmp(newBitmap);
                    InputStream inputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
                    printer.printImage(align, inputStream);
                    break;
                case Paper.COMMAND_LINE:
                    printer.printText(Printer.Alignment.CENTER, "--------------------------------\n");
                    break;
                case Paper.COMMAND_BARCODE:
                    printer.printBarCode(align, command.getValue());
                    break;
                case Paper.COMMAND_QRCODE:
                    printer.printQrCode(align, new QrCode(command.getValue(), QrCode.ECLEVEL_Q), 100);
                    break;
                case Paper.COMMAND_CUT_PAPER:
                    printer.feedLine(4);
                    break;
            }
        }
    }

    @Override
    public void onFinish(int code) {
        if (code == Printer.ERROR_NONE) {
            mListener.onPrinted();
            return;
        }
        String message = getErrorDescription(code);
        mListener.onError(String.valueOf(code), message);
    }

    @Override
    public void onCrash() {
        String message = "unknown error";
        mListener.onError("-1", message);
    }

    private String getErrorDescription(int code) {
        switch (code) {
            case Printer.ERROR_PAPERENDED:
                return "Paper-out, the operation is invalid this time";
            case Printer.ERROR_HARDERR:
                return "Hardware fault, can not find HP signal";
            case Printer.ERROR_OVERHEAT:
                return "Overheat";
            case Printer.ERROR_BUFOVERFLOW:
                return "The operation buffer mode position is out of range";
            case Printer.ERROR_LOWVOL:
                return "Low voltage protect";
            case Printer.ERROR_PAPERENDING:
                return "Paper-out, permit the latter operation";
            case Printer.ERROR_MOTORERR:
                return "The printer core fault (too fast or too slow)";
            case Printer.ERROR_PENOFOUND:
                return "Automatic positioning did not find the alignment position, the paper back to its original position";
            case Printer.ERROR_PAPERJAM:
                return "paper got jammed";
            case Printer.ERROR_NOBM:
                return "Black mark not found";
            case Printer.ERROR_BUSY:
                return "The printer is busy";
            case Printer.ERROR_BMBLACK:
                return "Black label detection to black signal";
            case Printer.ERROR_WORKON:
                return "The printer power is open";
            case Printer.ERROR_LIFTHEAD:
                return "Printer head lift";
            case Printer.ERROR_LOWTEMP:
                return "Low temperature protect";
        }
        return "Unknown error (" + code + ")";
    }
}
