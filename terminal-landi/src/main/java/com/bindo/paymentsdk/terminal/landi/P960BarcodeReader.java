package com.bindo.paymentsdk.terminal.landi;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

import com.bindo.paymentsdk.terminal.BarcodeReader;
import com.landicorp.android.eptapi.device.InnerScanner;

import java.io.IOException;

public class P960BarcodeReader implements BarcodeReader {
    private static final String TAG = "P960BarcodeReader";

    private InnerScanner scanner;
    private Listener mListener;
    private SoundPool soundPool;
    private int soundId;
    private boolean started = false;
    private InnerScanner.OnScanListener listener = new InnerScanner.OnScanListener() {

        @Override
        public void onScanSuccess(String s) {
            soundPool.play(soundId, 1, 1, 0, 0, 1);
            mListener.onResult(Format.NONE, s);
        }

        @Override
        public void onScanFail(int i) {
            mListener.onError(String.valueOf(i), "");
        }

        @Override
        public void onCrash() {
            mListener.onError("Crash", "");
        }
    };

    public P960BarcodeReader(Context context) {
        scanner = InnerScanner.getInstance();
        soundPool = new SoundPool(1, AudioManager.STREAM_NOTIFICATION, 100);
        try {
            soundId = soundPool.load(context.getAssets().openFd("Scan_new.ogg"), 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isSupported() {
        return true;
    }

    @Override
    public void setListener(Listener listener) {
        mListener = listener;
    }

    @Override
    public void openScanner() {
        mListener.onOpen();
    }

    @Override
    public void closeScanner() {
        mListener.onClose();
    }

    @Override
    public void startDecode() {
    }

    public void startScan() {
        if(started) return;
        started = true;
        scanner.setOnScanListener(listener);
        scanner.start(0);
    }

    public void stopScan() {
        started = false;
        scanner.stopListen();
        scanner.stop();
    }

    @Override
    public void stopDecode() {

    }
}
