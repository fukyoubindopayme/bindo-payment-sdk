package com.bindo.paymentsdk.terminal.landi;

import com.bindo.paymentsdk.terminal.OctopusCardReader;

public class APOSA8OctopusCardReader implements OctopusCardReader {
    private Listener mListener;

    @Override
    public boolean isSupported() {
        return false;
    }

    @Override
    public void setListener(Listener listener) {
        mListener = listener;
    }

    @Override
    public void start() {
        mListener.onError("-1", "This device may not be supported.");
    }

    @Override
    public void stop() {
        mListener.onError("-1", "This device may not be supported.");
    }
}
