package com.bindo.paymentsdk.terminal.landi;

import android.app.Activity;
import android.content.Context;

import com.bindo.paymentsdk.terminal.BarcodeReader;
import com.landicorp.android.scan.scanDecoder.ScanDecoder;

import java.util.HashMap;

public class APOSA8BarcodeReader implements BarcodeReader, ScanDecoder.ResultCallback {
    private static final String TAG = "APOSA8BarcodeReader";

    private Context mContext;
    private ScanDecoder mScanDecoder;
    private Listener mListener;

    public APOSA8BarcodeReader(Context context) {
        mContext = context;
        mScanDecoder = new ScanDecoder(mContext);
    }

    @Override
    public boolean isSupported() {
        return true;
    }

    @Override
    public void setListener(Listener listener) {
        mListener = listener;
    }

    @Override
    public void openScanner() {
        mScanDecoder.Create(ScanDecoder.CAMERA_ID_BACK, this);
    }

    @Override
    public void closeScanner() {
        mScanDecoder.Destroy();
    }

    @Override
    public void startDecode() {
        HashMap<String, String> params = new HashMap<>();
        mScanDecoder.startScanDecode((Activity) mContext, params);
    }

    @Override
    public void stopDecode() {
        mScanDecoder.Destroy();
    }

    @Override
    public void startScan() {

    }

    @Override
    public void stopScan() {

    }

    @Override
    public void onResult(String result) {
        mListener.onResult(Format.NONE, result);
    }

    @Override
    public void onCancel() {
        mListener.onError("-1", "User Cancel");
    }

    @Override
    public void onTimeout() {
        mListener.onError("-1", "Timeout");
    }
}
