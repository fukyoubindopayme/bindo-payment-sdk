package com.bindo.paymentsdk.terminal.landi;

import com.bindo.paymentsdk.terminal.Printer;
import com.landicorp.android.eptapi.exception.RequestException;

public class APOSA8Printer extends Printer {
    private static final String TAG = "APOSA8Printer";

    @Override
    public boolean isSupported() {
        return true;
    }

    @Override
    public void print(Paper paper) {
        try {
            new APOSA8PrinterProgress(paper, mListener).start();
        } catch (RequestException e) {
            mListener.onError("-1", e.getMessage());
        }
    }
}
