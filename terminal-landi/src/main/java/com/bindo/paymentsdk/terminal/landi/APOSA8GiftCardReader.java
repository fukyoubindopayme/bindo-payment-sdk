package com.bindo.paymentsdk.terminal.landi;

import android.util.Log;

import com.bindo.paymentsdk.terminal.GiftCardReader;
import com.landicorp.android.eptapi.device.Beeper;
import com.landicorp.android.eptapi.device.MagCardReader;
import com.landicorp.android.eptapi.exception.RequestException;

public class APOSA8GiftCardReader implements GiftCardReader {
    private static final String TAG = "APOSA8GiftCardReader";

    private MagCardReader.OnSearchListener mOnSearchListener;
    private Listener mListener;

    public APOSA8GiftCardReader() {
    }

    @Override
    public boolean isSupported() {
        return true;
    }

    @Override
    public void setListener(Listener listener) {
        mListener = listener;
    }

    @Override
    public void start() {
        if (mOnSearchListener == null) {
            mOnSearchListener = new MagCardReader.OnSearchListener() {

                @Override
                public boolean checkValid(int[] trackStates, String[] track) {
                    String track1 = null;
                    if (track != null) {
                        if (track.length > 0) {
                            track1 = "%" + track[0] + "?";
                        }
                    }
                    Beeper.startBeep(200);
                    // track1 必须存在 BINDO 字符串，否则为不正确的 Gift Card。
                    return !(track1 == null || !track1.contains("BINDO"));
                }

                @Override
                public void onCardStriped(boolean[] hasTrack, final String[] track) {
                    String track1 = null, track2 = null, track3 = null;
                    // Gift Card 的 track1 格式为 `%` 开始 `?` 结尾
                    if (track != null) {
                        if (track.length > 0)
                            track1 = "%" + track[0] + "?";
                        if (track.length > 1)
                            track2 = track[1];
                        if (track.length > 2)
                            track3 = track[2];
                    }
                    mListener.onResult(track1, track2, track3);
                }

                @Override
                public void onFail(int code) {
                    Log.d(TAG, String.format("onFail, code: %d", code));

                    String errorCode = String.valueOf(code);
                    String errorMessage = String.format("Oops, an error occurred! Code: %d", code);

                    switch (code) {
                        case ERROR_NODATA:
                            errorMessage = "No data";
                            break;
                        case ERROR_NEEDSTART:
                            errorMessage = "need restart search";
                            break;
                        case ERROR_INVALID:
                            errorMessage = "Invalid gift card";
                            break;
                    }
                    mListener.onError(errorCode, errorMessage);
                }

                @Override
                public void onCrash() {
                    Log.d(TAG, "Carsh");
                    mListener.onError("-1", String.format("Oops, an error occurred! Code: %d", -1));
                }
            };
        }
        try {
            MagCardReader.getInstance().enableTrack(MagCardReader.TRK1 | MagCardReader.TRK2 | MagCardReader.TRK3);
            MagCardReader.getInstance().setLRCCheckEnabled(false);
            MagCardReader.getInstance().searchCard(mOnSearchListener);
            mListener.onStart();
        } catch (RequestException e) {
            mListener.onError("-1", e.getMessage());
        }
    }

    @Override
    public void stop() {
        try {
            mOnSearchListener = null;
            MagCardReader.getInstance().stopSearch();
            mListener.onStop();
        } catch (RequestException e) {
            mListener.onError("-1", e.getMessage());
        }
    }
}
