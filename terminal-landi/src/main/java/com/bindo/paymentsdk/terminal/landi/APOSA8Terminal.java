package com.bindo.paymentsdk.terminal.landi;

import android.content.Context;

import com.bindo.paymentsdk.terminal.BarcodeReader;
import com.bindo.paymentsdk.terminal.GiftCardReader;
import com.bindo.paymentsdk.terminal.OctopusCardReader;
import com.bindo.paymentsdk.terminal.Printer;
import com.bindo.paymentsdk.terminal.Terminal;

public class APOSA8Terminal implements Terminal {

    private Printer mBuiltInPrinter;
    private BarcodeReader mBarcodeReader;
    private GiftCardReader mGiftCardReader;
    private OctopusCardReader mOctopusCardReader;

    public APOSA8Terminal(Context context) {
        mBuiltInPrinter = new APOSA8Printer();
        mBarcodeReader = new APOSA8BarcodeReader(context);
        mGiftCardReader = new APOSA8GiftCardReader();
        mOctopusCardReader = new APOSA8OctopusCardReader();
    }

    @Override
    public Printer getBuiltInPrinter() {
        return mBuiltInPrinter;
    }

    @Override
    public BarcodeReader getBarcodeReader() {
        return mBarcodeReader;
    }

    @Override
    public GiftCardReader getGiftCardReader() {
        return mGiftCardReader;
    }

    @Override
    public OctopusCardReader getOctopusCardReader() {
        return mOctopusCardReader;
    }
}
