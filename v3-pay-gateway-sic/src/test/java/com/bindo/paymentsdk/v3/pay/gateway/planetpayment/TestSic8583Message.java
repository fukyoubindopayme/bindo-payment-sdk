package com.bindo.paymentsdk.v3.pay.gateway.planetpayment;

import com.bindo.paymentsdk.v3.pay.gateway.sic.SicPaymentGateway;
import com.bindo.paymentsdk.v3.pay.gateway.sic.Utils.IReadResources;
import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.MessageFactory;
import com.solab.iso8583.config.BcdCodecConfig;
import com.solab.iso8583.config.ConfigTypeDef.*;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/** These are very simple tests for creating and manipulating messages.
 * 
 * @author Dave.Zhan
 */
public class TestSic8583Message {

	private MessageFactory<IsoMessage> mf;

	@Before
	public void init() throws IOException {
		mf = new MessageFactory<>(SicPaymentGateway.PAYMENT_GATEWAY_NAME);
		mf.setCharacterEncoding("UTF-8");
		mf.setUseBinaryMessages(true);
		mf.setBcdCodecConfig(new BcdCodecConfig(BcdFieldLenType.HEX_BYTE_COUNT, FillPosition.RIGHT, FillByteType.BYTE_0));
		mf.setConfigFile(SicPaymentGateway.getIReadResources().read8583configXmlfile());
	}

	@Test
	public void testEncoding() throws Exception {
		IsoMessage m1 = mf.newMessage(0x001);
		byte[] buf = m1.writeData();
	}

	@Test
	public void testTemplating() {
		IsoMessage iso1 = mf.newMessage(0x001);
		IsoMessage iso2 = mf.newMessage(0x001);
        Assert.assertNotSame(iso1, iso2);
	}

}
