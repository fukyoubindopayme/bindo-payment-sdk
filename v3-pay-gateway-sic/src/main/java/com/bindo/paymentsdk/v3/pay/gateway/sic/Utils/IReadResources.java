package com.bindo.paymentsdk.v3.pay.gateway.sic.Utils;

import java.io.InputStream;

public interface IReadResources {
    /**
     * 读取配置好的XML文件，返回文件字节流（Android和Java后台分别自己实现此方法）
     * @return
     */
    InputStream read8583configXmlfile();
}
