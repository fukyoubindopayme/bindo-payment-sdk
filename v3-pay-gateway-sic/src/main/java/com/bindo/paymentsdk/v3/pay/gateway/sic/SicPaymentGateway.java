package com.bindo.paymentsdk.v3.pay.gateway.sic;

import com.bindo.paymentsdk.v3.pay.common.gateway.SocketService;
import com.bindo.paymentsdk.v3.pay.common.CardDetectMode;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Request;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Response;
import com.bindo.paymentsdk.v3.pay.common.util.Hex;
import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.common.util.StringUtils;
import com.bindo.paymentsdk.v3.pay.gateway.Gateway;
import com.bindo.paymentsdk.v3.pay.gateway.sic.Utils.IReadResources;
import com.solab.iso8583.util.BytesUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.SSLException;

public class SicPaymentGateway implements Gateway {
    private static final String TAG = "SicPaymentGateway";

    public static final String PAYMENT_GATEWAY_NAME = "SicPaymentGateway";
    private static IReadResources mIReadResources; // 设置为静态变量，只需设置一次接口即可。
    private ISO8583Conversion mISO8583Conversion;

    // Gateway 请求参数
    public static final String CONFIG_REQUEST_CARD_ACCEPTOR_TERMINAL_ID = "config_request_card_acceptor_terminal_id";
    public static final String CONFIG_REQUEST_CARD_ACCEPTOR_ID_CODE = "config_request_card_acceptor_id_code";
    public static final String CONFIG_REQUEST_TPDU = "config_request_tpdu";
    public static final String CONFIG_REQUEST_KEYSTORE_TERMINAL = SocketService.KEYSTORE_TERMINAL;
    public static final String CONFIG_REQUEST_KEYSTORE_TRUSTED = SocketService.KEYSTORE_TRUSTED;
    // response parameters
    public static final String CONFIG_RESPONSE_MANUFACTURER_PUBLICK_KEY = "config_response_manufacturer_public_key"; //厂商公钥
    public static final String CONFIG_RESPONSE_TMK = "config_response_tmk"; //主密钥


    public static final int ISOMESSAGE_TOTAL_BYTES_NUM = 2; // 报文总长度占用字节数
    public static final int TPDU_BYTES_LEN = 5;             // TPDU占用字节长度
    public static final int ISOMESSAGE_HEADER_BYTES_LEN = 6;          // 报文头占用字节长度

    private final String DEFAULT_REQUEST_CARD_ACCEPTOR_TERMINAL_ID = "00001918";
    private final String DEFAULT_REQUEST_CARD_ACCEPTOR_ID_CODE = "549581041215001";
    private final String DEFAULT_REQUEST_IP = "210.48.142.168";
    private final String DEFAULT_REQUEST_PORT = "6000";
    private final String DEFAULT_REQUEST_TPDU = "6000060000";

    private HashMap<String, Object> mConfigMap = new HashMap<>();


    private SocketService mSocketService = new SocketService();

    public SicPaymentGateway(HashMap<String, Object> configMap) {
        // Gateway 参数默认值
        mConfigMap.put(CONFIG_REQUEST_CARD_ACCEPTOR_TERMINAL_ID, DEFAULT_REQUEST_CARD_ACCEPTOR_TERMINAL_ID);
        mConfigMap.put(CONFIG_REQUEST_CARD_ACCEPTOR_ID_CODE, DEFAULT_REQUEST_CARD_ACCEPTOR_ID_CODE);
        mConfigMap.put(CONFIG_REQUEST_TPDU, DEFAULT_REQUEST_TPDU);

        if (configMap == null) {
            Log.w(TAG, "Please set the gateway parameters");
        } else {
            for (String key : configMap.keySet()) {
                if (mConfigMap.containsKey(key)){
                    continue;
                }
                Object value = configMap.get(key);
                mConfigMap.put(key, value);
            }
        }


        mISO8583Conversion = new ISO8583Conversion();

        if(mIReadResources == null) {
            mIReadResources = new IReadResources() {
                @Override
                public InputStream read8583configXmlfile() {
                    Log.w(TAG, "default init invalid read8583configXmlfile!");
                    return null;
                }
            };
        }
    }

    public static void setIReadResources(IReadResources iReadResources) {
        Log.d(TAG, "iReadResources = " + iReadResources);
        if(iReadResources != null) {
            mIReadResources = iReadResources;
        }
    }
    public static IReadResources getIReadResources() {
        return mIReadResources;
    }

    @Override
    public Response sendRequest(Request request) {

        if (request.getCardDetectMode() != null) {
            String posEntryMode = "";

            switch (request.getCardDetectMode()) {
                case CardDetectMode.CONTACT:
                    posEntryMode = "05";
                    break;
                case CardDetectMode.CONTACTLESS:
                    posEntryMode = "07";
                    break;
                case CardDetectMode.SWIPE:
                    posEntryMode = "02";
                    break;
                case CardDetectMode.FALLBACK_SWIPE:
                    posEntryMode = "02";
                    break;
                case CardDetectMode.MANUAL:
                    posEntryMode = "01";
                    break;
                case CardDetectMode.QRCODE:
                    posEntryMode = "03";
                    break;
            }

            posEntryMode += "2";
            request.setPOSEntryMode(posEntryMode);
        }

        if (request.getTransactionType() != null) {
            request.setPOSConditionCode("00");
            switch (request.getTransactionType()) {
                case FACTORY_PUBLIC_KEY_DOWNLOAD:
                    request.setBatchNO(1);
                    break;
                default:
                    Log.e(TAG,"not support transaction type !");

            }
        }

        Response response = null;
        List<Response.Error> errors = new ArrayList<>();
        request.setTerminalIdentification((String) mConfigMap.get(CONFIG_REQUEST_CARD_ACCEPTOR_TERMINAL_ID));
        request.setMerchantIdentifier((String) mConfigMap.get(CONFIG_REQUEST_CARD_ACCEPTOR_ID_CODE));

        byte[] requestForISO8583RepoBytes = mISO8583Conversion.requestToISO8583RepoBytes(request);

        Log.d(TAG, "org iso8583Msg:"+Hex.encode(requestForISO8583RepoBytes));

        if(requestForISO8583RepoBytes == null || requestForISO8583RepoBytes.length <= 0) {
            Log.e(TAG, "requestForISO8583RepoBytes is invalid!!!");
            return response;
        }

        // Add TPDU
        byte[] finalISO8583Message = new byte[requestForISO8583RepoBytes.length + TPDU_BYTES_LEN];
        int offset = 0;
        String tpdu;
        if (mConfigMap.containsKey(CONFIG_REQUEST_TPDU))
        {
            tpdu = (String) mConfigMap.get(CONFIG_REQUEST_TPDU);

            if (!StringUtils.isEmpty(tpdu))
            {
                System.arraycopy(Hex.decode(tpdu), 0, finalISO8583Message, offset, Hex.decode(tpdu).length);
                offset += Hex.decode(tpdu).length;
            }
        }
        System.arraycopy(requestForISO8583RepoBytes, 0, finalISO8583Message, offset, requestForISO8583RepoBytes.length);
        Log.d(TAG, "final send iso8583Msg:"+Hex.encode(finalISO8583Message));

        // Add Message Total Length
        byte[] sendBytes = new byte[finalISO8583Message.length + ISOMESSAGE_TOTAL_BYTES_NUM];
        byte[] lenByte = BytesUtils.intToByteArray(finalISO8583Message.length);
        System.arraycopy(lenByte, ISOMESSAGE_TOTAL_BYTES_NUM, sendBytes, 0, ISOMESSAGE_TOTAL_BYTES_NUM);
        System.arraycopy(finalISO8583Message, 0, sendBytes, ISOMESSAGE_TOTAL_BYTES_NUM, finalISO8583Message.length);
        Log.d(TAG, "sendBytes :"+Hex.encode(sendBytes));

        try {
           mSocketService.setHostIp(DEFAULT_REQUEST_IP);
            mSocketService.setHostPort(DEFAULT_REQUEST_PORT);
            mSocketService.connect();

            if (mSocketService.isConnected()) {
                mSocketService.sendMessage(sendBytes);

                byte[] receivedDataBytes = mSocketService.receiveMessage();

                Log.d(TAG, "receivedBytes :"+Hex.encode(receivedDataBytes));

                if(receivedDataBytes != null && receivedDataBytes.length > TPDU_BYTES_LEN) {
                    // remove TPDU
                    byte[] receiveIsoMsg = new byte[receivedDataBytes.length - TPDU_BYTES_LEN];
                    System.arraycopy(receivedDataBytes, TPDU_BYTES_LEN, receiveIsoMsg, 0, receivedDataBytes.length-TPDU_BYTES_LEN);

                    if(receiveIsoMsg.length > ISOMESSAGE_HEADER_BYTES_LEN) {
                        response = mISO8583Conversion.iso8583RepoBytesToResponse(request.getTransactionType(), receiveIsoMsg);
                    }
                }
            }
        } catch (SSLException e) {
            // TODO: Handle ssl exception
            e.printStackTrace();
            errors.add(new Response.Error("-1", e.getMessage()));
        } catch (SocketException e) {
            // TODO: Handle socket exception
            
            e.printStackTrace();
            errors.add(new Response.Error("-1", e.getMessage()));
        } catch (Exception e) {
            // TODO: Handle socket exception
            e.printStackTrace();
            errors.add(new Response.Error("-1", e.getMessage()));
        } finally {
            try {
                mSocketService.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response == null) {
                response = new Response();
                response.setErrors(errors);
            }
        }

        return response;
    }
}
