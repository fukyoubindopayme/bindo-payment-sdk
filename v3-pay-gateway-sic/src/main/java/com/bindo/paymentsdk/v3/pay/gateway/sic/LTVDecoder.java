package com.bindo.paymentsdk.v3.pay.gateway.sic;

/**
 * Created by Mark on 2017/8/31.
 */

public class LTVDecoder {

    public static void ParseLTVFormatedData(String msg, OnDecodeEvent onDecodeEvent)
    {
        int offset = 0;
        int len = 0;
        String fl;
        String tag;
        String value;

        do {
            fl = msg.substring(offset, 4+offset);
            offset += 4;
            len = Integer.parseInt(fl);
            tag = msg.substring(offset, offset+4);
            value = msg.substring(offset+4, offset+len*2);
            onDecodeEvent.onFectchTag(tag, value);
            offset += len*2;
        }while(offset < msg.length());
    }

    public interface OnDecodeEvent{
        public void onFectchTag(String tag, String value);
    }
}
