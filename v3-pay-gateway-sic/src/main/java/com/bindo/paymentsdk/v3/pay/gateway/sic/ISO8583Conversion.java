package com.bindo.paymentsdk.v3.pay.gateway.sic;

import android.text.TextUtils;

import com.bindo.paymentsdk.v3.pay.common.TransactionType;
import com.bindo.paymentsdk.v3.pay.common.database.models.CountryToCurrency;
import com.bindo.paymentsdk.v3.pay.common.database.models.Currency;
import com.bindo.paymentsdk.v3.pay.common.database.models.LocalBIN;
import com.bindo.paymentsdk.v3.pay.common.database.models.PYCTable;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Request;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Response;
import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.solab.iso8583.CustomField;
import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoValue;
import com.solab.iso8583.MessageFactory;
import com.solab.iso8583.codecs.CompositeField;
import com.solab.iso8583.config.BcdCodecConfig;
import com.solab.iso8583.config.ConfigTypeDef.*;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.bindo.paymentsdk.v3.pay.common.TransactionType.FACTORY_PUBLIC_KEY_DOWNLOAD;
import static com.bindo.paymentsdk.v3.pay.common.TransactionType.TMK_DOWNLOAD;
import static com.solab.iso8583.IsoType.ALPHA;
import static com.solab.iso8583.IsoType.NUMERIC;

public class ISO8583Conversion {

    public static final String TAG = "ISO8583Conversion";
    private static MessageFactory<IsoMessage> mf; // 设置为静态变量，只用解析一次XML文件

    public ISO8583Conversion() {
        Log.d(TAG, "ISO8583Conversion: in");
        if(mf == null) {
            mf = new MessageFactory<>(SicPaymentGateway.PAYMENT_GATEWAY_NAME);
            try {
                mf.setCharacterEncoding("UTF-8");
                mf.setUseBinaryMessages(true);
                mf.setBcdCodecConfig(new BcdCodecConfig(BcdFieldLenType.HEX_BYTE_COUNT, FillPosition.RIGHT, FillByteType.BYTE_0));
                mf.setConfigFile(SicPaymentGateway.getIReadResources().read8583configXmlfile());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 根据请求数据Request对象，组交易请求报文
     * @param request
     * @return
     */
    public byte[] requestToISO8583RepoBytes(Request request) {

        byte[] result = null;
        if (request == null) {
            return result;
        }
        IsoMessage isoMessage = null;
        if (request.getTransactionType() != null) {
            switch (request.getTransactionType()) {
                case FACTORY_PUBLIC_KEY_DOWNLOAD:
                    isoMessage = mf.newMessage(0x0001);
                    int len = isoMessage.getField(41).getLength();
                    IsoValue<String> value = new IsoValue<>(SicPaymentGateway.PAYMENT_GATEWAY_NAME, ALPHA, request.getTerminalIdentification(), len);
                    isoMessage.setField(41, value);

                    len = isoMessage.getField(42).getLength();
                    value = new IsoValue<>(SicPaymentGateway.PAYMENT_GATEWAY_NAME, ALPHA, request.getMerchantIdentifier(), len);
                    isoMessage.setField(42, value);

                    CompositeField f = isoMessage.getObjectValue(60);
                    List<IsoValue> list = f.getValues();
                    len = list.get(1).getLength();
                    list.set(1, new IsoValue<>(SicPaymentGateway.PAYMENT_GATEWAY_NAME, NUMERIC, request.getBatchNO(), len));
                    break;
                default:
                    Log.e(TAG,"not support transaction type !");

            }
        }

        if(isoMessage != null) {
            result = isoMessage.writeData();
        }
        return result;
    }

    /**
     * 根据交易类型和应答报文，组装应答数据Response
     * @param transactionType
     * @param iso8583RepoBytes
     * @return
     */
    public Response iso8583RepoBytesToResponse(TransactionType transactionType, byte[] iso8583RepoBytes) {
        Response response = new Response();

        if(transactionType == null || iso8583RepoBytes == null ||
                iso8583RepoBytes.length < SicPaymentGateway.ISOMESSAGE_HEADER_BYTES_LEN) {
            return response;
        }
        IsoMessage isoMessage = null;
        switch (transactionType) {
            case FACTORY_PUBLIC_KEY_DOWNLOAD:
                try {
                    isoMessage = mf.parseMessage(iso8583RepoBytes, SicPaymentGateway.ISOMESSAGE_HEADER_BYTES_LEN, 0x0001);
                    response = formCommonReponseByMsg(isoMessage);
                    String keyHexString = "";

                    HashMap<String, String> configMap = new HashMap<>();
                    configMap.put(SicPaymentGateway.CONFIG_RESPONSE_MANUFACTURER_PUBLICK_KEY, keyHexString);
                    response.setGatewayConfigs(configMap);
                } catch (ParseException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                break;
            default:
                Log.e(TAG,"not support transaction type !");
        }

        return response;
    }

    /**
     * 组公共的应答数据Response
     * @param isoMessage
     * @return
     */
    private Response formCommonReponseByMsg(IsoMessage isoMessage) {
        Response response = new Response();

        // transaction type
        int messageId = isoMessage.getId();
        if(getTransactionTypeFromMessageId(messageId) != null) {
           response.setTransactionType(getTransactionTypeFromMessageId(messageId).name());
        }
        // 39 field :response code
        String field39 = (String) isoMessage.getField(39).getValue();
        boolean isApproved = false;
        if(!TextUtils.isEmpty(field39)) {
           if(field39.equals("00")) {
               isApproved = true;
           }
        }
        response.setApproved(isApproved);

        return response;
    }



    private static HashMap<TransactionType, Integer> mType2MessageIdMap = new HashMap<>();

    /**
     * Bind transaction type and message Id.
     * 绑定交易类型枚举和String类型的报文Id,方便阅读代码使用
     */
    public static void bindTransactionTypeAndMessageId() {
        mType2MessageIdMap.put(FACTORY_PUBLIC_KEY_DOWNLOAD, 0x0001);
        mType2MessageIdMap.put(TMK_DOWNLOAD, 0x0002);
    }

    public static int getMessageIdFromTransactionType(TransactionType type) {
       int messageId = 0;
       if(type != null) {
           messageId = mType2MessageIdMap.get(type);
       }
       return messageId;
    }

    public static TransactionType getTransactionTypeFromMessageId(int messageId) {
        TransactionType type = null;
        Set<Map.Entry<TransactionType, Integer>> set = mType2MessageIdMap.entrySet();
        for (Map.Entry<TransactionType, Integer> entry:set) {
            if(entry.getValue().equals(messageId)) {
                type = entry.getKey();
            }
        }
        return type;
    }

}
