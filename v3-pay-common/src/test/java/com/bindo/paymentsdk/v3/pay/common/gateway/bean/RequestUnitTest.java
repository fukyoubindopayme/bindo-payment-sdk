package com.bindo.paymentsdk.v3.pay.common.gateway.bean;

import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class RequestUnitTest {

    @Test
    public void setTransactionDateAndTimeString_isCorrect() throws Exception {
        Request request = new Request();
        request.setTransactionDateAndTime("180528060110");
        assertEquals("180528", request.getTransactionDate());
        assertEquals("060110", request.getTransactionTime());
    }

    @Test
    public void setTransactionDateAndTimeDate_isCorrect() throws Exception {
        Request request = new Request();
        request.setTransactionDateAndTime(new Date(2018, Calendar.MAY, 28, 6, 1, 10));
        assertEquals("180528", request.getTransactionDate());
        assertEquals("060110", request.getTransactionTime());
    }
}
