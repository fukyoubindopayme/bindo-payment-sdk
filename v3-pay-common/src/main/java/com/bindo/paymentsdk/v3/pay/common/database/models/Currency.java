package com.bindo.paymentsdk.v3.pay.common.database.models;

import com.bindo.paymentsdk.v3.pay.common.util.JSONUtils;

public class Currency {
    private String entryNumber;
    private String currencyName;
    private String numericCode;
    private String alphabeticCode;
    private String terminalSymbol;
    private String decimalPlaces;
    private String ignoreDigits;
    private String conversionRate;
    private String rateExponent;
    private String decimalSymbol;
    private String separatorSymbol;
    private String roundUnit;
    private String currOpt;
    private String menuName;

    public String getEntryNumber() {
        return entryNumber;
    }

    public void setEntryNumber(String entryNumber) {
        this.entryNumber = entryNumber;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getNumericCode() {
        return numericCode;
    }

    public void setNumericCode(String numericCode) {
        this.numericCode = numericCode;
    }

    public String getAlphabeticCode() {
        return alphabeticCode;
    }

    public void setAlphabeticCode(String alphabeticCode) {
        this.alphabeticCode = alphabeticCode;
    }

    public String getTerminalSymbol() {
        return terminalSymbol;
    }

    public void setTerminalSymbol(String terminalSymbol) {
        this.terminalSymbol = terminalSymbol;
    }

    public String getDecimalPlaces() {
        return decimalPlaces;
    }

    public void setDecimalPlaces(String decimalPlaces) {
        this.decimalPlaces = decimalPlaces;
    }

    public String getIgnoreDigits() {
        return ignoreDigits;
    }

    public void setIgnoreDigits(String ignoreDigits) {
        this.ignoreDigits = ignoreDigits;
    }

    public String getConversionRate() {
        return conversionRate;
    }

    public void setConversionRate(String conversionRate) {
        this.conversionRate = conversionRate;
    }

    public String getRateExponent() {
        return rateExponent;
    }

    public void setRateExponent(String rateExponent) {
        this.rateExponent = rateExponent;
    }

    public String getDecimalSymbol() {
        return decimalSymbol;
    }

    public void setDecimalSymbol(String decimalSymbol) {
        this.decimalSymbol = decimalSymbol;
    }

    public String getSeparatorSymbol() {
        return separatorSymbol;
    }

    public void setSeparatorSymbol(String separatorSymbol) {
        this.separatorSymbol = separatorSymbol;
    }

    public String getRoundUnit() {
        return roundUnit;
    }

    public void setRoundUnit(String roundUnit) {
        this.roundUnit = roundUnit;
    }

    public String getCurrOpt() {
        return currOpt;
    }

    public void setCurrOpt(String currOpt) {
        this.currOpt = currOpt;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    @Override
    public String toString() {
        try {
            return JSONUtils.toJSONString(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.toString();
    }
}
