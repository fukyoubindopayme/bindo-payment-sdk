package com.bindo.paymentsdk.v3.pay.common.util;

public class StringUtils {
    public static boolean isEmpty(CharSequence str) {
        return  (str == null || str.length() == 0);
    }

    public static String padEnd(String s, int n) {
        return String.format("%1$-" + n + "s", s);
    }

    public static String padStart(String s, int n) {
        return String.format("%1$" + n + "s", s);
    }
}
