package com.bindo.paymentsdk.v3.pay.common.gateway;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.security.KeyStore;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;

import javax.net.SocketFactory;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

public class SocketService {
    public static final String KEYSTORE_TERMINAL = "keystore_terminal";
    public static final String KEYSTORE_TRUSTED = "keystore_trusted";

    private static final int MAX_BUFFER_SIZE = 1024;

    private String hostIp;
    private String hostPort;
    private int timeout;
    private BufferedInputStream bufferedInputStream;
    private OutputStream outputStream;
    private HashMap<String, KeyStore> keyStoreMap;
    private Socket sk;

    public SocketService() {
    }

    private SocketService(String hostIp, String hostPort, int timeout, HashMap<String, KeyStore> keyStoreMap) {
        this.hostIp = hostIp;
        this.hostPort = hostPort;
        this.timeout = timeout;
        this.keyStoreMap = keyStoreMap;
    }

    public String getHostIp() {
        return hostIp;
    }

    public void setHostIp(String hostIp) {
        this.hostIp = hostIp;
    }

    public String getHostPort() {
        return hostPort;
    }

    public void setHostPort(String hostPort) {
        this.hostPort = hostPort;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public HashMap<String, KeyStore> getKeyStoreMap() {
        return keyStoreMap;
    }

    public void setKeyStoreMap(HashMap<String, KeyStore> keyStoreMap) {
        this.keyStoreMap = keyStoreMap;
    }

    private Socket clientWithCert(KeyStore clientKs, KeyStore trustedKs, String ip, String port) throws Exception {
        //创建并初始化证书库工厂
        String alg = KeyManagerFactory.getDefaultAlgorithm();
        KeyManagerFactory kmf = KeyManagerFactory.getInstance(alg);
        kmf.init(clientKs, "123456".toCharArray());
//        // 创建并初始化信任库工厂
        String alg_trust = TrustManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(alg_trust);
        tmf.init(trustedKs);

        TrustManager[] trustAll = new TrustManager[]{
                new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {

                    }

                    @Override
                    public X509Certificate[] getAcceptedIssuers() {
                        return new X509Certificate[0];
                    }
                }
        };


        /**
         * TLS安全套接字
         * Returns a SSLContext object that implements the specified secure socket protocol.
         */
        SSLContext sslContext = SSLContext.getInstance("SSLv3");
        sslContext.init(kmf.getKeyManagers(), trustAll, null);

        SocketFactory factory = sslContext.getSocketFactory();
        sk = factory.createSocket(ip, Integer.parseInt(port));


        bufferedInputStream = new BufferedInputStream(sk.getInputStream());
        outputStream = sk.getOutputStream();

        return sk;
    }

    /**
     * 建立连接
     */
    public void connect() throws Exception {
        KeyStore keystoreTerminal = null;
        KeyStore keystoreTrusted = null;

        if (keyStoreMap != null) {

            if (keyStoreMap.containsKey(KEYSTORE_TERMINAL)) {
                keystoreTerminal = keyStoreMap.get(KEYSTORE_TERMINAL);
            }
            if (keyStoreMap.containsKey(KEYSTORE_TRUSTED)) {
                keystoreTrusted = keyStoreMap.get(KEYSTORE_TRUSTED);
            }
        }

        if (keystoreTerminal == null && keystoreTrusted == null) //非SSL模式
        {
            sk = new Socket(hostIp, Integer.parseInt(hostPort));
            bufferedInputStream = new BufferedInputStream(sk.getInputStream());
            outputStream = sk.getOutputStream();
        } else {//SSL模式
            clientWithCert(keystoreTerminal, keystoreTrusted, hostIp, hostPort);
        }

    }

    /**
     * 连接是否已经建立
     *
     * @return
     */
    public boolean isConnected() {
        return sk.isConnected();
    }

    /**
     * 断开连接
     */
    public void disconnect() throws IOException {
        if (bufferedInputStream != null) {
            bufferedInputStream.close();
        }
        if (outputStream != null) {
            outputStream.close();
        }
        if (sk != null) {
            sk.close();
        }
    }

    /**
     * 发送消息
     */
    public void sendMessage(byte[] messageBytes) throws IOException {
        if (sk.isConnected()) {
            if (outputStream != null) {
                outputStream.write(messageBytes);
            }
        }
    }

    /**
     * 接收消息
     */
    public byte[] receiveMessage() throws IOException {
        int ret = 0;
        byte[] message = new byte[1024];
        int offset = 0;
        int wantedLen = 0;

        wantedLen = 2;
        ret = bufferedInputStream.read(message, offset, wantedLen);
        wantedLen = (message[0] & 0xFF) * 256 + (message[1] & 0xFF);
        ret = bufferedInputStream.read(message, offset, wantedLen);

        byte[] receiveMsg = new byte[wantedLen];
        System.arraycopy(message, 0, receiveMsg, 0, receiveMsg.length);
        return receiveMsg;
    }

    public byte[] receiveMessageWithTimeout() throws IOException {
        int ret = 0;
        byte[] message = new byte[MAX_BUFFER_SIZE];
        int offset = 0;
        int wantedLen = 2;
        long maxTimeMillis = System.currentTimeMillis() + timeout * 1000;
        boolean lengthRead = false;

        while (System.currentTimeMillis() < maxTimeMillis) {
            if (!lengthRead) {
                if (bufferedInputStream.available() > 0) {
                    ret = bufferedInputStream.read(message, offset, wantedLen);
                    wantedLen = (message[0] & 0xFF) * 256 + (message[1] & 0xFF);
                    lengthRead = true;
                }
            }
            if (lengthRead) {
                if (bufferedInputStream.available() > 0) {
                    ret = bufferedInputStream.read(message, offset, bufferedInputStream.available());
                    offset += ret;
                    if (offset == wantedLen) {
                        return message;
                    }
                }
            }
        }
        throw new SocketTimeoutException();
    }
}
