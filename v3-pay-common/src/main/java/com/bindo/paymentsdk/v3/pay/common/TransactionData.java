package com.bindo.paymentsdk.v3.pay.common;

import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Request;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Response;
import com.bindo.paymentsdk.v3.pay.common.util.JSONUtils;
import com.bindo.paymentsdk.v3.pay.common.util.TagLengthValuesUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class TransactionData {
    public static final String FORMAT_DATE = "yyMMdd";
    public static final String FORMAT_TIME = "hhmmss";
    public static final String FORMAT_DATE_AND_TIME = "yyMMddhhmmss";

    private Request request;
    private Response response;

    private Request requestForReversal;
    private Response responseForReversal;

    private HashMap<String, byte[]> tagLengthValues;
    private HashMap<String, String> tagLengthStringValues;

    // Transaction fields.
    private String transactionId;
    private String transactionType;
    private int transactionSystemTraceAuditNumber;
    private String transactionDate;
    private String transactionTime;
    private String transactionDateAndTime;
    private double transactionAmount;
    private String transactionCurrencyCode;
    private String transactionCurrencyExponent;
    private String messageTypeId;
    private String processingCode;
    private boolean transactionPyc;
    private boolean transactionOptOut;
    private String transactionPYCRateMarkupSign;
    private String transactionPYCRateMarkupText;

    // Transaction result fields.
    private boolean isApproved;
    private String approvalCode;
    private String actionCode;
    private String actionCodeDescription;
    private double cardholderBillingAmount;
    private String conversionRate;
    private String retrievalReferenceNumber;
    private String authorizationCode;
    private String cardholderBillingCurrencyCode;

    // Card fields.
    private String primaryAccountNumber;
    private String cardExpirationDate;
    private String cardDetectMode;
    private String POSEntryMode;
    private String cardSequenceNumber;
    private String track1DiscretionaryData;
    private String track2DiscretionaryData;
    private String track2EquivalentData;
    private String iccRelatedData;

    private boolean transactionPinVerified;
    private boolean transactionRequestSignature;

    private String applicationLabel;
    private String applicationIdentifier;
    private String applicationCryptogram;
    private String terminalVerificationResult;
    private String transactionStatusInformation;
    private String availableOfflineSpendingAmount;

    private String terminalId;
    private String merchantId;

    private boolean upload; // wether offline transaction record is upload to host
    private String  issuerApplicationData;
    private String unpredictableNumber;
    private String applicationTransactionCounter;
    private String terminalVerificationResults;
    private String iccTransactionDate;
    private String iccTransactionType;
    private String amountAuthorized;
    private String iccTransactionCurrencyCode;
    private String terminalCountryCode;
    private String applicationInterchangeProfile;
    private String amountOther;
    private String applicationPanSequence;
    private String cryptogramInformationData;
    private String posDataCode;

    public TransactionData() {
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public Request getRequestForReversal() {
        return requestForReversal;
    }

    public void setRequestForReversal(Request requestForReversal) {
        this.requestForReversal = requestForReversal;
    }

    public Response getResponseForReversal() {
        return responseForReversal;
    }

    public void setResponseForReversal(Response responseForReversal) {
        this.responseForReversal = responseForReversal;
    }

    public HashMap<String, byte[]> getTagLengthValues() {
        return tagLengthValues;
    }

    public void setTagLengthValues(HashMap<String, byte[]> tagLengthValues) {
        this.tagLengthValues = tagLengthValues;
    }

    public HashMap<String, String> getTagLengthStringValues() {
        return tagLengthStringValues;
    }

    public void setTagLengthStringValues(HashMap<String, String> tagLengthStringValues) {
        this.tagLengthStringValues = tagLengthStringValues;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public int getTransactionSystemTraceAuditNumber() {
        return transactionSystemTraceAuditNumber;
    }

    public void setTransactionSystemTraceAuditNumber(int transactionSystemTraceAuditNumber) {
        this.transactionSystemTraceAuditNumber = transactionSystemTraceAuditNumber;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(String transactionTime) {
        this.transactionTime = transactionTime;
    }

    public String getTransactionDateAndTime() {
        return transactionDateAndTime;
    }

    public void setTransactionDateAndTime(String transactionDateAndTime) {
        this.transactionDateAndTime = transactionDateAndTime;
    }

    public void setTransactionDateAndTime(Date date) {
        SimpleDateFormat sdfDateAndTime = new SimpleDateFormat(FORMAT_DATE_AND_TIME);
        SimpleDateFormat sdfDate = new SimpleDateFormat(FORMAT_DATE);
        SimpleDateFormat sdfTime = new SimpleDateFormat(FORMAT_TIME);
        this.transactionDateAndTime = sdfDateAndTime.format(date);
        this.transactionDate = sdfDate.format(date);
        this.transactionTime = sdfTime.format(date);
    }

    public double getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(double transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getTransactionCurrencyCode() {
        return transactionCurrencyCode;
    }

    public void setTransactionCurrencyCode(String transactionCurrencyCode) {
        this.transactionCurrencyCode = transactionCurrencyCode;
    }

    public String getTransactionCurrencyExponent() {
        return transactionCurrencyExponent;
    }

    public void setTransactionCurrencyExponent(String transactionCurrencyExponent) {
        this.transactionCurrencyExponent = transactionCurrencyExponent;
    }

    public String getMessageTypeId() {
        return messageTypeId;
    }

    public void setMessageTypeId(String messageTypeId) {
        this.messageTypeId = messageTypeId;
    }

    public String getProcessingCode() {
        return processingCode;
    }

    public void setProcessingCode(String processingCode) {
        this.processingCode = processingCode;
    }

    public String getCardholderBillingCurrencyCode() {
        return cardholderBillingCurrencyCode;
    }

    public void setCardholderBillingCurrencyCode(String cardholderBillingCurrencyCode) {
        this.cardholderBillingCurrencyCode = cardholderBillingCurrencyCode;
    }

    @Deprecated
    public boolean isTransactionPyc() {
        return transactionPyc;
    }

    @Deprecated
    public void setTransactionPyc(boolean transactionPyc) {
        this.transactionPyc = transactionPyc;
    }

    @Deprecated
    public boolean isTransactionOptOut() {
        return transactionOptOut;
    }

    @Deprecated
    public void setTransactionOptOut(boolean transactionOptOut) {
        this.transactionOptOut = transactionOptOut;
    }

    public String getTransactionPYCRateMarkupSign() {
        return transactionPYCRateMarkupSign;
    }

    public void setTransactionPYCRateMarkupSign(String transactionPYCRateMarkupSign) {
        this.transactionPYCRateMarkupSign = transactionPYCRateMarkupSign;
    }

    public String getTransactionPYCRateMarkupText() {
        return transactionPYCRateMarkupText;
    }

    public void setTransactionPYCRateMarkupText(String transactionPYCRateMarkupText) {
        this.transactionPYCRateMarkupText = transactionPYCRateMarkupText;
    }

    public boolean isApproved() {
        return isApproved;
    }

    public void setApproved(boolean approved) {
        isApproved = approved;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    public String getActionCode() {
        return actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    public String getActionCodeDescription() {
        return actionCodeDescription;
    }

    public void setActionCodeDescription(String actionCodeDescription) {
        this.actionCodeDescription = actionCodeDescription;
    }

    public double getCardholderBillingAmount() {
        return cardholderBillingAmount;
    }

    public void setCardholderBillingAmount(double cardholderBillingAmount) {
        this.cardholderBillingAmount = cardholderBillingAmount;
    }

    public String getConversionRate() {
        return conversionRate;
    }

    public void setConversionRate(String conversionRate) {
        this.conversionRate = conversionRate;
    }

    public String getRetrievalReferenceNumber() {
        return retrievalReferenceNumber;
    }

    public void setRetrievalReferenceNumber(String retrievalReferenceNumber) {
        this.retrievalReferenceNumber = retrievalReferenceNumber;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public String getPrimaryAccountNumber() {
        return primaryAccountNumber;
    }

    public void setPrimaryAccountNumber(String primaryAccountNumber) {
        this.primaryAccountNumber = primaryAccountNumber;
    }

    public String getCardExpirationDate() {
        return cardExpirationDate;
    }

    public void setCardExpirationDate(String cardExpirationDate) {
        this.cardExpirationDate = cardExpirationDate;
    }

    public String getCardDetectMode() {
        return cardDetectMode;
    }

    public void setCardDetectMode(String cardDetectMode) {
        this.cardDetectMode = cardDetectMode;
    }

    public String getPOSEntryMode() {
        return POSEntryMode;
    }

    public void setPOSEntryMode(String POSEntryMode) {
        this.POSEntryMode = POSEntryMode;
    }

    public String getCardSequenceNumber() {
        return cardSequenceNumber;
    }

    public void setCardSequenceNumber(String cardSequenceNumber) {
        this.cardSequenceNumber = cardSequenceNumber;
    }

    public String getTrack1DiscretionaryData() {
        return track1DiscretionaryData;
    }

    public void setTrack1DiscretionaryData(String track1DiscretionaryData) {
        this.track1DiscretionaryData = track1DiscretionaryData;
    }

    public String getTrack2DiscretionaryData() {
        return track2DiscretionaryData;
    }

    public void setTrack2DiscretionaryData(String track2DiscretionaryData) {
        this.track2DiscretionaryData = track2DiscretionaryData;
    }

    public String getTrack2EquivalentData() {
        return track2EquivalentData;
    }

    public void setTrack2EquivalentData(String track2EquivalentData) {
        this.track2EquivalentData = track2EquivalentData;
    }

    public String getIccRelatedData() {
        return iccRelatedData;
    }

    public void setIccRelatedData(String iccRelatedData) {
        this.iccRelatedData = iccRelatedData;
    }

    public boolean isTransactionPinVerified() {
        return transactionPinVerified;
    }

    public void setTransactionPinVerified(boolean transactionPinVerified) {
        this.transactionPinVerified = transactionPinVerified;
    }

    public boolean isTransactionRequestSignature() {
        return transactionRequestSignature;
    }

    public void setTransactionRequestSignature(boolean transactionRequestSignature) {
        this.transactionRequestSignature = transactionRequestSignature;
    }

    public String getApplicationLabel() {
        return applicationLabel;
    }

    public void setApplicationLabel(String applicationLabel) {
        this.applicationLabel = applicationLabel;
    }

    public String getApplicationIdentifier() {
        return applicationIdentifier;
    }

    public void setApplicationIdentifier(String applicationIdentifier) {
        this.applicationIdentifier = applicationIdentifier;
    }

    public String getApplicationCryptogram() {
        return applicationCryptogram;
    }

    public void setApplicationCryptogram(String applicationCryptogram) {
        this.applicationCryptogram = applicationCryptogram;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getTerminalVerificationResult() {
        return terminalVerificationResult;
    }

    public void setTerminalVerificationResult(String terminalVerificationResult) {
        this.terminalVerificationResult = terminalVerificationResult;
    }

    public String getTransactionStatusInformation() {
        return transactionStatusInformation;
    }

    public void setTransactionStatusInformation(String transactionStatusInformation) {
        this.transactionStatusInformation = transactionStatusInformation;
    }

    public String getAvailableOfflineSpendingAmount() {
        return availableOfflineSpendingAmount;
    }

    public void setAvailableOfflineSpendingAmount(String availableOfflineSpendingAmount) {
        this.availableOfflineSpendingAmount = availableOfflineSpendingAmount;
    }

    public boolean isUpload() {
        return upload;
    }

    public void setUpload(boolean upload) {
        this.upload = upload;
    }

    @Override
    public String toString() {
        try {
            return JSONUtils.toJSONString(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.toString();
    }

    public void setIssuerApplicationData(String issuerApplicationData) {
        this.issuerApplicationData = issuerApplicationData;
    }

    public String getIssuerApplicationData() {
        return issuerApplicationData;
    }

    public void setUnpredictableNumber(String unpredictableNumber) {
        this.unpredictableNumber = unpredictableNumber;
    }

    public String getUnpredictableNumber() {
        return unpredictableNumber;
    }

    public void setApplicationTransactionCounter(String applicationTransactionCounter) {
        this.applicationTransactionCounter = applicationTransactionCounter;
    }

    public String getApplicationTransactionCounter() {
        return applicationTransactionCounter;
    }

    public void setTerminalVerificationResults(String terminalVerificationResults) {
        this.terminalVerificationResults = terminalVerificationResults;
    }

    public String getTerminalVerificationResults() {
        return terminalVerificationResults;
    }

    public void setIccTransactionDate(String iccTransactionDate) {
        this.iccTransactionDate = iccTransactionDate;
    }

    public String getIccTransactionDate() {
        return iccTransactionDate;
    }

    public void setIccTransactionType(String iccTransactionType) {
        this.iccTransactionType = iccTransactionType;
    }

    public String getIccTransactionType() {
        return iccTransactionType;
    }

    public void setAmountAuthorized(String amountAuthorized) {
        this.amountAuthorized = amountAuthorized;
    }

    public String getAmountAuthorized() {
        return amountAuthorized;
    }

    public void setIccTransactionCurrencyCode(String iccTransactionCurrencyCode) {
        this.iccTransactionCurrencyCode = iccTransactionCurrencyCode;
    }

    public String getIccTransactionCurrencyCode() {
        return iccTransactionCurrencyCode;
    }

    public void setTerminalCountryCode(String terminalCountryCode) {
        this.terminalCountryCode = terminalCountryCode;
    }

    public String getTerminalCountryCode() {
        return terminalCountryCode;
    }

    public void setApplicationInterchangeProfile(String applicationInterchangeProfile) {
        this.applicationInterchangeProfile = applicationInterchangeProfile;
    }

    public String getApplicationInterchangeProfile() {
        return applicationInterchangeProfile;
    }

    public void setAmountOther(String amountOther) {
        this.amountOther = amountOther;
    }

    public String getAmountOther() {
        return amountOther;
    }

    public void setApplicationPanSequence(String applicationPanSequence) {
        this.applicationPanSequence = applicationPanSequence;
    }

    public String getApplicationPanSequence() {
        return applicationPanSequence;
    }

    public void setCryptogramInformationData(String cryptogramInformationData) {
        this.cryptogramInformationData = cryptogramInformationData;
    }

    public String getCryptogramInformationData() {
        return cryptogramInformationData;
    }

    public String getPosDataCode() {
        return posDataCode;
    }

    public void setPosDataCode(String posDataCode) {
        this.posDataCode = posDataCode;
    }
}
