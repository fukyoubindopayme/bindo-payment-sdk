package com.bindo.paymentsdk.v3.pay.common.gateway.bean;

import com.bindo.paymentsdk.v3.pay.common.database.models.CountryToCurrency;
import com.bindo.paymentsdk.v3.pay.common.database.models.Currency;
import com.bindo.paymentsdk.v3.pay.common.database.models.LocalBIN;
import com.bindo.paymentsdk.v3.pay.common.database.models.PYCTable;
import com.bindo.paymentsdk.v3.pay.common.emv.enums.EMVTag;
import com.bindo.paymentsdk.v3.pay.common.util.Hex;
import com.bindo.paymentsdk.v3.pay.common.util.JSONUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Response {
    private final String FORMAT_DATE = "yyMMdd";
    private final String FORMAT_TIME = "hhmmss";
    private final String FORMAT_DATE_AND_TIME = "yyMMddhhmmss";

    private String sdkVersion = "payment-sdk-v3";
    private HashMap<String, String> gatewayConfigs = new HashMap<>();

    private HashMap<String, byte[]> tagLengthValues;
    private HashMap<String, String> tagLengthStringValues;

    // Original transaction fields.
    private int originalTransactionSystemTraceAuditNumber;
    private String originalTransactionDateAndTime;
    // Transaction fields.
    private String transactionId;
    private String transactionType;
    private int transactionSystemTraceAuditNumber;
    private String transactionDate;
    private String transactionTime;
    private String transactionDateAndTime;
    private double transactionAmount;
    private String transactionCurrencyCode;
    private String transactionCurrencyExponent;
    private String transactionProcessingCode;
    private String transactionPYCRateMarkupSign;
    private double transactionPYCRateMarkup;
    private String transactionPYCRateMarkupText;

    // Transaction result fields.
    private boolean isNeedCallIssuerConfirm;
    private boolean isApproved;
    private String  approvalCode;
    private String  actionCode;
    private String  actionCodeDescription;
    private double  cardholderBillingAmount;
    private double  conversionRate;
    private String  retrievalReferenceNumber;
    private String  authorizationCode;
    private String  cardholderBillingCurrencyCode;
    private boolean pickUpCard;

    private double tipAmount;
    private double cardholderBillingTipAmount;

    // ICC Related Data
    private String iccRelatedData;
    private String iccRelatedDataIssuerScriptData;
    private String iccRelatedDataIssuerAuthenticationData;

    private String iccAuthorizationResponseCryptogram;
    private String iccAuthorizationResponseCode;

    // Planet payment fields.
    private List<LocalBIN> planetPaymentLocalBINs;
    private String lastLocalBINTableDate;
    private String lastLocalBINTableIndex;

    private List<Currency> planetPaymentCurrencies;
    private String nextEntryToSend;

    private List<CountryToCurrency> planetPaymentCountryToCurrencies;

    private List<PYCTable> planetPaymentPYCTable;
    private String lastLocalPYCTableDate;
    private String lastLocalPYCTableIndex;

    private String lastPageNumber;
    private String printText;

    private String acquiringIIC;
    private boolean localPYCBinTableDownloadRequired;

    private List<Error> errors;

    private String posDataCode;
    private String additionalResponseData;
    private String networkSpecificInformation;
    private Map<Integer, String> additionalResponseDataMap;
    private Map<Integer, String> networkSpecificInformationMap;

    public Response() {
    }

    public Response(Request request) {
        Field[] fields = request.getClass().getDeclaredFields();
        for (Field field : fields) {
            // 设置成员可访问权限
            field.setAccessible(true);
            try {
                String fieldKey = field.getName();
                Object fieldValue = field.get(request);
                if (Modifier.isFinal(field.getModifiers())) {
                    continue;
                }
                Field toField = this.getClass().getDeclaredField(fieldKey);
                toField.set(this, fieldValue);
            } catch (IllegalArgumentException e) {
                // ignore
            } catch (IllegalAccessException e) {
                // ignore
            } catch (NoSuchFieldException e) {
                // ignore
            }
        }
        this.setTransactionType(request.getTransactionType().name());
    }

    public Response(HashMap<String, byte[]> tagLengthValues) {
        this.tagLengthValues = tagLengthValues;
    }

    public String getSdkVersion() {
        return sdkVersion;
    }

    public void setSdkVersion(String sdkVersion) {
        this.sdkVersion = sdkVersion;
    }

    public String getTLVHexString(EMVTag tag) {
        String tagString = tag.toString();
        return getTLVHexString(tagString);
    }

    public String getTLVHexString(String tagString) {
        byte[] tagValue = tagLengthValues.get(tagString);
        return Hex.encode(tagValue);
    }

    public HashMap<String, String> getGatewayConfigs() {
        return gatewayConfigs;
    }

    public void setGatewayConfigs(HashMap<String, String> gatewayConfigs) {
        this.gatewayConfigs = gatewayConfigs;
    }

    public HashMap<String, byte[]> getTagLengthValues() {
        return tagLengthValues;
    }

    public void setTagLengthValues(HashMap<String, byte[]> tagLengthValues) {
        this.tagLengthValues = tagLengthValues;
    }

    public HashMap<String, String> getTagLengthStringValues() {
        return tagLengthStringValues;
    }

    public void setTagLengthStringValues(HashMap<String, String> tagLengthStringValues) {
        this.tagLengthStringValues = tagLengthStringValues;
    }

    public int getOriginalTransactionSystemTraceAuditNumber() {
        return originalTransactionSystemTraceAuditNumber;
    }

    public void setOriginalTransactionSystemTraceAuditNumber(int originalTransactionSystemTraceAuditNumber) {
        this.originalTransactionSystemTraceAuditNumber = originalTransactionSystemTraceAuditNumber;
    }

    public String getOriginalTransactionDateAndTime() {
        return originalTransactionDateAndTime;
    }

    public void setOriginalTransactionDateAndTime(String originalTransactionDateAndTime) {
        this.originalTransactionDateAndTime = originalTransactionDateAndTime;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public int getTransactionSystemTraceAuditNumber() {
        return transactionSystemTraceAuditNumber;
    }

    public void setTransactionSystemTraceAuditNumber(int transactionSystemTraceAuditNumber) {
        this.transactionSystemTraceAuditNumber = transactionSystemTraceAuditNumber;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(String transactionTime) {
        this.transactionTime = transactionTime;
    }

    public String getTransactionDateAndTime() {
        return transactionDateAndTime;
    }

    public void setTransactionDateAndTime(Date date) {
        SimpleDateFormat sdfDateAndTime = new SimpleDateFormat(FORMAT_DATE_AND_TIME);
        SimpleDateFormat sdfDate = new SimpleDateFormat(FORMAT_DATE);
        SimpleDateFormat sdfTime = new SimpleDateFormat(FORMAT_TIME);
        this.transactionDateAndTime = sdfDateAndTime.format(date);
        this.transactionDate = sdfDate.format(date);
        this.transactionTime = sdfTime.format(date);
    }

    public void setTransactionDateAndTime(String transactionDateAndTime) {
        this.transactionDateAndTime = transactionDateAndTime;
    }

    public double getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(double transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getTransactionCurrencyCode() {
        return transactionCurrencyCode;
    }

    public void setTransactionCurrencyCode(String transactionCurrencyCode) {
        this.transactionCurrencyCode = transactionCurrencyCode;
    }

    public String getTransactionCurrencyExponent() {
        return transactionCurrencyExponent;
    }

    public void setTransactionCurrencyExponent(String transactionCurrencyExponent) {
        this.transactionCurrencyExponent = transactionCurrencyExponent;
    }

    public String getTransactionProcessingCode() {
        return transactionProcessingCode;
    }

    public void setTransactionProcessingCode(String transactionProcessingCode) {
        this.transactionProcessingCode = transactionProcessingCode;
    }

    public String getTransactionPYCRateMarkupSign() {
        return transactionPYCRateMarkupSign;
    }

    public void setTransactionPYCRateMarkupSign(String transactionPYCRateMarkupSign) {
        this.transactionPYCRateMarkupSign = transactionPYCRateMarkupSign;
    }

    public String getTransactionPYCRateMarkupText() {
        return transactionPYCRateMarkupText;
    }

    public void setTransactionPYCRateMarkupText(String transactionPYCRateMarkupText) {
        this.transactionPYCRateMarkupText = transactionPYCRateMarkupText;
    }

    public double getTransactionPYCRateMarkup() {
        return transactionPYCRateMarkup;
    }

    public void setTransactionPYCRateMarkup(double transactionPYCRateMarkup) {
        this.transactionPYCRateMarkup = transactionPYCRateMarkup;
    }

    @Deprecated
    public boolean isNeedCallIssuerConfirm() {
        return isNeedCallIssuerConfirm;
    }

    @Deprecated
    public void setNeedCallIssuerConfirm(boolean needCallIssuerConfirm) {
        isNeedCallIssuerConfirm = needCallIssuerConfirm;
    }

    public boolean getIsNeedCallIssuerConfirm() {
        return isNeedCallIssuerConfirm;
    }

    public void setIsNeedCallIssuerConfirm(boolean needCallIssuerConfirm) {
        isNeedCallIssuerConfirm = needCallIssuerConfirm;
    }

    @Deprecated
    public boolean isApproved() {
        return isApproved;
    }

    @Deprecated
    public void setApproved(boolean approved) {
        isApproved = approved;
    }

    public boolean getIsApproved() {
        return isApproved;
    }

    public void setIsApproved(boolean isApproved) {
        this.isApproved = isApproved;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    public String getActionCode() {
        return actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    public String getActionCodeDescription() {
        return actionCodeDescription;
    }

    public void setActionCodeDescription(String actionCodeDescription) {
        this.actionCodeDescription = actionCodeDescription;
    }

    public double getCardholderBillingAmount() {
        return cardholderBillingAmount;
    }

    public void setCardholderBillingAmount(double cardholderBillingAmount) {
        this.cardholderBillingAmount = cardholderBillingAmount;
    }

    public double getConversionRate() {
        return conversionRate;
    }

    public void setConversionRate(double conversionRate) {
        this.conversionRate = conversionRate;
    }

    public String getRetrievalReferenceNumber() {
        return retrievalReferenceNumber;
    }

    public void setRetrievalReferenceNumber(String retrievalReferenceNumber) {
        this.retrievalReferenceNumber = retrievalReferenceNumber;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public String getCardholderBillingCurrencyCode() {
        return cardholderBillingCurrencyCode;
    }

    public void setCardholderBillingCurrencyCode(String cardholderBillingCurrencyCode) {
        this.cardholderBillingCurrencyCode = cardholderBillingCurrencyCode;
    }

    public String getIccRelatedData() {
        return iccRelatedData;
    }

    public void setIccRelatedData(String iccRelatedData) {
        this.iccRelatedData = iccRelatedData;
    }

    public String getIccRelatedDataIssuerScriptData() {
        return iccRelatedDataIssuerScriptData;
    }

    public void setIccRelatedDataIssuerScriptData(String iccRelatedDataIssuerScriptData) {
        this.iccRelatedDataIssuerScriptData = iccRelatedDataIssuerScriptData;
    }

    public String getIccRelatedDataIssuerAuthenticationData() {
        return iccRelatedDataIssuerAuthenticationData;
    }

    public void setIccRelatedDataIssuerAuthenticationData(String iccRelatedDataIssuerAuthenticationData) {
        this.iccRelatedDataIssuerAuthenticationData = iccRelatedDataIssuerAuthenticationData;
    }

    public List<LocalBIN> getPlanetPaymentLocalBINs() {
        return planetPaymentLocalBINs;
    }

    public void setPlanetPaymentLocalBINs(List<LocalBIN> planetPaymentLocalBINs) {
        this.planetPaymentLocalBINs = planetPaymentLocalBINs;
    }

    public String getLastLocalBINTableDate() {
        return lastLocalBINTableDate;
    }

    public void setLastLocalBINTable(String lastLocalBINTableDate) {
        this.lastLocalBINTableDate = lastLocalBINTableDate;
    }

    public String getLastLocalBINTableIndex() {
        return lastLocalBINTableIndex;
    }

    public void setLastLocalBINTableIndex(String lastLocalBINTableIndex) {
        this.lastLocalBINTableIndex = lastLocalBINTableIndex;
    }

    public String getLastLocalPYCTableDate() {
        return lastLocalPYCTableDate;
    }

    public void setLastLocalPYCTableDate(String lastLocalPYCTableDate) {
        this.lastLocalPYCTableDate = lastLocalPYCTableDate;
    }

    public String getLastLocalPYCTableIndex() {
        return lastLocalPYCTableIndex;
    }

    public void setLastLocalPYCTableIndex(String lastLocalPYCTableIndex) {
        this.lastLocalPYCTableIndex = lastLocalPYCTableIndex;
    }

    public List<Currency> getPlanetPaymentCurrencies() {
        return planetPaymentCurrencies;
    }

    public void setPlanetPaymentCurrencies(List<Currency> planetPaymentCurrencies) {
        this.planetPaymentCurrencies = planetPaymentCurrencies;
    }

    public List<PYCTable> getPlanetPaymentPYCTable() {
        return planetPaymentPYCTable;
    }

    public void setPlanetPaymentPYCTable(List<PYCTable> planetPaymentPYCTable) {
        this.planetPaymentPYCTable = planetPaymentPYCTable;
    }

    public String getNextEntryToSend() {
        return nextEntryToSend;
    }

    public void setNextEntryToSend(String nextEntryToSend) {
        this.nextEntryToSend = nextEntryToSend;
    }

    public String getAcquiringIIC() {
        return acquiringIIC;
    }

    public void setAcquiringIIC(String acquiringIIC) {
        this.acquiringIIC = acquiringIIC;
    }

    public String getLastPageNumber() {
        return lastPageNumber;
    }

    public void setLastPageNumber(String lastPageNumber) {
        this.lastPageNumber = lastPageNumber;
    }

    public String getPrintText() {
        return printText;
    }

    public void setPrintText(String printText) {
        this.printText = printText;
    }

    public List<Error> getErrors() {
        return errors;
    }

    public void setErrors(List<Error> errors) {
        this.errors = errors;
    }


    public double getTipAmount() {
        return tipAmount;
    }

    public void setTipAmount(double tipAmount) {
        this.tipAmount = tipAmount;
    }

    public double getCardholderBillingTipAmount() {
        return cardholderBillingTipAmount;
    }

    public void setCardholderBillingTipAmount(double cardholderBillingTipAmount) {
        this.cardholderBillingTipAmount = cardholderBillingTipAmount;
    }

    public String getIccAuthorizationResponseCryptogram() {
        return iccAuthorizationResponseCryptogram;
    }

    public void setIccAuthorizationResponseCryptogram(String iccAuthorizationResponseCryptogram) {
        this.iccAuthorizationResponseCryptogram = iccAuthorizationResponseCryptogram;
    }

    public String getIccAuthorizationResponseCode() {
        return iccAuthorizationResponseCode;
    }

    public void setIccAuthorizationResponseCode(String iccAuthorizationResponseCode) {
        this.iccAuthorizationResponseCode = iccAuthorizationResponseCode;
    }

    public boolean isLocalPYCBinTableDownloadRequired() {
        return localPYCBinTableDownloadRequired;
    }

    public void setIsLocalPYCBinTableDownloadRequired(boolean localPYCBinTableDownloadRequired) {
        this.localPYCBinTableDownloadRequired = localPYCBinTableDownloadRequired;
    }

    @Override
    public String toString() {
        try {
            return JSONUtils.toJSONString(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.toString();
    }

    public String getPosDataCode() {
        return posDataCode;
    }

    public void setPosDataCode(String posDataCode) {
        this.posDataCode = posDataCode;
    }

    public void setAdditionalResponseData(String additionalResponseData) {
        this.additionalResponseData = additionalResponseData;
    }

    public String getAdditionalResponseData() {
        return additionalResponseData;
    }

    public void setNetworkSpecificInformation(String networkSpecificInformation) {
        this.networkSpecificInformation = networkSpecificInformation;
    }

    public String getNetworkSpecificInformation() {
        return networkSpecificInformation;
    }

    public void setAdditionalResponseDataMap(Map<Integer, String> additionalResponseDataMap) {
        this.additionalResponseDataMap = additionalResponseDataMap;
    }

    public Map<Integer, String> getAdditionalResponseDataMap() {
        return additionalResponseDataMap;
    }

    public void setNetworkSpecificInformationMap(Map<Integer, String> networkSpecificInformationMap) {
        this.networkSpecificInformationMap = networkSpecificInformationMap;
    }

    public Map<Integer, String> getNetworkSpecificInformationMap() {
        return networkSpecificInformationMap;
    }

    public static class Error {
        public static final String ERROR_CODE_UNKNOWN = "-1";
        public static final String ERROR_CODE_UNKNOWN_HOST = "600";
        public static final String ERROR_CODE_TIMEOUT = "601";

        private String code;
        private String message;

        public Error(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public boolean isPickUpCard() {
        return pickUpCard;
    }

    public void setPickUpCard(boolean pickUpCard) {
        this.pickUpCard = pickUpCard;
    }
}
