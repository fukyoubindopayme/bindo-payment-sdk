package com.bindo.paymentsdk.v3.pay.common.gateway.bean;

import com.bindo.paymentsdk.v3.pay.common.ReversalType;
import com.bindo.paymentsdk.v3.pay.common.TransactionData;
import com.bindo.paymentsdk.v3.pay.common.emv.enums.EMVTag;
import com.bindo.paymentsdk.v3.pay.common.TransactionType;
import com.bindo.paymentsdk.v3.pay.common.util.CurrencyUtil;
import com.bindo.paymentsdk.v3.pay.common.util.Hex;
import com.bindo.paymentsdk.v3.pay.common.util.JSONUtils;
import com.bindo.paymentsdk.v3.pay.common.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class Request {
    private final String FORMAT_DATE = "yyMMdd";
    private final String FORMAT_TIME = "hhmmss";
    private final String FORMAT_DATE_AND_TIME = "yyMMddhhmmss";

    private String sdkVersion = "payment-sdk-v3";
    private HashMap<String, String> gatewayConfigs = new HashMap<>();

    private HashMap<String, byte[]> tagLengthValues;
    private HashMap<String, String> tagLengthStringValues;

    // Original transaction fields.
    private TransactionType originalTransactionType;
    private int originalTransactionSystemTraceAuditNumber;
    private String originalTransactionDateAndTime;
    private double originalTransactionAmount;
    private String originalAcquiringIIC;
    // Transaction fields.
    private TransactionType transactionType;
    private ReversalType reversalType;
    private int batchNO;
    private int transactionSystemTraceAuditNumber;
    private String transactionDate;
    private String transactionTime;
    private String transactionDateAndTime;
    private double transactionAmount;
    private double cardholderBillingAmount;
    private double tipAmount;
    private double cardholderBillingTipAmount;
    private String transactionCurrency;
    private String transactionCurrencyCode;
    private String transactionCurrencyExponent;
    private String messageTypeId;
    private String messageTypeCode; //60.1域:消息类型码
    private String processingCode;
    private String cardholderBillingCurrencyCode;
    private String PINData;
    private String rrn;
    private String authCode;
    private String cardSecurityCode;
    private String acquirerReferenceData;
    private String acquiringIIC;

    // Terminal fields.
    private String terminalCapabilities;
    private String terminalCountryCode;
    private String terminalFloorLimit;
    private String terminalIdentification;
    private String terminalRiskManagementData;
    private String terminalType;
    private String terminalVerificationResults;
    private String terminalStatusInfo;
    // Card fields.
    private String primaryAccountNumber;
    private String cardDetectMode;
    private String POSEntryMode;
    private String cardSequenceNumber;
    private String cardExpirationDate;
    private String track1DiscretionaryData;
    private String track2DiscretionaryData;
    private String track2EquivalentData;
    private int numberOfInstallment;
    // Merchant fields.
    private String merchantCategoryCode;
    private String merchantIdentifier;
    private String merchantNameAndLocation;

    //Other fields
    private String networkInternationalIdentifier;
    private String POSConditionCode;
    //planet payment
    private String planetPaymentField63CurrencyConversionIndicator;
    private String planetPaymentField63OptOutFlag;
    private String planetPaymentField63VerificationCode;
    private String planetPaymentField63Duration;
    private String planetPaymentField63AuthorizationCharacteristicsIndicator;
    private String planetPaymentField63RateLookupIndicator;
    private String planetPaymentField63PSCIndicator;
    private String planetPaymentField63RequestForAdditionalResponse;
    private String planetPaymentField63MarketSpecificDataIndicator;
    private String planetPaymentField63PrestigiousPropertyIndicator;
    private String planetPaymentField63AddressVerificationService;
    private String planetPaymentField63BillPaymentIndicator;
    private String planetPaymentField63HostKeyIndex;
    private String planetPaymentField63MacCheckDigits;
    private String planetPaymentField63PINPadSerialNumber;
    private String planetPaymentField63MOTOeCommerceIndicator;
    private String planetPaymentField63MPMOTOeCommercePurchase;
    private String planetPaymentField63EncryptedCardholderData;
    private String planetPaymentField63FinalAuthIndicator;
    private String planetPaymentField63RequestForCurrencyTableDownload;
    private String planetPaymentField63RequestForReportDownload;
    private String planetPaymentField63RequestForLocalBINTableDownload;
    private String planetPaymentField63RequestForPYCTableDownload;

    // DCC transaction fields.
//    private DCCTransactionType dccTransactionType;

    //for construct iso8583 message
    private int[] fieldIdSet;
    private byte flowType;
    private String iccRelatedData;

    //for settlement
    private BatchTotals batchTotals;
    private boolean beaSettlementFirstFlag;
    private boolean beaBatchUploadLastFlag;

    // AVS
    private String avsCardholderAddress;
    private String avsCardholderZipcode;
    private int transactionSequenceNumber;
    private List<TransactionData> transactionDataList;
    private int amexFileSequence;

    // Soft Descriptor
    private String softDescriptor;
    private String draftLocatorId;

    public Request() {
    }

    public String getSdkVersion() {
        return sdkVersion;
    }

    public void setSdkVersion(String sdkVersion) {
        this.sdkVersion = sdkVersion;
    }

    public Request(HashMap<String, byte[]> tagLengthValues) {
        this.tagLengthValues = tagLengthValues;
    }

    public String getTLVHexString(EMVTag tag) {
        String tagString = tag.toString();
        return getTLVHexString(tagString);
    }

    public String getTLVHexString(String tagString) {
        byte[] tagValue = tagLengthValues.get(tagString);
        return Hex.encode(tagValue);
    }

    public HashMap<String, String> getGatewayConfigs() {
        return gatewayConfigs;
    }

    public void setGatewayConfigs(HashMap<String, String> gatewayConfigs) {
        this.gatewayConfigs = gatewayConfigs;
    }

    public HashMap<String, byte[]> getTagLengthValues() {
        return tagLengthValues;
    }

    public void setTagLengthValues(HashMap<String, byte[]> tagLengthValues) {
        this.tagLengthValues = tagLengthValues;
    }

    public HashMap<String, String> getTagLengthStringValues() {
        return tagLengthStringValues;
    }

    public void setTagLengthStringValues(HashMap<String, String> tagLengthStringValues) {
        this.tagLengthStringValues = tagLengthStringValues;
    }

    public int getOriginalTransactionSystemTraceAuditNumber() {
        return originalTransactionSystemTraceAuditNumber;
    }

    public void setOriginalTransactionSystemTraceAuditNumber(int originalTransactionSystemTraceAuditNumber) {
        this.originalTransactionSystemTraceAuditNumber = originalTransactionSystemTraceAuditNumber;
    }

    public String getOriginalTransactionDateAndTime() {
        return originalTransactionDateAndTime;
    }

    public void setOriginalTransactionDateAndTime(String originalTransactionDateAndTime) {
        this.originalTransactionDateAndTime = originalTransactionDateAndTime;
    }

    public void setOriginalTransactionDateAndTime(Date date) {
        SimpleDateFormat sdfDateAndTime = new SimpleDateFormat(FORMAT_DATE_AND_TIME);
        this.originalTransactionDateAndTime = sdfDateAndTime.format(date);
    }

    public double getOriginalTransactionAmount() {
        return originalTransactionAmount;
    }

    public void setOriginalTransactionAmount(double originalTransactionAmount) {
        this.originalTransactionAmount = originalTransactionAmount;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public ReversalType getReversalType() {
        return reversalType;
    }

    public void setReversalType(ReversalType reversalType) {
        this.reversalType = reversalType;
    }

    public int getBatchNO() {
        return batchNO;
    }

    public void setBatchNO(int batchNO) {
        this.batchNO = batchNO;
    }

    public int getNumberOfInstallment(){ return numberOfInstallment; }
    public void setNumberOfInstallment(int numberOfInstallment){ this.numberOfInstallment=numberOfInstallment; }

    public void setTransactionType(String transactionTypeName) {
        this.transactionType = TransactionType.valueOf(transactionTypeName.toUpperCase());
    }

    public int getTransactionSystemTraceAuditNumber() {
        return transactionSystemTraceAuditNumber;
    }

    public void setTransactionSystemTraceAuditNumber(int transactionSystemTraceAuditNumber) {
        this.transactionSystemTraceAuditNumber = transactionSystemTraceAuditNumber;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(String transactionTime) {
        this.transactionTime = transactionTime;
    }

    public String getTransactionDateAndTime() {
        return transactionDateAndTime;
    }

    public void setTransactionDateAndTime(String transactionDateAndTime) {
        if (!StringUtils.isEmpty(transactionDateAndTime)) {
            this.transactionDateAndTime = transactionDateAndTime;
            setTransactionDate(transactionDateAndTime.substring(0, 6));
            setTransactionTime(transactionDateAndTime.substring(6));
        }
    }

    public void setTransactionDateAndTime(Date date) {
        SimpleDateFormat sdfDateAndTime = new SimpleDateFormat(FORMAT_DATE_AND_TIME);
        SimpleDateFormat sdfDate = new SimpleDateFormat(FORMAT_DATE);
        SimpleDateFormat sdfTime = new SimpleDateFormat(FORMAT_TIME);
        this.transactionDateAndTime = sdfDateAndTime.format(date);
        this.transactionDate = sdfDate.format(date);
        this.transactionTime = sdfTime.format(date);
    }

    public double getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(double transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public double getTipAmount() {
        return tipAmount;
    }

    public void setTipAmount(double tipAmount) {
        this.tipAmount = tipAmount;
    }

    public double getCardholderBillingTipAmount() {
        return cardholderBillingTipAmount;
    }

    public void setCardholderBillingTipAmount(double cardholderBillingTipAmount) {
        this.cardholderBillingTipAmount = cardholderBillingTipAmount;
    }

    public BatchTotals getBatchTotals() {
        return batchTotals;
    }

    public void setBatchTotals(BatchTotals batchTotals) {
        this.batchTotals = batchTotals;
    }

    public String getTransactionCurrency() {
        return transactionCurrency;
    }

    public void setTransactionCurrency(String transactionCurrency) throws Exception {
        this.transactionCurrency = transactionCurrency;
        this.transactionCurrencyCode = CurrencyUtil.toCurrencyCode(transactionCurrency);
    }

    public String getTransactionCurrencyCode() {
        return transactionCurrencyCode;
    }

    public void setTransactionCurrencyCode(String transactionCurrencyCode) {
        this.transactionCurrencyCode = transactionCurrencyCode;
    }

    public String getTransactionCurrencyExponent() {
        return transactionCurrencyExponent;
    }

    public void setTransactionCurrencyExponent(String transactionCurrencyExponent) {
        this.transactionCurrencyExponent = transactionCurrencyExponent;
    }

    public String getMessageTypeId() {
        return messageTypeId;
    }

    public void setMessageTypeId(String messageTypeId) {
        this.messageTypeId = messageTypeId;
    }

    public String getProcessingCode() {
        return processingCode;
    }

    public void setProcessingCode(String processingCode) {
        this.processingCode = processingCode;
    }

    public String getCardholderBillingCurrencyCode() {
        return cardholderBillingCurrencyCode;
    }

    public void setCardholderBillingCurrencyCode(String cardholderBillingCurrencyCode) {
        this.cardholderBillingCurrencyCode = cardholderBillingCurrencyCode;
    }

    public double getCardholderBillingAmount() {
        return cardholderBillingAmount;
    }

    public void setCardholderBillingAmount(double cardholderBillingAmount) {
        this.cardholderBillingAmount = cardholderBillingAmount;
    }

    public String getPINData() {
        return PINData;
    }

    public void setPINData(String PINData) {
        this.PINData = PINData;
    }

    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public String getTerminalCapabilities() {
        return terminalCapabilities;
    }

    public void setTerminalCapabilities(String terminalCapabilities) {
        this.terminalCapabilities = terminalCapabilities;
    }

    public String getTerminalCountryCode() {
        return terminalCountryCode;
    }

    public void setTerminalCountryCode(String terminalCountryCode) {
        this.terminalCountryCode = terminalCountryCode;
    }

    public String getTerminalFloorLimit() {
        return terminalFloorLimit;
    }

    public void setTerminalFloorLimit(String terminalFloorLimit) {
        this.terminalFloorLimit = terminalFloorLimit;
    }

    public String getTerminalIdentification() {
        return terminalIdentification;
    }

    public void setTerminalIdentification(String terminalIdentification) {
        this.terminalIdentification = terminalIdentification;
    }

    public String getTerminalRiskManagementData() {
        return terminalRiskManagementData;
    }

    public void setTerminalRiskManagementData(String terminalRiskManagementData) {
        this.terminalRiskManagementData = terminalRiskManagementData;
    }

    public String getTerminalType() {
        return terminalType;
    }

    public void setTerminalType(String terminalType) {
        this.terminalType = terminalType;
    }

    public String getTerminalVerificationResults() {
        return terminalVerificationResults;
    }

    public void setTerminalVerificationResults(String terminalVerificationResults) {
        this.terminalVerificationResults = terminalVerificationResults;
    }

    public String getPrimaryAccountNumber() {
        return primaryAccountNumber;
    }

    public void setPrimaryAccountNumber(String primaryAccountNumber) {
        this.primaryAccountNumber = primaryAccountNumber;
    }

    public String getCardDetectMode() {
        return cardDetectMode;
    }

    public void setCardDetectMode(String cardDetectMode) {
        this.cardDetectMode = cardDetectMode;
    }

    public String getPOSEntryMode() {
        return POSEntryMode;
    }

    public void setPOSEntryMode(String POSEntryMode) {
        this.POSEntryMode = POSEntryMode;
    }

    public String getCardSequenceNumber() {
        return cardSequenceNumber;
    }

    public void setCardSequenceNumber(String cardSequenceNumber) {
        this.cardSequenceNumber = cardSequenceNumber;
    }

    public String getCardExpirationDate() {
        return cardExpirationDate;
    }

    public void setCardExpirationDate(String cardExpirationDate) {
        this.cardExpirationDate = cardExpirationDate;
    }

    public String getTrack1DiscretionaryData() {
        return track1DiscretionaryData;
    }

    public void setTrack1DiscretionaryData(String track1DiscretionaryData) {
        this.track1DiscretionaryData = track1DiscretionaryData;
    }

    public String getTrack2DiscretionaryData() {
        return track2DiscretionaryData;
    }

    public void setTrack2DiscretionaryData(String track2DiscretionaryData) {
        this.track2DiscretionaryData = track2DiscretionaryData;
    }

    public String getTrack2EquivalentData() {
        return track2EquivalentData;
    }

    public void setTrack2EquivalentData(String track2EquivalentData) {
        this.track2EquivalentData = track2EquivalentData;
    }

    public String getMerchantCategoryCode() {
        return merchantCategoryCode;
    }

    public void setMerchantCategoryCode(String merchantCategoryCode) {
        this.merchantCategoryCode = merchantCategoryCode;
    }

    public String getMerchantIdentifier() {
        return merchantIdentifier;
    }

    public void setMerchantIdentifier(String merchantIdentifier) {
        this.merchantIdentifier = merchantIdentifier;
    }

    public String getMerchantNameAndLocation() {
        return merchantNameAndLocation;
    }

    public void setMerchantNameAndLocation(String merchantNameAndLocation) {
        this.merchantNameAndLocation = merchantNameAndLocation;
    }

    public String getNetworkInternationalIdentifier() {
        return networkInternationalIdentifier;
    }

    public void setNetworkInternationalIdentifier(String networkInternationalIdentifier) {
        this.networkInternationalIdentifier = networkInternationalIdentifier;
    }

    public String getPOSConditionCode() {
        return POSConditionCode;
    }

    public void setPOSConditionCode(String POSConditionCode) {
        this.POSConditionCode = POSConditionCode;
    }

    public String getPlanetPaymentField63CurrencyConversionIndicator() {
        return planetPaymentField63CurrencyConversionIndicator;
    }

    public void setPlanetPaymentField63CurrencyConversionIndicator(String planetPaymentField63CurrencyConversionIndicator) {
        this.planetPaymentField63CurrencyConversionIndicator = planetPaymentField63CurrencyConversionIndicator;
    }

    public String getPlanetPaymentField63OptOutFlag() {
        return planetPaymentField63OptOutFlag;
    }

    public void setPlanetPaymentField63OptOutFlag(String planetPaymentField63OptOutFlag) {
        this.planetPaymentField63OptOutFlag = planetPaymentField63OptOutFlag;
    }

    public String getPlanetPaymentField63VerificationCode() {
        return planetPaymentField63VerificationCode;
    }

    public void setPlanetPaymentField63VerificationCode(String planetPaymentField63VerificationCode) {
        this.planetPaymentField63VerificationCode = planetPaymentField63VerificationCode;
    }

    public String getPlanetPaymentField63Duration() {
        return planetPaymentField63Duration;
    }

    public void setPlanetPaymentField63Duration(String planetPaymentField63Duration) {
        this.planetPaymentField63Duration = planetPaymentField63Duration;
    }

    public String getPlanetPaymentField63AuthorizationCharateristicsIndicator() {
        return planetPaymentField63AuthorizationCharacteristicsIndicator;
    }

    public void setPlanetPaymentField63AuthorizationCharateristicsIndicator(String planetPaymentField63AuthorizationCharateristicsIndicator) {
        this.planetPaymentField63AuthorizationCharacteristicsIndicator = planetPaymentField63AuthorizationCharateristicsIndicator;
    }

    public String getPlanetPaymentField63RateLookupIndicator() {
        return planetPaymentField63RateLookupIndicator;
    }

    public void setPlanetPaymentField63RateLookupIndicator(String planetPaymentField63RateLookupIndicator) {
        this.planetPaymentField63RateLookupIndicator = planetPaymentField63RateLookupIndicator;
    }

    public String getPlanetPaymentField63PSCIndicator() {
        return planetPaymentField63PSCIndicator;
    }

    public void setPlanetPaymentField63PSCIndicator(String planetPaymentField63PSCIndicator) {
        this.planetPaymentField63PSCIndicator = planetPaymentField63PSCIndicator;
    }

    public String getPlanetPaymentField63RequestForAdditionalResponse() {
        return planetPaymentField63RequestForAdditionalResponse;
    }

    public void setPlanetPaymentField63RequestForAdditionalResponse(String planetPaymentField63RequestForAdditionalResponse) {
        this.planetPaymentField63RequestForAdditionalResponse = planetPaymentField63RequestForAdditionalResponse;
    }

    public String getPlanetPaymentField63MarketSpecificDataIndicator() {
        return planetPaymentField63MarketSpecificDataIndicator;
    }

    public void setPlanetPaymentField63MarketSpecificDataIndicator(String planetPaymentField63MarketSpecificDataIndicator) {
        this.planetPaymentField63MarketSpecificDataIndicator = planetPaymentField63MarketSpecificDataIndicator;
    }

    public String getPlanetPaymentField63PrestigiousPropertyIndicator() {
        return planetPaymentField63PrestigiousPropertyIndicator;
    }

    public void setPlanetPaymentField63PrestigiousPropertyIndicator(String planetPaymentField63PrestigiousPropertyIndicator) {
        this.planetPaymentField63PrestigiousPropertyIndicator = planetPaymentField63PrestigiousPropertyIndicator;
    }

    public String getPlanetPaymentField63AdrressVerificationService() {
        return planetPaymentField63AddressVerificationService;
    }

    public void setPlanetPaymentField63AdrressVerificationService(String planetPaymentField63AdrressVerificationService) {
        this.planetPaymentField63AddressVerificationService = planetPaymentField63AdrressVerificationService;
    }

    public String getPlanetPaymentField63BillPaymentIndicator() {
        return planetPaymentField63BillPaymentIndicator;
    }

    public void setPlanetPaymentField63BillPaymentIndicator(String planetPaymentField63BillPaymentIndicator) {
        this.planetPaymentField63BillPaymentIndicator = planetPaymentField63BillPaymentIndicator;
    }

    public String getPlanetPaymentField63HostKeyIndex() {
        return planetPaymentField63HostKeyIndex;
    }

    public void setPlanetPaymentField63HostKeyIndex(String planetPaymentField63HostKeyIndex) {
        this.planetPaymentField63HostKeyIndex = planetPaymentField63HostKeyIndex;
    }

    public String getPlanetPaymentField63MacCheckDigits() {
        return planetPaymentField63MacCheckDigits;
    }

    public void setPlanetPaymentField63MacCheckDigits(String planetPaymentField63MacCheckDigits) {
        this.planetPaymentField63MacCheckDigits = planetPaymentField63MacCheckDigits;
    }

    public String getPlanetPaymentField63PINPadSerialNumber() {
        return planetPaymentField63PINPadSerialNumber;
    }

    public void setPlanetPaymentField63PINPadSerialNumber(String planetPaymentField63PINPadSerialNumber) {
        this.planetPaymentField63PINPadSerialNumber = planetPaymentField63PINPadSerialNumber;
    }

    public String getPlanetPaymentField63MOTOeCommerceIndicator() {
        return planetPaymentField63MOTOeCommerceIndicator;
    }

    public void setPlanetPaymentField63MOTOeCommerceIndicator(String planetPaymentField63MOTOeCommerceIndicator) {
        this.planetPaymentField63MOTOeCommerceIndicator = planetPaymentField63MOTOeCommerceIndicator;
    }

    public String getPlanetPaymentField63MPMOTOeCommercePurchase() {
        return planetPaymentField63MPMOTOeCommercePurchase;
    }

    public void setPlanetPaymentField63MPMOTOeCommercePurchase(String planetPaymentField63MPMOTOeCommercePurchase) {
        this.planetPaymentField63MPMOTOeCommercePurchase = planetPaymentField63MPMOTOeCommercePurchase;
    }

    public String getPlanetPaymentField63EncryptedCardholderData() {
        return planetPaymentField63EncryptedCardholderData;
    }

    public void setPlanetPaymentField63EncryptedCardholderData(String planetPaymentField63EncryptedCardholderData) {
        this.planetPaymentField63EncryptedCardholderData = planetPaymentField63EncryptedCardholderData;
    }

    public String getPlanetPaymentField63FinalAuthIndicator() {
        return planetPaymentField63FinalAuthIndicator;
    }

    public void setPlanetPaymentField63FinalAuthIndicator(String planetPaymentField63FinalAuthIndicator) {
        this.planetPaymentField63FinalAuthIndicator = planetPaymentField63FinalAuthIndicator;
    }

    public String getPlanetPaymentField63RequestForCurrencyTableDownload() {
        return planetPaymentField63RequestForCurrencyTableDownload;
    }

    public void setPlanetPaymentField63RequestForCurrencyTableDownload(String planetPaymentField63RequestForCurrencyTableDownload) {
        this.planetPaymentField63RequestForCurrencyTableDownload = planetPaymentField63RequestForCurrencyTableDownload;
    }

    public String getPlanetPaymentField63RequestForReportDownload() {
        return planetPaymentField63RequestForReportDownload;
    }

    public void setPlanetPaymentField63RequestForReportDownload(String planetPaymentField63RequestForReportDownload) {
        this.planetPaymentField63RequestForReportDownload = planetPaymentField63RequestForReportDownload;
    }

    public String getPlanetPaymentField63RequestForLocalBINTableDownload() {
        return planetPaymentField63RequestForLocalBINTableDownload;
    }

    public void setPlanetPaymentField63RequestForLocalBINTableDownload(String planetPaymentField63RequestForLocalBINTableDownload) {
        this.planetPaymentField63RequestForLocalBINTableDownload = planetPaymentField63RequestForLocalBINTableDownload;
    }

    public String getPlanetPaymentField63RequestForPYCTableDownload() {
        return planetPaymentField63RequestForPYCTableDownload;
    }

    public void setPlanetPaymentField63RequestForPYCTableDownload(String planetPaymentField63RequestForPYCTableDownload) {
        this.planetPaymentField63RequestForPYCTableDownload = planetPaymentField63RequestForPYCTableDownload;
    }

    public int[] getFieldIdSet() {
        return fieldIdSet;
    }

    public void setFieldIdSet(int[] fieldIdSet) {
        this.fieldIdSet = fieldIdSet;
    }

    public byte getFlowType() {
        return flowType;
    }

    public void setFlowType(byte flowType) {
        this.flowType = flowType;
    }

    public String getIccRelatedData() {
        return iccRelatedData;
    }

    public void setIccRelatedData(String iccRelatedData) {
        this.iccRelatedData = iccRelatedData;
    }

    public boolean isBeaSettlementFirstFlag() {
        return beaSettlementFirstFlag;
    }

    public void setBeaSettlementFirstFlag(boolean beaSettlementFirstFlag) {
        this.beaSettlementFirstFlag = beaSettlementFirstFlag;
    }

    public boolean isBeaBatchUploadLastFlag() {
        return beaBatchUploadLastFlag;
    }

    public void setBeaBatchUploadLastFlag(boolean beaBatchUploadLastFlag) {
        this.beaBatchUploadLastFlag = beaBatchUploadLastFlag;
    }

    public TransactionType getOriginalTransactionType() {
        return originalTransactionType;
    }

    public void setOriginalTransactionType(TransactionType originalTransactionType) {
        this.originalTransactionType = originalTransactionType;
    }

    public void setOriginalTransactionType(String originalTransactionTypeName) {
        this.originalTransactionType = TransactionType.valueOf(originalTransactionTypeName.toUpperCase());
    }

    public String getOriginalAcquiringIIC() {
        return originalAcquiringIIC;
    }

    public void setOriginalAcquiringIIC(String originalAcquiringIIC) {
        this.originalAcquiringIIC = originalAcquiringIIC;
    }

    public String getCardSecurityCode() {
        return cardSecurityCode;
    }

    public void setCardSecurityCode(String cardSecurityCode) {
        this.cardSecurityCode = cardSecurityCode;
    }

    public String getAcquirerReferenceData() {
        return acquirerReferenceData;
    }

    public void setAcquirerReferenceData(String acquirerReferenceData) {
        this.acquirerReferenceData = acquirerReferenceData;
    }

    public String getAcquiringIIC() {
        return acquiringIIC;
    }

    public void setAcquiringIIC(String acquiringIIC) {
        this.acquiringIIC = acquiringIIC;
    }

    public String getTerminalStatusInfo() {
        return terminalStatusInfo;
    }

    public void setTerminalStatusInfo(String terminalStatusInfo) {
        this.terminalStatusInfo = terminalStatusInfo;
    }

    @Override
    public String toString() {
        try {
            return JSONUtils.toJSONString(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.toString();
    }

    public String getMessageTypeCode() {
        return messageTypeCode;
    }

    public void setMessageTypeCode(String messageTypeCode) {
        this.messageTypeCode = messageTypeCode;
    }

    public String getAvsCardholderAddress() {
        return avsCardholderAddress;
    }

    public void setAvsCardholderAddress(String avsCardholderAddress) {
        this.avsCardholderAddress = avsCardholderAddress;
    }

    public String getAvsCardholderZipcode() {
        return avsCardholderZipcode;
    }

    public void setAvsCardholderZipcode(String avsCardholderZipcode) {
        this.avsCardholderZipcode = avsCardholderZipcode;
    }

    public int getTransactionSequenceNumber() {
        return transactionSequenceNumber;
    }

    public void setTransactionSequenceNumber(int transactionSequenceNumber) {
        this.transactionSequenceNumber = transactionSequenceNumber;
    }

    public List<TransactionData> getTransactionDataList() {
        return transactionDataList;
    }

    public void setTransactionDataList(List<TransactionData> transactionDataList) {
        this.transactionDataList = transactionDataList;
    }

    public int getAmexFileSequence() {
        return amexFileSequence;
    }

    public void setAmexFileSequence(int amexFileSequence) {
        this.amexFileSequence = amexFileSequence;
    }

    // Descriptor
    public String getSoftDescriptor() {
        return softDescriptor;
    }

    public void setSoftDescriptor(String softDescriptor) {
        this.softDescriptor = softDescriptor;
    }

    public void setDraftLocatorId(String draftLocatorId) {
        this.draftLocatorId = draftLocatorId;
    }

    public String getDraftLocatorId() {
        return draftLocatorId;
    }
}
