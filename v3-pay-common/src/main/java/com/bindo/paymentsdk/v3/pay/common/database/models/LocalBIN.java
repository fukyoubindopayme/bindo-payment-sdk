package com.bindo.paymentsdk.v3.pay.common.database.models;

import com.bindo.paymentsdk.v3.pay.common.util.JSONUtils;

public class LocalBIN {
    private String lowPrefixNumber;
    private String highPrefixNumber;
    private String localCurrencyCode;

    public String getLowPrefixNumber() {
        return lowPrefixNumber;
    }

    public void setLowPrefixNumber(String lowPrefixNumber) {
        this.lowPrefixNumber = lowPrefixNumber;
    }

    public String getHighPrefixNumber() {
        return highPrefixNumber;
    }

    public void setHighPrefixNumber(String highPrefixNumber) {
        this.highPrefixNumber = highPrefixNumber;
    }

    public String getLocalCurrencyCode() {
        return localCurrencyCode;
    }

    public void setLocalCurrencyCode(String localCurrencyCode) {
        this.localCurrencyCode = localCurrencyCode;
    }

    @Override
    public String toString() {
        try {
            return JSONUtils.toJSONString(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.toString();
    }
}
