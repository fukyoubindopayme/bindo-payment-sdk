package com.bindo.paymentsdk.v3.pay.common.emv.enums;

public enum CVMFlag {
    NO_CVM,
    ONLINE_PIN,
    OFFLINE_PIN,
    SIGNATURE,
}
