package com.bindo.paymentsdk.v3.pay.common.emv.enums;

public enum KernelID {
    EMVContact,
    EMVContactless,
    MASTER,
    VISA,
    AMEX,
    JCB,
    PBOC,
    DEFINE,
}
