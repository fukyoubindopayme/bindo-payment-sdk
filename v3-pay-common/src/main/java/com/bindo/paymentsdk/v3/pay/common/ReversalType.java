package com.bindo.paymentsdk.v3.pay.common;

public enum ReversalType {

    MERCHANT_INITIATED,
    SYSTEM_GENERATED,
}