package com.bindo.paymentsdk.v3.pay.common.util;


public class TLVUtils {

    public static int parseTLVString(byte[] src, int srcLen, boolean decodeConstructedData,
                                     OnDecodeTlvObjectListner onDecodeTlvObjectListner) {
        if (src == null || srcLen <= 0 || srcLen > src.length) {
            return -1;
        }

        int start = 0;

        for (int i = 0; i < srcLen; i++) {

            if (src[i] == 0x00 || (src[i] & 0xFF) == 0xFF) {
                start++;
                continue;
            } else {
                break;
            }
        }

        if (start >= srcLen) {
            return 0;
        }

        short tag;
        int len;
        int lenBytes;

        for (int i = start; i < srcLen; ) {
            // parse tag
            tag = 0;

            if ((src[i] & 0x1F) == 0x1F) {
                tag = (short) (src[i++] & 0xFF);
                tag = (short) (tag << 8);
            }

            tag += (short) (src[i++] & 0xFF);
            // parse length
            len = 0;

            if ((src[i] & 0x80) == 0x80) {
                lenBytes = (src[i++] & 0x7F);
                for (int j = 0; j < lenBytes; j++) {
                    len = (len << 8) + (src[i++] & 0xFF);
                }
            } else {
                len = src[i++];
            }
            if (len > (srcLen - i)) {
                return -2;
            }
            byte[] value = new byte[len];
            System.arraycopy(src, i, value, 0, len);
            i += len;
            onDecodeTlvObjectListner.onDecode(tag, len, value);

            if (((tag & 0x2000) == 0x2000) && decodeConstructedData) {
                parseTLVString(value, value.length, true, onDecodeTlvObjectListner);
            }
        }
        return 0;
    }

    public interface OnDecodeTlvObjectListner {
        public void onDecode(int tag, int len, byte[] value);
    }
}
