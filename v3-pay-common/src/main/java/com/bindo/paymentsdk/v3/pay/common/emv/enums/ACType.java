package com.bindo.paymentsdk.v3.pay.common.emv.enums;

public enum ACType {
    /**
     * 交易拒绝
     */
    AAC,
    /**
     * 交易批准
     */
    TC,
    /**
     * 需要请求联机
     */
    ARQC,
}
