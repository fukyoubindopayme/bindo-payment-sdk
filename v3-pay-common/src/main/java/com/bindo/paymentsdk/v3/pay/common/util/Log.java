package com.bindo.paymentsdk.v3.pay.common.util;

public class Log {
    private static final int JSON_INDENT = 4;

    private static String sTag = "V3-PAY";
    private static boolean sEnabled = false;
    private static boolean sUseSystemOut = false;

    private Log() {
    }

    public static boolean isEnabled() {
        return sEnabled;
    }

    public static void setEnabled(boolean enabled) {
        sEnabled = enabled;
    }

    public static boolean isUseSystemOut() {
        return sUseSystemOut;
    }

    public static void setsUseSystemOut(boolean useSystemOut) {
        sUseSystemOut = useSystemOut;
    }

    public static int v(String msg) {
        return v(sTag, msg);
    }

    public static int v(String msg, Throwable tr) {
        return v(sTag, msg, tr);
    }

    public static int v(String tag, String msg) {
        if (sUseSystemOut) {
            return print(tag, msg);
        } else if (sEnabled) {
            return android.util.Log.v(tag, msg);
        }
        return 0;
    }

    public static int v(String tag, String msg, Throwable tr) {
        if (sUseSystemOut) {
            return print(tag, msg, tr);
        } else if (sEnabled) {
            return android.util.Log.v(tag, msg, tr);
        }
        return 0;
    }

    public static int d(String msg) {
        return d(sTag, msg);
    }

    public static int d(String msg, Throwable tr) {
        return d(sTag, msg, tr);
    }

    public static int d(String tag, String msg) {
        if (sUseSystemOut) {
            return print(tag, msg);
        } else if (sEnabled) {
            return android.util.Log.d(tag, msg);
        }
        return 0;
    }

    public static int d(String tag, String msg, Throwable tr) {
        if (sUseSystemOut) {
            return print(tag, msg, tr);
        } else if (sEnabled) {
            return android.util.Log.d(tag, msg, tr);
        }
        return 0;
    }

    public static int i(String msg) {
        return i(sTag, msg);
    }

    public static int i(String msg, Throwable tr) {
        return i(sTag, msg, tr);
    }

    public static int i(String tag, String msg) {
        if (sUseSystemOut) {
            return print(tag, msg);
        } else if (sEnabled) {
            return android.util.Log.i(tag, msg);
        }
        return 0;
    }

    public static int i(String tag, String msg, Throwable tr) {
        if (sUseSystemOut) {
            return print(tag, msg, tr);
        } else if (sEnabled) {
            return android.util.Log.i(tag, msg, tr);
        }
        return 0;
    }

    public static int w(String msg) {
        return w(sTag, msg);
    }

    public static int w(String tag, String msg) {
        if (sUseSystemOut) {
            return print(tag, msg);
        } else if (sEnabled) {
            return android.util.Log.w(tag, msg);
        }
        return 0;
    }

    public static int w(String tag, String msg, Throwable tr) {
        if (sUseSystemOut) {
            return print(tag, msg, tr);
        } else if (sEnabled) {
            return android.util.Log.w(tag, msg, tr);
        }
        return 0;
    }

    public static int w(String tag, Throwable tr) {
        if (sUseSystemOut) {
            return print(tag, tr);
        } else if (sEnabled) {
            return android.util.Log.w(tag, tr);
        }
        return 0;
    }

    public static int e(String msg) {
        return e(sTag, msg);
    }

    public static int e(String msg, Throwable tr) {
        return e(sTag, msg, tr);
    }

    public static int e(String tag, String msg) {
        if (sUseSystemOut) {
            return print(tag, msg);
        } else if (sEnabled) {
            return android.util.Log.e(tag, msg);
        }
        return 0;
    }

    public static int e(String tag, String msg, Throwable tr) {
        if (sUseSystemOut) {
            return print(tag, msg, tr);
        } else if (sEnabled) {
            return android.util.Log.e(tag, msg, tr);
        }
        return 0;
    }

    private static int print(String tag, String msg) {
        System.out.println(String.format("%s - %s", tag, msg));
        return 0;
    }

    private static int print(String tag, Throwable tr) {
        String msg = "";
        if (tr != null) {
            msg = tr.getMessage();
        }
        System.out.println(String.format("%s - %s", tag, msg));
        return 0;
    }

    private static int print(String tag, String msg, Throwable tr) {
        System.out.println(String.format("%s - %s", tag, msg));
        String trMsg = "";
        if (tr != null) {
            trMsg = tr.getMessage();
        }
        System.out.println(String.format("%s - %s", tag, trMsg));
        return 0;
    }
}