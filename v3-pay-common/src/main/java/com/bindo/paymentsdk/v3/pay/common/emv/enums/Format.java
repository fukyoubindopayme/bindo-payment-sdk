package com.bindo.paymentsdk.v3.pay.common.emv.enums;

public enum Format {

    /**
     * Only alphabetic (a to z and A to Z)
     */
    ALPHABETIC,

    /**
     * Alphabetic and numeric (a to z, A to Z and 0 to 9)
     */
    ALPHANUMERIC,

    /**
     *
     */
    ALPHANUMERIC_SPECIAL,

    /**
     *
     */
    BINARY,

    /**
     *
     */
    COMPRESSED_NUMERIC,

    /**
     *
     */
    NUMERIC,

    /**
     *
     */
    VARIABLE;
}