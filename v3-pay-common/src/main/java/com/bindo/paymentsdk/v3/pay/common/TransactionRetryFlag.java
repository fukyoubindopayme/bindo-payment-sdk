package com.bindo.paymentsdk.v3.pay.common;

public enum TransactionRetryFlag {
    DEFAULT,
    FORCE_USE_CHIP,
    FORCE_USE_MAG,
    FORCE_USE_CHIP_AND_MAG,
    SEE_PHONE_FOR_INSTRUCTIONS,
}
