package com.bindo.paymentsdk.v3.pay.common;

/**
 * 参考资料：
 * - http://jpos.org/doc/jPOS-CMF.pdf
 * - http://www.admfactory.com/iso8583-flows-data-elements-meaning-and-values/
 * Created by Leo on 3/10/2017.
 */

public enum TransactionType {
    /**
     * 下载终端厂商公钥(用于在下载主密钥流程中加密传输密钥)
     */
    FACTORY_PUBLIC_KEY_DOWNLOAD,
    /**
     * 下载主密钥
     */
    TMK_DOWNLOAD,
    /**
     * 下载Local BIN Table
     */
    LOCAL_BIN_TABLE_DOWNLOAD,
    /**
     * 下载PYC Table
     */
    PYC_TABLE_DOWNLOAD,

    /**
     * 下载CURRENCY_TABLE
     */
    CURRENCY_TABLE_DOWNLOAD,

    /**
     * 下载Report
     */
    REPORT_DOWNLOAD,

    /**
     * 消费
     */
    SALE,
    /**
     * 消费撤销
     */
    VOID_SALE,
    /**
     * 授权
     * FIXME 无此交易，讨论后去除
     */
    AUTHORIZATION,
    /**
     * 授权撤销
     * FIXME 无此交易，讨论后去除
     */
    AUTHORIZATION_VOID,
    /**
     * 预授权
     */
    PRE_AUTHORIZATION,
    /**
     * 预授权撤销
     */
    PRE_AUTHORIZATION_VOID,
    /**
     * 预授权完成
     */
    PRE_AUTHORIZATION_COMPLETION,
    /**
     * 预授权完成撤销
     * FIXME 新增交易
     */
    PRE_AUTHORIZATION_COMPLETION_VOID,
    /**
     * 冲正
     */
    REVERSAL,
    /**
     * 退货
     */
    REFUND,
    /**
     * 退货撤销
     * FIXME 无此交易，讨论后去除
     */
    REFUND_VOID,
    /**
     * 结算
     */
    SETTLEMENT,
    /**
     * 批上送
     */
    BATCH_UPLOAD,
    /**
     * 汇率查询
     */
    RATE_LOOKUP,
    /**
     * 离线消费
     */
    OFFLINE_SALE,
    /**
     * 消费调整
     */
    ADJUST_SALE,

    BALANCE_INQUIRY,

    AUTHORIZATION_TIMEOUT,

    AUTHORIZATION_ADVICE,

}
