package com.bindo.paymentsdk.v3.pay.common.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Leo.Li on 10/04/2018.
 */

public class CurrencyUtil {

    private final static String [][] CURRENCY_TABLE = new String [][] {
            {"BZD", "084", "2", "Belize Dollar"},
            {"XOF", "952", "0", "CFA Franc BCEAO"},
            {"BMD", "060", "2", "Bermudian Dollar"},
            {"BTN", "064", "2", "Ngultrum"},
            {"INR", "356", "2", "Indian Rupee"},
            {"BOB", "068", "2", "Boliviano"},
            {"BOV", "984", "2", "Mvdol"},
            {"BAM", "977", "2", "Convertible Mark"},
            {"BWP", "072", "2", "Pula"},
            {"NOK", "578", "2", "Norwegian Krone"},
            {"BRL", "986", "2", "Brazilian Real"},
            {"BND", "096", "2", "Brunei Dollar"},
            {"BGN", "975", "2", "Bulgarian Lev"},
            {"BIF", "108", "0", "Burundi Franc"},
            {"KHR", "116", "2", "Riel"},
            {"XAF", "950", "0", "CFA Franc BEAC"},
            {"CAD", "124", "2", "Canadian Dollar"},
            {"CVE", "132", "2", "Cape Verde Escudo"},
            {"KYD", "136", "2", "Cayman Islands Dollar"},
            {"CLF", "990", "4", "Unidad de Fomento"},
            {"CLP", "152", "0", "Chilean Peso"},
            {"CNY", "156", "2", "Yuan Renminbi"},
            {"COP", "170", "2", "Colombian Peso"},
            {"COU", "970", "2", "Unidad de Valor Real"},
            {"KMF", "174", "0", "Comoro Franc"},
            {"CDF", "976", "2", "Congolese Franc"},
            {"NZD", "554", "2", "New Zealand Dollar"},
            {"CRC", "188", "2", "Costa Rican Colon"},
            {"HRK", "191", "2", "Croatian Kuna"},
            {"CUC", "931", "2", "Peso Convertible"},
            {"CUP", "192", "2", "Cuban Peso"},
            {"ANG", "532", "2", "Netherlands Antillean Guilder"},
            {"CZK", "203", "2", "Czech Koruna"},
            {"DKK", "208", "2", "Danish Krone"},
            {"DJF", "262", "0", "Djibouti Franc"},
            {"DOP", "214", "2", "Dominican Peso"},
            {"EGP", "818", "2", "Egyptian Pound"},
            {"SVC", "222", "2", "El Salvador Colon"},
            {"ERN", "232", "2", "Nakfa"},
            {"ETB", "230", "2", "Ethiopian Birr"},
            {"FKP", "238", "2", "Falkland Islands Pound"},
            {"FJD", "242", "2", "Fiji Dollar"},
            {"XPF", "953", "0", "CFP Franc"},
            {"GMD", "270", "2", "Dalasi"},
            {"GEL", "981", "2", "Lari"},
            {"GHS", "936", "2", "Ghana Cedi"},
            {"GIP", "292", "2", "Gibraltar Pound"},
            {"GTQ", "320", "2", "Quetzal"},
            {"GBP", "826", "2", "Pound Sterling"},
            {"GNF", "324", "0", "Guinea Franc"},
            {"GYD", "328", "2", "Guyana Dollar"},
            {"HTG", "332", "2", "Gourde"},
            {"HNL", "340", "2", "Lempira"},
            {"HKD", "344", "2", "Hong Kong Dollar"},
            {"HUF", "348", "2", "Forint"},
            {"ISK", "352", "0", "Iceland Krona"},
            {"IDR", "360", "2", "Rupiah"},
            {"IRR", "364", "2", "Iranian Rial"},
            {"IQD", "368", "3", "Iraqi Dinar"},
            {"ILS", "376", "2", "New Israeli Sheqel"},
            {"JMD", "388", "2", "Jamaican Dollar"},
            {"JPY", "392", "0", "Yen"},
            {"JOD", "400", "3", "Jordanian Dinar"},
            {"KZT", "398", "2", "Tenge"},
            {"KES", "404", "2", "Kenyan Shilling"},
            {"KPW", "408", "2", "North Korean Won"},
            {"KRW", "410", "0", "Won"},
            {"KWD", "414", "3", "Kuwaiti Dinar"},
            {"KGS", "417", "2", "Som"},
            {"LAK", "418", "2", "Kip"},
            {"LBP", "422", "2", "Lebanese Pound"},
            {"LSL", "426", "2", "Loti"},
            {"ZAR", "710", "2", "Rand"},
            {"LRD", "430", "2", "Liberian Dollar"},
            {"LYD", "434", "3", "Libyan Dinar"},
            {"CHF", "756", "2", "Swiss Franc"},
            {"LTL", "440", "2", "Lithuanian Litas"},
            {"MOP", "446", "2", "Pataca"},
            {"MKD", "807", "2", "Denar"},
            {"MGA", "969", "2", "Malagasy Ariary"},
            {"MWK", "454", "2", "Kwacha"},
            {"MYR", "458", "2", "Malaysian Ringgit"},
            {"MVR", "462", "2", "Rufiyaa"},
            {"AFN", "971", "2", "Afghani"},
            {"EUR", "978", "2", "Euro"},
            {"ALL", "008", "2", "Lek"},
            {"DZD", "012", "2", "Algerian Dinar"},
            {"USD", "840", "2", "US Dollar"},
            {"AOA", "973", "2", "Kwanza"},
            {"XCD", "951", "2", "East Caribbean Dollar"},
            {"ARS", "032", "2", "Argentine Peso"},
            {"AMD", "051", "2", "Armenian Dram"},
            {"AWG", "533", "2", "Aruban Florin"},
            {"AUD", "036", "2", "Australian Dollar"},
            {"AZN", "944", "2", "Azerbaijanian Manat"},
            {"BSD", "044", "2", "Bahamian Dollar"},
            {"BHD", "048", "3", "Bahraini Dinar"},
            {"BDT", "050", "2", "Taka"},
            {"BBD", "052", "2", "Barbados Dollar"},
            {"BYR", "974", "0", "Belarussian Ruble"},
            {"MRO", "478", "2", "Ouguiya"},
            {"MUR", "480", "2", "Mauritius Rupee"},
            {"MXN", "484", "2", "Mexican Peso"},
            {"MXV", "979", "2", "Mexican Unidad de Inversion (UDI)"},
            {"MDL", "498", "2", "Moldovan Leu"},
            {"MNT", "496", "2", "Tugrik"},
            {"MAD", "504", "2", "Moroccan Dirham"},
            {"MZN", "943", "2", "Mozambique Metical"},
            {"MMK", "104", "2", "Kyat"},
            {"NAD", "516", "2", "Namibia Dollar"},
            {"NPR", "524", "2", "Nepalese Rupee"},
            {"NIO", "558", "2", "Cordoba Oro"},
            {"NGN", "566", "2", "Naira"},
            {"OMR", "512", "3", "Rial Omani"},
            {"PKR", "586", "2", "Pakistan Rupee"},
            {"PAB", "590", "2", "Balboa"},
            {"PGK", "598", "2", "Kina"},
            {"PYG", "600", "0", "Guarani"},
            {"PEN", "604", "2", "Nuevo Sol"},
            {"PHP", "608", "2", "Philippine Peso"},
            {"PLN", "985", "2", "Zloty"},
            {"QAR", "634", "2", "Qatari Rial"},
            {"RON", "946", "2", "New Romanian Leu"},
            {"RUB", "643", "2", "Russian Ruble"},
            {"RWF", "646", "0", "Rwanda Franc"},
            {"SHP", "654", "2", "Saint Helena Pound"},
            {"WST", "882", "2", "Tala"},
            {"STD", "678", "2", "Dobra"},
            {"SAR", "682", "2", "Saudi Riyal"},
            {"RSD", "941", "2", "Serbian Dinar"},
            {"SCR", "690", "2", "Seychelles Rupee"},
            {"SLL", "694", "2", "Leone"},
            {"SGD", "702", "2", "Singapore Dollar"},
            {"SBD", "090", "2", "Solomon Islands Dollar"},
            {"SOS", "706", "2", "Somali Shilling"},
            {"SSP", "728", "2", "South Sudanese Pound"},
            {"LKR", "144", "2", "Sri Lanka Rupee"},
            {"SDG", "938", "2", "Sudanese Pound"},
            {"SRD", "968", "2", "Surinam Dollar"},
            {"SZL", "748", "2", "Lilangeni"},
            {"SEK", "752", "2", "Swedish Krona"},
            {"CHE", "947", "2", "WIR Euro"},
            {"CHW", "948", "2", "WIR Franc"},
            {"SYP", "760", "2", "Syrian Pound"},
            {"TWD", "901", "2", "New Taiwan Dollar"},
            {"TJS", "972", "2", "Somoni"},
            {"TZS", "834", "2", "Tanzanian Shilling"},
            {"THB", "764", "2", "Baht"},
            {"TOP", "776", "2", "Pa’anga"},
            {"TTD", "780", "2", "Trinidad and Tobago Dollar"},
            {"TND", "788", "3", "Tunisian Dinar"},
            {"TRY", "949", "2", "Turkish Lira"},
            {"TMT", "934", "2", "Turkmenistan New Manat"},
            {"UGX", "800", "0", "Uganda Shilling"},
            {"UAH", "980", "2", "Hryvnia"},
            {"AED", "784", "2", "UAE Dirham"},
            {"USN", "997", "2", "US Dollar (Next day)"},
            {"UYI", "940", "0", "Uruguay Peso en Unidades Indexadas (URUIURUI)"},
            {"UYU", "858", "2", "Peso Uruguayo"},
            {"UZS", "860", "2", "Uzbekistan Sum"},
            {"VUV", "548", "0", "Vatu"},
            {"VEF", "937", "2", "Bolivar"},
            {"VND", "704", "0", "Dong"},
            {"YER", "886", "2", "Yemeni Rial"},
            {"ZMW", "967", "2", "Zambian Kwacha"},
            {"ZWL", "932", "2", "Zimbabwe Dollar"},
    };

    private static Map<String, Integer> currencyCodesMap = new HashMap<String, Integer>();
    private static Map<String, Integer> currencyNumCodesMap = new HashMap<String, Integer>();

    static {
        for (int i = 0; i < CURRENCY_TABLE.length; i++) {
            currencyCodesMap.put(CURRENCY_TABLE[i][0], i);
            currencyNumCodesMap.put(CURRENCY_TABLE[i][1], i);
        }
    }

    public static String toAlphabeticCode(String currencyNumCode) throws Exception {
        Integer i = currencyNumCodesMap.get(currencyNumCode);
        if (i == null) {
            throw new Exception(currencyNumCode + " is unsupported currency");
        } else {
            return CURRENCY_TABLE[i][0];
        }
    }

    public static String toCurrencyCode(String currencyAlphaCode) throws Exception {
        Integer i = currencyCodesMap.get(currencyAlphaCode);
        if (i == null) {
            throw new Exception(currencyAlphaCode + " is unsupported currency");
        } else {
            return CURRENCY_TABLE[i][1];
        }
    }

    public static int getCurrencyExponent(String currencyNumCode) throws Exception {
        Integer i = currencyNumCodesMap.get(currencyNumCode);
        if (i == null) {
            throw new Exception(currencyNumCode + " is unsupported currency");
        } else {
            return Integer.parseInt(CURRENCY_TABLE[i][2]);
        }
    }

    public static String getCurrencyName(String currencyNumCode) throws Exception {
        Integer i = currencyNumCodesMap.get(currencyNumCode);
        if (i == null) {
            throw new Exception(currencyNumCode + " is unsupported currency");
        } else {
            return CURRENCY_TABLE[i][3];
        }
    }

    /**
     * Formats amount to the commonly used 12 digits format, considering currency exponent
     * 1 USD = 000000000100
     * 1 TND = 000000001000
     * 1 JPY = 000000000001
     * @param amount
     * @param currencyCode
     * @return
     * @throws Exception
     */
    public static String formatAmount (double amount, String currencyCode) throws Exception {
        int exponent = CurrencyUtil.getCurrencyExponent(currencyCode);
        return String.format("%012d", Math.round(amount * Math.pow(10, exponent)));
    }
}
