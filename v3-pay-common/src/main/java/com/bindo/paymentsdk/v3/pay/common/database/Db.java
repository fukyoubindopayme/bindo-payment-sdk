package com.bindo.paymentsdk.v3.pay.common.database;

public interface Db {
    Table table(Class clazz);
}
