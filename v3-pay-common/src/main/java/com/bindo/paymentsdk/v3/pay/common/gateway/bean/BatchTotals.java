package com.bindo.paymentsdk.v3.pay.common.gateway.bean;

/**
 * Created by Mark on 2018/3/22.
 */

public class BatchTotals {
    private int CapturedSalesCount;
    private int CapturedSalesAmount;
    private int CapturedRefundCount;
    private int CapturedRefundAmount ;
    private int DebitSalesCount;
    private int DebitSalesAmount  ;
    private int DebitRefundCount ;
    private int DebitRefundAmount ;
    private int AuthorizeSalesCount;
    private int AuthorizeSalesAmount  ;
    private int AuthorizeRefundCount ;
    private int AuthorizeRefundAmount;

    public BatchTotals(int capturedSalesCount, int capturedSalesAmount, int capturedRefundCount, int capturedRefundAmount, int debitSalesCount, int debitSalesAmount, int debitRefundCount, int debitRefundAmount, int authorizeSalesCount, int authorizeSalesAmount, int authorizeRefundCount, int authorizeRefundAmount) {
        CapturedSalesCount = capturedSalesCount;
        CapturedSalesAmount = capturedSalesAmount;
        CapturedRefundCount = capturedRefundCount;
        CapturedRefundAmount = capturedRefundAmount;
        DebitSalesCount = debitSalesCount;
        DebitSalesAmount = debitSalesAmount;
        DebitRefundCount = debitRefundCount;
        DebitRefundAmount = debitRefundAmount;
        AuthorizeSalesCount = authorizeSalesCount;
        AuthorizeSalesAmount = authorizeSalesAmount;
        AuthorizeRefundCount = authorizeRefundCount;
        AuthorizeRefundAmount = authorizeRefundAmount;
    }

    public int getCapturedSalesCount() {
        return CapturedSalesCount;
    }

    public void setCapturedSalesCount(int capturedSalesCount) {
        CapturedSalesCount = capturedSalesCount;
    }

    public int getCapturedSalesAmount() {
        return CapturedSalesAmount;
    }

    public void setCapturedSalesAmount(int capturedSalesAmount) {
        CapturedSalesAmount = capturedSalesAmount;
    }

    public int getCapturedRefundCount() {
        return CapturedRefundCount;
    }

    public void setCapturedRefundCount(int capturedRefundCount) {
        CapturedRefundCount = capturedRefundCount;
    }

    public int getCapturedRefundAmount() {
        return CapturedRefundAmount;
    }

    public void setCapturedRefundAmount(int capturedRefundAmount) {
        CapturedRefundAmount = capturedRefundAmount;
    }

    public int getDebitSalesCount() {
        return DebitSalesCount;
    }

    public void setDebitSalesCount(int debitSalesCount) {
        DebitSalesCount = debitSalesCount;
    }

    public int getDebitSalesAmount() {
        return DebitSalesAmount;
    }

    public void setDebitSalesAmount(int debitSalesAmount) {
        DebitSalesAmount = debitSalesAmount;
    }

    public int getDebitRefundCount() {
        return DebitRefundCount;
    }

    public void setDebitRefundCount(int debitRefundCount) {
        DebitRefundCount = debitRefundCount;
    }

    public int getDebitRefundAmount() {
        return DebitRefundAmount;
    }

    public void setDebitRefundAmount(int debitRefundAmount) {
        DebitRefundAmount = debitRefundAmount;
    }

    public int getAuthorizeSalesCount() {
        return AuthorizeSalesCount;
    }

    public void setAuthorizeSalesCount(int authorizeSalesCount) {
        AuthorizeSalesCount = authorizeSalesCount;
    }

    public int getAuthorizeSalesAmount() {
        return AuthorizeSalesAmount;
    }

    public void setAuthorizeSalesAmount(int authorizeSalesAmount) {
        AuthorizeSalesAmount = authorizeSalesAmount;
    }

    public int getAuthorizeRefundCount() {
        return AuthorizeRefundCount;
    }

    public void setAuthorizeRefundCount(int authorizeRefundCount) {
        AuthorizeRefundCount = authorizeRefundCount;
    }

    public int getAuthorizeRefundAmount() {
        return AuthorizeRefundAmount;
    }

    public void setAuthorizeRefundAmount(int authorizeRefundAmount) {
        AuthorizeRefundAmount = authorizeRefundAmount;
    }

    public BatchTotals() {

    }
}
