package com.bindo.paymentsdk.v3.pay.common.gateway.enums;

public enum POSEntryMode {
    UNKNOWN,
    MANUAL,
}
