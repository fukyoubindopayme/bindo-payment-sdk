package com.bindo.paymentsdk.v3.pay.common.database.models;

import com.bindo.paymentsdk.v3.pay.common.util.JSONUtils;

/**
 * Created by miskolee on 2018/4/3.
 */

public class CountryToCurrency {

    private String numericCountryCode;
    private String numericCurrencyCode;
    private String association;

    public String getNumericCountryCode() {
        return numericCountryCode;
    }

    public void setNumericCountryCode(String numericCountryCode) {
        this.numericCountryCode = numericCountryCode;
    }

    public String getNumericCurrencyCode() {
        return numericCurrencyCode;
    }

    public void setNumericCurrencyCode(String numericCurrencyCode) {
        this.numericCurrencyCode = numericCurrencyCode;
    }

    public String getAssociation() {
        return association;
    }

    public void setAssociation(String association) {
        this.association = association;
    }
    @Override
    public String toString() {
        try {
            return JSONUtils.toJSONString(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.toString();
    }
}
