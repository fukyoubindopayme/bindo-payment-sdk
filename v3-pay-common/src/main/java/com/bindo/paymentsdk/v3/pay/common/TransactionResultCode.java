package com.bindo.paymentsdk.v3.pay.common;

public enum TransactionResultCode {
    /**
     * 交易脱机批准
     */
    APPROVED_BY_OFFLINE,
    /**
     * 交易联机批准
     */
    APPROVED_BY_ONLINE,
    /**
     * 交易脱机拒绝
     */
    DECLINED_BY_OFFLINE,
    /**
     * 交易联机拒绝
     */
    DECLINED_BY_ONLINE,
    /**
     * 交易联机超时，并且冲正成功
     */
    DECLINED_BY_TIMEOUT_AND_REVERSED,
    /**
     * 交易终端拒绝，并且冲正成功
     */
    DECLINED_BY_TERMINAL_AND_REVERSED,
    /**
     * 寻卡失败（未知）
     */
    ERROR_CARD_DETECT_UNUSUAL,
    /**
     * 寻卡超时
     */
    ERROR_CARD_DETECT_TIMEOUT,
    /**
     * 寻卡错误，存在多张卡片
     */
    ERROR_CARD_DETECT_MULTIPLE_CARDS,
    /**
     * 不受理
     */
    ERROR_CARD_NOT_ACCEPTED,
    /**
     * 卡片被禁用
     */
    ERROR_CARD_BLOCKED,
//    /**
//     * 参照手机
//     */
//    ERROR_SEE_PHONE_FOR_INSTRUCTIONS,
//    /**
//     * 尝试其它界面
//     */
//    ERROR_TRY_ANOTHER_INTERFACE,
    /**
     * 未知错误
     */
    ERROR_UNKNOWN,
}
