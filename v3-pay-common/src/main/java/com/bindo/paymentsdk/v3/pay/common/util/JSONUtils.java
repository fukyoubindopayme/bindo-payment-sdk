package com.bindo.paymentsdk.v3.pay.common.util;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class JSONUtils {
    public static String toJSONString(Object object) throws Exception {
        String jsonString = "";
        try {
            jsonString += "{";
            Field[] fields = object.getClass().getDeclaredFields();
            int fieldCount = 0;
            for (Field field : fields) {
                // 设置成员可访问权限
                field.setAccessible(true);
                try {
                    String fieldKey = field.getName();
                    Object fieldValue = field.get(object);
                    // 忽略 Final 和 Static 关键词修饰的 Field
                    if (Modifier.isFinal(field.getModifiers()) || Modifier.isStatic(field.getModifiers())) {
                        continue;
                    }
                    // 忽略不为 null 的成员
                    if (fieldValue != null) {
                        jsonString += "\"" + fieldKey + "\": \"" + fieldValue.toString() + "\",";
                        fieldCount += 1;
                    }
                } catch (IllegalAccessException | IllegalArgumentException e) {
                    e.printStackTrace();
                }
            }
            if (fieldCount > 0) {
                jsonString = jsonString.substring(0, jsonString.length() - 1);
            }
            jsonString += "}";

            jsonString = jsonString.trim();
        } catch (RuntimeException e) {
            // Ignore
        }
        return jsonString;
    }
}
