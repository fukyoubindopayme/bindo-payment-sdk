package com.bindo.paymentsdk.v3.pay.common.util;

import java.util.HashMap;

public class TagLengthValuesUtils {

    public static HashMap<String, String> convertToHashMapStringString(HashMap<String, byte[]> tagLengthValues) {
        if (tagLengthValues == null) {
            return null;
        }

        HashMap<String, String> tagLengthStringValues = new HashMap<>();
        for (String key : tagLengthValues.keySet()) {
            byte[] byteArray = tagLengthValues.get(key);
            tagLengthStringValues.put(key, Hex.encode(byteArray));
        }
        return tagLengthStringValues;
    }

    public static HashMap<String, byte[]> convertToHashMapStringByteArray(HashMap<String, String> tagLengthStringValues) {
        if (tagLengthStringValues == null) {
            return null;
        }

        HashMap<String, byte[]> tagLengthValues = new HashMap<>();
        for (String key : tagLengthStringValues.keySet()) {
            String hexString = tagLengthStringValues.get(key);
            tagLengthValues.put(key, Hex.decode(hexString));
        }
        return tagLengthValues;
    }
}
