package com.bindo.paymentsdk.v3.pay.common.emv.enums;

/**
 * Created by Mark on 2017/10/26.
 */

public class FlowType {
    public static final byte EMV = 0x00;
    public static final byte ECASH = 0x01;
    public static final byte QPBOC = 0x02;
    public static final byte PBOC_CONTACTLESS = 0x03;
    public static final byte MSD = 0x04;
    public static final byte MSD_LEGACY = 0x05;
    public static final byte VISA_QVSDC = 0x06;
    public static final byte VISA_PAYWAVE2 = 0x07;
    public static final byte PAYPASS_CHIP = 0x08;
    public static final byte PAYPASS_STRIP = 0x09;
    public static final byte AMEX_MAGSRTIPE = 0x0A;
    public static final byte AMEX_EMV = 0x0B;
    public static final byte AMEX_MOBILE_MAGSTRIPE = 0x0C;
}
