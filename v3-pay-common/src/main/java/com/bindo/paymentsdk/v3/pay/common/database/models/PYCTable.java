package com.bindo.paymentsdk.v3.pay.common.database.models;

import com.bindo.paymentsdk.v3.pay.common.util.JSONUtils;

/**
 * Created by huhai on 2018/4/4.
 */

public class PYCTable {
    private String lowPrefixNumber;
    private String highPrefixNumber;
    private String status;

    public String getLowPrefixNumber() {
        return lowPrefixNumber;
    }

    public void setLowPrefixNumber(String lowPrefixNumber) {
        this.lowPrefixNumber = lowPrefixNumber;
    }

    public String getHighPrefixNumber() {
        return highPrefixNumber;
    }

    public void setHighPrefixNumber(String highPrefixNumber) {
        this.highPrefixNumber = highPrefixNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    @Override
    public String toString() {
        try {
            return JSONUtils.toJSONString(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.toString();
    }
}
