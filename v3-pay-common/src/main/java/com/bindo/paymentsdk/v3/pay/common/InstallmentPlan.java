package com.bindo.paymentsdk.v3.pay.common;

/**
 * Created by Leo.Li on 02/04/2018.
 */

public class InstallmentPlan {
    public InstallmentPlan(String name, String description, int numberOfInstallment) {
        this.name = name;
        this.description = description;
        this.numberOfInstallment = numberOfInstallment;
    }

    private String name;
    private String description;
    private int numberOfInstallment;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNumberOfInstallment() {
        return numberOfInstallment;
    }

    public void setNumberOfInstallment(int numberOfInstallment) {
        this.numberOfInstallment = numberOfInstallment;
    }
}
