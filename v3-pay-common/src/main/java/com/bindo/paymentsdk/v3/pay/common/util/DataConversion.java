package com.bindo.paymentsdk.v3.pay.common.util;

import java.text.DecimalFormat;
import java.util.HashMap;

public class DataConversion {
    public static byte[] convertFloatToHexBytes(float number) {
        String hexString = Integer.toHexString((int) (number * 100));
        hexString = "00000000".substring(hexString.length()) + hexString;
        return Hex.decode(hexString);
    }

    public static byte[] convertFloatToBcdBytes(float number) {
        String numberString = new DecimalFormat("0000000000.00").format(number).replace(".", "");
        return Hex.decode(numberString);
    }

    public static String markupStringTextToNumbericText(String markupStringText)
    {
        HashMap<String , String>stringNumericMap = new HashMap<>();
        stringNumericMap.put("zero", "0");
        stringNumericMap.put("one", "1");
        stringNumericMap.put("two", "2");
        stringNumericMap.put("three", "3");
        stringNumericMap.put("four", "4");
        stringNumericMap.put("five", "5");
        stringNumericMap.put("six", "6");
        stringNumericMap.put("seven", "7");
        stringNumericMap.put("eight", "8");
        stringNumericMap.put("nine", "9");
        stringNumericMap.put("ten", "10");
        stringNumericMap.put("pt.", ".");

        String numbericText = "";
        String [] strArray = markupStringText.split(" ");

        for (int i = 0; i < strArray.length; i++) {
            numbericText += stringNumericMap.get(strArray[i]);
        }

        return numbericText;
    }
}
