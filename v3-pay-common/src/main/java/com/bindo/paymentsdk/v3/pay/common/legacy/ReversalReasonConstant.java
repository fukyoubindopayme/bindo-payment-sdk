package com.bindo.paymentsdk.v3.pay.common.legacy;

/**
 * Created by Mark on 2017/9/22.
 */

@Deprecated
public class ReversalReasonConstant {
    public static int TIMEOUT_REVERSAL = 0;
    public static int AUTHORIZATION_REVERSAL = 1;
}
