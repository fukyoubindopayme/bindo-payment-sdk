package com.bindo.paymentsdk.v3.pay.common;

public class CardDetectMode {
    public static final String MANUAL = "manual";
    public static final String SWIPE = "swipe";
    public static final String FALLBACK_SWIPE = "fallback_swipe";
    public static final String CONTACT = "contact";
    public static final String CONTACTLESS = "contactless";
    public static final String QRCODE = "qrcode";
}
