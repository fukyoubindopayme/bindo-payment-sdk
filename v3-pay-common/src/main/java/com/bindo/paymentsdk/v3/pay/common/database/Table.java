package com.bindo.paymentsdk.v3.pay.common.database;

import java.util.HashMap;
import java.util.List;

public interface Table<T> {
    public static final String WHERE_TYPE_EQUAL_TO = "equalTo";

    public static final String WHERE_TYPE = "where_type";
    public static final String WHERE_KEY = "where_key";
    public static final String WHERE_VALUE = "where_value";

    void create(T object);

    void update(T object);

    List<T> all();

    List<T> get(HashMap[] wheres, HashMap[] orderBy);

    T first();

    T first(HashMap[] wheres, HashMap[] orderBy);

    void clear();

    void delete(String key, String value);
}
