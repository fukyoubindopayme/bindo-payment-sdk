package com.bindo.paymentsdk.v3.pay.common.legacy.enums;

@Deprecated
public enum CardReadMode {
    /**
     * 接触式
     */
    CONTACT,

    /**
     * 非接触式
     */
    CONTACTLESS,

    /**
     * 刷卡
     */
    SWIPE,

    /**
     * 刷卡（降级）
     */
    FALLBACK_SWIPE,

    /**
     * 手动输入
     */
    MANUAL,

    /**
     * 二维码
     */
    QRCODE
}
