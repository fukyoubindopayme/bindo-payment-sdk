package com.bindo.paymentsdk.v3.pay.common.legacy;

import com.bindo.paymentsdk.v3.pay.common.CardDetectMode;
import com.bindo.paymentsdk.v3.pay.common.emv.enums.EMVTag;
import com.bindo.paymentsdk.v3.pay.common.legacy.enums.CardReadMode;
import com.bindo.paymentsdk.v3.pay.common.util.Hex;
import com.bindo.paymentsdk.v3.pay.common.util.JSONUtils;

import java.util.HashMap;

public class CreditCard {
    private CardReadMode cardReadMode = CardReadMode.MANUAL;
    private String cardNumber;
    private String expireDate;
    private String holderName;
    private String serviceCode;
    private MagData magData;
    private EmvData emvData;

    public String getCardDetectMode() {
        switch (cardReadMode) {
            case SWIPE:
                return CardDetectMode.SWIPE;
            case FALLBACK_SWIPE:
                return CardDetectMode.FALLBACK_SWIPE;
            case CONTACT:
                return CardDetectMode.CONTACT;
            case CONTACTLESS:
                return CardDetectMode.CONTACTLESS;
            case MANUAL:
            default:
                return CardDetectMode.MANUAL;
        }
    }

    public CardReadMode getCardReadMode() {
        return cardReadMode;
    }

    public void setCardReadMode(CardReadMode cardReadMode) {
        this.cardReadMode = cardReadMode;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getHolderName() {
        return holderName;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public MagData getMagData() {
        return magData;
    }

    public void setMagData(MagData magData) {
        this.magData = magData;
    }

    public EmvData getEmvData() {
        return emvData;
    }

    public void setEmvData(EmvData emvData) {
        this.emvData = emvData;
    }

    public static class MagData {
        private String track1;
        private String track2;

        public MagData(String track1, String track2) {
            this.track1 = track1;
            this.track2 = track2;
        }

        public String getTrack1() {
            return track1;
        }

        public void setTrack1(String track1) {
            this.track1 = track1;
        }

        public String getTrack2() {
            return track2;
        }

        public void setTrack2(String track2) {
            this.track2 = track2;
        }

        @Override
        public String toString() {
            return "MagData{" +
                    "track1='" + track1 + '\'' +
                    ", track2='" + track2 + '\'' +
                    '}';
        }
    }

    public static class EmvData {
        private String track1;
        private String track2;
        private String iccData;
        private HashMap<String, byte[]> tagLengthValues = new HashMap<>();

        public EmvData(String track1, String track2, HashMap<String, byte[]> tagLengthValues) {
            this.track1 = track1;
            this.track2 = track2;
            this.iccData = "";
            this.tagLengthValues = tagLengthValues;
        }

        public String getTrack1() {
            return track1;
        }

        public void setTrack1(String track1) {
            this.track1 = track1;
        }

        public String getTrack2() {
            return track2;
        }

        public void setTrack2(String track2) {
            this.track2 = track2;
        }

        public void setIccData(String iccData) {
            this.iccData = iccData;
        }

        public String getIccData() {
            EMVTag[] emvTags = new EMVTag[]{
                    EMVTag.TRACK_2_EQUIVALENT_DATA,
                    EMVTag.APPLICATION_PRIMARY_ACCOUNT_NUMBER,
                    EMVTag.TRANSACTION_CURRENCY_CODE,
                    EMVTag.APPLICATION_PRIMARY_ACCOUNT_NUMBER_SEQUENCE_NUMBER,
                    EMVTag.APPLICATION_INTERCHANGE_PROFILE,
                    EMVTag.DEDICATED_FILE_NAME,
                    EMVTag.TERMINAL_VERIFICATION_RESULTS,
                    EMVTag.TRANSACTION_DATE,
                    EMVTag.TRANSACTION_STATUS_INFORMATION,
                    EMVTag.TRANSACTION_TYPE,
                    EMVTag.AMOUNT_AUTHORISED_NUMERIC,
                    EMVTag.AMOUNT_OTHER_NUMERIC,
                    EMVTag.APPLICATION_VERSION_NUMBER_ICC,
                    EMVTag.APPLICATION_VERSION_NUMBER_TERMINAL,
                    EMVTag.TERMINAL_COUNTRY_CODE,
                    EMVTag.INTERFACE_DEVICE_SERIAL_NUMBER,
                    EMVTag.APPLICATION_CRYPTOGRAM,
                    EMVTag.CRYPTOGRAM_INFORMATION_DATA,
                    EMVTag.TERMINAL_CAPABILITIES,
                    EMVTag.CARDHOLDER_VERIFICATION_METHOD_RESULTS,
                    EMVTag.TERMINAL_TYPE,
                    EMVTag.APPLICATION_TRANSACTION_COUNTER,
                    EMVTag.UNPREDICTABLE_NUMBER,
                    EMVTag.TRANSACTION_SEQUENCE_COUNTER,
                    EMVTag.ISSUER_APPLICATION_DATA
            };

            StringBuilder iccDataBuilder = new StringBuilder();

            for (int i = 0; i < emvTags.length; i++) {
                EMVTag tag = emvTags[i];
                byte[] value = tagLengthValues.get(tag.toString());

                if (value != null) {
                    iccDataBuilder.append(tag);
                    iccDataBuilder.append(String.format("%02X", value.length));
                    iccDataBuilder.append(Hex.encode(value));
                }
            }
            return iccDataBuilder.toString();
        }

        public HashMap<String, byte[]> getTagLengthValues() {
            return tagLengthValues;
        }

        public void setTagLengthValues(HashMap<String, byte[]> tagLengthValues) {
            this.tagLengthValues = tagLengthValues;
            this.iccData = getIccData();
        }

        @Override
        public String toString() {
            try {
                return JSONUtils.toJSONString(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @Override
    public String toString() {
        try {
            return JSONUtils.toJSONString(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
