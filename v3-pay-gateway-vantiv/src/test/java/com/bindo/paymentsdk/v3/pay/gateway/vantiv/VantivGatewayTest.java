package com.bindo.paymentsdk.v3.pay.gateway.vantiv;

import com.bindo.paymentsdk.v3.pay.common.CardDetectMode;
import com.bindo.paymentsdk.v3.pay.common.ReversalType;
import com.bindo.paymentsdk.v3.pay.common.TransactionType;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Request;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Response;
import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.gateway.vantiv.enums.NetworkManagementInformationCode;

import org.junit.Assert;
import org.junit.Test;

import java.util.Random;

public class VantivGatewayTest {

    VantivGateway vantivGateway = new VantivGateway(null);

    public VantivGatewayTest() {
        Log.setsUseSystemOut(true);
    }

    @Test
    public void allVantivTests() throws InterruptedException {
        echoTest();
        Thread.sleep(3000);
        manualSale();
        magstripeSale();
    }

    @Test
    public void echoTest() {
        vantivGateway.sendNetworkManagementRequest(NetworkManagementInformationCode.ECHO_TEST);
    }

    @Test
    public void manualSale() {
        Request request = new Request();
        request.setTransactionType(TransactionType.SALE);
        request.setTransactionAmount(10);
        request.setMerchantNameAndLocation("BINDO");
        request.setPrimaryAccountNumber("4761739001010010");

        request.setCardDetectMode(CardDetectMode.MANUAL);
        request.setCardExpirationDate("1512");

        request.setTransactionSystemTraceAuditNumber(900123);
        request.setDraftLocatorId("12345678901");

        Log.d(request.toString());
        Response response = vantivGateway.sendRequest(request);
        Log.d(response.toString());
        Assert.assertEquals("00", response.getActionCode());
    }

    @Test
    public void manualAVS() {
        Request request = new Request();
        request.setTransactionType(TransactionType.SALE);
        request.setMerchantNameAndLocation("BINDO");
        request.setPrimaryAccountNumber("4445222299990007");

        request.setCardDetectMode(CardDetectMode.MANUAL);
        request.setCardExpirationDate("1812");
        request.setCardSecurityCode("222-1");

        request.setAvsCardholderAddress("123 Main Street, Cincinnati OH");
        request.setTransactionAmount(37.04);
        request.setAvsCardholderZipcode("45204");

        Log.d(request.toString());
        Response response = vantivGateway.sendRequest(request);
        Log.d(response.toString());
        Assert.assertEquals("00", response.getActionCode());
    }

    @Test
    public void magstripeSale() {
        Request request = new Request();
        request.setTransactionType(TransactionType.SALE);
        request.setTransactionAmount(10);
        request.setMerchantNameAndLocation("BINDO");
        request.setPrimaryAccountNumber("4761739001010010");
        //request.setPrimaryAccountNumber("4445222299990007");

        request.setCardDetectMode(CardDetectMode.SWIPE);
        request.setTrack2EquivalentData("4761739001010010=15122011143857589");
        //request.setTrack2EquivalentData("4445222299990007D19121010000023700000");

        Log.d(request.toString());
        Response response = vantivGateway.sendRequest(request);
        Log.d(response.toString());
        Assert.assertEquals("00", response.getActionCode());
    }

    @Test
    public void balanceInquiry() {
        Request request = new Request();
        request.setTransactionType(TransactionType.BALANCE_INQUIRY);
        request.setTransactionAmount(0);
        request.setMerchantNameAndLocation("BINDO");
        request.setPrimaryAccountNumber("4445222299990007");

        request.setCardDetectMode(CardDetectMode.MANUAL);
        request.setCardDetectMode(CardDetectMode.SWIPE);
        request.setTrack2EquivalentData("4445222299990007=15122011143857589");

        Response response = vantivGateway.sendRequest(request);
        Assert.assertEquals("00", response.getActionCode());
    }

    private final static int CARD_VISA_1                = 0;
    private final static int CARD_MASTERCARD_1          = 1;
    private final static int CARD_DISCOVER_1            = 2;
    private final static int CARD_DISCOVER_2            = 3;
    private final static int CARD_DINERS_CUP            = 4;
    private final static int CARD_DISCOVER_CUP          = 5;
    private final static int CARD_AMEX_1                = 6;
    private final static int CARD_AMEX_2                = 7;
    private final static int CARD_JCB                   = 8;
    private final static int CARD_VISA_PURCHASING       = 9;
    private final static int CARD_VISA_CORPORATE        = 10;
    private final static int CARD_MASTERCARD_BUSINESS_1 = 11;
    private final static int CARD_MASTERCARD_BUSINESS_2 = 12;
    private final static int CARD_MASTERCARD_2A         = 13;
    private final static int CARD_MASTERCARD_2B         = 14;
    private final static int CARD_MCRD_TOR_CREDIT       = 15;

    String cards[][] = {
            {"4445222299990007", "1812", "4445222299990007D19121010000023700000"},
            {"5444009999222205", "1812", "5444009999222205D21121010000050600"},
            {"6011000990911111", "1812", "6011000990911111D21121010000000000000"},
            {"6499999900017391", "1812", ""},
            {"36018612345678", "1812", ""},
            {"6221260004598744", "1812", "6221260004598744D19121010000000000000"},
            {"341111597242000", "1812", "341111597242000D21121010000000000000"},
            {"370000000000002", "1812", ""},
            {"3530111333300000", "1812", "3530111333300000D19121011234567890123"},
            {"4055060000000000", "1812", ""},
            {"4257000000000002", "1812", ""},
            {"5472060000000002", "1812", ""},
            {"5405980000337223", "1812", ""},
            {"2223000048400011", "1812", "2223000048400011D2512101111119990"},
            {"2223520043560014", "1812", "2223520043560014D2512101111119990"},
            {"", "", "5444009999222254D21091010000034500000"}
    };

    String transactionData[][] = {
            {String.valueOf(CARD_VISA_1), "S", "1.6"},
            {String.valueOf(CARD_VISA_1), "M", "1.61"},
            {String.valueOf(CARD_VISA_1), "M", "1.62"},
            {String.valueOf(CARD_VISA_1), "S", "1.63"},
            {String.valueOf(CARD_VISA_1), "S", "1.65"},
            {String.valueOf(CARD_VISA_1), "S", "2.7"},
            {String.valueOf(CARD_VISA_1), "S", "25.25"},
            {String.valueOf(CARD_VISA_1), "S", "22.51"},
            {String.valueOf(CARD_MASTERCARD_1), "S", "1.6"},
            {String.valueOf(CARD_MASTERCARD_1), "M", "1.61"},
            {String.valueOf(CARD_MASTERCARD_1), "M", "1.62"},
            {String.valueOf(CARD_MASTERCARD_1), "S", "1.63"},
            {String.valueOf(CARD_MASTERCARD_1), "S", "1.65"},
            {String.valueOf(CARD_MASTERCARD_1), "S", "2.71"},
            {String.valueOf(CARD_MASTERCARD_1), "S", "25.35"},
            {String.valueOf(CARD_MASTERCARD_1), "S", "22.61"},
            {String.valueOf(CARD_MASTERCARD_2A), "S", "41.6"},
            {String.valueOf(CARD_MASTERCARD_2A), "M", "41.61"},
            {String.valueOf(CARD_MASTERCARD_2B), "M", "51.6"},
            {String.valueOf(CARD_MASTERCARD_2B), "S", "51.61"},
            {String.valueOf(CARD_DISCOVER_1), "M", "24.67"},
            {String.valueOf(CARD_DISCOVER_1), "M", "187.75"},
            {String.valueOf(CARD_DISCOVER_1), "S", "40"},
            {String.valueOf(CARD_DISCOVER_2), "M", "56.56"},
            {String.valueOf(CARD_AMEX_1), "S", "35.01"},
            {String.valueOf(CARD_AMEX_2), "M", "34"},
            {String.valueOf(CARD_AMEX_2), "M", "189.55"},
            {String.valueOf(CARD_AMEX_1), "S", "25.55"},
            {String.valueOf(CARD_AMEX_1), "S", "22.81"},
            {String.valueOf(CARD_VISA_PURCHASING), "M", "48.48"},
            {String.valueOf(CARD_VISA_CORPORATE), "M", "49.49"},
            {String.valueOf(CARD_MASTERCARD_BUSINESS_1), "M", "50.5"},
            {String.valueOf(CARD_MASTERCARD_BUSINESS_2), "M", "51.51"},
            {String.valueOf(CARD_DINERS_CUP), "M", "52.52"},
            {String.valueOf(CARD_DINERS_CUP), "M", "53.53"},
            {String.valueOf(CARD_DISCOVER_CUP), "M", "54.54"},
            {String.valueOf(CARD_DISCOVER_CUP), "S", "55.55"},
            {String.valueOf(CARD_JCB), "M", "57.57"},
            {String.valueOf(CARD_JCB), "S", "58.58"},
    };

    @Test
    public void manualTest() {
        String[][] results = new String[transactionData.length][4];
        Random random = new Random();
        for (int i = 0; i < transactionData.length; i++) {
            Request request = new Request();
            request.setTransactionType(TransactionType.SALE);

            int card = Integer.parseInt(transactionData[i][0]);
            request.setPrimaryAccountNumber(cards[card][0]);
            if (transactionData[i][1].equals("M")) {
                request.setCardDetectMode(CardDetectMode.MANUAL);
                request.setCardExpirationDate(cards[card][1]);
            } else {
                request.setCardDetectMode(CardDetectMode.SWIPE);
                request.setTrack2EquivalentData(cards[card][2]);
            }
            request.setTransactionAmount(Double.parseDouble(transactionData[i][2]));
            request.setMerchantNameAndLocation("BINDO");


            Response response = vantivGateway.sendRequest(request);
            System.out.println(response.getActionCode());
            System.out.println(response.getTransactionId());

            results[i][0] = response.getActionCode();
            results[i][1] = response.getTransactionId();
            results[i][2] = response.getAuthorizationCode();
            results[i][3] = response.getTransactionDateAndTime();

            try {
                Thread.sleep(20000 + random.nextInt(15000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        for (int i = 0; i < results.length; i++) {
            System.out.println(i + 1 + ": " + results[i][0] + " " + results[i][1] + " " + results[i][2] + " " + results[i][3]);
        }
    }

    String transactionData2[][] = {
            {String.valueOf(CARD_VISA_1), "S", "0.51", "51"},
            {String.valueOf(CARD_VISA_1), "S", "0.01", "01"},
            {String.valueOf(CARD_VISA_1), "S", "0.02", "01"},
            {String.valueOf(CARD_VISA_1), "S", "0.03", "03"},
            {String.valueOf(CARD_VISA_1), "S", "0.04", "04"},
            {String.valueOf(CARD_VISA_1), "S", "2.52", "13"},
            {String.valueOf(CARD_VISA_1), "M", "2.53", "14"},
            {String.valueOf(CARD_VISA_1), "M", "2.54", "96"},
            {String.valueOf(CARD_VISA_1), "S", "1.71", "V1"},
            {String.valueOf(CARD_VISA_1), "S", "1.72", "V2"},
            {String.valueOf(CARD_VISA_1), "S", "1.73", "V3"},
            {String.valueOf(CARD_MASTERCARD_1), "S", "1.74", "V4"},
            {String.valueOf(CARD_MASTERCARD_1), "S", "1.75", "V5"},
            {String.valueOf(CARD_MASTERCARD_1), "S", "1.76", "V6"},
            {String.valueOf(CARD_MASTERCARD_1), "S", "2.55", "96"},
            {String.valueOf(CARD_MASTERCARD_1), "S", "2.59", "94"},
            {String.valueOf(CARD_MASTERCARD_1), "S", "2.61", "03"},
            {String.valueOf(CARD_MASTERCARD_1), "S", "2.67", "54"},
            {String.valueOf(CARD_MASTERCARD_1), "S", "2.69", "43"},
            {String.valueOf(CARD_MASTERCARD_1), "S", "10.50", "43"},
            {String.valueOf(CARD_MASTERCARD_1), "M", "2.57", "05"},
            {String.valueOf(CARD_MASTERCARD_1), "M", "2.60", "12"},
            {String.valueOf(CARD_DISCOVER_1), "S", "2.55", "96"},
            {String.valueOf(CARD_AMEX_1), "S", "2.60", "12"},
            {String.valueOf(CARD_DINERS_CUP), "M", "2.57", "05"},
            {String.valueOf(CARD_DISCOVER_CUP), "S", "2.67", "54"},
            {String.valueOf(CARD_JCB), "S", "2.54", "96"},
    };

    @Test
    public void exceptionRCTest() {
        String[][] results = new String[transactionData2.length][7];
        Random random = new Random();
        for (int i = 0; i < transactionData2.length; i++) {
            Request request = new Request();
            request.setTransactionType(TransactionType.SALE);

            int card = Integer.parseInt(transactionData2[i][0]);
            request.setPrimaryAccountNumber(cards[card][0]);
            if (transactionData2[i][1].equals("M")) {
                request.setCardDetectMode(CardDetectMode.MANUAL);
                request.setCardExpirationDate(cards[card][1]);
            } else {
                request.setCardDetectMode(CardDetectMode.SWIPE);
                request.setTrack2EquivalentData(cards[card][2]);
            }
            request.setTransactionAmount(Double.parseDouble(transactionData2[i][2]));
            request.setMerchantNameAndLocation("BINDO");


            Response response = vantivGateway.sendRequest(request);
            System.out.println(response.getActionCode());
            System.out.println(response.getTransactionId());

            results[i][0] = response.getActionCode();
            results[i][1] = response.getTransactionId();
            results[i][2] = response.getAuthorizationCode();
            results[i][3] = response.getTransactionDateAndTime();
            results[i][4] = transactionData2[i][3].equals(response.getActionCode()) ? "OK" : "NOT OK";
            results[i][5] = String.valueOf(response.getIsApproved());
            results[i][6] = response.getActionCodeDescription();

            try {
                //Thread.sleep(20000 + random.nextInt(15000));
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        for (int i = 0; i < results.length; i++) {
            System.out.println(i + 117 + ": "
                                       + results[i][0] + " "
                                       + results[i][1] + " "
                                       + results[i][3] + " "
                                       + results[i][4] + " "
                                       + results[i][5] + " "
                                       + results[i][6]);
        }
    }

    String transactionDataRefund[][] = {
            {String.valueOf(CARD_VISA_1), "S", "10", "00"},
            {String.valueOf(CARD_VISA_1), "M", "1.61", "00"},
            {String.valueOf(CARD_MASTERCARD_1), "S", "11", "00"},
            {String.valueOf(CARD_MASTERCARD_1), "M", "1.60", "00"},
            {String.valueOf(CARD_MASTERCARD_2A), "S", "15", "00"},
            {String.valueOf(CARD_MASTERCARD_2A), "M", "32.60", "00"},
            {String.valueOf(CARD_DISCOVER_1), "S", "12", "00"},
            {String.valueOf(CARD_DISCOVER_1), "M", "1.62", "00"},
            {String.valueOf(CARD_AMEX_1), "S", "13", "00"},
            {String.valueOf(CARD_AMEX_1), "M", "1.65", "00"},
            {String.valueOf(CARD_DINERS_CUP), "M", "100", "00"},
            {String.valueOf(CARD_DISCOVER_CUP), "S", "69.76", "00"},
            {String.valueOf(CARD_JCB), "S", "77.40", "00"},
    };

    @Test
    public void refundTest() {
        String[][] results = new String[transactionDataRefund.length][7];
        Random random = new Random();
        for (int i = 0; i < transactionDataRefund.length; i++) {
            Request request = new Request();
            request.setTransactionType(TransactionType.REFUND);

            int card = Integer.parseInt(transactionDataRefund[i][0]);
            request.setTransactionSequenceNumber(i + 181);
            request.setPrimaryAccountNumber(cards[card][0]);
            if (transactionDataRefund[i][1].equals("M")) {
                request.setCardDetectMode(CardDetectMode.MANUAL);
                request.setCardExpirationDate(cards[card][1]);
            } else {
                request.setCardDetectMode(CardDetectMode.SWIPE);
                request.setTrack2EquivalentData(cards[card][2]);
            }
            request.setTransactionAmount(Double.parseDouble(transactionDataRefund[i][2]));
            request.setMerchantNameAndLocation("BINDO");


            Response response = vantivGateway.sendRequest(request);
            System.out.println(response.getActionCode());
            System.out.println(response.getTransactionId());

            results[i][0] = response.getActionCode();
            results[i][1] = response.getTransactionId();
            results[i][2] = response.getAuthorizationCode();
            results[i][3] = response.getTransactionDateAndTime();
            results[i][4] = transactionData2[i][3].equals(response.getActionCode()) ? "OK" : "NOT OK";
            results[i][5] = String.valueOf(response.getIsApproved());
            results[i][6] = response.getActionCodeDescription();

            try {
                //Thread.sleep(20000 + random.nextInt(15000));
                Thread.sleep(25000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        for (int i = 0; i < results.length; i++) {
            System.out.println(i + 94 + ": "
                                       + results[i][0] + " "
                                       + results[i][1] + " "
                                       + results[i][2] + " "
                                       + results[i][3]);
        }
    }

    String transactionDataAVS[][] = {
            {String.valueOf(CARD_VISA_1), "M", "35.01", "45201", "A"},
            {String.valueOf(CARD_VISA_1), "M", "36.02", "45202", "G"},
            {String.valueOf(CARD_VISA_1), "M", "33.03", "45203", "N"},
            {String.valueOf(CARD_VISA_1), "M", "37.04", "45204", "R"},
            {String.valueOf(CARD_VISA_1), "M", "38.05", "45205", "S"},
            {String.valueOf(CARD_VISA_1), "M", "40.06", "45206", "U"},
    };

    @Test
    public void avsTest() {
        String[][] results = new String[transactionDataAVS.length][7];
        Random random = new Random();
        for (int i = 0; i < transactionDataAVS.length; i++) {
            Request request = new Request();
            request.setTransactionType(TransactionType.SALE);

            int card = Integer.parseInt(transactionDataAVS[i][0]);
            request.setPrimaryAccountNumber(cards[card][0]);
            if (transactionDataAVS[i][1].equals("M")) {
                request.setCardDetectMode(CardDetectMode.MANUAL);
                request.setCardExpirationDate(cards[card][1]);
                request.setAvsCardholderZipcode(transactionDataAVS[i][3]);
                request.setAvsCardholderAddress("123 Main Street, Cincinnati OH");
                request.setCardSecurityCode("222");
            }
            request.setTransactionAmount(Double.parseDouble(transactionDataAVS[i][2]));
            request.setMerchantNameAndLocation("BINDO");

            Response response = vantivGateway.sendRequest(request);
            System.out.println(response.getActionCode());
            System.out.println(response.getTransactionId());

            results[i][0] = response.getActionCode();
            results[i][1] = response.getTransactionId();
            results[i][2] = response.getAuthorizationCode();
            results[i][3] = response.getTransactionDateAndTime();
            results[i][4] = "00".equals(response.getActionCode()) ? "OK" : "NOT OK";
            results[i][5] = String.valueOf(response.getIsApproved());
            results[i][6] = response.getActionCodeDescription();

            try {
                //Thread.sleep(20000 + random.nextInt(15000));
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        for (int i = 0; i < results.length; i++) {
            System.out.println(i + 94 + ": "
                                       + results[i][0] + " "
                                       + results[i][1] + " "
                                       + results[i][2] + " "
                                       + results[i][3]);
        }
    }

    String transactionDataRev[][] = {
            //{String.valueOf(CARD_VISA_1),       "S", "4.99", "00"}, // timeout
            // reversals
            {String.valueOf(CARD_VISA_1),       "S", "40", "00"},
            /*
            {String.valueOf(CARD_MASTERCARD_1), "S", "40", "00"},
            {String.valueOf(CARD_MASTERCARD_2A),"S", "42", "00"},
            {String.valueOf(CARD_DISCOVER_1),   "S", "40", "00"},
            {String.valueOf(CARD_AMEX_1),       "S", "40", "00"},
            */
    };

    String transactionDataRef[][] = {
            // refunds
            {String.valueOf(CARD_VISA_1),       "S", "10", "00"},
            {String.valueOf(CARD_VISA_1),       "M", "1.61", "00"},
            {String.valueOf(CARD_MASTERCARD_1), "S", "11", "00"},
            {String.valueOf(CARD_MASTERCARD_1), "M", "1.6", "00"},
            {String.valueOf(CARD_MASTERCARD_2A),"S", "15", "00"},
            {String.valueOf(CARD_MASTERCARD_2A),"M", "32.6", "00"},
            {String.valueOf(CARD_DISCOVER_1),   "S", "12", "00"},
            {String.valueOf(CARD_DISCOVER_1),   "M", "1.62", "00"},
            {String.valueOf(CARD_AMEX_1),       "S", "13", "00"},
            {String.valueOf(CARD_AMEX_1),       "M", "1.65", "00"},
            {String.valueOf(CARD_DINERS_CUP),   "M", "100", "00"},
            {String.valueOf(CARD_DISCOVER_CUP), "S", "69.76", "00"},
            {String.valueOf(CARD_JCB),          "S", "77.40", "00"},
    };

    @Test
    public void reversalTest() {
        String[][] results = new String[transactionDataRev.length][7];
        Random random = new Random();
        for (int i = 0; i < transactionDataRev.length; i++) {
            Request request = new Request();
            request.setTransactionSequenceNumber(i + 52);
            request.setTransactionType(TransactionType.SALE);
            request.setCardDetectMode(CardDetectMode.SWIPE);

            int card = Integer.parseInt(transactionDataRev[i][0]);
            request.setPrimaryAccountNumber(cards[card][0]);
            request.setTrack2EquivalentData(cards[card][2]);
            request.setTransactionAmount(Double.parseDouble(transactionDataRev[i][2]));
            request.setMerchantNameAndLocation("BINDO");

            Response response = vantivGateway.sendRequest(request);
            System.out.println(response.getActionCode());
            System.out.println(response.getTransactionId());

            results[i][0] = response.getActionCode();
            results[i][1] = response.getTransactionId();
            results[i][2] = response.getAuthorizationCode();
            results[i][3] = response.getTransactionDateAndTime();

            request.setTransactionType(TransactionType.REVERSAL);
            request.setAcquirerReferenceData(response.getTransactionId());
            request.setTransactionDate(response.getTransactionDate());
            request.setTransactionTime(response.getTransactionTime());
            request.setAuthCode(response.getAuthorizationCode());
            request.setReversalType(ReversalType.MERCHANT_INITIATED);

            request.setOriginalTransactionType(TransactionType.SALE);
            request.setOriginalTransactionSystemTraceAuditNumber(response.getTransactionSystemTraceAuditNumber());
            request.setOriginalTransactionDateAndTime("20" + response.getTransactionDateAndTime());
            request.setOriginalAcquiringIIC(response.getAcquiringIIC());

            response = vantivGateway.sendRequest(request);

            results[i][4] = response.getActionCode();
            results[i][5] = response.getTransactionDateAndTime();

            try {
                //Thread.sleep(20000 + random.nextInt(15000));
                Thread.sleep(20000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        for (int i = 0; i < results.length; i++) {
            System.out.println(i + ": "
                                       + results[i][0] + " "
                                       + results[i][1] + " "
                                       + results[i][2] + " "
                                       + results[i][3] + " "
                                       + results[i][4] + " "
                                       + results[i][5]);
        }
    }

}
