package com.bindo.paymentsdk.v3.pay.gateway.vantiv.beans;

import java.util.Map;
import java.util.TreeMap;

public class AdditionalRequestData {

    Map<String, String> fields = new TreeMap<String, String>();

    public void add(String identifier, String value) {
        fields.put(identifier, value);
    }

    public String get(String identifier) {
        return fields.get(identifier);
    }

    public String toString() {
        StringBuilder data = new StringBuilder();
        for (Map.Entry<String, String> field : fields.entrySet()) {
            data.append(field.getKey() + field.getValue());
        }
        return data.toString();
    }
}
