package com.bindo.paymentsdk.v3.pay.gateway.vantiv.beans;

import com.bindo.paymentsdk.v3.pay.common.util.Hex;
import com.bindo.paymentsdk.v3.pay.common.util.Log;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class VantivISOMessage {

    private Map<Integer, String> fields;

    private String mti;
    private byte[] bitmap;

    private byte[] message = new byte[1024];

    public VantivISOMessage() {
        fields = new HashMap<Integer, String>();
    }

    public void set(int bit, String value) {
        if (value != null) {
            fields.put(bit, value);
        }
    }

    public String get(int bit) {
        return fields.get(bit);
    }

    public boolean has(int bit) {
        return fields.containsKey(bit);
    }

    private int packFieldToMessage(byte[] message, int pos, byte[] field) {
        System.arraycopy(field, 0, message, pos, field.length);
        return pos + field.length;
    }

    private int unpackLLField(int field, int position) {
        int length = Integer.parseInt(Hex.encode(Arrays.copyOfRange(message, position, position += 1)), 16);
        if (length % 2 != 0) {
            length = length + 1;
        }
        length = length / 2;
        fields.put(field, Hex.encode(Arrays.copyOfRange(message, position, position += length)));
        return position;
    }

    private int unpackLLLField(int field, int position) {
        int length = Integer.parseInt(Hex.encode(Arrays.copyOfRange(message, position, position += 2)), 16) / 2;
        fields.put(field, Hex.encode(Arrays.copyOfRange(message, position, position += length)));
        return position;
    }

    private int unpackLLLAsciiField(int field, int position) {
        int length = Integer.parseInt(Hex.encode(Arrays.copyOfRange(message, position, position += 2)), 16);
        fields.put(field, new String(Arrays.copyOfRange(message, position, position += length)));
        return position;
    }

    private int unpackFixedField (int field, int length, int position) {
        fields.put(field, Hex.encode(Arrays.copyOfRange(message, position, position += length)));
        return position;
    }

    private int unpackFixedAsciiField (int field, int length, int position) {
        fields.put(field, new String(Arrays.copyOfRange(message, position, position += length)));
        return position;
    }

    public void unpack(byte[] msg) {
        try {
            System.arraycopy(msg, 0, message, 0, msg.length);
            int pos = 0;
            setMti(Hex.encode(Arrays.copyOfRange(message, pos, pos + 2)));
            pos += 2;
            byte[] bitmap1 = Arrays.copyOfRange(message, pos, pos + 8);
            pos += 8;
            byte[] bitmap2 = null;
            if (testBit(bitmap1[0], 0)) {
                bitmap2 = Arrays.copyOfRange(message, pos, pos + 8);
                pos += 8;
            }
            for (int i = 2; i < 64; i++) {
                int B = (i - 1) / 8;
                int b = i - B * 8 - 1;
                if (testBit(bitmap1[B], b)) {
                    //System.out.println("i: " + i + " B: " + B + " b: " + b);
                    switch (i) {
                        case 2:
                            pos = unpackLLField(i, pos);
                            break;
                        case 3:
                            pos = unpackFixedField(i, 3, pos);
                            break;
                        case 4:
                            pos = unpackFixedField(i, 6, pos);
                            break;
                        case 7:
                            pos = unpackFixedField(i, 5, pos);
                            break;
                        case 11:
                            pos = unpackFixedField(i, 3, pos);
                            break;
                        case 12:
                            pos = unpackFixedField(i, 3, pos);
                            break;
                        case 13:
                            pos = unpackFixedField(i, 2, pos);
                            break;
                        case 14:
                            pos = unpackFixedField(i, 2, pos);
                            break;
                        case 15:
                            pos = unpackFixedField(i, 2, pos);
                            break;
                        case 18:
                            pos = unpackFixedField(i, 2, pos);
                            break;
                        case 19:
                            pos = unpackFixedField(i, 2, pos);
                            break;
                        case 22:
                            pos = unpackFixedField(i, 2, pos);
                            break;
                        case 25:
                            pos = unpackFixedField(i, 1, pos);
                            break;
                        case 32:
                            pos = unpackLLField(i, pos);
                            break;
                        case 35:
                            pos = unpackLLField(i, pos);
                            break;
                        case 37:
                            pos = unpackFixedAsciiField(i, 12, pos);
                            break;
                        case 38:
                            pos = unpackFixedAsciiField(i, 6, pos);
                            break;
                        case 39:
                            pos = unpackFixedAsciiField(i, 2, pos);
                            break;
                        case 41:
                            pos = unpackFixedAsciiField(i, 15, pos);
                            break;
                        case 42:
                            pos = unpackFixedAsciiField(i, 15, pos);
                            break;
                        case 43:
                            pos = unpackFixedAsciiField(i, 40, pos);
                            break;
                        case 44:
                            pos = unpackLLLAsciiField(i, pos);
                            break;
                        case 45:
                            pos = unpackLLLAsciiField(i, pos);
                            break;
                        case 49:
                            pos = unpackFixedField(i, 2, pos);
                            break;
                        case 54:
                            pos = unpackLLLAsciiField(i, pos);
                            break;
                        case 59:
                            pos = unpackLLLField(i, pos);
                            break;
                        case 60:
                            pos = unpackLLLAsciiField(i, pos);
                            break;
                        case 61:
                            pos = unpackLLLAsciiField(i, pos);
                            break;
                        case 62:
                            pos = unpackLLLField(i, pos);
                            break;
                    }
                }
            }
            if (bitmap2 != null) {
                for (int i = 0; i < 64; i++) {
                    int B = (i - 1) / 8;
                    int b = i - B * 8 - 1;
                    if (testBit(bitmap2[B], b)) {
                        //System.out.println("i: " + i + " B: " + B + " b: " + b);
                        int length = 0;
                        int j      = i + 64;
                        switch (j) {
                            case 112:
                                pos = unpackLLLField(j, pos);
                                break;
                            case 120:
                                pos = unpackLLLField(j, pos);
                                break;
                            case 123:
                                pos = unpackFixedAsciiField(j, 15, pos);
                                break;
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.d(toString());
            throw (e);
        }
    }

    private int packFixedField(int field, int position) {
        if (fields.containsKey(field) && fields.get(field) != null) {
            position = packFieldToMessage(message, position, hexStringToByteArray(get(field)));
        }
        return position;
    }

    private int packAsciiField(int field, int position) {
        if (fields.containsKey(field) && fields.get(field) != null) {
            position = packFieldToMessage(message, position, get(field).getBytes());
        }
        return position;
    }

    private int packLLField(int field, int position) {
        if (fields.containsKey(field) && fields.get(field) != null) {
            position = packFieldToMessage(message, position, hexStringToByteArray(String.format("%02X", get(field).length())));
            position = packFieldToMessage(message, position, hexStringToByteArray(get(field)));
        }
        return position;
    }

    private int packLLLField(int field, int position) {
        if (fields.containsKey(field) && fields.get(field) != null) {
            position = packFieldToMessage(message, position, hexStringToByteArray(String.format("%04X", get(field).length())));
            position = packFieldToMessage(message, position, get(field).getBytes());
        }
        return position;
    }

    private int packLLLBinaryField(int field, int position) {
        if (fields.containsKey(field) && fields.get(field) != null) {
            position = packFieldToMessage(message, position, hexStringToByteArray(String.format("%04X", get(field).length() / 2)));
            position = packFieldToMessage(message, position, hexStringToByteArray(get(field)));
        }
        return position;
    }

    // TODO: extract packager from message
    public byte[] pack() {
        for (int i = 0; i < message.length; i++) {
            message[i] = (byte) 0x00;
        }
        int pos = 0;

        pos = packFieldToMessage(message, pos, hexStringToByteArray(mti));
        calculateBitmap();
        pos = packFieldToMessage(message, pos, bitmap);
        pos = packLLField(2, pos);
        pos = packFixedField(3, pos);
        pos = packFixedField(4, pos);
        pos = packFixedField(7, pos);
        pos = packFixedField(11, pos);
        pos = packFixedField(12, pos);
        pos = packFixedField(13, pos);
        pos = packFixedField(14, pos);
        pos = packFixedField(15, pos);
        pos = packFixedField(18, pos);
        pos = packFixedField(19, pos);
        pos = packFixedField(22, pos);
        pos = packFixedField(25, pos);
        pos = packLLField(32, pos);
        pos = packLLField(35, pos);
        pos = packAsciiField(37, pos);
        pos = packAsciiField(38, pos);
        pos = packAsciiField(41, pos);
        pos = packAsciiField(42, pos);
        pos = packAsciiField(43, pos);
        pos = packLLLField(45, pos);
        pos = packFixedField(49, pos);
        pos = packLLLField(60, pos);
        pos = packLLLBinaryField(62, pos);
        pos = packAsciiField(63, pos);
        pos = packFixedField(70, pos);
        pos = packFixedField(90, pos);
        pos = packLLLField(120, pos);
        pos = packAsciiField(123, pos);

        return Arrays.copyOf(message, pos);
    }

    public static byte[] hexStringToByteArray(String s) {
        if (s.length() % 2 != 0) {
            s = "0" + s;
        }
        int    len  = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    public String getMti() {
        return mti;
    }

    public void setMti(String mti) {
        this.mti = mti;
    }

    private void calculateBitmap() {
        bitmap = getBitmap();
    }

    public byte[] getBitmap() {
        byte[] bitmap1 = new byte[]{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
        byte[] bitmap2 = new byte[]{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
        for (int i = 2; i < 64; i++) {
            if (fields.containsKey(i)) {
                int B = (i - 1) / 8;
                int b = i - B * 8 - 1;
                bitmap1[B] = setBit(bitmap1[B], b);
            }
        }
        boolean hasBitmap2 = false;
        for (int i = 65; i < 128; i++) {
            if (fields.containsKey(i)) {
                hasBitmap2 = true;
                int B = (i - 1) / 8 - 8;
                int b = i - B * 8 - 1;
                bitmap2[B] = setBit(bitmap2[B], b);
            }
        }
        if (hasBitmap2) {
            bitmap1[0] = setBit(bitmap1[0], 0);
        }
        if (hasBitmap2) {
            byte[] bitmap = new byte[16];
            System.arraycopy(bitmap1, 0, bitmap, 0, 8);
            System.arraycopy(bitmap2, 0, bitmap, 8, 8);
            return bitmap;
        } else {
            return bitmap1;
        }
    }

    private static byte setBit(byte B, int b) {
        return (byte) (B | (0x80 >> b));
    }

    public boolean testBit(byte B, int b) {
        return ((B << b) & 0x80) == 0x80;
    }

    public void parse(byte[] message) {
        unpack(message);
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\n[MTI] [" + getMti() + "]\n");
        for (int i = 0; i < 128; i++) {
            if (fields.containsKey(i)) {
                stringBuilder.append("[" + String.format("%03d", i) + "] [" + String.format("%04X", fields.get(i).length()) + "] [" + fields.get(i) + "]\n");
            }
        }
        return stringBuilder.toString();
    }
}
