package com.bindo.paymentsdk.v3.pay.gateway.vantiv.enums;

public enum NetworkManagementInformationCode {

    SIGNON("001"),
    SIGNOFF("002"),
    NEW_WORKING_KEY("101"),
    REQUEST_NEW_WORKING_KEY("180"),
    ECHO_TEST("301"),
    ERRORED_TRANSACTION("900");

    String value;

    NetworkManagementInformationCode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
