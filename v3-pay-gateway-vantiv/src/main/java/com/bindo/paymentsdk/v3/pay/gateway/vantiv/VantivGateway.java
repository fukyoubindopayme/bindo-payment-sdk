package com.bindo.paymentsdk.v3.pay.gateway.vantiv;

import com.bindo.paymentsdk.v3.pay.common.CardDetectMode;
import com.bindo.paymentsdk.v3.pay.common.emv.enums.EMVTag;
import com.bindo.paymentsdk.v3.pay.common.gateway.SocketService;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Request;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Response;
import com.bindo.paymentsdk.v3.pay.common.util.Hex;
import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.gateway.Gateway;
import com.bindo.paymentsdk.v3.pay.gateway.vantiv.beans.AdditionalRequestData;
import com.bindo.paymentsdk.v3.pay.gateway.vantiv.beans.VantivISOMessage;
import com.bindo.paymentsdk.v3.pay.gateway.vantiv.enums.NetworkManagementInformationCode;
import com.bindo.paymentsdk.v3.pay.gateway.vantiv.enums.ResponseCode;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

public class VantivGateway implements Gateway {

    private final static String TAG = "VantivGateway";

    // CONFIGURATION KEYS
    private final static String CONFIG_HOST_IP_ADDRESS   = "vantiv_host_ip";
    private final static String CONFIG_HOST_TCP_PORT     = "vantiv_host_port";
    private final static String CONFIG_HOST_TCP_HEADER   = "vantiv_host_header_len";
    private final static String CONFIG_HOST_TIMEOUT      = "vantiv_host_timeout";
    private final static String CONFIG_MERCHANT_MCC      = "vantiv_merchant_mcc";
    private final static String CONFIG_MERCHANT_CURRENCY = "vantiv_merchant_currency";
    private final static String CONFIG_MERCHANT_CHAIN    = "vantiv_merchant_chain";
    private final static String CONFIG_MERCHANT_STORE    = "vantiv_merchant_store";
    private final static String CONFIG_MERCHANT_LANE     = "vantiv_merchant_lane";
    private final static String CONFIG_MERCHANT_MID      = "vantiv_merchant_mid";
    private final static String CONFIG_MERCHANT_ADDRESS  = "vantiv_merchant_address";
    private final static String CONFIG_MERCHANT_CITY     = "vantiv_merchant_city";
    private final static String CONFIG_MERCHANT_STATE    = "vantiv_merchant_state";
    private final static String CONFIG_MERCHANT_COUNTRY  = "vantiv_merchant_country";
    private final static String CONFIG_ACQUIRER_COUNTRY  = "vantiv_acquirer_country";
    private final static String CONFIG_ACQUIRER_ID       = "vantiv_acquirer_id";

    // DEFAULT CONFIGURATION
    private final static String DEFAULT_HOST_IP_ADDRESS   = "47.90.250.144";
    private final static int    DEFAULT_HOST_TCP_PORT     = 8081;
    private final static int    DEFAULT_HOST_TCP_HEADER   = 2;
    private final static int    DEFAULT_HOST_TIMEOUT      = 30;
    private final static String DEFAULT_MERCHANT_MCC      = "5411";
    private final static String DEFAULT_MERCHANT_CURRENCY = "840";
    private final static String DEFAULT_MERCHANT_CHAIN    = "I4396";
    private final static String DEFAULT_MERCHANT_STORE    = "0002";
    private final static String DEFAULT_MERCHANT_LANE     = "01";
    private final static String DEFAULT_MERCHANT_MID      = "012739409";
    private final static String DEFAULT_MERCHANT_ADDRESS  = "1 BINDO ROAD";
    private final static String DEFAULT_MERCHANT_CITY     = "SAN FRANCISCO";
    private final static String DEFAULT_MERCHANT_STATE    = "CA";
    private final static String DEFAULT_MERCHANT_COUNTRY  = "US";
    private final static String DEFAULT_ACQUIRER_COUNTRY  = "840";
    private final static String DEFAULT_ACQUIRER_ID       = "1042000314";

    private static final SimpleDateFormat DF_DATE_TIME = new SimpleDateFormat("MMddHHmmss");
    private static final SimpleDateFormat DF_TIME      = new SimpleDateFormat("HHmmss");
    private static final SimpleDateFormat DF_DATE      = new SimpleDateFormat("MMdd");

    // TODO: connection timeout!
    private SocketService mSocketService = new SocketService();

    private Map<String, Object> mGatewayConfig = new HashMap<String, Object>();

    public Map<String, Object> getGatewayConfig() {
        return mGatewayConfig;
    }

    private final static TimeZone tzGMT = TimeZone.getTimeZone("GMT");

    public void setGatewayConfig(HashMap<String, Object> defaultGatewayConfig) {
        this.mGatewayConfig = defaultGatewayConfig;
    }

    public VantivGateway(HashMap<String, Object> configMap) {
        mGatewayConfig.put(CONFIG_HOST_IP_ADDRESS, DEFAULT_HOST_IP_ADDRESS);
        mGatewayConfig.put(CONFIG_HOST_TCP_PORT, DEFAULT_HOST_TCP_PORT);
        mGatewayConfig.put(CONFIG_HOST_TCP_HEADER, DEFAULT_HOST_TCP_HEADER);
        mGatewayConfig.put(CONFIG_HOST_TIMEOUT, DEFAULT_HOST_TIMEOUT);
        mGatewayConfig.put(CONFIG_MERCHANT_MCC, DEFAULT_MERCHANT_MCC);
        mGatewayConfig.put(CONFIG_MERCHANT_CURRENCY, DEFAULT_MERCHANT_CURRENCY);
        mGatewayConfig.put(CONFIG_MERCHANT_CHAIN, DEFAULT_MERCHANT_CHAIN);
        mGatewayConfig.put(CONFIG_MERCHANT_STORE, DEFAULT_MERCHANT_STORE);
        mGatewayConfig.put(CONFIG_MERCHANT_LANE, DEFAULT_MERCHANT_LANE);
        mGatewayConfig.put(CONFIG_MERCHANT_MID, DEFAULT_MERCHANT_MID);
        mGatewayConfig.put(CONFIG_MERCHANT_ADDRESS, DEFAULT_MERCHANT_ADDRESS);
        mGatewayConfig.put(CONFIG_MERCHANT_CITY, DEFAULT_MERCHANT_CITY);
        mGatewayConfig.put(CONFIG_MERCHANT_COUNTRY, DEFAULT_MERCHANT_COUNTRY);
        mGatewayConfig.put(CONFIG_MERCHANT_STATE, DEFAULT_MERCHANT_STATE);
        mGatewayConfig.put(CONFIG_ACQUIRER_COUNTRY, DEFAULT_ACQUIRER_COUNTRY);
        mGatewayConfig.put(CONFIG_ACQUIRER_ID, DEFAULT_ACQUIRER_ID);

        if (configMap == null) {
            Log.w(TAG, "Please set the gateway parameters");
        } else {
            for (String key : configMap.keySet()) {
                Object value = configMap.get(key);
                mGatewayConfig.put(key, value);
            }
        }

        configureGateway();
    }

    private void configureGateway() {
        mSocketService.setHostIp((String) mGatewayConfig.get(CONFIG_HOST_IP_ADDRESS));
        mSocketService.setHostPort(String.valueOf(mGatewayConfig.get(CONFIG_HOST_TCP_PORT)));
        mSocketService.setTimeout((Integer) mGatewayConfig.get(CONFIG_HOST_TIMEOUT));
        DF_DATE_TIME.setTimeZone(tzGMT);
    }

    // TODO: set enums
    private VantivISOMessage createISORequest(Request request) {
        VantivISOMessage isoRequest = new VantivISOMessage();
        boolean isReversal = false;
        switch (request.getTransactionType()) {
            case AUTHORIZATION:
            case SALE:
                isoRequest.setMti("0200");
                isoRequest.set(3, "003000");
                break;
            case REFUND:
                isoRequest.setMti("0200");
                isoRequest.set(3, "200030");
                break;
            case BALANCE_INQUIRY:
                isoRequest.setMti("0200");
                isoRequest.set(3, "313000");
                break;
            case REVERSAL:
                isoRequest.setMti("0420");
                isoRequest.set(3, "003000");
                isReversal = true;
                break;
            default:
                throw new RuntimeException("UNSUPPORTED TRANSACTION TYPE");
        }

        isoRequest.set(2, request.getPrimaryAccountNumber());
        isoRequest.set(4, String.format("%012d", Math.round(request.getTransactionAmount() * 100)));

        Date now = new Date();

        if (request.getTransactionDateAndTime() != null) {
            isoRequest.set(7, request.getTransactionDateAndTime().substring(2));
        } else {
            isoRequest.set(7, DF_DATE_TIME.format(now));
        }
        if (request.getTransactionSystemTraceAuditNumber() != 0) {
            isoRequest.set(11, String.format("%06d", request.getTransactionSystemTraceAuditNumber()));
        } else {
            isoRequest.set(11, DF_TIME.format(now));
        }

        isoRequest.set(12, DF_TIME.format(now));
        isoRequest.set(13, DF_DATE.format(now));
        isoRequest.set(14, request.getCardExpirationDate());

        isoRequest.set(18, (String) mGatewayConfig.get(CONFIG_MERCHANT_MCC));
        isoRequest.set(19, (String) mGatewayConfig.get(CONFIG_ACQUIRER_COUNTRY));

        String panEntryMode;
        switch (request.getTransactionType()) {
            case REVERSAL:
                panEntryMode = "02";
                break;
            default:
                switch (request.getCardDetectMode()) {
                    case CardDetectMode.MANUAL:
                        panEntryMode = "01";
                        break;
                    case CardDetectMode.CONTACT:
                        panEntryMode = "05";
                        break;
                    case CardDetectMode.SWIPE:
                    case CardDetectMode.FALLBACK_SWIPE:
                        panEntryMode = "90";
                        break;
                    case CardDetectMode.CONTACTLESS:
                        panEntryMode = "91";
                        break;
                    default:
                        panEntryMode = "00";
                }
        }
        String pinEntryMode = "2";
        isoRequest.set(22, panEntryMode + pinEntryMode + "0");

        isoRequest.set(25, "00");
        isoRequest.set(32, (String) mGatewayConfig.get(CONFIG_ACQUIRER_ID));

        String track2 = null;
        switch (request.getCardDetectMode()) {
            case CardDetectMode.SWIPE:
            case CardDetectMode.FALLBACK_SWIPE:
                if (request.getTrack2EquivalentData() != null) {
                    track2 = request.getTrack2EquivalentData().replace('=', 'D');
                }
                break;
            case CardDetectMode.CONTACTLESS:
                // TODO: move this code to EMV core
                // TODO: ms contactless for other brands
                track2 = request.getTrack2EquivalentData();
                String applicationCryptogram = request.getTLVHexString(EMVTag.APPLICATION_CRYPTOGRAM);
                applicationCryptogram = String.format("%05d", Integer.parseInt(applicationCryptogram.substring(applicationCryptogram.length() - 6), 16));
                applicationCryptogram = applicationCryptogram.substring(applicationCryptogram.length() - 5);

                String applicationTransactionCounter = request.getTLVHexString(EMVTag.APPLICATION_TRANSACTION_COUNTER);
                applicationTransactionCounter = String.format("%05d", Integer.parseInt(applicationTransactionCounter, 16));
                applicationTransactionCounter = applicationTransactionCounter.substring(applicationTransactionCounter.length() - 5);

                String unpredictableNumber = request.getTLVHexString(EMVTag.UNPREDICTABLE_NUMBER);
                unpredictableNumber = unpredictableNumber.substring(unpredictableNumber.length() - 4);

                int x = track2.indexOf('D');
                if (x == -1) {
                    x = track2.indexOf('=');
                }
                track2 = track2.substring(0, (x + 8)) + unpredictableNumber + applicationCryptogram + applicationTransactionCounter;
                break;
        }
        if (track2 != null) {
            switch (request.getTransactionType()) {
                case REVERSAL:
                    break;
                default:
                    isoRequest.set(35, track2);
            }
            String[] track2Halves = track2.split("D");
            if (track2Halves.length > 0 && track2Halves[1].length() >= 4) {
                isoRequest.set(14, track2Halves[1].substring(0, 4));
            }
        }

        String rrn = null;
        if (request.getAcquirerReferenceData() != null) {
            rrn = request.getAcquirerReferenceData();
        } else {
            rrn = (new SimpleDateFormat("yyyyDHHmmssSSS")).format(now).substring(3, 3 + 12);
        }
        if (rrn != null) {
            isoRequest.set(37, rrn);
        }

        if (request.getAuthCode() != null && isReversal) {
            isoRequest.set(38, request.getAuthCode());
        }

        isoRequest.set(41, String.format("%5s%4s%2s    ", mGatewayConfig.get(CONFIG_MERCHANT_CHAIN), mGatewayConfig.get(CONFIG_MERCHANT_STORE), mGatewayConfig.get(CONFIG_MERCHANT_LANE)));
        isoRequest.set(42, String.format("%-15s", mGatewayConfig.get(CONFIG_MERCHANT_MID)));

        isoRequest.set(43, String.format("%-23s%-13s%-2s%-2s", mGatewayConfig.get(CONFIG_MERCHANT_ADDRESS), mGatewayConfig.get(CONFIG_MERCHANT_CITY), mGatewayConfig.get(CONFIG_MERCHANT_STATE), mGatewayConfig.get(CONFIG_MERCHANT_COUNTRY)));
        if (!isoRequest.has(35)) {
            if (request.getTrack1DiscretionaryData() != null) {
                isoRequest.set(45, request.getTrack1DiscretionaryData());
                if (!isoRequest.has(14)) {
                    String[] track1Data = request.getTrack1DiscretionaryData().split("^");
                    if (track1Data.length > 2 && track1Data[1].length() >= 4) {
                        isoRequest.set(14, track1Data[1].substring(0, 4));
                    }
                }
            }
        }
        isoRequest.set(49, (String) mGatewayConfig.get(CONFIG_MERCHANT_CURRENCY));
        //isoRequest.set(60, "M" + "1" + "2" + " " + "0" + "0" + "0" + "000000" + "000" + "B0000000" + "000" + "000000000");
        // TODO: F53, CVV for AMEX
        switch (request.getCardDetectMode()) {
            case CardDetectMode.CONTACTLESS:
                isoRequest.set(60, "404 0P0");
                break;
            default:
                isoRequest.set(60, "402 0P0");
        }

        byte[] field62Bitmap = new byte[] {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
        String field62Value = "";
        int TSN;
        if (request.getTransactionSequenceNumber() != 0) {
            TSN = request.getTransactionSequenceNumber();
        } else {
            TSN = request.getTransactionSystemTraceAuditNumber();
        }
        field62Value += String.format("%06d", TSN);
        setBitmap(field62Bitmap, 2);

        if (request.getDraftLocatorId() != null) {
            setBitmap(field62Bitmap, 46);
            field62Value += String.format("%-11s", Hex.encode(request.getDraftLocatorId().getBytes()));
        }
        isoRequest.set(62, Hex.encode(field62Bitmap) + field62Value);

        if (isReversal && request.getReversalType() != null) {
            switch (request.getReversalType()) {
                case MERCHANT_INITIATED:
                    isoRequest.set(63, "02");
                    break;
                case SYSTEM_GENERATED:
                default:
                    isoRequest.set(63, "02");
            }
        }
        if (isReversal) {
            // mti 4 + stan 6 + dt 10 + acq 11 + fw 11 = 42
            String originalMti;
            String originalStan;
            String originalDateTime;
            String originalAII;
            String originalFII = "00000000000";
            if (request.getOriginalTransactionType() != null) {
                switch (request.getOriginalTransactionType()) {
                    case SALE:
                    default:
                        originalMti = "0200";
                }
            } else {
                originalMti = "0200";
            }
            originalStan = String.format("%06d", request.getOriginalTransactionSystemTraceAuditNumber());
            originalDateTime = request.getOriginalTransactionDateAndTime().substring(2);
            originalAII = "00000000000" + (String) mGatewayConfig.get(CONFIG_ACQUIRER_ID);
            originalAII = originalAII.substring(originalAII.length() - 11);
            isoRequest.set(90, originalMti + originalStan + originalDateTime + originalAII + originalFII);
            System.out.println(originalMti + " " + originalStan + " " + originalDateTime + " " + originalAII + " " + originalFII);
            System.out.println("90: '" + isoRequest.get(90) + "'");
        }
        AdditionalRequestData additionalRequestData = new AdditionalRequestData();
        additionalRequestData.add("AD", "Y"); // discover data request
        additionalRequestData.add("AX", "Y"); // am.ex. trx. qualification data
        additionalRequestData.add("CL", "1"); // card level product results code
        additionalRequestData.add("DS", "Y"); // discover network ref.id request
        additionalRequestData.add("RB", "Y"); // remaining balance request
        if (request.getCardDetectMode().equals(CardDetectMode.MANUAL)) {
            if (request.getAvsCardholderAddress() != null || request.getAvsCardholderZipcode() != null) {
                String zip = request.getAvsCardholderZipcode();
                String adr = request.getAvsCardholderAddress();
                String avsData = "";
                if (zip != null && zip.length() > 9) {
                    zip = zip.substring(0, 9);
                }
                if (adr != null && adr.length() > 20) {
                    adr = adr.substring(0, 20);
                }
                avsData += String.format("%-9s", zip).toUpperCase();
                avsData += String.format("%-20s", adr);
                additionalRequestData.add("AV", avsData);
            }
        }
        if (request.getCardSecurityCode() != null) {
            additionalRequestData.add("C2", String.format("%4s1   ", request.getCardSecurityCode()));
        }
        isoRequest.set(120, additionalRequestData.toString());

        //isoRequest.set(123, "BINDOBINDOBINDO");

        return isoRequest;
    }

    private static void setBitmap(byte[] bitmap, int bit) {
        int byt = (bit - 1) / 8;
        byte mask = (byte) (0x80 >>> ((bit - 1) % 8));
        bitmap[byt] = (byte) (bitmap[byt] | mask);
    }

    private Response createResponse(Request request) {
        Response response = new Response();
        response.setTransactionType(request.getTransactionType().name());
        response.setTransactionProcessingCode(request.getProcessingCode());
        response.setTransactionCurrencyCode(request.getTransactionCurrencyCode());
        response.setTransactionSystemTraceAuditNumber(request.getTransactionSystemTraceAuditNumber());
        response.setTransactionAmount(request.getTransactionAmount());
        response.setTransactionDate(request.getTransactionDate());
        response.setTransactionTime(request.getTransactionTime());
        response.setTransactionDateAndTime(request.getTransactionDateAndTime());
        return response;
    }

    private Response mergeResponse(Response response, VantivISOMessage isoResponse) {
        response.setActionCode(isoResponse.get(39));
        ResponseCode responseCode = ResponseCode.findByCode(response.getActionCode());
        response.setIsNeedCallIssuerConfirm(responseCode.isCall());
        response.setIsApproved(responseCode.isApproved());
        response.setPickUpCard(responseCode.isPickup());
        response.setActionCodeDescription(responseCode.getDescription());
        response.setAuthorizationCode(isoResponse.get(38));
        response.setTransactionId(isoResponse.get(37));
        response.setRetrievalReferenceNumber(isoResponse.get(37));
        response.setTransactionDateAndTime(isoResponse.get(7));
        response.setAcquiringIIC(isoResponse.get(32));
        if (isoResponse.get(44) != null) {
            response.setAdditionalResponseData(isoResponse.get(44));
            Map<Integer, String> additionalResponseDataMap = new HashMap<Integer, String>();
            for (int i=0; i<isoResponse.get(44).length(); i++) {
                additionalResponseDataMap.put(i + 1, isoResponse.get(44).substring(i, i + 1));
            }
            response.setAdditionalResponseDataMap(additionalResponseDataMap);
        }
        if (isoResponse.get(61) != null) {
            response.setNetworkSpecificInformation(isoResponse.get(61));
            Map<Integer, String> nsiMap = new HashMap<Integer, String>();
            String nsi = isoResponse.get(61);
            String fieldUse = nsi.substring(0, 2);
            nsiMap.put(0, fieldUse);
            int i = 2;
            try {
                switch (fieldUse) {
                    case "01":
                        nsiMap.put(1, nsi.substring(i, i += 15));
                        nsiMap.put(2, nsi.substring(i, i += 4));
                        nsiMap.put(3, nsi.substring(i, i += 1));
                        break;
                    case "02":
                        nsiMap.put(1, nsi.substring(i, i += 4));
                        nsiMap.put(2, nsi.substring(i, i += 9));
                        break;
                    case "03":
                        nsiMap.put(1, nsi.substring(i, i += 2));
                        nsiMap.put(2, nsi.substring(i, i += 1));
                        nsiMap.put(3, nsi.substring(i, i += 1));
                        break;
                    case "08":
                        nsiMap.put(1, nsi.substring(i, i += 2));
                        break;
                    case "09":
                        nsiMap.put(1, nsi.substring(i, i += 15));
                        nsiMap.put(2, nsi.substring(i, i += 12));
                        break;
                    case "10":
                        nsiMap.put(1, nsi.substring(i, i += 6));
                        nsiMap.put(2, nsi.substring(i, i += 6));
                        nsiMap.put(3, nsi.substring(i, i += 2));
                        nsiMap.put(4, nsi.substring(i, i += 1));
                        nsiMap.put(5, nsi.substring(i, i += 2));
                        nsiMap.put(6, nsi.substring(i, i += 13));
                        nsiMap.put(7, nsi.substring(i, i += 2));
                        break;
                    case "12":
                        nsiMap.put(1, nsi.substring(i, i += 3));
                        nsiMap.put(2, nsi.substring(i, i += Integer.parseInt(nsiMap.get(1))));
                        break;
                    case "13":
                        nsiMap.put(1, nsi.substring(i, i += 15));
                        break;
                    case "14":
                        nsiMap.put(1, nsi.substring(i, i += 4));
                        break;
                    case "15":
                        nsiMap.put(1, nsi.substring(i, i += 15));
                        break;
                    case "16":
                        nsiMap.put(1, nsi.substring(i, i += 4));
                        nsiMap.put(2, nsi.substring(i, i += 9));
                        nsiMap.put(3, nsi.substring(i, i += 1));
                        break;
                    case "17":
                        nsiMap.put(1, nsi.substring(i, i += 1));
                        break;
                    case "18":
                        nsiMap.put(1, nsi.substring(i, i += 1));
                        nsiMap.put(2, nsi.substring(i, i += 20));
                        nsiMap.put(3, nsi.substring(i, i += 4));
                        nsiMap.put(4, nsi.substring(i, i += 3));
                        break;
                    case "19":
                        nsiMap.put(1, nsi.substring(i, i += 1));
                        break;
                    case "21":
                        nsiMap.put(1, nsi.substring(i, i += 2));
                        nsiMap.put(2, nsi.substring(i, i += 1));
                        break;
                    case "22":
                        nsiMap.put(1, nsi.substring(i, i += 2));
                        nsiMap.put(2, nsi.substring(i, i += Integer.parseInt(nsiMap.get(1))));
                        break;
                    case "23":
                        nsiMap.put(1, nsi.substring(i, i += 11));
                        nsiMap.put(2, nsi.substring(i, i += 2));
                        nsiMap.put(3, nsi.substring(i, i += 4));
                        nsiMap.put(4, nsi.substring(i, i += 4));
                        break;
                }
            } catch (Exception e) {
                Log.e(e.getMessage());
            }
            response.setNetworkSpecificInformationMap(nsiMap);
        }
        if (isoResponse.has(11)) {
            response.setTransactionSystemTraceAuditNumber(Integer.parseInt(isoResponse.get(11)));
        }
        return response;
    }

    @Override
    public Response sendRequest(Request request) {
        VantivISOMessage isoRequest = createISORequest(request);
        List<Response.Error> errors = new ArrayList<>();

        Response response = createResponse(request);
        try {
            VantivISOMessage isoResponse = sendISOMessage(isoRequest);
            response = mergeResponse(response, isoResponse);
        } catch (Exception e) {
            response = new Response();

            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            Log.e(TAG, sw.toString());

            String errorCode;
            String errorMessage = sw.toString();

            Throwable cause = e.getCause();

            if (cause instanceof UnknownHostException) {
                errorCode = Response.Error.ERROR_CODE_UNKNOWN_HOST;
            } else if (cause instanceof SocketTimeoutException) {
                errorCode = Response.Error.ERROR_CODE_TIMEOUT;
            } else {
                errorCode = Response.Error.ERROR_CODE_UNKNOWN;
            }

            errors.add(new Response.Error(errorCode, errorMessage));
            response.setErrors(errors);
        }
        return response;
    }

    private VantivISOMessage sendISOMessage(VantivISOMessage request) throws Exception {
        Log.d(TAG, "Connecting...");
        mSocketService.connect();
        if (mSocketService.isConnected()) {
            VantivISOMessage response = new VantivISOMessage();
            try {
                byte[] isoRequest = request.pack();
                Log.d(TAG, request.toString());
                Log.d(TAG, "SEND: " + Hex.encode(isoRequest));

                int headerLen = (Integer) mGatewayConfig.get(CONFIG_HOST_TCP_HEADER);
                byte[] dataPacket = new byte[isoRequest.length + headerLen];
                System.arraycopy(isoRequest, 0, dataPacket, headerLen, isoRequest.length);
                byte[] length = Hex.decode(String.format("%0" + headerLen * 2 + "X", isoRequest.length));
                System.arraycopy(length, 0, dataPacket, 0, length.length);

                mSocketService.sendMessage(dataPacket);

                byte[] receivedDataBytes = mSocketService.receiveMessageWithTimeout();
                Log.d(TAG, "RECV: " + Hex.encode(receivedDataBytes));
                if (receivedDataBytes == null || receivedDataBytes.length == 0) {
                    throw new Exception("No reply from host");
                }
                response.parse(receivedDataBytes);
                Log.d(TAG, response.toString());
            } catch (SocketTimeoutException e) {
                throw new Exception(e);
            } finally {
                mSocketService.disconnect();
            }
            return response;
        } else {
            throw new Exception("Not connected");
        }
    }

    @Deprecated
    public void sendNetworkManagementRequest(NetworkManagementInformationCode networkManagementInformationCode) {
        VantivISOMessage vantivISOMessage = new VantivISOMessage();
        vantivISOMessage.setMti("0800");
        vantivISOMessage.set(7, (new SimpleDateFormat("MMddHHmmss")).format(new Date()));
        vantivISOMessage.set(11, "123456");
        vantivISOMessage.set(70, networkManagementInformationCode.getValue());
        try {
            sendISOMessage(vantivISOMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
