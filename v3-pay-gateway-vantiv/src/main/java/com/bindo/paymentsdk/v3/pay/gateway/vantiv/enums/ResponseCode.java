package com.bindo.paymentsdk.v3.pay.gateway.vantiv.enums;

import java.util.HashMap;
import java.util.Map;

public enum ResponseCode {

    APPROVED("00", "Approved", true, false, false),
    REFER("01", "Refer to Card Issuer", false, false, true),
    REFER_SPECIAL("02", "Refer to Card Issuer, Special Conditions", false, false, true),
    DECLINE_INVALID_MID("03", "Decline Invalid Merchant ID", false, false, false),
    DECLINE_PICK_UP("04", "Decline Pick Up Card", false, true, false),
    DECLINE_GENERIC("05", "Decline Generic Authorization Decline", false, false, false),
    DECLINE_ERROR("06", "Decline Error", false, false, false),
    DECLINE_PICK_UP_SPECIAL("07", "Decline Pick Up Card, Special Conditions", false, true, false),
    APPROVE_WITH_ID("08", "Approve Honor With Identification", true, false, false),
    APPROVE_PARTIAL("10", "Approve Approved For Partial Amount", true, false, false),
    APPROVE_VIP("11", "Approve VIP Approval", true, false, false),
    DECLINE_INVALID_TRANSACTION("12", "Decline Invalid Transaction", false, false, false),
    DECLINE_INVALID_AMOUNT("13", "Decline Invalid Amount", false, false, false),
    DECLINE_INVALID_ACCOUNT("14", "Decline Invalid Account Number", false, false, false),
    DECLINE_NO_ISSUER("15", "Decline No Such Issuer", false, false, false),
    DECLINE_CUSTOMER_CANCEL("17", "Decline Customer Cancellation", false, false, false),
    DECLINE_RETRY("19", "Decline Re-try Transaction", false, false, false),
    DECLINE_REVERSAL_UNSUCESSFUL("21", "Decline Reversal Unsuccessful", false, false, false),
    DECLINE_NO_RECORD("25", "Decline Unable to locate record on file", false, false, false),
    DECLINE_FILE_UPDATE_FIELD_ERROR("27", "Decline File update field edit error", false, false, false),
    DECLINE_FILE_UNAVAILABLE("28", "Decline Update file temporarily unavailable", false, false, false),
    DECLINE_FORMAT_ERROR("30", "Decline Message Format Error", false, false, false),
    DECLINE_PARTIAL_REVERSAL("32", "Decline Partial Reversal", false, false, false),
    DECLINE_EXPIRED("33", "Decline Pick Up Card - Expired", false, true, false),
    DECLINE_PIN_TRIES_EXCEEDED("38", "Decline Allowable Number of PIN Tries Exceeded", false, true, false),
    DECLINE_NO_CREDIT_ACCOUNT("39", "Decline No Credit Account", false, false, false),
    DECLINE_UNSUPPORTED_FUNCTION("40", "Decline Requested Function Not Supported", false, false, false),
    DECLINE_LOST("41", "Decline Pick Up Card - Lost", false, true, false),
    DECLINE_STOLEN("43", "Decline Pick Up Card - Stolen", false, true, false),
    DECLINE_INSUFFICIENT_FUNDS("51", "Decline Insufficient Funds", false, false, false),
    DECLINE_NO_CHECKING_ACCOUNT("52", "Decline No Checking Account", false, false, false),
    DECLINE_NO_SAVINGS_ACCOUNT("53", "Decline No Savings Account", false, false, false),
    DECLINE_EXPIRED_CARD("54", "Decline Expired Card", false, false, false),
    DECLINE_INCORRECT_PIN("55", "Decline Incorrect PIN", false, false, false),
    DECLINE_CANT_PROCESS("56", "Decline Cannot Process", false, false, false),
    DECLINE_TRANSACTION_NOT_PERMITTED_TO_CARDHOLDER("57", "Decline Transaction not Permitted to Cardholder", false, false, false),
    DECLINE_TRANSACTION_NOT_PERMITTED_TO_ACQUIRER("58", "Decline Transaction not Permitted to Acquirer", false, false, false),
    DECLINE_EXCEEDS_WITHDRAWAL_LIMIT("61", "Decline Exceeds Withdrawal Limit", false, false, false),
    DECLINE_RESTRICTED_CARD("62", "Decline Restricted Card", false, false, false),
    DECLINE_SECURITY_VIOLATION("63", "Decline Security Violation / Invalid AMEX CID", false, false, false),
    DECLINE_EXCEEDS_WITHDRAWAL_FREQUENCY_LIMIT("65", "Decline Exceeds Withdrawal Frequency Limit", false, false, false),
    DECLINE_PICKUPCARD("67", "Decline Pick Up Card", false, true, false),
    DECLINE_LATE_RESPONSE("68", "Decline Response Received Late", false, false, false),
    DECLINE_BAD_CLOSE("69", "Decline Bad Close (Gift Card)", false, false, false),
    DECLINE_CONTACT_ISSUER("70", "Decline Invalid Transaction, Contact Issuer / Card Already Active (Gift Card)", false, false, false),
    DECLINE_NOT_ACTIVE("71", "Decline Card Not Active (Gift Card)", false, false, false),
    DECLINE_ALREADY_CLOSED("72", "Decline Card Already Closed (Gift Card)", false, false, false),
    DECLINE_OVER_MAX_BALANCE("73", "Decline Over Max Balance (Gift Card)", false, false, false),
    DECLINE_INVALID_ACTIVATE("74", "Decline Invalid Activate (Gift Card)", false, false, false),
    DECLINE_PIN_TRIES_EXCEEDED_2("75", "Decline Allowable Number of PIN Tries Exceeded", false, false, false),
    DECLINE_LATE_REVERSAL("76", "Decline Late Reversal", false, false, false),
    DECLINE_REVERSAL_DOESNT_MATCH("77", "Decline Reversal Does Not Match Original Transaction", false, false, false),
    DECLINE_NO_TO_ACCOUNT("78", "Decline No 'To' Account Specified", false, false, false),
    DECLINE_NO_FROM_ACCOUNT("79", "Decline No 'From' Account Specified", false, false, false),
    DECLINE_LINK_OUT_OF_SERVICE("80", "Decline Processor Link Out of Service, Will Cause Vantiv to Invoke Stand-in", false, false, false),
    DECLINE_PIN_KEY_SYNC_ERROR("81", "Decline PIN Key Synchronization Error", false, false, false),
    DECLINE_INVALID_CVV("82", "Decline Invalid CVV", false, false, false),
    DECLINE_UNABLE_TO_VERIFY_PIN("83", "Decline Unable to Verify PIN", false, false, false),
    APPROVE_NOT_DECLINED("85", "Approve No Reason to Decline on Verification Request", true, false, false),
    APPROVE_PURCHASE_NOT_CASH("87", "Approve Purchase Amount Approved, Not Cash", true, false, false),
    DECLINE_NOT_AVAILABLE("88", "Decline Card Record Not Available", false, false, false),
    DECLINE_ISSUER_INOPERATIVE("91", "Decline Issuer or Switch Inoperative (MasterCard)", false, false, false),
    DECLINE_NO_ROUTE("92", "Decline Unable to Route Transaction", false, false, false),
    DECLINE_ILLEGAL_TRANSACTION("93", "Decline Illegal Transaction", false, false, false),
    DECLINE_DUPLICATE_TRANSACTION("94", "Decline Duplicate Transaction", false, false, false),
    DECLINE_RECONCILIATION_ERROR("95", "Decline Reconciliation Error", false, false, false),
    DECLINE_SYSTEM_ERROR("96", "Decline System Error", false, false, false),
    APPROVE_AMEX_REWARDS("97", "Approval American Express Rewards Approval", true, false, false),
    DECLINE_DUPLICATE("98", "Decline Duplicate Transaction", false, false, false),
    DECLINE_PREFFERED("99", "Decline Preferred Debit Routing Denial Credit transaction can be performed as debit", false, false, false),
    DECLINE_CONVERSION_COMPLETE("D1", "Decline Currency Conversion Complete, No Auth Performed (1st Pass)", false, false, false),
    DECLINE_CVV2_MISMATCH("N7", "Decline CVV2 Value Mismatch", false, false, false),
    DECLINE_DCC_FAIL("M1", "Decline Multi-Currency DCC Fail", false, false, false),
    DECLINE_INVERT_FAIL("M2", "Decline Multi-Currency Invert Fail", false, false, false),
    DECLINE_SWITCH_INOPERATIVE("N0", "Decline Issuer or Switch Inoperative (Visa)", false, false, false),
    APPROVE_P2PE("RG", "Approve P2PE Successful Registration Event", true, false, false),
    DECLINE_STOP_PAYMENT("R0", "Decline Stop Payment Order", false, false, false),
    DECLINE_REVOCATION("R1", "Decline Revocation of Auth Order", false, false, false),
    DECLINE_REVOCATION_ALL("R3", "Decline Revocation of All Auth Orders", false, false, false),
    DECLINE_VELOCITY_COUNT("V1", "Decline Velocity - Excessive Count", false, false, false),
    DECLINE_VELOCITY_AMOUNT("V2", "Decline Velocity - Excessive Amount", false, false, false),
    DECLINE_VELOCITY_COUNT_AMOUNT("V3", "Decline Velocity - Excessive Count/Amount", false, false, false),
    DECLINE_VELOCITY_NEGATIVE_FILE("V4", "Decline Velocity - Negative File Exception", false, false, false),
    DECLINE_VELOCITY_FRAUD("V5", "Decline Velocity - Fraud Exception", false, false, false),
    DECLINE_VELOCITY_ZIP("V6", "Decline Velocity - ZIP Match Failure", false, false, false),
    DECLINE_ESCHEATED("XE", "Decline Card Escheated (Gift Card)", false, false, false),
    DECLINE_DEPLETED("XD", "Decline Merchant Depleted (Gift Card)", false, false, false),
    DECLINE_DECONVERTED("XB", "Decline Deconverted BIN (Gift Card)", false, false, false),
    DECLINE_CHIP_FAILURE("Q1", "Decline Chip Failure", false, false, false);

    private String  code;
    private boolean approved;
    private boolean pickup;
    private String  description;
    private boolean call;

    ResponseCode(String code, String description, boolean approved, boolean pickup, boolean call) {
        this.approved = approved;
        this.code = code;
        this.description = description;
        this.pickup = pickup;
        this.call = call;
    }

    public String getDescription() {
        return description;
    }

    public String getCode() {
        return code;
    }

    public boolean isApproved() {
        return approved;
    }

    public boolean isPickup() {
        return pickup;
    }

    public boolean isCall() {
        return call;
    }

    private final static Map<String, ResponseCode> responseCodeList = new HashMap<String, ResponseCode>();

    public static ResponseCode findByCode(String code) {
        return responseCodeList.get(code);
    }

    static {
        for (ResponseCode responseCode : ResponseCode.values()) {
            responseCodeList.put(responseCode.code, responseCode);
        }
    }
}
