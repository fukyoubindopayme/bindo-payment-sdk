package com.paymentsdk.v3.pay.gateway.globalpayments;

import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Request;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Response;
import com.bindo.paymentsdk.v3.pay.common.util.DataConversion;
import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.gateway.Gateway;

import java.util.HashMap;

public class GlobalPaymentsGateway implements Gateway {
    private final String TAG = "GlobalPaymentsGateway";

    private HashMap<String, Object> mConfigMap = new HashMap<>();

    public GlobalPaymentsGateway(HashMap<String, Object> configMap) {
        if (configMap == null) {
            Log.w(TAG, "Please set the gateway parameters");
        } else {
            for (String key : configMap.keySet()) {
                Object value = configMap.get(key);
                mConfigMap.put(key, value);
            }
        }
    }

    @Override
    public Response sendRequest(Request request) {
        return null;
    }
}
