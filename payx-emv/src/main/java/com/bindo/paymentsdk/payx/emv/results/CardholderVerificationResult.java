package com.bindo.paymentsdk.payx.emv.results;


public enum CardholderVerificationResult {
    CONFIRM,
    CANCEL,
    ERROR,
}
