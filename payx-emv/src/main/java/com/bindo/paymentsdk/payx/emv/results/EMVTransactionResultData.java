package com.bindo.paymentsdk.payx.emv.results;

import com.bindo.paymentsdk.payx.emv.core.enmus.ACType;
import com.bindo.paymentsdk.payx.emv.core.enmus.CVMFlag;
import com.bindo.paymentsdk.v3.pay.common.legacy.CreditCard;
import com.bindo.paymentsdk.v3.pay.common.legacy.enums.CardReadMode;

import java.util.HashMap;

@Deprecated
public class EMVTransactionResultData {
    private CardReadMode cardReadMode;
    private CreditCard creditCard;
    private OnlineProcessingResult onlineProcessingResult;
    private OnlineReversalProcessingResult onlineReversalProcessingResult;

    private ACType acType;
    private CVMFlag cvmFlag;
    private HashMap<String, byte[]> tagLengthValues;

    public EMVTransactionResultData() {
    }

    public CardReadMode getCardReadMode() {
        return cardReadMode;
    }

    public void setCardReadMode(CardReadMode cardReadMode) {
        this.cardReadMode = cardReadMode;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }

    public OnlineProcessingResult getOnlineProcessingResult() {
        return onlineProcessingResult;
    }

    public void setOnlineProcessingResult(OnlineProcessingResult onlineProcessingResult) {
        this.onlineProcessingResult = onlineProcessingResult;
    }

    public OnlineReversalProcessingResult getOnlineReversalProcessingResult() {
        return onlineReversalProcessingResult;
    }

    public void setOnlineReversalProcessingResult(OnlineReversalProcessingResult onlineReversalProcessingResult) {
        this.onlineReversalProcessingResult = onlineReversalProcessingResult;
    }

    public ACType getACType() {
        return acType;
    }

    public void setACType(ACType acType) {
        this.acType = acType;
    }

    public CVMFlag getCVMFlag() {
        return cvmFlag;
    }

    public void setCVMFlag(CVMFlag cvmFlag) {
        this.cvmFlag = cvmFlag;
    }

    public HashMap<String, byte[]> getTagLengthValues() {
        return tagLengthValues;
    }

    public void setTagLengthValues(HashMap<String, byte[]> tagLengthValues) {
        this.tagLengthValues = tagLengthValues;
    }
}
