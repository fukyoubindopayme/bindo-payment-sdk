package com.bindo.paymentsdk.payx.emv.core;

public class Application {
    private String[] BINRanges;
    private byte[] applicationID;
    private byte applicationSelectionIndicator;
    private byte[] terminalActionCodeDefault;
    private byte[] terminalActionCodeOnline;
    private byte[] terminalActionCodeDenial;
    private byte[] applicationVersionNumber;
    private byte[] TDOL;
    private byte[] DDOL;
    private float floorLimit;
    private float contactlessFloorLimit;
    private float contactlessCardholderVerificationLimit;
    private float contactlessTransactionLimit;
    private CAPK[] certificationAuthorityPublicKeys;
    private String merchantNumber;

    public String[] getBINRanges() {
        return BINRanges;
    }

    public void setBINRanges(String[] BINRanges) {
        this.BINRanges = BINRanges;
    }

    public byte[] getApplicationID() {
        return applicationID;
    }

    public void setApplicationID(byte[] applicationID) {
        this.applicationID = applicationID;
    }

    public byte getApplicationSelectionIndicator() {
        return applicationSelectionIndicator;
    }

    public void setApplicationSelectionIndicator(byte applicationSelectionIndicator) {
        this.applicationSelectionIndicator = applicationSelectionIndicator;
    }

    public byte[] getTerminalActionCodeDefault() {
        return terminalActionCodeDefault;
    }

    public void setTerminalActionCodeDefault(byte[] terminalActionCodeDefault) {
        this.terminalActionCodeDefault = terminalActionCodeDefault;
    }

    public byte[] getTerminalActionCodeOnline() {
        return terminalActionCodeOnline;
    }

    public void setTerminalActionCodeOnline(byte[] terminalActionCodeOnline) {
        this.terminalActionCodeOnline = terminalActionCodeOnline;
    }

    public byte[] getTerminalActionCodeDenial() {
        return terminalActionCodeDenial;
    }

    public void setTerminalActionCodeDenial(byte[] terminalActionCodeDenial) {
        this.terminalActionCodeDenial = terminalActionCodeDenial;
    }

    public byte[] getApplicationVersionNumber() {
        return applicationVersionNumber;
    }

    public void setApplicationVersionNumber(byte[] applicationVersionNumber) {
        this.applicationVersionNumber = applicationVersionNumber;
    }

    public byte[] getTDOL() {
        return TDOL;
    }

    public void setTDOL(byte[] TDOL) {
        this.TDOL = TDOL;
    }

    public byte[] getDDOL() {
        return DDOL;
    }

    public void setDDOL(byte[] DDOL) {
        this.DDOL = DDOL;
    }

    public float getFloorLimit() {
        return floorLimit;
    }

    public void setFloorLimit(float floorLimit) {
        this.floorLimit = floorLimit;
    }

    public float getContactlessFloorLimit() {
        return contactlessFloorLimit;
    }

    public void setContactlessFloorLimit(float contactlessFloorLimit) {
        this.contactlessFloorLimit = contactlessFloorLimit;
    }

    public float getContactlessCardholderVerificationLimit() {
        return contactlessCardholderVerificationLimit;
    }

    public void setContactlessCardholderVerificationLimit(float contactlessCardholderVerificationLimit) {
        this.contactlessCardholderVerificationLimit = contactlessCardholderVerificationLimit;
    }

    public float getContactlessTransactionLimit() {
        return contactlessTransactionLimit;
    }

    public void setContactlessTransactionLimit(float contactlessTransactionLimit) {
        this.contactlessTransactionLimit = contactlessTransactionLimit;
    }

    public CAPK[] getCertificationAuthorityPublicKeys() {
        return certificationAuthorityPublicKeys;
    }

    public void setCertificationAuthorityPublicKeys(CAPK[] certificationAuthorityPublicKeys) {
        this.certificationAuthorityPublicKeys = certificationAuthorityPublicKeys;
    }

    public String getMerchantNumber() {
        return merchantNumber;
    }

    public void setMerchantNumber(String merchantNumber) {
        this.merchantNumber = merchantNumber;
    }

    public static class Builder {
        private Application application;

        public Builder() {
            application = new Application();
        }

        public Application build() {
            return application;
        }

        public Builder setBINRanges(String[] BINRanges) {
            this.application.setBINRanges(BINRanges);
            return this;
        }

        public Builder setApplicationID(byte[] applicationID) {
            this.application.setApplicationID(applicationID);;
            return this;
        }

        public Builder setApplicationSelectionIndicator(byte applicationSelectionIndicator) {
            this.application.setApplicationSelectionIndicator(applicationSelectionIndicator);
            return this;
        }

        public Builder setTerminalActionCodeDefault(byte[] terminalActionCodeDefault) {
            this.application.setTerminalActionCodeDefault(terminalActionCodeDefault);
            return this;
        }

        public Builder setTerminalActionCodeOnline(byte[] terminalActionCodeOnline) {
            this.application.setTerminalActionCodeOnline(terminalActionCodeOnline);
            return this;
        }

        public Builder setTerminalActionCodeDenial(byte[] terminalActionCodeDenial) {
            this.application.setTerminalActionCodeDenial(terminalActionCodeDenial);
            return this;
        }

        public Builder setApplicationVersionNumber(byte[] applicationVersionNumber) {
            this.application.setApplicationVersionNumber(applicationVersionNumber);
            return this;
        }

        public Builder setTDOL(byte[] TDOL) {
            this.application.setTDOL(TDOL);
            return this;
        }

        public Builder setDDOL(byte[] DDOL) {
            this.application.setDDOL(DDOL);
            return this;
        }

        public Builder setFloorLimit(float floorLimit) {
            this.application.setFloorLimit(floorLimit);
            return this;
        }

        public Builder setContactlessFloorLimit(float contactlessFloorLimit) {
            this.application.setContactlessFloorLimit(contactlessFloorLimit);
            return this;
        }

        public Builder setContactlessCardholderVerificationLimit(float contactlessCardholderVerificationLimit) {
            this.application.setContactlessCardholderVerificationLimit(contactlessCardholderVerificationLimit);
            return this;
        }

        public Builder setContactlessTransactionLimit(float contactlessTransactionLimit) {
            this.application.setContactlessTransactionLimit(contactlessTransactionLimit);
            return this;
        }

        public Builder setCertificationAuthorityPublicKeys(CAPK[] certificationAuthorityPublicKeys) {
            this.application.setCertificationAuthorityPublicKeys(certificationAuthorityPublicKeys);
            return this;
        }

        public Builder setMerchantNumber(String merchantNumber) {
            this.application.setMerchantNumber(merchantNumber);
            return this;
        }
    }
}
