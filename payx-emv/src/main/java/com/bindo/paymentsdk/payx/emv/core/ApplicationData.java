package com.bindo.paymentsdk.payx.emv.core;

import com.bindo.paymentsdk.payx.emv.core.enmus.KernelID;

public class ApplicationData {
    private KernelID kernelID;
    private byte[] applicationID;
    private byte flowType;

    public ApplicationData(KernelID kernelID, byte[] applicationID) {
        this.kernelID = kernelID;
        this.applicationID = applicationID;
    }

    public KernelID getKernelID() {
        return kernelID;
    }

    public void setKernelID(KernelID kernelID) {
        this.kernelID = kernelID;
    }

    public byte[] getApplicationID() {
        return applicationID;
    }

    public void setApplicationID(byte[] applicationID) {
        this.applicationID = applicationID;
    }

    public byte getFlowType() {
        return flowType;
    }

    public void setFlowType(byte flowType) {
        this.flowType = flowType;
    }
}
