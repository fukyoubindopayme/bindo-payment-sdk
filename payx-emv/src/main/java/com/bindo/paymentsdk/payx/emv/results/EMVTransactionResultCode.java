package com.bindo.paymentsdk.payx.emv.results;

@Deprecated
public enum EMVTransactionResultCode {
    APPROVED_BY_OFFLINE,
    APPROVED_BY_ONLINE,
    DECLINED_BY_OFFLINE,
    DECLINED_BY_ONLINE,
    DECLINED_BY_TIMEOUT_AND_REVERSED,
    DECLINED_BY_TERMINAL_AND_REVERSED,
    CARD_DETECT_MULTIPLE_CARDS,
    CARD_DETECT_TIMEOUT,
    CARD_DETECT_IC_CARD_UNUSUAL,
    CARD_DETECT_RF_CARD_UNUSUAL,
    CARD_DETECT_MAG_CARD_UNUSUAL,
    EMV_RESULT_NO_APP,
    EMV_RESULT_NO_PUBLIC_KEY,
    EMV_RESULT_CARD_BLOCKED,
    EMV_RESULT_APP_BLOCKED,
    AMEX_RESULT_TRY_ANOTHER_INTERFACE,
    AMEX_RESULT_TRY_ANOTHER_PAYMENT,
    AMEX_RESULT_TRY_AGAIN,
    AMEX_RESULT_OFFLINE_DECLINED,
    EMV_RESULT_REPOWERICC,
    EMV_RESULT_RESTART,
    ERROR_UNKNOWN;

    private int code;
    private String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    EMVTransactionResultCode() {
        this.code = 0;
        this.message = this.name();
    }
}
