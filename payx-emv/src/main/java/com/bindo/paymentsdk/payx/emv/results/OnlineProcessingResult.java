package com.bindo.paymentsdk.payx.emv.results;

public class OnlineProcessingResult {
    private boolean isUnableToGoOnline;
    private boolean isRequestTimeout;

    private boolean isNeedCallIssuerConfirm;
    private boolean isApproved;
    private String approvalCode;

    private String iccRelatedDataIssuerScriptData;
    private String iccRelatedDataIssuerAuthenticationData;

    public boolean isUnableToGoOnline() {
        return isUnableToGoOnline;
    }

    public void setUnableToGoOnline(boolean unableToGoOnline) {
        isUnableToGoOnline = unableToGoOnline;
    }

    public boolean isRequestTimeout() {
        return isRequestTimeout;
    }

    public void setRequestTimeout(boolean requestTimeout) {
        isRequestTimeout = requestTimeout;
    }

    public boolean isNeedCallIssuerConfirm() {
        return isNeedCallIssuerConfirm;
    }

    public void setNeedCallIssuerConfirm(boolean needCallIssuerConfirm) {
        isNeedCallIssuerConfirm = needCallIssuerConfirm;
    }

    public boolean isApproved() {
        return isApproved;
    }

    public void setApproved(boolean approved) {
        isApproved = approved;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    public String getIccRelatedDataIssuerScriptData() {
        return iccRelatedDataIssuerScriptData;
    }

    public void setIccRelatedDataIssuerScriptData(String iccRelatedDataIssuerScriptData) {
        this.iccRelatedDataIssuerScriptData = iccRelatedDataIssuerScriptData;
    }

    public String getIccRelatedDataIssuerAuthenticationData() {
        return iccRelatedDataIssuerAuthenticationData;
    }

    public void setIccRelatedDataIssuerAuthenticationData(String iccRelatedDataIssuerAuthenticationData) {
        this.iccRelatedDataIssuerAuthenticationData = iccRelatedDataIssuerAuthenticationData;
    }

}
