package com.bindo.paymentsdk.payx.emv;

import com.bindo.paymentsdk.payx.emv.core.Candidate;
import com.bindo.paymentsdk.payx.emv.results.CallIssuerConfirmResult;
import com.bindo.paymentsdk.payx.emv.results.EMVTransactionResultData;
import com.bindo.paymentsdk.payx.emv.core.enmus.CVMFlag;
import com.bindo.paymentsdk.payx.emv.core.ApplicationData;
import com.bindo.paymentsdk.payx.emv.results.CardholderVerificationResult;
import com.bindo.paymentsdk.payx.emv.results.OnlineOfflineDecisionResult;
import com.bindo.paymentsdk.payx.emv.results.EMVTransactionResultCode;
import com.bindo.paymentsdk.payx.emv.results.OnlineProcessingResult;
import com.bindo.paymentsdk.payx.emv.results.OnlineReversalProcessingResult;
import com.bindo.paymentsdk.v3.pay.common.TransactionData;
import com.bindo.paymentsdk.v3.pay.common.legacy.CreditCard;
import com.bindo.paymentsdk.v3.pay.common.legacy.enums.CardReadMode;

import java.util.HashMap;
import java.util.List;

public interface EMVCoreListener {

    void onError(String code, String message);

    void onReset();

    void onCardDetected(CardReadMode cardReadMode, CreditCard creditCard);

    Candidate confirmApplicationSelection(List<Candidate> candidateList);

    HashMap<String, byte[]> resolveTerminalParameters(ApplicationData applicationData);

    HashMap<String, byte[]> resolvePreTransactionParameters(ApplicationData applicationData);

    void onReadApplicationData(ApplicationData applicationData, byte capkIndex);

    HashMap<String, byte[]> resolveTransactionParameters(ApplicationData applicationData);

    CardholderVerificationResult cardholderVerification(CVMFlag cvmFlag);

    OnlineOfflineDecisionResult onlineOfflineDecision(CardReadMode cardReadMode, CreditCard creditCard);

    OnlineProcessingResult performOnlineProcessing(CreditCard creditCard, HashMap<String, byte[]> tagLengthValues);

    CallIssuerConfirmResult callIssuerConfirm();

    HashMap<String, byte[]> resolveOnlineProcessingResult(OnlineProcessingResult onlineProcessingResult, CallIssuerConfirmResult callIssuerConfirmResult);

    @Deprecated
    OnlineReversalProcessingResult performOnlineReversalProcessing(int reason, OnlineProcessingResult onlineProcessingResult, CreditCard creditCard, HashMap<String, byte[]> tagLengthValues);

    @Deprecated
    void onTransactionCompleted(EMVTransactionResultCode emvTransactionResultCode, EMVTransactionResultData EMVTransactionResultData);
}