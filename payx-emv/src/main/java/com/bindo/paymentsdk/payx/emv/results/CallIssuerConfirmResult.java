package com.bindo.paymentsdk.payx.emv.results;

public enum CallIssuerConfirmResult {
    NO_NEED,
    WAITING,
    ACCEPT,
    REJECT,
}
