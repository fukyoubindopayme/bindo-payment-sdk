package com.bindo.paymentsdk.payx.emv;

import com.bindo.paymentsdk.payx.emv.core.Application;
import com.bindo.paymentsdk.payx.emv.core.ApplicationData;
import com.bindo.paymentsdk.payx.emv.core.CAPK;
import com.bindo.paymentsdk.payx.emv.results.CallIssuerConfirmResult;
import com.bindo.paymentsdk.payx.emv.results.OnlineProcessingResult;
import com.bindo.paymentsdk.v3.pay.common.TransactionRetryFlag;
import com.bindo.paymentsdk.v3.pay.common.TransactionType;
import com.bindo.paymentsdk.v3.pay.common.emv.enums.EMVTag;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class EMVCore {
    public final String TAG = "EMVCore";

    protected TransactionRetryFlag retryFlag = TransactionRetryFlag.DEFAULT;
    protected boolean supportICCard = true;
    protected boolean supportRFCard = true;
    protected boolean supportMagCard = true;
    protected boolean forceOnline = false;
    protected EMVCoreListener mListener;
    private TransactionType transactionType;

    protected List<Application> mApplications = new ArrayList<>();

    public EMVCore() {
    }

    public TransactionRetryFlag getRetryFlag() {
        return retryFlag;
    }

    public void setRetryFlag(TransactionRetryFlag retryFlag) {
        this.retryFlag = retryFlag;
    }

    public boolean isSupportRFCard() {
        return supportRFCard;
    }

    public void setSupportRFCard(boolean supportRFCard) {
        this.supportRFCard = supportRFCard;
    }

    public boolean isSupportMagCard() {
        return supportMagCard;
    }

    public void setSupportMagCard(boolean supportMagCard) {
        this.supportMagCard = supportMagCard;
    }

    public boolean isSupportICCard() {
        return supportICCard;
    }

    public void setSupportICCard(boolean supportICCard) {
        this.supportICCard = supportICCard;
    }

    public boolean isForceOnline() {
        return forceOnline;
    }

    public void setForceOnline(boolean forceOnline) {
        this.forceOnline = forceOnline;
    }

    public void setListener(EMVCoreListener listener) {
        mListener = listener;
    }

    public abstract void startProcess();

    public abstract void startProcess(boolean isFallback);

    public abstract void startProcess(boolean isFallback, int detectRetryCount);

    public abstract void stopProcess();

    public abstract void addApplication(Application application);

    public abstract void removeApplication(Application application);

    public List<Application> getApplications() {
        return mApplications;
    }

    public abstract HashMap<String, byte[]> resolveSpecificTerminalParameters(ApplicationData applicationData);

    public abstract HashMap<String, byte[]> resolveSpecificPreTransactionParameters(ApplicationData applicationData);

    public abstract HashMap<String, byte[]> resolveSpecificTransactionParameters(ApplicationData applicationData);

    public abstract HashMap<String, byte[]> resolveSpecificOnlineProcessingResult(OnlineProcessingResult onlineAuthenticationProcessingResult, CallIssuerConfirmResult callIssuerConfirmResult);

    public abstract byte[] getTLV(EMVTag emvTag);

    public abstract byte[] getTLV(String emvTag);

    public abstract int setTLV(EMVTag emvTag, byte[] data);

    public abstract int setTLV(String emvTag, byte[] data);

    public abstract int setCertificationAuthorityPublicKey(CAPK capk);

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }
}
