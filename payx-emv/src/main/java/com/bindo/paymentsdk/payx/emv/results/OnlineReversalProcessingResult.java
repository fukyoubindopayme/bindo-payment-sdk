package com.bindo.paymentsdk.payx.emv.results;

@Deprecated
public class OnlineReversalProcessingResult {
    private boolean isApproved;

    public boolean isApproved() {
        return isApproved;
    }

    public void setApproved(boolean approved) {
        isApproved = approved;
    }
}
