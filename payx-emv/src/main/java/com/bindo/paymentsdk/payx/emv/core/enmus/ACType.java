package com.bindo.paymentsdk.payx.emv.core.enmus;

public enum ACType {
    /**
     * 交易拒绝
     */
    AAC,
    /**
     * 交易批准
     */
    TC,
    /**
     * 需要请求联机
     */
    ARQC,
}
