package com.bindo.paymentsdk.payx.emv.core.enmus;

public enum CVMFlag {
    NO_CVM,
    ONLINE_PIN,
    OFFLINE_PIN,
    SIGNATURE,
}
