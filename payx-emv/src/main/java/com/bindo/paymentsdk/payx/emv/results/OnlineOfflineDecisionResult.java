package com.bindo.paymentsdk.payx.emv.results;

public enum OnlineOfflineDecisionResult {
    UNSURE,
    ONLINE,
    OFFLINE,
}
