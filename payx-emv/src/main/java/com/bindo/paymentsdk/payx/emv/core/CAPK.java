package com.bindo.paymentsdk.payx.emv.core;

public class CAPK {
    private byte header;
    private byte[] serviceIdentifier;
    private byte CAPKAlgorithmIndicator;
    private byte[] RID;
    private byte CAPKIndex;
    private byte[] CAPKModulus;
    private byte lengthOfCAPKModulus;
    private byte[] CAPKExponent;
    private byte lengthOfCAPKExponent;
    private byte[] hashValue;

    public byte getHeader() {
        return header;
    }

    public void setHeader(byte header) {
        this.header = header;
    }

    public byte[] getServiceIdentifier() {
        return serviceIdentifier;
    }

    public void setServiceIdentifier(byte[] serviceIdentifier) {
        this.serviceIdentifier = serviceIdentifier;
    }

    public byte getCAPKAlgorithmIndicator() {
        return CAPKAlgorithmIndicator;
    }

    public void setCAPKAlgorithmIndicator(byte CAPKAlgorithmIndicator) {
        this.CAPKAlgorithmIndicator = CAPKAlgorithmIndicator;
    }

    public byte[] getRID() {
        return RID;
    }

    public void setRID(byte[] RID) {
        this.RID = RID;
    }

    public byte getCAPKIndex() {
        return CAPKIndex;
    }

    public void setCAPKIndex(byte CAPKIndex) {
        this.CAPKIndex = CAPKIndex;
    }

    public byte[] getCAPKModulus() {
        return CAPKModulus;
    }

    public void setCAPKModulus(byte[] CAPKModulus) {
        this.CAPKModulus = CAPKModulus;
    }

    public byte getLengthOfCAPKModulus() {
        return lengthOfCAPKModulus;
    }

    public void setLengthOfCAPKModulus(byte lengthOfCAPKModulus) {
        this.lengthOfCAPKModulus = lengthOfCAPKModulus;
    }

    public byte[] getCAPKExponent() {
        return CAPKExponent;
    }

    public void setCAPKExponent(byte[] CAPKExponent) {
        this.CAPKExponent = CAPKExponent;
    }

    public byte getLengthOfCAPKExponent() {
        return lengthOfCAPKExponent;
    }

    public void setLengthOfCAPKExponent(byte lengthOfCAPKExponent) {
        this.lengthOfCAPKExponent = lengthOfCAPKExponent;
    }

    public byte[] getHashValue() {
        return hashValue;
    }

    public void setHashValue(byte[] hashValue) {
        this.hashValue = hashValue;
    }

    public static class Builder {
        private CAPK capk;

        public Builder() {
            capk = new CAPK();
        }

        public Builder setHeader(byte header) {
            this.capk.setHeader(header);
            return this;
        }

        public Builder setServiceIdentifier(byte[] serviceIdentifier) {
            this.capk.setServiceIdentifier(serviceIdentifier);
            return this;
        }

        public Builder setCAPKAlgorithmIndicator(byte CAPKAlgorithmIndicator) {
            this.capk.setCAPKAlgorithmIndicator(CAPKAlgorithmIndicator);
            return this;
        }

        public Builder setRID(byte[] RID) {
            this.capk.setRID(RID);
            return this;
        }

        public Builder setCAPKIndex(byte CAPKIndex) {
            this.capk.setCAPKIndex(CAPKIndex);
            return this;
        }

        public Builder setCAPKModulus(byte[] CAPKModulus) {
            this.capk.setCAPKModulus(CAPKModulus);
            return this;
        }

        public Builder setLengthOfCAPKModulus(byte lengthOfCAPKModulus) {
            this.capk.setLengthOfCAPKModulus(lengthOfCAPKModulus);
            return this;
        }

        public Builder setCAPKExponent(byte[] CAPKExponent) {
            this.capk.setCAPKExponent(CAPKExponent);
            return this;
        }

        public Builder setLengthOfCAPKExponent(byte lengthOfCAPKExponent) {
            this.capk.setLengthOfCAPKExponent(lengthOfCAPKExponent);
            return this;
        }

        public Builder setHashValue(byte[] hashValue) {
            this.capk.setHashValue(hashValue);
            return this;
        }

        public CAPK build() {
            return this.capk;
        }
    }
}
