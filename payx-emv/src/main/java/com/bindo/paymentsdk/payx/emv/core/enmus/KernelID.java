package com.bindo.paymentsdk.payx.emv.core.enmus;

public enum KernelID {
    EMVContact,
    EMVContactless,
    MASTER,
    VISA,
    AMEX,
    JCB,
    PBOC,
    DEFINE,
}
