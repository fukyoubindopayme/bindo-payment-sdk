package com.bindo.paymentsdk.payx.emv.pinpad;

import com.bindo.paymentsdk.payx.emv.results.CardholderVerificationResult;

public abstract class PinPad {
    public abstract CardholderVerificationResult verifyPinByPinPad(boolean isOnlinePin, byte[] lengthLimit);
}
