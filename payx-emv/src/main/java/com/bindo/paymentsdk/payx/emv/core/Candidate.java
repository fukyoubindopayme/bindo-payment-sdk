package com.bindo.paymentsdk.payx.emv.core;

public class Candidate {
    private byte[] applicationID;
    private byte[] applicationLabel;

    public Candidate(byte[] applicationID, byte[] applicationLabel) {
        this.applicationID = applicationID;
        this.applicationLabel = applicationLabel;
    }

    public byte[] getApplicationID() {
        return applicationID;
    }

    public void setApplicationID(byte[] applicationID) {
        this.applicationID = applicationID;
    }

    public byte[] getApplicationLabel() {
        return applicationLabel;
    }

    public void setApplicationLabel(byte[] applicationLabel) {
        this.applicationLabel = applicationLabel;
    }
}
