package com.bindo.paymentsdk.v3.examples.modules.payprocessor;

import android.content.Context;
import android.content.res.AssetManager;

import com.bindo.paymentsdk.v3.pay.common.TransactionType;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Request;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Response;
import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.gateway.Gateway;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gateway.AmexGateway;
import com.bindo.paymentsdk.v3.pay.gateway.bea.BankOfEastAsiaGateway;
import com.bindo.paymentsdk.v3.pay.gateway.fake.FakeGateway;
import com.bindo.paymentsdk.v3.pay.gateway.planetpayment.PlanetPaymentGateway;
import com.bindo.paymentsdk.v3.pay.gateway.vantiv.VantivGateway;

import java.io.FileInputStream;
import java.io.InputStream;
import java.security.KeyStore;
import java.util.HashMap;

public class PayProcessorSimpleGateway implements Gateway {
    private final String TAG = PayProcessorSimpleGateway.class.getSimpleName();

    private Context context;
    private int processingRule = ProcessingRules.American_Express;

    public PayProcessorSimpleGateway(Context context, int processingRule) {
        this.context = context;
        this.processingRule = processingRule;
    }

    @Override
    public Response sendRequest(Request request) {
        Gateway gateway = null;

        try {
            switch (processingRule) {
                case ProcessingRules.American_Express:
                    gateway = createAmericanExpressGateway();
                    break;
                case ProcessingRules.Bank_Of_East_Asia:
                    HashMap<String, String> beaGatewayConfigs = new HashMap<>();
                    // move to production, remove default value
                /*
                beaGatewayConfigs.put(BankOfEastAsiaGateway.CONFIG_REQUEST_CARD_ACCEPTOR_ID_CODE, "581226980703000");
                beaGatewayConfigs.put(BankOfEastAsiaGateway.CONFIG_REQUEST_CARD_ACCEPTOR_TERMINAL_ID, "63150002");
                beaGatewayConfigs.put(BankOfEastAsiaGateway.CONFIG_REQUEST_TPDU, "7000280000");
                beaGatewayConfigs.put(BankOfEastAsiaGateway.CONFIG_REQUEST_EDS, "0003000A00F000");
                gateway = new BankOfEastAsiaGateway(beaGatewayConfigs);
                */
                    gateway = createBankOfEastAsiaGateway();
                    break;
                case ProcessingRules.Bank_Of_East_Asia_With_DCC:
                    if (request.getTransactionType() == TransactionType.RATE_LOOKUP) {
                        gateway = createPlanetPaymentGateway();
                    } else {
                        gateway = createBankOfEastAsiaGateway();
                    }
                    break;
                case ProcessingRules.Planet_Payment:
                    gateway = createPlanetPaymentGateway();
                    break;
                case ProcessingRules.Vantiv:
                    gateway = createVantivGateway();
                    break;
                case ProcessingRules.Fake:
                    gateway = new FakeGateway(new HashMap<String, Object>());
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d(TAG, "Use " + gateway.getClass().getSimpleName() + " sendRequest -> " + request.getTransactionType().name());
        return gateway.sendRequest(request);
    }

    private Gateway createAmericanExpressGateway() {
        return new AmexGateway(new HashMap<String, Object>());
    }

    private Gateway createBankOfEastAsiaGateway() throws Exception {
        AssetManager assetManager = context.getAssets();
        HashMap<String, Object> configMap = new HashMap<>();

        try {
            KeyStore keyStore = KeyStore.getInstance("PKCS12");
            InputStream keyIS = assetManager.open("bindo.keystore");
            keyStore.load(keyIS, "123456".toCharArray());
//            KeyStore trustedKeyStore = KeyStore.getInstance("PKCS12");
//            InputStream tkeyIS = assetManager.open("ca-trust.keystore");
//            trustedKeyStore.load(tkeyIS, "123456".toCharArray());
            configMap.put(PlanetPaymentGateway.CONFIG_REQUEST_KEYSTORE_TERMINAL, keyStore);
            configMap.put(PlanetPaymentGateway.CONFIG_REQUEST_KEYSTORE_TRUSTED, null);
            // TODO: Unsure how to set keyStore to gateway.
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new BankOfEastAsiaGateway(new HashMap<String, String>());
    }

    private Gateway createPlanetPaymentGateway() {
        AssetManager assetManager = context.getAssets();
        HashMap<String, Object> gatewayConfig = new HashMap<>();

        gatewayConfig.put(PlanetPaymentGateway.CONFIG_REQUEST_CARD_ACCEPTOR_ID_CODE, "001711112222");
        gatewayConfig.put(PlanetPaymentGateway.CONFIG_REQUEST_CARD_ACCEPTOR_TERMINAL_ID, "82831111");
        gatewayConfig.put(PlanetPaymentGateway.CONFIG_REQUEST_TPDU, "60005281B6");

        try {
            KeyStore keyStoreTerminal = KeyStore.getInstance("PKCS12");
            InputStream keyIS = assetManager.open("bindo.keystore");
            keyStoreTerminal.load(keyIS, "123456".toCharArray());
            KeyStore keyStoreTrusted = KeyStore.getInstance("PKCS12");
            InputStream tkeyIS = assetManager.open("ca-trust.keystore");
            gatewayConfig.put(PlanetPaymentGateway.CONFIG_REQUEST_KEYSTORE_TERMINAL, keyStoreTerminal);
            gatewayConfig.put(PlanetPaymentGateway.CONFIG_REQUEST_KEYSTORE_TRUSTED, keyStoreTrusted);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new PlanetPaymentGateway(gatewayConfig);
    }

    private Gateway createVantivGateway() {
        AssetManager assetManager = context.getAssets();
        HashMap<String, Object> gatewayConfig = new HashMap<>();

        /*
        gatewayConfig.put(PlanetPaymentGateway.CONFIG_REQUEST_CARD_ACCEPTOR_ID_CODE, "001711112222");
        gatewayConfig.put(PlanetPaymentGateway.CONFIG_REQUEST_CARD_ACCEPTOR_TERMINAL_ID, "82831111");
        gatewayConfig.put(PlanetPaymentGateway.CONFIG_REQUEST_TPDU, "60005281B6");
        */

        try {
            KeyStore keyStoreTerminal = KeyStore.getInstance("PKCS12");
            InputStream keyIS = assetManager.open("bindo.keystore");
            keyStoreTerminal.load(keyIS, "123456".toCharArray());
            KeyStore keyStoreTrusted = KeyStore.getInstance("PKCS12");
            InputStream tkeyIS = assetManager.open("ca-trust.keystore");
            /*
            gatewayConfig.put(PlanetPaymentGateway.CONFIG_REQUEST_KEYSTORE_TERMINAL, keyStoreTerminal);
            gatewayConfig.put(PlanetPaymentGateway.CONFIG_REQUEST_KEYSTORE_TRUSTED, keyStoreTrusted);
            */
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new VantivGateway(gatewayConfig);
    }

}
