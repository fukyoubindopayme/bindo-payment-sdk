package com.bindo.paymentsdk.v3.examples.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SimpleStorageManager {
    public static final String STORAGE_PROCESSING_RULE = "storage_processing_rule";
    public static final String STORAGE_TRANSACTION_BATCH = "storage_transaction_batch";
    public static final String STORAGE_TRANSACTION_STAN = "storage_transaction_stan";
    public static final String STORAGE_AMEX_FILE_SEQUENCE = "storage_amex_file_sequence";

    private static SharedPreferences mSharedPreferences;

    public static void init(Context context) {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static int batchNow() {
        return mSharedPreferences.getInt(STORAGE_TRANSACTION_BATCH, 1);
    }

    public static int batchNext() {
        int batchNow = mSharedPreferences.getInt(STORAGE_TRANSACTION_BATCH, 1);
        int batchNext = batchNow + 1;

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(STORAGE_TRANSACTION_BATCH, batchNext);
        editor.apply();

        return batchNext;
    }

    public static int stanNow() {
        return mSharedPreferences.getInt(STORAGE_TRANSACTION_STAN, 1);
    }

    public static int stanNext() {
        int stanNow = mSharedPreferences.getInt(STORAGE_TRANSACTION_STAN, 1);
        int stanNext = stanNow + 1;

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(STORAGE_TRANSACTION_STAN, stanNext);
        editor.apply();

        return stanNext;
    }

    public static int nextAmexFileSequence() {
        int currentValue = mSharedPreferences.getInt(STORAGE_AMEX_FILE_SEQUENCE, 1);
        int nextValue = currentValue + 1;

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(STORAGE_AMEX_FILE_SEQUENCE, nextValue);
        editor.apply();

        return nextValue;
    }

    public static SharedPreferences getSharedPreferences() {
        return mSharedPreferences;
    }
}
