package com.bindo.paymentsdk.v3.examples.utilities;

import android.content.Context;
import android.util.Log;

import com.bindo.paymentsdk.v3.pay.gateway.sic.Utils.IReadResources;

import java.io.IOException;
import java.io.InputStream;

public class Read8583ConfigFIle implements IReadResources{
    private static final String TAG = "Read8583ConfigFIle";

    private Context mContext;
    public Read8583ConfigFIle(Context context) {
       mContext = context;
    }

    @Override
    public InputStream read8583configXmlfile() {
        InputStream result = null;
        try {
            result = mContext.getAssets().open("sic_iso8583_message_config.xml");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "read8583configXmlfile: result = " + result);
        return result;
    }
}
