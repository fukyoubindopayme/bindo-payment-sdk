package com.bindo.paymentsdk.v3.examples.utilities;

import android.content.Context;

import com.landicorp.android.eptapi.DeviceService;
import com.landicorp.android.eptapi.exception.ReloginException;
import com.landicorp.android.eptapi.exception.RequestException;
import com.landicorp.android.eptapi.exception.ServiceOccupiedException;
import com.landicorp.android.eptapi.exception.UnsupportMultiProcess;

public class LandiServiceUtil {

    /**
     * 绑定联迪A8设备服务
     */
    public static void bindDeviceService(Context context) {
        try {
            DeviceService.login(context);
        } catch (ServiceOccupiedException e) {
            e.printStackTrace();
        } catch (ReloginException e) {
            e.printStackTrace();
        } catch (UnsupportMultiProcess e) {
            e.printStackTrace();
        } catch (RequestException e) {
            e.printStackTrace();
        }
    }

    /**
     * 解除绑定联迪A8设备服务
     */
    public static void unbindDeviceService() {
        DeviceService.logout();
    }
}
