package com.bindo.paymentsdk.v3.examples.screens;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import com.bindo.paymentsdk.terminal.Printer;
import com.bindo.paymentsdk.terminal.landi.APOSA8Printer;
import com.bindo.paymentsdk.terminal.urovo.I9000SPrinter;
import com.bindo.paymentsdk.v3.examples.R;
import com.bindo.paymentsdk.v3.examples.screens.base.BaseActivity;
import com.landicorp.android.eptapi.DeviceService;
import com.landicorp.android.eptapi.exception.ReloginException;
import com.landicorp.android.eptapi.exception.RequestException;
import com.landicorp.android.eptapi.exception.ServiceOccupiedException;
import com.landicorp.android.eptapi.exception.UnsupportMultiProcess;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class HtmlToReceiptActivity extends BaseActivity {
    private final String TAG = "HtmlReceiptActivity";

    private WebView mWvReceipt;

    private List<Bitmap> mHtmlToBitmapResults = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.bindDeviceService();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_html_to_receipt);

        mWvReceipt = (WebView) findViewById(R.id.wv_receipt);

        WebSettings webSettings = mWvReceipt.getSettings();
        webSettings.setMinimumFontSize(18);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setDisplayZoomControls(false);

//        mWvReceipt.loadUrl("file:///android_asset/testreceipt.html");
        mWvReceipt.loadUrl("http://blankapp.org/news/2017/09/05/v1-0-2-released/");
        mWvReceipt.requestFocus();
        mWvReceipt.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(final WebView webView, int newProgress) {
                Log.d(TAG, ">>>onProgressChanged: " + newProgress);
                if (newProgress < 100) return;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mHtmlToBitmapResults = htmlToBitmapList(mWvReceipt);
                    }
                }, 500);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_html_to_receipt, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_print) {
//            htmlToReceipt();
            printReceipt();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        this.unbindDeviceService();
        super.onDestroy();
    }

    /**
     * 绑定联迪A8设备服务
     */
    private void bindDeviceService() {
        try {
            DeviceService.login(this);
        } catch (ServiceOccupiedException e) {
            e.printStackTrace();
        } catch (ReloginException e) {
            e.printStackTrace();
        } catch (UnsupportMultiProcess e) {
            e.printStackTrace();
        } catch (RequestException e) {
            e.printStackTrace();
        }
    }

    /**
     * 解除绑定联迪A8设备服务
     */
    private void unbindDeviceService() {
        DeviceService.logout();
    }

    private void printReceipt() {
        if (mHtmlToBitmapResults == null) {
            Toast.makeText(this, "Please wait page loaded.", Toast.LENGTH_SHORT).show();
            return;
        }

        Printer.Paper paper = new Printer.Paper();
        paper.pushCommand(Printer.Paper.COMMAND_TEXT, "HTML -> IMAGE", Printer.Paper.ALIGN_CENTER, Printer.Paper.SIZE_DEFAULT);
        paper.pushCommand(Printer.Paper.COMMAND_LINE);
        paper.pushCommand(Printer.Paper.COMMAND_TEXT, "IMAGE IN HERE");
        for (int i = 0; i < mHtmlToBitmapResults.size(); i++) {
            Bitmap bitmap = mHtmlToBitmapResults.get(i);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);

            String receiptBase64String = Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
            paper.pushCommand(Printer.Paper.COMMAND_IMAGE, receiptBase64String);
        }
        paper.pushCommand(Printer.Paper.COMMAND_LINE);
        paper.pushCommand(Printer.Paper.COMMAND_CUT_PAPER);

        Printer printer = new APOSA8Printer();
        printer.setListener(new Printer.Listener() {
            @Override
            public void onPrinted() {
                Toast.makeText(HtmlToReceiptActivity.this, "Printed", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(String code, String message) {
                Toast.makeText(HtmlToReceiptActivity.this, "Error: " + message, Toast.LENGTH_SHORT).show();
            }
        });
        printer.print(paper);
    }

    private List<Bitmap> htmlToBitmapList(WebView webView) {
        List<Bitmap> bitmapList = new ArrayList<>();

        int htmlContentHeight = (int) (webView.getContentHeight() * webView.getScale());

        Bitmap viewBitmap = Bitmap.createBitmap(webView.getWidth(), htmlContentHeight, Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(viewBitmap);
        webView.draw(canvas);

        //If the view is scrolled, cut off the top part that is off the screen.
        try {
            viewBitmap = Bitmap.createBitmap(viewBitmap, 0, 0, webView.getWidth(), htmlContentHeight);

            int segmentHeight = 400;
            int segmentCount = (htmlContentHeight / segmentHeight) + ((htmlContentHeight % segmentHeight) > 0 ? 1 : 0);
            for (int i = 1; i <= segmentCount; i++) {
                int bitmapX = 0;
                int bitmapY = segmentHeight * (i - 1);
                int bitmapWidth = webView.getWidth();
                int bitmapHeight = (i < segmentCount) ? segmentHeight : (htmlContentHeight % segmentHeight);
                Bitmap bitmap = Bitmap.createBitmap(viewBitmap, bitmapX, bitmapY, bitmapWidth, bitmapHeight);
                bitmapList.add(bitmap);
            }
        } catch (Exception ex) {
            Log.e(TAG, "Could not remove top part of the WebView image.  ex=" + ex);
        }
        return bitmapList;
    }
}
