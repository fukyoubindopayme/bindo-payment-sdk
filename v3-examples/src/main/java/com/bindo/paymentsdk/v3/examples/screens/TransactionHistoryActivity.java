package com.bindo.paymentsdk.v3.examples.screens;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.bindo.paymentsdk.v3.examples.R;
import com.bindo.paymentsdk.v3.examples.database.SimpleDb;
import com.bindo.paymentsdk.v3.examples.screens.adapters.TransactionRecyclerViewAdapter;
import com.bindo.paymentsdk.v3.examples.screens.base.BaseActivity;
import com.bindo.paymentsdk.v3.examples.utilities.Constants;
import com.bindo.paymentsdk.v3.pay.common.TransactionData;
import com.bindo.paymentsdk.v3.pay.common.database.Db;

import java.util.List;

public class TransactionHistoryActivity extends BaseActivity implements TransactionRecyclerViewAdapter.OnItemClickListener {
    private final String TAG = this.getClass().getSimpleName();

    private RecyclerView mRecyclerView;
    private TransactionRecyclerViewAdapter mAdapter;

    private Db mDb;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_transaction_history);

        mDb = SimpleDb.getInstance();

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mAdapter = new TransactionRecyclerViewAdapter(this);
        mAdapter.setOnItemClickListener(this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mAdapter);

        this.loadData();
    }

    private void loadData() {
        mAdapter.getItemsSource().clear();
        mAdapter.notifyDataSetChanged();
        new Thread(new Runnable() {
            @Override
            public void run() {
                List results = mDb.table(TransactionData.class).all();

                Log.d(TAG, "----------------------------------------BG_THREAD");
                for (int i = 0; i < results.size(); i++) {
                    Object object = results.get(i);
                    TransactionData transactionData = (TransactionData) object;
                    Log.d(TAG, object.toString());

                    mAdapter.getItemsSource().add(transactionData);
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.notifyDataSetChanged();
                    }
                });
            }
        }).start();
    }

    @Override
    public void onListItemClick(View v, int position, long id) {
        TransactionData transactionData = mAdapter.getItemsSource().get(position);
        Intent intent = new Intent(this, TransactionDetailActivity.class);
        intent.putExtra(Constants.EXTRA_TRANSACTION_STAN, transactionData.getTransactionSystemTraceAuditNumber());
        startActivity(intent);
    }
}
