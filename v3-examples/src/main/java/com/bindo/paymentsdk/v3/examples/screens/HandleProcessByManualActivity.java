package com.bindo.paymentsdk.v3.examples.screens;

import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bindo.paymentsdk.payx.emv.core.Candidate;
import com.bindo.paymentsdk.payx.emv.results.CallIssuerConfirmResult;
import com.bindo.paymentsdk.payx.emv.results.OnlineProcessingResult;
import com.bindo.paymentsdk.v3.examples.R;
import com.bindo.paymentsdk.v3.examples.database.SimpleDb;
import com.bindo.paymentsdk.v3.examples.screens.base.BaseActivity;
import com.bindo.paymentsdk.v3.examples.screens.dialogs.CurrencySelectDialog;
import com.bindo.paymentsdk.v3.examples.utilities.Constants;
import com.bindo.paymentsdk.v3.examples.utilities.SimpleStorageManager;
import com.bindo.paymentsdk.v3.pay.PayProcessorListener;
import com.bindo.paymentsdk.v3.pay.common.InstallmentPlan;
import com.bindo.paymentsdk.v3.pay.common.TransactionData;
import com.bindo.paymentsdk.v3.pay.common.TransactionResultCode;
import com.bindo.paymentsdk.v3.pay.common.TransactionRetryFlag;
import com.bindo.paymentsdk.v3.pay.common.TransactionType;
import com.bindo.paymentsdk.v3.pay.common.database.Db;
import com.bindo.paymentsdk.v3.pay.common.database.models.Currency;
import com.bindo.paymentsdk.v3.pay.common.database.models.LocalBIN;
import com.bindo.paymentsdk.v3.pay.common.CardDetectMode;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Request;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Response;
import com.bindo.paymentsdk.v3.pay.common.gateway.enums.CurrencyCode;
import com.bindo.paymentsdk.v3.pay.common.legacy.CreditCard;
import com.bindo.paymentsdk.v3.pay.common.legacy.enums.CardReadMode;
import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.common.util.StringUtils;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gateway.AmexGateway;
import com.bindo.paymentsdk.v3.pay.gateway.planetpayment.PlanetPaymentGateway;

import java.io.InputStream;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
//
//KRW 5421 5800 0000 4102 01/21
//        TWD 4123 4000 0158 1111 12/25
//        JPY 4051 7000 0392 1116 12/25
//        USD 5178 6400 0000 8407 12/25

public class HandleProcessByManualActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = HandleProcessByManualActivity.class.getSimpleName();
    private Button btn_confirm;
    private TransactionData mTransactionData;
    private TransactionType mTransactionType;
    private int mTransactionSystemTraceAuditNumber;
    private double mTransactionAmount;
    private EditText etPan;
    private EditText etExpirationDate;
    private EditText etSecurityCode;
    HashMap<String, Object> configMap;
    private Date mTransactionDateAndTime;
    private String mTransactionCurrencyCode;
    private Db mDb = SimpleDb.getInstance();
    private PayProcessorListener mListener = new PayProcessorListener() {
        @Override
        public void onReset() {

        }

        @Override
        public void onRetry(TransactionRetryFlag retryFlag) {

        }

        @Override
        public void onCardDetected(CardReadMode cardReadMode, CreditCard creditCard) {

        }

        @Override
        public Candidate confirmApplicationSelection(List<Candidate> candidateList) {
            return null;
        }

        @Override
        public void onReadRecord() {

        }

        @Override
        public void onCardholderVerification() {

        }

        @Override
        public void onRateLookup(CreditCard creditCard) {

        }

        @Override
        public Currency confirmCurrencySelection(List<Currency> currencyList, double transactionAmount, String markupText) {
            int selectedIndex = 0;
            if (currencyList != null) {
                if (currencyList.size() > 1) {
                    try {
                        List<CurrencySelectDialog.CurrencySelectItemData> currencySelectItemDataList = new ArrayList<>();

                        Currency firstCurrency = currencyList.get(0);

                        for (int i = 0; i < currencyList.size(); i++) {
                            Currency currency = currencyList.get(i);
                            String title = "";
                            String summary = "";

                            if (currency.getConversionRate() != null) {
                                String sRate = Double.parseDouble(currency.getConversionRate())+"";
                                int precise = Integer.parseInt(sRate.substring(0,1));
                                double rate = Double.parseDouble(sRate.substring(1)) / Math.pow(10,precise);
                                title = currency.getAlphabeticCode() + String.format("%.2f", transactionAmount * rate);
                                summary += "FX.Rate   ";
                                summary += String.format("%.3f", 1.0 / rate) + firstCurrency.getAlphabeticCode() + "/" + currency.getAlphabeticCode();

                                if (markupText != null) {
                                    summary += "\n" + markupText;
                                }
                            } else {
                                title = currency.getAlphabeticCode() + String.format("%.2f", transactionAmount);
                            }


                            CurrencySelectDialog.CurrencySelectItemData itemData = new CurrencySelectDialog.CurrencySelectItemData(currency, title, summary);
                            currencySelectItemDataList.add(itemData);
                        }

                        selectedIndex = new CurrencySelectDialog(HandleProcessByManualActivity.this, currencySelectItemDataList).call();
                    } catch (Exception e) {
                        // Ignore
                        e.printStackTrace();
                    }
                }

                return currencyList.get(selectedIndex);
            }
            // 非 DCC 流程是不需要确认货币的
            return null;
        }

        @Override
        public List<InstallmentPlan> resolveInstallmentPlans() {
            return null;
        }

        @Override
        public InstallmentPlan confirmInstallmentPlanSelection(List<InstallmentPlan> installmentPlan) {
            return null;
        }

        @Override
        public void onPerformOnlineProcessing(CreditCard creditCard, HashMap<String, byte[]> tagLengthValues) {

        }

        @Override
        public void onPerformOnlineAuthenticationProcessing(CreditCard creditCard, HashMap<String, byte[]> tagLengthValues) {

        }

        @Override
        public void onPerformOnlineReversalProcessing(CreditCard creditCard, HashMap<String, byte[]> tagLengthValues) {

        }

        @Override
        public CallIssuerConfirmResult callIssuerConfirm() {
            return null;
        }

        @Override
        public void onCompleted(TransactionResultCode transactionResultCode, TransactionData transactionData) {

        }

        @Override
        public void onError(String code, String message) {

        }
    };
    ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_handle_process_by_manual);
        btn_confirm = (Button) findViewById(R.id.btn_ok);
        etPan = (EditText) findViewById(R.id.pan);
        etExpirationDate = (EditText) findViewById(R.id.tiet_exp_input);
        etSecurityCode = (EditText) findViewById(R.id.tiet_cvv_input);
        btn_confirm.setOnClickListener(this);

        mTransactionType = TransactionType.valueOf(getIntent().getStringExtra(Constants.EXTRA_TRANSACTION_TYPE));
        mTransactionSystemTraceAuditNumber = SimpleStorageManager.stanNext();
        mTransactionAmount = getIntent().getDoubleExtra(Constants.EXTRA_TRANSACTION_AMOUNT, 0);
        mTransactionData = new TransactionData();


        AssetManager assetManager = getAssets();
        configMap = new HashMap<>();

        try {
            KeyStore keyStore = KeyStore.getInstance("PKCS12");
            InputStream keyIS = assetManager.open("bindo.keystore");
            keyStore.load(keyIS, "123456".toCharArray());
//            KeyStore trustedKeyStore = KeyStore.getInstance("PKCS12");
//            InputStream tkeyIS = assetManager.open("ca-trust.keystore");
//            trustedKeyStore.load(tkeyIS, "123456".toCharArray());
            configMap.put(PlanetPaymentGateway.CONFIG_REQUEST_KEYSTORE_TERMINAL, keyStore);
            configMap.put(PlanetPaymentGateway.CONFIG_REQUEST_KEYSTORE_TRUSTED, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isForeignCard(String pan) {
        Db mDB = SimpleDb.getInstance();
        List<Object> localBINS = mDB.table(LocalBIN.class).all();

        if (localBINS == null) {
            return true;
        }

        for (int i = 0; i < localBINS.size(); i++) {
            LocalBIN bin = (LocalBIN) localBINS.get(i);

            if (bin.getLowPrefixNumber().compareTo(pan.substring(0, bin.getLowPrefixNumber().length())) <= 0 &&
                    bin.getHighPrefixNumber().compareTo(pan.substring(0, bin.getHighPrefixNumber().length())) >= 0) {
                return true;
            }
        }

        return false;
    }

    private OnlineProcessingResult HandleOnlinePorocess(String pan, String expirationDate, String securityCode) {
        OnlineProcessingResult onlineProcessingResult = new OnlineProcessingResult();
        this.mTransactionCurrencyCode = CurrencyCode.Hong_Kong_Dollar.getCurrencyCode();
        Request request = new Request();
        request.setCardDetectMode(CardReadMode.MANUAL.name());
        request.setPrimaryAccountNumber(pan);
        request.setCardExpirationDate(expirationDate);
        request.setCardSecurityCode(securityCode);
        request.setTransactionType(mTransactionType);
        request.setTransactionAmount(mTransactionAmount);
        request.setTransactionSystemTraceAuditNumber(mTransactionSystemTraceAuditNumber);
        AmexGateway gateway = new AmexGateway(null);

        mTransactionDateAndTime = new Date();

        // Transaction fields.
        request.setTransactionType(mTransactionType);
        request.setTransactionSystemTraceAuditNumber(mTransactionSystemTraceAuditNumber);
        request.setTransactionDateAndTime(mTransactionDateAndTime);
        request.setTransactionAmount(mTransactionAmount);
        request.setTransactionCurrencyCode(mTransactionCurrencyCode);

        // Card fields.
        String cardDetectMode = CardDetectMode.MANUAL;
        request.setCardDetectMode(cardDetectMode);

//        if (isForeignCard(request.getPrimaryAccountNumber())) {
//            request.setPlanetPaymentField63CurrencyConversionIndicator(Hex.encode("D".getBytes()));
//            request.setPlanetPaymentField63PSCIndicator(Hex.encode("P".getBytes()));
//        }
//
//        // DCC - Rate Lookup
//        request.setPlanetPaymentField63CurrencyConversionIndicator(Hex.encode("D".getBytes()));
//        request.setPlanetPaymentField63OptOutFlag(Hex.encode("N".getBytes()));
//        request.setPlanetPaymentField63RateLookupIndicator(Hex.encode("R".getBytes()));
//        request.setPlanetPaymentField63RequestForAdditionalResponse("37");
//
//        request.setDCCTransactionType(DCCTransactionType.RATE_LOOKUP);
//        Response responseForDCCRateLookup = gateway.sendRequest(request);

        Response responseForDCCRateLookup = null;
        Response responseForAuthorization = null;

        // DCC 流程
        if (responseForDCCRateLookup != null) {
//            // PYC Flow - Pay in your currency
//            // DCC - Authorization
//            request.setPlanetPaymentField63RateLookupIndicator("");
//            request.setCardholderBillingCurrencyCode(responseForDCCRateLookup.getCardholderBillingCurrencyCode().substring(1));
//            request.setDCCTransactionType(DCCTransactionType.AUTHORIZATION);
//            Response responseForDCCAuthorization = gateway.sendRequest(request);
//            final Request dbRequest = request;
//            if (responseForDCCAuthorization.isApproved()) {
//                // 确认货币
//                List<Currency> currencies = responseForDCCRateLookup.getPlanetPaymentCurrencies();
//                Currency localCurrency = new Currency();
//                localCurrency.setAlphabeticCode("HKD");
//                localCurrency.setNumericCode(request.getTransactionCurrencyCode());
//                currencies.add(0, localCurrency);
//
//                Currency selectedCurrency = mListener.confirmCurrencySelection(currencies, request.getTransactionAmount(),
//                        responseForDCCAuthorization.getTransactionPYCRateMarkupText());
//
//                if (request.getTransactionCurrencyCode().equals(selectedCurrency.getNumericCode())) {
//                    // OPT-OUT
//                    mTransactionData.setTransactionOptOut(true);
//                    // DCC - Authorization Void
//                    request.setDCCTransactionType(DCCTransactionType.AUTHORIZATION_VOID);
//                    Response responseForDCCAuthorizationVoid = gateway.sendRequest(request);
//
////                    mListener.onPerformOnlineAuthenticationProcessing(creditCard, tagLengthValues);
//                    // Authorization Or Refund.
//                    request.setTransactionType(mTransactionType);
//                    request.setDCCTransactionType(null);
//                    responseForAuthorization = gateway.sendRequest(request);
//
//                    if (responseForAuthorization != null && responseForAuthorization.isApproved()) {
//                        onlineProcessingResult.setApproved(true);
//                    }
//
//                } else {
//                    // OPT-IN
//                    mTransactionData.setTransactionOptOut(false);
//                    onlineProcessingResult.setApproved(true);
//                }
//            } else {
//                onlineProcessingResult.setApproved(false);
//            }
            // 普通流程
        } else {
//            mListener.onPerformOnlineAuthenticationProcessing(creditCard, tagLengthValues);
            // Authorization Or Refund.
            request.setTransactionType(mTransactionType);
//            request.setDCCTransactionType(null);
            responseForAuthorization = gateway.sendRequest(request);

            if (responseForAuthorization != null) {
                onlineProcessingResult.setApproved(responseForAuthorization.isApproved());
                onlineProcessingResult.setApprovalCode(responseForAuthorization.getApprovalCode());
                onlineProcessingResult.setIccRelatedDataIssuerScriptData(responseForAuthorization.getIccRelatedDataIssuerScriptData());
                onlineProcessingResult.setIccRelatedDataIssuerAuthenticationData(responseForAuthorization.getIccRelatedDataIssuerAuthenticationData());
                Log.d(TAG, responseForAuthorization.toString());
            }
        }


        mTransactionData.setTransactionType(mTransactionType.name());
        mTransactionData.setTransactionSystemTraceAuditNumber(mTransactionSystemTraceAuditNumber);
        mTransactionData.setTransactionDateAndTime(mTransactionDateAndTime);
        mTransactionData.setTransactionAmount(mTransactionAmount);

        mTransactionData.setPrimaryAccountNumber(pan);
        mTransactionData.setCardExpirationDate(expirationDate);
        mTransactionData.setTransactionId(responseForAuthorization.getTransactionId());
        mTransactionData.setTransactionCurrencyCode(CurrencyCode.Hong_Kong_Dollar.getCurrencyCode());

        if (responseForAuthorization != null) {
            mTransactionData.setApproved(onlineProcessingResult.isApproved());
            mTransactionData.setActionCode(responseForAuthorization.getActionCode());
            mTransactionData.setAuthorizationCode(responseForAuthorization.getAuthorizationCode());
            mTransactionData.setRetrievalReferenceNumber(responseForAuthorization.getRetrievalReferenceNumber());
            mTransactionData.setTransactionPYCRateMarkupSign(responseForAuthorization.getTransactionPYCRateMarkupSign());
            mTransactionData.setTransactionPYCRateMarkupText(responseForAuthorization.getTransactionPYCRateMarkupText());
            mTransactionData.setApprovalCode(responseForAuthorization.getApprovalCode());
            mTransactionData.setPosDataCode(responseForAuthorization.getPosDataCode());
        }
        if (responseForAuthorization.getIsApproved()) {
            if (mDb != null) {
                mDb.table(TransactionData.class).create(mTransactionData);
            }
        }
        return onlineProcessingResult;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_ok:
                final String pan = etPan.getText().toString();
                final String expirationDate = etExpirationDate.getText().toString();
                final String securityCode = etSecurityCode.getText().toString();

                if (StringUtils.isEmpty(pan)) {
                    Toast.makeText(HandleProcessByManualActivity.this, "Pls input pan", Toast.LENGTH_LONG).show();
                    return;
                }

                if (StringUtils.isEmpty(expirationDate)) {
                    Toast.makeText(HandleProcessByManualActivity.this, "Pls input expiration date", Toast.LENGTH_LONG).show();
                    return;
                }

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        HandleOnlinePorocess(pan, expirationDate, securityCode);
                    }
                }).start();


                break;
        }
    }
}
