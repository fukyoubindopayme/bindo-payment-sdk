package com.bindo.paymentsdk.v3.examples.utilities;

public class CurrencyCodeToSymbolUtil {
    public static String convert(String currencyCode) {
        String currencySymbol = "Unknown";
        switch (currencyCode) {
            case "344": // 港币
                currencySymbol = "HK$";
                break;
            case "840": // 美元
                currencySymbol = "$";
                break;
            case "156": // 人民币
                currencySymbol = "￥";
                break;
        }

        return currencySymbol;
    }
}
