package com.bindo.paymentsdk.v3.examples.screens.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.WindowManager;

import com.bindo.paymentsdk.v3.examples.R;
import com.bindo.paymentsdk.v3.pay.common.InstallmentPlan;

import java.util.List;
import java.util.concurrent.Callable;

public class InstallmentPlanSelectDialog implements Callable<Integer> {

    private int mSelectedIndex = -1;
    private boolean mWaitingConfirm = true;
    private AlertDialog mSelectDialog;
    private AlertDialog mSelectConfirmDialog;

    public InstallmentPlanSelectDialog(final Context context, final List<InstallmentPlan> installmentPlans) {
        final String[] items = new String[installmentPlans.size()];
        for (int i = 0; i < installmentPlans.size(); i++) {
            InstallmentPlan installmentPlan = installmentPlans.get(i);
            items[i] = installmentPlan.getName();
        }

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AppTheme_Dialog);
                builder.setCancelable(false);
                builder.setTitle(R.string.dialog_installment_plan_select_title);
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mSelectedIndex = which;
                    }
                });
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            // 如果用户不需要而关闭对话框，默认选择第一个
                            if (mSelectedIndex == -1) {
                                mSelectedIndex = 0;
                            }

                            AlertDialog.Builder confirmDialogBuilder = new AlertDialog.Builder(context, R.style.AppTheme_Dialog);
                            confirmDialogBuilder.setCancelable(false);
                            confirmDialogBuilder.setTitle(R.string.dialog_installment_plan_select_title);
                            confirmDialogBuilder.setMessage(R.string.dialog_installment_plan_select_confirm_message);
                            confirmDialogBuilder.setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // FIRE ZE MISSILES!
                                    mWaitingConfirm = false;
                                }
                            });
                            confirmDialogBuilder.setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    mSelectedIndex = -1;
                                    mWaitingConfirm = false;
                                }
                            });

                            mSelectConfirmDialog = confirmDialogBuilder.create();
                            if (mSelectConfirmDialog.getWindow() != null) {
                                mSelectConfirmDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                            }
                            mSelectConfirmDialog.show();
                        }
                    });
                }
                mSelectDialog = builder.create();
                if (mSelectDialog.getWindow() != null) {
                    mSelectDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                }
                mSelectDialog.show();
            }
        });
    }

    @Override
    public Integer call() throws Exception {
        while (mWaitingConfirm) {
            Thread.sleep(500);
        }

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if (mSelectDialog != null && mSelectDialog.isShowing()) {
                    mSelectDialog.dismiss();
                    mSelectDialog = null;
                }
                if (mSelectConfirmDialog != null && mSelectConfirmDialog.isShowing()) {
                    mSelectConfirmDialog.dismiss();
                    mSelectConfirmDialog = null;
                }
            }
        });

        return mSelectedIndex;
    }
}
