package com.bindo.paymentsdk.v3.examples.screens.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.view.WindowManager;

import com.bindo.paymentsdk.payx.emv.results.CallIssuerConfirmResult;
import com.bindo.paymentsdk.v3.examples.R;

import java.util.concurrent.Callable;

public class CallIssuerConfirmDialog  implements Callable<CallIssuerConfirmResult> {

    private CallIssuerConfirmResult callIssuerConfirmResult = CallIssuerConfirmResult.WAITING;
    private AlertDialog mAlertDialog;

    public CallIssuerConfirmDialog(final Context context) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AppTheme_Dialog);
                builder.setCancelable(false);
                builder.setMessage(R.string.dialog_call_issuer_confirm_message);
                builder.setPositiveButton(R.string.button_accept, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callIssuerConfirmResult = CallIssuerConfirmResult.ACCEPT;
                    }
                });

                builder.setNegativeButton(R.string.button_reject, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callIssuerConfirmResult = CallIssuerConfirmResult.REJECT;
                    }
                });

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            // 如果用户不需要而关闭对话框，默认选择第一个
                            if (callIssuerConfirmResult == CallIssuerConfirmResult.WAITING) {
                                callIssuerConfirmResult = CallIssuerConfirmResult.NO_NEED;
                            }
                        }
                    });
                }
                mAlertDialog = builder.create();
                if (mAlertDialog.getWindow() != null) {
                    mAlertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                }
                mAlertDialog.show();
            }
        });
    }

    @Override
    public CallIssuerConfirmResult call() throws Exception {
        while (callIssuerConfirmResult == CallIssuerConfirmResult.WAITING) {
            Thread.sleep(500);
        }

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if (mAlertDialog != null && mAlertDialog.isShowing()) {
                    mAlertDialog.dismiss();
                    mAlertDialog = null;
                }
            }
        });

        return callIssuerConfirmResult;
    }
}
