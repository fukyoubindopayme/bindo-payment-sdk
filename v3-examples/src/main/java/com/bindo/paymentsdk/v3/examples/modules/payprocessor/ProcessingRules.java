package com.bindo.paymentsdk.v3.examples.modules.payprocessor;

public class ProcessingRules {
    public static final int Fake = 0;
    public static final int American_Express = 1;
    public static final int Bank_Of_East_Asia = 2;
    public static final int Bank_Of_East_Asia_With_DCC = 3;
    public static final int Planet_Payment = 4;
    public static final int Vantiv = 5;
}
