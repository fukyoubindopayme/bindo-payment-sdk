package com.bindo.paymentsdk.v3.examples.screens;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bindo.paymentsdk.payx.emv.core.Application;
import com.bindo.paymentsdk.payx.emv.core.CAPK;
import com.bindo.paymentsdk.payx.emv.core.Candidate;
import com.bindo.paymentsdk.payx.emv.results.CallIssuerConfirmResult;
import com.bindo.paymentsdk.terminal.Printer;
import com.bindo.paymentsdk.terminal.Terminal;
import com.bindo.paymentsdk.v3.examples.R;
import com.bindo.paymentsdk.v3.examples.commons.AMEXReceipt;
import com.bindo.paymentsdk.v3.examples.database.SimpleDb;
import com.bindo.paymentsdk.v3.examples.modules.payprocessor.PayProcessorSimpleGateway;
import com.bindo.paymentsdk.v3.examples.modules.payprocessor.ProcessingRules;
import com.bindo.paymentsdk.v3.examples.screens.base.BaseActivity;
import com.bindo.paymentsdk.v3.examples.screens.dialogs.AppSelectDialog;
import com.bindo.paymentsdk.v3.examples.screens.dialogs.CallIssuerConfirmDialog;
import com.bindo.paymentsdk.v3.examples.screens.dialogs.CurrencySelectDialog;
import com.bindo.paymentsdk.v3.examples.screens.dialogs.InstallmentPlanSelectDialog;
import com.bindo.paymentsdk.v3.examples.utilities.Constants;
import com.bindo.paymentsdk.v3.examples.utilities.SimpleStorageManager;
import com.bindo.paymentsdk.v3.pay.PayProcessor;
import com.bindo.paymentsdk.v3.pay.PayProcessorFactory;
import com.bindo.paymentsdk.v3.pay.PayProcessorListener;
import com.bindo.paymentsdk.v3.pay.TerminalFactory;
import com.bindo.paymentsdk.v3.pay.common.InstallmentPlan;
import com.bindo.paymentsdk.v3.pay.common.TransactionData;
import com.bindo.paymentsdk.v3.pay.common.TransactionResultCode;
import com.bindo.paymentsdk.v3.pay.common.TransactionRetryFlag;
import com.bindo.paymentsdk.v3.pay.common.TransactionType;
import com.bindo.paymentsdk.v3.pay.common.database.models.Currency;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Request;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Response;
import com.bindo.paymentsdk.v3.pay.common.legacy.CreditCard;
import com.bindo.paymentsdk.v3.pay.common.legacy.enums.CardReadMode;
import com.bindo.paymentsdk.v3.pay.common.util.Hex;
import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.common.util.TagLengthValuesUtils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class HandleProcessByTerminalActivity extends BaseActivity {
    private final String TAG = "HandleProcessByTerminal";

    private final float DEFAULT_TERMINAL_FLOOR_LIMIT = 150;
    //private final float DEFAULT_TERMINAL_FLOOR_LIMIT = 0; // online only
//    // AMEX use it.
    private final float DEFAULT_TERMINAL_CONTACTLESS_FLOOR_LIMIT = 0;
    private final float DEFAULT_TERMINAL_CONTACTLESS_CARDHOLDER_VERIFICATION_LIMIT = 0;
    private final float DEFAULT_TERMINAL_CONTACTLESS_TRANSACTION_LIMIT = 0;
    //private final float DEFAULT_TERMINAL_CONTACTLESS_TRANSACTION_LIMIT = 10000; // online only

    /*
    private final float DEFAULT_TERMINAL_CONTACTLESS_FLOOR_LIMIT = 0;
    private final float DEFAULT_TERMINAL_CONTACTLESS_CARDHOLDER_VERIFICATION_LIMIT = 30;
    private final float DEFAULT_TERMINAL_CONTACTLESS_TRANSACTION_LIMIT = 200;
    */

    /*
    // amount < this, stay offline
    private final float AMEX_DEFAULT_TERMINAL_FLOOR_LIMIT = 6;
    // amount >= this, go online, no cvm
    private final float AMEX_DEFAULT_TERMINAL_CONTACTLESS_FLOOR_LIMIT = 10;
    // amount >= this, request CVM (pin or signature) and go online
    private final float AMEX_DEFAULT_TERMINAL_CONTACTLESS_CARDHOLDER_VERIFICATION_LIMIT = 12;
    // amount >= this, don't allow contactless
    private final float AMEX_DEFAULT_TERMINAL_CONTACTLESS_TRANSACTION_LIMIT = 15;
    */

    private TextView mTvAmount;
    private ImageView mIvResultIcon;
    private ImageView mIvProcessingIcon;
    private TextView mTvMessage;
    private LinearLayout mLlTransButtonsRetry;
    private Button mBtnRetry;
    private TextView mTvLog;
    private CardReadMode mCardReadMode;

    private StringBuilder mLogContent = new StringBuilder();

    private TransactionData mTransactionData;
    private TransactionType mTransactionType;
    private int mTransactionSystemTraceAuditNumber;
    private Date mTransactionDateAndTime;
    private double mTransactionAmount;

    private AMEXReceipt amexReceipt;

    // Save paper please!
    final boolean disableReceiptsPrinting = true;

    private Terminal mTerminal;
    private PayProcessor mPayProcessor;
    private PayProcessorSimpleGateway mPayProcessorGateway;
    private PayProcessorSimpleGateway mPYCPayProcessorGateway;
    private PayProcessorListener mPayProcessorListener = new PayProcessorListener() {
        @Override
        public void onReset() {
            logDebug(">>>onReset");
            switchToUiStatusProcessing(R.mipmap.creditcard_in_trans, R.string.message_present_card);
        }

        @Override
        public void onRetry(TransactionRetryFlag retryFlag) {
            logDebug(">>>onRetry");
            logDebug("\t" + retryFlag.name());
            switch (retryFlag) {
                case SEE_PHONE_FOR_INSTRUCTIONS:
                    switchToUiStatusProcessing(R.mipmap.creditcard_see_phone, R.string.message_see_phone_for_instructions);
                    break;
                case FORCE_USE_CHIP:
                    switchToUiStatusProcessing(R.mipmap.creditcard_please_insert, R.string.message_please_insert_card);
                    break;
                case FORCE_USE_MAG:
                    switchToUiStatusProcessing(R.mipmap.creditcard_please_swipe, R.string.message_please_swipe_card);
                    break;
                case FORCE_USE_CHIP_AND_MAG:
                    switchToUiStatusProcessing(R.mipmap.creditcard_in_trans, R.string.message_please_insert_or_swipe_card);
                    break;
                case DEFAULT:
                    switchToUiStatusProcessing(R.mipmap.creditcard_in_trans, R.string.message_please_retry_insert_card);
                    break;
            }

        }

        @Override
        public void onCardDetected(CardReadMode cardReadMode, CreditCard creditCard) {
            logDebug(">>>onCardDetected");
            logDebug("\t" + cardReadMode.name());
            mCardReadMode = cardReadMode;
//            switch (cardReadMode) {
//                case CONTACTLESS:
//                    switchToUiStatusProcessing(R.mipmap.creditcard_detected_rf, R.string.message_card_is_detected_conactless);
//                    break;
//                default:
//                    switchToUiStatusProcessing(R.mipmap.creditcard_detected, R.string.message_card_is_detected);
//                    break;
//            }
        }

        @Override
        public Candidate confirmApplicationSelection(List<Candidate> candidateList) {
            logDebug(">>>confirmApplicationSelection");
            logDebug("\tCandidate Size: " + candidateList.size());
            switchToUiStatusProcessing(R.mipmap.creditcard_in_trans, R.string.message_please_confirm_application);

            int selectedIndex = 0;
            if (candidateList.size() > 1) {
                try {
                    selectedIndex = new AppSelectDialog(HandleProcessByTerminalActivity.this, candidateList).call();
                } catch (Exception e) {
                    // Ignore
                }
            }
            return candidateList.get(selectedIndex);
        }

        @Override
        public void onReadRecord() {
            switch (mCardReadMode) {
                case CONTACTLESS:
                    switchToUiStatusProcessing(R.mipmap.creditcard_detected_rf, R.string.message_card_is_detected_conactless);
                    break;
                default:
                    switchToUiStatusProcessing(R.mipmap.creditcard_detected, R.string.message_card_is_detected);
                    break;
            }
        }

        @Override
        public void onCardholderVerification() {
            logDebug(">>>onCardholderVerification");
            switchToUiStatusProcessing(R.mipmap.creditcard_in_trans, R.string.message_please_entry_pin);
        }

        @Override
        public void onRateLookup(CreditCard creditCard) {
            logDebug(">>>onRateLookup");
            switchToUiStatusProcessing(R.mipmap.creditcard_in_trans, R.string.message_perform_online_rate_lookup_processing);
        }

        /*
        @Override
        public Currency confirmCurrencySelection(List<Currency> currencyList, double transactionAmount, String markupText) {
            logDebug(">>>confirmCurrencySelection");
            int selectedIndex = 0;
            if (currencyList != null) {
                if (currencyList.size() > 1) {
                    try {
                        List<CurrencySelectDialog.CurrencySelectItemData> currencySelectItemDataList = new ArrayList<>();

                        Currency firstCurrency = currencyList.get(0);

                        for (int i = 0; i < currencyList.size(); i++) {
                            Currency currency = currencyList.get(i);
                            String title = "";
                            String summary = "";

                            if (currency.getConversionRate() != null) {
                                String sRate = Double.parseDouble(currency.getConversionRate()) + "";
                                int precise = Integer.parseInt(sRate.substring(0, 1));
                                double rate = Double.parseDouble(sRate.substring(1)) / Math.pow(10, precise);
                                title = currency.getAlphabeticCode() + String.format("%.2f", transactionAmount * rate);
                                summary += "FX.Rate   ";
                                summary += String.format("%.3f", 1.0 / rate) + firstCurrency.getAlphabeticCode() + "/" + currency.getAlphabeticCode();

                                if (markupText != null) {
                                    summary += "\n" + markupText;
                                }
                            } else {
                                title = currency.getAlphabeticCode() + String.format("%.2f", transactionAmount);
                            }


                            CurrencySelectDialog.CurrencySelectItemData itemData = new CurrencySelectDialog.CurrencySelectItemData(currency, title, summary);
                            currencySelectItemDataList.add(itemData);
                        }

                        selectedIndex = new CurrencySelectDialog(HandleProcessByTerminalActivity.this, currencySelectItemDataList).call();
                    } catch (Exception e) {
                        // Ignore
                        e.printStackTrace();
                    }
                }

                return currencyList.get(selectedIndex);
            }
            // 非 DCC 流程是不需要确认货币的
            return null;
        }
        */

        @Override
        public Currency confirmCurrencySelection(List<Currency> currencyList, double transactionAmount, String markupText) {
            logDebug(">>>confirmCurrencySelection");
            int selectedIndex = 0;
            if (currencyList != null) {
                if (currencyList.size() > 1) {
                    try {
                        List<CurrencySelectDialog.CurrencySelectItemData> currencySelectItemDataList = new ArrayList<>();

                        Currency firstCurrency = currencyList.get(0);

                        for (int i = 0; i < currencyList.size(); i++) {
                            Currency currency = currencyList.get(i);
                            String title = "";
                            String summary = "";

                            if (currency.getConversionRate() != null) {
                                String sRate = Double.parseDouble(currency.getConversionRate()) + "";
                                int precise = Integer.parseInt(sRate.substring(0, 1));
                                double rate = Double.parseDouble(sRate.substring(1)) / Math.pow(10, precise);
                                title = currency.getAlphabeticCode() + String.format("%.2f", transactionAmount * rate);
                                summary += "FX.Rate   ";
                                summary += String.format("%.3f", 1.0 / rate) + firstCurrency.getAlphabeticCode() + "/" + currency.getAlphabeticCode();

                                if (markupText != null) {
                                    summary += "\n" + markupText;
                                }
                            } else {
                                title = currency.getAlphabeticCode() + String.format("%.2f", transactionAmount);
                            }


                            CurrencySelectDialog.CurrencySelectItemData itemData = new CurrencySelectDialog.CurrencySelectItemData(currency, title, summary);
                            currencySelectItemDataList.add(itemData);
                        }

                        selectedIndex = new CurrencySelectDialog(HandleProcessByTerminalActivity.this, currencySelectItemDataList).call();
                    } catch (Exception e) {
                        // Ignore
                        e.printStackTrace();
                    }
                }

                return currencyList.get(selectedIndex);
            }
            // 非 DCC 流程是不需要确认货币的
            return null;
        }

        @Override
        public List<InstallmentPlan> resolveInstallmentPlans() {
            List<InstallmentPlan> installmentPlans = new ArrayList<>();
            installmentPlans.add(new InstallmentPlan("3 Months", null, 3));
            installmentPlans.add(new InstallmentPlan("6 Months", null, 6));
            installmentPlans.add(new InstallmentPlan("9 Months", null, 9));
            installmentPlans.add(new InstallmentPlan("12 Months", null, 12));
            installmentPlans.add(new InstallmentPlan("24 Months", null, 24));
            installmentPlans.add(new InstallmentPlan("36 Months", null, 36));
            return installmentPlans;
        }

        @Override
        public InstallmentPlan confirmInstallmentPlanSelection(List<InstallmentPlan> installmentPlans) {
            logDebug(">>>confirmInstallmentPlanSelection");
            logDebug("\tCandidate Size: " + installmentPlans.size());
            switchToUiStatusProcessing(R.mipmap.creditcard_in_trans, R.string.message_please_confirm_installment_plans);

            int selectedIndex = 0;
            if (installmentPlans.size() > 1) {
                try {
                    selectedIndex = new InstallmentPlanSelectDialog(HandleProcessByTerminalActivity.this, installmentPlans).call();
                } catch (Exception e) {
                    // Ignore
                }
            }
            if (selectedIndex >= 0) {
                return installmentPlans.get(selectedIndex);
            } else {
                return null;
            }
        }

        @Override
        public void onPerformOnlineProcessing(CreditCard creditCard, HashMap<String, byte[]> tagLengthValues) {
            logDebug(">>>onPerformOnlineProcessing");
            logDebug("\tPAN: " + creditCard.getCardNumber());
            mTransactionData.setTagLengthValues(tagLengthValues);
        }

        @Override
        public void onPerformOnlineAuthenticationProcessing(CreditCard creditCard, HashMap<String, byte[]> tagLengthValues) {
            logDebug(">>>onPerformOnlineAuthenticationProcessing");
            logDebug("\tPAN: " + creditCard.getCardNumber());
            switchToUiStatusProcessing(R.mipmap.creditcard_in_trans, R.string.message_perform_online_authentication_processing);
        }

        @Override
        public void onPerformOnlineReversalProcessing(CreditCard creditCard, HashMap<String, byte[]> tagLengthValues) {
            logDebug(">>>onPerformOnlineReversalProcessing");
            logDebug("\tPAN: " + creditCard.getCardNumber());
            switchToUiStatusProcessing(R.mipmap.creditcard_in_trans, R.string.message_perform_online_reversal_processing);
        }

        @Override
        public CallIssuerConfirmResult callIssuerConfirm() {
            logDebug(">>>callIssuerConfirm");
            try {
                return new CallIssuerConfirmDialog(HandleProcessByTerminalActivity.this).call();
            } catch (Exception e) {
                e.printStackTrace();
            }
            // 默认拒绝需要联系发卡行的授权请求
            return CallIssuerConfirmResult.REJECT;
        }

        @Override
        public void onCompleted(TransactionResultCode transactionResultCode, TransactionData transactionData) {
            logDebug(">>>onCompleted");
            HashMap<String, byte[]> tagLengthValue = transactionData.getTagLengthValues();
            //logDebug("\t" + transactionResultCode.name());
            logDebug("\t TVR:" + mTransactionData.getTerminalVerificationResult());
            logDebug("\t TSI:" + mTransactionData.getTransactionStatusInformation());
            switchToUiStatusError(R.string.message_error_unknown);

            logDebug("\tactionCode: "+ transactionData.getActionCode());
            logDebug("\tactionCodeDescription: "+ transactionData.getActionCodeDescription());

            // 测试的 EMV 数据保存
            if (transactionData.getRequest() != null) {
                Request request = transactionData.getRequest();
                if (request != null) {
                    request.setTagLengthStringValues(TagLengthValuesUtils.convertToHashMapStringString(request.getTagLengthValues()));
                    request.setTagLengthValues(null);
                }

                Gson gson = new Gson();
                //Log.d("AMEX_EMV_DATA", gson.toJson(request));
                //Log.d("AMEX_EMV_DATA", "----------------------------------------");
            }

            if (transactionData.getResponse() != null) {
                Response response = transactionData.getResponse();
                if (response != null) {
                    response.setTagLengthStringValues(TagLengthValuesUtils.convertToHashMapStringString(response.getTagLengthValues()));
                    response.setTagLengthValues(null);
                }

                Gson gson = new Gson();
                Log.d("AMEX_EMV_DATA_RES", gson.toJson(response));
                Log.d("AMEX_EMV_DATA_RES", "----------------------------------------");
            }

            if (transactionResultCode == TransactionResultCode.DECLINED_BY_TIMEOUT_AND_REVERSED ||
                    transactionResultCode == TransactionResultCode.DECLINED_BY_TERMINAL_AND_REVERSED) {
                SimpleStorageManager.stanNext();
            }

            if (transactionData.getRequest() == null) {
                Request request = new Request();
                HashMap<String, String> gatewayConfigs = new HashMap<>();

                request.setGatewayConfigs(gatewayConfigs);
            }

            switch (transactionResultCode) {
                case APPROVED_BY_OFFLINE:
                case APPROVED_BY_ONLINE:
                    switchToUiStatusSuccess(R.string.message_approved);
                    if (!disableReceiptsPrinting) {printReceipt(transactionData);}
                    break;
                case DECLINED_BY_OFFLINE:
                case DECLINED_BY_ONLINE:
                case DECLINED_BY_TIMEOUT_AND_REVERSED:
                case DECLINED_BY_TERMINAL_AND_REVERSED:
                    if (transactionData.getResponse() != null) {
                        if (transactionData.getResponse().getIsNeedCallIssuerConfirm()) {
                            switchToUiStatusError(R.string.message_call_issuer);
                            if (!disableReceiptsPrinting) {printDeclinedReceipt(transactionData);}
                            break;
                        }
                    }
                    switchToUiStatusError(R.string.message_declined);
                    if (!disableReceiptsPrinting) {printDeclinedReceipt(transactionData);}
                    break;
                case ERROR_CARD_DETECT_TIMEOUT:
                    switchToUiStatusError(R.string.message_error_card_detect_timeout);
                    break;
                case ERROR_CARD_DETECT_MULTIPLE_CARDS:
                    switchToUiStatusError(R.string.message_error_card_detect_multiple_cards);
                    break;
                case ERROR_CARD_NOT_ACCEPTED:
                    switchToUiStatusProcessing(R.mipmap.creditcard_blcoked, R.string.message_error_card_not_accepted);
                    break;
                case ERROR_CARD_BLOCKED:
                    switchToUiStatusProcessing(R.mipmap.creditcard_blcoked, R.string.message_error_card_is_blocked);
                    break;
//                case ERROR_SEE_PHONE_FOR_INSTRUCTIONS:
//                    switchToUiStatusError(R.string.message_see_phone_for_instructions);
//                    break;
//                case ERROR_TRY_ANOTHER_INTERFACE:
//                    switchToUiStatusError(R.string.message_please_insert_card);
//                    break;
                case ERROR_UNKNOWN:
                default:
                    switchToUiStatusError(R.string.message_error_terminated);
                    if (!disableReceiptsPrinting) {printDeclinedReceipt(transactionData);}
                    break;
            }
//            // SETTLEMENT 数据保存
//            if (transactionResultCode == TransactionResultCode.APPROVED_BY_OFFLINE ||
//                transactionResultCode == TransactionResultCode.APPROVED_BY_ONLINE) {
//
//                Request request = transactionData.getRequest();
//                Response response = transactionData.getResponse();
//
//                if (transactionData.getResponse() != null) {
//                    transactionData.setTagLengthStringValues(TagLengthValuesUtils.convertToHashMapStringString(transactionData.getRequest().getTagLengthValues()));
//                } else {
//                    transactionData.setTagLengthStringValues(TagLengthValuesUtils.convertToHashMapStringString(transactionData.getTagLengthValues()));
//                }
//
//                if (request != null) {
//                    request.setTagLengthStringValues(TagLengthValuesUtils.convertToHashMapStringString(request.getTagLengthValues()));
//                    request.setTagLengthValues(null);
//                }
//                if (response != null) {
//                    response.setTagLengthStringValues(TagLengthValuesUtils.convertToHashMapStringString(response.getTagLengthValues()));
//                    response.setTagLengthValues(null);
//                }
//
//                transactionData.setRequest(null);
//                transactionData.setResponse(null);
//                transactionData.setTagLengthValues(null);
//
//                Gson gson = new Gson();
//                Log.d("AMEX_EMV_DATA", gson.toJson(request));
//                Log.d("AMEX_EMV_DATA", "----------------------------------------");
////                Log.d("AMEX_SETTLEMENT", gson.toJson(response));
////                Log.d("AMEX_SETTLEMENT", gson.toJson(transactionData));
////                Log.d("AMEX_SETTLEMENT", "----------------------------------------");
//            }
        }

        @Override
        public void onError(String code, String message) {
            logDebug(">>>onError");
            logDebug("\tCode: " + code);
            logDebug("\tMessage: " + message);
            switchToUiStatusError(message);
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_handle_process_by_terminal);

        mTransactionType = TransactionType.valueOf(getIntent().getStringExtra(Constants.EXTRA_TRANSACTION_TYPE));
        mTransactionSystemTraceAuditNumber = SimpleStorageManager.stanNext();
        mTransactionAmount = getIntent().getDoubleExtra(Constants.EXTRA_TRANSACTION_AMOUNT, 0);

        mTransactionData = new TransactionData();

        // 支付处理器应用（EMV Application）配置
        List<Application> applications = new ArrayList<>();
        CAPK capkAmex0xc8 = new CAPK.Builder()
                .setHeader((byte) 0x20)
                .setServiceIdentifier(new byte[]{0x00, 0x00, 0x00, 0x00})
                .setLengthOfCAPKModulus((byte) 0x90)
                .setCAPKAlgorithmIndicator((byte) 0x01)
                .setLengthOfCAPKExponent((byte) 0x01)
                .setRID(new byte[]{(byte) 0xa0, 0x00, 0x00, 0x00, 0x25})
                .setCAPKIndex((byte) 0xc8)
                .setCAPKModulus(Hex.decode("BF0CFCED708FB6B048E3014336EA24AA007D7967B8AA4E613D26D015C4FE7805D9DB131CED0D2A8ED504C3B5CCD48C33199E5A5BF644DA043B54DBF60276F05B1750FAB39098C7511D04BABC649482DDCF7CC42C8C435BAB8DD0EB1A620C31111D1AAAF9AF6571EEBD4CF5A08496D57E7ABDBB5180E0A42DA869AB95FB620EFF2641C3702AF3BE0B0C138EAEF202E21D"))
                .setCAPKExponent(new byte[]{0x03})
                .setHashValue(Hex.decode("33BD7A059FAB094939B90A8F35845C9DC779BD50"))
                .build();
        CAPK capkAmex0xc9 = new CAPK.Builder()
                .setHeader((byte) 0x20)
                .setServiceIdentifier(new byte[]{0x00, 0x00, 0x00, 0x00})
                .setLengthOfCAPKModulus((byte) 0xb0)
                .setCAPKAlgorithmIndicator((byte) 0x01)
                .setLengthOfCAPKExponent((byte) 0x01)
                .setRID(new byte[]{(byte) 0xa0, 0x00, 0x00, 0x00, 0x25})
                .setCAPKIndex((byte) 0xc9)
                .setCAPKModulus(Hex.decode("B362DB5733C15B8797B8ECEE55CB1A371F760E0BEDD3715BB270424FD4EA26062C38C3F4AAA3732A83D36EA8E9602F6683EECC6BAFF63DD2D49014BDE4D6D603CD744206B05B4BAD0C64C63AB3976B5C8CAAF8539549F5921C0B700D5B0F83C4E7E946068BAAAB5463544DB18C63801118F2182EFCC8A1E85E53C2A7AE839A5C6A3CABE73762B70D170AB64AFC6CA482944902611FB0061E09A67ACB77E493D998A0CCF93D81A4F6C0DC6B7DF22E62DB"))
                .setCAPKExponent(new byte[]{0x03})
                .setHashValue(Hex.decode("8E8DFF443D78CD91DE88821D70C98F0638E51E49"))
                .build();
        CAPK capkAmex0xca = new CAPK.Builder()
                .setHeader((byte) 0x20)
                .setServiceIdentifier(new byte[]{0x00, 0x00, 0x00, 0x00})
                .setLengthOfCAPKModulus((byte) 0xf8)
                .setCAPKAlgorithmIndicator((byte) 0x01)
                .setLengthOfCAPKExponent((byte) 0x01)
                .setRID(new byte[]{(byte) 0xa0, 0x00, 0x00, 0x00, 0x25})
                .setCAPKIndex((byte) 0xca)
                .setCAPKModulus(Hex.decode("C23ECBD7119F479C2EE546C123A585D697A7D10B55C2D28BEF0D299C01DC65420A03FE5227ECDECB8025FBC86EEBC1935298C1753AB849936749719591758C315FA150400789BB14FADD6EAE2AD617DA38163199D1BAD5D3F8F6A7A20AEF420ADFE2404D30B219359C6A4952565CCCA6F11EC5BE564B49B0EA5BF5B3DC8C5C6401208D0029C3957A8C5922CBDE39D3A564C6DEBB6BD2AEF91FC27BB3D3892BEB9646DCE2E1EF8581EFFA712158AAEC541C0BBB4B3E279D7DA54E45A0ACC3570E712C9F7CDF985CFAFD382AE13A3B214A9E8E1E71AB1EA707895112ABC3A97D0FCB0AE2EE5C85492B6CFD54885CDD6337E895CC70FB3255E3"))
                .setCAPKExponent(new byte[]{0x03})
                .setHashValue(Hex.decode("6BDA32B1AA171444C7E8F88075A74FBFE845765F"))
                .build();

        //Amex Production CAPK
        CAPK capkAmex0x0e = new CAPK.Builder()
                .setHeader((byte) 0x20)
                .setServiceIdentifier(new byte[]{0x00, 0x00, 0x00, 0x00})
                .setLengthOfCAPKModulus((byte) 0x90)
                .setCAPKAlgorithmIndicator((byte) 0x01)
                .setLengthOfCAPKExponent((byte) 0x03)
                .setRID(new byte[]{(byte) 0xa0, 0x00, 0x00, 0x00, 0x25})
                .setCAPKIndex((byte) 0x0e)
                .setCAPKModulus(Hex.decode("AA94A8C6DAD24F9BA56A27C09B01020819568B81A026BE9FD0A3416CA9A71166ED5084ED91CED47DD457DB7E6CBCD53E560BC5DF48ABC380993B6D549F5196CFA77DFB20A0296188E969A2772E8C4141665F8BB2516BA2C7B5FC91F8DA04E8D512EB0F6411516FB86FC021CE7E969DA94D33937909A53A57F907C40C22009DA7532CB3BE509AE173B39AD6A01BA5BB85"))
                .setCAPKExponent(new byte[]{0x03})
                .setHashValue(Hex.decode("A7266ABAE64B42A3668851191D49856E17F8FBCD"))
                .build();
        CAPK capkAmex0x0f = new CAPK.Builder()
                .setHeader((byte) 0x20)
                .setServiceIdentifier(new byte[]{0x00, 0x00, 0x00, 0x00})
                .setLengthOfCAPKModulus((byte) 0xb0)
                .setCAPKAlgorithmIndicator((byte) 0x01)
                .setLengthOfCAPKExponent((byte) 0x03)
                .setRID(new byte[]{(byte) 0xa0, 0x00, 0x00, 0x00, 0x25})
                .setCAPKIndex((byte) 0x0f)
                .setCAPKModulus(Hex.decode("C8D5AC27A5E1FB89978C7C6479AF993AB3800EB243996FBB2AE26B67B23AC482C4B746005A51AFA7D2D83E894F591A2357B30F85B85627FF15DA12290F70F05766552BA11AD34B7109FA49DE29DCB0109670875A17EA95549E92347B948AA1F045756DE56B707E3863E59A6CBE99C1272EF65FB66CBB4CFF070F36029DD76218B21242645B51CA752AF37E70BE1A84FF31079DC0048E928883EC4FADD497A719385C2BBBEBC5A66AA5E5655D18034EC5"))
                .setCAPKExponent(new byte[]{0x03})
                .setHashValue(Hex.decode("A73472B3AB557493A9BC2179CC8014053B12BAB4"))
                .build();
        CAPK capkAmex0x10 = new CAPK.Builder()
                .setHeader((byte) 0x20)
                .setServiceIdentifier(new byte[]{0x00, 0x00, 0x00, 0x00})
                .setLengthOfCAPKModulus((byte) 0xf8)
                .setCAPKAlgorithmIndicator((byte) 0x01)
                .setLengthOfCAPKExponent((byte) 0x03)
                .setRID(new byte[]{(byte) 0xa0, 0x00, 0x00, 0x00, 0x25})
                .setCAPKIndex((byte) 0x10)
                .setCAPKModulus(Hex.decode("CF98DFEDB3D3727965EE7797723355E0751C81D2D3DF4D18EBAB9FB9D49F38C8C4A826B99DC9DEA3F01043D4BF22AC3550E2962A59639B1332156422F788B9C16D40135EFD1BA94147750575E636B6EBC618734C91C1D1BF3EDC2A46A43901668E0FFC136774080E888044F6A1E65DC9AAA8928DACBEB0DB55EA3514686C6A732CEF55EE27CF877F110652694A0E3484C855D882AE191674E25C296205BBB599455176FDD7BBC549F27BA5FE35336F7E29E68D783973199436633C67EE5A680F05160ED12D1665EC83D1997F10FD05BBDBF9433E8F797AEE3E9F02A34228ACE927ABE62B8B9281AD08D3DF5C7379685045D7BA5FCDE58637"))
                .setCAPKExponent(new byte[]{0x03})
                .setHashValue(Hex.decode("C729CF2FD262394ABC4CC173506502446AA9B9FD"))
                .build();


        Application appAmex = new Application.Builder()
                .setBINRanges(new String[]{"340000 - 349999", "370000 – 379999"})
                .setApplicationID(Hex.decode("A00000002501"))
                .setApplicationSelectionIndicator((byte) 1)
                /*
                .setTerminalActionCodeDefault(Hex.decode("C800000000"))
                .setTerminalActionCodeOnline (Hex.decode("C800000000"))
                .setTerminalActionCodeDenial (Hex.decode("0000000000"))
                 */
                /*
                //UAT TAC
                .setTerminalActionCodeDefault(Hex.decode("DC50840000"))
                .setTerminalActionCodeOnline (Hex.decode("C400000000"))
                .setTerminalActionCodeDenial (Hex.decode("0000000000"))
                */
                //Production TAC
                .setTerminalActionCodeDefault(Hex.decode("DC50FC9800"))
                .setTerminalActionCodeOnline (Hex.decode("DE00FC9800"))
                .setTerminalActionCodeDenial (Hex.decode("0010000000"))
                //.setTerminalActionCodeDefault(Hex.decode("584000A800")) // online only
                //.setTerminalActionCodeOnline (Hex.decode("584004F800")) // online only
                //.setTerminalActionCodeDenial (Hex.decode("0010000000")) // online only
                .setApplicationVersionNumber (Hex.decode("0001"))
                .setTDOL(Hex.decode("000000"))
                .setDDOL(Hex.decode("9F3704"))
                .setFloorLimit(DEFAULT_TERMINAL_FLOOR_LIMIT)
                .setContactlessFloorLimit(DEFAULT_TERMINAL_CONTACTLESS_FLOOR_LIMIT)
                .setContactlessCardholderVerificationLimit(DEFAULT_TERMINAL_CONTACTLESS_CARDHOLDER_VERIFICATION_LIMIT)
                .setContactlessTransactionLimit(DEFAULT_TERMINAL_CONTACTLESS_TRANSACTION_LIMIT)
                //.setCertificationAuthorityPublicKeys(new CAPK[]{capkAmex0xc8, capkAmex0xc9, capkAmex0xca})// UAT CAPK
                .setCertificationAuthorityPublicKeys(new CAPK[]{capkAmex0x0e, capkAmex0x0f, capkAmex0x10})  // Production CAPK
                .setMerchantNumber("9425821733")
                .build();
        applications.add(appAmex);


        // Master Test CAPK
        CAPK capkMCFE = new CAPK.Builder()
                .setHeader((byte) 0x00)
                .setServiceIdentifier(new byte[]{0x00, 0x00, 0x00, 0x00})
                .setLengthOfCAPKModulus((byte) 0x80)
                .setCAPKAlgorithmIndicator((byte) 0x01)
                .setLengthOfCAPKExponent((byte) 0x01)
                .setRID(new byte[]{(byte) 0xa0, 0x00, 0x00, 0x00, 0x04})
                .setCAPKIndex((byte) 0xFE)
                .setCAPKModulus(Hex.decode("A653EAC1C0F786C8724F737F172997D63D1C3251C44402049B865BAE877D0F398CBFBE8A6035E24AFA086BEFDE9351E54B95708EE672F0968BCD50DCE40F783322B2ABA04EF137EF18ABF03C7DBC5813AEAEF3AA7797BA15DF7D5BA1CBAF7FD520B5A482D8D3FEE105077871113E23A49AF3926554A70FE10ED728CF793B62A1"))
                .setCAPKExponent(new byte[]{0x03})
                .setHashValue(Hex.decode("9A295B05FB390EF7923F57618A9FDA2941FC34E0"))
                .build();

        CAPK capkMCF3 = new CAPK.Builder()
                .setHeader((byte) 0x00)
                .setServiceIdentifier(new byte[]{0x00, 0x00, 0x00, 0x00})
                .setLengthOfCAPKModulus((byte) 0x90)
                .setCAPKAlgorithmIndicator((byte) 0x01)
                .setLengthOfCAPKExponent((byte) 0x01)
                .setRID(new byte[]{(byte) 0xa0, 0x00, 0x00, 0x00, 0x04})
                .setCAPKIndex((byte) 0xF3)
                .setCAPKModulus(Hex.decode("98F0C770F23864C2E766DF02D1E833DFF4FFE92D696E1642F0A88C5694C6479D16DB1537BFE29E4FDC6E6E8AFD1B0EB7EA0124723C333179BF19E93F10658B2F776E829E87DAEDA9C94A8B3382199A350C077977C97AFF08FD11310AC950A72C3CA5002EF513FCCC286E646E3C5387535D509514B3B326E1234F9CB48C36DDD44B416D23654034A66F403BA511C5EFA3"))
                .setCAPKExponent(new byte[]{0x03})
                .setHashValue(Hex.decode("A69AC7603DAF566E972DEDC2CB433E07E8B01A9A"))
                .build();

        CAPK capkMCFA = new CAPK.Builder()
                .setHeader((byte) 0x00)
                .setServiceIdentifier(new byte[]{0x00, 0x00, 0x00, 0x00})
                .setLengthOfCAPKModulus((byte) 0x90)
                .setCAPKAlgorithmIndicator((byte) 0x01)
                .setLengthOfCAPKExponent((byte) 0x01)
                .setRID(new byte[]{(byte) 0xa0, 0x00, 0x00, 0x00, 0x04})
                .setCAPKIndex((byte) 0xFA)
                .setCAPKModulus(Hex.decode("A90FCD55AA2D5D9963E35ED0F440177699832F49C6BAB15CDAE5794BE93F934D4462D5D12762E48C38BA83D8445DEAA74195A301A102B2F114EADA0D180EE5E7A5C73E0C4E11F67A43DDAB5D55683B1474CC0627F44B8D3088A492FFAADAD4F42422D0E7013536C3C49AD3D0FAE96459B0F6B1B6056538A3D6D44640F94467B108867DEC40FAAECD740C00E2B7A8852D"))
                .setCAPKExponent(new byte[]{0x03})
                .setHashValue(Hex.decode("5BED4068D96EA16D2D77E03D6036FC7A160EA99C"))
                .build();

        CAPK capkMCF1 = new CAPK.Builder()
                .setHeader((byte) 0x00)
                .setServiceIdentifier(new byte[]{0x00, 0x00, 0x00, 0x00})
                .setLengthOfCAPKModulus((byte) 0x90)
                .setCAPKAlgorithmIndicator((byte) 0x01)
                .setLengthOfCAPKExponent((byte) 0x01)
                .setRID(new byte[]{(byte) 0xa0, 0x00, 0x00, 0x00, 0x04})
                .setCAPKIndex((byte) 0xF1)
                .setCAPKModulus(Hex.decode("A0DCF4BDE19C3546B4B6F0414D174DDE294AABBB828C5A834D73AAE27C99B0B053A90278007239B6459FF0BBCD7B4B9C6C50AC02CE91368DA1BD21AAEADBC65347337D89B68F5C99A09D05BE02DD1F8C5BA20E2F13FB2A27C41D3F85CAD5CF6668E75851EC66EDBF98851FD4E42C44C1D59F5984703B27D5B9F21B8FA0D93279FBBF69E090642909C9EA27F898959541AA6757F5F624104F6E1D3A9532F2A6E51515AEAD1B43B3D7835088A2FAFA7BE7"))
                .setCAPKExponent(new byte[]{0x03})
                .setHashValue(Hex.decode("D8E68DA167AB5A85D8C3D55ECB9B0517A1A5B4BB"))
                .build();

        CAPK capkMCEF = new CAPK.Builder()
                .setHeader((byte) 0x00)
                .setServiceIdentifier(new byte[]{0x00, 0x00, 0x00, 0x00})
                .setLengthOfCAPKModulus((byte) 0xF8)
                .setCAPKAlgorithmIndicator((byte) 0x01)
                .setLengthOfCAPKExponent((byte) 0x01)
                .setRID(new byte[]{(byte) 0xa0, 0x00, 0x00, 0x00, 0x04})
                .setCAPKIndex((byte) 0xEF)
                .setCAPKModulus(Hex.decode("A191CB87473F29349B5D60A88B3EAEE0973AA6F1A082F358D849FDDFF9C091F899EDA9792CAF09EF28F5D22404B88A2293EEBBC1949C43BEA4D60CFD879A1539544E09E0F09F60F065B2BF2A13ECC705F3D468B9D33AE77AD9D3F19CA40F23DCF5EB7C04DC8F69EBA565B1EBCB4686CD274785530FF6F6E9EE43AA43FDB02CE00DAEC15C7B8FD6A9B394BABA419D3F6DC85E16569BE8E76989688EFEA2DF22FF7D35C043338DEAA982A02B866DE5328519EBBCD6F03CDD686673847F84DB651AB86C28CF1462562C577B853564A290C8556D818531268D25CC98A4CC6A0BDFFFDA2DCCA3A94C998559E307FDDF915006D9A987B07DDAEB3B"))
                .setCAPKExponent(new byte[]{0x03})
                .setHashValue(Hex.decode("21766EBB0EE122AFB65D7845B73DB46BAB65427A"))
                .build();

        CAPK capkMCF8 = new CAPK.Builder()
                .setHeader((byte) 0x00)
                .setServiceIdentifier(new byte[]{0x00, 0x00, 0x00, 0x00})
                .setLengthOfCAPKModulus((byte) 0x80)
                .setCAPKAlgorithmIndicator((byte) 0x01)
                .setLengthOfCAPKExponent((byte) 0x01)
                .setRID(new byte[]{(byte) 0xa0, 0x00, 0x00, 0x00, 0x04})
                .setCAPKIndex((byte) 0xF8)
                .setCAPKModulus(Hex.decode("A1F5E1C9BD8650BD43AB6EE56B891EF7459C0A24FA84F9127D1A6C79D4930F6DB1852E2510F18B61CD354DB83A356BD190B88AB8DF04284D02A4204A7B6CB7C5551977A9B36379CA3DE1A08E69F301C95CC1C20506959275F41723DD5D2925290579E5A95B0DF6323FC8E9273D6F849198C4996209166D9BFC973C361CC826E1"))
                .setCAPKExponent(new byte[]{0x03})
                .setHashValue(Hex.decode("F06ECC6D2AAEBF259B7E755A38D9A9B24E2FF3DD"))
                .build();

        //Master Live CAPK
        CAPK capkMC00 = new CAPK.Builder()
                .setHeader((byte) 0x00)
                .setServiceIdentifier(new byte[]{0x00, 0x00, 0x00, 0x00})
                .setLengthOfCAPKModulus((byte) 0x60)
                .setCAPKAlgorithmIndicator((byte) 0x01)
                .setLengthOfCAPKExponent((byte) 0x01)
                .setRID(new byte[]{(byte) 0xA0, 0x00, 0x00, 0x00, 0x04})
                .setCAPKIndex((byte) 0x00)
                .setCAPKModulus(Hex.decode("9E15214212F6308ACA78B80BD986AC287516846C8D548A9ED0A42E7D997C902C3E122D1B9DC30995F4E25C75DD7EE0A0CE293B8CC02B977278EF256D761194924764942FE714FA02E4D57F282BA3B2B62C9E38EF6517823F2CA831BDDF6D363D"))
                .setCAPKExponent(new byte[]{0x03})
                .setHashValue(Hex.decode("8BB99ADDF7B560110955014505FB6B5F8308CE27"))
                .build();

        CAPK capkMC01 = new CAPK.Builder()
                .setHeader((byte) 0x00)
                .setServiceIdentifier(new byte[]{0x00, 0x00, 0x00, 0x00})
                .setLengthOfCAPKModulus((byte) 0x60)
                .setCAPKAlgorithmIndicator((byte) 0x01)
                .setLengthOfCAPKExponent((byte) 0x01)
                .setRID(new byte[]{(byte) 0xA0, 0x00, 0x00, 0x00, 0x04})
                .setCAPKIndex((byte) 0x01)
                .setCAPKModulus(Hex.decode("D2010716C9FB5264D8C91A14F4F32F8981EE954F20087ED77CDC5868431728D3637C632CCF2718A4F5D92EA8AB166AB992D2DE24E9FBDC7CAB9729401E91C502D72B39F6866F5C098B1243B132AFEE65F5036E168323116338F8040834B98725"))
                .setCAPKExponent(new byte[]{0x03})
                .setHashValue(Hex.decode("EA950DD4234FEB7C900C0BE817F64DE66EEEF7C4"))
                .build();

        CAPK capkMC02 = new CAPK.Builder()
                .setHeader((byte) 0x00)
                .setServiceIdentifier(new byte[]{0x00, 0x00, 0x00, 0x00})
                .setLengthOfCAPKModulus((byte) 0x70)
                .setCAPKAlgorithmIndicator((byte) 0x01)
                .setLengthOfCAPKExponent((byte) 0x01)
                .setRID(new byte[]{(byte) 0xA0, 0x00, 0x00, 0x00, 0x04})
                .setCAPKIndex((byte) 0x02)
                .setCAPKModulus(Hex.decode("CF4264E1702D34CA897D1F9B66C5D63691EACC612C8F147116BB22D0C463495BD5BA70FB153848895220B8ADEEC3E7BAB31EA22C1DC9972FA027D54265BEBF0AE3A23A8A09187F21C856607B98BDA6FC908116816C502B3E58A145254EEFEE2A3335110224028B67809DCB8058E24895"))
                .setCAPKExponent(new byte[]{0x03})
                .setHashValue(Hex.decode("AF1CC1FD1C1BC9BCA07E78DA6CBA2163F169CBB7"))
                .build();

        CAPK capkMC03 = new CAPK.Builder()
                .setHeader((byte) 0x00)
                .setServiceIdentifier(new byte[]{0x00, 0x00, 0x00, 0x00})
                .setLengthOfCAPKModulus((byte) 0x80)
                .setCAPKAlgorithmIndicator((byte) 0x01)
                .setLengthOfCAPKExponent((byte) 0x01)
                .setRID(new byte[]{(byte) 0xA0, 0x00, 0x00, 0x00, 0x04})
                .setCAPKIndex((byte) 0x03)
                .setCAPKModulus(Hex.decode("C2490747FE17EB0584C88D47B1602704150ADC88C5B998BD59CE043EDEBF0FFEE3093AC7956AD3B6AD4554C6DE19A178D6DA295BE15D5220645E3C8131666FA4BE5B84FE131EA44B039307638B9E74A8C42564F892A64DF1CB15712B736E3374F1BBB6819371602D8970E97B900793C7C2A89A4A1649A59BE680574DD0B60145"))
                .setCAPKExponent(new byte[]{0x03})
                .setHashValue(Hex.decode("5ADDF21D09278661141179CBEFF272EA384B13BB"))
                .build();

        CAPK capkMC04 = new CAPK.Builder()
                .setHeader((byte) 0x00)
                .setServiceIdentifier(new byte[]{0x00, 0x00, 0x00, 0x00})
                .setLengthOfCAPKModulus((byte) 0x90)
                .setCAPKAlgorithmIndicator((byte) 0x01)
                .setLengthOfCAPKExponent((byte) 0x01)
                .setRID(new byte[]{(byte) 0xA0, 0x00, 0x00, 0x00, 0x04})
                .setCAPKIndex((byte) 0x04)
                .setCAPKModulus(Hex.decode("A6DA428387A502D7DDFB7A74D3F412BE762627197B25435B7A81716A700157DDD06F7CC99D6CA28C2470527E2C03616B9C59217357C2674F583B3BA5C7DCF2838692D023E3562420B4615C439CA97C44DC9A249CFCE7B3BFB22F68228C3AF13329AA4A613CF8DD853502373D62E49AB256D2BC17120E54AEDCED6D96A4287ACC5C04677D4A5A320DB8BEE2F775E5FEC5"))
                .setCAPKExponent(new byte[]{0x03})
                .setHashValue(Hex.decode("381A035DA58B482EE2AF75F4C3F2CA469BA4AA6C"))
                .build();

        CAPK capkMC05 = new CAPK.Builder()
                .setHeader((byte) 0x00)
                .setServiceIdentifier(new byte[]{0x00, 0x00, 0x00, 0x00})
                .setLengthOfCAPKModulus((byte) 0xB0)
                .setCAPKAlgorithmIndicator((byte) 0x01)
                .setLengthOfCAPKExponent((byte) 0x01)
                .setRID(new byte[]{(byte) 0xA0, 0x00, 0x00, 0x00, 0x04})
                .setCAPKIndex((byte) 0x05)
                .setCAPKModulus(Hex.decode("B8048ABC30C90D976336543E3FD7091C8FE4800DF820ED55E7E94813ED00555B573FECA3D84AF6131A651D66CFF4284FB13B635EDD0EE40176D8BF04B7FD1C7BACF9AC7327DFAA8AA72D10DB3B8E70B2DDD811CB4196525EA386ACC33C0D9D4575916469C4E4F53E8E1C912CC618CB22DDE7C3568E90022E6BBA770202E4522A2DD623D180E215BD1D1507FE3DC90CA310D27B3EFCCD8F83DE3052CAD1E48938C68D095AAC91B5F37E28BB49EC7ED597"))
                .setCAPKExponent(new byte[]{0x03})
                .setHashValue(Hex.decode("EBFA0D5D06D8CE702DA3EAE890701D45E274C845"))
                .build();

        CAPK capkMC06 = new CAPK.Builder()
                .setHeader((byte) 0x00)
                .setServiceIdentifier(new byte[]{0x00, 0x00, 0x00, 0x00})
                .setLengthOfCAPKModulus((byte) 0xF8)
                .setCAPKAlgorithmIndicator((byte) 0x01)
                .setLengthOfCAPKExponent((byte) 0x01)
                .setRID(new byte[]{(byte) 0xA0, 0x00, 0x00, 0x00, 0x04})
                .setCAPKIndex((byte) 0x06)
                .setCAPKModulus(Hex.decode("CB26FC830B43785B2BCE37C81ED334622F9622F4C89AAE641046B2353433883F307FB7C974162DA72F7A4EC75D9D657336865B8D3023D3D645667625C9A07A6B7A137CF0C64198AE38FC238006FB2603F41F4F3BB9DA1347270F2F5D8C606E420958C5F7D50A71DE30142F70DE468889B5E3A08695B938A50FC980393A9CBCE44AD2D64F630BB33AD3F5F5FD495D31F37818C1D94071342E07F1BEC2194F6035BA5DED3936500EB82DFDA6E8AFB655B1EF3D0D7EBF86B66DD9F29F6B1D324FE8B26CE38AB2013DD13F611E7A594D675C4432350EA244CC34F3873CBA06592987A1D7E852ADC22EF5A2EE28132031E48F74037E3B34AB747F"))
                .setCAPKExponent(new byte[]{0x03})
                .setHashValue(Hex.decode("F910A1504D5FFB793D94F3B500765E1ABCAD72D9"))
                .build();

//        Application appMaster = new Application.Builder()
//                .setBINRanges(new String[]{"340000 - 349999", "370000 – 379999"})
//                .setApplicationID(Hex.decode("A000000004"))
//                .setApplicationSelectionIndicator((byte) 1)
//                .setTerminalActionCodeDefault(Hex.decode("CC00FC8000"))
//                .setTerminalActionCodeOnline(Hex.decode("CC00FC8000"))
//                .setTerminalActionCodeDenial(Hex.decode("0000000000"))
//                .setApplicationVersionNumber(Hex.decode("0001"))
//                .setTDOL(Hex.decode("000000"))
//                .setDDOL(Hex.decode("9F3704"))
//                .setFloorLimit(DEFAULT_TERMINAL_FLOOR_LIMIT)
//                .setContactlessFloorLimit(DEFAULT_TERMINAL_CONTACTLESS_FLOOR_LIMIT)
//                .setContactlessCardholderVerificationLimit(DEFAULT_TERMINAL_CONTACTLESS_CARDHOLDER_VERIFICATION_LIMIT)
//                .setContactlessTransactionLimit(DEFAULT_TERMINAL_CONTACTLESS_TRANSACTION_LIMIT)
//                .setCertificationAuthorityPublicKeys(new CAPK[]{capkMCFE, capkMCF3, capkMCFA, capkMCF1, capkMCEF, capkMCF8})
//                .setMerchantNumber("9425821733")
//                .build();
//        applications.add(appMaster);
//
//        Application appMaster1 = new Application.Builder()
//                .setBINRanges(new String[]{"340000 - 349999", "370000 – 379999"})
//                .setApplicationID(Hex.decode("A0000000046000"))
//                .setApplicationSelectionIndicator((byte) 1)
//                .setTerminalActionCodeDefault(Hex.decode("CC00FC8000"))
//                .setTerminalActionCodeOnline(Hex.decode("CC00FC8000"))
//                .setTerminalActionCodeDenial(Hex.decode("0000000000"))
//                .setApplicationVersionNumber(Hex.decode("0001"))
//                .setTDOL(Hex.decode("000000"))
//                .setDDOL(Hex.decode("9F3704"))
//                .setFloorLimit(DEFAULT_TERMINAL_FLOOR_LIMIT)
//                .setContactlessFloorLimit(DEFAULT_TERMINAL_CONTACTLESS_FLOOR_LIMIT)
//                .setContactlessCardholderVerificationLimit(DEFAULT_TERMINAL_CONTACTLESS_CARDHOLDER_VERIFICATION_LIMIT)
//                .setContactlessTransactionLimit(DEFAULT_TERMINAL_CONTACTLESS_TRANSACTION_LIMIT)
//                .setCertificationAuthorityPublicKeys(new CAPK[]{capkMCFE, capkMCF3, capkMCFA, capkMCF1, capkMCEF, capkMCF8})
//                .setMerchantNumber("9425821733")
//                .build();
//        applications.add(appMaster1);

        Application appMaster2 = new Application.Builder()
                .setBINRanges(new String[]{"340000 - 349999", "370000 – 379999"})
                .setApplicationID(Hex.decode("A0000000041010"))
                .setApplicationSelectionIndicator((byte) 1)
                .setTerminalActionCodeDefault(Hex.decode("CC00FC8000"))
                .setTerminalActionCodeOnline(Hex.decode("CC00FC8000"))
                .setTerminalActionCodeDenial(Hex.decode("0000000000"))
                .setApplicationVersionNumber(Hex.decode("0001"))
                .setTDOL(Hex.decode("000000"))
                .setDDOL(Hex.decode("9F3704"))
                .setFloorLimit(DEFAULT_TERMINAL_FLOOR_LIMIT)
                .setContactlessFloorLimit(DEFAULT_TERMINAL_CONTACTLESS_FLOOR_LIMIT)
                .setContactlessCardholderVerificationLimit(DEFAULT_TERMINAL_CONTACTLESS_CARDHOLDER_VERIFICATION_LIMIT)
                .setContactlessTransactionLimit(DEFAULT_TERMINAL_CONTACTLESS_TRANSACTION_LIMIT)
                //.setCertificationAuthorityPublicKeys(new CAPK[]{capkMCFE, capkMCF3, capkMCFA, capkMCF1, capkMCEF, capkMCF8}) // Test CAPK
                .setCertificationAuthorityPublicKeys(new CAPK[]{capkMC00, capkMC01, capkMC02, capkMC03, capkMC04, capkMC05, capkMC06}) // Live CAPK
                .setMerchantNumber("9425821733")
                .build();
        applications.add(appMaster2);

        //Visa Test CAPK
        CAPK capkVisa95 = new CAPK.Builder()
                .setHeader((byte) 0x20)
                .setServiceIdentifier(new byte[]{0x00, 0x00, 0x00, 0x00})
                .setLengthOfCAPKModulus((byte) 0x72)
                .setCAPKAlgorithmIndicator((byte) 0x01)
                .setLengthOfCAPKExponent((byte) 0x01)
                .setRID(new byte[]{(byte) 0xa0, 0x00, 0x00, 0x00, 0x03})
                .setCAPKIndex((byte) 0x95)
                .setCAPKModulus(Hex.decode("BE9E1FA5E9A803852999C4AB432DB28600DCD9DAB76DFAAA47355A0FE37B1508AC6BF38860D3C6C2E5B12A3CAAF2A7005A7241EBAA7771112C74CF9A0634652FBCA0E5980C54A64761EA101A114E0F0B5572ADD57D010B7C9C887E104CA4EE1272DA66D997B9A90B5A6D624AB6C57E73C8F919000EB5F684898EF8C3DBEFB330C62660BED88EA78E909AFF05F6DA627B"))
                .setCAPKExponent(new byte[]{0x03})
                .setHashValue(Hex.decode("EE1511CEC71020A9B90443B37B1D5F6E703030F6"))
                .build();

        CAPK capkVisa92 = new CAPK.Builder()
                .setHeader((byte) 0x20)
                .setServiceIdentifier(new byte[]{0x00, 0x00, 0x00, 0x00})
                .setLengthOfCAPKModulus((byte) 0xB0)
                .setCAPKAlgorithmIndicator((byte) 0x01)
                .setLengthOfCAPKExponent((byte) 0x01)
                .setRID(new byte[]{(byte) 0xA0, 0x00, 0x00, 0x00, 0x03})
                .setCAPKIndex((byte) 0x92)
                .setCAPKModulus(Hex.decode("996AF56F569187D09293C14810450ED8EE3357397B18A2458EFAA92DA3B6DF6514EC060195318FD43BE9B8F0CC669E3F844057CBDDF8BDA191BB64473BC8DC9A730DB8F6B4EDE3924186FFD9B8C7735789C23A36BA0B8AF65372EB57EA5D89E7D14E9C7B6B557460F10885DA16AC923F15AF3758F0F03EBD3C5C2C949CBA306DB44E6A2C076C5F67E281D7EF56785DC4D75945E491F01918800A9E2DC66F60080566CE0DAF8D17EAD46AD8E30A247C9F"))
                .setCAPKExponent(new byte[]{0x03})
                .setHashValue(Hex.decode("429C954A3859CEF91295F663C963E582ED6EB253"))
                .build();

        CAPK capkVisa94 = new CAPK.Builder()
                .setHeader((byte) 0x20)
                .setServiceIdentifier(new byte[]{0x00, 0x00, 0x00, 0x00})
                .setLengthOfCAPKModulus((byte) 0xB0)
                .setCAPKAlgorithmIndicator((byte) 0x01)
                .setLengthOfCAPKExponent((byte) 0x01)
                .setRID(new byte[]{(byte) 0xA0, 0x00, 0x00, 0x00, 0x03})
                .setCAPKIndex((byte) 0x94)
                .setCAPKModulus(Hex.decode("ACD2B12302EE644F3F835ABD1FC7A6F62CCE48FFEC622AA8EF062BEF6FB8BA8BC68BBF6AB5870EED579BC3973E121303D34841A796D6DCBC41DBF9E52C4609795C0CCF7EE86FA1D5CB041071ED2C51D2202F63F1156C58A92D38BC60BDF424E1776E2BC9648078A03B36FB554375FC53D57C73F5160EA59F3AFC5398EC7B67758D65C9BFF7828B6B82D4BE124A416AB7301914311EA462C19F771F31B3B57336000DFF732D3B83DE07052D730354D297BEC72871DCCF0E193F171ABA27EE464C6A97690943D59BDABB2A27EB71CEEBDAFA1176046478FD62FEC452D5CA393296530AA3F41927ADFE434A2DF2AE3054F8840657A26E0FC617"))
                .setCAPKExponent(new byte[]{0x03})
                .setHashValue(Hex.decode("C4A3C43CCF87327D136B804160E47D43B60E6E0F"))
                .build();

        //Visa Live CAPK
        CAPK capkVisa01 = new CAPK.Builder()
                .setHeader((byte) 0x20)
                .setServiceIdentifier(new byte[]{0x00, 0x00, 0x00, 0x00})
                .setLengthOfCAPKModulus((byte) 0x80)
                .setCAPKAlgorithmIndicator((byte) 0x01)
                .setLengthOfCAPKExponent((byte) 0x01)
                .setRID(new byte[]{(byte) 0xA0, 0x00, 0x00, 0x00, 0x03})
                .setCAPKIndex((byte) 0x01)
                .setCAPKModulus(Hex.decode("C696034213D7D8546984579D1D0F0EA519CFF8DEFFC429354CF3A871A6F7183F1228DA5C7470C055387100CB935A712C4E2864DF5D64BA93FE7E63E71F25B1E5F5298575EBE1C63AA617706917911DC2A75AC28B251C7EF40F2365912490B939BCA2124A30A28F54402C34AECA331AB67E1E79B285DD5771B5D9FF79EA630B75"))
                .setCAPKExponent(new byte[]{0x03})
                .setHashValue(Hex.decode("D34A6A776011C7E7CE3AEC5F03AD2F8CFC5503CC"))
                .build();

        CAPK capkVisa07 = new CAPK.Builder()
                .setHeader((byte) 0x20)
                .setServiceIdentifier(new byte[]{0x00, 0x00, 0x00, 0x00})
                .setLengthOfCAPKModulus((byte) 0x90)
                .setCAPKAlgorithmIndicator((byte) 0x01)
                .setLengthOfCAPKExponent((byte) 0x01)
                .setRID(new byte[]{(byte) 0xA0, 0x00, 0x00, 0x00, 0x03})
                .setCAPKIndex((byte) 0x07)
                .setCAPKModulus(Hex.decode("A89F25A56FA6DA258C8CA8B40427D927B4A1EB4D7EA326BBB12F97DED70AE5E4480FC9C5E8A972177110A1CC318D06D2F8F5C4844AC5FA79A4DC470BB11ED635699C17081B90F1B984F12E92C1C529276D8AF8EC7F28492097D8CD5BECEA16FE4088F6CFAB4A1B42328A1B996F9278B0B7E3311CA5EF856C2F888474B83612A82E4E00D0CD4069A6783140433D50725F"))
                .setCAPKExponent(new byte[]{0x03})
                .setHashValue(Hex.decode("B4BC56CC4E88324932CBC643D6898F6FE593B172"))
                .build();

        CAPK capkVisa08 = new CAPK.Builder()
                .setHeader((byte) 0x20)
                .setServiceIdentifier(new byte[]{0x00, 0x00, 0x00, 0x00})
                .setLengthOfCAPKModulus((byte) 0xB0)
                .setCAPKAlgorithmIndicator((byte) 0x01)
                .setLengthOfCAPKExponent((byte) 0x01)
                .setRID(new byte[]{(byte) 0xA0, 0x00, 0x00, 0x00, 0x03})
                .setCAPKIndex((byte) 0x08)
                .setCAPKModulus(Hex.decode("D9FD6ED75D51D0E30664BD157023EAA1FFA871E4DA65672B863D255E81E137A51DE4F72BCC9E44ACE12127F87E263D3AF9DD9CF35CA4A7B01E907000BA85D24954C2FCA3074825DDD4C0C8F186CB020F683E02F2DEAD3969133F06F7845166ACEB57CA0FC2603445469811D293BFEFBAFAB57631B3DD91E796BF850A25012F1AE38F05AA5C4D6D03B1DC2E568612785938BBC9B3CD3A910C1DA55A5A9218ACE0F7A21287752682F15832A678D6E1ED0B"))
                .setCAPKExponent(new byte[]{0x03})
                .setHashValue(Hex.decode("C4A3C43CCF87327D136B804160E47D43B60E6E0F"))
                .build();

        CAPK capkVisa09 = new CAPK.Builder()
                .setHeader((byte) 0x20)
                .setServiceIdentifier(new byte[]{0x00, 0x00, 0x00, 0x00})
                .setLengthOfCAPKModulus((byte) 0xF8)
                .setCAPKAlgorithmIndicator((byte) 0x01)
                .setLengthOfCAPKExponent((byte) 0x01)
                .setRID(new byte[]{(byte) 0xA0, 0x00, 0x00, 0x00, 0x03})
                .setCAPKIndex((byte) 0x09)
                .setCAPKModulus(Hex.decode("9D912248DE0A4E39C1A7DDE3F6D2588992C1A4095AFBD1824D1BA74847F2BC4926D2EFD904B4B54954CD189A54C5D1179654F8F9B0D2AB5F0357EB642FEDA95D3912C6576945FAB897E7062CAA44A4AA06B8FE6E3DBA18AF6AE3738E30429EE9BE03427C9D64F695FA8CAB4BFE376853EA34AD1D76BFCAD15908C077FFE6DC5521ECEF5D278A96E26F57359FFAEDA19434B937F1AD999DC5C41EB11935B44C18100E857F431A4A5A6BB65114F174C2D7B59FDF237D6BB1DD0916E644D709DED56481477C75D95CDD68254615F7740EC07F330AC5D67BCD75BF23D28A140826C026DBDE971A37CD3EF9B8DF644AC385010501EFC6509D7A41"))
                .setCAPKExponent(new byte[]{0x03})
                .setHashValue(Hex.decode("1FF80A40173F52D7D27E0F26A146A1C8CCB29046"))
                .build();

        Application appVisa = new Application.Builder()
                .setBINRanges(new String[]{"340000 - 349999", "370000 – 379999"})
                .setApplicationID(Hex.decode("A0000000031010"))
                .setApplicationSelectionIndicator((byte) 1)
                .setTerminalActionCodeDefault(Hex.decode("DC4000A800"))
                .setTerminalActionCodeOnline(Hex.decode("DC4004F800"))
                .setTerminalActionCodeDenial(Hex.decode("0010000000"))
                .setApplicationVersionNumber(Hex.decode("0001"))
                .setTDOL(Hex.decode("000000"))
                .setDDOL(Hex.decode("9F3704"))
                .setFloorLimit(DEFAULT_TERMINAL_FLOOR_LIMIT)
                .setContactlessFloorLimit(DEFAULT_TERMINAL_CONTACTLESS_FLOOR_LIMIT)
                .setContactlessCardholderVerificationLimit(DEFAULT_TERMINAL_CONTACTLESS_CARDHOLDER_VERIFICATION_LIMIT)
                .setContactlessTransactionLimit(DEFAULT_TERMINAL_CONTACTLESS_TRANSACTION_LIMIT)
                //.setCertificationAuthorityPublicKeys(new CAPK[]{capkVisa95, capkVisa92, capkVisa94}) // Test CAPK
                .setCertificationAuthorityPublicKeys(new CAPK[]{capkVisa01, capkVisa07, capkVisa08, capkVisa09}) // Live CAPK
                .setMerchantNumber("9425821733")
                .build();
        applications.add(appVisa);

        Application appVisa1 = new Application.Builder()
                .setBINRanges(new String[]{"340000 - 349999", "370000 – 379999"})
                .setApplicationID(Hex.decode("A0000000032010"))
                .setApplicationSelectionIndicator((byte) 1)
                .setTerminalActionCodeDefault(Hex.decode("DC4000A800"))
                .setTerminalActionCodeOnline(Hex.decode("DC4004F800"))
                .setTerminalActionCodeDenial(Hex.decode("0010000000"))
                .setApplicationVersionNumber(Hex.decode("0001"))
                .setTDOL(Hex.decode("000000"))
                .setDDOL(Hex.decode("9F3704"))
                .setFloorLimit(DEFAULT_TERMINAL_FLOOR_LIMIT)
                .setContactlessFloorLimit(DEFAULT_TERMINAL_CONTACTLESS_FLOOR_LIMIT)
                .setContactlessCardholderVerificationLimit(DEFAULT_TERMINAL_CONTACTLESS_CARDHOLDER_VERIFICATION_LIMIT)
                .setContactlessTransactionLimit(DEFAULT_TERMINAL_CONTACTLESS_TRANSACTION_LIMIT)
                //.setCertificationAuthorityPublicKeys(new CAPK[]{capkVisa95, capkVisa92, capkVisa94}) // Test CAPK
                .setCertificationAuthorityPublicKeys(new CAPK[]{capkVisa01, capkVisa07, capkVisa08, capkVisa09}) // Live CAPK
                .setMerchantNumber("9425821733")
                .build();
        applications.add(appVisa1);

        Application appVisa2 = new Application.Builder()
                .setBINRanges(new String[]{"340000 - 349999", "370000 – 379999"})
                .setApplicationID(Hex.decode("A000000003"))
                .setApplicationSelectionIndicator((byte) 1)
                .setTerminalActionCodeDefault(Hex.decode("DC4000A800"))
                .setTerminalActionCodeOnline(Hex.decode("DC4004F800"))
                .setTerminalActionCodeDenial(Hex.decode("0010000000"))
                .setApplicationVersionNumber(Hex.decode("0001"))
                .setTDOL(Hex.decode("000000"))
                .setDDOL(Hex.decode("9F3704"))
                .setFloorLimit(DEFAULT_TERMINAL_FLOOR_LIMIT)
                .setContactlessFloorLimit(DEFAULT_TERMINAL_CONTACTLESS_FLOOR_LIMIT)
                .setContactlessCardholderVerificationLimit(DEFAULT_TERMINAL_CONTACTLESS_CARDHOLDER_VERIFICATION_LIMIT)
                .setContactlessTransactionLimit(DEFAULT_TERMINAL_CONTACTLESS_TRANSACTION_LIMIT)
                //.setCertificationAuthorityPublicKeys(new CAPK[]{capkVisa95, capkVisa92, capkVisa94}) // Test CAPK
                .setCertificationAuthorityPublicKeys(new CAPK[]{capkVisa01, capkVisa07, capkVisa08, capkVisa09}) // Live CAPK
                .setMerchantNumber("9425821733")
                .build();
        applications.add(appVisa2);


        CAPK capkJCB0F = new CAPK.Builder()
                .setHeader((byte) 0x20)
                .setServiceIdentifier(new byte[]{0x00, 0x00, 0x00, 0x00})
                .setLengthOfCAPKModulus((byte) 0x90)
                .setCAPKAlgorithmIndicator((byte) 0x01)
                .setLengthOfCAPKExponent((byte) 0x01)
                .setRID(new byte[]{(byte) 0xA0, 0x00, 0x00, 0x00, 0x65})
                .setCAPKIndex((byte) 0x0F)
                .setCAPKModulus(Hex.decode("9EFBADDE4071D4EF98C969EB32AF854864602E515D6501FDE576B310964A4F7C2CE842ABEFAFC5DC9E26A619BCF2614FE07375B9249BEFA09CFEE70232E75FFD647571280C76FFCA87511AD255B98A6B577591AF01D003BD6BF7E1FCE4DFD20D0D0297ED5ECA25DE261F37EFE9E175FB5F12D2503D8CFB060A63138511FE0E125CF3A643AFD7D66DCF9682BD246DDEA1"))
                .setCAPKExponent(new byte[]{0x03})
                .setHashValue(Hex.decode("2A1B82DE00F5F0C401760ADF528228D3EDE0F403"))
                .build();

        CAPK capkJCB11 = new CAPK.Builder()
                .setHeader((byte) 0x20)
                .setServiceIdentifier(new byte[]{0x00, 0x00, 0x00, 0x00})
                .setLengthOfCAPKModulus((byte) 0xB0)
                .setCAPKAlgorithmIndicator((byte) 0x01)
                .setLengthOfCAPKExponent((byte) 0x01)
                .setRID(new byte[]{(byte) 0xA0, 0x00, 0x00, 0x00, 0x65})
                .setCAPKIndex((byte) 0x11)
                .setCAPKModulus(Hex.decode("A2583AA40746E3A63C22478F576D1EFC5FB046135A6FC739E82B55035F71B09BEB566EDB9968DD649B94B6DEDC033899884E908C27BE1CD291E5436F762553297763DAA3B890D778C0F01E3344CECDFB3BA70D7E055B8C760D0179A403D6B55F2B3B083912B183ADB7927441BED3395A199EEFE0DEBD1F5FC3264033DA856F4A8B93916885BD42F9C1F456AAB8CFA83AC574833EB5E87BB9D4C006A4B5346BD9E17E139AB6552D9C58BC041195336485"))
                .setCAPKExponent(new byte[]{0x03})
                .setHashValue(Hex.decode("D9FD62C9DD4E6DE7741E9A17FB1FF2C5DB948BCB"))
                .build();

        CAPK capkJCB13 = new CAPK.Builder()
                .setHeader((byte) 0x20)
                .setServiceIdentifier(new byte[]{0x00, 0x00, 0x00, 0x00})
                .setLengthOfCAPKModulus((byte) 0xF8)
                .setCAPKAlgorithmIndicator((byte) 0x01)
                .setLengthOfCAPKExponent((byte) 0x01)
                .setRID(new byte[]{(byte) 0xA0, 0x00, 0x00, 0x00, 0x65})
                .setCAPKIndex((byte) 0x13)
                .setCAPKModulus(Hex.decode("A3270868367E6E29349FC2743EE545AC53BD3029782488997650108524FD051E3B6EACA6A9A6C1441D28889A5F46413C8F62F3645AAEB30A1521EEF41FD4F3445BFA1AB29F9AC1A74D9A16B93293296CB09162B149BAC22F88AD8F322D684D6B49A12413FC1B6AC70EDEDB18EC1585519A89B50B3D03E14063C2CA58B7C2BA7FB22799A33BCDE6AFCBEB4A7D64911D08D18C47F9BD14A9FAD8805A15DE5A38945A97919B7AB88EFA11A88C0CD92C6EE7DC352AB0746ABF13585913C8A4E04464B77909C6BD94341A8976C4769EA6C0D30A60F4EE8FA19E767B170DF4FA80312DBA61DB645D5D1560873E2674E1F620083F30180BD96CA589"))
                .setCAPKExponent(new byte[]{0x03})
                .setHashValue(Hex.decode("54CFAE617150DFA09D3F901C9123524523EBEDF3"))
                .build();

        Application appJCB = new Application.Builder()
                .setBINRanges(new String[]{"340000 - 349999", "370000 – 379999"})
                .setApplicationID(Hex.decode("A0000000651010"))
                .setApplicationSelectionIndicator((byte) 1)
                .setTerminalActionCodeDefault(Hex.decode("FC6024A800"))
                .setTerminalActionCodeOnline(Hex.decode("FC60ACF800"))
                .setTerminalActionCodeDenial(Hex.decode("0010000000"))
                .setApplicationVersionNumber(Hex.decode("0200"))
                .setTDOL(Hex.decode("000000"))
                .setDDOL(Hex.decode("9F3704"))
                .setFloorLimit(DEFAULT_TERMINAL_FLOOR_LIMIT)
                .setContactlessFloorLimit(DEFAULT_TERMINAL_CONTACTLESS_FLOOR_LIMIT)
                .setContactlessCardholderVerificationLimit(DEFAULT_TERMINAL_CONTACTLESS_CARDHOLDER_VERIFICATION_LIMIT)
                .setContactlessTransactionLimit(DEFAULT_TERMINAL_CONTACTLESS_TRANSACTION_LIMIT)
                .setCertificationAuthorityPublicKeys(new CAPK[]{capkJCB0F, capkJCB11, capkJCB13})
                .setMerchantNumber("9425821733")
                .build();
        applications.add(appJCB);

        mTerminal = TerminalFactory.create(this);

        int processingRule = SimpleStorageManager.getSharedPreferences().getInt(SimpleStorageManager.STORAGE_PROCESSING_RULE, 0);
        mPayProcessorGateway = new PayProcessorSimpleGateway(this, processingRule);
        mPYCPayProcessorGateway = new PayProcessorSimpleGateway(this, ProcessingRules.Planet_Payment);

        amexReceipt = new AMEXReceipt("", "");

        // 支付处理器初始化
        mPayProcessor = PayProcessorFactory.create(this);
        mPayProcessor.setApplications(applications);
        mPayProcessor.setGateway(mPayProcessorGateway);
        mPayProcessor.setPYCGateway(mPYCPayProcessorGateway);
        mPayProcessor.setDb(SimpleDb.getInstance());
        mPayProcessor.setListener(mPayProcessorListener);
        if (getIntent().hasExtra("enableAlwaysUseSwipe")) {
            boolean enableAlwaysUseSwipe = getIntent().getBooleanExtra("enableAlwaysUseSwipe", false);
            mPayProcessor.setEnableAlwaysUseSwipe(enableAlwaysUseSwipe);
        }
        if (getIntent().hasExtra("enableInstallmentPlan")) {
            boolean enableInstallmentPlan = getIntent().getBooleanExtra("enableInstallmentPlan", false);
            mPayProcessor.setEnableInstallmentPlan(enableInstallmentPlan);
        }
        if (processingRule == ProcessingRules.Bank_Of_East_Asia_With_DCC) {
            mPayProcessor.setEnableDynamicCurrencyConversion(true);
        }
        mPayProcessor.setEnableForceOnline(true);

        // 控件初始化
        mTvAmount = (TextView) findViewById(R.id.tv_amount);
        mIvResultIcon = (ImageView) findViewById(R.id.iv_result_icon);
        mIvProcessingIcon = (ImageView) findViewById(R.id.iv_processing_icon);
        mTvMessage = (TextView) findViewById(R.id.tv_message);
        mLlTransButtonsRetry = (LinearLayout) findViewById(R.id.ll_trans_buttons_retry);
        mBtnRetry = (Button) findViewById(R.id.btn_retry);
        mTvLog = (TextView) findViewById(R.id.tv_log);

        // 设置控件的值
        mTvAmount.setText(mTransactionType.name() + ": $" + mTransactionAmount);
        // 设置控件的事件
        mBtnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HandleProcessByTerminalActivity.this.stopHandleProcess();
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        HandleProcessByTerminalActivity.this.startHandleProcess();
                    }
                }, 300);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        this.startHandleProcess();
    }

    @Override
    protected void onStop() {
        super.onStop();
        this.stopHandleProcess();
    }

    /**
     *
     *
     * 开启支付处理流程
     */
    private void startHandleProcess() {
        mTransactionDateAndTime = new Date();
        mPayProcessor.startProcess(mTransactionData, mTransactionType.name(), mTransactionSystemTraceAuditNumber, mTransactionDateAndTime, mTransactionAmount);
    }

    /**
     * 停止支付处理流程
     */
    private void stopHandleProcess() {
        mPayProcessor.stopProcess();
    }

    private void switchToUiStatusProcessing(final int iconId, final int messageId) {
        switchToUiStatusProcessing(iconId, getString(messageId));
    }

    private void switchToUiStatusProcessing(final int iconId, final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mIvResultIcon.setVisibility(View.GONE);
                mIvProcessingIcon.setVisibility(View.VISIBLE);
                mTvMessage.setVisibility(View.VISIBLE);

                mIvProcessingIcon.setImageResource(iconId);
                mTvMessage.setText(message);
            }
        });
    }

    private void switchToUiStatusSuccess(final int messageId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mIvResultIcon.setVisibility(View.VISIBLE);
                mIvProcessingIcon.setVisibility(View.GONE);
                mTvMessage.setVisibility(View.VISIBLE);

                mIvResultIcon.setImageResource(R.mipmap.ic_success);
                mTvMessage.setText(messageId);
            }
        });
    }

    private void switchToUiStatusError(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mIvResultIcon.setVisibility(View.VISIBLE);
                mIvProcessingIcon.setVisibility(View.GONE);
                mTvMessage.setVisibility(View.VISIBLE);

                mIvResultIcon.setImageResource(R.mipmap.ic_error);
                mTvMessage.setText(message);
            }
        });
    }

    private void switchToUiStatusError(int messageId) {
        this.switchToUiStatusError(getString(messageId));
    }

    private void printReceipt(TransactionData transactionData) {
        Printer printer = mTerminal.getBuiltInPrinter();
        printer.setListener(new Printer.Listener() {
            @Override
            public void onPrinted() {

            }

            @Override
            public void onError(String code, String message) {

            }
        });

        if (printer.isSupported()) {
            Printer.Paper paper = amexReceipt.createApprovedReceipt(transactionData);
            printer.print(paper);
        }
    }

    private void printDeclinedReceipt(TransactionData transactionData) {
        Printer printer = mTerminal.getBuiltInPrinter();
        printer.setListener(new Printer.Listener() {
            @Override
            public void onPrinted() {

            }

            @Override
            public void onError(String code, String message) {

            }
        });

        if (printer.isSupported()) {
            try {
                Printer.Paper paper = amexReceipt.createDeclinedReceipt(transactionData);
                printer.print(paper);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void logDebug(String logText) {
        Log.d(TAG, logText);
        mLogContent.append(logText).append("\n");

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mTvLog.setText(mLogContent.toString());
            }
        });
    }
}
