package com.bindo.paymentsdk.v3.examples.utilities;

public class Constants {
    public static final String EXTRA_TRANSACTION_TYPE = "extra_transaction_type";
    public static final String EXTRA_TRANSACTION_STAN = "extra_transaction_stan";
    public static final String EXTRA_TRANSACTION_AMOUNT = "extra_transaction_amount";

    public static final String STORAGE_PREF_TERMINAL_SETTINGS_DOWNLOAD_FACTORY_PUBLIC_KEY= "storage_pref_terminal_settings_download_factory_public_key";

    public static final String STORAGE_PREF_TERMINAL_SETTINGS_DOWNLOAD_LOCALBIN = "storage_pref_terminal_settings_download_localbin";
    public static final String STORAGE_PREF_TERMINAL_SETTINGS_DOWNLOAD_CURRENCY = "storage_pref_terminal_settings_download_currency";

    public static final String STORAGE_PREF_TERMINAL_CONTACT_FLOOR_LIMIT = "storage_pref_terminal_contact_floor_limit";
    public static final String STORAGE_PREF_TERMINAL_CONTACTLESS_TRANSACTION_LIMIT = "storage_pref_terminal_contactless_transaction_limit";
    public static final String STORAGE_PREF_TERMINAL_CONTACTLESS_FLOOR_LIMIT = "storage_pref_terminal_contactless_floor_limit";
    public static final String STORAGE_PREF_TERMINAL_CONTACTLESS_CVM_LIMIT = "storage_pref_terminal_contactless_cvm_limit";
}
