package com.bindo.paymentsdk.v3.examples;

import android.app.Application;

import com.bindo.paymentsdk.v3.examples.utilities.Read8583ConfigFIle;
import com.bindo.paymentsdk.v3.examples.database.SimpleDb;
import com.bindo.paymentsdk.v3.examples.utilities.SimpleStorageManager;
import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.gateway.sic.SicPaymentGateway;

import com.facebook.stetho.Stetho;

import cat.ereza.customactivityoncrash.config.CaocConfig;

public class AppContext extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        SimpleDb.initialize(this);
        Stetho.initializeWithDefaults(this);

        CaocConfig.Builder.create()
                .minTimeBetweenCrashesMs(2000)
                .apply();

        SimpleStorageManager.init(this);


        SicPaymentGateway.setIReadResources(new Read8583ConfigFIle(getApplicationContext()));

        // PaymentSDK Log Utils Class.
        Log.setEnabled(true);
    }


}
