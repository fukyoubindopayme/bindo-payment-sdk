package com.bindo.paymentsdk.v3.examples.commons;

import com.bindo.paymentsdk.terminal.Printer;
import com.bindo.paymentsdk.v3.examples.utilities.SimpleStorageManager;
import com.bindo.paymentsdk.v3.pay.common.TransactionData;
import com.bindo.paymentsdk.v3.pay.common.legacy.CreditCard;
import com.bindo.paymentsdk.v3.pay.common.legacy.enums.CardReadMode;
import com.bindo.paymentsdk.v3.pay.common.util.StringUtils;

import xyz.belvi.luhn.cardValidator.CardValidator;
import xyz.belvi.luhn.cardValidator.models.Card;

public class TransactionReceipt {

    public static Printer.Paper createApprovedReceipt(TransactionData transactionData) {
        Printer.Paper.Builder builder = new Printer.Paper.Builder();


        CardReadMode cardReadMode = CardReadMode.CONTACT;


        CreditCard creditCard = new CreditCard();
        creditCard.setCardReadMode(cardReadMode);
        creditCard.setCardNumber(transactionData.getPrimaryAccountNumber());
        creditCard.setExpireDate(transactionData.getCardExpirationDate());

        CardReadMode cardReaderMode = creditCard.getCardReadMode();

        String storeName = "Bindo Labs";
        String storeAddress1 = "N/A";
        String storeCity = "Shenzhen";
        String storePhone = "852-2777-1177";

        String merchantId = ""; // transaction.getMerchantID();
        String terminalId = ""; // transaction.getTerminalID();

        String batch = String.format("%06d", SimpleStorageManager.batchNow());
        String transTime = transactionData.getTransactionDateAndTime();
        String transType = transactionData.getTransactionType();

//        String STAN = transaction.getSystemsTraceAuditNumber();
        String receiptNumber = String.valueOf(transactionData.getTransactionSystemTraceAuditNumber());

        String cardEncryptedNumber = creditCard.getCardNumber().replaceAll("\\d(?=\\d{4})", "*");
        String cardCheckFlag;

        Card card = new CardValidator(creditCard.getCardNumber()).guessCard();
        String cardType = "";

        if (card != null) {
            cardType = card.getCardName();
        }

        String cardExpMonth = "**"; //creditCard.getExpireDate().substring(0, 2);
        String cardExpYear = "**"; // creditCard.getExpireDate().substring(2);
        String cardAppLabel = transactionData.getApplicationLabel();
        String cardAid = transactionData.getApplicationIdentifier();

        String approvalCode = transactionData.getApprovalCode();
        String applicationCryptogram = transactionData.getApplicationCryptogram();

        switch (cardReaderMode) {
            case MANUAL:
                cardCheckFlag = "Manual";
                break;
            case CONTACT:
                cardCheckFlag = "Chip";
                break;
            case CONTACTLESS:
                cardCheckFlag = "Contactless";
                break;
            case SWIPE:
            case FALLBACK_SWIPE:
                cardCheckFlag = "Swiped";
                break;
            default:
                cardCheckFlag = "X";
                break;
        }

        String amountBase = String.valueOf(transactionData.getTransactionAmount());
        String amountTip = "";
        String amountTotal = amountBase + amountTip;
        String cardPinVerified = transactionData.isTransactionPinVerified() ? "PIN VERIFIED" : "";

        builder
                .text(storeName, Printer.Paper.ALIGN_CENTER, Printer.Paper.SIZE_LARGE)
//                .text(storeAddress1, Paper.ALIGN_CENTER)
                .text(storeCity, Printer.Paper.ALIGN_CENTER)
                .text(storePhone, Printer.Paper.ALIGN_CENTER)
                .line()
                .text(String.format("MERCHANT ID: %s", merchantId))
                .text(String.format("TERMINAL ID: %s", terminalId))
                .newLine()
                .text(String.format("BATCH# %s", batch))
                .text(String.format("%s - %s", cardEncryptedNumber, cardCheckFlag))
                .newLine()
                .text(String.format("%s EXPIRES %s/%s", cardType.toUpperCase(), cardExpMonth, cardExpYear))
                .newLine()
                .text(cardAppLabel)
                .text(cardAid)
                .newLine()
                .text(transTime)
                .newLine()
                .text(transType)
                .text(String.format("RRN: %s", transactionData.getRetrievalReferenceNumber()))
                .text(String.format("Auth Code: %s", transactionData.getAuthorizationCode()))
                .newLine()
                .text(String.format("BASE:                   $ %s", amountBase))
                .text("TIP:                    $__________")
                .text("TOTAL:                  $__________");

        if (!StringUtils.isEmpty(transactionData.getAvailableOfflineSpendingAmount())) {
            builder = builder.text(String.format("BALANCE:         $ %s", transactionData.getAvailableOfflineSpendingAmount()));
        }


        builder = builder.newLine()
                .text(cardPinVerified);

        if (transactionData.isTransactionRequestSignature()) {
            builder = builder.newLine()
                    .newLine()
                    .text("X_______________________________")
                    .newLine();
        }

        if (!StringUtils.isEmpty(applicationCryptogram)) {
            builder = builder.text(String.format("TC-%s", applicationCryptogram));
        }
        if (!StringUtils.isEmpty(transactionData.getTerminalVerificationResult())) {
            builder = builder.text(String.format("TVR-%s", transactionData.getTerminalVerificationResult()));
        }
        if (!StringUtils.isEmpty(transactionData.getTransactionStatusInformation())) {
            builder = builder.text(String.format("TSI-%s", transactionData.getTransactionStatusInformation()));
        }
        if (!StringUtils.isEmpty(approvalCode)) {
            builder = builder.text(String.format("AUTHORIZATION CODE: %s", approvalCode));
        }
        builder = builder.newLine()
                .text("I AGREE TO PAY THE ABOVE TOTAL \nAMOUNT ACCORDING TO THE CARD \nISSUER AGREEMENT.", Printer.Paper.ALIGN_CENTER)
                .newLine()
                .text("MERCHANT COPY", Printer.Paper.ALIGN_CENTER)
                .text("APPROVED", Printer.Paper.ALIGN_CENTER)
                .newLine()
                .cutPaper();


        Printer.Paper paper = builder.create();
        return paper;
    }

    public static Printer.Paper createDeclinedReceipt(TransactionData transactionData) {

        Printer.Paper.Builder builder = new Printer.Paper.Builder();


        CardReadMode cardReadMode = CardReadMode.CONTACT;


        CreditCard creditCard = new CreditCard();
        creditCard.setCardReadMode(cardReadMode);
        creditCard.setCardNumber(transactionData.getPrimaryAccountNumber());
        creditCard.setExpireDate(transactionData.getCardExpirationDate());

        CardReadMode cardReaderMode = creditCard.getCardReadMode();

        String storeName = "Bindo Labs";
        String storeAddress1 = "N/A";
        String storeCity = "Shenzhen";
        String storePhone = "852-2777-1177";

        String merchantId = ""; // transaction.getMerchantID();
        String terminalId = ""; // transaction.getTerminalID();

        String batch = String.format("%06d", SimpleStorageManager.batchNow());
        String transTime = transactionData.getTransactionDateAndTime();
        String transType = transactionData.getTransactionType();

        String receiptNumber = String.valueOf(transactionData.getTransactionSystemTraceAuditNumber());

        String cardEncryptedNumber = creditCard.getCardNumber().replaceAll("\\d(?=\\d{4})", "*");
        String cardCheckFlag;
        String cardType = new CardValidator(creditCard.getCardNumber()).guessCard().getCardName();
        String cardExpMonth = "**"; //creditCard.getExpireDate().substring(0, 2);
        String cardExpYear = "**"; // creditCard.getExpireDate().substring(2);
        String cardAppLabel = transactionData.getApplicationLabel();
        String cardAid = transactionData.getApplicationIdentifier();
        String cardholderName = "";
        String rateMarkUpText = "";

        String approvalCode = transactionData.getApprovalCode();
        String applicationCryptogram = transactionData.getApplicationCryptogram();

        switch (cardReaderMode) {
            case MANUAL:
                cardCheckFlag = "Manual";
                break;
            case CONTACT:
                cardCheckFlag = "Chip";
                break;
            case CONTACTLESS:
                cardCheckFlag = "Contactless";
                break;
            case SWIPE:
            case FALLBACK_SWIPE:
                cardCheckFlag = "Swiped";
                break;
            default:
                cardCheckFlag = "X";
                break;
        }

        String amountBase = String.valueOf(transactionData.getTransactionAmount());
        String amountTip = "";
        String amountTotal = amountBase + amountTip;
        String cardPinVerified = transactionData.isTransactionPinVerified() ? "PIN VERIFIED" : "";

        builder
                .text(storeName, Printer.Paper.ALIGN_CENTER, Printer.Paper.SIZE_LARGE)
//                .text(storeAddress1, Paper.ALIGN_CENTER)
                .text(storeCity, Printer.Paper.ALIGN_CENTER)
                .text(storePhone, Printer.Paper.ALIGN_CENTER)
                .text("-------------------------------------")
                .text("Cashier: Alex Chan")
                .line()
                .text(String.format("TID:%S     MID: %s", terminalId, merchantId))
                .text(String.format("Batch: %s   Trace: %06d", batch, transactionData.getTransactionSystemTraceAuditNumber()))
                .text(transTime + "  Card:" + cardType.toUpperCase())
                .newLine()
                .text(transType, Printer.Paper.ALIGN_CENTER, Printer.Paper.SIZE_LARGE)
                .text(String.format("%s - %s", cardEncryptedNumber, cardCheckFlag))
                .text(String.format("%s EXPIRES %s/%s", cardholderName, cardExpMonth, cardExpYear))
                .text(String.format("Appr code:%s RRN:%s", transactionData.getAuthorizationCode(), transactionData.getRetrievalReferenceNumber()))
                .text(String.format("App:%s TC:%s", cardAppLabel, applicationCryptogram))
                .text(String.format("AID:%s", cardAid))
                .text("-------------------------------------")
                .newLine();

        if (!transactionData.isTransactionOptOut() && !StringUtils.isEmpty(transactionData.getConversionRate())) {
            builder = builder.text(rateMarkUpText)
                    .text("SELECT [X] TXN Currency")
                    .newLine()
                    .text("[ ] HKD AMOUNT  USD AMOUNT[ ]")
                    .text(String.format("%.2f      %.2f", transactionData.getTransactionAmount(), transactionData.getTransactionAmount() * Double.parseDouble(transactionData.getConversionRate())))
                    .newLine()
                    .text(String.format("TIPS IN TXN CURRENCY ____________"))
                    .newLine()
                    .text(String.format("TOTAL IN TXN CURRENCY ____________"))
                    .newLine();
        }

        builder = builder.text(String.format("BASE:                   $ %s", amountBase))
                .text("TIP:                    $__________")
                .text("TOTAL:                  $__________")
                .newLine()
                .text(cardPinVerified);
        if (transactionData.isTransactionRequestSignature()) {
            builder = builder.newLine()
                    .newLine()
                    .text("X_______________________________")
                    .newLine();
        }

        if (!StringUtils.isEmpty(transactionData.getTerminalVerificationResult())) {
            builder = builder.text(String.format("TVR-%s", transactionData.getTerminalVerificationResult()));
        }

        if (!StringUtils.isEmpty(transactionData.getTransactionStatusInformation())) {
            builder = builder.text(String.format("TSI-%s", transactionData.getTransactionStatusInformation()));
        }

        if (!StringUtils.isEmpty(approvalCode)) {
            builder = builder.text(String.format("AUTHORIZATION CODE: %s", approvalCode));
        }

        builder = builder.newLine()
                .text("This service is offered by the merchant's service\nprovider, with FX rate at visa rate plus x.x%. I have a\nchoice of currencies including HKD and agree to pay\n" +
                        "above total amount according to card issuer agreement.", Printer.Paper.ALIGN_CENTER)
                .newLine()
                .text("MERCHANT COPY", Printer.Paper.ALIGN_CENTER)
                .newLine()
                .text("DECLINED", Printer.Paper.ALIGN_CENTER)
                .cutPaper();


        Printer.Paper paper = builder.create();
        return paper;
    }
}
