package com.bindo.paymentsdk.v3.examples.screens;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v14.preference.PreferenceFragment;
import android.support.v7.preference.EditTextPreference;
import android.support.v7.preference.Preference;

import com.bindo.paymentsdk.v3.examples.R;
import com.bindo.paymentsdk.v3.examples.database.SimpleDb;
import com.bindo.paymentsdk.v3.examples.modules.payprocessor.PayProcessorSimpleGateway;
import com.bindo.paymentsdk.v3.examples.modules.payprocessor.ProcessingRules;
import com.bindo.paymentsdk.v3.examples.screens.base.BaseActivity;
import com.bindo.paymentsdk.v3.examples.utilities.Constants;
import com.bindo.paymentsdk.v3.pay.common.TransactionType;
import com.bindo.paymentsdk.v3.pay.common.database.models.Currency;
import com.bindo.paymentsdk.v3.pay.common.database.models.LocalBIN;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Request;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Response;
import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.gateway.sic.SicPaymentGateway;

import java.util.List;

public class SettingsActivity extends BaseActivity {
    private static final String TAG = SettingsActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
    }

    public static class SettingsFragment extends PreferenceFragment {

        private Preference mNpTerminalSettingsDownloadFactoryPublicKey;
        private Preference mNpTerminalSettingsDownloadLocalBIN;
        private Preference mNpTerminalSettingsDownloadCurrency;
        private EditTextPreference mEtpContactFloorLimit;
        private EditTextPreference mEtpContactlessTransactionLimit;
        private EditTextPreference mEtpContactlessFloorLimit;
        private EditTextPreference mEtpContactlessCvmLimit;

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            addPreferencesFromResource(R.xml.pref_settings);
            mNpTerminalSettingsDownloadFactoryPublicKey = findPreference(Constants.STORAGE_PREF_TERMINAL_SETTINGS_DOWNLOAD_FACTORY_PUBLIC_KEY);
            mNpTerminalSettingsDownloadLocalBIN = findPreference(Constants.STORAGE_PREF_TERMINAL_SETTINGS_DOWNLOAD_LOCALBIN);
            mNpTerminalSettingsDownloadCurrency = findPreference(Constants.STORAGE_PREF_TERMINAL_SETTINGS_DOWNLOAD_CURRENCY);
            mEtpContactFloorLimit = (EditTextPreference) findPreference(Constants.STORAGE_PREF_TERMINAL_CONTACT_FLOOR_LIMIT);
            mEtpContactFloorLimit = (EditTextPreference) findPreference(Constants.STORAGE_PREF_TERMINAL_CONTACT_FLOOR_LIMIT);
            mEtpContactlessTransactionLimit = (EditTextPreference) findPreference(Constants.STORAGE_PREF_TERMINAL_CONTACTLESS_TRANSACTION_LIMIT);
            mEtpContactlessFloorLimit = (EditTextPreference) findPreference(Constants.STORAGE_PREF_TERMINAL_CONTACTLESS_FLOOR_LIMIT);
            mEtpContactlessCvmLimit = (EditTextPreference) findPreference(Constants.STORAGE_PREF_TERMINAL_CONTACTLESS_CVM_LIMIT);

            mNpTerminalSettingsDownloadFactoryPublicKey.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    clickTerminalSettingsDownloadFactoryPublicKey();
                    return false;
                }
            });

            mNpTerminalSettingsDownloadLocalBIN.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    clickTerminalSettingsDownloadLocalBIN();
                    return false;
                }
            });
            mNpTerminalSettingsDownloadCurrency.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    clickTerminalSettingsDownloadCurrency();
                    return false;
                }
            });
        }

        private void clickTerminalSettingsDownloadFactoryPublicKey() {
            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Downloading...");
            progressDialog.show();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    Request request = new Request();
                    request.setTransactionType(TransactionType.FACTORY_PUBLIC_KEY_DOWNLOAD);

                    SicPaymentGateway sicPaymentGateway = new SicPaymentGateway(null);
                    Response response = sicPaymentGateway.sendRequest(request);
                    Log.d(TAG, "clickTerminalSettingsDownloadFactoryPublicKey return response.");
                    if (response.isApproved()) {
                        //TODO 存储公钥到PinPad

                    }

                    if (response.getTransactionProcessingCode() != null &&
                            response.getTransactionProcessingCode().endsWith("0")) {

                    }

                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                        }
                    }, 6000);
                }
            }).start();

        }

        // TODO: incremental download
        // TODO: download if download requested
        private void clickTerminalSettingsDownloadLocalBIN() {
            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Downloading LocalBIN...");
            progressDialog.show();

            // truncate data before download
            SimpleDb.getInstance().table(LocalBIN.class).clear();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    PayProcessorSimpleGateway simpleGateway = new PayProcessorSimpleGateway(getActivity(), ProcessingRules.Planet_Payment);

                    try {
                        Request request = new Request();
                        request.setTransactionType(TransactionType.LOCAL_BIN_TABLE_DOWNLOAD);
                        request.setTransactionSystemTraceAuditNumber(99);
                        request.setTransactionCurrency("HKD");
                        request.setPlanetPaymentField63RequestForLocalBINTableDownload("00000000000000000344");

                        boolean finished = false;
                        while (!finished) {
                            Response response = simpleGateway.sendRequest(request);

                            if (response.getIsApproved()) {
                                List<LocalBIN> localBINs = response.getPlanetPaymentLocalBINs();
                                for (LocalBIN localBIN : localBINs) {
                                    Log.d(TAG, localBIN.getLowPrefixNumber() + " - " + localBIN.getHighPrefixNumber());
                                    SimpleDb.getInstance().table(LocalBIN.class).create(localBIN);
                                }
                                if (!response.getTransactionProcessingCode().endsWith("1")) {
                                    finished = true;
                                } else {
                                    String t01 = request.getPlanetPaymentField63RequestForLocalBINTableDownload();
                                    t01 = response.getLastLocalBINTableDate() + response.getLastLocalBINTableIndex() + t01.substring(t01.length() - 4);
                                    request.setPlanetPaymentField63RequestForLocalBINTableDownload(t01);
                                }
                            } else {
                                finished = true;
                                new Handler(Looper.getMainLooper()).post(new Runnable() {
                                    @Override
                                    public void run() {
                                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                        builder.setMessage("Download fails");
                                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                            }
                                        });
                                        builder.create().show();
                                    }
                                });
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                            }
                        }, 200);
                    }
                }
            }).start();
        }

        private void clickTerminalSettingsDownloadCurrency() {
            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Downloading Currency...");
            progressDialog.show();

            List<LocalBIN> results = SimpleDb.getInstance().table(LocalBIN.class).all();

            for (LocalBIN localBIN : results) {
                Log.d(TAG, localBIN.getLowPrefixNumber() + " - " + localBIN.getHighPrefixNumber());
            }

            SimpleDb.getInstance().table(Currency.class).clear();

            // TODO: Download Currency

            new Thread(new Runnable() {
                @Override
                public void run() {

                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                        }
                    }, 2000);
                }
            }).start();

        }
    }
}
