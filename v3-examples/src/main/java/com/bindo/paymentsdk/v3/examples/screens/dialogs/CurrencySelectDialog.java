package com.bindo.paymentsdk.v3.examples.screens.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.TextView;

import com.bindo.paymentsdk.v3.examples.R;
import com.bindo.paymentsdk.v3.pay.common.database.models.Currency;
import com.bindo.paymentsdk.v3.pay.common.util.StringUtils;

import java.util.List;
import java.util.concurrent.Callable;

public class CurrencySelectDialog implements Callable<Integer> {

    private int mSelectedIndex = -1;
    private int mLoopCount = 0;
    private AlertDialog mAlertDialog;

    private RecyclerView mRecyclerView;
    private CurrencyRecyclerViewAdapter mAdapter;

    public CurrencySelectDialog(final Context context, final List<CurrencySelectItemData> currencySelectItemDataList) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {

                View view = ((Activity) context).getLayoutInflater().inflate(R.layout.dialog_currency_select, null);

                mAdapter = new CurrencyRecyclerViewAdapter(context, currencySelectItemDataList);
                mAdapter.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        mSelectedIndex = position;
                    }
                });

                mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
                mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                mRecyclerView.setAdapter(mAdapter);


                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setCancelable(false);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            // 如果用户不需要而关闭对话框，默认选择第一个
                            if (mSelectedIndex == -1) {
                                mSelectedIndex = 0;
                            }
                        }
                    });
                }
                mAlertDialog = builder.create();
                mAlertDialog.setView(view);
                if (mAlertDialog.getWindow() != null) {
                    mAlertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                }
                mAlertDialog.show();
            }
        });
    }

    @Override
    public Integer call() throws Exception {
        while (mSelectedIndex == -1) {
            Thread.sleep(1000);
            mLoopCount++;
            if (mLoopCount > 30) {
                mSelectedIndex = 1;
            }
        }

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if (mAlertDialog != null && mAlertDialog.isShowing()) {
                    mAlertDialog.dismiss();
                    mAlertDialog = null;
                }
            }
        });

        return mSelectedIndex;
    }

    public static class CurrencySelectItemData {
        private Currency mCurrency;
        private String mTitle;
        private String mSummary;

        public CurrencySelectItemData(Currency currency, String title, String summary) {
            this.mCurrency = currency;
            this.mTitle = title;
            this.mSummary = summary;
        }

        public Currency getCurrency() {
            return mCurrency;
        }

        public void setCurrency(Currency currency) {
            this.mCurrency = currency;
        }

        public String getTitle() {
            return mTitle;
        }

        public void setTitle(String title) {
            this.mTitle = title;
        }

        public String getSummary() {
            return mSummary;
        }

        public void setSummary(String summary) {
            this.mSummary = summary;
        }
    }

    class CurrencyRecyclerViewAdapter extends RecyclerView.Adapter<CurrencyItemViewHolder> {
        private List<CurrencySelectItemData> mItemsSource;
        private Context mContext;
        private LayoutInflater mLayoutInflater;
        private AdapterView.OnItemClickListener mOnItemClickListener;

        public CurrencyRecyclerViewAdapter(Context context, List<CurrencySelectItemData> itemsSource) {
            mContext = context;
            mLayoutInflater = LayoutInflater.from(mContext);
            mItemsSource = itemsSource;
        }

        @Override
        public CurrencyItemViewHolder onCreateViewHolder(ViewGroup parent, int position) {
            return new CurrencyItemViewHolder(mLayoutInflater.inflate(R.layout.dialog_currency_select_list_item, parent, false));
        }

        @Override
        public void onBindViewHolder(CurrencyItemViewHolder holder, final int position) {
            CurrencySelectItemData itemData = mItemsSource.get(position);
            holder.mTvTitle.setText(itemData.getTitle());
            holder.mTvSummary.setText(itemData.getSummary());
            if (StringUtils.isEmpty(itemData.getSummary())) {
                holder.mTvSummary.setVisibility(View.GONE);
            }


            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemClickListener.onItemClick(null, null, position, 0);
                }
            });
        }

        @Override
        public int getItemCount() {
            return mItemsSource.size();
        }

        public void setOnItemClickListener(AdapterView.OnItemClickListener listener) {
            this.mOnItemClickListener = listener;
        }
    }

    class CurrencyItemViewHolder extends RecyclerView.ViewHolder {
        private TextView mTvTitle;
        private TextView mTvSummary;

        public CurrencyItemViewHolder(View itemView) {
            super(itemView);

            mTvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            mTvSummary = (TextView) itemView.findViewById(R.id.tv_summary);
        }
    }
}
