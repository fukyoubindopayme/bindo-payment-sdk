package com.bindo.paymentsdk.v3.examples.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.bindo.paymentsdk.v3.pay.common.TransactionData;
import com.bindo.paymentsdk.v3.pay.common.database.models.Currency;
import com.bindo.paymentsdk.v3.pay.common.database.models.LocalBIN;
import com.bindo.paymentsdk.v3.pay.common.util.Log;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Date;

public class SimpleDbHelper extends SQLiteOpenHelper {
    private static final String TAG = SimpleDbHelper.class.getSimpleName();
    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "payment-sdk-v3.db";

    private static final Class[] TABLE_ENTRIES = new Class[] {
            LocalBIN.class,
            Currency.class,
            TransactionData.class,
    };

    public SimpleDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        for (Class clazz : TABLE_ENTRIES) {
            String tableName = clazz.getSimpleName();
            String sqlCreateEntries = "CREATE TABLE " + tableName +" ";

            sqlCreateEntries += " (";
            sqlCreateEntries += "  _ID INTEGER PRIMARY KEY ";
            Field[] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                // 设置成员可访问权限
                field.setAccessible(true);
                try {
                    String fieldKey = field.getName();
                    if (Modifier.isFinal(field.getModifiers())) {
                        continue;
                    }
                    // 仅将以下几种类型添加到 Schema
                    if (field.getType() == boolean.class ||
                        field.getType() ==  byte.class ||
                        field.getType() ==  short.class ||
                        field.getType() == int.class ||
                        field.getType() ==  long.class ||
                        field.getType() == float.class ||
                        field.getType() == double.class ||
                        field.getType() ==  String.class ||
                        field.getType() ==  Date.class) {
                        sqlCreateEntries += " , " + fieldKey + " TEXT";
                    }
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
            }

            sqlCreateEntries += " )";

            Log.d(TAG, "execSQL: "+ sqlCreateEntries);
            try {
                // TODO: this masks table create errors, should be removed, see SimpleTable.clear()
                db.execSQL(sqlCreateEntries);
            } catch (Exception e) {
                Log.w(e.getMessage());
            }
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        for (Class clazz: TABLE_ENTRIES) {
            String tableName = clazz.getSimpleName();

            String sqlDeleteEntries = "DROP TABLE IF EXISTS " + tableName;
            // This database is only a cache for online data, so its upgrade policy is
            // to simply to discard the data and start over
            db.execSQL(sqlDeleteEntries);
        }
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
