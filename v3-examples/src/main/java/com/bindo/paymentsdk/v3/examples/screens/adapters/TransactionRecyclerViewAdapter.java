package com.bindo.paymentsdk.v3.examples.screens.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bindo.paymentsdk.v3.examples.R;
import com.bindo.paymentsdk.v3.examples.utilities.CurrencyCodeToSymbolUtil;
import com.bindo.paymentsdk.v3.pay.common.TransactionData;
import com.bindo.paymentsdk.v3.pay.common.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class TransactionRecyclerViewAdapter extends RecyclerView.Adapter<TransactionRecyclerViewAdapter.TransactionItemViewHolder> {

    private List<TransactionData> mItemsSource;
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private OnItemClickListener mOnItemClickListener;

    public TransactionRecyclerViewAdapter(Context context) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(mContext);
        mItemsSource = new ArrayList<>();
    }

    @Override
    public TransactionItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TransactionItemViewHolder(mLayoutInflater.inflate(R.layout.activity_transaction_history_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final TransactionItemViewHolder holder, final int position) {
        TransactionData transactionData = mItemsSource.get(position);

        String transactionSystemTraceAuditNumber = String.valueOf(transactionData.getTransactionSystemTraceAuditNumber());
        String transactionDateAndTime = "";
        String transactionType = "";
        String transactionCurrencySymbol = "";
        String transactionAmount = String.valueOf(transactionData.getTransactionAmount());
        if (!StringUtils.isEmpty(transactionData.getTransactionDateAndTime())) {
            SimpleDateFormat sdfDateAndTime = new SimpleDateFormat("yyMMddhhmmss");
            try {
                transactionDateAndTime = sdfDateAndTime.parse(transactionData.getTransactionDateAndTime()).toLocaleString();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        if (!StringUtils.isEmpty(transactionData.getTransactionCurrencyCode())) {
            transactionCurrencySymbol = CurrencyCodeToSymbolUtil.convert(transactionData.getTransactionCurrencyCode());
        }
        if (!StringUtils.isEmpty(transactionData.getTransactionType())) {
            transactionType = transactionData.getTransactionType();
        }

        holder.mTvStan.setText(String.format("#%s", transactionSystemTraceAuditNumber));
        holder.mTvDateAndTime.setText(transactionDateAndTime);
        holder.mTvType.setText(transactionType);
        holder.mTvCurrency.setText(transactionCurrencySymbol);
        holder.mTvAmount.setText(transactionAmount);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onListItemClick(holder.itemView, position, getItemId(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItemsSource.size();
    }

    public List<TransactionData> getItemsSource() {
        return mItemsSource;
    }

    public void setItemsSource(List<TransactionData> mItemsSource) {
        this.mItemsSource = mItemsSource;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
    }

    public static interface OnItemClickListener {
        void onListItemClick(View v, int position, long id);
    }

    public static class TransactionItemViewHolder extends RecyclerView.ViewHolder {
        private TextView mTvStan;
        private TextView mTvDateAndTime;
        private TextView mTvType;
        private TextView mTvCurrency;
        private TextView mTvAmount;

        public TransactionItemViewHolder(View itemView) {
            super(itemView);

            mTvStan = (TextView) itemView.findViewById(R.id.tv_stan);
            mTvDateAndTime = (TextView) itemView.findViewById(R.id.tv_date_and_time);
            mTvType = (TextView) itemView.findViewById(R.id.tv_type);
            mTvCurrency = (TextView) itemView.findViewById(R.id.tv_currency);
            mTvAmount = (TextView) itemView.findViewById(R.id.tv_amount);
        }
    }
}
