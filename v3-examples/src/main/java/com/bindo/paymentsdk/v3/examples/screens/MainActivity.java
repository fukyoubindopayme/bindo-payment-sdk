package com.bindo.paymentsdk.v3.examples.screens;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.bindo.paymentsdk.v3.examples.R;
import com.bindo.paymentsdk.v3.examples.screens.base.BaseActivity;
import com.bindo.paymentsdk.v3.examples.utilities.Constants;
import com.bindo.paymentsdk.v3.examples.modules.payprocessor.ProcessingRules;
import com.bindo.paymentsdk.v3.examples.utilities.LandiServiceUtil;
import com.bindo.paymentsdk.v3.examples.utilities.SimpleStorageManager;
import com.bindo.paymentsdk.v3.pay.common.TransactionType;
import com.bindo.paymentsdk.v3.pay.common.util.StringUtils;

public class MainActivity extends BaseActivity {

    private Spinner mSpGatewayProcessingRules;
    private EditText mTiTransactionAmount;
    private Button mBtnHandleTransactionSale;
    private Button mBtnHandleTransactionPreAuthorization;
    private Button mBtnHandleTransactionPreAuthorizationCompletion;
    private Button mBtnHandleTransactionRefund;
    private Button mBtnTransactionHistory;
    private Button mBtnSettlement;

    private final int WHICH_SWIPE = 0;
    private final int WHICH_SWIPE_EMI = 1;
    private final int WHICH_SWIPE_TAP_INSTALL = 2;
    private final int WHICH_SWIPE_TAP_INSTALL_EMI = 3;
    private final int WHICH_SWIPE_MANUAL = 4;
    private final int WHICH_SWIPE_QRCODE = 5;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        LandiServiceUtil.bindDeviceService(this);
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_main);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
        }

        mSpGatewayProcessingRules = (Spinner) findViewById(R.id.sp_gateway_processing_rules);
        mTiTransactionAmount = (EditText) findViewById(R.id.ti_transaction_amount);
        mBtnHandleTransactionSale = (Button) findViewById(R.id.btn_handle_transaction_sale);
        mBtnHandleTransactionPreAuthorization = (Button) findViewById(R.id.btn_handle_transaction_pre_authorization);
        mBtnHandleTransactionPreAuthorizationCompletion = (Button) findViewById(R.id.btn_handle_transaction_pre_authorization_completion);
        mBtnHandleTransactionRefund = (Button) findViewById(R.id.btn_handle_transaction_refund);
        mBtnTransactionHistory = (Button) findViewById(R.id.btn_transaction_history);
        mBtnSettlement = (Button) findViewById(R.id.btn_settlement);

        int processingRule = SimpleStorageManager.getSharedPreferences().getInt(SimpleStorageManager.STORAGE_PROCESSING_RULE, 0);
        ArrayAdapter<CharSequence> gatewayProcessingRulesAdapter = ArrayAdapter.createFromResource(this, R.array.gateway_processing_rules, android.R.layout.simple_spinner_item);
        gatewayProcessingRulesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpGatewayProcessingRules.setAdapter(gatewayProcessingRulesAdapter);
        mSpGatewayProcessingRules.setSelection(processingRule);
        mSpGatewayProcessingRules.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SharedPreferences.Editor editor = SimpleStorageManager.getSharedPreferences().edit();
                editor.putInt(SimpleStorageManager.STORAGE_PROCESSING_RULE, position);
                editor.apply();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mTiTransactionAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean isValid = !StringUtils.isEmpty(mTiTransactionAmount.getText());
                mBtnHandleTransactionSale.setEnabled(isValid);
                mBtnHandleTransactionPreAuthorization.setEnabled(isValid);
                mBtnHandleTransactionPreAuthorizationCompletion.setEnabled(isValid);
                mBtnHandleTransactionRefund.setEnabled(isValid);
            }
        });
        mBtnHandleTransactionSale.setOnClickListener(mHandleTransactionClickListener);
        mBtnHandleTransactionPreAuthorization.setOnClickListener(mHandleTransactionClickListener);
        mBtnHandleTransactionPreAuthorizationCompletion.setOnClickListener(mHandleTransactionClickListener);
        mBtnHandleTransactionRefund.setOnClickListener(mHandleTransactionClickListener);
        mBtnTransactionHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, TransactionHistoryActivity.class);
                startActivity(intent);
            }
        });
        mBtnSettlement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SettlementActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onDestroy() {
        LandiServiceUtil.unbindDeviceService();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;
        switch (item.getItemId()) {
            case R.id.action_settings:
                intent = new Intent(this, SettingsActivity.class);
                break;
            case R.id.action_dev_tools:
                intent = new Intent(this, DevToolsActivity.class);
                break;
        }
        if (intent != null) {
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    // 处理交易（如授权、预授权、退款、等）点击事件
    private View.OnClickListener mHandleTransactionClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            TransactionType transactionType = TransactionType.SALE;
            double transactionAmount = Double.valueOf(mTiTransactionAmount.getText().toString());

            switch (v.getId()) {
                case R.id.btn_handle_transaction_sale:
                    transactionType = TransactionType.SALE;
                    break;
                case R.id.btn_handle_transaction_pre_authorization:
                    transactionType = TransactionType.PRE_AUTHORIZATION;
                    break;
                case R.id.btn_handle_transaction_pre_authorization_completion:
                    transactionType = TransactionType.PRE_AUTHORIZATION_COMPLETION;
                    break;
                case R.id.btn_handle_transaction_refund:
                    transactionType = TransactionType.REFUND;
                    break;
            }

            final TransactionType finalTransactionType = transactionType;
            final double finalTransactionAmount = transactionAmount;

            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("How to handle?");
            builder.setItems(new String[]{"Swipe", "Swipe - EMI", "Swipe, Tap, Insert", "Swipe, Tap, Insert - EMI", "Manual", "QRCode"}, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = null;
                    switch (which) {
                        // terminal
                        case WHICH_SWIPE:
                        case WHICH_SWIPE_EMI:
                        case WHICH_SWIPE_TAP_INSTALL:
                        case WHICH_SWIPE_TAP_INSTALL_EMI:
                            intent = new Intent(MainActivity.this, HandleProcessByTerminalActivity.class);
                            intent.putExtra("enableDirectCurrencyConversion", true);
                            if (which == WHICH_SWIPE || which == WHICH_SWIPE_EMI) {
                                intent.putExtra("enableAlwaysUseSwipe", true);
                            }
                            if (which == WHICH_SWIPE_EMI || which == WHICH_SWIPE_TAP_INSTALL_EMI) {
                                intent.putExtra("enableInstallmentPlan", true);
                            }
                            break;
                        // manual
                        case WHICH_SWIPE_MANUAL:
                            intent = new Intent(MainActivity.this, HandleProcessByManualActivity.class);
                            break;
                        case WHICH_SWIPE_QRCODE:
                            intent = new Intent(MainActivity.this, HandleProcessByQRCodeActivity.class);
                            break;
                    }
                    if (intent == null) {
                        return;
                    }
                    intent.putExtra(Constants.EXTRA_TRANSACTION_TYPE, finalTransactionType.name());
                    intent.putExtra(Constants.EXTRA_TRANSACTION_AMOUNT, finalTransactionAmount);
                    startActivity(intent);
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        }
    };
}
