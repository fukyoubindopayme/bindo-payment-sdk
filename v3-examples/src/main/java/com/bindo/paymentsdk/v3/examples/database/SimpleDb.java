package com.bindo.paymentsdk.v3.examples.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.bindo.paymentsdk.v3.pay.common.database.Db;
import com.bindo.paymentsdk.v3.pay.common.database.Table;

public class SimpleDb implements Db {
    private static SimpleDb globalSimpleDb;

    public static void initialize(Context context) {
        globalSimpleDb = new SimpleDb(context);
    }

    public static SimpleDb getInstance() {
        return globalSimpleDb;
    }

    private SimpleDbHelper mDbHelper;

    public SimpleDb(Context context) {
        mDbHelper = new SimpleDbHelper(context);
    }

    @Override
    public Table table(Class clazz) {
        return new SimpleTable(clazz);
    }

    public SimpleDbHelper getDbHelper() {
        return mDbHelper;
    }
}
