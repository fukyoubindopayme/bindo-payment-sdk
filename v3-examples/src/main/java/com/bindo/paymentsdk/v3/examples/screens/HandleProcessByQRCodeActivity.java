package com.bindo.paymentsdk.v3.examples.screens;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.bindo.paymentsdk.v3.examples.R;
import com.bindo.paymentsdk.v3.examples.screens.base.BaseActivity;

public class HandleProcessByQRCodeActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_handle_process_by_qrcode);
    }
}
