package com.bindo.paymentsdk.v3.examples.screens;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.bindo.paymentsdk.terminal.BarcodeReader;
import com.bindo.paymentsdk.terminal.Printer;
import com.bindo.paymentsdk.terminal.Terminal;
import com.bindo.paymentsdk.terminal.TerminalModel;
import com.bindo.paymentsdk.v3.examples.R;
import com.bindo.paymentsdk.v3.examples.commons.AMEXReceipt;
import com.bindo.paymentsdk.v3.examples.database.SimpleDb;
import com.bindo.paymentsdk.v3.examples.screens.base.BaseActivity;
import com.bindo.paymentsdk.v3.examples.screens.dialogs.CurrencySelectDialog;
import com.bindo.paymentsdk.v3.pay.TerminalFactory;
import com.bindo.paymentsdk.v3.pay.common.TransactionData;
import com.bindo.paymentsdk.v3.pay.common.database.Db;
import com.bindo.paymentsdk.v3.pay.common.database.Table;
import com.bindo.paymentsdk.v3.pay.common.database.models.Currency;
import com.bindo.paymentsdk.v3.pay.common.database.models.LocalBIN;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DevToolsActivity extends BaseActivity {
    private final String TAG = "DevToolsActivity";

    private Button mBtnTestHtmlToReceipt;
    private Button mBtnTestBarcodeReader;
    private Button mBtnTestCurrencySelectDialog;
    private Button mBtnTestSimpleDbRead;
    private Button mBtnTestSimpleDbWrite;
    private Button mBtnTestPrintAllReceipt;

    private Db mDb;
    private Terminal mTerminal;
    private BarcodeReader barcodeReader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dev_tools);

        mDb = SimpleDb.getInstance();
        mTerminal = TerminalFactory.create(this);

        mBtnTestHtmlToReceipt = (Button) findViewById(R.id.btn_test_html_to_receipt);
        mBtnTestBarcodeReader = (Button) findViewById(R.id.btn_test_barcode_reader);
        mBtnTestCurrencySelectDialog = (Button) findViewById(R.id.btn_test_currency_select_dialog);
        mBtnTestSimpleDbRead = (Button) findViewById(R.id.btn_test_simple_db_read);
        mBtnTestSimpleDbWrite = (Button) findViewById(R.id.btn_test_simple_db_write);
        mBtnTestPrintAllReceipt = (Button) findViewById(R.id.btn_test_print_all_receipt);

        mBtnTestHtmlToReceipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleTestHtmlToReceipt();
            }
        });
        mBtnTestBarcodeReader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleTestBarcodeReader();
            }
        });
        mBtnTestCurrencySelectDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleTestCurrencySelectDialog();
            }
        });
        mBtnTestSimpleDbRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleTestDbRead();
            }
        });
        mBtnTestSimpleDbWrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleTestDbWrite();
            }
        });
        mBtnTestPrintAllReceipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleTestPrintAllReceipt();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mTerminal.getBarcodeReader().stopDecode();
        mTerminal.getBarcodeReader().closeScanner();
    }

    private void handleTestHtmlToReceipt() {
        Intent intent = new Intent(this, HtmlToReceiptActivity.class);
        startActivity(intent);
    }

    private void handleTestBarcodeReader() {
        barcodeReader = mTerminal.getBarcodeReader();
        if (!barcodeReader.isSupported()) {
            Toast.makeText(this, "Barcode reader not supported.", Toast.LENGTH_SHORT).show();
            return;
        }
        barcodeReader.setListener(new BarcodeReader.Listener() {
            @Override
            public void onOpen() {

            }

            @Override
            public void onClose() {

            }

            @Override
            public void onResult(BarcodeReader.Format format, String text) {
                String message = "ON RESULT:\n FORMAT: " + format.name() + "\nTEXT: " + text;
                Toast.makeText(DevToolsActivity.this, message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(String code, String message) {

            }
        });
        barcodeReader.openScanner();
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if(TerminalModel.LANDI_P960.equals(Build.MODEL)) {
            if (event.getKeyCode() == KeyEvent.KEYCODE_FOCUS) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    barcodeReader.startScan();
                } else {
                    barcodeReader.stopScan();
                }
                return true;
            } else {
                return super.dispatchKeyEvent(event);
            }
        } else {
            return super.dispatchKeyEvent(event);
        }
    }

    private void handleTestCurrencySelectDialog() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Currency usdCurrency = new Currency();
                    usdCurrency.setAlphabeticCode("USD");
                    usdCurrency.setConversionRate("0.6");
                    Currency hkdCurrency= new Currency();
                    hkdCurrency.setAlphabeticCode("HKD");
                    hkdCurrency.setConversionRate("1.1");

                    List<Currency> currencyList = new ArrayList<>();
                    currencyList.add(usdCurrency);
                    currencyList.add(hkdCurrency);

                    List<CurrencySelectDialog.CurrencySelectItemData>  currencySelectItemDataList = new ArrayList<>();

                    Currency firstCurrency = currencyList.get(0);

                    for (int i = 0; i < currencyList.size(); i++) {
                        Currency currency = currencyList.get(i);
                        String title = currency.getAlphabeticCode() + String.format("%.2f", 31f);
                        String summary = "";

                        double rate = Double.parseDouble(currency.getConversionRate());

                        if (i == 0) {
                            summary += "FX.Rate\n";
                            summary += String.format("%.2f", 1 / rate) + currency.getAlphabeticCode() + "/" + firstCurrency.getAlphabeticCode();
                        }
                        CurrencySelectDialog.CurrencySelectItemData itemData = new CurrencySelectDialog.CurrencySelectItemData(currency, title, summary);

                        currencySelectItemDataList.add(itemData);
                    }


                    int selectedIndex = new CurrencySelectDialog(DevToolsActivity.this, currencySelectItemDataList).call();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void handleTestDbRead() {
        Object firstObject = mDb.table(TransactionData.class).first();
        if (firstObject != null) {
            Log.d(TAG, firstObject.toString());
        }
        HashMap[] wheres = new HashMap[]{
                new HashMap(){{
                    put(Table.WHERE_TYPE, Table.WHERE_TYPE_EQUAL_TO);
                    put(Table.WHERE_KEY, "transactionDateAndTime");
                    put(Table.WHERE_VALUE, "171013123007");
                }},
        };

        Object whereObject = mDb.table(TransactionData.class).first(wheres, null);
        if (whereObject != null) {
            Log.d(TAG, whereObject.toString());
        }

        List results = mDb.table(LocalBIN.class).all();
        Log.d(TAG, "----------------------------------------UI_THREAD");
        for (int i = 0; i < results.size(); i++) {
            Object object = results.get(i);
            Log.d(TAG, object.toString());
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                List resultsInBackgroundThread = mDb.table(LocalBIN.class).all();

                Log.d(TAG, "----------------------------------------BG_THREAD");
                for (int i = 0; i < resultsInBackgroundThread.size(); i++) {
                    Object object = resultsInBackgroundThread.get(i);
                    LocalBIN localBIN = (LocalBIN) object;
                    Log.d(TAG, object.toString());
                }
            }
        }).start();
    }

    private void handleTestDbWrite() {
        LocalBIN localBIN = new LocalBIN();
        localBIN.setLowPrefixNumber("0000");

        mDb.table(LocalBIN.class).create(localBIN);
        mDb.table(LocalBIN.class).create(localBIN);
    }

    private void handleTestPrintAllReceipt() {
        AMEXReceipt amexReceipt = null;

        List<String> transactions001 = new ArrayList<>();
        transactions001.add("{\"actionCode\":\"000\",\"applicationCryptogram\":\"D9DA160D4BB57243\",\"applicationIdentifier\":\"A000000025010801\",\"applicationLabel\":\"AMERICAN EXPRESS\",\"approvalCode\":\"207353\",\"cardDetectMode\":\"contact\",\"cardExpirationDate\":\"2001\",\"cardholderBillingAmount\":0.0,\"isApproved\":true,\"primaryAccountNumber\":\"374245002741006\",\"tagLengthStringValues\":{\"9F06\":\"A000000025010801\",\"90\":\"0B1B26577A67B9A2F307E06F8A0ABD55C2CD37CB748561602B9D95AB9F9FB80FB8BEAF23A55C5C699195AF555BE3C2B924D7020BFC727D88D44317BC1899A1EDCF46D7DB66CB79295C796ABED56C45764520054E7D97D74412D724480BDDFD1F8DB90CCFA783912DB7824E4E22C3D0E6A3F1F07AAD8CBEF1E5FF224687B945B56B4BF639FA9C40DC8D6329A11B074386AC54703AC4DD9E79F86CC0D523C2B9BDA2808E05A5A2D885B60FD4BB4C5811F9\",\"5F2A\":\"0344\",\"84\":\"A000000025010801\",\"9B\":\"F800\",\"4F\":\"A000000025010801\",\"5F25\":\"150201\",\"9F1B\":\"00003A98\",\"9F03\":\"000000000000\",\"9F07\":\"FF00\",\"5F34\":\"00\",\"5F24\":\"200131\",\"81\":\"00000C1C\",\"8D\":\"8A029F02069F03069F1A0295055F2A029A039C019F3704\",\"9F32\":\"03\",\"8F\":\"C9\",\"9F40\":\"6000F0A001\",\"5F2D\":\"656E\",\"9F26\":\"D9DA160D4BB57243\",\"9F35\":\"22\",\"94\":\"080101000802020108030300080505001002030018030300\",\"9F1E\":\"3132333435363738\",\"8A\":\"3030\",\"9F08\":\"0001\",\"9F41\":\"00000035\",\"9F4A\":\"82\",\"9F42\":\"0840\",\"9F04\":\"00000000\",\"9F0E\":\"0000000000\",\"8C\":\"9F02069F03069F1A0295055F2A029A039C019F3704\",\"91\":\"CCC2647EF918F71A3030\",\"93\":\"08B89762CBA45A03BA13EA5F974103F23F3D7012BD0868E55DC124771C8561F12D5CDC92A9D753DE9BB82EF136CD1F631B6D6D80A3109AD2593F7101E29B5CAFE8B3EE51BE92FC535175022A4676B33467902F710F249643E0221579F819F73EC58E20CF191FD18CA0C8AF71387C4183F27BC30174DF8A1888E0FAB5529DC954D36924181B5C423B642DDF10C45D86A0\",\"8E\":\"000000000000000042015E035F0300000000000000000000\",\"9F09\":\"0001\",\"9F45\":\"DAC1\",\"57\":\"374245002741006D200122115021234500000F\",\"9F36\":\"0001\",\"9C\":\"00\",\"87\":\"01\",\"5A\":\"374245002741006F\",\"9F10\":\"06020103602000\",\"9F33\":\"E0B8C8\",\"5F28\":\"0840\",\"9A\":\"171110\",\"9F0D\":\"FC50ECA800\",\"DF01\":\"00\",\"82\":\"5C00\",\"9F02\":\"000000003100\",\"9F27\":\"40\",\"9F34\":\"5E0300\",\"9F1A\":\"0344\",\"9F37\":\"17D11917\",\"9F0F\":\"FC78FCF800\",\"9F21\":\"115421\",\"95\":\"0200000000\",\"50\":\"414D45524943414E2045585052455353\",\"92\":\"B48FA1DD\"},\"terminalVerificationResult\":\"0200000000\",\"transactionAmount\":31.0,\"transactionCurrencyCode\":\"344\",\"transactionDate\":\"171110\",\"transactionDateAndTime\":\"171110115421\",\"transactionOptOut\":false,\"transactionPinVerified\":false,\"transactionPyc\":false,\"transactionRequestSignature\":true,\"transactionStatusInformation\":\"F800\",\"transactionSystemTraceAuditNumber\":35,\"transactionTime\":\"115421\",\"transactionType\":\"AUTHORIZATION\"}");
        transactions001.add("{\"actionCode\":\"000\",\"applicationCryptogram\":\"\",\"applicationIdentifier\":\"\",\"applicationLabel\":\"\",\"approvalCode\":\"208134\",\"cardDetectMode\":\"fallback_swipe\",\"cardExpirationDate\":\"2001\",\"cardholderBillingAmount\":0.0,\"isApproved\":true,\"primaryAccountNumber\":\"374245003731006\",\"terminalVerificationResult\":\"0000000000\",\"transactionAmount\":36.0,\"transactionCurrencyCode\":\"344\",\"transactionDate\":\"171110\",\"transactionDateAndTime\":\"171110115607\",\"transactionOptOut\":false,\"transactionPinVerified\":false,\"transactionPyc\":false,\"transactionRequestSignature\":true,\"transactionStatusInformation\":\"0000\",\"transactionSystemTraceAuditNumber\":36,\"transactionTime\":\"115607\",\"transactionType\":\"AUTHORIZATION\"}");
        transactions001.add("{\"actionCode\":\"000\",\"applicationCryptogram\":\"F1BEE89E16E3AC1A\",\"applicationIdentifier\":\"A000000025010801\",\"applicationLabel\":\"AMERICAN EXPRESS\",\"approvalCode\":\"212337\",\"cardDetectMode\":\"contact\",\"cardExpirationDate\":\"2001\",\"cardholderBillingAmount\":0.0,\"isApproved\":true,\"primaryAccountNumber\":\"374245001721009\",\"tagLengthStringValues\":{\"9F06\":\"A000000025010801\",\"90\":\"0B1B26577A67B9A2F307E06F8A0ABD55C2CD37CB748561602B9D95AB9F9FB80FB8BEAF23A55C5C699195AF555BE3C2B924D7020BFC727D88D44317BC1899A1EDCF46D7DB66CB79295C796ABED56C45764520054E7D97D74412D724480BDDFD1F8DB90CCFA783912DB7824E4E22C3D0E6A3F1F07AAD8CBEF1E5FF224687B945B56B4BF639FA9C40DC8D6329A11B074386AC54703AC4DD9E79F86CC0D523C2B9BDA2808E05A5A2D885B60FD4BB4C5811F9\",\"5F2A\":\"0344\",\"84\":\"A000000025010801\",\"9B\":\"FC00\",\"4F\":\"A000000025010801\",\"5F25\":\"150201\",\"9F1B\":\"00003A98\",\"9F03\":\"000000000000\",\"9F07\":\"FF00\",\"5F34\":\"02\",\"5F24\":\"200131\",\"81\":\"00000CE4\",\"8D\":\"8A029F02069F03069F1A0295055F2A029A039C019F3704\",\"9F32\":\"03\",\"8F\":\"C9\",\"9F40\":\"6000F0A001\",\"5F2D\":\"656E\",\"9F26\":\"F1BEE89E16E3AC1A\",\"9F35\":\"22\",\"94\":\"080101000802020108030300080505001002030018030300\",\"9F1E\":\"3132333435363738\",\"8A\":\"3030\",\"9F08\":\"0001\",\"9F41\":\"00000038\",\"9F4A\":\"82\",\"9F42\":\"0826\",\"9F04\":\"00000000\",\"9F0E\":\"0000000000\",\"8C\":\"9F02069F03069F1A0295055F2A029A039C019F3704\",\"91\":\"95E2EC8AAF09D7793030\",\"93\":\"0541833D49594FA625A3543AE2CD24081B04980DB24A019DE939FC58BAFBE8B5A66DE6418D83FA388A6CC4B401476B888F6306006ACCCC95AA3C489663C1DD39A0AEF4B5EA43E1AB2EF8095FBB5755AD290E491BA3219687361A1EAC52674AB39AA3DCFE30A8E666502AE35F71EACBF00FED99D226E1ED6A23D4E9C2D5B1CDEF837299ACCEE8F84BDE22570C7D5DC85F\",\"8E\":\"0000000000000000420141035E035F030000000000000000\",\"9F09\":\"0001\",\"9F45\":\"DAC1\",\"57\":\"374245001721009D200120115021234500000F\",\"9F36\":\"0401\",\"9C\":\"00\",\"9F17\":\"03\",\"87\":\"01\",\"5A\":\"374245001721009F\",\"9F10\":\"06020103640000\",\"9F33\":\"E0B8C8\",\"5F28\":\"0826\",\"9A\":\"171110\",\"9F0D\":\"FC50ECA800\",\"DF01\":\"00\",\"82\":\"5C00\",\"9F02\":\"000000003300\",\"9F27\":\"40\",\"9F34\":\"410302\",\"9F1A\":\"0344\",\"9F37\":\"D79335AB\",\"9F0F\":\"FC78FCF800\",\"9F21\":\"120508\",\"95\":\"0200000800\",\"50\":\"414D45524943414E2045585052455353\",\"92\":\"B48FA1DD\"},\"terminalVerificationResult\":\"0200000800\",\"transactionAmount\":33.0,\"transactionCurrencyCode\":\"344\",\"transactionDate\":\"171110\",\"transactionDateAndTime\":\"171110120508\",\"transactionOptOut\":false,\"transactionPinVerified\":true,\"transactionPyc\":false,\"transactionRequestSignature\":false,\"transactionStatusInformation\":\"FC00\",\"transactionSystemTraceAuditNumber\":38,\"transactionTime\":\"120508\",\"transactionType\":\"AUTHORIZATION\"}");
        transactions001.add("{\"applicationCryptogram\":\"EC05A38A11D4C281\",\"applicationIdentifier\":\"A000000025010801\",\"applicationLabel\":\"AMERICAN EXPRESS\",\"cardDetectMode\":\"contact\",\"cardExpirationDate\":\"2001\",\"cardholderBillingAmount\":0.0,\"isApproved\":true,\"primaryAccountNumber\":\"374245001721009\",\"tagLengthStringValues\":{\"9F06\":\"A000000025010801\",\"90\":\"0B1B26577A67B9A2F307E06F8A0ABD55C2CD37CB748561602B9D95AB9F9FB80FB8BEAF23A55C5C699195AF555BE3C2B924D7020BFC727D88D44317BC1899A1EDCF46D7DB66CB79295C796ABED56C45764520054E7D97D74412D724480BDDFD1F8DB90CCFA783912DB7824E4E22C3D0E6A3F1F07AAD8CBEF1E5FF224687B945B56B4BF639FA9C40DC8D6329A11B074386AC54703AC4DD9E79F86CC0D523C2B9BDA2808E05A5A2D885B60FD4BB4C5811F9\",\"5F2A\":\"0344\",\"84\":\"A000000025010801\",\"9B\":\"E800\",\"4F\":\"A000000025010801\",\"5F25\":\"150201\",\"9F1B\":\"00003A98\",\"9F03\":\"000000000000\",\"9F07\":\"FF00\",\"5F34\":\"02\",\"5F24\":\"200131\",\"81\":\"00000CE4\",\"8D\":\"8A029F02069F03069F1A0295055F2A029A039C019F3704\",\"9F32\":\"03\",\"8F\":\"C9\",\"9F40\":\"6000F0A001\",\"5F2D\":\"656E\",\"9F26\":\"EC05A38A11D4C281\",\"9F35\":\"22\",\"94\":\"080101000802020108030300080505001002030018030300\",\"9F1E\":\"3132333435363738\",\"8A\":\"5931\",\"9F08\":\"0001\",\"9F41\":\"00000054\",\"9F4A\":\"82\",\"9F42\":\"0826\",\"9F04\":\"00000000\",\"9F0E\":\"0000000000\",\"8C\":\"9F02069F03069F1A0295055F2A029A039C019F3704\",\"93\":\"0541833D49594FA625A3543AE2CD24081B04980DB24A019DE939FC58BAFBE8B5A66DE6418D83FA388A6CC4B401476B888F6306006ACCCC95AA3C489663C1DD39A0AEF4B5EA43E1AB2EF8095FBB5755AD290E491BA3219687361A1EAC52674AB39AA3DCFE30A8E666502AE35F71EACBF00FED99D226E1ED6A23D4E9C2D5B1CDEF837299ACCEE8F84BDE22570C7D5DC85F\",\"8E\":\"0000000000000000420141035E035F030000000000000000\",\"9F09\":\"0001\",\"9F45\":\"DAC1\",\"57\":\"374245001721009D200120115021234500000F\",\"9F36\":\"0001\",\"9C\":\"20\",\"9F17\":\"03\",\"87\":\"01\",\"5A\":\"374245001721009F\",\"9F10\":\"06020103940000\",\"9F33\":\"E0B8C8\",\"5F28\":\"0826\",\"9A\":\"171110\",\"9F0D\":\"FC50ECA800\",\"DF01\":\"00\",\"82\":\"5C00\",\"9F02\":\"000000003300\",\"9F27\":\"40\",\"9F34\":\"410302\",\"9F1A\":\"0344\",\"9F37\":\"4516EC94\",\"9F0F\":\"FC78FCF800\",\"9F21\":\"023614\",\"95\":\"0200000000\",\"50\":\"414D45524943414E2045585052455353\",\"92\":\"B48FA1DD\"},\"terminalVerificationResult\":\"0200000000\",\"transactionAmount\":33.0,\"transactionCurrencyCode\":\"344\",\"transactionDate\":\"171110\",\"transactionDateAndTime\":\"171110023614\",\"transactionOptOut\":false,\"transactionPinVerified\":true,\"transactionPyc\":false,\"transactionRequestSignature\":false,\"transactionStatusInformation\":\"E800\",\"transactionSystemTraceAuditNumber\":54,\"transactionTime\":\"023614\",\"transactionType\":\"REFUND\"}");
        transactions001.add("{\"actionCode\":\"000\",\"applicationCryptogram\":\"0D07D8EE627AB8AA\",\"applicationIdentifier\":\"A000000025010801\",\"applicationLabel\":\"AMERICAN EXPRESS\",\"approvalCode\":\"213920\",\"cardDetectMode\":\"contact\",\"cardExpirationDate\":\"2001\",\"cardholderBillingAmount\":0.0,\"isApproved\":true,\"primaryAccountNumber\":\"374245002741006\",\"tagLengthStringValues\":{\"9F06\":\"A000000025010801\",\"90\":\"0B1B26577A67B9A2F307E06F8A0ABD55C2CD37CB748561602B9D95AB9F9FB80FB8BEAF23A55C5C699195AF555BE3C2B924D7020BFC727D88D44317BC1899A1EDCF46D7DB66CB79295C796ABED56C45764520054E7D97D74412D724480BDDFD1F8DB90CCFA783912DB7824E4E22C3D0E6A3F1F07AAD8CBEF1E5FF224687B945B56B4BF639FA9C40DC8D6329A11B074386AC54703AC4DD9E79F86CC0D523C2B9BDA2808E05A5A2D885B60FD4BB4C5811F9\",\"5F2A\":\"0344\",\"84\":\"A000000025010801\",\"9B\":\"E800\",\"4F\":\"A000000025010801\",\"5F25\":\"150201\",\"9F1B\":\"00003A98\",\"9F03\":\"000000000000\",\"9F07\":\"FF00\",\"5F34\":\"00\",\"5F24\":\"200131\",\"81\":\"00001770\",\"8D\":\"8A029F02069F03069F1A0295055F2A029A039C019F3704\",\"9F32\":\"03\",\"8F\":\"C9\",\"9F40\":\"6000F0A001\",\"5F2D\":\"656E\",\"9F26\":\"0D07D8EE627AB8AA\",\"9F35\":\"22\",\"94\":\"080101000802020108030300080505001002030018030300\",\"9F1E\":\"3132333435363738\",\"8A\":\"3030\",\"9F08\":\"0001\",\"9F41\":\"00000040\",\"9F4A\":\"82\",\"9F42\":\"0840\",\"9F04\":\"00000000\",\"9F0E\":\"0000000000\",\"8C\":\"9F02069F03069F1A0295055F2A029A039C019F3704\",\"93\":\"08B89762CBA45A03BA13EA5F974103F23F3D7012BD0868E55DC124771C8561F12D5CDC92A9D753DE9BB82EF136CD1F631B6D6D80A3109AD2593F7101E29B5CAFE8B3EE51BE92FC535175022A4676B33467902F710F249643E0221579F819F73EC58E20CF191FD18CA0C8AF71387C4183F27BC30174DF8A1888E0FAB5529DC954D36924181B5C423B642DDF10C45D86A0\",\"8E\":\"000000000000000042015E035F0300000000000000000000\",\"9F09\":\"0001\",\"9F45\":\"DAC1\",\"57\":\"374245002741006D200122115021234500000F\",\"9F36\":\"2801\",\"9C\":\"00\",\"87\":\"01\",\"5A\":\"374245002741006F\",\"9F10\":\"06020103602400\",\"9F33\":\"E0B8C8\",\"5F28\":\"0840\",\"9A\":\"171110\",\"9F0D\":\"FC50ECA800\",\"DF01\":\"00\",\"82\":\"5C00\",\"9F02\":\"000000006000\",\"9F27\":\"40\",\"9F34\":\"5E0300\",\"9F1A\":\"0344\",\"9F37\":\"0028644D\",\"9F0F\":\"FC78FCF800\",\"9F21\":\"120845\",\"95\":\"0200000000\",\"50\":\"414D45524943414E2045585052455353\",\"92\":\"B48FA1DD\"},\"terminalVerificationResult\":\"0200000000\",\"transactionAmount\":60.0,\"transactionCurrencyCode\":\"344\",\"transactionDate\":\"171110\",\"transactionDateAndTime\":\"171110120845\",\"transactionOptOut\":false,\"transactionPinVerified\":false,\"transactionPyc\":false,\"transactionRequestSignature\":true,\"transactionStatusInformation\":\"E800\",\"transactionSystemTraceAuditNumber\":40,\"transactionTime\":\"120845\",\"transactionType\":\"AUTHORIZATION\"}");

        List<String> transactions002 = new ArrayList<>();
        transactions002.add("{\"applicationCryptogram\":\"171BFB8D9163360F\",\"applicationIdentifier\":\"A000000025010801\",\"applicationLabel\":\"AMERICAN EXPRESS\",\"cardDetectMode\":\"contact\",\"cardExpirationDate\":\"2001\",\"cardholderBillingAmount\":0.0,\"isApproved\":true,\"primaryAccountNumber\":\"374245001721009\",\"tagLengthStringValues\":{\"9F06\":\"A000000025010801\",\"90\":\"0B1B26577A67B9A2F307E06F8A0ABD55C2CD37CB748561602B9D95AB9F9FB80FB8BEAF23A55C5C699195AF555BE3C2B924D7020BFC727D88D44317BC1899A1EDCF46D7DB66CB79295C796ABED56C45764520054E7D97D74412D724480BDDFD1F8DB90CCFA783912DB7824E4E22C3D0E6A3F1F07AAD8CBEF1E5FF224687B945B56B4BF639FA9C40DC8D6329A11B074386AC54703AC4DD9E79F86CC0D523C2B9BDA2808E05A5A2D885B60FD4BB4C5811F9\",\"5F2A\":\"0344\",\"84\":\"A000000025010801\",\"9B\":\"E800\",\"4F\":\"A000000025010801\",\"5F25\":\"150201\",\"9F1B\":\"00003A98\",\"9F03\":\"000000000000\",\"9F07\":\"FF00\",\"5F34\":\"02\",\"5F24\":\"200131\",\"81\":\"000014B4\",\"8D\":\"8A029F02069F03069F1A0295055F2A029A039C019F3704\",\"9F32\":\"03\",\"8F\":\"C9\",\"9F40\":\"6000F0A001\",\"5F2D\":\"656E\",\"9F26\":\"171BFB8D9163360F\",\"9F35\":\"22\",\"94\":\"080101000802020108030300080505001002030018030300\",\"9F1E\":\"3132333435363738\",\"8A\":\"5931\",\"9F08\":\"0001\",\"9F41\":\"00000056\",\"9F4A\":\"82\",\"9F42\":\"0826\",\"9F04\":\"00000000\",\"9F0E\":\"0000000000\",\"8C\":\"9F02069F03069F1A0295055F2A029A039C019F3704\",\"93\":\"0541833D49594FA625A3543AE2CD24081B04980DB24A019DE939FC58BAFBE8B5A66DE6418D83FA388A6CC4B401476B888F6306006ACCCC95AA3C489663C1DD39A0AEF4B5EA43E1AB2EF8095FBB5755AD290E491BA3219687361A1EAC52674AB39AA3DCFE30A8E666502AE35F71EACBF00FED99D226E1ED6A23D4E9C2D5B1CDEF837299ACCEE8F84BDE22570C7D5DC85F\",\"8E\":\"0000000000000000420141035E035F030000000000000000\",\"9F09\":\"0001\",\"9F45\":\"DAC1\",\"57\":\"374245001721009D200120115021234500000F\",\"9F36\":\"0001\",\"9C\":\"20\",\"9F17\":\"03\",\"87\":\"01\",\"5A\":\"374245001721009F\",\"9F10\":\"06020103940000\",\"9F33\":\"E0B8C8\",\"5F28\":\"0826\",\"9A\":\"171110\",\"9F0D\":\"FC50ECA800\",\"DF01\":\"00\",\"82\":\"5C00\",\"9F02\":\"000000005300\",\"9F27\":\"40\",\"9F34\":\"410302\",\"9F1A\":\"0344\",\"9F37\":\"446E64F9\",\"9F0F\":\"FC78FCF800\",\"9F21\":\"024522\",\"95\":\"0200000000\",\"50\":\"414D45524943414E2045585052455353\",\"92\":\"B48FA1DD\"},\"terminalVerificationResult\":\"0200000000\",\"transactionAmount\":53.0,\"transactionCurrencyCode\":\"344\",\"transactionDate\":\"171110\",\"transactionDateAndTime\":\"171110024522\",\"transactionOptOut\":false,\"transactionPinVerified\":true,\"transactionPyc\":false,\"transactionRequestSignature\":false,\"transactionStatusInformation\":\"E800\",\"transactionSystemTraceAuditNumber\":56,\"transactionTime\":\"024522\",\"transactionType\":\"REFUND\"}");
        transactions002.add("{\"actionCode\":\"000\",\"applicationCryptogram\":\"\",\"applicationIdentifier\":\"\",\"applicationLabel\":\"\",\"approvalCode\":\"225393\",\"cardDetectMode\":\"swipe\",\"cardExpirationDate\":\"2001\",\"cardholderBillingAmount\":0.0,\"isApproved\":true,\"primaryAccountNumber\":\"373953192351004\",\"terminalVerificationResult\":\"0000000000\",\"transactionAmount\":58.0,\"transactionCurrencyCode\":\"344\",\"transactionDate\":\"171110\",\"transactionDateAndTime\":\"171110123330\",\"transactionOptOut\":false,\"transactionPinVerified\":false,\"transactionPyc\":false,\"transactionRequestSignature\":true,\"transactionStatusInformation\":\"0000\",\"transactionSystemTraceAuditNumber\":42,\"transactionTime\":\"123330\",\"transactionType\":\"AUTHORIZATION\"}");
        transactions002.add("{\"actionCode\":\"000\",\"applicationCryptogram\":\"7FE4DC32CA09ADB4\",\"applicationIdentifier\":\"A000000025010403\",\"applicationLabel\":\"AMERICAN EXPRESS\",\"approvalCode\":\"247635\",\"cardDetectMode\":\"contactless\",\"cardExpirationDate\":\"2001\",\"cardholderBillingAmount\":0.0,\"isApproved\":true,\"primaryAccountNumber\":\"374245001741007\",\"tagLengthStringValues\":{\"9F06\":\"A000000025010403\",\"90\":\"8F58707E3C81B3C4EE99B41296197BF57687823320A9B860015AC4C2AC5BFC98460825A28E56CCF46C051B2187680443A794DEA8F25A29507D38E7DBD13F178A83C64534ECA39CFA7C7BC1416E1842F50B0EB38E2586202C6157F1274DFB441FE4B55A21D161E9506CAD30B4906B71FFAEF4C53CA34926DC325DFA5CBED107BC9691D25BAEC73B0AE16D220EE5AC3DB4\",\"5F2A\":\"0344\",\"84\":\"A000000025010403\",\"9B\":\"E000\",\"4F\":\"A000000025010403\",\"5F25\":\"150201\",\"9F1B\":\"00003A98\",\"9F03\":\"000000000000\",\"9F07\":\"3D00\",\"5F34\":\"00\",\"5F24\":\"200131\",\"9F38\":\"9F3501\",\"81\":\"000004B1\",\"8D\":\"8A029F02069F03069F1A0295055F2A029A039C019F3704\",\"9F32\":\"03\",\"8F\":\"C8\",\"9F40\":\"6000F0A001\",\"5F2D\":\"656E\",\"9F26\":\"7FE4DC32CA09ADB4\",\"9F35\":\"22\",\"9F48\":\"A15B4C656B465136AFF87272E77C0BD3060DDDC9DA85A88A4DC3\",\"94\":\"080101000805050008060601100202001005050020030400\",\"9F6E\":\"D8E00000\",\"9F1E\":\"3132333435363738\",\"9F08\":\"0001\",\"9F41\":\"00000049\",\"9F4A\":\"82\",\"9F42\":\"0826\",\"9F47\":\"03\",\"9F04\":\"00000000\",\"9F0E\":\"0000000000\",\"8C\":\"9F02069F03069F1A0295055F2A029A039C019F3704\",\"9F4C\":\"FE38078DA7A062A2\",\"8E\":\"000000000000000042015E031F02\",\"9F4B\":\"B5B220E61AC9AE02ECC6EBECCC70C27F6BEEBBC41064440D832C5BE20F0FAEF723367DFF29AB0A2A5E79732A691BA608439D1D5CB43FE4D99008CF6610EA3FF27AD8570E93148B79E4601D7EF4D80706189D74CA707334FD0568A089B3DBF95A1D7D8FF50A59F8497635CE7DCD78AA41C8C22ED798CF0DA73E38AC91D1316764\",\"57\":\"374245001741007D200170215021234500000F\",\"9F36\":\"0001\",\"9C\":\"00\",\"87\":\"01\",\"5A\":\"374245001741007F\",\"9F10\":\"06020103A01000\",\"9F33\":\"E0B8C8\",\"5F28\":\"0826\",\"9A\":\"171110\",\"9F0D\":\"F470C49800\",\"DF01\":\"0B\",\"9F46\":\"AACEA0E1986EDE5B51A65FBFEDC58760B54E304DB5A929962B5B54E3394607CDEFCE5C3C057B791E54C8BAC7A73947B3CD078D37EC9B8173B62903B09A5D64BA22E1537060AD48A99C2D4D60C739CE651669BCE4278ED90147696AF6EEFC7CDBBB924F59467ECC3673478F30870FC3D9F8F6A371C5E1C681F60DCA80DAAC70B087C25CD6C5569D7DB74CA994935723FE\",\"82\":\"1D80\",\"9F02\":\"000000001201\",\"9F27\":\"80\",\"9F34\":\"5E0300\",\"9F1A\":\"0344\",\"9F37\":\"0DE65BE1\",\"9F0F\":\"F470C49800\",\"9F21\":\"012052\",\"95\":\"0000008000\",\"50\":\"414D45524943414E2045585052455353\",\"92\":\"86682438E7947E21D3F161FD57ABC3E004B1728A9615C368A60886A5AC5A936FB48FA1DD\"},\"terminalVerificationResult\":\"0000008000\",\"transactionAmount\":12.01,\"transactionCurrencyCode\":\"344\",\"transactionDate\":\"171110\",\"transactionDateAndTime\":\"171110012052\",\"transactionOptOut\":false,\"transactionPinVerified\":false,\"transactionPyc\":false,\"transactionRequestSignature\":true,\"transactionStatusInformation\":\"E000\",\"transactionSystemTraceAuditNumber\":49,\"transactionTime\":\"012052\",\"transactionType\":\"AUTHORIZATION\"}");
        transactions002.add("{\"applicationCryptogram\":\"3E12EA103C3DED19\",\"applicationIdentifier\":\"A000000025010403\",\"applicationLabel\":\"AMERICAN EXPRESS\",\"cardDetectMode\":\"contactless\",\"cardExpirationDate\":\"2001\",\"cardholderBillingAmount\":0.0,\"isApproved\":true,\"primaryAccountNumber\":\"374245002741006\",\"tagLengthStringValues\":{\"9F06\":\"A000000025010403\",\"90\":\"7C487B635EF866D54244BFFC0C6AF4733188F71841A7057BF3B07C7A5886E15EF4C20B3DDAA427DC4083436D8FBA6DFDEDA1A52926A3EC3D89CFD792BC4CC298EB38F5670A9D3060D8F9AFE4A21BDD211AC835D48ABCA3B26AE4E882809B7F7ED7595D8FAF1D4056B6FB428226A47E1E33190A8BF03D7467610B2B6E56128AA979390BFC70A72EE0F80C24B8E1EB5DFC4D2AC2CC63F643E85B678A7461C9F4CC0DA1C53CD055B38CBF332171C544FFB720C16608DE6A7ED5FE02E184518EC8EA0357B064D57F7038D9BE5719034B9B2FCFEBDAA37BC7DB44B6ED3990C601FF1CB886C3E1CBA3D05EC83E6D985EA5F42B10A2B487F7805F0B\",\"5F2A\":\"0344\",\"84\":\"A000000025010403\",\"9B\":\"A000\",\"4F\":\"A000000025010403\",\"5F25\":\"150201\",\"9F1B\":\"00003A98\",\"9F03\":\"000000000000\",\"9F07\":\"3D00\",\"5F34\":\"00\",\"5F24\":\"200131\",\"9F38\":\"9F3501\",\"81\":\"00000066\",\"8D\":\"8A029F02069F03069F1A0295055F2A029A039C019F3704\",\"9F32\":\"03\",\"8F\":\"CA\",\"9F40\":\"6000F0A001\",\"5F2D\":\"6672\",\"9F26\":\"3E12EA103C3DED19\",\"9F35\":\"22\",\"94\":\"080101000805050008060601100202001005050020030400\",\"9F6E\":\"D8E00000\",\"9F1E\":\"3132333435363738\",\"8A\":\"5931\",\"9F08\":\"0001\",\"9F41\":\"00000057\",\"9F4A\":\"82\",\"9F42\":\"0978\",\"9F47\":\"03\",\"9F04\":\"00000000\",\"9F0E\":\"0000000000\",\"8C\":\"9F02069F03069F1A0295055F2A029A039C019F3704\",\"9F4C\":\"5A15FD8A0035C08E\",\"8E\":\"000000000000000042011F02\",\"9F4B\":\"77DDE17C3920D887EC7904697C17153A1D41606FE06B38F44FB6D6F6B87648F83C85AC876252B28FC58C744A278335A826B3EAF44DCA8CAD54CA24E976A5DE05219EB792BB91F1D99E4268F135FCED94C823263B17BBC02DE2C967334A02254A8D901CDD2F3EEB1F6EB761A6A3369348B3465A94A6F152DF9DF7D919412DB539\",\"57\":\"374245002741006D200170215021234500000F\",\"9F36\":\"0701\",\"9C\":\"00\",\"87\":\"01\",\"5A\":\"374245002741006F\",\"9F10\":\"06020103901000\",\"9F33\":\"E0B8C8\",\"5F28\":\"0250\",\"9A\":\"171110\",\"9F0D\":\"F470C49800\",\"DF01\":\"0B\",\"9F46\":\"86960542866C72C82C1AA2568C682CDA7562FB3DCA3C516C515E08333619184250D46B8C8F5B073B40D00AE33B254430526C9398AD7F53D7FEC6495E76ED002B8C23386BD1E9D63CE36D64C930D9A44A7C1BB1CB77490597804D4F162B03542DA5466044EAB9C5B0FEE701D21FAC882DC45817481FE54978CCFB4C393A720EF80FF32FDBAE5E83E1A29EEA4FC320B1255372D6EB055DECD807A273215288C9EABC1D6D5E57A50703FC6AC3201F082AE6\",\"82\":\"1D80\",\"9F02\":\"000000000102\",\"9F27\":\"40\",\"9F34\":\"1F0202\",\"9F1A\":\"0344\",\"9F37\":\"5595A4AB\",\"9F0F\":\"F470C49800\",\"9F21\":\"024725\",\"95\":\"0000000000\",\"50\":\"414D45524943414E2045585052455353\"},\"terminalVerificationResult\":\"0000000000\",\"transactionAmount\":1.02,\"transactionCurrencyCode\":\"344\",\"transactionDate\":\"171110\",\"transactionDateAndTime\":\"171110024725\",\"transactionOptOut\":false,\"transactionPinVerified\":false,\"transactionPyc\":false,\"transactionRequestSignature\":false,\"transactionStatusInformation\":\"A000\",\"transactionSystemTraceAuditNumber\":57,\"transactionTime\":\"024725\",\"transactionType\":\"AUTHORIZATION\"}");
        transactions002.add("{\"actionCode\":\"000\",\"applicationCryptogram\":\"2C22A56C542A16E7\",\"applicationIdentifier\":\"A000000025010403\",\"applicationLabel\":\"AMERICAN EXPRESS\",\"approvalCode\":\"248967\",\"cardDetectMode\":\"contactless\",\"cardExpirationDate\":\"2001\",\"cardholderBillingAmount\":0.0,\"isApproved\":true,\"primaryAccountNumber\":\"374245001721009\",\"tagLengthStringValues\":{\"9F06\":\"A000000025010403\",\"90\":\"029F211AAB3F9657D3C346C2E56CDFC2FC5E7C4D1162EE8ED90F395970603F3E4BBE499874067DB843BAB476FAF3D32461A3221DAFDF2AA53755481D70C610CF5ECD3519963B7829ADCA3724648D956156750C9C280D61CDEA1554111EE80BA3E2A68979FBE8D6D6344B442A7BE7BA238CDC7A5BD6B1159F996F20F5CD5B0901C3E2D6CEF16AC5B23DE22147BA1A4FBE\",\"5F2A\":\"0344\",\"84\":\"A000000025010403\",\"9B\":\"2000\",\"4F\":\"A000000025010403\",\"5F25\":\"150201\",\"9F1B\":\"00003A98\",\"9F03\":\"000000000000\",\"9F07\":\"FF00\",\"5F34\":\"00\",\"5F24\":\"200131\",\"81\":\"000000C9\",\"8D\":\"8A029F3704\",\"9F32\":\"03\",\"8F\":\"C8\",\"9F40\":\"6000F0A001\",\"5F2D\":\"656E\",\"9F26\":\"2C22A56C542A16E7\",\"9F35\":\"22\",\"9F48\":\"834BB266639E17923C93\",\"94\":\"080101000803030108050500100202001004040020010200\",\"9F6E\":\"D8E00000\",\"9F1E\":\"3132333435363738\",\"9F08\":\"FF01\",\"9F41\":\"00000051\",\"9F4A\":\"82\",\"9F42\":\"0826\",\"9F47\":\"03\",\"9F04\":\"00000000\",\"9F0E\":\"0000000000\",\"8C\":\"9F3704\",\"8E\":\"0000000000000000\",\"57\":\"374245001721009D200110100000000000000F\",\"9F36\":\"1301\",\"9C\":\"00\",\"87\":\"01\",\"5A\":\"374245001721009F\",\"9F10\":\"06020203A00000\",\"9F33\":\"E0B8C8\",\"5F28\":\"0826\",\"9A\":\"171110\",\"9F0D\":\"F0F0009800\",\"DF01\":\"0A\",\"9F46\":\"00E19C0AB0DCE9D647E3F3C866AD023F0BE39F9BE5CB5E9EED9540402FD38EB50D7F4E6AFCE77320ED95471A7B2C6A8B8065043973D467489247FAD019B2F174258729A9A88048D4B42D1EAA4ADE1D8694ADCDF53933F41368E30E6B3C631CB00415CC5B85449406C380E3AD243E6A068C227F354F76265AC7DFDC92F978F22B\",\"82\":\"1800\",\"9F02\":\"000000000201\",\"9F27\":\"80\",\"9F34\":\"3F0000\",\"9F1A\":\"0344\",\"9F37\":\"00001408\",\"9F0F\":\"F0F0009800\",\"9F21\":\"012352\",\"95\":\"A000000000\",\"50\":\"414D45524943414E2045585052455353\"},\"terminalVerificationResult\":\"A000000000\",\"transactionAmount\":2.01,\"transactionCurrencyCode\":\"344\",\"transactionDate\":\"171110\",\"transactionDateAndTime\":\"171110012352\",\"transactionOptOut\":false,\"transactionPinVerified\":false,\"transactionPyc\":false,\"transactionRequestSignature\":false,\"transactionStatusInformation\":\"2000\",\"transactionSystemTraceAuditNumber\":51,\"transactionTime\":\"012352\",\"transactionType\":\"AUTHORIZATION\"}");
        transactions002.add("{\"actionCode\":\"000\",\"applicationCryptogram\":\"E32B0E3B220C4BD6\",\"applicationIdentifier\":\"A000000025010901\",\"applicationLabel\":\"AMERICAN EXPRESS\",\"approvalCode\":\"249804\",\"cardDetectMode\":\"contactless\",\"cardExpirationDate\":\"2001\",\"cardholderBillingAmount\":0.0,\"isApproved\":true,\"primaryAccountNumber\":\"374245002751005\",\"tagLengthStringValues\":{\"9F06\":\"A000000025010901\",\"90\":\"029F211AAB3F9657D3C346C2E56CDFC2FC5E7C4D1162EE8ED90F395970603F3E4BBE499874067DB843BAB476FAF3D32461A3221DAFDF2AA53755481D70C610CF5ECD3519963B7829ADCA3724648D956156750C9C280D61CDEA1554111EE80BA3E2A68979FBE8D6D6344B442A7BE7BA238CDC7A5BD6B1159F996F20F5CD5B0901C3E2D6CEF16AC5B23DE22147BA1A4FBE\",\"5F2A\":\"0344\",\"84\":\"A000000025010901\",\"9B\":\"E000\",\"4F\":\"A000000025010901\",\"5F25\":\"150201\",\"9F1B\":\"00003A98\",\"9F03\":\"000000000000\",\"9F07\":\"3D00\",\"5F34\":\"00\",\"5F24\":\"200131\",\"9F38\":\"9F3501\",\"81\":\"000004B3\",\"8D\":\"8A029F02069F03069F1A0295055F2A029A039C019F3704\",\"9F32\":\"03\",\"8F\":\"C8\",\"9F40\":\"6000F0A001\",\"5F2D\":\"656E\",\"9F26\":\"E32B0E3B220C4BD6\",\"9F35\":\"22\",\"9F48\":\"163292873F5362A6A87EAD577DEE55E70F322865253B29E23FA5\",\"94\":\"0801010020010501\",\"9F6E\":\"D8E00000\",\"9F1E\":\"3132333435363738\",\"9F08\":\"0001\",\"9F41\":\"00000052\",\"9F4A\":\"82\",\"9F42\":\"0978\",\"9F47\":\"03\",\"9F04\":\"00000000\",\"9F0E\":\"0000000000\",\"8C\":\"9F02069F03069F1A0295055F2A029A039C019F3704\",\"9F4C\":\"55036034E5A7EEBE\",\"8E\":\"0000000000000000420141031F02\",\"9F4B\":\"27E50D37347E8C15BA7E52E27ECB7E95D8A15C3F0AB3D502864EEA5DD05747E4F3320A14B9E025FE2FC17667D057BEE7B6F669458D59884A205EA2DA3DAB46585A77F24803ED26D255C7C902E1A846C90C6C1610F3C0D775CB5D2B2838A7491479EDED3C15BAEA8929B4849734B2D3CC\",\"57\":\"374245002751005D200170115021234500000F\",\"9F36\":\"1801\",\"9C\":\"00\",\"87\":\"01\",\"5A\":\"374245002751005F\",\"9F10\":\"06020103A4B002\",\"9F33\":\"E0B8C8\",\"5F28\":\"0620\",\"9A\":\"171110\",\"9F0D\":\"F850ECA000\",\"DF01\":\"00\",\"9F46\":\"97B3253B02A1C61AEE0D8B2717B40B7D9B28451FACE643E8D86CEC2711D82B4ADD704B1A8F1AFEC4D911ABB8A4BF62FE3EBDECB4D640CDA56C68E10E5A48045E5756CCAC158953348B5B99522A2FEFC26EB533887C57D5459136B5AA15806288DFA4E5D3534F9C99E1BDD0A15B1ADB20AFBAE4E452557D83FF8456E5BD4A1523\",\"82\":\"19C0\",\"9F02\":\"000000001203\",\"9F27\":\"80\",\"9F34\":\"010302\",\"9F1A\":\"0344\",\"9F37\":\"22F9B3A4\",\"9F0F\":\"F078FCF800\",\"9F21\":\"012534\",\"95\":\"0000008000\",\"50\":\"414D45524943414E2045585052455353\",\"92\":\"B74FBB0CE30EBBDA51E925D0F6970B1AAAB6C597\"},\"terminalVerificationResult\":\"0000008000\",\"transactionAmount\":12.03,\"transactionCurrencyCode\":\"344\",\"transactionDate\":\"171110\",\"transactionDateAndTime\":\"171110012534\",\"transactionOptOut\":false,\"transactionPinVerified\":true,\"transactionPyc\":false,\"transactionRequestSignature\":false,\"transactionStatusInformation\":\"E000\",\"transactionSystemTraceAuditNumber\":52,\"transactionTime\":\"012534\",\"transactionType\":\"AUTHORIZATION\"}");
        transactions002.add("{\"actionCode\":\"000\",\"applicationCryptogram\":\"ADE38BB81E8DBAA0\",\"applicationIdentifier\":\"A000000025010403\",\"applicationLabel\":\"AMERICAN EXPRESS\",\"approvalCode\":\"250989\",\"cardDetectMode\":\"contactless\",\"cardExpirationDate\":\"2001\",\"cardholderBillingAmount\":0.0,\"isApproved\":true,\"primaryAccountNumber\":\"374245001771004\",\"tagLengthStringValues\":{\"9F06\":\"A000000025010403\",\"90\":\"029F211AAB3F9657D3C346C2E56CDFC2FC5E7C4D1162EE8ED90F395970603F3E4BBE499874067DB843BAB476FAF3D32461A3221DAFDF2AA53755481D70C610CF5ECD3519963B7829ADCA3724648D956156750C9C280D61CDEA1554111EE80BA3E2A68979FBE8D6D6344B442A7BE7BA238CDC7A5BD6B1159F996F20F5CD5B0901C3E2D6CEF16AC5B23DE22147BA1A4FBE\",\"5F2A\":\"0344\",\"84\":\"A000000025010403\",\"9B\":\"A000\",\"4F\":\"A000000025010403\",\"5F25\":\"150201\",\"9F1B\":\"00003A98\",\"9F03\":\"000000000000\",\"9F07\":\"3D00\",\"5F34\":\"00\",\"5F24\":\"200131\",\"9F38\":\"9F3501\",\"81\":\"000003F4\",\"8D\":\"8A029F02069F03069F1A0295055F2A029A039C019F3704\",\"9F32\":\"03\",\"8F\":\"C8\",\"9F40\":\"6000F0A001\",\"5F2D\":\"656E\",\"9F26\":\"ADE38BB81E8DBAA0\",\"9F35\":\"22\",\"9F48\":\"A617B6ED998C7BEB453EA846834DB05233FE5049F792E839B199\",\"94\":\"080101000805050008060601100202001005050020030400\",\"9F6E\":\"D8E00000\",\"9F1E\":\"3132333435363738\",\"9F08\":\"0001\",\"9F41\":\"00000053\",\"9F4A\":\"82\",\"9F42\":\"0826\",\"9F47\":\"03\",\"9F04\":\"00000000\",\"9F0E\":\"0000000000\",\"8C\":\"9F02069F03069F1A0295055F2A029A039C019F3704\",\"9F4C\":\"7E303431AC7172C2\",\"8E\":\"0000000000000000\",\"9F4B\":\"8D01FBFF01C876890C663AFC4219DEA2C94F178CDB12DF0CDF56C15AC0B39CACA594A6B81142869830CFF59E748918C1121F8636B2B1BB940FD48BEABCBA3F20883993683CC34B2CB24C5FB5AC876825D2A26CE73A20EDC146DB67533B320B30D9C4666FBA8603E9BE7841625AA378CF\",\"57\":\"374245001771004D200170215021234500000F\",\"9F36\":\"2301\",\"9C\":\"00\",\"87\":\"01\",\"5A\":\"374245001771004F\",\"9F10\":\"06020103A01000\",\"9F33\":\"E0B8C8\",\"5F28\":\"0826\",\"9A\":\"171110\",\"9F0D\":\"F470C49800\",\"DF01\":\"0B\",\"9F46\":\"722EC2C492EAFB8611D1675AF82E7F7D23825B6D7434B6C1B889F5F40D4F277B2492BCA224702C459441989ACDAAB38409A7374196D64507F81DFEFA98CC98C6753EDC1CE560F7857AB2BCA212E18953226B0EC702F90F7008F5F21C1361E9C964AC8658C8B105873D6D85E26435C090D43CA0282F8BB68F077379FDCE63EFCF\",\"82\":\"0D80\",\"9F02\":\"000000001012\",\"9F27\":\"80\",\"9F34\":\"3F0000\",\"9F1A\":\"0344\",\"9F37\":\"C170181F\",\"9F0F\":\"F470C49800\",\"9F21\":\"012811\",\"95\":\"0000800000\",\"50\":\"414D45524943414E2045585052455353\",\"92\":\"B74FBB0CE30EBBDA51E925D0F6970B1AAAB6C597\"},\"terminalVerificationResult\":\"0000800000\",\"transactionAmount\":10.12,\"transactionCurrencyCode\":\"344\",\"transactionDate\":\"171110\",\"transactionDateAndTime\":\"171110012811\",\"transactionOptOut\":false,\"transactionPinVerified\":false,\"transactionPyc\":false,\"transactionRequestSignature\":false,\"transactionStatusInformation\":\"A000\",\"transactionSystemTraceAuditNumber\":53,\"transactionTime\":\"012811\",\"transactionType\":\"AUTHORIZATION\"}");


        Printer printer = mTerminal.getBuiltInPrinter();

        Printer.Paper allInOnePaper = new Printer.Paper();

//        Request request001 = new Request();
//        request001.setGatewayConfigs(new HashMap<String, String>() {{
//            // TBT Identification#000001
//            put(AmericanExpressGateway.CONFIG_REQUEST_CARD_ACCEPTOR_TERMINAL_ID, "12345678");
//            put(AmericanExpressGateway.CONFIG_REQUEST_CARD_ACCEPTOR_ID_CODE, "4380171421");
//            put(AmericanExpressGateway.CONFIG_SDK_MERCHANT_NUMBER, "4380171421");
//        }});

        amexReceipt = new AMEXReceipt("4380171421", "12345678");

        for (int i = 0; i < transactions001.size(); i++) {
            Gson gson = new Gson();
            TransactionData transactionData = gson.fromJson(transactions001.get(i), TransactionData.class);
//            transactionData.setRequest(request001);

            Printer.Paper paper = amexReceipt.createApprovedReceipt(transactionData);

            allInOnePaper.pushCommand(Printer.Paper.COMMAND_LINE);
            allInOnePaper.pushCommand(Printer.Paper.COMMAND_TEXT, String.format("No.%d", i + 1));
            allInOnePaper.pushCommand(Printer.Paper.COMMAND_LINE);
            allInOnePaper.pushCommand(Printer.Paper.COMMAND_NEW_LINE);
            allInOnePaper.pushCommand(Printer.Paper.COMMAND_NEW_LINE);
            allInOnePaper.getCommands().addAll(paper.getCommands());
        }

//        Request request002 = new Request();
//        request002.setGatewayConfigs(new HashMap<String, String>() {{
//            // TBT Identification#000002
//            put(AmericanExpressGateway.CONFIG_REQUEST_CARD_ACCEPTOR_TERMINAL_ID, "87654321");
//            put(AmericanExpressGateway.CONFIG_REQUEST_CARD_ACCEPTOR_ID_CODE, "3236718441");
//            put(AmericanExpressGateway.CONFIG_SDK_MERCHANT_NUMBER, "3236718441");
//        }});

        amexReceipt = new AMEXReceipt("3236718441", "87654321");


        for (int i = 0; i < transactions002.size(); i++) {
            Gson gson = new Gson();
            TransactionData transactionData = gson.fromJson(transactions002.get(i), TransactionData.class);
//            transactionData.setRequest(request002);

            Printer.Paper paper = amexReceipt.createApprovedReceipt(transactionData);

            allInOnePaper.pushCommand(Printer.Paper.COMMAND_LINE);
            allInOnePaper.pushCommand(Printer.Paper.COMMAND_TEXT, String.format("No.%d", i + 1));
            allInOnePaper.pushCommand(Printer.Paper.COMMAND_LINE);
            allInOnePaper.pushCommand(Printer.Paper.COMMAND_NEW_LINE);
            allInOnePaper.pushCommand(Printer.Paper.COMMAND_NEW_LINE);
            allInOnePaper.getCommands().addAll(paper.getCommands());
        }

        printer.setListener(new Printer.Listener() {
            @Override
            public void onPrinted() {
                Toast.makeText(DevToolsActivity.this, "Printed", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(String code, String message) {
                Toast.makeText(DevToolsActivity.this, "Error:\n code: " + code + "\nmessage: " + message, Toast.LENGTH_SHORT).show();
            }
        });
        printer.print(allInOnePaper);

    }
}
