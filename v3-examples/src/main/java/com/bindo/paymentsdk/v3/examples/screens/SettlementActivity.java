package com.bindo.paymentsdk.v3.examples.screens;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.bindo.paymentsdk.v3.examples.R;
import com.bindo.paymentsdk.v3.examples.database.SimpleDb;
import com.bindo.paymentsdk.v3.examples.screens.base.BaseActivity;
import com.bindo.paymentsdk.v3.examples.utilities.SimpleStorageManager;
import com.bindo.paymentsdk.v3.pay.common.TransactionData;
import com.bindo.paymentsdk.v3.pay.common.database.Db;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Request;
import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gateway.AmexSubmissionGateway;

import java.util.List;

class RetrieveFeedTask extends AsyncTask {

    @Override
    protected Object doInBackground(Object[] objects) {
        ((AmexSubmissionGateway) objects[0]).send((Request)objects[1]);
        return null;
    }
}

public class SettlementActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_settlement);

        //System.out.println(AmexSubmissionGatewayTest.class.getResource(".").getPath());
        //InputStream keyIn = AmexSubmissionGatewayTest.class.getResourceAsStream("./BINLABHKGTST_INC_PK01.txt");
        //System.out.println(keyIn == null);

        Db mDb = SimpleDb.getInstance();
        List<TransactionData> transactionDataList = mDb.table(TransactionData.class).all();
        Log.d("TRANSACTION TO SETTLE: " + transactionDataList.size());
        if (transactionDataList.size() > 0) {
            //@SuppressLint("ResourceType") String publicKey = String.valueOf(getResources().openRawResource(R.string.amex_public_key));
            String publicKey = String.valueOf(getResources().getString(R.string.amex_public_key));
            Log.i("PUBLIC KEY", publicKey);
            Request request = new Request();
            request.setTransactionDataList(transactionDataList);
            request.setAmexFileSequence(SimpleStorageManager.nextAmexFileSequence());
            AmexSubmissionGateway amexSubmissionGateway = new AmexSubmissionGateway(null, getFilesDir(), publicKey);
            new RetrieveFeedTask().execute(amexSubmissionGateway, request);
            //amexSubmissionGateway.send(transactionDataList);
        }
    }
}
