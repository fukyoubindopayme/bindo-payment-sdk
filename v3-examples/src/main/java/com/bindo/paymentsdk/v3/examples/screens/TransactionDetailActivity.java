package com.bindo.paymentsdk.v3.examples.screens;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bindo.paymentsdk.v3.examples.R;
import com.bindo.paymentsdk.v3.examples.database.SimpleDb;
import com.bindo.paymentsdk.v3.examples.screens.base.BaseActivity;
import com.bindo.paymentsdk.v3.examples.utilities.Constants;
import com.bindo.paymentsdk.v3.examples.utilities.CurrencyCodeToSymbolUtil;
import com.bindo.paymentsdk.v3.examples.utilities.SimpleStorageManager;
import com.bindo.paymentsdk.v3.pay.common.ReversalType;
import com.bindo.paymentsdk.v3.pay.common.TransactionData;
import com.bindo.paymentsdk.v3.pay.common.TransactionType;
import com.bindo.paymentsdk.v3.pay.common.database.Db;
import com.bindo.paymentsdk.v3.pay.common.database.Table;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Request;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Response;
import com.bindo.paymentsdk.v3.pay.common.util.StringUtils;
import com.bindo.paymentsdk.v3.pay.gateway.amex.gateway.AmexGateway;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class TransactionDetailActivity extends BaseActivity {

    private TextView mTvStan;
    private TextView mTvDateAndTime;
    private TextView mTvType;
    private TextView mTvCurrency;
    private TextView mTvAmount;


    private Button mBtnHandleTransactionAuthorizationVoid;
    private Button mBtnHandleTransactionPreAuthorizationVoid;
    private Button mBtnHandleTransactionPreAuthorizationCompletion;

    private Db mDb;
    private List<TransactionData> mTransactions;

    private int mTransactionStan;
    private TransactionData mTransactionData;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_transaction_detail);

        mTransactionStan = getIntent().getIntExtra(Constants.EXTRA_TRANSACTION_STAN, 0);

        mDb = SimpleDb.getInstance();

        mTvStan = (TextView) findViewById(R.id.tv_stan);
        mTvDateAndTime = (TextView) findViewById(R.id.tv_date_and_time);
        mTvType = (TextView) findViewById(R.id.tv_type);
        mTvCurrency = (TextView) findViewById(R.id.tv_currency);
        mTvAmount = (TextView) findViewById(R.id.tv_amount);

        mBtnHandleTransactionAuthorizationVoid = (Button) findViewById(R.id.btn_handle_transaction_authorization_void);
        mBtnHandleTransactionPreAuthorizationVoid = (Button) findViewById(R.id.btn_handle_transaction_pre_authorization_void);
        mBtnHandleTransactionPreAuthorizationCompletion = (Button) findViewById(R.id.btn_handle_transaction_pre_authorization_completion);


        mBtnHandleTransactionAuthorizationVoid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleTransactionAuthorizationVoid();
            }
        });
        mBtnHandleTransactionPreAuthorizationVoid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleTransactionPreAuthorizationVoid();
            }
        });
        mBtnHandleTransactionPreAuthorizationCompletion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleTransactionPreAuthorizationCompletion();
            }
        });

        initData();
    }

    private void initData() {
        HashMap[] wheres = new HashMap[]{
                new HashMap() {{
                    put(Table.WHERE_TYPE, Table.WHERE_TYPE_EQUAL_TO);
                    put(Table.WHERE_KEY, "transactionSystemTraceAuditNumber");
                    put(Table.WHERE_VALUE, mTransactionStan);
                }},
        };
        //Object firstObject = mDb.table(TransactionData.class).first(wheres, null);
        mTransactions = mDb.table(TransactionData.class).all();

        // select last!
        Object firstObject = mTransactions.get(mTransactions.size() - 1);

        if (firstObject == null) {
            Toast.makeText(this, "Can not find transactionData, Stan is " + mTransactionStan, Toast.LENGTH_SHORT).show();
            return;
        }

        TransactionData transactionData = (TransactionData) firstObject;
        mTransactionData = transactionData;

        String transactionSystemTraceAuditNumber = String.valueOf(transactionData.getTransactionSystemTraceAuditNumber());
        String transactionDateAndTime = "";
        String transactionType = "";
        String transactionCurrencySymbol = "";
        String transactionAmount = String.valueOf(transactionData.getTransactionAmount());
        if (!StringUtils.isEmpty(transactionData.getTransactionDateAndTime())) {
            SimpleDateFormat sdfDateAndTime = new SimpleDateFormat("yyMMddhhmmss");
            try {
                transactionDateAndTime = sdfDateAndTime.parse(transactionData.getTransactionDateAndTime()).toLocaleString();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        if (!StringUtils.isEmpty(transactionData.getTransactionCurrencyCode())) {
            transactionCurrencySymbol = CurrencyCodeToSymbolUtil.convert(transactionData.getTransactionCurrencyCode());
        }
        if (!StringUtils.isEmpty(transactionData.getTransactionType())) {
            transactionType = transactionData.getTransactionType();
            switch (TransactionType.valueOf(transactionType)) {
                case AUTHORIZATION:
                    mBtnHandleTransactionPreAuthorizationVoid.setVisibility(View.GONE);
                    mBtnHandleTransactionPreAuthorizationCompletion.setVisibility(View.GONE);
                    break;
                case PRE_AUTHORIZATION:
                    mBtnHandleTransactionAuthorizationVoid.setVisibility(View.GONE);
                    break;
            }
        }

        mTvStan.setText(String.format("#%s", transactionSystemTraceAuditNumber));
        mTvDateAndTime.setText(transactionDateAndTime);
        mTvType.setText(transactionType);
        mTvCurrency.setText(transactionCurrencySymbol);
        mTvAmount.setText(transactionAmount);
    }

    private void handleTransactionAuthorizationVoid() {
        Toast.makeText(this, "Authorization void.", Toast.LENGTH_SHORT).show();

        final AmexGateway gateway = new AmexGateway(null);
        final Request request = new Request();

        request.setOriginalTransactionSystemTraceAuditNumber(mTransactionData.getTransactionSystemTraceAuditNumber());
        request.setOriginalTransactionDateAndTime(mTransactionData.getTransactionDateAndTime());

        request.setTransactionType(TransactionType.REVERSAL);
        request.setTransactionSystemTraceAuditNumber(SimpleStorageManager.stanNext());
        request.setTransactionDateAndTime(new Date());
        request.setTransactionAmount(mTransactionData.getTransactionAmount());
        request.setTransactionCurrencyCode(mTransactionData.getTransactionCurrencyCode());
        request.setAcquirerReferenceData(mTransactionData.getTransactionId());
        request.setPrimaryAccountNumber(mTransactionData.getPrimaryAccountNumber());
        request.setCardDetectMode(mTransactionData.getCardDetectMode());
        request.setCardExpirationDate(mTransactionData.getCardExpirationDate());
        request.setReversalType(ReversalType.MERCHANT_INITIATED);

        new Thread(new Runnable() {

            @Override
            public void run() {
                Response response = gateway.sendRequest(request);
                if (response.getActionCode().equals("400")) {
                    // delete last!
                    mTransactions.remove(mTransactions.size() - 1);
                    mDb.table(TransactionData.class).delete("transactionSystemTraceAuditNumber", String.valueOf(mTransactionStan));
                }
            }
        }).start();

    }

    private void handleTransactionPreAuthorizationVoid() {
        Toast.makeText(this, "Pre authorization void.", Toast.LENGTH_SHORT).show();
    }

    private void handleTransactionPreAuthorizationCompletion() {
        Toast.makeText(this, "Pre authorization completion.", Toast.LENGTH_SHORT).show();
    }
}
