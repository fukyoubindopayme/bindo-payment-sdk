package com.bindo.paymentsdk.v3.examples.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.bindo.paymentsdk.v3.pay.common.database.Table;
import com.bindo.paymentsdk.v3.pay.common.util.Log;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class SimpleTable<T> implements Table {
    private static final String TAG = SimpleTable.class.getSimpleName();

    private Class<T> tableClass;
    private String tableName;


    public SimpleTable(Class<T> clazz) {
        this.tableClass = clazz;
        this.tableName = clazz.getSimpleName();
    }
    @Override
    public void create(Object object) {
        SQLiteDatabase db = SimpleDb.getInstance().getDbHelper().getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        Class clazz = object.getClass();

        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            // 设置成员可访问权限
            field.setAccessible(true);
            try {
                String fieldKey = field.getName();
                Object fieldValue = field.get(object);
                if (Modifier.isFinal(field.getModifiers())) {
                    continue;
                }
                // 忽略不为 null 的成员
                if (fieldValue == null) {
                    continue;
                }
                if (field.getType() == boolean.class ||
                    field.getType() ==  byte.class ||
                    field.getType() ==  short.class ||
                    field.getType() == int.class ||
                    field.getType() ==  long.class ||
                    field.getType() == float.class ||
                    field.getType() == double.class ||
                    field.getType() ==  String.class ||
                    field.getType() ==  Date.class) {
                    contentValues.put(fieldKey, fieldValue.toString());
                }
            } catch (IllegalAccessException | IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        long newRowId = db.insert(this.tableName, null, contentValues);
    }

    @Override
    public void update(Object object) {

    }

    @Override
    public List<T> all() {
        return this.get(null, null);
    }

    @Override
    public List<T> get(HashMap[] wheres, HashMap[] orderBy) {
        SQLiteDatabase db = SimpleDb.getInstance().getDbHelper().getReadableDatabase();

        ArrayList results = new ArrayList();

        String[] qColumns = null;
        String qSelection = "1 = 1";
        String[] qSelectionArgs = new String[wheres == null ? 0 : wheres.length];
        String qGroupBy = null;
        String qHaving = null;
        String qOrderBy = null;
        String qLimit = null;

        for (int i = 0; wheres != null && i < wheres.length; i++) {
            HashMap hashMap = wheres[i];
            String whereType = (String) hashMap.get(Table.WHERE_TYPE);
            String whereKey = (String) hashMap.get(Table.WHERE_KEY);
            Object whereValue = hashMap.get(Table.WHERE_VALUE);

//            int whereIntegerValue = 0;
//            double whereDoubleValue = 0;
            String whereStringValue = null;

//            if (whereValue instanceof Integer) {
//                whereIntegerValue = (int) whereValue;
//            } else if (whereValue instanceof Double) {
//                whereDoubleValue = (double) whereValue;
//            } else if (whereValue instanceof String) {
//                whereStringValue = (String) whereValue;
//            }
            if (whereValue instanceof String) {
                whereStringValue = (String) whereValue;
            }

            if (whereValue != null) {
                switch (whereType) {
                    case WHERE_TYPE_EQUAL_TO:
                        qSelection += "AND " + whereKey + " = ?";
                        qSelectionArgs[i] = whereStringValue;
                        break;
                }
            }
        }

        Log.d(TAG, "query: ");
        Cursor cursor = db.query(
                this.tableName,
                qColumns,
                qSelection,
                qSelectionArgs,
                qGroupBy,
                qHaving,
                qOrderBy,
                qLimit
        );


        while (cursor.moveToNext()) {
            T resultObject = null;
            try {
                resultObject = this.tableClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }
            Field[] fields = this.tableClass.getDeclaredFields();
            for (Field field : fields) {
                // 设置成员可访问权限
                field.setAccessible(true);
                try {
                    String fieldKey = field.getName();
                    // 忽略 不存在的 Field Name
                    if (cursor.getColumnIndex(fieldKey) == -1) {
                        continue;
                    }
                    if (Modifier.isFinal(field.getModifiers())) {
                        continue;
                    }

                    String fieldValue = cursor.getString(cursor.getColumnIndexOrThrow(fieldKey));

                    if (field.getType() == boolean.class) {
                        field.set(resultObject, Boolean.valueOf(fieldValue));
                    }
                    if (field.getType() == byte.class) {
                        field.set(resultObject, Byte.valueOf(fieldValue));
                    }
                    if (field.getType() == short.class) {
                        field.set(resultObject, Short.valueOf(fieldValue));
                    }
                    if (field.getType() == int.class) {
                        field.set(resultObject, Integer.valueOf(fieldValue));
                    }
                    if (field.getType() == long.class) {
                        field.set(resultObject, Long.valueOf(fieldValue));
                    }
                    if (field.getType() == float.class) {
                        field.set(resultObject, Float.valueOf(fieldValue));
                    }
                    if (field.getType() == double.class) {
                        field.set(resultObject, Double.valueOf(fieldValue));
                    }
                    if (field.getType() == String.class) {
                        field.set(resultObject, fieldValue);
                    }
                    if (field.getType() == Date.class) {
                        field.set(resultObject, Date.parse(fieldValue));
                    }
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            results.add(resultObject);
        }
        return results;
    }

    @Override
    public Object first() {
        return null;
    }

    @Override
    public Object first(HashMap[] wheres, HashMap[] orderBy) {
        return null;
    }

    @Override
    public void clear() {
        SQLiteDatabase db = SimpleDb.getInstance().getDbHelper().getWritableDatabase();
        try {
            String sqlCommand = "DROP TABLE IF EXISTS '" + this.tableName + "'";
            db.execSQL(sqlCommand);
        } catch (Exception e) {
            Log.w(e.getMessage());
        }
        try {
            // TODO: only recreate this table, not all of them
            SimpleDb.getInstance().getDbHelper().onCreate(db);
        } catch (Exception e) {
            Log.w(e.getMessage());
        }
    }

    @Override
    public void delete(String key, String value) {
        String sqlCommand = "DELETE FROM '" + this.tableName + "' WHERE " + key + " = '" + value + "'";
        SQLiteDatabase db = SimpleDb.getInstance().getDbHelper().getWritableDatabase();
        db.execSQL(sqlCommand);
    }
}
