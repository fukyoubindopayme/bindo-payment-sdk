package com.bindo.paymentsdk.payx.emv.landi;

import android.util.Log;

import com.bindo.paymentsdk.payx.emv.results.CardholderVerificationResult;
import com.bindo.paymentsdk.payx.emv.pinpad.PinPad;
import com.landicorp.android.eptapi.pinpad.Pinpad;

public class LandiPinPad extends PinPad {
    private final String TAG = "LandiPinPad";

    private Pinpad mLegacyPinPad;

    private CardholderVerificationResult mCardholderVerificationResult = null;

    public LandiPinPad() {
        mLegacyPinPad = new Pinpad(2, "IPP");
    }

    public Pinpad getLegacyPinPad() {
        return mLegacyPinPad;
    }

    @Override
    public CardholderVerificationResult verifyPinByPinPad(boolean isOnlinePin, byte[] lengthLimit) {
        mCardholderVerificationResult = null;
        if (!mLegacyPinPad.isOpen()) {
            mLegacyPinPad.open();
        }

        if (mLegacyPinPad.isInputing()) {
            mLegacyPinPad.cancelInput();
        }

        mLegacyPinPad.setPinLengthLimit(lengthLimit);
        mLegacyPinPad.setOnPinInputListener(new Pinpad.OnPinInputListener() {
            @Override
            public void onInput(int pinLength, int value) {
                Log.d(TAG, "Value: " + value);
            }

            @Override
            public void onConfirm(byte[] data, boolean isNonePin) {
                mCardholderVerificationResult = CardholderVerificationResult.CONFIRM;
            }

            @Override
            public void onCancel() {
                mCardholderVerificationResult = CardholderVerificationResult.CANCEL;
            }

            @Override
            public void onError(int code) {
                mCardholderVerificationResult = CardholderVerificationResult.ERROR;
            }
        });

        if (isOnlinePin) {
            mLegacyPinPad.inputOnlinePin(Pinpad.KEYOFFSET_PINKEY);
        } else {
            mLegacyPinPad.startOfflinePinEntry(0);
        }

        while (mCardholderVerificationResult == null) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                // Ignore
            }
        }
        return mCardholderVerificationResult;
    }


}
