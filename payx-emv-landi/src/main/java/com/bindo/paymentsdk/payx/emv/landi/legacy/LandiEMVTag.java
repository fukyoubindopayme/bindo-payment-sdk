package com.bindo.paymentsdk.payx.emv.landi.legacy;

import com.landicorp.emvkernel.tag.EMVTag;

public class LandiEMVTag implements EMVTag {
    public static final String A_TAG_TM_TRANS_LIMIT = "DF8124";
    public static final String A_TAG_TM_CVM_LIMIT = "DF8126";
    public static final String A_TAG_TM_FLOOR_LIMIT = "DF8123";
}
