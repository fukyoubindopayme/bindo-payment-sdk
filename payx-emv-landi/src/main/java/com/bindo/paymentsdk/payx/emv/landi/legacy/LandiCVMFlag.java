package com.bindo.paymentsdk.payx.emv.landi.legacy;

import com.bindo.paymentsdk.payx.emv.core.enmus.CVMFlag;

public class LandiCVMFlag implements com.landicorp.emvkernel.data.CVMFlag {

    public static CVMFlag toPayXCVMFlag(int cvmFlag) {
        switch (cvmFlag) {
            case NOCVM:
                return CVMFlag.NO_CVM;
            case OFFLINEPIN:
                return CVMFlag.OFFLINE_PIN;
            case ONLINEPIN:
                return CVMFlag.ONLINE_PIN;
            case SIGNATURE:
                return CVMFlag.SIGNATURE;
        }
        return null;
    }
}
