package com.bindo.paymentsdk.payx.emv.landi;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import com.bindo.paymentsdk.payx.emv.core.Application;
import com.bindo.paymentsdk.payx.emv.core.ApplicationData;
import com.bindo.paymentsdk.payx.emv.core.CAPK;
import com.bindo.paymentsdk.payx.emv.core.Candidate;
import com.bindo.paymentsdk.payx.emv.EMVCore;
import com.bindo.paymentsdk.payx.emv.core.enmus.KernelID;
import com.bindo.paymentsdk.payx.emv.results.CallIssuerConfirmResult;
import com.bindo.paymentsdk.payx.emv.results.OnlineProcessingResult;
import com.bindo.paymentsdk.payx.emv.results.OnlineReversalProcessingResult;
import com.bindo.paymentsdk.payx.emv.results.EMVTransactionResultData;
import com.bindo.paymentsdk.payx.emv.core.enmus.ACType;
import com.bindo.paymentsdk.payx.emv.core.enmus.CVMFlag;
import com.bindo.paymentsdk.payx.emv.results.CardholderVerificationResult;
import com.bindo.paymentsdk.payx.emv.results.OnlineOfflineDecisionResult;
import com.bindo.paymentsdk.payx.emv.results.EMVTransactionResultCode;
import com.bindo.paymentsdk.payx.emv.landi.legacy.LandiACType;
import com.bindo.paymentsdk.payx.emv.landi.legacy.LandiCVMFlag;
import com.bindo.paymentsdk.payx.emv.landi.legacy.LandiEMVTag;
import com.bindo.paymentsdk.payx.emv.landi.legacy.LandiKernelID;
import com.bindo.paymentsdk.payx.emv.landi.legacy.LandiResultCode;
import com.bindo.paymentsdk.v3.pay.common.emv.enums.EMVTag;
import com.bindo.paymentsdk.v3.pay.common.emv.enums.FlowType;
import com.bindo.paymentsdk.v3.pay.common.legacy.CreditCard;
import com.bindo.paymentsdk.v3.pay.common.legacy.ReversalReasonConstant;
import com.bindo.paymentsdk.v3.pay.common.legacy.enums.CardReadMode;
import com.bindo.paymentsdk.v3.pay.common.util.DataConversion;
import com.bindo.paymentsdk.v3.pay.common.util.Hex;
import com.bindo.paymentsdk.v3.pay.common.util.JSONUtils;
import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.common.util.StringUtils;
import com.landicorp.android.eptapi.device.Beeper;
import com.landicorp.android.eptapi.emv.process.data.CAPKey;
import com.landicorp.android.eptapi.emv.process.data.CVMData;
import com.landicorp.android.eptapi.emv.process.data.CandidateAIDInfo;
import com.landicorp.android.eptapi.emv.process.data.FinalSelectData;
import com.landicorp.android.eptapi.emv.process.data.RecordData;
import com.landicorp.android.eptapi.emv.process.data.TransactionData;
import com.landicorp.android.eptapi.emv.process.data.VerifyOfflinePinResult;
import com.landicorp.android.eptapi.pinpad.Pinpad;
import com.landicorp.android.eptapi.utils.TLV;
import com.landicorp.android.eptapi.utils.TLVList;
import com.landicorp.emvkernel.EMVEngine;
import com.landicorp.emvkernel.EMVFactory;
import com.landicorp.emvkernel.ResultCode;
import com.landicorp.emvkernel.data.ActionFlag;
import com.landicorp.emvkernel.data.CardType;
import com.landicorp.emvkernel.data.EMVKernelID;
import com.landicorp.emvkernel.data.KernelINS;
import com.landicorp.emvkernel.data.LogLevel;
import com.landicorp.emvkernel.data.MagCardData;
import com.landicorp.emvkernel.data.MessageID;
import com.landicorp.pinpad.OfflinePinVerifyResult;
import com.landicorp.pinpad.PinVerifyCfg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LandiEMVCore extends EMVCore {
    private static final String TAG = "LandiEMVCore";

    private Pinpad mPinPad;
    private EMVEngine mEMVEngine;

    private int mKernelID = -1;
    private int mCheckType = -1;
    private byte mFlowType = -1;
    private int mDetectRetryCount = 0;
    private Handler mMainHandler;

    private CardReadMode mTransCardReadMode;
    private OnlineProcessingResult mTransOnlineProcessingResult;
    private OnlineReversalProcessingResult mTransOnlineReversalProcessingResult;


    public LandiEMVCore(Context context, LandiPinPad landiPinPad) {
        EMVFactory.init(context);
        mPinPad = landiPinPad.getLegacyPinPad();
        mEMVEngine = EMVFactory.getDefault();
        mEMVEngine.setLogLevel(LogLevel.REALTIME);

        mMainHandler = new Handler(Looper.getMainLooper());
    }

    @Override
    public void startProcess() {
        this.startProcess(false);
    }

    @Override
    public void startProcess(final boolean isFallbackSwipe) {
        this.startProcess(isFallbackSwipe, 0);
    }

    @Override
    public void startProcess(final boolean isFallbackSwipe, final int detectRetryCount) {
        if (mListener == null) {
            return;
        }

        mFlowType = -1;
        mCheckType = -1;
        mFlowType = -1;

        mTransCardReadMode = null;
        mTransOnlineProcessingResult = null;
        mTransOnlineReversalProcessingResult = null;
        this.mDetectRetryCount = detectRetryCount;

        final Bundle options = new Bundle();

        options.putBoolean("supportICCard", isSupportICCard());
        options.putBoolean("supportRFCard", isSupportRFCard());
        options.putBoolean("supportMagCard", isSupportMagCard());
        options.putBoolean("flagICCLog", false);
        options.putBoolean("flagRecovery", false);
        options.putInt("timeout", 120);
        options.putByte("flagPSE", (byte) 0x00);

        mEMVEngine.stopProcess();
        mEMVEngine.startProcess(options, new EMVEngine.EMVEventHandler() {

            @Override
            public void onInitEMV() {
                Log.d(TAG, ">>>onInitEMV");
                mListener.onReset();

                TLVList tlvList = new TLVList();
                tlvList.addTLV(TLV.fromData("DF918155", new byte[]{0x01}));
                tlvList.addTLV(TLV.fromData("9F1E", new byte[]{0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38}));
                mEMVEngine.setTLVList(EMVKernelID.AMEX, tlvList.toString());
            }

            @Override
            public void onWaitCard(int flag) {
                Log.d(TAG, ">>>onWaitCard");
            }

            @Override
            public void onCardChecked(int checkType, final MagCardData trackInfo) {
                Log.d(TAG, ">>>onCardChecked");
                mCheckType = checkType;
                CardReadMode cardReadMode = CardReadMode.MANUAL;
                switch (checkType) {
                    case CardType.CARD_INSERT:
                        cardReadMode = CardReadMode.CONTACT;
                        break;
                    case CardType.CARD_RF:
                        cardReadMode = CardReadMode.CONTACTLESS;
                        break;
                    case CardType.CARD_SWIPE:
                        cardReadMode = !isFallbackSwipe ? CardReadMode.SWIPE : CardReadMode.FALLBACK_SWIPE;
                        break;
                }

                mTransCardReadMode = cardReadMode;
                final CreditCard creditCard = new CreditCard();
                creditCard.setCardReadMode(cardReadMode);

                if (checkType == CardType.CARD_SWIPE) {
                    if (trackInfo != null) {
                        String cardNumber = trackInfo.getPAN();
                        String track1 = trackInfo.getTrack1();
                        String track2 = trackInfo.getTrack2();

                        if (!StringUtils.isEmpty(cardNumber) && cardNumber.length() == 17) {
                            cardNumber = cardNumber.substring(2);
                            track2 = track2.substring(2);
                        }

                        CreditCard.MagData magData = new CreditCard.MagData(track1, track2.replace('=','D'));
                        creditCard.setCardNumber(cardNumber);
                        creditCard.setExpireDate(trackInfo.getExpireDate().substring(0, 4));
                        creditCard.setServiceCode(trackInfo.getServiceCode());
                        creditCard.setMagData(magData);
                    }
                }

                Beeper.startBeep(200);
                mListener.onCardDetected(cardReadMode, creditCard);

                if (cardReadMode == CardReadMode.SWIPE || cardReadMode == CardReadMode.FALLBACK_SWIPE) {
                    OnlineOfflineDecisionResult onlineOfflineDecisionResult = mListener.onlineOfflineDecision(cardReadMode, creditCard);

                    if (onlineOfflineDecisionResult == OnlineOfflineDecisionResult.UNSURE) {
                        return;
                    }
                    if (onlineOfflineDecisionResult == OnlineOfflineDecisionResult.ONLINE) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                mTransOnlineProcessingResult = mListener.performOnlineProcessing(creditCard, new HashMap<String, byte[]>());

                                EMVTransactionResultCode emvTransactionResultCode = EMVTransactionResultCode.ERROR_UNKNOWN;
                                EMVTransactionResultData emvTransactionResultData = new EMVTransactionResultData();

                                emvTransactionResultData.setCardReadMode(mTransCardReadMode);
                                emvTransactionResultData.setCreditCard(creditCard);

                                if (mTransOnlineProcessingResult != null) {
                                    if (mTransOnlineProcessingResult.isRequestTimeout()) {
                                        // 当联机请求超时时，冲正请求
                                        int reversalReason = ReversalReasonConstant.TIMEOUT_REVERSAL;
                                        final HashMap<String, byte[]> tagLengthValues = new HashMap<>();

                                        mTransOnlineReversalProcessingResult = mListener.performOnlineReversalProcessing(reversalReason, mTransOnlineProcessingResult, creditCard, tagLengthValues);

                                        if (mTransOnlineReversalProcessingResult != null &&
                                                mTransOnlineReversalProcessingResult.isApproved()) {
                                            emvTransactionResultCode = EMVTransactionResultCode.DECLINED_BY_TIMEOUT_AND_REVERSED;
                                        }

                                        final EMVTransactionResultCode finalEMVTransactionResultCode = emvTransactionResultCode;
                                        final EMVTransactionResultData finalEMVTransactionResultData = emvTransactionResultData;
                                        mMainHandler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                mListener.onTransactionCompleted(finalEMVTransactionResultCode, finalEMVTransactionResultData);
                                            }
                                        });
                                        return;
                                    }

                                    if (mTransOnlineProcessingResult.isApproved()) {
                                        emvTransactionResultCode = EMVTransactionResultCode.APPROVED_BY_ONLINE;
                                    } else {
                                        emvTransactionResultCode = EMVTransactionResultCode.DECLINED_BY_ONLINE;
                                    }
                                    emvTransactionResultData.setOnlineProcessingResult(mTransOnlineProcessingResult);
                                }

                                final EMVTransactionResultCode finalEMVTransactionResultCode = emvTransactionResultCode;
                                final EMVTransactionResultData finalEMVTransactionResultData = emvTransactionResultData;
                                mMainHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        mListener.onTransactionCompleted(finalEMVTransactionResultCode, finalEMVTransactionResultData);
                                    }
                                });
                            }
                        }).start();
                    } else {
                        EMVTransactionResultData EMVTransactionResultData = new EMVTransactionResultData();

                        EMVTransactionResultData.setCardReadMode(mTransCardReadMode);
                        EMVTransactionResultData.setCreditCard(creditCard);
                        mListener.onTransactionCompleted(EMVTransactionResultCode.ERROR_UNKNOWN, EMVTransactionResultData);
                    }
                }
            }

            @Override
            public void onAppSelect(boolean b, final List<CandidateAIDInfo> candidateAIDInfoList) {
                Log.d(TAG, ">>>onAppSelect");
                final List<Candidate> candidateList = new ArrayList<>();
                for (CandidateAIDInfo candidateAIDInfo : candidateAIDInfoList) {
                    Candidate candidate = new Candidate(candidateAIDInfo.getAID(), candidateAIDInfo.getAppLabel());
                    candidateList.add(candidate);
                }

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final Candidate selectedCandidate = mListener.confirmApplicationSelection(candidateList);
                        mMainHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                mEMVEngine.responseEvent(TLV.fromData(LandiEMVTag.EMV_TAG_TM_AID, selectedCandidate.getApplicationID()).toString());
                            }
                        });
                    }
                }).start();
            }

            @Override
            public void onFinalSelect(FinalSelectData finalData) {
                mKernelID = finalData.getKernelID();

                ApplicationData applicationData = new ApplicationData(LandiKernelID.toPayXKernelID(mKernelID), finalData.getAID());

                // 确定并设置终端参数
                HashMap<String, byte[]> terminalParameters = mListener.resolveTerminalParameters(applicationData);
                final TLVList tlvListTerminalParameters = new TLVList();
                for (String emvTag : terminalParameters.keySet()) {
                    tlvListTerminalParameters.addTLV(TLV.fromData(emvTag, terminalParameters.get(emvTag)));
                }

                mEMVEngine.setTLVList(mKernelID, tlvListTerminalParameters.toString());

                // 确定并设置交易预处理参数
                HashMap<String, byte[]> preTransactionParameters = mListener.resolvePreTransactionParameters(applicationData);
                final TLVList tlvListPreTransactionParameters = new TLVList();
                for (String emvTag : preTransactionParameters.keySet()) {
                    tlvListPreTransactionParameters.addTLV(TLV.fromData(emvTag, preTransactionParameters.get(emvTag)));
                }

                tlvListPreTransactionParameters.addTLV(TLV.fromData(EMVTag.TRANSACTION_CATEGORY_CODE.toString(), new byte[]{0x41}));

                mEMVEngine.responseEvent(tlvListPreTransactionParameters.toString());
            }

            @Override
            public void onReadRecord(final RecordData record) {

                switch (record.getFlowType()) {
                    case com.landicorp.emvkernel.data.FlowType.EMV:
                        mFlowType = FlowType.EMV;
                        break;
                    case com.landicorp.emvkernel.data.FlowType.ECASH:
                        mFlowType = FlowType.ECASH;
                        break;
                    case com.landicorp.emvkernel.data.FlowType.QPBOC:
                        mFlowType = FlowType.QPBOC;
                        break;
                    case com.landicorp.emvkernel.data.FlowType.PBOC_CTLESS:
                        mFlowType = FlowType.PBOC_CONTACTLESS;
                        break;
                    case com.landicorp.emvkernel.data.FlowType.MSD:
                        mFlowType = FlowType.MSD;
                        break;
                    case com.landicorp.emvkernel.data.FlowType.MSD_LEGACY:
                        mFlowType = FlowType.MSD_LEGACY;
                        break;
                    case com.landicorp.emvkernel.data.FlowType.QVSDC:
                        mFlowType = FlowType.VISA_QVSDC;
                        break;
                    case com.landicorp.emvkernel.data.FlowType.WAVE2:
                        mFlowType = FlowType.VISA_PAYWAVE2;
                        break;
                    case com.landicorp.emvkernel.data.FlowType.M_CHIP:
                        mFlowType = FlowType.PAYPASS_CHIP;
                        break;
                    case com.landicorp.emvkernel.data.FlowType.M_STRIPE:
                        mFlowType = FlowType.PAYPASS_STRIP;
                        break;
                    case 0x41:
                        mFlowType = FlowType.AMEX_MAGSRTIPE;
                        break;
                    case 0x42:
                        mFlowType = FlowType.AMEX_EMV;
                        break;
                    case 0x43:
                        mFlowType = FlowType.AMEX_MOBILE_MAGSTRIPE;
                        break;
                }

                byte[] rid = new byte[5];
                System.arraycopy(record.getAID(), 0, rid, 0, 5);

                final byte capkIndex = record.getPubKIndex();
                final ApplicationData applicationData = new ApplicationData(LandiKernelID.toPayXKernelID(mKernelID), record.getAID());

                applicationData.setFlowType(mFlowType);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        mListener.onReadApplicationData(applicationData, capkIndex);

                        // 确定并设置交易处理参数
                        final HashMap<String, byte[]> transactionParameters = mListener.resolveTransactionParameters(applicationData);
                        final TLVList tlvListPreTransactionParameters = new TLVList();
                        for (String emvTag : transactionParameters.keySet()) {
                            tlvListPreTransactionParameters.addTLV(TLV.fromData(emvTag, transactionParameters.get(emvTag)));
                        }

                        switch (getTransactionType()) {
                            case REFUND:
                                tlvListPreTransactionParameters.addTLV(TLV.fromData("DF918102", new byte[]{0x02}));
                                break;
                        }

                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                mEMVEngine.setTLVList(mKernelID, tlvListPreTransactionParameters.toString());
                                Log.e(TAG, "onReadRecord: pan:" + Hex.encode(record.getPan()));
                                mEMVEngine.responseEvent(null);
                            }
                        });
                    }
                }).start();
            }

            @Override
            public void onCardHolderVerify(CVMData cvmData) {
                final CVMFlag cvmFlag = LandiCVMFlag.toPayXCVMFlag(cvmData.getCVM());

                android.util.Log.e(TAG, "onCardHolderVerify: cvmFlag:" + cvmFlag);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final CardholderVerificationResult cardholderVerificationResult = mListener.cardholderVerification(cvmFlag);

                        TLV tlv = null;
                        switch (cardholderVerificationResult) {
                            case CONFIRM:
                                tlv = TLV.fromData(LandiEMVTag.DEF_TAG_CHV_STATUS, new byte[]{0x01});
                                break;
                            case CANCEL:
                                tlv = TLV.fromData(LandiEMVTag.DEF_TAG_CHV_STATUS, new byte[]{0x04});
                                break;
                            case ERROR:
                                tlv = TLV.fromData(LandiEMVTag.DEF_TAG_CHV_STATUS, new byte[]{0x02});
                                break;
                        }

                        final TLV finalTLV = tlv;
                        mMainHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                mEMVEngine.responseEvent(finalTLV.toString());
                            }
                        });
                    }
                }).start();
            }

            @Override
            public void onOnlineProcess(TransactionData transData) {
                final HashMap<String, byte[]> tagLengthValues = new HashMap<>();
                for (EMVTag emvTag : EMVTag.values()) {
                    byte[] value = getTLV(emvTag);
                    if (value != null && value.length > 0) {
                        tagLengthValues.put(emvTag.toString(), value);
                    }
                }

                final CreditCard creditCard = new CreditCard();
                if (transData != null) {
                    String track1 = Hex.encode(getTLV(EMVTag.TRACK_1_DISCRETIONARY_DATA));
                    String track2 = Hex.encode(getTLV(EMVTag.TRACK_2_EQUIVALENT_DATA));

                    if (StringUtils.isEmpty(track2)) {
                        track2 = Hex.encode(getTLV(EMVTag.TRACK2_DATA));
                    }

                    if (mFlowType == FlowType.PAYPASS_STRIP) {
                        track2 = mEMVEngine.getTLV(EMVTag.TRACK2_DATA.toString());
                        track1 = mEMVEngine.getTLV("56");
                    }

                    CreditCard.EmvData emvData = new CreditCard.EmvData(track1, track2, tagLengthValues);
                    creditCard.setCardReadMode(mTransCardReadMode);
                    creditCard.setCardNumber(Hex.encode(transData.getPan()).toUpperCase().replaceAll("F", "").replaceAll("D", ""));

                    byte[] value = tagLengthValues.get(EMVTag.APPLICATION_EXPIRATION_DATE.toString());

                    if (value != null && value.length > 0) {
                        transData.setExpiry(value);
                    } else {
                        transData.setExpiry(Hex.decode(track2.split("D")[1].substring(0, 4)));
                    }

                    creditCard.setExpireDate(Hex.encode(transData.getExpiry()).substring(0, 4));
                    creditCard.setEmvData(emvData);
                }

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        mTransOnlineProcessingResult = mListener.performOnlineProcessing(creditCard, tagLengthValues);

                        CallIssuerConfirmResult callIssuerConfirmResult = CallIssuerConfirmResult.NO_NEED;

                        if (mTransOnlineProcessingResult.isNeedCallIssuerConfirm()) {
                            callIssuerConfirmResult = mListener.callIssuerConfirm();
                            // If Call Issuer Confirm Result Is ACCEPT Set Transaction To Approved
                            mTransOnlineProcessingResult.setApproved(true);
                        }

                        final HashMap<String, byte[]> onlineProcessingResultTagLengthValues = mListener.resolveOnlineProcessingResult(mTransOnlineProcessingResult, callIssuerConfirmResult);

                        final TLVList tlvList = new TLVList();
                        for (String emvTag : onlineProcessingResultTagLengthValues.keySet()) {
                            tlvList.addTLV(TLV.fromData(emvTag, onlineProcessingResultTagLengthValues.get(emvTag)));
                        }

                        mMainHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                mEMVEngine.responseEvent(tlvList.toString());
                            }
                        });
                    }
                }).start();
            }

            @Override
            public void onVerifyOfflinePin(int flag,
                                           byte[] random,
                                           CAPKey publicKey,
                                           VerifyOfflinePinResult result) {
                byte fmtOfPin = (flag == 1) ? PinVerifyCfg.FOPTBV_ENCRYPTED_BY_PUBLIC_KEY : PinVerifyCfg.FOPTBV_PLAIN_TEXT;
                PinVerifyCfg.PinEncPublicKey pinPublicKey = new PinVerifyCfg.PinEncPublicKey(
                        publicKey.getRid(), publicKey.getIndex(),
                        publicKey.getMod(), publicKey.getExp(),
                        publicKey.getExpDate(), publicKey.getHashFlg(),
                        publicKey.getHash());

                OfflinePinVerifyResult pinVerifyResult = new OfflinePinVerifyResult();
                boolean verified = mPinPad.verifyOfflinePin(0, 0, fmtOfPin, PinVerifyCfg.VCF_DEFAULT, random, pinPublicKey, pinVerifyResult);
                Log.d(TAG, "PIN Verified: " + verified + ", 9F17: "+ Hex.encode(getTLV(EMVTag.PIN_TRY_COUNTER)));

                int apduRet = pinVerifyResult.mAPDURet;
                int sw1 = pinVerifyResult.mSW1;
                int sw2 = pinVerifyResult.mSW2;

                result.setSW(sw1, sw2);
                result.setResult(apduRet);
            }

            @Override
            public void onEndProcess(final int result, final TransactionData transData) {
                // 当 mListener 为空时代表流程已经被中止
                if (mListener == null) {
                    return;
                }
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final CreditCard creditCard = new CreditCard();

                        EMVTransactionResultCode emvTransactionResultCode = EMVTransactionResultCode.ERROR_UNKNOWN;
                        EMVTransactionResultData emvTransactionResultData = new EMVTransactionResultData();

                        switch (result) {
                            case ResultCode.CHECKCARD_MAG_SWIPE_ERROR:
                                mTransCardReadMode = CardReadMode.SWIPE;
                                emvTransactionResultCode = EMVTransactionResultCode.CARD_DETECT_MAG_CARD_UNUSUAL;
                                break;
                            case ResultCode.CHECKCARD_IC_POWERUP_ERROR:
                                mTransCardReadMode = CardReadMode.CONTACT;
                                emvTransactionResultCode = EMVTransactionResultCode.CARD_DETECT_IC_CARD_UNUSUAL;
                                break;
                        }
                        creditCard.setCardReadMode(mTransCardReadMode);

                        if (transData != null) {
                            emvTransactionResultData.setACType(LandiACType.toPayXACFlag(transData.getACType()));
                            emvTransactionResultData.setCVMFlag(LandiCVMFlag.toPayXCVMFlag(transData.getCVM()));
                        }

                        final HashMap<String, byte[]> tagLengthValues = new HashMap<>();

                        for (EMVTag emvTag : EMVTag.values()) {
                            byte[] value = getTLV(emvTag);
                            if (value != null && value.length > 0) {
                                tagLengthValues.put(emvTag.toString(), value);
                            }
                        }

                        if (transData != null) {
//                            byte[] data = getTLV(EMVTag.APPLICATION_PRIMARY_ACCOUNT_NUMBER);
//
//                            if (data != null) {
//                                transData.setPan(data);
//                            }
//
//                            String track1 = Hex.encode(getTLV(EMVTag.TRACK_1_DISCRETIONARY_DATA));
//                            String track2 = Hex.encode(getTLV(EMVTag.TRACK_2_EQUIVALENT_DATA));
//
//                            if (StringUtils.isEmpty(track2)) {
//                                track2 = Hex.encode(getTLV(EMVTag.TRACK2_DATA));
//                            }
//
//                            if (data == null && !StringUtils.isEmpty(track2)) {
//                                data = Hex.decode(track2.split("D")[0]);
//                                transData.setPan(data);
//                            }
//
//                            if (mFlowType == FlowType.PAYPASS_STRIP) {
//                                track2 = mEMVEngine.getTLV(EMVTag.TRACK2_DATA.toString());
//                                track1 = new String(Hex.decode(mEMVEngine.getTLV("56")));
//
//                                if (!StringUtils.isEmpty(track2)) {
//
//                                    creditCard.setMagData(new CreditCard.MagData(track1, track2));
//                                    creditCard.setCardNumber(track2.split("D")[0]);
//                                    creditCard.setExpireDate(track2.split("D")[1].substring(0, 4));
//                                }
//                            } else {
//                                creditCard.setCardReadMode(mTransCardReadMode);
//                                creditCard.setCardNumber(Hex.encode(transData.getPan()).toUpperCase().replaceAll("F", "").replaceAll("D", ""));
//                                creditCard.setExpireDate(Hex.encode(transData.getExpiry()).substring(0, 4));
//                            }

                            String track1 = Hex.encode(getTLV(EMVTag.TRACK_1_DISCRETIONARY_DATA));
                            String track2 = Hex.encode(getTLV(EMVTag.TRACK_2_EQUIVALENT_DATA));
                            String PAN = Hex.encode(getTLV(EMVTag.APPLICATION_PRIMARY_ACCOUNT_NUMBER));
                            String expireDate = "";

                            if (StringUtils.isEmpty(track2)) {
                                track2 = Hex.encode(getTLV(EMVTag.TRACK2_DATA));
                            }

                            if (StringUtils.isEmpty(PAN) && !StringUtils.isEmpty(track2)) {
                                PAN = track2.split("D")[0];
                            }

                            if (StringUtils.isEmpty(expireDate) && !StringUtils.isEmpty(track2)) {
                                expireDate = track2.split("D")[1].substring(0, 4);
                            }

                            // ContactLess Mag Mode
                            if (mFlowType == FlowType.PAYPASS_STRIP) {
                                track2 = mEMVEngine.getTLV(EMVTag.TRACK2_DATA.toString());
                                track1 = new String(Hex.decode(mEMVEngine.getTLV("56")));

                                PAN = track2.split("D")[0];
                                expireDate = track2.split("D")[1].substring(0, 4);
                                creditCard.setMagData(new CreditCard.MagData(track1, track2));
                            }

                            creditCard.setCardReadMode(mTransCardReadMode);
                            creditCard.setCardNumber(PAN.toUpperCase().replaceAll("F", "").replaceAll("D", ""));
                            creditCard.setExpireDate(expireDate);
                            CreditCard.EmvData emvData = new CreditCard.EmvData(track1, track2, tagLengthValues);
                            creditCard.setEmvData(emvData);
                        }

                        tagLengthValues.put(EMVTag.CUSTOM_FLOW_TYPE.toString(), new byte[]{mFlowType});

                        if (result == LandiResultCode.SUCCESS) {
                            ACType acType = emvTransactionResultData.getACType();
                            byte[] terminalVerificationResults = tagLengthValues.get(EMVTag.TERMINAL_VERIFICATION_RESULTS.toString());
                            boolean cdaFailed = (terminalVerificationResults[0] & 0x04) == 0x04;
                            // 当读卡模式为非接时，并且 ACType 为 ARQC 时，联机请求
                            if (mTransCardReadMode == CardReadMode.CONTACTLESS &&
                                    emvTransactionResultData.getACType() == ACType.ARQC &&
                                    !cdaFailed) {

//                                if (mFlowType == FlowType.PAYPASS_STRIP)
//                                {
//                                    String track2 = mEMVEngine.getTLV(EMVTag.TRACK2_DATA.toString());
//                                    String track1 = mEMVEngine.getTLV("56");
//
//                                    if (!StringUtils.isEmpty(track2)) {
//
//                                        creditCard.setMagData(new CreditCard.MagData(track1, track2));
//                                        creditCard.setCardNumber(track2.split("D")[0]);
//                                        creditCard.setExpireDate(track2.split("D")[1].substring(0, 4));
//                                    }
//                                }

                                mTransOnlineProcessingResult = mListener.performOnlineProcessing(creditCard, tagLengthValues);
                            }

                            boolean needReversal = false;
                            int reversalReason = 0;
                            // 当联机请求超时时，冲正请求
                            if ((mTransOnlineProcessingResult != null && mTransOnlineProcessingResult.isRequestTimeout())) {
                                needReversal = true;
                                reversalReason = ReversalReasonConstant.TIMEOUT_REVERSAL;
                                // 当读卡模式为接触时，并且 ACType 为 AAC 时，冲正请求
                            } else if (mTransCardReadMode == CardReadMode.CONTACT &&
                                    mTransOnlineProcessingResult != null &&
                                    mTransOnlineProcessingResult.isApproved() &&
                                    emvTransactionResultData.getACType() == ACType.AAC) {
                                needReversal = true;
                                reversalReason = ReversalReasonConstant.AUTHORIZATION_REVERSAL;
                            }

                            if (needReversal) {
                                mTransOnlineReversalProcessingResult = mListener.performOnlineReversalProcessing(reversalReason, mTransOnlineProcessingResult, creditCard, tagLengthValues);
                            }

                            switch (acType) {
                                case ARQC:
                                    if (mCheckType == CardType.CARD_RF && mTransOnlineProcessingResult != null) {
                                        // 联机批准
                                        if (mTransOnlineProcessingResult.isApproved()) {
                                            emvTransactionResultCode = EMVTransactionResultCode.APPROVED_BY_ONLINE;
                                            // 联机拒绝
                                        } else {
                                            emvTransactionResultCode = EMVTransactionResultCode.DECLINED_BY_ONLINE;
                                        }
                                    } else {
                                        emvTransactionResultCode = EMVTransactionResultCode.DECLINED_BY_OFFLINE;
                                    }
                                    break;
                                case TC:
                                    if (mTransOnlineProcessingResult != null) {
                                        if (mTransOnlineProcessingResult.isApproved()) {
                                            emvTransactionResultCode = EMVTransactionResultCode.APPROVED_BY_ONLINE;
                                        } else {
                                            emvTransactionResultCode = EMVTransactionResultCode.DECLINED_BY_ONLINE;
                                        }
                                        // 终端拒绝，并且冲正成功
                                        if (mTransOnlineReversalProcessingResult != null &&
                                                mTransOnlineReversalProcessingResult.isApproved()) {
                                            emvTransactionResultCode = EMVTransactionResultCode.DECLINED_BY_TERMINAL_AND_REVERSED;
                                        }
                                    } else {
                                        emvTransactionResultCode = EMVTransactionResultCode.APPROVED_BY_OFFLINE;
                                    }
                                    break;
                                case AAC:
                                    if (mTransOnlineProcessingResult != null) {
                                        emvTransactionResultCode = EMVTransactionResultCode.DECLINED_BY_ONLINE;
                                        // 联机授权请求超时，并且冲正成功
                                        if (mTransOnlineProcessingResult.isRequestTimeout() &&
                                                mTransOnlineReversalProcessingResult != null &&
                                                mTransOnlineReversalProcessingResult.isApproved()) {
                                            emvTransactionResultCode = EMVTransactionResultCode.DECLINED_BY_TIMEOUT_AND_REVERSED;
                                        }
                                    } else {
                                        emvTransactionResultCode = EMVTransactionResultCode.DECLINED_BY_OFFLINE;
                                    }
                                    break;
                            }
                        } else {
                            Log.e(TAG, "run: result:" + result);
                            emvTransactionResultCode = LandiResultCode.toPayXResultCode(result);
                            if (mCheckType == CardType.CARD_INSERT) {
                                String tlvString = mEMVEngine.getTLV("DF918156");
                                Log.d(TAG, "DF918156: " + tlvString);
                                if (tlvString.startsWith("02") && tlvString.endsWith("6283")) {
                                    emvTransactionResultCode = EMVTransactionResultCode.EMV_RESULT_CARD_BLOCKED;
                                }
                            }
                        }

                        emvTransactionResultData.setCardReadMode(mTransCardReadMode);
                        emvTransactionResultData.setCreditCard(creditCard);
                        emvTransactionResultData.setOnlineProcessingResult(mTransOnlineProcessingResult);
                        emvTransactionResultData.setOnlineReversalProcessingResult(mTransOnlineReversalProcessingResult);
                        emvTransactionResultData.setTagLengthValues(tagLengthValues);

//                        String lastinfo = "";
//
//                        switch(mTransCardReadMode){
//                            case CONTACT:
//                            case CONTACTLESS:
//                                lastinfo = "01";
//                                break;
//                            default:
//                                lastinfo = "00";
//                                break;
//                        }
//                        lastinfo += transac


                        Log.e(TAG, "run: emvTransactionResultCode:" + emvTransactionResultCode);
                        final EMVTransactionResultCode finalEMVTransactionResultCode = emvTransactionResultCode;
                        final EMVTransactionResultData finalEMVTransactionResultData = emvTransactionResultData;
                        mMainHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                mListener.onTransactionCompleted(finalEMVTransactionResultCode, finalEMVTransactionResultData);
                            }
                        });
                    }
                }).start();
            }

            @Override
            public void onSendOut(int ins, byte[] data) {
                android.util.Log.e(TAG, "onSendOut: ");

                switch (ins) {
                    case KernelINS.DISPLAY:

                        switch (data[0] & 0x00FF) {
                            case MessageID.PCII:
                                byte[] value = getTLV("DF4B");

                                if (value != null && value.length > 1) {
                                    if ((value[1] & 0x0F) > 0) {

                                        mListener.onTransactionCompleted(EMVTransactionResultCode.EMV_RESULT_REPOWERICC, null);
//                                        mListener.onMessage("Please see phone");
                                    }
                                }
                                break;
                        }
                        break;
                }
            }

            @Override
            public void onObtainData(int ins, byte[] data) {
            }
        });
    }

    @Override
    public void stopProcess() {
        mEMVEngine.stopProcess();
    }

    @Override
    public void addApplication(Application application) {
        mEMVEngine.manageAID(ActionFlag.ADD, Hex.encode(application.getApplicationID()), true);
        mApplications.add(application);
    }

    @Override
    public void removeApplication(Application application) {
        mEMVEngine.manageAID(ActionFlag.DELETE, Hex.encode(application.getApplicationID()), true);
        for (int i = mApplications.size() - 1; i >= 0; i++) {
            if (Hex.encode(application.getApplicationID()).equals(Hex.encode(mApplications.get(i).getApplicationID()))) {
                mApplications.remove(i);
            }
        }
    }

    @Override
    public HashMap<String, byte[]> resolveSpecificTerminalParameters(ApplicationData applicationData) {
        KernelID kernelID = applicationData.getKernelID();
        byte[] applicationID = applicationData.getApplicationID();

        HashMap<String, byte[]> tagLengthValues = new HashMap<>();

        Application selectedApplication = null;
        for (Application application : getApplications()) {
            if (Hex.encode(applicationID).contains(Hex.encode(application.getApplicationID()))) {
                selectedApplication = application;
                break;
            }
        }

        if (selectedApplication == null) {
            return null;
        }

        tagLengthValues.put(LandiEMVTag.DEF_TAG_TAC_DEFAULT, selectedApplication.getTerminalActionCodeDefault());
        tagLengthValues.put(LandiEMVTag.DEF_TAG_TAC_ONLINE, selectedApplication.getTerminalActionCodeOnline());
        tagLengthValues.put(LandiEMVTag.DEF_TAG_TAC_DECLINE, selectedApplication.getTerminalActionCodeDenial());
        tagLengthValues.put(LandiEMVTag.DEF_TAG_RAND_SLT_THRESHOLD, new byte[]{0x00, 0x00, 0x00, 0x00, 0x00, 0x00});
        tagLengthValues.put(LandiEMVTag.DEF_TAG_RAND_SLT_PER, new byte[]{0x00});
        tagLengthValues.put(LandiEMVTag.DEF_TAG_RAND_SLT_MAXPER, new byte[]{0x00});

        switch (kernelID) {
            // EMV 接触式
            case EMVContact:
                byte[] floorLimit = DataConversion.convertFloatToBcdBytes(selectedApplication.getFloorLimit());
                android.util.Log.w(TAG, "resolveSpecificTerminalParameters: floor limit:" + Hex.encode(floorLimit));

                break;
            // EMV 非接触式
            case EMVContactless:
                break;
            case MASTER:
                tagLengthValues.put(LandiEMVTag.M_TAG_TM_9F6D, new byte[]{(byte) 0x00, 0x01});
                tagLengthValues.put("9F1D", Hex.decode("24B0000000000000"));
                tagLengthValues.put(LandiEMVTag.M_TAG_TM_9F7C, "shenzhen bindo".getBytes());
                tagLengthValues.put(LandiEMVTag.M_TAG_TM_9F53, new byte[]{(byte) 0x01});
                byte[] mcTransLimit = DataConversion.convertFloatToBcdBytes(selectedApplication.getContactlessTransactionLimit());
                android.util.Log.w(TAG, "resolveSpecificTerminalParameters: transaction limit:" + Hex.encode(mcTransLimit));
                // 非接CMV限额，授权金额超过该限额将要求CVM
                tagLengthValues.put(LandiEMVTag.M_TAG_TM_TRANS_LIMIT, mcTransLimit);
//                byte[] mcCvmLimit = DataConversion.convertFloatToBcdBytes(selectedApplication.getContactlessCardholderVerificationLimit());
//                tagLengthValues.put(LandiEMVTag.A_TAG_TM_CVM_LIMIT, mcCvmLimit);
                break;
            case VISA:
                // 非接交易限额，授权金额超过该限额将终止交易
                tagLengthValues.put(LandiEMVTag.A_TAG_TM_TRANS_LIMIT, DataConversion.convertFloatToBcdBytes(selectedApplication.getContactlessTransactionLimit()));
                byte[] transLimit = DataConversion.convertFloatToBcdBytes(selectedApplication.getContactlessTransactionLimit());
                android.util.Log.w(TAG, "resolveSpecificTerminalParameters: transaction limit:" + Hex.encode(transLimit));
                // 非接CMV限额，授权金额超过该限额将要求CVM
                tagLengthValues.put(LandiEMVTag.A_TAG_TM_CVM_LIMIT, DataConversion.convertFloatToBcdBytes(selectedApplication.getContactlessCardholderVerificationLimit()));
                byte[] cvmLimit = DataConversion.convertFloatToBcdBytes(selectedApplication.getContactlessCardholderVerificationLimit());
                android.util.Log.w(TAG, "resolveSpecificTerminalParameters: cvm limit:" + Hex.encode(cvmLimit));
                // 非接最低限额，授权金额超过该限额将要求联机授权
                tagLengthValues.put(LandiEMVTag.A_TAG_TM_FLOOR_LIMIT, DataConversion.convertFloatToBcdBytes(selectedApplication.getContactlessFloorLimit()));
                tagLengthValues.put(LandiEMVTag.C_TAG_TM_9F66, new byte[]{0x22, 0x00, 0x40, 0x00});
                tagLengthValues.put(LandiEMVTag.V_TAG_RD_RCP, new byte[]{0x7C, 0x00});
                byte[] clFloorLimit = DataConversion.convertFloatToBcdBytes(selectedApplication.getContactlessFloorLimit());
                android.util.Log.w(TAG, "resolveSpecificTerminalParameters: cl floor limit:" + Hex.encode(clFloorLimit));
                break;
            case AMEX:
                // 非接交易限额，授权金额超过该限额将终止交易
                tagLengthValues.put(LandiEMVTag.A_TAG_TM_TRANS_LIMIT, DataConversion.convertFloatToBcdBytes(selectedApplication.getContactlessTransactionLimit()));
                // 非接CMV限额，授权金额超过该限额将要求CVM
                tagLengthValues.put(LandiEMVTag.A_TAG_TM_CVM_LIMIT, DataConversion.convertFloatToBcdBytes(selectedApplication.getContactlessCardholderVerificationLimit()));
                // 非接最低限额，授权金额超过该限额将要求联机授权
                tagLengthValues.put(LandiEMVTag.A_TAG_TM_FLOOR_LIMIT, DataConversion.convertFloatToBcdBytes(selectedApplication.getContactlessFloorLimit()));
                // 重新寻卡
                if (mDetectRetryCount > 0) {
                    tagLengthValues.put(LandiEMVTag.A_TAG_PREAGAIN, new byte[]{0x01});
                }
                break;
            case JCB:
                break;
            case PBOC:
                break;
        }

        return tagLengthValues;
    }

    @Override
    public HashMap<String, byte[]> resolveSpecificPreTransactionParameters(ApplicationData applicationData) {
        HashMap<String, byte[]> tagLengthValues = new HashMap<>();

        tagLengthValues.put(LandiEMVTag.DEF_TAG_SERVICE_TYPE, new byte[]{0x00});
//        tagLengthValues.put("9F6D", new byte[]{(byte) 0x00, 0x01});
//        tagLengthValues.put("9F6E", new byte[]{(byte) 0xD8, (byte) 0xE0, 0x00, 0x00});

        // 强制联机
        if (isForceOnline()) {
            tagLengthValues.put(LandiEMVTag.DEF_TAG_GAC_CONTROL, new byte[]{0x02});
        } else {
            tagLengthValues.put(LandiEMVTag.DEF_TAG_GAC_CONTROL, new byte[]{0x00});
        }

        switch (getTransactionType()) {
            case REFUND:
                tagLengthValues.put(EMVTag.TRANSACTION_TYPE.toString(), new byte[]{0x20});
                break;
            default:
                tagLengthValues.put(EMVTag.TRANSACTION_TYPE.toString(), new byte[]{0x00});
                break;
        }

        // 添加不同设备特有的终端参数
        switch (applicationData.getKernelID()) {
            // EMV 接触式
            case EMVContact:
                tagLengthValues.put("9F6D", new byte[]{(byte) 0xC8});
                tagLengthValues.put("9F6E", new byte[]{(byte) 0xD8, (byte) 0xE0, 0x00, 0x00});
                break;
            // EMV 非接触式
            case EMVContactless:
                break;
            case MASTER:
                switch (getTransactionType()) {
                    case AUTHORIZATION:
                        tagLengthValues.put(EMVTag.TRANSACTION_TYPE.toString(), new byte[]{0x00});

                        break;
                    case REFUND:
                        tagLengthValues.put(EMVTag.TRANSACTION_TYPE.toString(), new byte[]{0x20});
//                            tagLengthValues.put("DF918102", new byte[]{0x03});
                        break;
                }

//                    String V_TAG_TM_CVM_LIMIT = "DF8126";
//                    String V_TAG_TM_FLOOR_LIMIT = "DF8123";
//                    String M_TAG_TM_TRANS_LIMIT = "DF8124";
//                    String M_TAG_TM_TRANS_LIMIT_CDV = "DF8125";
//                    String M_TAG_TM_CVM_LIMIT = "DF8126";
//                    String M_TAG_TM_FLOOR_LIMIT = "DF8123";

                tagLengthValues.put(com.landicorp.emvkernel.tag.EMVTag.DEF_TAG_M_CDV_SUP, new byte[]{0x01});//support CDCVM

                tagLengthValues.put(com.landicorp.emvkernel.tag.EMVTag.DEF_TAG_M_REQ_CVM, new byte[]{0x20});//Contactless Floor limit
                tagLengthValues.put(com.landicorp.emvkernel.tag.EMVTag.DEF_TAG_M_REQ_NOCVM, new byte[]{0x08});//Contactless Floor limit
                tagLengthValues.put(com.landicorp.emvkernel.tag.EMVTag.DEF_TAG_M_MAG_REQ_CVM, new byte[]{0x20});//Contactless Floor limit
                tagLengthValues.put(com.landicorp.emvkernel.tag.EMVTag.DEF_TAG_M_MAG_REQ_NOCVM, new byte[]{0x00});//Contactless Floor limit

                tagLengthValues.put(com.landicorp.emvkernel.tag.EMVTag.M_TAG_TM_FLOOR_LIMIT, Hex.decode("000000000000"));//Contactless Floor limit
                tagLengthValues.put(com.landicorp.emvkernel.tag.EMVTag.M_TAG_TM_CVM_LIMIT, Hex.decode("000000006000"));//Contactless CVM Require limit
                tagLengthValues.put(com.landicorp.emvkernel.tag.EMVTag.M_TAG_TM_TRANS_LIMIT, Hex.decode("000000007000"));//Contactless NO CDCVM Require limit
                tagLengthValues.put(com.landicorp.emvkernel.tag.EMVTag.M_TAG_TM_TRANS_LIMIT_CDV, Hex.decode("000000008000"));//Contactless CDCVM Require limit
                break;
            case VISA:

                break;
            case AMEX:
                tagLengthValues.put("9F6D", new byte[]{(byte) 0xC8});
                tagLengthValues.put("9F6E", new byte[]{(byte) 0xD8, (byte) 0xE0, 0x00, 0x00});
                break;
            case JCB:
                break;
            case PBOC:
                break;
        }

        return tagLengthValues;
    }

    @Override
    public HashMap<String, byte[]> resolveSpecificTransactionParameters(ApplicationData applicationData) {
        KernelID kernelID = applicationData.getKernelID();
        byte[] applicationID = applicationData.getApplicationID();

        HashMap<String, byte[]> tagLengthValues = new HashMap<>();

        Application selectedApplication = null;
        for (Application application : getApplications()) {
            if (Hex.encode(applicationID).contains(Hex.encode(application.getApplicationID()))) {
                selectedApplication = application;
                break;
            }
        }
        if (selectedApplication == null) {
            // TODO: 错误处理，Application 不存在。
            return null;
        }

        Log.d(TAG, "ForceOnline: " + isForceOnline());
        Log.d(TAG, "FloorLimit: " + selectedApplication.getFloorLimit());
        Log.d(TAG, "ContactlessTransactionLimit: " + selectedApplication.getContactlessTransactionLimit());
        Log.d(TAG, "ContactlessCardholderVerificationLimit: " + selectedApplication.getContactlessCardholderVerificationLimit());
        Log.d(TAG, "ContactlessFloorLimit: " + selectedApplication.getContactlessFloorLimit());

        switch (kernelID) {
            // EMV 接触式
            case EMVContact:
                break;
            // EMV 非接触式
            case EMVContactless:
                break;
            case MASTER:
                break;
            case VISA:
                break;
            case AMEX:
                // 非接交易限额，授权金额超过该限额将终止交易
                tagLengthValues.put(LandiEMVTag.A_TAG_TM_TRANS_LIMIT, DataConversion.convertFloatToBcdBytes(selectedApplication.getContactlessTransactionLimit()));
                // 非接CMV限额，授权金额超过该限额将要求CVM
                tagLengthValues.put(LandiEMVTag.A_TAG_TM_CVM_LIMIT, DataConversion.convertFloatToBcdBytes(selectedApplication.getContactlessCardholderVerificationLimit()));
                // 非接最低限额，授权金额超过该限额将要求联机授权
                tagLengthValues.put(LandiEMVTag.A_TAG_TM_FLOOR_LIMIT, DataConversion.convertFloatToBcdBytes(selectedApplication.getContactlessFloorLimit()));
                break;
            case JCB:
                break;
            case PBOC:
                break;
        }
        return tagLengthValues;
    }

    @Override
    public HashMap<String, byte[]> resolveSpecificOnlineProcessingResult(OnlineProcessingResult onlineProcessingResult, CallIssuerConfirmResult callIssuerConfirmResult) {
        HashMap<String, byte[]> tagLengthValues = new HashMap<>();
        try {
            Log.d(TAG, JSONUtils.toJSONString(onlineProcessingResult));
        } catch (Exception e) {
            e.printStackTrace();
        }

        boolean isApproved = onlineProcessingResult.isApproved();
        if (onlineProcessingResult.isNeedCallIssuerConfirm()) {
            isApproved = callIssuerConfirmResult == CallIssuerConfirmResult.ACCEPT;
        }

        tagLengthValues.put(LandiEMVTag.DEF_TAG_ONLINE_STATUS, new byte[]{0x00});
        if (isApproved) {
            tagLengthValues.put(LandiEMVTag.EMV_TAG_TM_ARC, "00".getBytes());
            tagLengthValues.put(LandiEMVTag.DEF_TAG_AUTHORIZE_FLAG, new byte[]{0x01});
        } else if (onlineProcessingResult.isUnableToGoOnline()) {
            tagLengthValues.put(LandiEMVTag.EMV_TAG_TM_ARC, "Z3".getBytes());
            tagLengthValues.put(LandiEMVTag.DEF_TAG_AUTHORIZE_FLAG, new byte[]{0x00});
        } else {
            tagLengthValues.put(LandiEMVTag.EMV_TAG_TM_ARC, "51".getBytes());
            tagLengthValues.put(LandiEMVTag.DEF_TAG_AUTHORIZE_FLAG, new byte[]{0x00});
        }

        // 发卡行认证数据
        if (!StringUtils.isEmpty(onlineProcessingResult.getApprovalCode())) {
//                tagLengthValues.put(LandiEMVTag.EMV_TAG_TM_AUTHCODE, Hex.decode(onlineProcessingResult.getApprovalCode()));
//                tagLengthValues.put(LandiEMVTag.EMV_TAG_TM_AUTHCODE, Hex.decode("000000"));
        }
        // 脚本数据
        StringBuilder hostTlvData = new StringBuilder();
        if (!StringUtils.isEmpty(onlineProcessingResult.getIccRelatedDataIssuerScriptData())) {
            hostTlvData.append(onlineProcessingResult.getIccRelatedDataIssuerScriptData());
        }
        // Issuer Authentication Data
        if (!StringUtils.isEmpty(onlineProcessingResult.getIccRelatedDataIssuerAuthenticationData())) {
            hostTlvData.append("91" + String.format("%02X", onlineProcessingResult.getIccRelatedDataIssuerAuthenticationData().length() / 2) + onlineProcessingResult.getIccRelatedDataIssuerAuthenticationData());
        }

        if (hostTlvData.length() > 0) {
            tagLengthValues.put(LandiEMVTag.DEF_TAG_HOST_TLVDATA, Hex.decode(hostTlvData.toString()));
        }
        return tagLengthValues;
    }

    @Override
    public byte[] getTLV(EMVTag emvTag) {
        String tagHexString = emvTag.toString();
        return getTLV(tagHexString);
    }

    @Override
    public byte[] getTLV(String emvTag) {
        return Hex.decode(mEMVEngine.getTLV(emvTag));
    }

    @Override
    public int setTLV(EMVTag emvTag, byte[] data) {
        String tagHexString = emvTag.toString();
        return setTLV(tagHexString, data);
    }

    @Override
    public int setTLV(String emvTag, byte[] data) {
        return mEMVEngine.setTLV(mKernelID, emvTag, Hex.encode(data));
    }

    @Override
    public int setCertificationAuthorityPublicKey(CAPK capk) {
        CAPKey capKey = new CAPKey();

        capk.setHeader(capk.getHeader());
        capKey.setRid(capk.getRID());
        capKey.setIndex(capk.getCAPKIndex());
        capKey.setMod(capk.getCAPKModulus());
        capKey.setExp(capk.getCAPKExponent());
        capKey.setHash(capk.getHashValue());
        capKey.setHashFlg((byte) 0x00);

        return mEMVEngine.setCAPubKey(capKey);
    }

}
