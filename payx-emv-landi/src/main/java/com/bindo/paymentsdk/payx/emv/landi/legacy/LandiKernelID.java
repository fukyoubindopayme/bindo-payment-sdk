package com.bindo.paymentsdk.payx.emv.landi.legacy;

import com.bindo.paymentsdk.payx.emv.core.enmus.KernelID;
import com.landicorp.emvkernel.data.EMVKernelID;

public class LandiKernelID implements EMVKernelID {
    public static KernelID toPayXKernelID(int kernelId) {
        switch (kernelId) {
            case EMV:
                return KernelID.EMVContact;
            case EMVCTLess:
                return KernelID.EMVContactless;
            case MASTER:
                return KernelID.MASTER;
            case VISA:
                return KernelID.VISA;
            case AMEX:
                return KernelID.AMEX;
            case JCB:
                return KernelID.JCB;
            case PBOC:
                return KernelID.PBOC;
            case DEFINE:
                return KernelID.DEFINE;
        }
        return null;
    }
}
