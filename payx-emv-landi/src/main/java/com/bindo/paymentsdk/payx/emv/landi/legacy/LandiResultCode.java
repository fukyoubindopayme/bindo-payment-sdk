package com.bindo.paymentsdk.payx.emv.landi.legacy;

import com.bindo.paymentsdk.payx.emv.results.EMVTransactionResultCode;

public class LandiResultCode extends com.landicorp.emvkernel.ResultCode {
    public static EMVTransactionResultCode toPayXResultCode(int result) {
        switch (result) {
            // EMV RESULT
            case EMV_RESULT_NOAPP:
                return EMVTransactionResultCode.EMV_RESULT_NO_APP;
            case EMV_RESULT_NOPUBKEY:
                return EMVTransactionResultCode.EMV_RESULT_NO_PUBLIC_KEY;
            // AMEX 超过限额，提示使用其他磁条或IC卡
            case 0xF40000E1:
                return EMVTransactionResultCode.AMEX_RESULT_TRY_ANOTHER_INTERFACE;
            // AMEX 超过限额，提示使用其他卡片交易
            case 0xF40000E2:
                return EMVTransactionResultCode.AMEX_RESULT_TRY_ANOTHER_PAYMENT;
            case 0xF40000E3:
                return EMVTransactionResultCode.AMEX_RESULT_TRY_AGAIN;
            case 0xF40000E4:
                return EMVTransactionResultCode.AMEX_RESULT_OFFLINE_DECLINED;
            // 离线拒绝
            case 0xF40000E5:
                return EMVTransactionResultCode.DECLINED_BY_ONLINE;
            case 0xF40000E6:
                return EMVTransactionResultCode.ERROR_UNKNOWN;
            // CARD DETECT
            case CHECKCARD_TIMEOUT:
                return EMVTransactionResultCode.CARD_DETECT_TIMEOUT;
//            case CHECKCARD_BASE_ERROR:
            case CHECKCARD_MAG_SWIPE_ERROR:
            case CHECKCARD_IC_INSERT_ERROR:
            case CHECKCARD_IC_POWERUP_ERROR:
                return EMVTransactionResultCode.CARD_DETECT_IC_CARD_UNUSUAL;
            case CHECKCARD_RF_PASS_ERROR:
            case CHECKCARD_RF_ACTIVATE_ERROR:
                return EMVTransactionResultCode.CARD_DETECT_RF_CARD_UNUSUAL;
            case EMV_RESULT_CARDLOCK:
                return EMVTransactionResultCode.EMV_RESULT_CARD_BLOCKED;
            case EMV_RESULT_REPOWERICC:
                return EMVTransactionResultCode.EMV_RESULT_RESTART;
            case 0xf2c00002:
                return EMVTransactionResultCode.EMV_RESULT_RESTART;
            case 0xf30000e1:
            case 0xf2c00003:
                return EMVTransactionResultCode.AMEX_RESULT_TRY_ANOTHER_INTERFACE;
            default:
                EMVTransactionResultCode resultCode = EMVTransactionResultCode.ERROR_UNKNOWN;
                resultCode.setCode(result);
                resultCode.setMessage("Error Unknown");
                return resultCode;
        }
    }
}
