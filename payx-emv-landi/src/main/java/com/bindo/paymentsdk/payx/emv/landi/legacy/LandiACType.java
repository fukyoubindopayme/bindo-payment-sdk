package com.bindo.paymentsdk.payx.emv.landi.legacy;

import com.bindo.paymentsdk.payx.emv.core.enmus.ACType;

public class LandiACType implements com.landicorp.emvkernel.data.ACType {
    public static ACType toPayXACFlag(int acType) {
        switch (acType) {
            case AAC:
                return ACType.AAC;
            case TC:
                return ACType.TC;
            case ARQC:
                return ACType.ARQC;
        }
        return null;
    }
}
