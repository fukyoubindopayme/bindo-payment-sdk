package com.bindo.paymentsdk.v3.pay;

import com.bindo.paymentsdk.payx.emv.core.Candidate;
import com.bindo.paymentsdk.payx.emv.results.CallIssuerConfirmResult;
import com.bindo.paymentsdk.v3.pay.common.CardDetectMode;
import com.bindo.paymentsdk.v3.pay.common.InstallmentPlan;
import com.bindo.paymentsdk.v3.pay.common.TransactionData;
import com.bindo.paymentsdk.v3.pay.common.TransactionResultCode;
import com.bindo.paymentsdk.v3.pay.common.TransactionRetryFlag;
import com.bindo.paymentsdk.v3.pay.common.database.models.Currency;
import com.bindo.paymentsdk.v3.pay.common.legacy.CreditCard;
import com.bindo.paymentsdk.v3.pay.common.legacy.enums.CardReadMode;

import java.util.HashMap;
import java.util.List;

public interface PayProcessorListener {
    void onReset();

    void onRetry(TransactionRetryFlag retryFlag);

    void onCardDetected(CardReadMode cardReadMode, CreditCard creditCard);

    Candidate confirmApplicationSelection(List<Candidate> candidateList);

    void onReadRecord();

    void onCardholderVerification();

    void onRateLookup(CreditCard creditCard);

    Currency confirmCurrencySelection(List<Currency> currencyList, double transactionAmount, String markupText);

    List<InstallmentPlan> resolveInstallmentPlans();

    InstallmentPlan confirmInstallmentPlanSelection(List<InstallmentPlan> installmentPlans);

    void onPerformOnlineProcessing(CreditCard creditCard, HashMap<String, byte[]> tagLengthValues);

    void onPerformOnlineAuthenticationProcessing(CreditCard creditCard, HashMap<String, byte[]> tagLengthValues);

    void onPerformOnlineReversalProcessing(CreditCard creditCard, HashMap<String, byte[]> tagLengthValues);

    CallIssuerConfirmResult callIssuerConfirm();

    void onCompleted(TransactionResultCode transactionResultCode, TransactionData transactionData);

    void onError(String code, String message);
}
