package com.bindo.paymentsdk.v3.pay;

import android.content.Context;
import android.os.Build;

import com.bindo.paymentsdk.payx.emv.EMVCore;
import com.bindo.paymentsdk.payx.emv.landi.LandiEMVCore;
import com.bindo.paymentsdk.payx.emv.landi.LandiPinPad;
import com.bindo.paymentsdk.payx.emv.pinpad.PinPad;
import com.bindo.paymentsdk.payx.emv.urovo.UrovoEMVCore;
import com.bindo.paymentsdk.terminal.TerminalModel;

public class PayProcessorFactory {

    public static PayProcessor create(Context context) {
        String model = Build.MODEL;

        PinPad pinPad = null;
        EMVCore emvCore = null;

        switch (model) {
            case TerminalModel.LANDI_APOS_A8:
            case TerminalModel.LANDI_APOS_A9:
            case TerminalModel.LANDI_P960:
                LandiPinPad landiPinPad = new LandiPinPad();

                pinPad = landiPinPad;
                emvCore = new LandiEMVCore(context, landiPinPad);
                break;
            case TerminalModel.UROVO_I9000S:
                emvCore = new UrovoEMVCore();
                break;
            default:
                break;
        }

        return new PayProcessor(pinPad, emvCore, null);
    }
}
