package com.bindo.paymentsdk.v3.pay;

import android.content.Context;
import android.os.Build;

import com.bindo.paymentsdk.terminal.Terminal;
import com.bindo.paymentsdk.terminal.TerminalModel;
import com.bindo.paymentsdk.terminal.general.GeneralTerminal;
import com.bindo.paymentsdk.terminal.landi.APOSA8Terminal;
import com.bindo.paymentsdk.terminal.landi.P960Terminal;
import com.bindo.paymentsdk.terminal.urovo.I9000STerminal;

public class TerminalFactory {

    public static Terminal create(Context context) {
        String model = Build.MODEL;
        switch (model) {
            case TerminalModel.LANDI_APOS_A8:
            case TerminalModel.LANDI_APOS_A9:
                return new APOSA8Terminal(context);
            case TerminalModel.LANDI_P960:
                return new P960Terminal(context);
            case TerminalModel.UROVO_I9000S:
                return new I9000STerminal(context);
            default:
                return new GeneralTerminal();
        }
    }
}
