package com.bindo.paymentsdk.v3.pay;

import android.os.Build;
import android.os.Handler;
import android.util.Log;

import com.bindo.paymentsdk.payx.emv.EMVCore;
import com.bindo.paymentsdk.payx.emv.EMVCoreListener;
import com.bindo.paymentsdk.payx.emv.core.Application;
import com.bindo.paymentsdk.payx.emv.core.ApplicationData;
import com.bindo.paymentsdk.payx.emv.core.CAPK;
import com.bindo.paymentsdk.payx.emv.core.Candidate;
import com.bindo.paymentsdk.payx.emv.core.enmus.CVMFlag;
import com.bindo.paymentsdk.payx.emv.core.enmus.KernelID;
import com.bindo.paymentsdk.payx.emv.pinpad.PinPad;
import com.bindo.paymentsdk.payx.emv.results.CallIssuerConfirmResult;
import com.bindo.paymentsdk.payx.emv.results.CardholderVerificationResult;
import com.bindo.paymentsdk.payx.emv.results.EMVTransactionResultCode;
import com.bindo.paymentsdk.payx.emv.results.EMVTransactionResultData;
import com.bindo.paymentsdk.payx.emv.results.OnlineOfflineDecisionResult;
import com.bindo.paymentsdk.payx.emv.results.OnlineProcessingResult;
import com.bindo.paymentsdk.payx.emv.results.OnlineReversalProcessingResult;
import com.bindo.paymentsdk.terminal.TerminalModel;
import com.bindo.paymentsdk.v3.pay.common.CardDetectMode;
import com.bindo.paymentsdk.v3.pay.common.InstallmentPlan;
import com.bindo.paymentsdk.v3.pay.common.ReversalType;
import com.bindo.paymentsdk.v3.pay.common.TransactionData;
import com.bindo.paymentsdk.v3.pay.common.TransactionResultCode;
import com.bindo.paymentsdk.v3.pay.common.TransactionRetryFlag;
import com.bindo.paymentsdk.v3.pay.common.TransactionType;
import com.bindo.paymentsdk.v3.pay.common.database.Db;
import com.bindo.paymentsdk.v3.pay.common.database.models.Currency;
import com.bindo.paymentsdk.v3.pay.common.database.models.LocalBIN;
import com.bindo.paymentsdk.v3.pay.common.emv.enums.EMVTag;
import com.bindo.paymentsdk.v3.pay.common.emv.enums.FlowType;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Request;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Response;
import com.bindo.paymentsdk.v3.pay.common.gateway.enums.CurrencyCode;
import com.bindo.paymentsdk.v3.pay.common.legacy.CreditCard;
import com.bindo.paymentsdk.v3.pay.common.legacy.enums.CardReadMode;
import com.bindo.paymentsdk.v3.pay.common.util.CurrencyUtil;
import com.bindo.paymentsdk.v3.pay.common.util.DataConversion;
import com.bindo.paymentsdk.v3.pay.common.util.Hex;
import com.bindo.paymentsdk.v3.pay.gateway.Gateway;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class PayProcessor {
    private static final String TAG = "PayProcessor";

    private final byte[] DEFAULT_TERMINAL_TYPE = new byte[]{0x22};
    //private final byte[] DEFAULT_TERMINAL_TYPE = new byte[]{0x21}; // online only
    private final byte[] DEFAULT_TERMINAL_CAPABILITIES = new byte[]{(byte) 0xE0, (byte) 0xb8, (byte) 0xC8}; // 支持 Offline PIN 和签名

    private final int DEFAULT_RETRY_DELAY_MILLIS = 3000;

    private List<Application> mApplications = new ArrayList<>();

    private PinPad mPinPad;
    private EMVCore mEMVCore;
    private Gateway mGateway;
    private Gateway mPYCGateway;
    private Db mDb;
    private PayProcessorListener mListener;

    // 支付能力开关配置
    private boolean enableAlwaysUseSwipe = false;
    private boolean enableInstallmentPlan = false;
    private boolean enablePinPad = true;
    private boolean enableForceOnline = false;
    private boolean enableDynamicCurrencyConversion = false;

    // 冲下交易参数
    private int mOriginalTransactionSystemTraceAuditNumber;
    private Date mOriginalTransactionDateAndTime;
    // 交易相关参数
    private CreditCard mCreditCard;
    private TransactionData mTransactionData;
    private TransactionType mTransactionType;
    private int mTransactionSystemTraceAuditNumber;
    private Date mTransactionDateAndTime;
    private double mTransactionAmount;
    private String mTransactionCurrencyCode;
    private int mTransactionDetectRetryCount = 0;
    private boolean mForceMagstripeFallback = false;

    public PayProcessor(PinPad pinPad, EMVCore emvCore, Gateway gateway) {
        this(pinPad, emvCore, gateway, null);
    }

    public PayProcessor(PinPad pinPad, EMVCore emvCore, Gateway gateway, Db db) {
        this.mPinPad = pinPad;
        this.mEMVCore = emvCore;
        this.mGateway = gateway;
        this.mDb = db;

        if (this.mEMVCore != null) {
            this.mEMVCore.setListener(mEMVCoreListener);
        }
    }

    public boolean isSupported() {
        String model = Build.MODEL;
        switch (model) {
            case TerminalModel.LANDI_APOS_A8:
            case TerminalModel.LANDI_APOS_A9:
            case TerminalModel.LANDI_P960:
            case TerminalModel.UROVO_I9000S:
                return true;
        }
        return false;
    }

    public void setApplications(List<Application> applications) {
        this.mApplications = applications;
    }

    public void setPinPad(PinPad pinPad) {
        this.mPinPad = pinPad;
    }

    public void setEMVCore(EMVCore emvCore) {
        this.mEMVCore = emvCore;
    }

    public void setGateway(Gateway gateway) {
        this.mGateway = gateway;
    }

    public void setPYCGateway(Gateway pycGateway) {
        this.mPYCGateway = pycGateway;
    }

    public void setDb(Db db) {
        this.mDb = db;
    }

    public void setListener(PayProcessorListener listener) {
        this.mListener = listener;
    }

    public boolean isEnableAlwaysUseSwipe() {
        return enableAlwaysUseSwipe;
    }

    public void setEnableAlwaysUseSwipe(boolean enableAlwaysUseSwipe) {
        this.enableAlwaysUseSwipe = enableAlwaysUseSwipe;
    }

    public boolean isEnableInstallmentPlan() {
        return enableInstallmentPlan;
    }

    public void setEnableInstallmentPlan(boolean enableInstallmentPlan) {
        this.enableInstallmentPlan = enableInstallmentPlan;
    }

    public boolean isEnablePinPad() {
        return enablePinPad;
    }

    public void setEnablePinPad(boolean enablePinPad) {
        this.enablePinPad = enablePinPad;
    }

    public boolean isEnableForceOnline() {
        return enableForceOnline;
    }

    public void setEnableForceOnline(boolean enableForceOnline) {
        this.enableForceOnline = enableForceOnline;
        mEMVCore.setForceOnline(enableForceOnline);
    }

    public boolean isEnableDynamicCurrencyConversion() {
        return enableDynamicCurrencyConversion;
    }

    public void setEnableDynamicCurrencyConversion(boolean enableDynamicCurrencyConversion) {
        this.enableDynamicCurrencyConversion = enableDynamicCurrencyConversion;
    }

    public void startProcess(TransactionData transactionData, String type, int systemTraceAuditNumber, Date date, double amount) {
        this.mTransactionData = transactionData;
        this.mTransactionType = TransactionType.valueOf(type.toUpperCase());
        this.mTransactionSystemTraceAuditNumber = systemTraceAuditNumber;
        this.mTransactionDateAndTime = date;
//        if (mTransactionData.isTransactionPyc()) {
//            this.mTransactionAmount = mTransactionData.getCardholderBillingAmount();
//            this.mTransactionCurrencyCode = mTransactionData.getCardholderBillingCurrencyCode();
//        } else {
//            this.mTransactionAmount = amount;
//            this.mTransactionCurrencyCode = CurrencyCode.Hong_Kong_Dollar.getCurrencyCode();
//            this.mTransactionData.setTransactionAmount(mTransactionAmount);
//            this.mTransactionData.setTransactionCurrencyCode(mTransactionCurrencyCode);
//        }
        this.mTransactionAmount = amount;
        this.mTransactionCurrencyCode = CurrencyCode.Hong_Kong_Dollar.getCurrencyCode();
        this.mTransactionData.setTransactionAmount(mTransactionAmount);
        this.mTransactionData.setTransactionCurrencyCode(mTransactionCurrencyCode);
        this.mTransactionDetectRetryCount = 0;

        mTransactionData.setTransactionType(mTransactionType.name());
        mEMVCore.setTransactionType(mTransactionType);
        mTransactionData.setTransactionSystemTraceAuditNumber(mTransactionSystemTraceAuditNumber);
        mTransactionData.setTransactionDateAndTime(mTransactionDateAndTime);

        mTransactionData.setPrimaryAccountNumber(null);
        mTransactionData.setCardDetectMode(null);
        mTransactionData.setCardExpirationDate(null);

        if (!this.isEnableAlwaysUseSwipe()) {
            mEMVCore.setRetryFlag(TransactionRetryFlag.DEFAULT);
            mEMVCore.setSupportICCard(true);
            mEMVCore.setSupportRFCard(true);
            mEMVCore.setSupportMagCard(true);
        } else {
            mEMVCore.setRetryFlag(TransactionRetryFlag.FORCE_USE_MAG);
            mEMVCore.setSupportICCard(false);
            mEMVCore.setSupportRFCard(false);
            mEMVCore.setSupportMagCard(true);
        }
        mEMVCore.setListener(mEMVCoreListener);
        // 当强制使用磁条卡时，全部使用 fallback 交易方式
        mEMVCore.startProcess(this.isEnableAlwaysUseSwipe());
    }

    public void stopProcess() {
        mEMVCore.setListener(null);
        mEMVCore.stopProcess();
    }

    private boolean isForeignCard(String pan) {
        List<LocalBIN> localBINs = mDb.table(LocalBIN.class).all();

        if (localBINs == null) {
            return false;
        }

        for (int i = 0; i < localBINs.size(); i++) {
            LocalBIN localBIN = localBINs.get(i);
            String lowPrefixNumber = localBIN.getLowPrefixNumber();
            String highPrefixNumber = localBIN.getHighPrefixNumber();

            if (lowPrefixNumber.compareTo(pan.substring(0, lowPrefixNumber.length())) <= 0 &&
                    highPrefixNumber.compareTo(pan.substring(0, highPrefixNumber.length())) >= 0) {
                return true;
            }
        }

        return true;
    }

    public void handleDynamicCurrencyConversionFlowIfNeed(CreditCard creditCard) {
        if (!(isEnableDynamicCurrencyConversion() && isForeignCard(creditCard.getCardNumber()))) {
            return;
        }

        mListener.onRateLookup(creditCard);

        Request requestForRateLookup = new Request();
        requestForRateLookup.setTransactionType(TransactionType.PRE_AUTHORIZATION);
        requestForRateLookup.setTransactionSystemTraceAuditNumber(mTransactionSystemTraceAuditNumber);
        requestForRateLookup.setTransactionDateAndTime(mTransactionDateAndTime);
        requestForRateLookup.setTransactionAmount(mTransactionAmount);
        requestForRateLookup.setTransactionCurrencyCode(this.mTransactionCurrencyCode);

        String cardDetectMode = creditCard.getCardDetectMode();

        requestForRateLookup.setPrimaryAccountNumber(creditCard.getCardNumber());
        requestForRateLookup.setCardDetectMode(cardDetectMode);
        requestForRateLookup.setCardExpirationDate(creditCard.getExpireDate());
        if (creditCard.getMagData() != null) {
            requestForRateLookup.setTrack1DiscretionaryData(creditCard.getMagData().getTrack1());
            requestForRateLookup.setTrack2DiscretionaryData(creditCard.getMagData().getTrack2());
            requestForRateLookup.setTrack2EquivalentData(creditCard.getMagData().getTrack2());
        } else {
            requestForRateLookup.setTrack1DiscretionaryData(creditCard.getEmvData().getTrack1());
            requestForRateLookup.setTrack2DiscretionaryData(creditCard.getEmvData().getTrack2());
            requestForRateLookup.setTrack2EquivalentData(creditCard.getEmvData().getTrack2());
            requestForRateLookup.setTagLengthValues(creditCard.getEmvData().getTagLengthValues());
        }

        Response responseForRateLookup = mPYCGateway.sendRequest(requestForRateLookup);

        if (!responseForRateLookup.getIsApproved()) {
            return;
        }

        Currency ourCurrency = new Currency();
        try {
            ourCurrency.setCurrencyName(CurrencyUtil.getCurrencyName(mTransactionCurrencyCode));
            ourCurrency.setNumericCode(mTransactionCurrencyCode);
            ourCurrency.setAlphabeticCode(CurrencyUtil.toAlphabeticCode(mTransactionCurrencyCode));
        } catch (Exception e) {
            return;
        }

        List<Currency> currencies = responseForRateLookup.getPlanetPaymentCurrencies();
        currencies.add(ourCurrency);


        Currency selectedCurrency = mListener.confirmCurrencySelection(currencies, mTransactionAmount, responseForRateLookup.getTransactionPYCRateMarkupText());
        Log.d(TAG, selectedCurrency.getCurrencyName());

        if (mTransactionCurrencyCode.equals(selectedCurrency.getNumericCode())) {
            // OPT-OUT
            // start new EMV transaction and send AUTHORIZATION to BEA
            mTransactionData.setTransactionOptOut(false);
            mTransactionData.setTransactionPyc(false);
        } else {
            // OPT-IN
            // start new EMV transaction and send AUTHORIZATION to PP
            mTransactionData.setTransactionAmount(mTransactionAmount);
            mTransactionData.setTransactionCurrencyCode(mTransactionCurrencyCode);
            mTransactionData.setCardholderBillingAmount(responseForRateLookup.getCardholderBillingAmount());
            mTransactionData.setCardholderBillingCurrencyCode(selectedCurrency.getNumericCode());
            mTransactionData.setTransactionPyc(true);
            mTransactionData.setTransactionOptOut(false);
            mTransactionAmount = responseForRateLookup.getCardholderBillingAmount();
            mTransactionCurrencyCode = selectedCurrency.getNumericCode();
        }
    }

    private EMVCoreListener mEMVCoreListener = new EMVCoreListener() {

        @Override
        public void onError(String code, String message) {
            mListener.onError(code, message);
            PayProcessor.this.stopProcess();
        }

        @Override
        public void onReset() {
            if (mTransactionDetectRetryCount == 0) {
                mListener.onReset();
            }
            for (Application application : mApplications) {
                mEMVCore.addApplication(application);
            }
        }

        @Override
        public void onCardDetected(CardReadMode cardReadMode, CreditCard creditCard) {
            mCreditCard = creditCard;
            mListener.onCardDetected(cardReadMode, creditCard);
            if (cardReadMode == CardReadMode.SWIPE || cardReadMode == CardReadMode.FALLBACK_SWIPE) {
                mTransactionData.setCardExpirationDate(creditCard.getExpireDate());
                mTransactionData.setTrack2EquivalentData(creditCard.getMagData().getTrack2());
                mTransactionData.setTrack1DiscretionaryData(creditCard.getMagData().getTrack1());
                Log.e(TAG, "onCardDetected: " +mTransactionData.toString());
            }
        }

        @Override
        public Candidate confirmApplicationSelection(List<Candidate> candidateList) {
            Candidate selectedCandidate = mListener.confirmApplicationSelection(candidateList);

            String cardDetectMode = CardDetectMode.MANUAL;
            switch (mCreditCard.getCardReadMode()) {
                case CONTACT:
                    cardDetectMode = CardDetectMode.CONTACT;
                    break;
                case CONTACTLESS:
                    cardDetectMode = CardDetectMode.CONTACTLESS;
                    break;
            }

            // Handle contact & contactless DCC Flow.
            if (CardDetectMode.CONTACT.equals(cardDetectMode) ||
                    CardDetectMode.CONTACTLESS.equals(cardDetectMode)) {
//                handleDynamicCurrencyConversionFlowIfNeed(mCreditCard);
            }

            return selectedCandidate;
        }

        @Override
        public HashMap<String, byte[]> resolveTerminalParameters(ApplicationData applicationData) {
            KernelID kernelID = applicationData.getKernelID();
            byte[] applicationID = applicationData.getApplicationID();

            HashMap<String, byte[]> tagLengthValues = new HashMap<>();

            Application selectedApplication = null;
            for (Application application : mEMVCore.getApplications()) {
                if (Hex.encode(applicationID).contains(Hex.encode(application.getApplicationID()))) {
                    selectedApplication = application;
                    break;
                }
            }

            if (selectedApplication == null) {
                return null;
            }

            tagLengthValues.put(EMVTag.TERMINAL_TYPE.toString(), DEFAULT_TERMINAL_TYPE);
            tagLengthValues.put(EMVTag.TERMINAL_CAPABILITIES.toString(), DEFAULT_TERMINAL_CAPABILITIES);
//            tagLengthValues.put(EMVTag.ADDITIONAL_TERMINAL_CAPABILITIES.toString(), new byte[]{0x01});
            tagLengthValues.put(EMVTag.TERMINAL_COUNTRY_CODE.toString(), new byte[]{0x03, 0x44});
            tagLengthValues.put(EMVTag.TERMINAL_FLOOR_LIMIT.toString(), DataConversion.convertFloatToHexBytes(selectedApplication.getFloorLimit()));

            tagLengthValues.putAll(mEMVCore.resolveSpecificTerminalParameters(applicationData));
            return tagLengthValues;
        }

        @Override
        public HashMap<String, byte[]> resolvePreTransactionParameters(ApplicationData applicationData) {
            HashMap<String, byte[]> tagLengthValues = new HashMap<>();

            SimpleDateFormat sdfDate = new SimpleDateFormat("yyMMdd");
            SimpleDateFormat sdfTime = new SimpleDateFormat("hhmmss");

            SimpleDateFormat.getDateInstance(DateFormat.MEDIUM, Locale.US);

            String transactionAmountHexString = new DecimalFormat("0000000000.00").format(mTransactionAmount).replace(".", "");
            String transactionDateHexString = sdfDate.format(mTransactionDateAndTime);
            String transactionTimeHexString = sdfTime.format(mTransactionDateAndTime);
            String transactionSequenceCounterHexString = String.format("%08d", mTransactionSystemTraceAuditNumber);
            String transactionCurrencyCode = "0" + mTransactionCurrencyCode;

            switch (mTransactionType) {
                default:
                case AUTHORIZATION:
                    Log.e(TAG, "resolvePreTransactionParameters: transaction type is purchase");
                    tagLengthValues.put(EMVTag.TRANSACTION_TYPE.toString(), new byte[]{0x00});
                    break;
                case REFUND:
                    Log.e(TAG, "resolvePreTransactionParameters: transaction type is refund");
                    tagLengthValues.put(EMVTag.TRANSACTION_TYPE.toString(), new byte[]{0x00});
                    break;
            }

            tagLengthValues.put(EMVTag.AMOUNT_AUTHORISED_NUMERIC.toString(), Hex.decode(transactionAmountHexString));
            tagLengthValues.put(EMVTag.AMOUNT_OTHER_NUMERIC.toString(), new byte[]{0x00, 0x00, 0x00, 0x00, 0x00, 0x00});
            tagLengthValues.put(EMVTag.TRANSACTION_DATE.toString(), Hex.decode(transactionDateHexString));
            tagLengthValues.put(EMVTag.TRANSACTION_TIME.toString(), Hex.decode(transactionTimeHexString));
            tagLengthValues.put(EMVTag.TRANSACTION_SEQUENCE_COUNTER.toString(), Hex.decode(transactionSequenceCounterHexString));
            tagLengthValues.put(EMVTag.TRANSACTION_CURRENCY_CODE.toString(), Hex.decode(transactionCurrencyCode));

            System.out.println("EMV TAGS:");
            System.out.println(EMVTag.AMOUNT_AUTHORISED_NUMERIC.toString() + ": " + (transactionAmountHexString));
            System.out.println(EMVTag.TRANSACTION_DATE.toString() + ": " + (transactionDateHexString));
            System.out.println(EMVTag.TRANSACTION_TIME.toString() + ": " + (transactionTimeHexString));
            System.out.println(EMVTag.TRANSACTION_SEQUENCE_COUNTER.toString() + ": " + (transactionSequenceCounterHexString));
            System.out.println(EMVTag.TRANSACTION_CURRENCY_CODE.toString() + ": " + (transactionCurrencyCode));

            // 添加不同设备特有的预交易参数
            tagLengthValues.putAll(mEMVCore.resolveSpecificPreTransactionParameters(applicationData));

            return tagLengthValues;
        }

        @Override
        public void onReadApplicationData(ApplicationData applicationData, byte capkIndex) {
            byte[] applicationID = applicationData.getApplicationID();
            byte[] rid = new byte[5];

            mListener.onReadRecord();

            System.arraycopy(applicationID, 0, rid, 0, 5);

            Application selectedApplication = null;
            for (Application application : mEMVCore.getApplications()) {
                if (Hex.encode(applicationID).contains(Hex.encode(application.getApplicationID()))) {
                    selectedApplication = application;
                    break;
                }
            }
            if (selectedApplication != null) {
                // 查找并载入公钥
                for (CAPK capk : selectedApplication.getCertificationAuthorityPublicKeys()) {
                    if (capk.getCAPKIndex() == capkIndex && Arrays.equals(rid, capk.getRID())) {
                        mEMVCore.setCertificationAuthorityPublicKey(capk);
                        break;
                    }
                }
            }

            byte[] t2Data = null;

            switch (applicationData.getFlowType()) {
                case FlowType.AMEX_EMV:

                    break;
                case FlowType.AMEX_MAGSRTIPE:

                    break;
                case FlowType.AMEX_MOBILE_MAGSTRIPE:

                    break;
                case FlowType.ECASH:

                    break;
                case FlowType.EMV:
                    t2Data = mEMVCore.getTLV(EMVTag.TRACK_2_EQUIVALENT_DATA.toString());

                    if (t2Data != null && t2Data.length > 0) {
                        String track2 = Hex.encode(t2Data);

                        mTransactionData.setTrack2EquivalentData(track2);
                        mTransactionData.setPrimaryAccountNumber(track2.split("D")[0]);
                        mTransactionData.setCardExpirationDate(track2.split("D")[1].substring(0, 4));
                    }
                    break;
                case FlowType.MSD:

                    break;
                case FlowType.MSD_LEGACY:

                    break;
                case FlowType.PAYPASS_CHIP:
                    byte[] pcData = mEMVCore.getTLV(EMVTag.TRACK_2_EQUIVALENT_DATA.toString());

                    if (pcData != null && pcData.length > 0) {
                        String track2 = Hex.encode(pcData);

                        mTransactionData.setTrack2EquivalentData(track2);
                        mTransactionData.setPrimaryAccountNumber(track2.split("D")[0]);
                        mTransactionData.setCardExpirationDate(track2.split("D")[1].substring(0, 4));
                    }
                    break;
                case FlowType.PAYPASS_STRIP:
                    byte[] data = mEMVCore.getTLV(EMVTag.TRACK2_DATA.toString());

                    if (data != null && data.length > 0) {
                        String track2 = Hex.encode(data);

                        mTransactionData.setTrack2EquivalentData(track2);
                        mTransactionData.setPrimaryAccountNumber(track2.split("D")[0]);
                        mTransactionData.setCardExpirationDate(track2.split("D")[1].substring(0, 4));
                    }

                    break;
                case FlowType.PBOC_CONTACTLESS:

                    break;
                case FlowType.QPBOC:

                    break;

                case FlowType.VISA_PAYWAVE2:
                    t2Data = mEMVCore.getTLV(EMVTag.TRACK_2_EQUIVALENT_DATA.toString());

                    if (t2Data != null && t2Data.length > 0) {
                        String track2 = Hex.encode(t2Data);

                        mTransactionData.setTrack2EquivalentData(track2);
                        mTransactionData.setPrimaryAccountNumber(track2.split("D")[0]);
                        mTransactionData.setCardExpirationDate(track2.split("D")[1].substring(0, 4));
                    }
                    break;
                case FlowType.VISA_QVSDC:
                    t2Data = mEMVCore.getTLV(EMVTag.TRACK_2_EQUIVALENT_DATA.toString());

                    if (t2Data != null && t2Data.length > 0) {
                        String track2 = Hex.encode(t2Data);

                        mTransactionData.setTrack2EquivalentData(track2);
                        mTransactionData.setPrimaryAccountNumber(track2.split("D")[0]);
                        mTransactionData.setCardExpirationDate(track2.split("D")[1].substring(0, 4));
                    }
                    break;
                default:
                    break;
            }

            final HashMap<String, byte[]> tagLengthValues = new HashMap<>();
            for (EMVTag emvTag : EMVTag.values()) {
                byte[] value = mEMVCore.getTLV(emvTag);
                if (value != null && value.length > 0) {
                    tagLengthValues.put(emvTag.toString(), value);
                }
            }
            String track1 = mTransactionData.getTrack1DiscretionaryData();
            String track2 = mTransactionData.getTrack2EquivalentData();
            CreditCard.EmvData emvData = new CreditCard.EmvData(track1, track2, tagLengthValues);

            mCreditCard.setCardNumber(mTransactionData.getPrimaryAccountNumber());
            mCreditCard.setExpireDate(mTransactionData.getCardExpirationDate());
            mCreditCard.setEmvData(emvData);

            handleDynamicCurrencyConversionFlowIfNeed(mCreditCard);
        }

        @Override
        public HashMap<String, byte[]> resolveTransactionParameters(ApplicationData applicationData) {
            HashMap<String, byte[]> tagLengthValues = new HashMap<>();

            // 添加不同设备特有的预交易参数
            tagLengthValues.putAll(mEMVCore.resolveSpecificTransactionParameters(applicationData));

            String transactionAmountHexString = new DecimalFormat("0000000000.00").format(mTransactionAmount).replace(".", "");
            String transactionCurrencyCode = "0" + mTransactionCurrencyCode;
            tagLengthValues.put(EMVTag.AMOUNT_AUTHORISED_NUMERIC.toString(), Hex.decode(transactionAmountHexString));
            tagLengthValues.put(EMVTag.TRANSACTION_CURRENCY_CODE.toString(), Hex.decode(transactionCurrencyCode));

            return tagLengthValues;
        }

        @Override
        public CardholderVerificationResult cardholderVerification(CVMFlag cvmFlag) {
            mListener.onCardholderVerification();
            CardholderVerificationResult cardholderVerificationResult = CardholderVerificationResult.ERROR;

            if (mPinPad == null || !isEnablePinPad()) {
                return cardholderVerificationResult;
            }

            switch (cvmFlag) {
                case OFFLINE_PIN:
                    cardholderVerificationResult = mPinPad.verifyPinByPinPad(false, new byte[]{0x00, 0x04, 06});
                    break;
                // 当前不支持 Online PIN，所有出现 Online PIN 的均返回 ERROR 。
                case ONLINE_PIN:
                default:
                    cardholderVerificationResult = CardholderVerificationResult.ERROR;
                    break;
            }
            return cardholderVerificationResult;
        }

        @Override
        public OnlineOfflineDecisionResult onlineOfflineDecision(CardReadMode cardReadMode, CreditCard creditCard) {
            OnlineOfflineDecisionResult onlineOfflineDecisionResult = OnlineOfflineDecisionResult.UNSURE;

            switch (cardReadMode) {
                case CONTACT:
                    // 由内核决定，无须干预
                    break;
                case CONTACTLESS:
                    onlineOfflineDecisionResult = OnlineOfflineDecisionResult.ONLINE;
                    break;
                case SWIPE:
                    String serviceCode = creditCard.getServiceCode();
                    // 强制要求使用 IC 卡进行交易
                    if ((serviceCode.startsWith("2") || serviceCode.startsWith("6")) && !mForceMagstripeFallback) {
                        mListener.onRetry(TransactionRetryFlag.FORCE_USE_CHIP);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mTransactionDetectRetryCount = 0;
                                mEMVCore.setRetryFlag(TransactionRetryFlag.FORCE_USE_CHIP);
                                mEMVCore.setSupportICCard(true);
                                mEMVCore.setSupportRFCard(true);
                                mEMVCore.setSupportMagCard(false);
                                mEMVCore.startProcess();
                            }
                        }, DEFAULT_RETRY_DELAY_MILLIS);
                        onlineOfflineDecisionResult = OnlineOfflineDecisionResult.UNSURE;
                    } else {
                        onlineOfflineDecisionResult = OnlineOfflineDecisionResult.ONLINE;
                    }
                    break;
                case FALLBACK_SWIPE:
                    // 降级交易，默认联机
                    onlineOfflineDecisionResult = OnlineOfflineDecisionResult.ONLINE;
                    break;
            }

            return onlineOfflineDecisionResult;
        }

        @Override
        public OnlineProcessingResult performOnlineProcessing(CreditCard creditCard, HashMap<String, byte[]> tagLengthValues) {
            String cardDetectMode = creditCard.getCardDetectMode();

            // Handle swipe & manual DCC Flow.
            if (CardDetectMode.MANUAL.equals(cardDetectMode) ||
                CardDetectMode.SWIPE.equals(cardDetectMode) ||
                CardDetectMode.FALLBACK_SWIPE.equals(cardDetectMode)) {
                handleDynamicCurrencyConversionFlowIfNeed(mCreditCard);
            }

            OnlineProcessingResult onlineProcessingResult = new OnlineProcessingResult();
            Request request = new Request(tagLengthValues);

            mListener.onPerformOnlineProcessing(creditCard, tagLengthValues);

            // Transaction fields.
            request.setTransactionType(mTransactionType);
            request.setTransactionSystemTraceAuditNumber(mTransactionSystemTraceAuditNumber);
            request.setTransactionDateAndTime(mTransactionDateAndTime);
            request.setPrimaryAccountNumber(creditCard.getCardNumber());
            request.setCardDetectMode(cardDetectMode);
            request.setCardExpirationDate(creditCard.getExpireDate());
            if (creditCard.getMagData() != null) {
                request.setTrack1DiscretionaryData(creditCard.getMagData().getTrack1());
                request.setTrack2DiscretionaryData(creditCard.getMagData().getTrack2());
                request.setTrack2EquivalentData(creditCard.getMagData().getTrack2());
            } else {
                request.setTrack1DiscretionaryData(creditCard.getEmvData().getTrack1());
                request.setTrack2DiscretionaryData(creditCard.getEmvData().getTrack2());
                request.setTrack2EquivalentData(creditCard.getEmvData().getTrack2());
            }

            Response response;

            if (mTransactionData.isTransactionPyc()) {
                request.setTransactionAmount(mTransactionData.getTransactionAmount());
                request.setTransactionCurrencyCode(mTransactionData.getTransactionCurrencyCode());
                request.setCardholderBillingAmount(mTransactionData.getCardholderBillingAmount());
                request.setCardholderBillingCurrencyCode(mTransactionData.getCardholderBillingCurrencyCode());
            } else {
                request.setTransactionAmount(mTransactionData.getTransactionAmount());
                request.setTransactionCurrencyCode(mTransactionData.getTransactionCurrencyCode());
            }


            // Enable DCC & is Foreign Card
            if (isEnableDynamicCurrencyConversion() &&
                    isForeignCard(creditCard.getCardNumber()) &&
                    mTransactionData.isTransactionPyc()) {

                mListener.onPerformOnlineAuthenticationProcessing(creditCard, tagLengthValues);

                request.setTransactionType(TransactionType.AUTHORIZATION);
                response = mPYCGateway.sendRequest(request);
//                if (!mTransactionData.isTransactionPyc()) {
//                    mListener.onRateLookup();
//                    TransactionType originalTransactionType = request.getTransactionType();
//                    request.setTransactionType(TransactionType.PRE_AUTHORIZATION);
//
//                    responseForAuthorization = mPYCGateway.sendRequest(request);
//
//                    List<Currency> currencyList = responseForAuthorization.getPlanetPaymentCurrencies();
//
//                    Currency ourCurrency = new Currency();
//                    ourCurrency.setNumericCode(mTransactionCurrencyCode);
//                    currencyList.add(ourCurrency);
//                    Currency currency = mListener.confirmCurrencySelection(currencyList, mTransactionAmount, responseForAuthorization.getTransactionPYCRateMarkupText());
//
//                    if (request.getTransactionCurrencyCode().equals(currency.getNumericCode())) {
//                        // OPT-OUT
//                        // start new EMV transaction and send AUTHORIZATION to BEA
//                        mTransactionData.setTransactionOptOut(true);
//                        mTransactionData.setTransactionPyc(false);
//                        setEnableDynamicCurrencyConversion(false);
//                    } else {
//                        // OPT-IN
//                        // start new EMV transaction and send AUTHORIZATION to PP
//                        mTransactionData.setTransactionAmount(mTransactionAmount);
//                        mTransactionData.setTransactionCurrencyCode(mTransactionCurrencyCode);
//                        mTransactionData.setCardholderBillingAmount(responseForAuthorization.getCardholderBillingAmount());
//                        mTransactionData.setCardholderBillingCurrencyCode(currency.getNumericCode());
//                        mTransactionData.setTransactionPyc(true);
//                        mTransactionData.setTransactionOptOut(false);
//                        mTransactionType = TransactionType.AUTHORIZATION;
//                    }
//                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            startProcess(mTransactionData, mTransactionType.name(), mTransactionSystemTraceAuditNumber, mTransactionDateAndTime, mTransactionAmount);
//                        }
//                    }, 300);
//                } else {
//                    mTransactionType = TransactionType.AUTHORIZATION;
//                    responseForAuthorization = mPYCGateway.sendRequest(request);
//                }
            } else {
                // 选择分期付款计划
                if (isEnableInstallmentPlan()) {
                    List<InstallmentPlan> installmentPlans = mListener.resolveInstallmentPlans();

                    InstallmentPlan installmentPlan = mListener.confirmInstallmentPlanSelection(installmentPlans);
                    if (installmentPlan != null && installmentPlan.getNumberOfInstallment() > 0) {
                        request.setNumberOfInstallment(installmentPlan.getNumberOfInstallment());
                    }
                }

                mListener.onPerformOnlineAuthenticationProcessing(creditCard, tagLengthValues);
                response = mGateway.sendRequest(request);
            }


            mTransactionData.setTerminalId(request.getTerminalIdentification());
            mTransactionData.setMerchantId(request.getMerchantIdentifier());


            mTransactionData.setTransactionType(mTransactionType.name());
            mTransactionData.setTransactionSystemTraceAuditNumber(mTransactionSystemTraceAuditNumber);
            mTransactionData.setTransactionDateAndTime(mTransactionDateAndTime);
            mTransactionData.setTransactionAmount(mTransactionAmount);

            mTransactionData.setPrimaryAccountNumber(creditCard.getCardNumber());
            mTransactionData.setCardExpirationDate(creditCard.getExpireDate());

            mTransactionData.setRequest(request);
            mTransactionData.setResponse(response);

            if (response != null) {
                onlineProcessingResult.setApproved(response.isApproved());
                onlineProcessingResult.setApprovalCode(response.getApprovalCode());
                onlineProcessingResult.setNeedCallIssuerConfirm(response.getIsNeedCallIssuerConfirm());
                onlineProcessingResult.setIccRelatedDataIssuerScriptData(response.getIccRelatedDataIssuerScriptData());
                onlineProcessingResult.setIccRelatedDataIssuerAuthenticationData(response.getIccRelatedDataIssuerAuthenticationData());

                mTransactionData.setTransactionId(response.getTransactionId());
                mTransactionData.setApprovalCode(response.getApprovalCode());
                mTransactionData.setPosDataCode(response.getPosDataCode());
                mTransactionData.setActionCode(response.getActionCode());
                mTransactionData.setAuthorizationCode(response.getAuthorizationCode());
                mTransactionData.setRetrievalReferenceNumber(response.getRetrievalReferenceNumber());
                mTransactionData.setTransactionPYCRateMarkupSign(response.getTransactionPYCRateMarkupSign());
                mTransactionData.setTransactionPYCRateMarkupText(response.getTransactionPYCRateMarkupText());

                // 授权异常处理（未联网或连接超时）
                if (response.getErrors() != null) {
                    for (Response.Error error : response.getErrors()) {
                        switch (error.getCode()) {
                            case Response.Error.ERROR_CODE_UNKNOWN_HOST:
                                onlineProcessingResult.setUnableToGoOnline(true);
                                break;
                            case Response.Error.ERROR_CODE_TIMEOUT:
                                onlineProcessingResult.setRequestTimeout(true);
                                break;
                        }
                    }
                }
            }

            // 冲正时使用的变量。
            mOriginalTransactionSystemTraceAuditNumber = mTransactionSystemTraceAuditNumber;
            mOriginalTransactionDateAndTime = mTransactionDateAndTime;

            Log.d(TAG, "isApproved: " + onlineProcessingResult.isApproved());
            Log.d(TAG, "isUnableToGoOnline: " + onlineProcessingResult.isUnableToGoOnline());
            Log.d(TAG, "isRequestTimeout: " + onlineProcessingResult.isRequestTimeout());
            return onlineProcessingResult;
        }

        @Override
        public CallIssuerConfirmResult callIssuerConfirm() {
            return mListener.callIssuerConfirm();
        }

        @Override
        public HashMap<String, byte[]> resolveOnlineProcessingResult(OnlineProcessingResult onlineProcessingResult, CallIssuerConfirmResult callIssuerConfirmResult) {
            HashMap<String, byte[]> tagLengthValues = new HashMap<>();

            tagLengthValues.putAll(mEMVCore.resolveSpecificOnlineProcessingResult(onlineProcessingResult, callIssuerConfirmResult));

            return tagLengthValues;
        }

        @Override
        public OnlineReversalProcessingResult performOnlineReversalProcessing(int reason, OnlineProcessingResult onlineProcessingResult, CreditCard creditCard, HashMap<String, byte[]> tagLengthValues) {
            Log.d(TAG, "ReversalReason: " + reason);
            mListener.onPerformOnlineReversalProcessing(creditCard, tagLengthValues);

            int transactionSystemTraceAuditNumber = mOriginalTransactionSystemTraceAuditNumber + 1;
            Date transactionDateAndTime = new Date();

            Request request = new Request(tagLengthValues);

            String cardDetectMode = creditCard.getCardReadMode().name().toLowerCase();

            request.setOriginalTransactionSystemTraceAuditNumber(mOriginalTransactionSystemTraceAuditNumber);
            request.setOriginalTransactionDateAndTime(mOriginalTransactionDateAndTime);

            request.setTransactionType(TransactionType.REVERSAL);
            request.setTransactionSystemTraceAuditNumber(transactionSystemTraceAuditNumber);
            request.setTransactionDateAndTime(transactionDateAndTime);
            request.setTransactionAmount(mTransactionAmount);
            request.setTransactionCurrencyCode(mTransactionCurrencyCode);
            request.setAcquirerReferenceData(mTransactionData.getTransactionId());
            request.setPrimaryAccountNumber(creditCard.getCardNumber());
            request.setCardDetectMode(cardDetectMode);

            request.setCardExpirationDate(creditCard.getExpireDate());
            if (creditCard.getMagData() != null) {
                request.setTrack1DiscretionaryData(creditCard.getMagData().getTrack1());
                request.setTrack2DiscretionaryData(creditCard.getMagData().getTrack2());
                request.setTrack2EquivalentData(creditCard.getMagData().getTrack2());
            } else {
                request.setTrack1DiscretionaryData(creditCard.getEmvData().getTrack1());
                request.setTrack2DiscretionaryData(creditCard.getEmvData().getTrack2());
                request.setTrack2EquivalentData(creditCard.getEmvData().getTrack2());
            }
            if (onlineProcessingResult.isRequestTimeout()) {
                request.setReversalType(ReversalType.SYSTEM_GENERATED);
            }
            Response response = mGateway.sendRequest(request);

            OnlineReversalProcessingResult onlineReversalProcessingResult = new OnlineReversalProcessingResult();
            onlineReversalProcessingResult.setApproved(response.isApproved());

            mTransactionData.setRequestForReversal(request);
            mTransactionData.setResponseForReversal(response);

            return onlineReversalProcessingResult;
        }

        @Override
        public void onTransactionCompleted(EMVTransactionResultCode emvTransactionResultCode, final EMVTransactionResultData emvTransactionResultData) {
            TransactionResultCode transactionResultCode = TransactionResultCode.ERROR_UNKNOWN;
            final CreditCard creditCard = emvTransactionResultData.getCreditCard();

            String cardDetectMode = CardDetectMode.MANUAL;
            switch (creditCard.getCardReadMode()) {
                case CONTACT:
                    cardDetectMode = CardDetectMode.CONTACT;
                    break;
                case CONTACTLESS:
                    cardDetectMode = CardDetectMode.CONTACTLESS;
                    break;
                case SWIPE:
                    cardDetectMode = CardDetectMode.SWIPE;
                    break;
                case FALLBACK_SWIPE:
                    cardDetectMode = CardDetectMode.FALLBACK_SWIPE;
                    break;
                case MANUAL:
                    cardDetectMode = CardDetectMode.MANUAL;
                    break;
            }

            mTransactionData.setPrimaryAccountNumber(creditCard.getCardNumber());
            mTransactionData.setCardDetectMode(cardDetectMode);
            mTransactionData.setCardExpirationDate(creditCard.getExpireDate());
            mTransactionData.setTagLengthValues(emvTransactionResultData.getTagLengthValues());

            byte[] balance = mEMVCore.getTLV("9F5D");

            if (balance != null && balance.length > 0) {
                Log.e(TAG, "onTransactionCompleted: balance:" + Hex.encode(balance));
                double sbalance = Double.parseDouble(Hex.encode(balance)) / 100;
                Log.e(TAG, "onTransactionCompleted: formated balance:" + String.format("%.2f", sbalance));
                mTransactionData.setAvailableOfflineSpendingAmount(String.format("%.2f", sbalance));
            }

            // 刷卡和手输卡号需要请求签名
            if (emvTransactionResultData.getCardReadMode() == CardReadMode.SWIPE ||
                emvTransactionResultData.getCardReadMode() == CardReadMode.FALLBACK_SWIPE ||
                emvTransactionResultData.getCardReadMode() == CardReadMode.MANUAL) {
                mTransactionData.setTransactionRequestSignature(true);
            } else {
                mTransactionData.setTransactionRequestSignature(emvTransactionResultData.getCVMFlag() == CVMFlag.SIGNATURE);
            }

            // 只有这两种情况 TransactionData 的 Approved 为 true.
            if (emvTransactionResultCode == EMVTransactionResultCode.APPROVED_BY_ONLINE ||
                emvTransactionResultCode == EMVTransactionResultCode.APPROVED_BY_OFFLINE) {
                mTransactionData.setApproved(true);
            }

            // 交易被批准备或被拒绝
            if (emvTransactionResultCode == EMVTransactionResultCode.APPROVED_BY_ONLINE ||
                emvTransactionResultCode == EMVTransactionResultCode.APPROVED_BY_OFFLINE ||
                emvTransactionResultCode == EMVTransactionResultCode.DECLINED_BY_ONLINE ||
                emvTransactionResultCode == EMVTransactionResultCode.DECLINED_BY_OFFLINE ||
                emvTransactionResultCode == EMVTransactionResultCode.DECLINED_BY_TIMEOUT_AND_REVERSED ||
                emvTransactionResultCode == EMVTransactionResultCode.DECLINED_BY_TERMINAL_AND_REVERSED ||
                emvTransactionResultCode == EMVTransactionResultCode.AMEX_RESULT_OFFLINE_DECLINED) {

                switch (emvTransactionResultCode) {
                    case APPROVED_BY_OFFLINE:
                        transactionResultCode = TransactionResultCode.APPROVED_BY_OFFLINE;
                        break;
                    case APPROVED_BY_ONLINE:
                        transactionResultCode = TransactionResultCode.APPROVED_BY_ONLINE;
                        break;
                    case DECLINED_BY_OFFLINE:
                        transactionResultCode = TransactionResultCode.DECLINED_BY_OFFLINE;
                        break;
                    case DECLINED_BY_ONLINE:
                        transactionResultCode = TransactionResultCode.DECLINED_BY_ONLINE;
                        if (CardDetectMode.CONTACTLESS.equals(cardDetectMode)) {
                            byte flowType = mTransactionData.getTagLengthValues().get(EMVTag.CUSTOM_FLOW_TYPE.toString())[0];
                            if (flowType == FlowType.PAYPASS_STRIP && "65".equals(mTransactionData.getActionCode())) {
                                mListener.onRetry(TransactionRetryFlag.FORCE_USE_CHIP_AND_MAG);
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        mTransactionDetectRetryCount += 1;
                                        mEMVCore.setRetryFlag(TransactionRetryFlag.FORCE_USE_CHIP_AND_MAG);
                                        mEMVCore.setSupportICCard(true);
                                        mEMVCore.setSupportRFCard(false);
                                        mEMVCore.setSupportMagCard(true);
                                        mEMVCore.startProcess();
                                    }
                                }, DEFAULT_RETRY_DELAY_MILLIS);

                                return;
                            }
                        }
                        break;
                    case DECLINED_BY_TIMEOUT_AND_REVERSED:
                        transactionResultCode = TransactionResultCode.DECLINED_BY_TIMEOUT_AND_REVERSED;
                        break;
                    case DECLINED_BY_TERMINAL_AND_REVERSED:
                        transactionResultCode = TransactionResultCode.DECLINED_BY_TERMINAL_AND_REVERSED;
                        break;
                    case AMEX_RESULT_OFFLINE_DECLINED:
                        transactionResultCode = TransactionResultCode.DECLINED_BY_OFFLINE;
                        break;
                }

                if (emvTransactionResultData.getTagLengthValues() != null) {
                    HashMap<String, byte[]> tagLengthValues = emvTransactionResultData.getTagLengthValues();

                    byte[] terminalVerificationResults = tagLengthValues.get(EMVTag.TERMINAL_VERIFICATION_RESULTS.toString());
                    byte[] cardholderVerificationMethodResults = tagLengthValues.get(EMVTag.CARDHOLDER_VERIFICATION_METHOD_RESULTS.toString());

                    if (cardholderVerificationMethodResults != null && cardholderVerificationMethodResults.length > 0) {
                        Log.d(TAG, "CVR: " + Hex.encode(cardholderVerificationMethodResults));
                        switch ((cardholderVerificationMethodResults[0] & 0x3f)) {
                            case 0x01:
                            case 0x02:
                            case 0x03:
                            case 0x04:
                            case 0x05:
                                if (terminalVerificationResults[2] == 0x00) {
                                    mTransactionData.setTransactionPinVerified(true);
                                }
                                break;
                        }
                    }
                }
                // 这两种情况下设置 Approved 为 true 。
                if (transactionResultCode == TransactionResultCode.APPROVED_BY_OFFLINE ||
                        transactionResultCode == TransactionResultCode.APPROVED_BY_ONLINE) {
                    mTransactionData.setApproved(true);
                }
            } else {
                // 异常处理流程

                Log.e(TAG, "onTransactionCompleted: emvTransactionResultCode:" + emvTransactionResultCode);

                switch (emvTransactionResultCode) {
                    // 读卡超时
                    case CARD_DETECT_TIMEOUT:
                        transactionResultCode = TransactionResultCode.ERROR_CARD_DETECT_TIMEOUT;
                        break;
                    case CARD_DETECT_IC_CARD_UNUSUAL:
                    case ERROR_UNKNOWN: // for correct processing of fallback (AXP EMV 007)
                        // 当 IC 卡读卡异常时进行降级处理逻辑
                        if (mTransactionDetectRetryCount < 2) {
                            mForceMagstripeFallback = true;
                            mListener.onRetry(TransactionRetryFlag.DEFAULT);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mTransactionDetectRetryCount += 1;
                                    mEMVCore.setRetryFlag(TransactionRetryFlag.DEFAULT);
                                    mEMVCore.setSupportICCard(true);
                                    mEMVCore.setSupportRFCard(true);
                                    mEMVCore.setSupportMagCard(true);
                                    mEMVCore.startProcess();
                                }
                            }, 2000);
                        } else {
                            mListener.onRetry(TransactionRetryFlag.FORCE_USE_MAG);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mTransactionDetectRetryCount += 1;
                                    mEMVCore.setRetryFlag(TransactionRetryFlag.FORCE_USE_MAG);
                                    mEMVCore.setSupportICCard(false);
                                    mEMVCore.setSupportRFCard(false);
                                    mEMVCore.setSupportMagCard(true);
                                    mEMVCore.startProcess(true);
                                }
                            }, 2000);
                        }
                        return;
                    case CARD_DETECT_RF_CARD_UNUSUAL:
                        transactionResultCode = TransactionResultCode.ERROR_CARD_DETECT_MULTIPLE_CARDS;
                        break;
                    case CARD_DETECT_MAG_CARD_UNUSUAL:
                        transactionResultCode = TransactionResultCode.ERROR_CARD_DETECT_UNUSUAL;
                        break;
                    // 内核找不到卡片的 APP ，降级为磁卡交易
                    case EMV_RESULT_NO_APP:
                        switch (creditCard.getCardReadMode()) {
                            case CONTACT:
                                mListener.onRetry(TransactionRetryFlag.FORCE_USE_MAG);
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        mTransactionDetectRetryCount += 1;
                                        mEMVCore.setRetryFlag(TransactionRetryFlag.FORCE_USE_MAG);
                                        mEMVCore.setSupportICCard(false);
                                        mEMVCore.setSupportRFCard(false);
                                        mEMVCore.setSupportMagCard(true);
                                        mEMVCore.startProcess(true);
                                    }
                                }, DEFAULT_RETRY_DELAY_MILLIS);
                                transactionResultCode = TransactionResultCode.ERROR_CARD_NOT_ACCEPTED;
                                break;
                            case CONTACTLESS:
                                transactionResultCode = TransactionResultCode.ERROR_UNKNOWN;
                                break;
                            case SWIPE:
                                break;
                            case FALLBACK_SWIPE:
                                break;
                            case MANUAL:
                                break;
                        }

//                        return;
                        break;
                    case EMV_RESULT_APP_BLOCKED:
                    case EMV_RESULT_CARD_BLOCKED:
                        transactionResultCode = TransactionResultCode.ERROR_CARD_BLOCKED;
                        break;
                    case AMEX_RESULT_TRY_ANOTHER_INTERFACE:
                        // 开启一个支持IC卡和磁条卡的处理
                        mListener.onRetry(TransactionRetryFlag.FORCE_USE_CHIP_AND_MAG);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mTransactionDetectRetryCount += 1;
                                mEMVCore.setRetryFlag(TransactionRetryFlag.FORCE_USE_CHIP_AND_MAG);
                                mEMVCore.setSupportICCard(true);
                                mEMVCore.setSupportRFCard(false);
                                mEMVCore.setSupportMagCard(true);
                                mEMVCore.startProcess();
                            }
                        }, DEFAULT_RETRY_DELAY_MILLIS);

                        return;
                    case AMEX_RESULT_TRY_ANOTHER_PAYMENT:
                        transactionResultCode = TransactionResultCode.ERROR_UNKNOWN;
                        break;
                    // SEE PHONE FOR INSTRUCTIONS
                    case AMEX_RESULT_TRY_AGAIN:
                    case EMV_RESULT_REPOWERICC:
                    case EMV_RESULT_RESTART:
                        mListener.onRetry(TransactionRetryFlag.SEE_PHONE_FOR_INSTRUCTIONS);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mTransactionDetectRetryCount += 1;
                                mEMVCore.setRetryFlag(TransactionRetryFlag.SEE_PHONE_FOR_INSTRUCTIONS);
                                mEMVCore.setSupportICCard(false);
                                mEMVCore.setSupportRFCard(true);
                                mEMVCore.setSupportMagCard(false);
                                mEMVCore.startProcess(false, mTransactionDetectRetryCount);
                            }
                        }, DEFAULT_RETRY_DELAY_MILLIS);
                        return;
                    default:
                        transactionResultCode = TransactionResultCode.ERROR_UNKNOWN;
                        break;
                }
            }

//            if (transactionResultCode == TransactionResultCode.APPROVED_BY_OFFLINE) {
//                new Thread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Request request = new Request(emvTransactionResultData.getTagLengthValues());
//
//                        request.setMessageTypeId("1220");
//                        request.setTransactionType(mTransactionType);
//                        request.setTransactionSystemTraceAuditNumber(mTransactionSystemTraceAuditNumber);
//                        request.setTransactionDateAndTime(mTransactionDateAndTime);
//                        request.setTransactionAmount(mTransactionAmount);
//                        request.setTransactionCurrencyCode(mTransactionCurrencyCode);
//                        // Card fields.
//                        String cardDetectMode = CardDetectMode.MANUAL;
//                        switch (creditCard.getCardReadMode()) {
//                            case CONTACT:
//                                cardDetectMode = CardDetectMode.CONTACT;
//                                break;
//                            case CONTACTLESS:
//                                cardDetectMode = CardDetectMode.CONTACTLESS;
//                                break;
//                            case SWIPE:
//                                cardDetectMode = CardDetectMode.SWIPE;
//                                break;
//                            case FALLBACK_SWIPE:
//                                cardDetectMode = CardDetectMode.FALLBACK_SWIPE;
//                                break;
//                            case MANUAL:
//                                cardDetectMode = CardDetectMode.MANUAL;
//                                break;
//                        }
//
//                        request.setPrimaryAccountNumber(creditCard.getCardNumber());
//                        request.setCardDetectMode(cardDetectMode);
//
//                        request.setCardExpirationDate(creditCard.getExpireDate());
//                        if (creditCard.getMagData() != null) {
//                            request.setTrack1DiscretionaryData(creditCard.getMagData().getTrack1());
//                            request.setTrack2DiscretionaryData(creditCard.getMagData().getTrack2());
//                            request.setTrack2EquivalentData(creditCard.getMagData().getTrack2());
//                        } else {
//                            request.setTrack1DiscretionaryData(creditCard.getEmvData().getTrack1());
//                            request.setTrack2DiscretionaryData(creditCard.getEmvData().getTrack2());
//                            request.setTrack2EquivalentData(creditCard.getEmvData().getTrack2());
//                        }
//
//                        mGateway.sendRequest(request);
//                    }
//                }).start();
//            }


            String applicationLabel = new String(mEMVCore.getTLV(EMVTag.APPLICATION_LABEL.toString()));
            String applicationIdentifier = Hex.encode(mEMVCore.getTLV(EMVTag.APPLICATION_IDENTIFIER_TERMINAL.toString()));
            String terminalVerificationResult = Hex.encode(mEMVCore.getTLV(EMVTag.TERMINAL_VERIFICATION_RESULTS.toString()));
            String transactionStatusInformation = Hex.encode(mEMVCore.getTLV(EMVTag.TRANSACTION_STATUS_INFORMATION.toString()));

            Map<String, byte[]> tlv = mTransactionData.getRequest().getTagLengthValues();
            if (tlv != null && !tlv.isEmpty()) {
                String applicationCryptogram = Hex.encode(tlv.get(EMVTag.APPLICATION_CRYPTOGRAM.toString()));
                String issuerApplicationData = Hex.encode(tlv.get(EMVTag.ISSUER_APPLICATION_DATA.toString()));
                String unpredictableNumber = Hex.encode(tlv.get(EMVTag.UNPREDICTABLE_NUMBER.toString()));
                String applicationTransactionCounter = Hex.encode(tlv.get(EMVTag.APPLICATION_TRANSACTION_COUNTER.toString()));
                String terminalVerificationResults = Hex.encode(tlv.get(EMVTag.TERMINAL_VERIFICATION_RESULTS.toString()));
                String transactionDate = Hex.encode(tlv.get(EMVTag.TRANSACTION_DATE.toString()));
                String transactionType = Hex.encode(tlv.get(EMVTag.TRANSACTION_TYPE.toString()));
                String amountAuthorized = Hex.encode(tlv.get(EMVTag.AMOUNT_AUTHORISED_NUMERIC.toString()));
                String transactionCurrencyCode = Hex.encode(tlv.get(EMVTag.TRANSACTION_CURRENCY_CODE.toString()));
                String terminalCountryCode = Hex.encode(tlv.get(EMVTag.TERMINAL_COUNTRY_CODE.toString()));
                String applicationInterchangeProfile = Hex.encode(tlv.get(EMVTag.APPLICATION_INTERCHANGE_PROFILE.toString()));
                String amountOther = Hex.encode(tlv.get(EMVTag.AMOUNT_OTHER_NUMERIC.toString()));
                String applicationPanSequence = Hex.encode(tlv.get(EMVTag.APPLICATION_PRIMARY_ACCOUNT_NUMBER_SEQUENCE_NUMBER.toString()));
                String cryptogramInformationData = Hex.encode(tlv.get(EMVTag.CRYPTOGRAM_INFORMATION_DATA.toString()));

                mTransactionData.setApplicationCryptogram(applicationCryptogram);
                mTransactionData.setIssuerApplicationData (issuerApplicationData);
                mTransactionData.setUnpredictableNumber (unpredictableNumber);
                mTransactionData.setApplicationTransactionCounter (applicationTransactionCounter);
                mTransactionData.setTerminalVerificationResults (terminalVerificationResults);
                mTransactionData.setIccTransactionDate (transactionDate);
                mTransactionData.setIccTransactionType (transactionType);
                mTransactionData.setAmountAuthorized (amountAuthorized);
                mTransactionData.setIccTransactionCurrencyCode (transactionCurrencyCode);
                mTransactionData.setTerminalCountryCode (terminalCountryCode);
                mTransactionData.setApplicationInterchangeProfile (applicationInterchangeProfile);
                mTransactionData.setAmountOther (amountOther);
                mTransactionData.setApplicationPanSequence (applicationPanSequence);
                mTransactionData.setCryptogramInformationData (cryptogramInformationData);
            }

            mTransactionData.setApplicationLabel(applicationLabel);
            mTransactionData.setApplicationIdentifier(applicationIdentifier);
            mTransactionData.setTerminalVerificationResult(terminalVerificationResult);
            mTransactionData.setTransactionStatusInformation(transactionStatusInformation);

            switch (transactionResultCode) {
                case APPROVED_BY_OFFLINE:
                case APPROVED_BY_ONLINE:
                    if (mDb != null) {
                        mDb.table(TransactionData.class).create(mTransactionData);
                    }
                    break;
            }

            mListener.onCompleted(transactionResultCode, mTransactionData);
        }
    };
}
