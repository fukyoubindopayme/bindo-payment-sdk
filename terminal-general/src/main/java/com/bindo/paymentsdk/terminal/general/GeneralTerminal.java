package com.bindo.paymentsdk.terminal.general;

import com.bindo.paymentsdk.terminal.BarcodeReader;
import com.bindo.paymentsdk.terminal.GiftCardReader;
import com.bindo.paymentsdk.terminal.OctopusCardReader;
import com.bindo.paymentsdk.terminal.Printer;
import com.bindo.paymentsdk.terminal.Terminal;

public class GeneralTerminal implements Terminal {

    private Printer mBuiltInPrinter;
    private BarcodeReader mBarcodeReader;
    private GiftCardReader mGiftCardReader;
    private OctopusCardReader mOctopusCardReader;

    public GeneralTerminal() {
        mBuiltInPrinter = new GeneralPrinter();
        mBarcodeReader = new GeneralBarcodeReader();
        mGiftCardReader = new GeneralGiftCardReader();
        mOctopusCardReader = new GeneralOctopusCardReader();
    }

    @Override
    public Printer getBuiltInPrinter() {
        return mBuiltInPrinter;
    }

    @Override
    public BarcodeReader getBarcodeReader() {
        return mBarcodeReader;
    }

    @Override
    public GiftCardReader getGiftCardReader() {
        return mGiftCardReader;
    }

    @Override
    public OctopusCardReader getOctopusCardReader() {
        return mOctopusCardReader;
    }
}
