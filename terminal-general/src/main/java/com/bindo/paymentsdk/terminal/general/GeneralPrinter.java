package com.bindo.paymentsdk.terminal.general;

import com.bindo.paymentsdk.terminal.Printer;

public class GeneralPrinter extends Printer {
    private static final String TAG = "GeneralPrinter";

    @Override
    public boolean isSupported() {
        return false;
    }

    @Override
    public void print(Paper paper) {
        mListener.onError("-1", "This device may not be supported.");
    }
}
