package com.bindo.paymentsdk.terminal.general;

import com.bindo.paymentsdk.terminal.OctopusCardReader;

public class GeneralOctopusCardReader implements OctopusCardReader {
    private OctopusCardReader.Listener mListener;

    @Override
    public boolean isSupported() {
        return false;
    }

    @Override
    public void setListener(OctopusCardReader.Listener listener) {
        mListener = listener;
    }

    @Override
    public void start() {
        mListener.onError("-1", "This device may not be supported.");
    }

    @Override
    public void stop() {
        mListener.onError("-1", "This device may not be supported.");
    }
}
