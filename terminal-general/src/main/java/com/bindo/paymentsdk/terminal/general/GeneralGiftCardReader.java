package com.bindo.paymentsdk.terminal.general;

import com.bindo.paymentsdk.terminal.GiftCardReader;

public class GeneralGiftCardReader implements GiftCardReader {
    private static final String TAG = "GeneralGiftCardReader";

    private Listener mListener;

    public GeneralGiftCardReader() {
    }

    @Override
    public boolean isSupported() {
        return false;
    }

    @Override
    public void setListener(Listener listener) {
        mListener = listener;
    }

    @Override
    public void start() {
        mListener.onError("-1", "This device may not be supported.");
    }

    @Override
    public void stop() {
        mListener.onError("-1", "This device may not be supported.");
    }
}
