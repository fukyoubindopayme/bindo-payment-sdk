package com.bindo.paymentsdk.terminal.general;

import com.bindo.paymentsdk.terminal.BarcodeReader;

public class GeneralBarcodeReader implements BarcodeReader {
    private static final String TAG = "GeneralBarcodeReader";

    private Listener mListener;

    public GeneralBarcodeReader() {
    }

    @Override
    public boolean isSupported() {
        return false;
    }

    @Override
    public void setListener(Listener listener) {
        mListener = listener;
    }

    @Override
    public void openScanner() {
        mListener.onError("-1", "This device may not be supported.");
    }

    @Override
    public void closeScanner() {
        mListener.onError("-1", "This device may not be supported.");
    }

    @Override
    public void startDecode() {
        mListener.onError("-1", "This device may not be supported.");
    }

    @Override
    public void stopDecode() {
        mListener.onError("-1", "This device may not be supported.");
    }

    @Override
    public void startScan() {
        mListener.onError("-1", "This device may not be supported.");
    }

    @Override
    public void stopScan() {
        mListener.onError("-1", "This device may not be supported.");
    }
}
