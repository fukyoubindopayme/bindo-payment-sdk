package com.bindo.paymentsdk.v3.pay.gateway.bea;

import com.bindo.paymentsdk.v3.pay.common.TransactionData;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Request;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Response;

import java.util.HashMap;

/**
 * Created by Mark on 2017/11/9.
 */

public class OfflineSaleAdvice {

    private TransactionData mTransactionData;

    public OfflineSaleAdvice(TransactionData transactionData) {
        this.mTransactionData = transactionData;
    }

    public Response createAndSendRequest(HashMap<String, String> configMap) throws Exception {
        Response response = new Response();
        BankOfEastAsiaGateway gateway = new BankOfEastAsiaGateway(configMap);

        Request request = new Request();
        request.setMessageTypeId("0220");
        request.setProcessingCode("000000");
        request.setCardExpirationDate(mTransactionData.getCardExpirationDate());
        request.setTransactionType(mTransactionData.getTransactionType());
        request.setTransactionAmount(mTransactionData.getTransactionAmount());
        request.setCardDetectMode(mTransactionData.getCardDetectMode());
        request.setTrack1DiscretionaryData(mTransactionData.getTrack1DiscretionaryData());
        request.setTrack2EquivalentData(mTransactionData.getTrack2EquivalentData());
        request.setTrack2DiscretionaryData(mTransactionData.getTrack2DiscretionaryData());
        request.setIccRelatedData(mTransactionData.getIccRelatedData());
        request.setTransactionDateAndTime(mTransactionData.getTransactionDateAndTime());
        request.setTransactionSystemTraceAuditNumber(mTransactionData.getTransactionSystemTraceAuditNumber());

        response = gateway.sendRequest(request);


        return response;
    }
}
