package com.bindo.paymentsdk.v3.pay.gateway.bea;

import com.bindo.paymentsdk.v3.pay.common.emv.enums.EMVTag;
import com.bindo.paymentsdk.v3.pay.common.emv.enums.FlowType;
import com.bindo.paymentsdk.v3.pay.common.gateway.SocketService;
import com.bindo.paymentsdk.v3.pay.common.CardDetectMode;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Request;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Response;
import com.bindo.paymentsdk.v3.pay.common.util.Hex;
import com.bindo.paymentsdk.v3.pay.common.util.StringUtils;
import com.bindo.paymentsdk.v3.pay.gateway.Gateway;

import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.SSLException;

public class BankOfEastAsiaGateway implements Gateway {
    private final String TAG = "BankOfEastAsiaGateway";

    public static final String CONFIG_REQUEST_CARD_ACCEPTOR_TERMINAL_ID = "config_request_card_acceptor_terminal_id";
    public static final String CONFIG_REQUEST_CARD_ACCEPTOR_ID_CODE = "config_request_card_acceptor_id_code";
    public static final String CONFIG_REQUEST_TPDU = "config_request_tpdu";
    public static final String CONFIG_REQUEST_EDS = "config_request_eds";
    public static final String CONFIG_TMK = "config_tmk";
    public static final String CONFIG_IP = "ip";
    public static final String CONFIG_PORT = "port";

    private HashMap<String, String> mConfigMap = new HashMap<>();

    private SocketService mSocketService = new SocketService();

    public BankOfEastAsiaGateway(HashMap<String, String> configMap) throws Exception {
        if (configMap == null || configMap.size() == 0) {
            System.out.println("Please set the gateway parameters");
            throw new Exception("Please set the gateway parameters");
        } else {
            for (String key : configMap.keySet()) {
                String value = configMap.get(key);
                mConfigMap.put(key, value);
            }
        }

        if (StringUtils.isEmpty(mConfigMap.get(CONFIG_IP))) {
            throw new Exception("please set the gateway param: ip");
        }
        if (StringUtils.isEmpty(mConfigMap.get(CONFIG_PORT))) {
            throw new Exception("please set the gateway param: port");
        }
        if (StringUtils.isEmpty(mConfigMap.get(CONFIG_TMK))) {
            throw new Exception("please set the gateway param: config_tmk");
        }
        if (StringUtils.isEmpty(mConfigMap.get(CONFIG_REQUEST_EDS))) {
            throw new Exception("please set the gateway param: config_request_eds");
        }
        if (StringUtils.isEmpty(mConfigMap.get(CONFIG_REQUEST_TPDU))) {
            throw new Exception("please set the gateway param: config_request_tpdu");
        }
        if (StringUtils.isEmpty(mConfigMap.get(CONFIG_REQUEST_CARD_ACCEPTOR_ID_CODE))) {
            throw new Exception("please set the gateway param: config_request_card_acceptor_id_code");
        }
        if (StringUtils.isEmpty(mConfigMap.get(CONFIG_REQUEST_CARD_ACCEPTOR_TERMINAL_ID))) {
            throw new Exception("please set the gateway param: config_request_card_acceptor_terminal_id");
        }
    }

    private int[] addElementToArray(int[] arr, int num) {
        int[] result = new int[arr.length + 1];
        for (int i = 0; i < arr.length; i++) {
            result[i] = arr[i];
        }
        result[result.length - 1] = num;
        return result;
    }

    private byte[] prepareBeaAuthorizationMessage(byte[] primitiveISO8583Message, String TPDU, String EDS) {
        if (primitiveISO8583Message == null) {
            return null;
        }
        byte[] key = Hex.decode((String) mConfigMap.get(CONFIG_TMK));

        int appDataLen = primitiveISO8583Message.length - (2 + 2 + 8);

        appDataLen = ((appDataLen + 7) / 8) * 8;
        byte[] plaintextMsg = new byte[appDataLen];

        byte[] eds = Hex.decode(EDS);
        byte[] tpdu = Hex.decode(TPDU);
        byte[] finalBeaISO8583Msg = new byte[plaintextMsg.length + tpdu.length + eds.length + 2 + 2 + 8];

        finalBeaISO8583Msg[0] = (byte) ((finalBeaISO8583Msg.length - 2) / 256);
        finalBeaISO8583Msg[1] = (byte) ((finalBeaISO8583Msg.length - 2) % 256);
        System.arraycopy(tpdu, 0, finalBeaISO8583Msg, 2, tpdu.length);
        eds[4] = (byte) (appDataLen / 256);
        eds[5] = (byte) (appDataLen % 256);
        System.arraycopy(eds, 0, finalBeaISO8583Msg, 2 + tpdu.length, eds.length);
        System.arraycopy(primitiveISO8583Message, 2, finalBeaISO8583Msg, 2 + tpdu.length + eds.length, 2 + 8);
        System.arraycopy(primitiveISO8583Message, 2 + 2 + 8, plaintextMsg, 0, primitiveISO8583Message.length - (2 + 2 + 8));


        byte[] encryptedText = CryptUtils.encryptBy3DES(key, plaintextMsg);

        System.arraycopy(encryptedText, 0, finalBeaISO8583Msg, 2 + tpdu.length + eds.length + 2 + 8, encryptedText.length);

        return finalBeaISO8583Msg;
    }

    @Override
    public Response sendRequest(Request request) {

        request.setGatewayConfigs(mConfigMap);

        int[] fieldSet = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 35, 41, 42, 55, 60, 62};

        if (request.getCardDetectMode() != null) {
            String posEntryMode = "";

            switch (request.getCardDetectMode()) {
                case CardDetectMode.CONTACT:
                    posEntryMode = "05";
                    break;
                case CardDetectMode.CONTACTLESS:
                    byte flowType = request.getTagLengthValues().get(EMVTag.CUSTOM_FLOW_TYPE.toString())[0];
                    if (flowType == FlowType.PAYPASS_STRIP) {
                        posEntryMode = "91";
                    } else {
                        posEntryMode = "07";
                    }
                    break;
                case CardDetectMode.SWIPE:
                    posEntryMode = "90";
                    fieldSet = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 35, 41, 42, 62};
                    break;
                case CardDetectMode.FALLBACK_SWIPE:
                    posEntryMode = "80";
                    fieldSet = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 35, 41, 42, 62};
                    break;
                case CardDetectMode.MANUAL:
                    posEntryMode = "01";
                    fieldSet = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 41, 42, 62};
                    break;
            }

            posEntryMode += "2";
            request.setPOSEntryMode(posEntryMode);
        }


        if (request.getTransactionType() != null) {
            switch (request.getTransactionType()) {
                case AUTHORIZATION:
                case PRE_AUTHORIZATION:
                    request.setMessageTypeId("0100");
                    request.setProcessingCode("000000");
                    switch (request.getCardDetectMode()) {
                        case CardDetectMode.CONTACT:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 35, 41, 42, 55};
                            break;
                        case CardDetectMode.CONTACTLESS:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 35, 41, 42, 55};
                            break;
                        case CardDetectMode.SWIPE:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 35, 41, 42};
                            break;
                        case CardDetectMode.FALLBACK_SWIPE:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 35, 41, 42};
                            break;
                        case CardDetectMode.MANUAL:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 41, 42};
                            break;
                    }
                    break;
                case AUTHORIZATION_VOID:
                    request.setMessageTypeId("0100");
                    request.setProcessingCode("020000");
                    switch (request.getCardDetectMode()) {
                        case CardDetectMode.CONTACT:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 12, 13, 14, 22, 24, 25, 35, 37, 38, 41, 42, 55};
                            break;
                        case CardDetectMode.CONTACTLESS:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 12, 13, 14, 22, 24, 25, 35, 37, 38, 41, 42, 55};
                            break;
                        case CardDetectMode.SWIPE:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 12, 13, 14, 22, 24, 25, 35, 37, 38, 41, 42};
                            break;
                        case CardDetectMode.FALLBACK_SWIPE:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 12, 13, 14, 22, 24, 25, 35, 37, 38, 41, 42};
                            break;
                        case CardDetectMode.MANUAL:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 12, 13, 14, 22, 24, 25, 37, 38, 41, 42};
                            break;
                    }
                    break;
                case SALE:
                    request.setMessageTypeId("0200");
                    request.setProcessingCode("000000");
                    switch (request.getCardDetectMode()) {
                        case CardDetectMode.CONTACT:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 35, 41, 42, 55, 60, 62};
                            break;
                        case CardDetectMode.CONTACTLESS:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 35, 41, 42, 55, 60, 62};
                            break;
                        case CardDetectMode.SWIPE:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 35, 41, 42, 60, 62};
                            break;
                        case CardDetectMode.FALLBACK_SWIPE:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 35, 41, 42, 60, 62};
                            break;
                        case CardDetectMode.MANUAL:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 41, 42, 60, 62};

                            break;
                    }
                    break;
                case VOID_SALE:
                    request.setMessageTypeId("0200");
                    request.setProcessingCode("020000");
                    switch (request.getCardDetectMode()) {
                        case CardDetectMode.CONTACT:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 35, 37, 41, 42, 55, 60, 62};
                            break;
                        case CardDetectMode.CONTACTLESS:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 35, 37, 41, 42, 55, 60, 62};
                            break;
                        case CardDetectMode.SWIPE:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 35, 37, 41, 42, 60, 62};
                            break;
                        case CardDetectMode.FALLBACK_SWIPE:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 35, 37, 41, 42, 60, 62};
                            break;
                        case CardDetectMode.MANUAL:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 37, 41, 42, 60, 62};
                            break;
                    }
                    break;
                case PRE_AUTHORIZATION_COMPLETION:
                    request.setMessageTypeId("0220");
                    request.setProcessingCode("000000");
                    switch (request.getCardDetectMode()) {
                        case CardDetectMode.CONTACT:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 12, 13, 14, 22, 24, 25, 37, 38, 41, 42, 60, 62};
                            break;
                        case CardDetectMode.CONTACTLESS:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 12, 13, 14, 22, 24, 25, 37, 38, 41, 42, 60, 62};
                            break;
                        case CardDetectMode.SWIPE:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 12, 13, 14, 22, 24, 25, 37, 38, 41, 42, 60, 62};
                            break;
                        case CardDetectMode.FALLBACK_SWIPE:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 12, 13, 14, 22, 24, 25, 37, 38, 41, 42, 60, 62};
                            break;
                        case CardDetectMode.MANUAL:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 12, 13, 14, 22, 24, 25, 37, 38, 41, 42, 60, 62};
                            break;
                    }
                    break;
                case REFUND:
                    request.setMessageTypeId("0200");
                    request.setProcessingCode("200000");
                    switch (request.getCardDetectMode()) {
                        case CardDetectMode.CONTACT:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 35, 41, 42, 55, 60, 62};
                            break;
                        case CardDetectMode.CONTACTLESS:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 35, 41, 42, 55, 60, 62};
                            break;
                        case CardDetectMode.SWIPE:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 35, 41, 42, 60, 62};
                            break;
                        case CardDetectMode.FALLBACK_SWIPE:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 35, 41, 42, 60, 62};
                            break;
                        case CardDetectMode.MANUAL:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 41, 42, 60, 62};
                            break;
                    }
                    break;
                case REFUND_VOID:
                    request.setMessageTypeId("0200");
                    request.setProcessingCode("220000");
                    switch (request.getCardDetectMode()) {
                        case CardDetectMode.CONTACT:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 35, 37, 41, 42, 55, 60, 62};
                            break;
                        case CardDetectMode.CONTACTLESS:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 35, 37, 41, 42, 55, 60, 62};
                            break;
                        case CardDetectMode.SWIPE:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 35, 37, 41, 42, 60, 62};
                            break;
                        case CardDetectMode.FALLBACK_SWIPE:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 35, 37, 41, 42, 60, 62};
                            break;
                        case CardDetectMode.MANUAL:
                            fieldSet = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 37, 41, 42, 60, 62};
                            break;
                    }
                    break;
                case ADJUST_SALE:
                    request.setMessageTypeId("0220");
                    request.setProcessingCode("020000");
                    fieldSet = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 37, 38, 41, 42, 54, 60, 62};
                    break;
                case REVERSAL:
                    request.setMessageTypeId("0400");
                    break;
            }
        }

        // Soft Descriptor
        if (request.getSoftDescriptor() != null) {
            fieldSet = addElementToArray(fieldSet, 43);
        }

        if (request.getNumberOfInstallment() > 0) {
            fieldSet = addElementToArray(fieldSet, 63);
        }

        if (request.getFieldIdSet() == null) {
            request.setFieldIdSet(fieldSet);
        }

        Response response = new Response(request);
        List<Response.Error> errors = new ArrayList<>();
        request.setPOSConditionCode("00");
        request.setNetworkInternationalIdentifier("028");
        request.setTerminalIdentification((String) mConfigMap.get(CONFIG_REQUEST_CARD_ACCEPTOR_TERMINAL_ID));
        request.setMerchantIdentifier((String) mConfigMap.get(CONFIG_REQUEST_CARD_ACCEPTOR_ID_CODE));
        byte[] requestForISO8583RepoBytes = ISO8583Conversion.requestToISO8583RepoBytes(request);

        String tpdu = mConfigMap.get(CONFIG_REQUEST_TPDU);
        String eds = mConfigMap.get(CONFIG_REQUEST_EDS);


        byte[] finalISO8583Message = null;

        if (requestForISO8583RepoBytes != null) {
            finalISO8583Message = prepareBeaAuthorizationMessage(requestForISO8583RepoBytes, tpdu, eds);

            System.out.println("org iso8583Msg:" + Hex.encode(requestForISO8583RepoBytes));
            System.out.println("final iso8583Msg:" + Hex.encode(finalISO8583Message));
        } else {
            System.out.println("requestForISO8583RepoBytes is null, please check your request");
        }

        try {
            String ip = mConfigMap.get(CONFIG_IP);
            String port = mConfigMap.get(CONFIG_PORT);
            mSocketService.setHostIp(ip);
            mSocketService.setHostPort(port);
            mSocketService.connect();
            if (mSocketService.isConnected()) {
                mSocketService.sendMessage(finalISO8583Message);
                byte[] receivedDataBytes = mSocketService.receiveMessage();
                response = ISO8583Conversion.iso8583RepoBytesToResponse(response, receivedDataBytes);
            } else {
                errors.add(new Response.Error("-1", "can't connect to BEA host"));
            }
        } catch (SSLException e) {
            // TODO: Handle ssl exception
            e.printStackTrace();
            errors.add(new Response.Error("-1", e.getMessage()));
        } catch (SocketException e) {
            // TODO: Handle socket exception
            e.printStackTrace();
            errors.add(new Response.Error("-1", e.getMessage()));
        } catch (Exception e) {
            // TODO: Handle socket exception
            e.printStackTrace();
            errors.add(new Response.Error("-1", e.getMessage()));
        } finally {
            try {
                mSocketService.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response == null) {
                response = new Response();
                response.setErrors(errors);
            }
        }

        return response;
    }


    public Response sendBatchUploadRequest(Request request) {

        request.setMessageTypeId("0320");

        int[] fieldSet = new int[]{0, 2, 3, 4, 11, 12, 13, 14, 22, 24, 25, 35, 37, 38, 39, 41, 42, 54, 60, 62};

        if (request.getCardDetectMode() != null) {
            String posEntryMode = "";

            switch (request.getCardDetectMode()) {
                case CardDetectMode.CONTACT:
                    posEntryMode = "05";
                    break;
                case CardDetectMode.CONTACTLESS:
                    if (request.getFlowType() == FlowType.PAYPASS_STRIP) {
                        posEntryMode = "91";
                    } else {
                        posEntryMode = "07";
                    }
                    break;
                case CardDetectMode.SWIPE:
                    posEntryMode = "90";
                    //fieldSet = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 35, 41,  42, 62};
                    break;
                case CardDetectMode.FALLBACK_SWIPE:
                    posEntryMode = "80";
                    //fieldSet = new int[]{0, 2, 3, 4, 11, 14, 22, 24, 25, 35, 41, 42, 62};
                    break;
                case CardDetectMode.MANUAL:
                    posEntryMode = "01";
                    break;
            }

            posEntryMode += "2";
            request.setPOSEntryMode(posEntryMode);
        }

        String lastFlag = "1";
        String prefixProcessingCode = "";

        if (request.getTransactionType() != null) {
            switch (request.getTransactionType()) {
                case PRE_AUTHORIZATION:
                case AUTHORIZATION:
                case SALE:
                case PRE_AUTHORIZATION_COMPLETION:
                    prefixProcessingCode = "00000";
                    break;
                case REFUND:
                    prefixProcessingCode = "20000";
                    break;
                case ADJUST_SALE:
                    prefixProcessingCode = "02000";
                    break;
                default:
                    System.out.println("unsupport upload type:" + request.getTransactionType());
                    return null;
            }
        }

        if (request.isBeaBatchUploadLastFlag()) {
            lastFlag = "0";
        }
        if (request.getNumberOfInstallment() > 0) {
            fieldSet = addElementToArray(fieldSet, 63);
        }


        request.setProcessingCode(prefixProcessingCode + lastFlag);

        if (request.getFieldIdSet() == null) {
            request.setFieldIdSet(fieldSet);
        }

        Response response = new Response(request);
        List<Response.Error> errors = new ArrayList<>();
        request.setPOSConditionCode("00");
        request.setNetworkInternationalIdentifier("028");
        request.setTerminalIdentification((String) mConfigMap.get(CONFIG_REQUEST_CARD_ACCEPTOR_TERMINAL_ID));
        request.setMerchantIdentifier((String) mConfigMap.get(CONFIG_REQUEST_CARD_ACCEPTOR_ID_CODE));
        byte[] requestForISO8583RepoBytes = ISO8583Conversion.requestToISO8583RepoBytes(request);


        String tpdu = null;
        String eds = null;

        if (mConfigMap.containsKey(CONFIG_REQUEST_TPDU)) {
            tpdu = (String) mConfigMap.get(CONFIG_REQUEST_TPDU);
        }

        if (mConfigMap.containsKey(CONFIG_REQUEST_EDS)) {
            eds = (String) mConfigMap.get(CONFIG_REQUEST_EDS);
        }

        byte[] finalISO8583Message = null;

        if (requestForISO8583RepoBytes != null) {
            finalISO8583Message = prepareBeaAuthorizationMessage(requestForISO8583RepoBytes, tpdu, eds);

            System.out.println("org iso8583Msg:" + Hex.encode(requestForISO8583RepoBytes));
            System.out.println("final iso8583Msg:" + Hex.encode(finalISO8583Message));
        } else {
            System.out.println("requestForISO8583RepoBytes is null, please check your request");
        }

        try {
            String ip = "bea-uat.bindolabs.com";
            String port = "8081";

            if (mConfigMap.containsKey(CONFIG_IP)) {
                ip = (String) mConfigMap.get(CONFIG_IP);
            }
            if (mConfigMap.containsKey(CONFIG_PORT)) {
                port = (String) mConfigMap.get(CONFIG_PORT);
            }

            mSocketService.setHostIp(ip);
            mSocketService.setHostPort(port);
            mSocketService.connect();

            if (mSocketService.isConnected()) {
                mSocketService.sendMessage(finalISO8583Message);

                byte[] receivedDataBytes = mSocketService.receiveMessage();

                response = ISO8583Conversion.iso8583RepoBytesToResponse(response, receivedDataBytes);
            }
        } catch (SSLException e) {
            // TODO: Handle ssl exception
            e.printStackTrace();
            errors.add(new Response.Error("-1", e.getMessage()));
        } catch (SocketException e) {
            // TODO: Handle socket exception
            e.printStackTrace();
            errors.add(new Response.Error("-1", e.getMessage()));
        } catch (Exception e) {
            // TODO: Handle socket exception
            e.printStackTrace();
            errors.add(new Response.Error("-1", e.getMessage()));
        } finally {
            try {
                mSocketService.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response == null) {
                response = new Response();
                response.setErrors(errors);
            }
        }

        return response;
    }

    public Response sendSettlementRequest(Request request) {

        request.setMessageTypeId("0500");

        if (request.isBeaSettlementFirstFlag()) {
            request.setProcessingCode("920000");
        } else {
            request.setProcessingCode("960000");
        }

        int[] fieldSet = new int[]{0, 3, 11, 24, 41, 42, 60, 63};


        if (request.getFieldIdSet() == null) {
            request.setFieldIdSet(fieldSet);
        }

        Response response = new Response(request);
        List<Response.Error> errors = new ArrayList<>();
        request.setPOSConditionCode("00");
        request.setNetworkInternationalIdentifier("028");
        request.setTerminalIdentification((String) mConfigMap.get(CONFIG_REQUEST_CARD_ACCEPTOR_TERMINAL_ID));
        request.setMerchantIdentifier((String) mConfigMap.get(CONFIG_REQUEST_CARD_ACCEPTOR_ID_CODE));
        byte[] requestForISO8583RepoBytes = ISO8583Conversion.requestToISO8583RepoBytes(request);


        String tpdu = mConfigMap.get(CONFIG_REQUEST_TPDU);
        ;
        String eds = mConfigMap.get(CONFIG_REQUEST_EDS);

        byte[] finalISO8583Message = null;

        if (requestForISO8583RepoBytes != null) {
            finalISO8583Message = prepareBeaAuthorizationMessage(requestForISO8583RepoBytes, tpdu, eds);

            System.out.println("org iso8583Msg:" + Hex.encode(requestForISO8583RepoBytes));
            System.out.println("final iso8583Msg:" + Hex.encode(finalISO8583Message));
        } else {
            System.out.println("requestForISO8583RepoBytes is null, please check your request");
        }

        try {
            String ip = (String) mConfigMap.get(CONFIG_IP);
            String port = (String) mConfigMap.get(CONFIG_PORT);
            mSocketService.setHostIp(ip);
            mSocketService.setHostPort(port);
            mSocketService.connect();

            if (mSocketService.isConnected()) {
                mSocketService.sendMessage(finalISO8583Message);

                byte[] receivedDataBytes = mSocketService.receiveMessage();

                response = ISO8583Conversion.iso8583RepoBytesToResponse(response, receivedDataBytes);
            } else {
                errors.add(new Response.Error("-1", "can't connect to BEA host"));
            }
        } catch (SSLException e) {
            // TODO: Handle ssl exception
            e.printStackTrace();
            errors.add(new Response.Error("-1", e.getMessage()));
        } catch (SocketException e) {
            // TODO: Handle socket exception
            e.printStackTrace();
            errors.add(new Response.Error("-1", e.getMessage()));
        } catch (Exception e) {
            // TODO: Handle socket exception
            e.printStackTrace();
            errors.add(new Response.Error("-1", e.getMessage()));
        } finally {
            try {
                mSocketService.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response == null) {
                response = new Response();
                response.setErrors(errors);
            }
        }

        return response;
    }
}
