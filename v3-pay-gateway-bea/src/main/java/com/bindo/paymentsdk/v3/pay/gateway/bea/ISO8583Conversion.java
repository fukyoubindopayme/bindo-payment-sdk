package com.bindo.paymentsdk.v3.pay.gateway.bea;

import com.bindo.paymentsdk.v3.pay.common.emv.enums.EMVTag;
import com.bindo.paymentsdk.v3.pay.common.CardDetectMode;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Request;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Response;
import com.bindo.paymentsdk.v3.pay.common.util.Hex;
import com.bindo.paymentsdk.v3.pay.common.util.StringUtils;
import com.bindo.paymentsdk.v3.pay.common.util.TLVUtils;
import com.iso8583.Element;
import com.iso8583.ISO8583;
import com.iso8583.ISO8583Repo;

import java.text.DecimalFormat;
import java.util.HashMap;

public class ISO8583Conversion {
    public static final String TAG = "ISO8583Conversion";

    public static ISO8583Repo requestToISO8583Repo(Request request) {
        ISO8583Repo iso8583Repo = ISO8583Repo.getInstance();


        iso8583Repo.put(new Element(0, request.getMessageTypeId(), request.getMessageTypeId().length()));
        if (!StringUtils.isEmpty(request.getPrimaryAccountNumber())) {
            iso8583Repo.put(new Element(2, request.getPrimaryAccountNumber(), request.getPrimaryAccountNumber().length()));
        }

        if (!StringUtils.isEmpty(request.getProcessingCode())) {
            iso8583Repo.put(new Element(3, request.getProcessingCode(), request.getProcessingCode().length()));
        }

        String amountTransaction = new DecimalFormat("0000000000.00").format(request.getTransactionAmount()).replace(".", "");
        iso8583Repo.put(new Element(4, amountTransaction, amountTransaction.length()));

        String STAN = String.format("%06d", request.getTransactionSystemTraceAuditNumber());
        iso8583Repo.put(new Element(11, STAN, STAN.length()));
        iso8583Repo.put(new Element(62, STAN, STAN.length()));

        if (!StringUtils.isEmpty(request.getTransactionTime())) {
            // hhmmss
            String transactionTime = request.getTransactionTime();
            iso8583Repo.put(new Element(12, transactionTime, transactionTime.length()));
        }

        if (!StringUtils.isEmpty(request.getTransactionDate())) {
            // yyMM
            String transactionDate = request.getTransactionDate().substring(2);
            iso8583Repo.put(new Element(13, transactionDate, transactionDate.length()));
        }

        if (!StringUtils.isEmpty(request.getCardExpirationDate())) {
            iso8583Repo.put(new Element(14, request.getCardExpirationDate(), request.getCardExpirationDate().length()));
        }

        if (!StringUtils.isEmpty(request.getPOSEntryMode())) {
            iso8583Repo.put(new Element(22, request.getPOSEntryMode(), request.getPOSEntryMode().length()));
        }

        if (!StringUtils.isEmpty(request.getCardSequenceNumber())) {
            iso8583Repo.put(new Element(23, request.getCardSequenceNumber(), request.getCardSequenceNumber().length()));
        }

        if (!StringUtils.isEmpty(request.getNetworkInternationalIdentifier())) {
            iso8583Repo.put(new Element(24, request.getNetworkInternationalIdentifier(), request.getNetworkInternationalIdentifier().length()));
        }

        if (!StringUtils.isEmpty(request.getPOSConditionCode())) {
            iso8583Repo.put(new Element(25, request.getPOSConditionCode(), request.getPOSConditionCode().length()));
        }

        if (!StringUtils.isEmpty(request.getTrack2EquivalentData())) {
            String track2 = request.getTrack2EquivalentData().replaceAll("F", "").replaceAll("=", "D");
            iso8583Repo.put(new Element(35, track2, track2.length()));
        }

        if (!StringUtils.isEmpty(request.getRrn())) {
            iso8583Repo.put(new Element(37, request.getRrn(), request.getRrn().length()));
        }

        if (!StringUtils.isEmpty(request.getAuthCode())) {
            iso8583Repo.put(new Element(38, request.getAuthCode(), request.getAuthCode().length()));
        }

        if (!StringUtils.isEmpty(request.getTerminalIdentification())) {
            iso8583Repo.put(new Element(41, request.getTerminalIdentification(), request.getTerminalIdentification().length()));
        }

        if (!StringUtils.isEmpty(request.getMerchantIdentifier())) {
            iso8583Repo.put(new Element(42, request.getMerchantIdentifier(), request.getMerchantIdentifier().length()));
        }

        // Soft Descriptor
        if (!StringUtils.isEmpty(request.getSoftDescriptor())) {
            iso8583Repo.put(new Element(43, request.getSoftDescriptor(), request.getSoftDescriptor().length()));
        }

//        if (!StringUtils.isEmpty(request.getTrack1DiscretionaryData())) {
//            iso8583Repo.put(new Element(45, request.getTrack1DiscretionaryData(), request.getTrack1DiscretionaryData().length()));
//        }

//        if (!StringUtils.isEmpty(request.getTransactionCurrencyCode())) {
//            iso8583Repo.put(new Element(49, request.getTransactionCurrencyCode(), request.getTransactionCurrencyCode().length()));
//        }

        if (request.getTipAmount() > 0){
            String tipAmount= new DecimalFormat("0000000000.00").format(request.getTipAmount()).replace(".", "");
            iso8583Repo.put(new Element(54, tipAmount, tipAmount.length()));

        }

        if (request.getCardDetectMode() != null) {
            switch (request.getCardDetectMode()) {
                case CardDetectMode.CONTACT:
                case CardDetectMode.CONTACTLESS:
                    HashMap<String, byte[]> tagLengthValues = request.getTagLengthValues();
                    String[] tagSet = new String[]{EMVTag.TRACK_2_EQUIVALENT_DATA.toString(),
                            EMVTag.APPLICATION_PRIMARY_ACCOUNT_NUMBER.toString(), EMVTag.TRANSACTION_CURRENCY_CODE.toString(),
                            EMVTag.APPLICATION_PRIMARY_ACCOUNT_NUMBER_SEQUENCE_NUMBER.toString(), EMVTag.APPLICATION_INTERCHANGE_PROFILE.toString(),
                            EMVTag.DEDICATED_FILE_NAME.toString(), EMVTag.TERMINAL_VERIFICATION_RESULTS.toString(), EMVTag.TRANSACTION_DATE.toString(),
                            EMVTag.TRANSACTION_STATUS_INFORMATION.toString(), EMVTag.TRANSACTION_TYPE.toString(),
                            EMVTag.AMOUNT_AUTHORISED_NUMERIC.toString(), EMVTag.AMOUNT_OTHER_NUMERIC.toString(),
                            EMVTag.APPLICATION_VERSION_NUMBER_ICC.toString(), EMVTag.APPLICATION_VERSION_NUMBER_TERMINAL.toString(),
                            EMVTag.TERMINAL_COUNTRY_CODE.toString(), EMVTag.INTERFACE_DEVICE_SERIAL_NUMBER.toString(),
                            EMVTag.APPLICATION_CRYPTOGRAM.toString(), EMVTag.CRYPTOGRAM_INFORMATION_DATA.toString(),
                            EMVTag.TERMINAL_CAPABILITIES.toString(), EMVTag.CARDHOLDER_VERIFICATION_METHOD_RESULTS.toString(),
                            EMVTag.TERMINAL_TYPE.toString(), EMVTag.APPLICATION_TRANSACTION_COUNTER.toString(),
                            EMVTag.UNPREDICTABLE_NUMBER.toString(), EMVTag.TRANSACTION_SEQUENCE_COUNTER.toString(),
                            EMVTag.ISSUER_APPLICATION_DATA.toString(), EMVTag.TRACK1_CVC3.toString(), EMVTag.TRACK2_CVC3.toString(), EMVTag.TRACK1_PCVC3.toString(), EMVTag.TRACK1_NATC.toString(),
                            EMVTag.TRACK1_PUNATC.toString(), EMVTag.TRACK2_PCVC3.toString(), EMVTag.TRACK2_NATC.toString(), EMVTag.TRACK2_PUNATC.toString(),
                            EMVTag.UN_NUMERIC.toString(), EMVTag.TRACK2_DATA.toString()};//, EMVTag.TRANSACTION_CATEGORY_CODE.toString(),

                    StringBuilder iccdataBuilder = new StringBuilder();

                    for (String tag : tagSet) {
                        byte[] value = tagLengthValues.get(tag);

                        if (value != null && value.length > 0) {

//                            if (tag.equalsIgnoreCase(EMVTag.FORM_FACTOR_INDICATOR.toString())) {
//                                if (value.length > 4) {
//                                    String v = Hex.encode(value).substring(4, 4 + 8);
//
//                                    iccdataBuilder.append(tag);
//                                    iccdataBuilder.append(String.format("%02X", (v.length()/2)));
//                                    iccdataBuilder.append(v);
//                                }
//                            } else {
                                iccdataBuilder.append(tag);
                                iccdataBuilder.append(String.format("%02X", value.length));
                                iccdataBuilder.append(Hex.encode(value));
//                            }

//                            if (tag.equalsIgnoreCase(EMVTag.APPLICATION_PRIMARY_ACCOUNT_NUMBER_SEQUENCE_NUMBER.toString()))
//                            {
//                                request.setCardSequenceNumber("0"+Hex.encode(value));
//                                iso8583Repo.put(new Element(23, request.getCardSequenceNumber(), request.getCardSequenceNumber().length()));
//                            }
                        }else{
                            System.out.println("requestToISO8583Repo: tag:" + tag + " not found");
                        }
                    }

                    iso8583Repo.put(new Element(55, iccdataBuilder.toString(), iccdataBuilder.length()));
                    break;

                case CardDetectMode.FALLBACK_SWIPE:

                    iso8583Repo.put(new Element(55, "DFEC0101", "DFEC0101".length()));
                    break;
            }
        }

        if (request.getBatchNO() > 0){
            String batchNo = new DecimalFormat("000000").format(request.getBatchNO());
            iso8583Repo.put(new Element(60, batchNo, batchNo.length()));
        } else if (request.getOriginalTransactionAmount() > 0.00) {
            String orginAmt = new DecimalFormat("0000000000.00").format(request.getOriginalTransactionAmount()).replace(".", "");
            iso8583Repo.put(new Element(60, orginAmt, orginAmt.length()));
        }

        if (request.getBatchTotals() != null){
            String batchTotals = String.format("%03d%012d%03d%012d%03d%012d%03d%012d%03d%012d%03d%012d",
                    request.getBatchTotals().getCapturedSalesCount(), request.getBatchTotals().getCapturedSalesAmount(),
                    request.getBatchTotals().getCapturedRefundCount(), request.getBatchTotals().getCapturedRefundAmount(),
                    request.getBatchTotals().getDebitSalesCount(), request.getBatchTotals().getDebitSalesAmount(),
                    request.getBatchTotals().getDebitRefundCount(), request.getBatchTotals().getDebitRefundAmount(),
                    request.getBatchTotals().getAuthorizeSalesCount(), request.getBatchTotals().getAuthorizeSalesAmount(),
                    request.getBatchTotals().getAuthorizeRefundCount(), request.getBatchTotals().getAuthorizeRefundAmount());
            iso8583Repo.put(new Element(63, batchTotals, batchTotals.length()));
        }else if (request.getNumberOfInstallment() > 0){
            String numOfInstallment = String.format("%02d",request.getNumberOfInstallment());
            iso8583Repo.put(new Element(63, numOfInstallment, numOfInstallment.length()));
        }

        return iso8583Repo;
    }


    public static byte[] requestToISO8583RepoBytes(Request request) {

        ISO8583Repo iso8583Repo = ISO8583Conversion.requestToISO8583Repo(request);

        ISO8583 iso8583 = new ISO8583() {
            @Override
            public int onGenMac(byte[] msg, int offset, int len, byte index, byte[] mac) {
                return 0;
            }
        };
        iso8583.initBEAISO8583();

        byte[] iso8583Msg = new byte[1024];
        int ret = iso8583.generateISO8583Msg(iso8583Msg, (byte) 0x00, request.getFieldIdSet());

        if (ret <= 0) {
            System.out.println("generateISO8583Msg failed:" + ret);
            return null;
        }

        byte[] finalMsg = new byte[ret];
        System.arraycopy(iso8583Msg, 0, finalMsg, 0, ret);
        return finalMsg;
    }

    public static Response iso8583RepoBytesToResponse(final Response response, byte[] iso8583RepoBytes) {

        if (iso8583RepoBytes == null) {
            return null;
        }

        ISO8583 iso8583 = new ISO8583() {
            @Override
            public int onGenMac(byte[] msg, int offset, int len, byte index, byte[] mac) {
                return 0;
            }
        };

        iso8583.parseISO8583Msg(iso8583RepoBytes, 5 + 2, 8);
        ISO8583Repo iso8583Repo = ISO8583Repo.getInstance();
//        final Response response = new Response();

        Element element;

        element = iso8583Repo.getElement(3);

        if (element != null) {
            response.setTransactionProcessingCode(element.getValue());
        }

        element = iso8583Repo.getElement(4);

        if (element != null) {
            response.setTransactionAmount(Double.parseDouble(element.getValue()) / 100);
        }

        element = iso8583Repo.getElement(6);

        if (element != null) {
            response.setCardholderBillingAmount(Double.parseDouble(element.getValue()) / 100);
        }

        element = iso8583Repo.getElement(10);

        if (element != null) {
            //response.setConversionRate(element.getValue());
        }

        element = iso8583Repo.getElement(11);

        if (element != null) {
            response.setTransactionSystemTraceAuditNumber(Integer.parseInt(element.getValue()));
        }

        element = iso8583Repo.getElement(12);

        if (element != null) {
            response.setTransactionTime(element.getValue());
        }

        element = iso8583Repo.getElement(13);

        if (element != null && StringUtils.isEmpty(response.getTransactionDate())) {
            String transactionDate = response.getTransactionDate();
            response.setTransactionDate(transactionDate.substring(0, 2) + element.getValue());
        }

        element = iso8583Repo.getElement(37);

        if (element != null) {
            response.setRetrievalReferenceNumber(element.getValue());
        }

        element = iso8583Repo.getElement(38);

        if (element != null) {
            response.setAuthorizationCode(element.getValue());
        }

        element = iso8583Repo.getElement(39);

        if (element != null) {
            response.setActionCode(element.getValue());
            if (response.getActionCode().equals("00")) {
                response.setApproved(true);
            }
        }

        element = iso8583Repo.getElement(55);

        if (element != null) {
            byte[] rspDE55 = Hex.decode(element.getValue());

            TLVUtils.parseTLVString(rspDE55, rspDE55.length, true, new TLVUtils.OnDecodeTlvObjectListner() {
                @Override
                public void onDecode(int tag, int len, byte[] value) {
                    switch (tag) {

                        case 0x91:
                            response.setIccRelatedDataIssuerAuthenticationData(Hex.encode(value));
                            break;
                        case 0x71:
                            response.setIccRelatedDataIssuerScriptData("71" + String.format("%02X", len) + Hex.encode(value));
                            break;
                        case 0x72:
                            response.setIccRelatedDataIssuerScriptData(response.getIccRelatedDataIssuerScriptData() + "72" + String.format("%02X", len) + Hex.encode(value));
                            break;
                    }
                }
            });

        }

        return response;
    }
}
