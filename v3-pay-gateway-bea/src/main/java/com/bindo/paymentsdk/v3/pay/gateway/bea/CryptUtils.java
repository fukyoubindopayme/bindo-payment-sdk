package com.bindo.paymentsdk.v3.pay.gateway.bea;


import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class CryptUtils {

    public static byte[] encryptByDES(byte[] keybyte, byte[] src) {
        try {
            SecretKey deskey = new SecretKeySpec(keybyte, "DES");
            Cipher c1 = Cipher.getInstance("DES");
            c1.init(Cipher.ENCRYPT_MODE, deskey);
            return c1.doFinal(src);
        } catch (java.security.NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        } catch (javax.crypto.NoSuchPaddingException e2) {
            e2.printStackTrace();
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        return null;
    }

    public static byte[] decryptByDES(byte[] keybyte, byte[] src) {
        try {
            SecretKey deskey = new SecretKeySpec(keybyte, "DES");
            Cipher c1 = Cipher.getInstance("DES");
            c1.init(Cipher.DECRYPT_MODE, deskey);
            return c1.doFinal(src);
        } catch (java.security.NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        } catch (javax.crypto.NoSuchPaddingException e2) {
            e2.printStackTrace();
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        return null;
    }

    public static byte[] encryptBy3DES(byte[] key, byte[] data) {
        byte[] keyByte = new byte[24];
        switch (key.length) {
            case 8:
                System.arraycopy(key, 0, keyByte, 0, 8);
                System.arraycopy(key, 0, keyByte, 8, 8);
                System.arraycopy(key, 0, keyByte, 16, 8);
                break;
            case 16:
                System.arraycopy(key, 0, keyByte, 0, 16);
                System.arraycopy(key, 0, keyByte, 16, 8);
                break;
            case 24:
                System.arraycopy(key, 0, keyByte, 0, 24);
                break;
            default:
                if (key.length < 24) {
                    System.arraycopy(key, 0, keyByte, 0, key.length);
                    System.arraycopy(new byte[24 - key.length], 0, keyByte, key.length, 24 - key.length);
                } else {
                    System.arraycopy(key, 0, keyByte, 0, 24);
                }
                break;
        }
        try {
            SecretKey deskey = new SecretKeySpec(keyByte, "DESede");
            Cipher cipher = Cipher.getInstance("DESede/CBC/NoPadding");
            byte[] IV = new byte[8];
            IvParameterSpec iv = new IvParameterSpec(IV);
            cipher.init(Cipher.ENCRYPT_MODE, deskey, iv);
            return cipher.doFinal(data);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] decryptBy3DES(byte[] key, byte[] data) {
        byte[] keyByte = new byte[24];
        switch (key.length) {
            case 8:
                System.arraycopy(key, 0, keyByte, 0, 8);
                System.arraycopy(key, 0, keyByte, 8, 8);
                System.arraycopy(key, 0, keyByte, 16, 8);
                break;
            case 16:
                System.arraycopy(key, 0, keyByte, 0, 16);
                break;
            case 24:
                System.arraycopy(key, 0, keyByte, 0, 24);
                break;
            default:
                if (key.length < 24) {
                    System.arraycopy(key, 0, keyByte, 0, key.length);
                    System.arraycopy(new byte[24 - key.length], 0, keyByte, key.length, 24 - key.length);
                } else {
                    System.arraycopy(key, 0, keyByte, 0, 24);
                }
                break;
        }
        try {
            SecretKey deskey = new SecretKeySpec(key, "DESede");
            Cipher cipher = Cipher.getInstance("DESede/CBC");
            byte[] IV = new byte[8];
            IvParameterSpec iv = new IvParameterSpec(IV);
            cipher.init(Cipher.DECRYPT_MODE, deskey, iv);
            return cipher.doFinal(data);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] encryptByRSA(byte[] modulus, byte[] publicExponent, byte[] data) {
        BigInteger mod = new BigInteger(modulus);
        BigInteger exp = new BigInteger(publicExponent);
        try {
            RSAPublicKeySpec keySpec = new RSAPublicKeySpec(mod, exp);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PublicKey publicKey = keyFactory.generatePublic(keySpec);
            System.out.println("publickey length = " + publicKey.getEncoded().length);
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            return cipher.doFinal(data);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] decryptByRSA(byte[] modulus, byte[] privateExponet, byte[] data) {
        BigInteger mod = new BigInteger(modulus);
        BigInteger exp = new BigInteger(privateExponet);
        try {
            RSAPrivateKeySpec keySpec = new RSAPrivateKeySpec(mod, exp);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PrivateKey privateKey = keyFactory.generatePrivate(keySpec);
            System.out.println("privateKey length = " + privateKey.getEncoded().length);
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            return cipher.doFinal(data);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
