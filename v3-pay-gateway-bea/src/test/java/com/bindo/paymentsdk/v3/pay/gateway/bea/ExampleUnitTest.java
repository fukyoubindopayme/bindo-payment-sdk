package com.bindo.paymentsdk.v3.pay.gateway.bea;

import com.bindo.paymentsdk.v3.pay.common.CardDetectMode;
import com.bindo.paymentsdk.v3.pay.common.ReversalType;
import com.bindo.paymentsdk.v3.pay.common.TransactionType;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Request;
import com.bindo.paymentsdk.v3.pay.common.gateway.bean.Response;
import com.bindo.paymentsdk.v3.pay.common.util.Log;
import com.bindo.paymentsdk.v3.pay.gateway.Gateway;

import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }



    @Test
    public void Test_BEA_Production() throws Exception {
        HashMap<String, String> config = new HashMap<>();
        HashMap<String, String> gatewayConfig = new HashMap<>();
        config.put("config_request_card_acceptor_terminal_id", "42850002");
        config.put("config_request_card_acceptor_id_code", "581212674403000");
        config.put("config_request_tpdu", "7000280000");
        config.put("config_request_eds", "0068000A00F000");

        config.put("config_tmk", "0000000000000000");
        config.put("ip", "10.13.137.1");
        config.put("port", "8081");

        Gateway gateway = new BankOfEastAsiaGateway(config);
        Request req = new Request();
        req.setTransactionType(TransactionType.SALE);
        req.setTransactionAmount(1.23);
        req.setTransactionSystemTraceAuditNumber(1);
        req.setBatchNO(1);
        req.setCardDetectMode(CardDetectMode.MANUAL);
        req.setPrimaryAccountNumber("4445222299990007");
        req.setCardExpirationDate("1912");
        req.setGatewayConfigs(gatewayConfig);
        Response resp = gateway.sendRequest(req);
        System.out.print(resp.toString());
    }
}