package com.bindo.paymentsdk.terminal.urovo;

import android.content.Context;

import com.bindo.paymentsdk.terminal.BarcodeReader;
import com.bindo.paymentsdk.terminal.GiftCardReader;
import com.bindo.paymentsdk.terminal.OctopusCardReader;
import com.bindo.paymentsdk.terminal.Printer;
import com.bindo.paymentsdk.terminal.Terminal;

public class I9000STerminal implements Terminal {

    private Printer mBuiltInPrinter;
    private BarcodeReader mBarcodeReader;
    private GiftCardReader mGiftCardReader;
    private OctopusCardReader mOctopusCardReader;

    public I9000STerminal(Context context) {
        mBuiltInPrinter = new I9000SPrinter(context);
        mBarcodeReader = new I9000SBarcodeReader(context);
        mGiftCardReader = new I9000SGiftCardReader();
        mOctopusCardReader = new I9000SOctopusCardReader();
    }

    @Override
    public Printer getBuiltInPrinter() {
        return mBuiltInPrinter;
    }

    @Override
    public BarcodeReader getBarcodeReader() {
        return mBarcodeReader;
    }

    @Override
    public GiftCardReader getGiftCardReader() {
        return mGiftCardReader;
    }

    @Override
    public OctopusCardReader getOctopusCardReader() {
        return mOctopusCardReader;
    }
}
