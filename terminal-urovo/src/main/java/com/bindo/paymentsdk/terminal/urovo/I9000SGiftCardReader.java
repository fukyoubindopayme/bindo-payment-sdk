package com.bindo.paymentsdk.terminal.urovo;

import android.device.MagManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import com.bindo.paymentsdk.terminal.GiftCardReader;

public class I9000SGiftCardReader implements GiftCardReader {
    private static final String TAG = "I9000sGiftCardReader";

    private final static int MESSAGE_MAG_READER_READ   = 1;
    private final static int MESSAGE_MAG_READER_FAILED = 2;

    private Listener mListener;
    private MagManager mMagManager;
    private MagReaderThread mMagReaderThread;
    private Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            Bundle data = msg.getData();
            switch (msg.what) {
                case MESSAGE_MAG_READER_READ:
                    String track1 = data.getString("track1");
                    String track2 = data.getString("track2");
                    String track3 = data.getString("track3");
                    mListener.onResult(track1, track2, track3);
                    break;
                case MESSAGE_MAG_READER_FAILED:
                    String message = data.getString("message");
                    if (TextUtils.isEmpty(message)) {
                        message = "Unknown error";
                    }
                    mListener.onError("-1", message);
                    break;
            }
        }
    };

    private Handler mTimeoutHandler;
    private Runnable mTimeoutRunnable;

    public I9000SGiftCardReader() {
        mMagManager = new MagManager();
        mTimeoutHandler = new Handler(Looper.getMainLooper());
    }

    @Override
    public boolean isSupported() {
        return true;
    }

    @Override
    public void setListener(Listener listener) {
        mListener = listener;
    }

    @Override
    public synchronized void start() {
        this.mTimeoutRunnable  = new Runnable() {
            @Override
            public void run() {
                stop();
            }
        };
        this.mTimeoutHandler.postDelayed(this.mTimeoutRunnable, 20000);
        if (mMagReaderThread != null) {
            mMagReaderThread.kill();
        }
        mMagReaderThread = new MagReaderThread();
        mMagReaderThread.start();
        mListener.onStart();
    }

    @Override
    public synchronized void stop() {
        this.mTimeoutHandler.removeCallbacks(this.mTimeoutRunnable);
        if (mMagManager != null) {
            mMagManager.close();
        }
        if (mMagReaderThread != null) {
            mMagReaderThread.kill();
        }
        mListener.onStop();
    }

    private class MagReaderThread extends Thread {
        private boolean mKilled = false;

        MagReaderThread() {
            super("Urovo-I9000s MagGiftCardReaderThread");
        }

        public void kill() {
            mKilled = true;
        }

        public void run() {
            if (mMagManager != null) {
                int ret = mMagManager.open();
                if (ret != 0) {
                    Bundle data = new Bundle();
                    data.putString("message", "Init mag reader failed.");
                    Message message = new Message();
                    message.what = MESSAGE_MAG_READER_FAILED;
                    message.setData(data);
                    mHandler.sendMessage(message);
                    return;
                }
            }
            while (!mKilled) {
                Log.d(TAG, this.getName() + " : " + getId());
                if (isInterrupted()) {
                    break;
                }
                if (mMagManager == null)
                    return;
                int ret = mMagManager.checkCard();
                if (ret != 0) {
                    // 未读到卡，等待 600 毫秒后重试
                    try {
                        Thread.sleep(600);
                    } catch (Exception e) {
                        // Ignore
                    }
                    continue;
                } else {
                    try {
                        Thread.sleep(500);
                    } catch (Exception e) {
                        // Ignore
                    }
                }
                byte[] stripInfo = new byte[1024];
                int allLen = mMagManager.getAllStripInfo(stripInfo);
                if (allLen > 0) {
                    String track1 = null, track2 = null, track3 = null;
                    int track1Length = stripInfo[1];
                    if (track1Length != 0) {
                        track1 = new String(stripInfo, 2, track1Length);
                    }
                    int track2Length = stripInfo[3 + track1Length];
                    if (track2Length != 0) {
                        track2 = new String(stripInfo, 4 + track1Length, track2Length);
                    }
                    int track3Length = stripInfo[5 + track1Length];
                    if (track3Length != 0) {
                        track3 = new String(stripInfo, 6 + track1Length + track2Length, track3Length);
                    }

                    if ((track1 == null && track2 == null && track3 == null) ||
                            (track1 != null && !track1.contains("BINDO"))) {
                        Bundle data = new Bundle();
                        data.putString("message", "Invalid gift card");
                        Message message = new Message();
                        message.what = MESSAGE_MAG_READER_FAILED;
                        message.setData(data);
                        mHandler.sendMessage(message);
                    } else {
                        Bundle data = new Bundle();
                        data.putString("track1", "%" + track1 + "?");
                        data.putString("track2", track2);
                        data.putString("track3", track3);

                        Message message = new Message();
                        message.what = MESSAGE_MAG_READER_READ;
                        message.setData(data);
                        mHandler.sendMessage(message);
                    }
                }
                try {
                    Thread.sleep(800);
                } catch (Exception e) {
                    // Ignore
                }
            }
        }
    }
}
