package com.bindo.paymentsdk.terminal.urovo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.device.ScanManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Vibrator;


import com.bindo.paymentsdk.terminal.BarcodeReader;

import static android.device.ScanManager.BARCODE_STRING_TAG;
import static android.device.ScanManager.BARCODE_TYPE_TAG;
import static com.bindo.paymentsdk.terminal.BarcodeReader.Format.AZTEC;
import static com.bindo.paymentsdk.terminal.BarcodeReader.Format.CODABAR;
import static com.bindo.paymentsdk.terminal.BarcodeReader.Format.CODE_128;
import static com.bindo.paymentsdk.terminal.BarcodeReader.Format.CODE_39;
import static com.bindo.paymentsdk.terminal.BarcodeReader.Format.CODE_93;
import static com.bindo.paymentsdk.terminal.BarcodeReader.Format.DATA_MATRIX;
import static com.bindo.paymentsdk.terminal.BarcodeReader.Format.EAN_13;
import static com.bindo.paymentsdk.terminal.BarcodeReader.Format.EAN_8;
import static com.bindo.paymentsdk.terminal.BarcodeReader.Format.ITF;
import static com.bindo.paymentsdk.terminal.BarcodeReader.Format.MAXICODE;
import static com.bindo.paymentsdk.terminal.BarcodeReader.Format.NONE;
import static com.bindo.paymentsdk.terminal.BarcodeReader.Format.PDF_417;
import static com.bindo.paymentsdk.terminal.BarcodeReader.Format.QR_CODE;
import static com.bindo.paymentsdk.terminal.BarcodeReader.Format.UPC_A;
import static com.bindo.paymentsdk.terminal.BarcodeReader.Format.UPC_E;

public class I9000SBarcodeReader implements BarcodeReader {
    private static final String TAG = "I9000SBarcodeReader";

    private Context mContext;
    private ScanManager mScanManager;
    private SoundPool mSoundPool;
    private int mSoundID;
    private Vibrator mVibrator;
    private Listener mListener;

    private BroadcastReceiver mScanReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mSoundPool.play(mSoundID, 1, 1, 0, 0, 1);
            mVibrator.vibrate(100);

            byte barcodeType = intent.getByteExtra(BARCODE_TYPE_TAG, (byte) 0);
            String barcodeString = intent.getStringExtra(BARCODE_STRING_TAG);

            mListener.onResult(intValueToBarcodeFormat(barcodeType), barcodeString);
        }
    };

    public I9000SBarcodeReader(Context context) {
        mContext = context;
        mSoundPool = new SoundPool(1, AudioManager.STREAM_NOTIFICATION, 100); // MODE_RINGTONE
        mSoundID = mSoundPool.load("/etc/Scan_new.ogg", 1);

        mVibrator = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
    }

    @Override
    public boolean isSupported() {
        return true;
    }

    @Override
    public void setListener(Listener listener) {
        mListener = listener;
    }

    @Override
    public void openScanner() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ScanManager.ACTION_DECODE);
        mContext.registerReceiver(mScanReceiver, intentFilter);

        if (mScanManager == null) {
            mScanManager = new ScanManager();
            mScanManager.openScanner();
            mScanManager.switchOutputMode(0);
        }
        mListener.onOpen();
    }

    @Override
    public void closeScanner() {
        try {
            mContext.unregisterReceiver(mScanReceiver);
        } catch (IllegalArgumentException e) {
            // 忽略由于未注册而引起的崩溃
        }
        this.stopDecode();
        mListener.onClose();
    }

    @Override
    public void startDecode() {
        if (mScanManager != null) {
            mScanManager.startDecode();
        }
    }

    @Override
    public void stopDecode() {
        if (mScanManager != null) {
            mScanManager.stopDecode();
        }
    }

    @Override
    public void startScan() {

    }

    @Override
    public void stopScan() {

    }

    public static BarcodeReader.Format intValueToBarcodeFormat(int value) {
        switch (value) {
            case 0x7a:
                return AZTEC;
            case 0x61:
                return CODABAR;
            case 0x62:
                return CODE_39;
            case 0x69:
                return CODE_93;
            case 0x6a:
                return CODE_128;
            case 0x77:
                return DATA_MATRIX;
            case 0x44:
                return EAN_8;
            case 0x64:
                return EAN_13;
            case 0x65:
                return ITF;
            case 0x78:
                return MAXICODE;
            case 0x72:
                return PDF_417;
            case 0x73:
                return QR_CODE;
//            return RSS_14;
//            return RSS_EXPANDED;
            case 0x63:
                return UPC_A;
            case 0x45:
                return UPC_E;
//            return UPC_EAN_EXTENSION;
        }
        return NONE;
    }
}
