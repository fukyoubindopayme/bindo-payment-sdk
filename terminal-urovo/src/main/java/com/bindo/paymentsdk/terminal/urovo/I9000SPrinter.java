package com.bindo.paymentsdk.terminal.urovo;

import android.content.Context;
import android.device.PrinterManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Base64;

import com.bindo.paymentsdk.terminal.Printer;

import java.util.List;

public class I9000SPrinter extends Printer {
    private static final String TAG = "I9000sPrinter";

    private final int PAPER_PRINT_WIDTH = 384;
    private final int PAPER_PRINT_PADDING = 5;

    private final int FONT_SIZE_DEFAULT = 24;
    private final int FONT_SIZE_LARGE = 36;
    private final int FONT_SIZE_BIG = 48;

    private final int BARCODE_CODE128 = 20;
    private final int BARCODE_QRCODE = 58;

    private Context mContext;
    private PrinterManager mPrinterManager;

    public I9000SPrinter(Context context) {
        mContext = context;
    }

    @Override
    public boolean isSupported() {
        return true;
    }

    @Override
    public void print(Paper paper) {
        if (mPrinterManager == null) {
            mPrinterManager = new PrinterManager();
        }

        mPrinterManager.setupPage(PAPER_PRINT_WIDTH, -1);

        List<Paper.Command> commands = paper.getCommands();

        int currentY = PAPER_PRINT_PADDING;
        int currentFontSize;
        int currentAlign;

        for (Paper.Command command : commands) {
//            switch (command.getAlign()) {
//                case Paper.ALIGN_LEFT:
//                    currentAlign = PrinterManager.CONTENT_ALIGN_LEFT;
//                    break;
//                case Paper.ALIGN_CENTER:
//                    currentAlign = PrinterManager.CONTENT_ALIGN_CENTER;
//                    break;
//                case Paper.ALIGN_RIGHT:
//                    currentAlign = PrinterManager.CONTENT_ALIGN_RIGHT;
//                    break;
//                default:
//                    currentAlign = PrinterManager.CONTENT_ALIGN_LEFT;
//                    break;
//            }

            switch (command.getSize()) {
                case Paper.SIZE_LARGE:
                    currentFontSize = FONT_SIZE_LARGE;
                    break;
                case Paper.SIZE_BIG:
                    currentFontSize = FONT_SIZE_BIG;
                    break;
                default:
                    currentFontSize = FONT_SIZE_DEFAULT;
                    break;
            }

            switch (command.getKey()) {
                case Paper.COMMAND_NEW_LINE:
                    currentY += mPrinterManager.drawTextEx(" ", 5, currentY, PAPER_PRINT_WIDTH, -1, "arial", currentFontSize, 0, 0, 0);
                    break;
                case Paper.COMMAND_TEXT:
//                    int currentX = PAPER_PRINT_PADDING;
//                    // 计算文字对齐方式
//                    if (currentAlign != PrinterManager.CONTENT_ALIGN_LEFT) {
//                        float textWidth = calculateTextWidth(currentFontSize, command.getValue());
//                        int printWidth = PAPER_PRINT_WIDTH - (PAPER_PRINT_PADDING * 2);
//                        switch (currentAlign) {
//                            case PrinterManager.CONTENT_ALIGN_CENTER:
//                                currentX = (int) (printWidth - textWidth) / 2;
//                                break;
//                            case PrinterManager.CONTENT_ALIGN_RIGHT:
//                                currentX = (int) (printWidth - textWidth);
//                                break;
//                        }
//                    }

//                    currentY += mPrinterManager.drawTextEx(
//                            command.getValue(),
//                            currentX,
//                            currentY,
//                            PAPER_PRINT_WIDTH,
//                            -1,
//                            "arial",
//                            currentFontSize,
//                            0,
//                            0,
//                            0
//                    );

                    Paint.Align align = Paint.Align.LEFT;
                    switch (command.getAlign()) {
                        case Paper.ALIGN_LEFT:
                            align = Paint.Align.LEFT;
                            break;
                        case Paper.ALIGN_CENTER:
                            align = Paint.Align.CENTER;
                            break;
                        case Paper.ALIGN_RIGHT:
                            align = Paint.Align.RIGHT;
                            break;
                        default:
                            align = Paint.Align.LEFT;
                            break;
                    }

                    Bitmap textBitmap = createBitmapFromText(command.getValue(), currentFontSize, align);

                    mPrinterManager.drawBitmap(textBitmap, 0, currentY);
                    currentY += textBitmap.getHeight();
                    break;
                case Paper.COMMAND_IMAGE:
                    byte[] base64Bytes = Base64.decode(command.getValue(), Base64.DEFAULT);
                    Bitmap originalBitmap = BitmapFactory.decodeByteArray(base64Bytes, 0, base64Bytes.length);

                    final float scale = (384f / (float) originalBitmap.getWidth());

                    final int width = (int) ((float) originalBitmap.getWidth() * scale);
                    final int height = (int) ((float) originalBitmap.getHeight() * scale);

                    Bitmap resizedBitmap = Bitmap.createScaledBitmap(originalBitmap, width, height, false);

                    Bitmap newBitmap = Bitmap.createBitmap(resizedBitmap.getWidth(), resizedBitmap.getHeight(), resizedBitmap.getConfig());
                    Canvas canvas = new Canvas(newBitmap);
                    canvas.drawColor(Color.WHITE);
                    canvas.drawBitmap(resizedBitmap, 0, 0, null);

                    mPrinterManager.drawBitmap(newBitmap, 0, currentY);
                    currentY += newBitmap.getHeight();
                    break;
                case Paper.COMMAND_LINE:
                    currentY += mPrinterManager.drawTextEx(
                            "----------------------------------------------",
                            PAPER_PRINT_PADDING,
                            currentY,
                            PAPER_PRINT_WIDTH,
                            -1,
                            "arial",
                            currentFontSize,
                            0,
                            0,
                            0
                    );
                    break;
                case Paper.COMMAND_BARCODE:
                    mPrinterManager.drawBarcode(command.getValue(), PAPER_PRINT_PADDING, currentY, BARCODE_CODE128, 2, 100, 0);
                    currentY += 100;
                    break;
                case Paper.COMMAND_QRCODE:
                    currentY += mPrinterManager.drawBarcode(command.getValue(), PAPER_PRINT_PADDING, currentY, BARCODE_QRCODE, 2, 20, 0);
                    break;
                case Paper.COMMAND_CUT_PAPER:
                    currentY += mPrinterManager.drawTextEx("\n\n\n\n\n", PAPER_PRINT_PADDING, currentY, PAPER_PRINT_WIDTH, -1, "arial", currentFontSize, 0, 0, 0);
                    mPrinterManager.paperFeed(0);
                    break;
            }
        }

        int result = mPrinterManager.printPage(0);
        switch (result) {
            case -1:
                mListener.onError("-1", "Out of paper");
                break;
            case 0:
                mListener.onPrinted();
                break;
            default:
                mListener.onError("-1", "Unknown error");
                break;
        }
    }

    /**
     * 计算文本显示宽度
     *
     * @param fontSize
     * @param text
     * @return 文本宽度
     */
    private int calculateTextWidth(float fontSize, String text) {
        Paint paint = new Paint();
        paint.setTextSize(fontSize);
        float textWidth = paint.measureText(text);
        return (int) textWidth;
    }

    private Bitmap createBitmapFromText(String printText, int textSize, Paint.Align align) {
        Typeface typeface = Typeface.create(Typeface.DEFAULT, Typeface.NORMAL);
//        try {
//            typeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/Merchant Copy Doublesize.ttf");
//        } catch (Exception e) {
//            e.printStackTrace();
//            // Ignore
//        }

        Paint paint = new Paint();
        Bitmap bitmap;
        Canvas canvas;

        paint.setTextSize(textSize);
        paint.setTypeface(typeface);
        paint.getTextBounds(printText, 0, printText.length(), new Rect());

        TextPaint textPaint = new TextPaint(paint);
        textPaint.setTextAlign(align);
        android.text.StaticLayout staticLayout = new StaticLayout(printText, textPaint, PAPER_PRINT_WIDTH, Layout.Alignment.ALIGN_NORMAL, 1, 0, false);

        // Create bitmap
        bitmap = Bitmap.createBitmap(staticLayout.getWidth(), staticLayout.getHeight(), Bitmap.Config.ARGB_8888);

        // Create canvas
        canvas = new Canvas(bitmap);
        canvas.drawColor(Color.WHITE);
        switch (align) {
            case LEFT:
                canvas.translate(0, 0);
                break;
            case RIGHT:
                canvas.translate(bitmap.getWidth(), 0);
                break;
            case CENTER:
                canvas.translate(bitmap.getWidth() / 2, 0);
                break;
        }
        staticLayout.draw(canvas);

        return bitmap;
    }
}


