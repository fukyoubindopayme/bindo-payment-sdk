package com.bindo.paymentsdk.terminal.urovo;

import com.bindo.paymentsdk.terminal.OctopusCardReader;

public class I9000SOctopusCardReader implements OctopusCardReader {
    private Listener mListener;

    @Override
    public boolean isSupported() {
        return true;
    }

    @Override
    public void setListener(Listener listener) {
        mListener = listener;
    }

    @Override
    public void start() {
        mListener.onError("-1", "This device may not be supported.");
    }

    @Override
    public void stop() {
        mListener.onError("-1", "This device may not be supported.");
    }
}
