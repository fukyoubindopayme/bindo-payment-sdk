package com.bindo.paymentsdk.v3.pay.tools;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.bindo.paymentsdk.v3.pay.tools.util.Hex;

public class Main {

    @Parameter(names = {"--command", "-c"})
    private String command;
    @Parameter(names={"--kvec1"})
    protected String kvec1;
    @Parameter(names={"--kvec2"})
    protected String kvec2;


    public void tmkCalc() {
        byte[] kvec1 = Hex.decode(this.kvec1);
        byte[] kvec2 = Hex.decode(this.kvec2);

        byte[] key = new byte[kvec1.length];

        for (int i = 0; i < kvec1.length; i++) {
            key[i] = (byte) (kvec1[i] ^ kvec2[i]);
        }

        System.out.println(Hex.encode(key));
    }


    public static final String COMMAND_TMK_CALC = "tmk-calc";

    public static void main(String[] args) {
        Main main = new Main();
        JCommander.newBuilder()
                .addObject(main)
                .build()
                .parse(args);

        switch (main.command) {
            case COMMAND_TMK_CALC:
                main.tmkCalc();
                break;
            default:
                break;
        }

    }
}
